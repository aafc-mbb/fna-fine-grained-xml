<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">136</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">140</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="N. H. Holmgren" date="1978" rank="species">goodrichii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>30: 416, fig. 1. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species goodrichii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>63.</number>
  <other_name type="common_name">Lapoint or Goodrich’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, (9–) 15–35 cm, glabrous or slightly retrorsely hairy proximally, glandular-pubescent distally.</text>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, not leathery, glabrous abaxially, retrorsely hairy adaxially, sometimes only along midvein;</text>
      <biological_entity id="o33738" name="midvein" name_original="midvein" src="d0_s1" type="structure" />
      <relation from="o33737" id="r2565" modifier="sometimes only; only" name="along" negation="false" src="d0_s1" to="o33738" />
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, 32–65 (–80) × 1–6 (–9) mm, blade lanceolate to linear, base tapered, margins entire or obscurely serrate, apex obtuse to acute;</text>
      <biological_entity id="o33737" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely; adaxially" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o33739" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o33740" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear" />
      </biological_entity>
      <biological_entity id="o33741" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o33742" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o33743" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 3–7 pairs, sessile, 30–45 (–70) × 1–5 (–8) mm, blade lanceolate to linear, base tapered to slightly clasping, margins entire or obscurely serrate, apex acute.</text>
      <biological_entity constraint="cauline" id="o33744" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="70" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="45" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33745" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o33746" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o33747" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o33748" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, cylindric, (4–) 6–15 cm, axis glandular-pubescent, verticillasters 3–6 (–9), cymes (1 or) 2–5-flowered, 2 per node;</text>
      <biological_entity id="o33749" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33750" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o33751" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o33752" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-5-flowered" value_original="2-5-flowered" />
        <character constraint="per node" constraintid="o33753" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o33753" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate to linear, 7–20 (–45) × 1–4 (–8) mm;</text>
      <biological_entity constraint="proximal" id="o33754" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-pubescent.</text>
      <biological_entity id="o33755" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o33756" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes elliptic-lanceolate, 4–6 × 1–1.6 mm, glandular-pubescent;</text>
      <biological_entity id="o33757" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o33758" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla violet to blue or bluish lavender, with reddish purple nectar guides, weakly bilabiate, funnelform, 10–16 mm, glabrous or sparsely white-villous internally abaxially, tube 5–7 mm, throat gradually inflated, not constricted at orifice, 5–6 mm diam., rounded abaxially;</text>
      <biological_entity id="o33759" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o33760" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s8" to="blue or bluish lavender" />
        <character is_modifier="false" modifier="weakly" name="architecture" notes="" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="16" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; internally abaxially; abaxially" name="pubescence" src="d0_s8" value="white-villous" value_original="white-villous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o33761" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o33762" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33763" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o33764" is_modifier="false" modifier="not" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" notes="" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o33764" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o33760" id="r2566" name="with" negation="false" src="d0_s8" to="o33761" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs opposite, explanate, 0.7–0.9 mm, dehiscing completely, sutures smooth;</text>
      <biological_entity id="o33765" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o33766" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o33767" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="distance" src="d0_s9" to="0.9" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o33768" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 6–8 mm, included, 0.5–0.9 mm diam., tip straight to recurved, distal 5–7 mm densely pilose, hairs yellow, to 0.8 mm;</text>
      <biological_entity id="o33769" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33770" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33771" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o33772" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o33773" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 4–5 mm.</text>
      <biological_entity id="o33774" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o33775" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 7–10 × 3–5 mm.</text>
      <biological_entity id="o33776" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon goodrichii is known from the Lapoint-Tridell-Whiterocks vicinity in Duchesne and Uintah counties on clayey badlands associated with the Duchesne River Formation.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clayey slopes, shadscale and sagebrush shrublands, juniper-mountain mahogany woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey slopes" />
        <character name="habitat" value="shadscale" />
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="juniper-mountain mahogany woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>