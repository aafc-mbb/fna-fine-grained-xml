<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">450</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="mention_page">452</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="species">longiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Nat. Hist.</publication_title>
      <place_in_publication>1: 139. 1838</place_in_publication>
      <other_info_on_pub>(as longiflora)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species longiflorus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diplacus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">arachnoideus</taxon_name>
    <taxon_hierarchy>genus diplacus;species arachnoideus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">D.</taxon_name>
    <taxon_name authority="(J. C. Wendland) Nuttall" date="unknown" rank="species">glutinosus</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="variety">pubescens</taxon_name>
    <taxon_hierarchy>genus d.;species glutinosus;variety pubescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Curtis" date="unknown" rank="species">aurantiacus</taxon_name>
    <taxon_name authority="(Torrey) D. M. Thompson" date="unknown" rank="variety">pubescens</taxon_name>
    <taxon_hierarchy>genus mimulus;species aurantiacus;variety pubescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="(Nuttall) A. L. Grant" date="unknown" rank="species">longiflorus</taxon_name>
    <taxon_hierarchy>genus m.;species longiflorus</taxon_hierarchy>
  </taxon_identification>
  <number>44.</number>
  <other_name type="common_name">Southern bush monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs.</text>
      <biological_entity id="o13720" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 300–1000 (–2000) mm, glandular-puberulent and short-villous.</text>
      <biological_entity id="o13721" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1000" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="2000" to_unit="mm" />
        <character char_type="range_value" from="300" from_unit="mm" name="some_measurement" src="d0_s1" to="1000" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="short-villous" value_original="short-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually cauline, relatively even-sized;</text>
      <biological_entity id="o13722" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="size" notes="" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
      <biological_entity id="o13723" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent;</text>
      <biological_entity id="o13724" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic to lanceolate, elliptic-lanceolate, or elliptic-oblanceolate, 25–65 (–80) × 4–15 (–25) mm, margins entire or serrate, revolute, apex acute to obtuse, abaxial surfaces densely hairy, hairs branched, adaxial glabrescent.</text>
      <biological_entity id="o13725" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate elliptic-lanceolate or elliptic-oblanceolate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate elliptic-lanceolate or elliptic-oblanceolate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate elliptic-lanceolate or elliptic-oblanceolate" />
        <character char_type="range_value" from="65" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="65" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13726" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o13727" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13728" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o13729" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13730" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 5–16 mm in fruit.</text>
      <biological_entity id="o13731" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o13732" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13732" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, chasmogamous.</text>
      <biological_entity id="o13733" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o13734" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o13734" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces not inflated in fruit, 22–32 mm, glandular-puberulent and short glandular-villous to hirsute-villous, tube slightly dilated distally, lobes unequal, apex acute, ribs green, intercostal areas light green.</text>
      <biological_entity id="o13735" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o13736" is_modifier="false" modifier="not" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="32" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="short" value_original="short" />
        <character char_type="range_value" from="glandular-villous" name="pubescence" src="d0_s7" to="hirsute-villous" />
      </biological_entity>
      <biological_entity id="o13736" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o13737" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly; distally" name="shape" src="d0_s7" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o13738" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o13739" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13740" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o13741" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="light green" value_original="light green" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas light orange to pale yellow-orange, palate ridges orangish, tube-throat 34–45 mm, limb (25–) 28–40 mm diam., bilabiate, lobes oblong, apex of adaxial 2 each shallowly incised.</text>
      <biological_entity id="o13742" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="light orange" name="coloration" src="d0_s8" to="pale yellow-orange" />
      </biological_entity>
      <biological_entity constraint="palate" id="o13743" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="orangish" value_original="orangish" />
      </biological_entity>
      <biological_entity id="o13744" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="34" from_unit="mm" name="distance" src="d0_s8" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13745" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="diameter" src="d0_s8" to="28" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="28" from_unit="mm" name="diameter" src="d0_s8" to="40" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity id="o13746" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o13747" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13748" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o13747" id="r1071" name="consist_of" negation="false" src="d0_s8" to="o13748" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o13749" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles minutely glandular.</text>
      <biological_entity id="o13750" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas included, lobes equal.</text>
      <biological_entity id="o13751" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o13752" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 18–28 mm. 2n = 20.</text>
      <biological_entity id="o13753" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s12" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13754" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus longiflorus occurs in southwestern California and northeastern Baja California.</discussion>
  <discussion>Plants and populations intermediate between Diplacus longiflorus and D. puniceus are found where their ranges meet in Los Angeles, Orange, Riverside, and San Diego counties. The intermediate morphology and geography indicate that these are hybrids (as has been hypothesized by, for example, M. A. Streisfeld and J. R. Kohn 2005; D. M. Thompson 2005; M. C. Tulig and G. L. Nesom 2012), which have been identified as D. ×australis (McMinn ex Munz) Tulig. Streisfeld and Kohn found that in San Diego County, D. longiflorus and D. puniceus are discrete in morphology and separated in geography, with a narrow zone of hybrids and putative introgressants between.</discussion>
  <discussion>Plants identified as Diplacus ×lompocensis McMinn (as species) occur where the geographic ranges of D. aurantiacus and D. longiflorus meet in Santa Barbara County and southern San Luis Obispo County; these plants have floral features intermediate between these two species. Stable populations of the putative hybrid are found throughout this region, although at either end of its distribution, the populations may more closely resemble the nearer parent. Considering that both D. aurantiacus and D. longiflorus are morphologically consistent across broad regions, D. ×lompocensis is perhaps best interpreted as a zone of introgression.</discussion>
  <discussion>Diplacus ×australis and D. ×lompocensis are similar to D. longiflorus as well as to each other in most features; they are easily separated only by geographic range. Diplacus longiflorus is distinct from both in its larger corolla features and, frequently, calyx indument.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides and slopes, talus, chaparral, live oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak woodlands" modifier="live" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(50–)100–1300(–1800) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>