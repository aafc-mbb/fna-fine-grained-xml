<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="section">Saccanthera</taxon_name>
    <taxon_name authority="Rydberg" date="1913" rank="species">leonardii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>40: 483. 1913</place_in_publication>
      <other_info_on_pub>(as Pentstemon leonardi)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section saccanthera;species leonardii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>212.</number>
  <other_name type="common_name">Leonard’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs.</text>
      <biological_entity id="o4964" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending or erect, (5–) 10–30 (–47) cm, puberulent or retrorsely hairy, hairs pointed, not glaucous.</text>
      <biological_entity id="o4966" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending or erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="47" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4967" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="pointed" value_original="pointed" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, opposite, glabrous, sometimes proximals puberulent, hairs pointed, not glaucous;</text>
      <biological_entity id="o4968" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4969" name="proximal" name_original="proximals" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o4970" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pointed" value_original="pointed" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–7 pairs, short-petiolate or sessile, 11–48 (–60) × 2–12 mm, blade oblanceolate to spatulate or obovate, rarely linear, base tapered, margins entire, apex rounded to obtuse or acute.</text>
      <biological_entity constraint="cauline" id="o4971" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="48" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s3" to="48" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4972" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="spatulate or obovate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o4973" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o4974" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4975" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous or interrupted, cylindric to ± secund, (1–) 2–15 (–25) cm, axis glabrous or retrorsely hairy, verticillasters 2–8, cymes 1–4-flowered, 2 per node;</text>
      <biological_entity id="o4976" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4977" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4978" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
      <biological_entity id="o4979" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-4-flowered" value_original="1-4-flowered" />
        <character constraint="per node" constraintid="o4980" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4980" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate, 10–35 × 1–4 mm;</text>
      <biological_entity constraint="proximal" id="o4981" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, peduncles glabrous or sparsely retrorsely hairy, pedicels glabrous or papillate distally.</text>
      <biological_entity id="o4982" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
      </biological_entity>
      <biological_entity id="o4983" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
      </biological_entity>
      <biological_entity id="o4984" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely retrorsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4985" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate to lanceolate, 3–5.5 × 0.9–2.2 mm, margins entire or erose, scarious, apex acuminate to caudate, glabrous;</text>
      <biological_entity id="o4986" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o4987" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s7" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4988" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o4989" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s7" to="caudate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla lavender to blue or violet, with or without faint lavender nectar guides, funnelform to ventricose, 14–26 mm, glabrous externally, glabrous internally, tube 5–8 mm, throat gradually inflated, 6–8 (–10) mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o4990" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4991" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="blue or violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="26" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="externally; externally" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o4992" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="faint" value_original="faint" />
        <character is_modifier="true" name="coloration_or_odor" src="d0_s8" value="lavender" value_original="lavender" />
      </biological_entity>
      <biological_entity id="o4993" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" modifier="externally; externally" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4994" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s8" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o4991" id="r440" name="with or without" negation="false" src="d0_s8" to="o4992" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included or longer pair reaching orifice, filaments glabrous, pollen-sacs parallel, 0.9–1.9 mm, distal 1/2–2/3 indehiscent, sides glabrous or puberulent, hairs white, to 0.1 mm, sutures denticulate, teeth to 0.2 mm;</text>
      <biological_entity id="o4995" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4996" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o4997" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o4998" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4999" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s9" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s9" to="2/3" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o5000" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5001" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5002" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o5003" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o4996" id="r441" name="reaching" negation="false" src="d0_s9" to="o4997" />
    </statement>
    <statement id="d0_s10">
      <text>staminode 10–15 mm, included or reaching orifice, 0.6–0.9 mm diam., glabrous;</text>
      <biological_entity id="o5004" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5005" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s10" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5006" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <relation from="o5005" id="r442" name="reaching" negation="false" src="d0_s10" to="o5006" />
    </statement>
    <statement id="d0_s11">
      <text>style 12–18 mm.</text>
      <biological_entity id="o5007" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5008" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–8 × 2.5–4.5 mm.</text>
      <biological_entity id="o5009" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Idaho, Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon leonardii is common in the Wasatch Mountains, Utah Plateaus, and mountain ranges in the eastern Basin and Range Province. Elements allied with it have been interpreted variously as one species comprising three varieties, two species (one with two varieties), or three species; the broadest concept is followed here.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 20–26 mm; pollen sacs (1.2–)1.4–1.9 mm; e Nevada, w Utah.</description>
      <determination>212c. Penstemon leonardii var. patricus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 14–22 mm; pollen sacs 0.9–1.6 mm; nw Arizona, se Idaho, se Nevada, Utah.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla limbs blue to lavender; thyrses continuous; se Idaho, n Utah.</description>
      <determination>212a. Penstemon leonardii var. leonardii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla limbs lavender to violet; thyrses continuous or interrupted; nw Arizona, se Nevada, sw Utah.</description>
      <determination>212b. Penstemon leonardii var. higginsii</determination>
    </key_statement>
  </key>
</bio:treatment>