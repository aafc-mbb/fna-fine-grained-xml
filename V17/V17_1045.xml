<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">596</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="mention_page">597</other_info_on_meta>
    <other_info_on_meta type="mention_page">609</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="mention_page">651</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Greenman" date="1909" rank="species">chrysantha</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>48: 146. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species chrysantha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="Pennell" date="unknown" rank="species">ownbeyana</taxon_name>
    <taxon_hierarchy>genus castilleja;species ownbeyana</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <other_name type="common_name">Yellowish or common wallowa paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, (0.5–) 1–2 (–5) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o10093" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o10094" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o10093" id="r806" name="with" negation="false" src="d0_s2" to="o10094" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, erect or ascending, often decumbent at base, unbranched, sometimes branched, hairs spreading, long, soft, mixed with dense, shorter stipitate-glandular ones.</text>
      <biological_entity id="o10095" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character constraint="at base" constraintid="o10096" is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o10096" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o10097" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character constraint="with shorter ones" constraintid="o10098" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o10098" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="true" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green or purple-tinged, sometimes deep purple, broadly lanceolate, sometimes linear-lanceolate or narrowly oblong, 1.5–4.8 cm, not fleshy, margins plane, flat or involute, 0–3-lobed, apex acute;</text>
      <biological_entity id="o10099" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" modifier="sometimes" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4.8" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o10100" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0-3-lobed" value_original="0-3-lobed" />
      </biological_entity>
      <biological_entity id="o10101" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending or spreading, linear to narrowly linear or narrowly lanceolate, short, apex acuminate.</text>
      <biological_entity id="o10102" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o10103" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 3–17 × 1–3 cm;</text>
      <biological_entity id="o10104" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="17" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts greenish or pale yellow-green throughout, or proximally greenish or pale yellow-green, distally pale-yellow to whitish, sometimes pink-purple, or pale, dull purplish, sometimes aging pink or yellow, often infused with light purple, rarely pink, ovate to broadly acute, (0–) 3-lobed;</text>
      <biological_entity id="o10105" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s7" value="pale yellow-green" value_original="pale yellow-green" />
        <character char_type="range_value" from="pale yellow-green distally pale-yellow" name="coloration" src="d0_s7" to="whitish" />
        <character char_type="range_value" from="pale yellow-green distally pale-yellow" name="coloration" src="d0_s7" to="whitish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="pink-purple" value_original="pink-purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink-purple" value_original="pink-purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale" value_original="pale" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often; rarely" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="broadly acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="(0-)3-lobed" value_original="(0-)3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending, linear-lanceolate, oblong, or triangular, short, arising below mid length, apex acute to obtuse.</text>
      <biological_entity id="o10106" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o10107" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="true" modifier="below" name="position" src="d0_s8" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o10108" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="length" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces proximally green, pale with green veins, purple-tinged green, or purple, distally pale-yellow, white, or purplish, 12–20 mm;</text>
      <biological_entity id="o10109" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character constraint="with veins" constraintid="o10110" is_modifier="false" name="coloration" src="d0_s9" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple-tinged green" value_original="purple-tinged green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple-tinged green" value_original="purple-tinged green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple-tinged green" value_original="purple-tinged green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple-tinged green" value_original="purple-tinged green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple-tinged green" value_original="purple-tinged green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10110" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 8–12 mm, 50% of calyx length, deeper than laterals, lateral 0.5–3 mm, 5–15% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o10111" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character constraint="than laterals" constraintid="o10112" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="50%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10112" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes short-triangular, apex obtuse, rounded, or truncate.</text>
      <biological_entity id="o10113" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="short-triangular" value_original="short-triangular" />
      </biological_entity>
      <biological_entity id="o10114" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight, (16–) 20–25 mm;</text>
      <biological_entity id="o10115" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="16" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s12" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 15–18 mm;</text>
      <biological_entity id="o10116" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s13" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak slightly exserted, adaxially green to yellow, 5.5–8.5 mm;</text>
      <biological_entity id="o10117" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="adaxially green" name="coloration" src="d0_s14" to="yellow" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s14" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip white, green-and-white, pink, or purple, ± prominent, appressed (proximally scarcely or not pouched), 3–5 mm, 67% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o10118" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="green-and-white" value_original="green-and-white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10119" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o10118" id="r807" modifier="67%" name="as long as" negation="false" src="d0_s15" to="o10119" />
    </statement>
    <statement id="d0_s16">
      <text>teeth erect, white or pink with some purple or red, 1.5–2.5 mm.</text>
      <biological_entity id="o10120" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink with some purple or pink with red" />
        <character name="coloration" src="d0_s16" value="," value_original="," />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja chrysantha is endemic to the mountains of northeastern Oregon, and its patterns of variation need further study. Most plants in the Blue Mountains are taller, more erect, and tolerate lower elevation habitats than populations around the type locality in the Wallowa Mountains. Plants with purplish inflorescences and longer hairs were described as C. ownbeyana and tend to favor drier talus and ridges than plants like the type, found in flat, mesic, montane to subalpine meadows. Hybrids between C. chrysantha and C. fraterna occur in Wallowa County. In the Wallowa Mountains, a recurrent and variable hybrid form between C. chrysantha and C. rhexiifolia was described as C. wallowensis Pennell.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Flat, mesic meadows, dry talus and ridges, montane to alpine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="flat" />
        <character name="habitat" value="mesic meadows" />
        <character name="habitat" value="dry talus" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>