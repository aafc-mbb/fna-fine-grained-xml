<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">608</other_info_on_meta>
    <other_info_on_meta type="mention_page">587</other_info_on_meta>
    <other_info_on_meta type="mention_page">605</other_info_on_meta>
    <other_info_on_meta type="mention_page">664</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="species">flava</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">flava</taxon_name>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species flava;variety flava</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>33a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 0–5-lobed.</text>
      <biological_entity id="o9365" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="0-5-lobed" value_original="0-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bracts distally pale to bright-yellow, sometimes to light orange;</text>
      <biological_entity id="o9366" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character char_type="range_value" from="distally pale" name="coloration" src="d0_s1" to="bright-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="light orange" value_original="light orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lobes long.</text>
      <biological_entity id="o9367" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Calyces proximally green, sometimes purple, distally yellow to light orange, 13–28 mm;</text>
      <biological_entity id="o9368" name="calyx" name_original="calyces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="purple" value_original="purple" />
        <character char_type="range_value" from="distally yellow" name="coloration" src="d0_s3" to="light orange" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s3" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>abaxial clefts 8–16 mm, adaxial 4–8 (–15.5) mm, abaxial ca. 60% of calyx length, adaxial 30–50% of calyx length, slightly to much more deeply abaxially in front than adaxially, lateral 1.5–4 mm, 5–15% of calyx length;</text>
      <biological_entity constraint="abaxial" id="o9369" name="cleft" name_original="clefts" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9370" name="cleft" name_original="clefts" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="15.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9371" name="cleft" name_original="clefts" src="d0_s4" type="structure" />
      <biological_entity id="o9372" name="calyx" name_original="calyx" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o9373" name="cleft" name_original="clefts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="30-50%; slightly to much; much" name="length" src="d0_s4" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9374" name="front" name_original="front" src="d0_s4" type="structure" />
      <relation from="o9371" id="r751" modifier="60%" name="part_of" negation="false" src="d0_s4" to="o9372" />
      <relation from="o9373" id="r752" modifier="30-50%; slightly to much; much; deeply abaxially" name="in" negation="false" src="d0_s4" to="o9374" />
    </statement>
    <statement id="d0_s5">
      <text>lobes lanceolate to narrowly triangular.</text>
      <biological_entity id="o9375" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="narrowly triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 13–30 mm;</text>
      <biological_entity id="o9376" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>beak 5–12 mm;</text>
      <biological_entity id="o9377" name="beak" name_original="beak" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>abaxial lip 1–2 mm.</text>
      <biological_entity constraint="abaxial" id="o9378" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety flava often differs from var. rustica in the color of the bracts, as well as in the structure of the calyx and corolla. They also have different ranges, with var. rustica replacing var. flava in southwestern Idaho and northeastern Oregon. The latter is found in the northern Great Basin and central Rocky Mountains. Variety flava hybridizes with Castilleja viscidula in Elko County, Nevada. Hybrids between C. angustifolia var. angustifolia and var. flava are known from the western slopes of the Big Horn Mountains, Wyoming. M. Ownbey (1959) reported from the same region a hybrid swarm between var. flava and C. linariifolia.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane to subalpine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane to subalpine." />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–3000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Nev., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>