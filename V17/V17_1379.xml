<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="mention_page">439</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(Greenman) G. L. Nesom" date="2012" rank="species">clivicola</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 28. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species clivicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Greenman" date="1899" rank="species">clivicola</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>7: 119. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species clivicola</taxon_hierarchy>
  </taxon_identification>
  <number>16.</number>
  <other_name type="common_name">Slope monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o28938" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (10–) 20–180 mm, glandular-puberulent to short glandular-villous.</text>
      <biological_entity id="o28939" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s1" to="180" to_unit="mm" />
        <character char_type="range_value" from="glandular-puberulent" name="pubescence" src="d0_s1" to="short glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually cauline, relatively even-sized;</text>
      <biological_entity id="o28940" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="size" notes="" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
      <biological_entity id="o28941" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent, base sometimes petiolelike;</text>
      <biological_entity id="o28942" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o28943" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="petiole-like" value_original="petiolelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly elliptic, sometimes broadly elliptic to obovate or oblanceolate, (2.5–) 6–20 (–26) × (1–) 2–10 (–14) mm, margins crenate to serrulate or entire, plane, apex rounded or acute, surfaces glandular-puberulent.</text>
      <biological_entity id="o28944" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="sometimes broadly elliptic" name="shape" src="d0_s4" to="obovate or oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s4" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="26" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28945" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="crenate to serrulate" value_original="crenate to serrulate" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o28946" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28947" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 2–7 (–10) mm in fruit.</text>
      <biological_entity id="o28948" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o28949" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28949" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, or 1 or 2 per node on 1 plant, chasmogamous.</text>
      <biological_entity id="o28950" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o28951" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28951" name="node" name_original="node" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28952" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o28953" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o28952" id="r2203" name="on" negation="false" src="d0_s6" to="o28953" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces symmetrically attached to pedicels, not inflated in fruit, (5–) 7–8 mm, glandular-puberulent, lobes subequal, apex acute, ribs green, intercostal areas whitish.</text>
      <biological_entity id="o28954" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicels" constraintid="o28955" is_modifier="false" modifier="symmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o28956" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o28955" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o28956" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o28957" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o28958" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28959" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o28960" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas rose-pink to purplish, limb often pale, especially abaxial lip, abaxial lip often purple-dotted near base, markings often coalescing and forming broken lines radiating toward each lobe, tube yellow, palate ridges yellow with magenta speckling, confluent and extending onto abaxial lip base, tube-throat (8–) 11–12 mm, limb 7–12 mm diam., bilabiate.</text>
      <biological_entity id="o28961" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="rose-pink" name="coloration" src="d0_s8" to="purplish" />
      </biological_entity>
      <biological_entity id="o28962" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o28963" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="especially" name="position" src="d0_s8" value="abaxial" value_original="abaxial" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28964" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character constraint="near base" constraintid="o28965" is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="purple-dotted" value_original="purple-dotted" />
      </biological_entity>
      <biological_entity id="o28965" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o28966" name="marking" name_original="markings" src="d0_s8" type="structure">
        <character constraint="toward lobe" constraintid="o28968" is_modifier="false" name="arrangement" src="d0_s8" value="radiating" value_original="radiating" />
      </biological_entity>
      <biological_entity id="o28967" name="line" name_original="lines" src="d0_s8" type="structure">
        <character is_modifier="true" name="condition_or_fragility" src="d0_s8" value="broken" value_original="broken" />
      </biological_entity>
      <biological_entity id="o28968" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <biological_entity id="o28969" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="palate" id="o28970" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow with magenta" value_original="yellow with magenta" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity constraint="lip" id="o28971" name="base" name_original="base" src="d0_s8" type="structure" constraint_original="abaxial lip" />
      <biological_entity id="o28972" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_distance" src="d0_s8" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="distance" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28973" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <relation from="o28966" id="r2204" name="coalescing" negation="false" src="d0_s8" to="o28967" />
      <relation from="o28970" id="r2205" name="extending onto" negation="false" src="d0_s8" to="o28971" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, ciliate.</text>
      <biological_entity id="o28974" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles glandular-puberulent distally.</text>
      <biological_entity id="o28975" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas exserted, lobes subequal.</text>
      <biological_entity id="o28976" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o28977" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 8–13 mm. 2n = 16.</text>
      <biological_entity id="o28978" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28979" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus clivicola is known from northern Idaho and immediately adjacent Montana and Oregon. It is similar to typical D. nanus in its strongly bilabiate corollas; it differs in its slightly toothed leaf blade margins, relatively long pedicels, and calyces with cuneate bases.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bluffs, disturbed slopes, well-developed loam soils, vegetation openings.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="disturbed slopes" />
        <character name="habitat" value="loam soils" modifier="well-developed" />
        <character name="habitat" value="vegetation openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1200(–2000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>