<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">538</other_info_on_meta>
    <other_info_on_meta type="mention_page">550</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">AGALINIS</taxon_name>
    <taxon_name authority="Pennell" date="1913" rank="species">oligophylla</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>40: 432. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus agalinis;species oligophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gerardia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">aphylla</taxon_name>
    <taxon_name authority="Bentham" date="1836" rank="variety">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Compan. Bot. Mag.</publication_title>
      <place_in_publication>1: 174. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gerardia;species aphylla;variety grandiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">microphylla</taxon_name>
    <taxon_hierarchy>genus g.;species microphylla</taxon_hierarchy>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Ridgestem false foxglove</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or branched, 30–90 (–110) cm;</text>
      <biological_entity id="o40461" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches spreading-ascending, quadrangular, with siliceous ridges on angles, scabridulous.</text>
      <biological_entity id="o40462" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="quadrangular" value_original="quadrangular" />
        <character is_modifier="false" name="relief" notes="" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o40463" name="ridge" name_original="ridges" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s1" value="siliceous" value_original="siliceous" />
      </biological_entity>
      <biological_entity id="o40464" name="angle" name_original="angles" src="d0_s1" type="structure" />
      <relation from="o40462" id="r3116" name="with" negation="false" src="d0_s1" to="o40463" />
      <relation from="o40463" id="r3117" name="on" negation="false" src="d0_s1" to="o40464" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending;</text>
      <biological_entity id="o40465" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade subulate, elliptic, or filiform, (1–) 4–13 x 0.3–1.1 mm, margins entire, heavily siliceous, scabridulous, adaxial surface siliceous, scabridulous;</text>
      <biological_entity id="o40466" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s3" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40467" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="heavily" name="pubescence_or_texture" src="d0_s3" value="siliceous" value_original="siliceous" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles absent.</text>
      <biological_entity constraint="adaxial" id="o40468" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="siliceous" value_original="siliceous" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemiform-paniculate, with short multinoded floriferous branches, some with pseudoterminal flowers, flowers 1 or 2 per node;</text>
      <biological_entity id="o40469" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="racemiform-paniculate" value_original="racemiform-paniculate" />
      </biological_entity>
      <biological_entity id="o40470" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="floriferous" value_original="floriferous" />
      </biological_entity>
      <biological_entity constraint="pseudoterminal" id="o40471" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o40472" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o40473" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o40469" id="r3118" name="with" negation="false" src="d0_s5" to="o40470" />
      <relation from="o40469" id="r3119" name="with" negation="false" src="d0_s5" to="o40471" />
    </statement>
    <statement id="d0_s6">
      <text>bracts shorter than or sometimes equal to pedicels.</text>
      <biological_entity id="o40474" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="than or sometimes equal to pedicels" constraintid="o40475" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o40475" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="sometimes" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ascending, 4–16 mm, moreorless scabridulous.</text>
      <biological_entity id="o40476" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx campanulate to funnelform, tube 2.5–3.2 mm, glabrous, lobes subulate to triangular-subulate, 0.1–1 mm;</text>
      <biological_entity id="o40477" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o40478" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character constraint="to funnelform" constraintid="o40479" is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o40479" name="funnelform" name_original="funnelform" src="d0_s8" type="structure" />
      <biological_entity id="o40480" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o40481" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s8" to="triangular-subulate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pink to dark-pink, with 2 yellow lines and red spots in abaxial throat, 15–25 mm, throat pilose externally and villous within across bases and sinus of adaxial lobes, lobes: abaxial spreading, adaxial reflexed-spreading, 4–7 (–9) mm, glabrous externally;</text>
      <biological_entity id="o40482" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="dark-pink" />
        <character constraint="in abaxial throat" constraintid="o40484" is_modifier="false" name="coloration" notes="" src="d0_s9" value="red spots" value_original="red spots" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40483" name="line" name_original="lines" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o40484" name="throat" name_original="throat" src="d0_s9" type="structure" />
      <biological_entity id="o40485" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o40486, o40487" is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o40486" name="base" name_original="bases" src="d0_s9" type="structure" />
      <biological_entity id="o40487" name="sinus" name_original="sinus" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o40488" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o40489" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o40490" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o40491" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed-spreading" value_original="reflexed-spreading" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o40482" id="r3120" name="with" negation="false" src="d0_s9" to="o40483" />
      <relation from="o40486" id="r3121" name="part_of" negation="false" src="d0_s9" to="o40488" />
      <relation from="o40487" id="r3122" name="part_of" negation="false" src="d0_s9" to="o40488" />
    </statement>
    <statement id="d0_s10">
      <text>proximal anthers parallel to filaments, distal oblique or perpendicular to filaments, pollen-sacs 1.8–2.6 mm;</text>
      <biological_entity id="o40492" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="dark-pink" />
        <character constraint="in abaxial throat" constraintid="o40494" is_modifier="false" name="coloration" notes="" src="d0_s10" value="red spots" value_original="red spots" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40493" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o40494" name="throat" name_original="throat" src="d0_s10" type="structure" />
      <biological_entity id="o40495" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o40496, o40497" is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o40496" name="base" name_original="bases" src="d0_s10" type="structure" />
      <biological_entity id="o40497" name="sinus" name_original="sinus" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o40498" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o40499" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o40500" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="to filaments" constraintid="o40501" is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s10" value="distal" value_original="distal" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="oblique" value_original="oblique" />
        <character constraint="to filaments" constraintid="o40502" is_modifier="false" name="orientation" src="d0_s10" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o40501" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o40502" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o40503" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="distance" src="d0_s10" to="2.6" to_unit="mm" />
      </biological_entity>
      <relation from="o40492" id="r3123" name="with" negation="false" src="d0_s10" to="o40493" />
      <relation from="o40496" id="r3124" name="part_of" negation="false" src="d0_s10" to="o40498" />
      <relation from="o40497" id="r3125" name="part_of" negation="false" src="d0_s10" to="o40498" />
    </statement>
    <statement id="d0_s11">
      <text>style exserted, 6–11 mm.</text>
      <biological_entity id="o40504" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="dark-pink" />
        <character constraint="in abaxial throat" constraintid="o40506" is_modifier="false" name="coloration" notes="" src="d0_s11" value="red spots" value_original="red spots" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40505" name="line" name_original="lines" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o40506" name="throat" name_original="throat" src="d0_s11" type="structure" />
      <biological_entity id="o40507" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o40508, o40509" is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o40508" name="base" name_original="bases" src="d0_s11" type="structure" />
      <biological_entity id="o40509" name="sinus" name_original="sinus" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial" id="o40510" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o40511" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o40512" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
      <relation from="o40504" id="r3126" name="with" negation="false" src="d0_s11" to="o40505" />
      <relation from="o40508" id="r3127" name="part_of" negation="false" src="d0_s11" to="o40510" />
      <relation from="o40509" id="r3128" name="part_of" negation="false" src="d0_s11" to="o40510" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules globular, 4–6 mm.</text>
      <biological_entity id="o40513" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globular" value_original="globular" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds tan to yellow, 0.5–0.9 mm. 2n = 26.</text>
      <biological_entity id="o40514" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s13" to="yellow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o40515" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants of Agalinis oligophylla are rare in the easternmost portions of the range; they are abundant farther west into Texas. Agalinis oligophylla is often the last species of Agalinis to begin flowering, usually in late September, sometimes not until mid October and often remains in flower well into November. Unbranched plants of A. oligophylla can be confused with A. aphylla, with which it often shares the habitat. Agalinis oligophylla can be distinguished from A. aphylla by the longer leaves, more diffusely branched inflorescence, glabrous style, and the much later flowering period of A. oligophylla. Also, on the older buds of A. oligophylla the corolla is globular, but in A. aphylla the corolla on older buds is obovoid. Corollas of A. oligophylla are asymmetric; the adaxial side of the corolla throat is conspicuously shorter than the upcurved abaxial side of the corolla. The buds, pedicels, and capsules of A. oligophylla turn dark brown or blackish, and corollas turn dark purplish or pinkish brown when dried. These dark structures contrast strikingly with the pale green to gray (or reddish) branches and leaves.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Sep–mid Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Nov" from="late Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry longleaf pine savannas, edges of pine plantations, dry roadsides, chalky outcrops, seepage slopes of clay roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry longleaf" />
        <character name="habitat" value="moist to dry longleaf pine savannas" />
        <character name="habitat" value="edges" constraint="of pine plantations , dry roadsides , chalky outcrops" />
        <character name="habitat" value="pine plantations" />
        <character name="habitat" value="dry roadsides" />
        <character name="habitat" value="chalky outcrops" />
        <character name="habitat" value="slopes" modifier="seepage" constraint="of clay roadsides" />
        <character name="habitat" value="clay roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., La., Miss., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>