<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">SCOPARIA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">dulcis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 116. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus scoparia;species dulcis</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Licorice-weed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials.</text>
      <biological_entity id="o21651" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, usually much-branched distally, (17–) 30–100 (–150) cm, glabrous or puberulent.</text>
      <biological_entity id="o21653" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="17" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade oblanceolate to narrowly oblanceolate or rhombic, 8–53 × 3–25 mm, base tapered to cuneate, margins crenate to dentate in distal 1/2.</text>
      <biological_entity id="o21654" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21655" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="narrowly oblanceolate or rhombic" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="53" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21656" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity id="o21657" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in distal 1/2" constraintid="o21658" from="crenate" name="shape" src="d0_s2" to="dentate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21658" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: flowers 1 or 2 (or 3) per axil;</text>
      <biological_entity id="o21659" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o21660" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s3" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21661" name="axil" name_original="axil" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracts narrowly oblanceolate to narrowly elliptic, 4–35 mm.</text>
      <biological_entity id="o21662" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o21663" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s4" to="narrowly elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 2–10 mm, glabrous.</text>
      <biological_entity id="o21664" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes 4, ovate to elliptic-ovate, 1.2–1.5 × 0.6–1 mm, margins ciliolate;</text>
      <biological_entity id="o21665" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o21666" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="elliptic-ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s6" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21667" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla white, sometimes tinged pink or lavender, 2–2.5 × 3–4 mm.</text>
      <biological_entity id="o21668" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o21669" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="tinged pink" value_original="tinged pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules ovoid to subglobose, (1.6–) 2–2.5 (–4) × 1.4–2 mm.</text>
      <biological_entity id="o21670" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s8" to="subglobose" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="atypical_length" src="d0_s8" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds: 0.1–0.3 mm. 2n = 40 (India).</text>
      <biological_entity id="o21671" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21672" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Scoparia dulcis is a pantropical weed. Noting that it was a widespread weed in lowland tropical America, F. W. Pennell (1935) believed that it was adventive in the United States. Most United States floras consider it to be native in the flora area, and it is treated that way here.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov(–Jan).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jan" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes, wet hammocks, flatwoods, sandy woods, disturbed sites.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="wet hammocks" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="sandy woods" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., S.C., Tex.; Mexico; West Indies; Central America; South America; introduced in Asia, Africa, Indian Ocean Islands (Madagascar), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>