<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">550</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="mention_page">551</other_info_on_meta>
    <other_info_on_meta type="mention_page">553</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">AGALINIS</taxon_name>
    <taxon_name authority="(Elliott) Rafinesque" date="1837" rank="species">plukenetii</taxon_name>
    <place_of_publication>
      <publication_title>New Fl.</publication_title>
      <place_in_publication>2: 63. 1837</place_in_publication>
      <other_info_on_pub>(as plukeneti)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus agalinis;species plukenetii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gerardia</taxon_name>
    <taxon_name authority="Elliott" date="1822" rank="species">plukenetii</taxon_name>
    <place_of_publication>
      <publication_title>Sketch Bot. S. Carolina</publication_title>
      <place_in_publication>2: 114. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gerardia;species plukenetii</taxon_hierarchy>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Plukenet’s false foxglove</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems often leaning, simple or branched, 30–100 cm, bushy;</text>
      <biological_entity id="o9312" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="leaning" value_original="leaning" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushy" value_original="bushy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches spreading-ascending, subterete proximally to quadrangular-ridged distally, glabrous or sparsely scabridulous distally.</text>
      <biological_entity id="o9313" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s1" value="subterete" value_original="subterete" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s1" value="quadrangular-ridged" value_original="quadrangular-ridged" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves widely spreading to slightly ascending;</text>
      <biological_entity id="o9314" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="widely spreading" name="orientation" src="d0_s2" to="slightly ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade filiform, 18–45 x 0.2–0.8 mm, not fleshy, margins entire, adaxial surface scabridulous;</text>
      <biological_entity id="o9315" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s3" to="45" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s3" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o9316" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles absent.</text>
      <biological_entity constraint="adaxial" id="o9317" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemiform, flowers 1 per node, some flowers pseudoterminal;</text>
      <biological_entity id="o9318" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <biological_entity id="o9319" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="per node" constraintid="o9320" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9320" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o9321" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="pseudoterminal" value_original="pseudoterminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts longer than pedicels.</text>
      <biological_entity id="o9322" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="than pedicels" constraintid="o9323" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o9323" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels spreading-ascending, 3–8 mm, glabrous.</text>
      <biological_entity id="o9324" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading-ascending" value_original="spreading-ascending" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx hemispheric, tube 3–5 mm, glabrous, lobes deltate-subulate, 0.2–1 mm;</text>
      <biological_entity id="o9325" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9326" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o9327" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9328" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate-subulate" value_original="deltate-subulate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla dark-pink to rose-pink, with 2 yellow lines and dark-pink spots in abaxial throat, 18–30 mm, throat pilose externally and villous within across bases and sinus of adaxial lobes, lobes: abaxial spreading, adaxial reflexed-spreading, 6–10 mm, abaxial pilose externally, adaxial glabrous externally;</text>
      <biological_entity id="o9329" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="dark-pink" name="coloration" src="d0_s9" to="rose-pink" />
        <character constraint="in abaxial throat" constraintid="o9331" is_modifier="false" name="coloration" notes="" src="d0_s9" value="dark-pink spots" value_original="dark-pink spots" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9330" name="line" name_original="lines" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9331" name="throat" name_original="throat" src="d0_s9" type="structure" />
      <biological_entity id="o9332" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o9333, o9334" is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o9333" name="base" name_original="bases" src="d0_s9" type="structure" />
      <biological_entity id="o9334" name="sinus" name_original="sinus" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o9335" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o9336" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o9337" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9338" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed-spreading" value_original="reflexed-spreading" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9339" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9340" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o9329" id="r742" name="with" negation="false" src="d0_s9" to="o9330" />
      <relation from="o9333" id="r743" name="part_of" negation="false" src="d0_s9" to="o9335" />
      <relation from="o9334" id="r744" name="part_of" negation="false" src="d0_s9" to="o9335" />
    </statement>
    <statement id="d0_s10">
      <text>proximal anthers parallel to filaments, distal perpendicular to filaments, pollen-sacs 2.2–4 mm;</text>
      <biological_entity id="o9341" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark-pink" name="coloration" src="d0_s10" to="rose-pink" />
        <character constraint="in abaxial throat" constraintid="o9343" is_modifier="false" name="coloration" notes="" src="d0_s10" value="dark-pink spots" value_original="dark-pink spots" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9342" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9343" name="throat" name_original="throat" src="d0_s10" type="structure" />
      <biological_entity id="o9344" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o9345, o9346" is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o9345" name="base" name_original="bases" src="d0_s10" type="structure" />
      <biological_entity id="o9346" name="sinus" name_original="sinus" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o9347" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o9348" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o9349" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="to filaments" constraintid="o9350" is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s10" value="distal" value_original="distal" />
        <character constraint="to filaments" constraintid="o9351" is_modifier="false" name="orientation" src="d0_s10" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o9350" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o9351" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o9352" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="distance" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o9341" id="r745" name="with" negation="false" src="d0_s10" to="o9342" />
      <relation from="o9345" id="r746" name="part_of" negation="false" src="d0_s10" to="o9347" />
      <relation from="o9346" id="r747" name="part_of" negation="false" src="d0_s10" to="o9347" />
    </statement>
    <statement id="d0_s11">
      <text>style exserted, 11–17 mm.</text>
      <biological_entity id="o9353" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark-pink" name="coloration" src="d0_s11" to="rose-pink" />
        <character constraint="in abaxial throat" constraintid="o9355" is_modifier="false" name="coloration" notes="" src="d0_s11" value="dark-pink spots" value_original="dark-pink spots" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9354" name="line" name_original="lines" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9355" name="throat" name_original="throat" src="d0_s11" type="structure" />
      <biological_entity id="o9356" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o9357, o9358" is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o9357" name="base" name_original="bases" src="d0_s11" type="structure" />
      <biological_entity id="o9358" name="sinus" name_original="sinus" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial" id="o9359" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o9360" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o9361" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="17" to_unit="mm" />
      </biological_entity>
      <relation from="o9353" id="r748" name="with" negation="false" src="d0_s11" to="o9354" />
      <relation from="o9357" id="r749" name="part_of" negation="false" src="d0_s11" to="o9359" />
      <relation from="o9358" id="r750" name="part_of" negation="false" src="d0_s11" to="o9359" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules globular, 4–5 mm.</text>
      <biological_entity id="o9362" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globular" value_original="globular" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds dark-brown to black, 0.5–0.8 mm. 2n = 28.</text>
      <biological_entity id="o9363" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s13" to="black" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9364" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agalinis plukenetii can be bushy, relatively large, and showy. Agalinis plukenetii is a common component of dry to xeric roadsides in the southern portions of its range. It readily colonizes open, dry ground with very little vegetation. It was reported in South Carolina (F. W. Pennell 1935) based on a specimen that has little label data; its occurrence there is doubtful.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Sep–early Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Nov" from="late Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to xeric, sandy, gravelly or clay roadsides, pine-oak forests, margins of savannas, disturbed ground.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="xeric" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="clay roadsides" />
        <character name="habitat" value="pine-oak forests" />
        <character name="habitat" value="dry to xeric" constraint="of savannas , disturbed ground" />
        <character name="habitat" value="sandy" constraint="of savannas , disturbed ground" />
        <character name="habitat" value="gravelly" constraint="of savannas , disturbed ground" />
        <character name="habitat" value="clay roadsides" constraint="of savannas , disturbed ground" />
        <character name="habitat" value="pine-oak forests" constraint="of savannas , disturbed ground" />
        <character name="habitat" value="margins" constraint="of savannas , disturbed ground" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="disturbed ground" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>