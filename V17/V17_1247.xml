<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Elizabeth H. Zacharias</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">684</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">666</other_info_on_meta>
    <other_info_on_meta type="mention_page">670</other_info_on_meta>
    <other_info_on_meta type="mention_page">679</other_info_on_meta>
    <other_info_on_meta type="mention_page">680</other_info_on_meta>
    <other_info_on_meta type="mention_page">685</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="1836" rank="genus">TRIPHYSARIA</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (St. Petersburg)</publication_title>
      <place_in_publication>2: 52. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus triphysaria</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek tria, three, and physarion, small bladder, alluding to three pouches of abaxial corolla lip</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="(Fischer &amp; C. A. Meyer) D. D. Keck" date="unknown" rank="subgenus">Triphysaria</taxon_name>
    <taxon_hierarchy>genus orthocarpus;subgenus triphysaria</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">False owl's-clover</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>hemiparasitic.</text>
      <biological_entity id="o7507" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="nutrition" src="d0_s1" value="hemiparasitic" value_original="hemiparasitic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, not fleshy, glabrous or hairy.</text>
      <biological_entity id="o7508" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, usually subopposite to opposite proximally, alternate distally;</text>
      <biological_entity id="o7509" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="usually subopposite" name="arrangement" src="d0_s3" to="opposite proximally" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o7510" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, not leathery, margins entire (proximal cauline leaves) or pinnatifid, rarely bipinnatifid.</text>
      <biological_entity id="o7511" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o7512" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="bipinnatifid" value_original="bipinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, spikelike racemes;</text>
      <biological_entity id="o7513" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o7514" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o7515" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present;</text>
      <biological_entity id="o7516" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles absent.</text>
      <biological_entity id="o7517" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 4, calyx bilaterally symmetric, tubular or campanulate, lobes triangular to narrowly lanceolate;</text>
      <biological_entity id="o7518" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7519" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o7520" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o7521" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s10" to="narrowly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, corolla yellow, white, purple, or white, fading to rose-pink, strongly bilabiate, tubular and club-shaped, abaxial lobes 3, saccate, throat folded proximal to abaxial lips (except in T. pusilla), adaxial 2, adaxial lip beaked, ± straight, opening directed forward;</text>
      <biological_entity id="o7522" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7523" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o7524" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="white fading" name="coloration" src="d0_s11" to="rose-pink" />
        <character char_type="range_value" from="white fading" name="coloration" src="d0_s11" to="rose-pink" />
        <character char_type="range_value" from="white fading" name="coloration" src="d0_s11" to="rose-pink" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7525" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o7526" name="throat" name_original="throat" src="d0_s11" type="structure" />
      <biological_entity constraint="folded" id="o7527" name="throat" name_original="throat" src="d0_s11" type="structure" />
      <biological_entity id="o7528" name="lip" name_original="lips" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial" id="o7529" name="lip" name_original="lips" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7530" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="forward" name="orientation" src="d0_s11" value="directed" value_original="directed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, didynamous, filaments glabrous, pollen-sac 1;</text>
      <biological_entity id="o7531" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7532" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o7533" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7534" name="pollen-sac" name_original="pollen-sac" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 0;</text>
      <biological_entity id="o7535" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7536" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o7537" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o7538" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate, 2-lobed, or filiform to subcapitate.</text>
      <biological_entity id="o7539" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o7540" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s15" to="subcapitate" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s15" to="subcapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules: dehiscence loculicidal.</text>
      <biological_entity id="o7541" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 10–100, dark-brown, ovoid to ellipsoid, wings absent.</text>
      <biological_entity id="o7542" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s17" to="100" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 11.</text>
      <biological_entity id="o7543" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o7544" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 (5 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America; introduced in Asia (China).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="in Asia (China)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Taxa of Triphysaria have often been placed in Orthocarpus; both genera have adaxial corolla lips that are open at the tips (versus connate and folded downward forming a hood), expanded stigmas that are either capitate or two-lobed (versus unexpanded), and terminal attachments of the ovules to the placentas (versus lateral attachments). D. C. Tank and R. G. Olmstead (2008) found strong molecular support for their sister relationship. Triphysaria is distinguished from Castilleja by stamens with a single pollen sac (versus two), the throats of the corollas folded proximal to the abaxial corolla lips (except in T. pusilla), and the base chromosome number of 11.</discussion>
  <discussion>Triphysaria pusilla has been introduced in China, where the name T. chinensis (D. Y. Hong) D. Y. Hong has been incorrectly applied to it (T. I. Chuang and L. R. Heckard 1991).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems: branches decumbent to ascending; corollas 4–7 mm, beaks hooked, abaxial lobes 0.5–1 mm.</description>
      <determination>4. Triphysaria pusilla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems: branches ascending; corollas 6–25 mm, beaks not hooked, abaxial lobes 1.5–5 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stamens exserted; corollas creamy white, rarely pale yellow.</description>
      <determination>2. Triphysaria floribunda</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stamens included; corollas yellow, rarely yellow and white, or white, fading to rose pink.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems sparsely pubescent distally; corolla beaks ± yellow, white, or rose pink.</description>
      <determination>5. Triphysaria versicolor</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems puberulent to glandular-puberulent or pubescent to glandular-pubescent distally; corolla beaks dark purple, rarely yellow.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Corollas 10–25 mm, abaxial lobes 2–5 mm.</description>
      <determination>1. Triphysaria eriantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Corollas 10–12 mm, abaxial lobes 1.5–2 mm.</description>
      <determination>3. Triphysaria micrantha</determination>
    </key_statement>
  </key>
</bio:treatment>