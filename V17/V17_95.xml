<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Wayne J. Elisens</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">33</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Elisens" date="1985" rank="genus">HOLMGRENANTHE</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>5: 54, fig. 24. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus holmgrenanthe</taxon_hierarchy>
    <other_info_on_name type="etymology">For Arthur Herman Holmgren, 1912–1992, Noel Herman Holmgren, b. 1937, and Patricia Kern Holmgren, b. 1940, American botanists, and Greek anthos, flower</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Rocklady</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
      <biological_entity id="o20695" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex woody.</text>
      <biological_entity id="o20696" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, hairy.</text>
      <biological_entity id="o20697" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, alternate;</text>
      <biological_entity id="o20698" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o20699" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, leathery, margins spinulose.</text>
      <biological_entity id="o20700" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o20701" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spinulose" value_original="spinulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, flowers solitary;</text>
      <biological_entity id="o20702" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o20703" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts absent.</text>
      <biological_entity id="o20704" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present;</text>
      <biological_entity id="o20705" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles absent.</text>
      <biological_entity id="o20706" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual;</text>
      <biological_entity id="o20707" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, distinct, lanceolate, calyx radially symmetric, urceolate;</text>
      <biological_entity id="o20708" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o20709" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s11" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corolla pale-yellow, bilaterally symmetric, weakly bilabiate, tubular, tube base not spurred or gibbous, lobes 5, abaxial 3, adaxial 2;</text>
      <biological_entity id="o20710" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s12" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s12" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity constraint="tube" id="o20711" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="spurred" value_original="spurred" />
        <character is_modifier="false" name="shape" src="d0_s12" value="gibbous" value_original="gibbous" />
      </biological_entity>
      <biological_entity id="o20712" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20713" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20714" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 4, basally adnate to corolla, didynamous, filaments glandular-hairy;</text>
      <biological_entity id="o20715" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="4" value_original="4" />
        <character constraint="to corolla" constraintid="o20716" is_modifier="false" modifier="basally" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o20716" name="corolla" name_original="corolla" src="d0_s13" type="structure" />
      <biological_entity id="o20717" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>staminode 1, filamentous;</text>
      <biological_entity id="o20718" name="staminode" name_original="staminode" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="false" name="texture" src="d0_s14" value="filamentous" value_original="filamentous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 1-locular, placentation axile;</text>
      <biological_entity id="o20719" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma conical.</text>
      <biological_entity id="o20720" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="conical" value_original="conical" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsules, dehiscence loculicidal.</text>
      <biological_entity constraint="fruits" id="o20721" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 30–70, tan, polygonal-pyramidal, wings absent.</text>
      <biological_entity id="o20722" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s18" to="70" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s18" value="polygonal-pyramidal" value_original="polygonal-pyramidal" />
      </biological_entity>
      <biological_entity id="o20723" name="wing" name_original="wings" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Holmgrenanthe is defined by characteristics unique or uncommon in Antirrhineae: cespitose habit, leaves and sepals with spinulose margins, a one-locular ovary with a T-shaped septum, and foveolate, pyramidal seeds. Based on its morphological distinctness, Holmgrenanthe has been recognized as monotypic since its delimitation (D. A. Sutton 1988; E. Fischer 2004). In phylogenetic studies based on morphological data, H. petrophila is basal to other genera (M. Ghebrehiwet et al. 2000) or is part of an unresolved basal trichotomy among genera (W. J. Elisens 1985) within the Maurandya clade (Maurandyinae). Bayesian and maximum likelihood analyses of nuclear ITS data in Antirrhineae (M. Fernández-Mazuecos et al. 2013) placed Holmgrenanthe in a Cymbalaria clade comprising two subclades, one including Holmgrenanthe and six other genera in subtribe Maurandyinae and the other Asarina and Cymbalaria.</discussion>
  
</bio:treatment>