<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">verna</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 14. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species verna</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Spring speedwell</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o31574" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, (1–) 3–15 (–20) cm, at least distally glandular and eglandular-hairy.</text>
      <biological_entity id="o31575" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="at-least distally" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade lanceolate to ovate, (4–) 6–13 × (2.5–) 4–10 mm, 1.3–1.6 times as long as wide, base cuneate, proximal margins coarsely crenate-serrate, distal pinnatifid to subpalmatifid, lobes 3–7, central largest, lateral linear to lanceolate, apex of central lobe obtuse, lateral ± acute, surfaces sparsely glandular-hairy.</text>
      <biological_entity id="o31576" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o31577" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s2" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s2" to="13" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s2" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="1.3-1.6" value_original="1.3-1.6" />
      </biological_entity>
      <biological_entity id="o31578" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31579" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s2" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o31580" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="to subpalmatifid" constraintid="o31581" is_modifier="false" name="shape" src="d0_s2" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o31581" name="subpalmatifid" name_original="subpalmatifid" src="d0_s2" type="structure" />
      <biological_entity id="o31582" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity constraint="central" id="o31583" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="largest" value_original="largest" />
        <character is_modifier="false" name="position" src="d0_s2" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o31584" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="position" src="d0_s2" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="central" id="o31585" name="lobe" name_original="lobe" src="d0_s2" type="structure" />
      <biological_entity id="o31586" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <relation from="o31584" id="r2391" name="part_of" negation="false" src="d0_s2" to="o31585" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1–3, terminal and axillary, 20–80 mm, (5–) 15–40 (–60) -flowered, axis eglandular and glandular-hairy;</text>
      <biological_entity id="o31587" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="80" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="(5-)15-40(-60)-flowered" value_original="(5-)15-40(-60)-flowered" />
      </biological_entity>
      <biological_entity id="o31588" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts proximalmost often 3-fid, others linear-lanceolate, 3–5 (–8) mm.</text>
      <biological_entity id="o31589" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="3-fid" value_original="3-fid" />
      </biological_entity>
      <biological_entity id="o31590" name="other" name_original="others" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels erect, (0.4–) 1–3 mm, shorter than subtending bract, length 1/3–1/2 times calyx, eglandular and glandular-hairy.</text>
      <biological_entity id="o31591" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character constraint="than subtending bract" constraintid="o31592" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character constraint="calyx" constraintid="o31593" is_modifier="false" name="length" src="d0_s5" value="1/3-1/2 times calyx" value_original="1/3-1/2 times calyx" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o31592" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <biological_entity id="o31593" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes (2–) 3–5 (–7) mm, apex acute, eglandular and glandular-hairy;</text>
      <biological_entity id="o31594" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o31595" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31596" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla sky to pale blue, 1.5–3 mm diam.;</text>
      <biological_entity id="o31597" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o31598" name="corolla" name_original="corolla" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 0.2–0.6 mm;</text>
      <biological_entity id="o31599" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o31600" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 0.2–0.6 mm, stigma white.</text>
      <biological_entity id="o31601" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o31602" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31603" name="stigma" name_original="stigma" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules compressed in cross-section, obcordiform, 2.5–3.5 × 3.5–5 mm, apex emarginate, eglandular-hairy.</text>
      <biological_entity id="o31604" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="in cross-section" constraintid="o31605" is_modifier="false" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="obcordiform" value_original="obcordiform" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31605" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o31606" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 8–20 (–26), yellowish, ellipsoid, flat, 0.9–1.6 × 0.6–1.3 mm, 0.2 mm thick, smooth to ± rugulose.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 16 (Eurasia).</text>
      <biological_entity id="o31607" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="26" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character name="thickness" src="d0_s11" unit="mm" value="0.9-1.6×0.6-1.3" value_original="0.9-1.6×0.6-1.3" />
        <character name="thickness" src="d0_s11" unit="mm" value="0.2" value_original="0.2" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s11" to="more or less rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31608" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Specimens of Veronica verna from Alberta have not been verified.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Jun(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open pine and oak forests, rocky and sandy steppes, pastures, meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open pine" />
        <character name="habitat" value="oak forests" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy steppes" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., N.B., N.S., Ont., P.E.I.; Idaho, Ind., Mass., Mich., Minn., Mont., N.Y., Oreg., Wash., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>