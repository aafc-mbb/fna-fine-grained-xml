<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Glabri</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">glaber</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glaber</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section glabri;species glaber;variety glaber;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>107a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous.</text>
      <biological_entity id="o28206" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: calyx lobes ovate to orbiculate, 2–4.8 × 1.4–3.8 mm, apex rounded or abruptly short-acuminate;</text>
      <biological_entity id="o28207" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity constraint="calyx" id="o28208" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="orbiculate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="4.8" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s1" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28209" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s1" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>corolla 24–33 mm, glabrous or sparsely to moderately white-lanate internally abaxially;</text>
      <biological_entity id="o28210" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o28211" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s2" to="33" to_unit="mm" />
        <character char_type="range_value" from="glabrous or" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s2" to="sparsely moderately white-lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>staminode apex rounded.</text>
      <biological_entity id="o28212" name="flower" name_original="flowers" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 16.</text>
      <biological_entity constraint="staminode" id="o28213" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28214" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety glaber occurs from southwestern North Dakota and north-central South Dakota into the Nebraska Panhandle, southeastern Montana, and through most of eastern Wyoming, and it is replaced by var. alpinus and var. brandegeei southward along the Front Range of the Rocky Mountains.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly shortgrass prairies, pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" modifier="sandy or gravelly shortgrass" />
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont., Nebr., N.Dak., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>