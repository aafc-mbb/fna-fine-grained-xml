<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">587</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">583</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="mention_page">589</other_info_on_meta>
    <other_info_on_meta type="mention_page">591</other_info_on_meta>
    <other_info_on_meta type="mention_page">603</other_info_on_meta>
    <other_info_on_meta type="mention_page">610</other_info_on_meta>
    <other_info_on_meta type="mention_page">625</other_info_on_meta>
    <other_info_on_meta type="mention_page">664</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Fernald" date="1898" rank="species">applegatei</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>6: 49. 1898</place_in_publication>
      <other_info_on_pub>(as Castilleia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species applegatei</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Applegate’s paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, (0.8–) 1–5 (–6) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a branched, woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o24849" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o24850" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o24849" id="r1894" name="with" negation="false" src="d0_s2" to="o24850" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, erect, ascending, or decumbent, unbranched or branched, hairs sparse to dense, spreading, long, soft to ± stiff, eglandular, mixed with shorter stipitate-glandular ones.</text>
      <biological_entity id="o24851" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o24852" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s3" to="dense" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character constraint="with shorter stipitate-glandular ones" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green to purplish or brown, linear to broadly lanceolate, sometimes ovate, 1–6 cm, not fleshy, margins wavy, involute, (0–) 3 (–5) -lobed, apex rounded or narrowly acute to acuminate;</text>
      <biological_entity id="o24853" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purplish or brown" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="broadly lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o24854" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(0-)3(-5)-lobed" value_original="(0-)3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o24855" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes spreading or ascending, lanceolate to broadly lanceolate, often short, apex acute to rounded.</text>
      <biological_entity id="o24856" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="broadly lanceolate" />
        <character is_modifier="false" modifier="often" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o24857" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2–12 (–21 in fruit) × 1–5 cm;</text>
      <biological_entity id="o24858" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally green to dull purplish brown, distally red, red-orange, or scarlet, sometimes orange, white, or yellow, rarely with a narrow yellowish band medially, lanceolate, broadly lanceolate, oblong, or lanceolate-ovate, 3–5 (–7) -lobed;</text>
      <biological_entity id="o24859" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="dull purplish brown distally red red-orange or scarlet" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="dull purplish brown distally red red-orange or scarlet" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="dull purplish brown distally red red-orange or scarlet" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="dull purplish brown distally red red-orange or scarlet" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-5(-7)-lobed" value_original="3-5(-7)-lobed" />
      </biological_entity>
      <biological_entity id="o24860" name="band" name_original="band" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <relation from="o24859" id="r1895" modifier="rarely" name="with" negation="false" src="d0_s7" to="o24860" />
    </statement>
    <statement id="d0_s8">
      <text>lobes spreading to ascending, linear, sometimes expanded near tip, long, arising from ca. mid length, central lobe apex obtuse to rounded, sometimes expanded, others acute, rounded, obtuse, or acuminate.</text>
      <biological_entity id="o24861" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="ascending" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character constraint="near tip" constraintid="o24862" is_modifier="false" modifier="sometimes" name="size" src="d0_s8" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s8" value="long" value_original="long" />
        <character constraint="from mid lobes" constraintid="o24863" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o24862" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="mid" id="o24863" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity constraint="lobe" id="o24864" name="apex" name_original="apex" src="d0_s8" type="structure" constraint_original="central lobe">
        <character char_type="range_value" from="obtuse" name="length" src="d0_s8" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s8" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o24865" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces proximally green or whitish, sometimes yellow, lobes colored as bract lobes, sometimes with a yellow band between proximal and distal portions, 13–25 mm;</text>
      <biological_entity id="o24866" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o24867" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character constraint="as bract lobes" constraintid="o24868" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="bract" id="o24868" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity constraint="between proximal and portions" constraintid="o24870" id="o24869" name="band" name_original="band" src="d0_s9" type="structure" constraint_original="between  proximal and  portions, ">
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="proximal and distal" id="o24870" name="portion" name_original="portions" src="d0_s9" type="structure" />
      <relation from="o24867" id="r1896" modifier="sometimes" name="with" negation="false" src="d0_s9" to="o24869" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 4–14 mm, 33–50% of calyx length, deeper than laterals, lateral 2.5–8 mm, 12–50% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o24871" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
        <character constraint="than laterals" constraintid="o24872" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="33-50%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24872" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes lanceolate-acuminate, narrowly oblong, or narrowly triangular, apex acuminate, acute, or obtuse.</text>
      <biological_entity id="o24873" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate-acuminate" value_original="lanceolate-acuminate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o24874" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight or curved in proximal 1/3, 16–35 (–41) mm;</text>
      <biological_entity id="o24875" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character constraint="in proximal 1/3" constraintid="o24876" is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s12" to="41" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24876" name="1/3" name_original="1/3" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>tube 9–22 mm;</text>
      <biological_entity id="o24877" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak usually long-exserted, adaxially green, yellow-green, or yellow, 6–20 mm;</text>
      <biological_entity id="o24878" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s14" value="long-exserted" value_original="long-exserted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip deep green to whitish or yellow, reduced, inconspicuous, protuberant, thickened, included or exserted, 1–3 mm, 5–20% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o24879" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="deep" value_original="deep" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s15" to="whitish or yellow" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="protuberant" value_original="protuberant" />
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24880" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o24879" id="r1897" modifier="5-20%" name="as long as" negation="false" src="d0_s15" to="o24880" />
    </statement>
    <statement id="d0_s16">
      <text>teeth ascending or incurved, deep green to yellow, 0.5–1 mm. 2n = 24, 48.</text>
      <biological_entity id="o24881" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="deep" value_original="deep" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s16" to="yellow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24882" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
        <character name="quantity" src="d0_s16" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja applegatei is a widespread and often common species, with complex patterns of variation and several common but inconstant color forms, especially in var. pinetorum. Castilleja disticha and C. martini, although sometimes included as subspecies within C. applegatei, are treated as species here. Both are more morphologically divergent and more easily distinguished from typical C. applegatei than are the four varieties accepted here. Variety pinetorum is the most widespread form and also occurs over a wider range of elevations than the other three varieties, which are primarily montane to subalpine and occasionally alpine.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 25–41 mm, beaks 9–20 mm, tubes 15–22 mm; leaves usually 0–3-lobed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas 34–41 mm; herbs 1–2.5(–3.5) dm; 2100–2800 m; Crater Lake to Newberry Crater, Cascade Range, Oregon.</description>
      <determination>4a. Castilleja applegatei var. applegatei</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas 25–35 mm; herbs 1.2–6 dm; 300–3600 m; California to Oregon and Idaho.</description>
      <determination>4c. Castilleja applegatei var. pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 16–31 mm, beaks 6–14 mm, tubes 9–14 mm; leaves usually 3–5-lobed.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla beaks 6–10 mm; abaxial and adaxial calyx clefts (4–)5–8(–10) mm.</description>
      <determination>4b. Castilleja applegatei var. breweri</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla beaks 9–14 mm; abaxial and adaxial calyx clefts 7–14 mm.</description>
      <determination>4d. Castilleja applegatei var. viscida</determination>
    </key_statement>
  </key>
</bio:treatment>