<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="(Rafinesque) Pennell" date="1920" rank="subgenus">Dasanthera</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Erianthera</taxon_name>
    <taxon_name authority="Greene" date="1892" rank="species">davidsonii</taxon_name>
    <taxon_name authority="(D. D. Keck) Cronquist in C. L. Hitchcock et al." date="1959" rank="variety">menziesii</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>4: 379. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus dasanthera;section erianthera;species davidsonii;variety menziesii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">davidsonii</taxon_name>
    <taxon_name authority="D. D. Keck" date="1957" rank="subspecies">menziesii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>8: 247. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species davidsonii;subspecies menziesii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">menziesii</taxon_name>
    <taxon_name authority="Pennell &amp; D. D. Keck" date="unknown" rank="subspecies">thompsonii</taxon_name>
    <taxon_hierarchy>genus p.;species menziesii;subspecies thompsonii</taxon_hierarchy>
  </taxon_identification>
  <number>3b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades spatulate to elliptic, margins ± serrulate, apex rounded to obtuse or acute.</text>
      <biological_entity id="o13683" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s0" to="elliptic" />
      </biological_entity>
      <biological_entity id="o13684" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s0" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o13685" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s0" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: corolla violet, 20–35 mm, moderately white-villous internally abaxially.</text>
      <biological_entity id="o13686" name="flower" name_original="flowers" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>2n = 16.</text>
      <biological_entity id="o13687" name="corolla" name_original="corolla" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="violet" value_original="violet" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s1" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="moderately; internally abaxially; abaxially" name="pubescence" src="d0_s1" value="white-villous" value_original="white-villous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13688" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The ranges of var. davidsonii and var. menziesii overlap throughout much of British Columbia and Washington.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops, ledges, talus slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="slopes" modifier="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>