<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Christopher P. Randle</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">559</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="mention_page">562</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Pennell" date="1928" rank="genus">BRACHYSTIGMA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>80: 432. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus brachystigma</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek brachys, short, and stigma, stigma</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Desert foxglove</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>hemiparasitic, caudex woody.</text>
      <biological_entity id="o17567" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="nutrition" src="d0_s1" value="hemiparasitic" value_original="hemiparasitic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o17568" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, not fleshy, hirsutulous.</text>
      <biological_entity id="o17569" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, in whorls of 3;</text>
      <biological_entity id="o17570" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o17571" name="whorl" name_original="whorls" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
      <relation from="o17570" id="r1363" name="in" negation="false" src="d0_s3" to="o17571" />
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o17572" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, not leathery, margins entire.</text>
      <biological_entity id="o17573" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o17574" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes;</text>
      <biological_entity id="o17575" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o17576" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o17577" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present;</text>
      <biological_entity id="o17578" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles absent.</text>
      <biological_entity id="o17579" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 5, calyx nearly radially symmetric, broadly campanulate, lobes deltate;</text>
      <biological_entity id="o17580" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17581" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o17582" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="nearly radially" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o17583" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, corolla yellow, bilabiate, subrotate, abaxial lobes 3, adaxial 2;</text>
      <biological_entity id="o17584" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17585" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o17586" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subrotate" value_original="subrotate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17587" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17588" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, didynamous, filaments glabrescent proximally, villous distally;</text>
      <biological_entity id="o17589" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17590" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o17591" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s12" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s12" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 0;</text>
      <biological_entity id="o17592" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o17593" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o17594" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o17595" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma clavate.</text>
      <biological_entity id="o17596" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o17597" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules: dehiscence loculicidal.</text>
      <biological_entity id="o17598" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 100+, dark-brown, ellipsoid, wings absent or present.</text>
      <biological_entity id="o17599" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s17" upper_restricted="false" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o17600" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachystigma is monospecific and narrowly restricted to dry mountain slopes of southeastern Arizona, southwestern New Mexico, and northern Mexico. F. W. Pennell (1928) differentiated it from the similar genus Agalinis by its yellow corollas, glabrous anthers, capitate stigmas, more acute capsules, and winged seeds. Phylogenetic analysis of three chloroplast genes supports this distinction (M. C. Neel and M. P. Cummings 2004). Further, Brachystigma may be differentiated from closely related genera Aureolaria, Dasistoma, and Seymeria by its leaves arranged in whorls of three, a characteristic unique in Orobanchaceae.</discussion>
  <references>
    <reference>Pennell, F. W. 1928. Agalinis and allies in North America: I. Proc. Acad. Nat. Sci. Philadelphia 80: 339–449.</reference>
  </references>
  
</bio:treatment>