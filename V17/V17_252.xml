<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">92</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="(Rafinesque) Pennell" date="1920" rank="subgenus">Dasanthera</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Erianthera</taxon_name>
    <taxon_name authority="(Piper) Howell" date="1901" rank="species">rupicola</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N.W. Amer.,</publication_title>
      <place_in_publication>510. 1901</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus dasanthera;section erianthera;species rupicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">newberryi</taxon_name>
    <taxon_name authority="Piper" date="1900" rank="variety">rupicola</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>27: 397. 1900</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species newberryi;variety rupicola</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Cliff beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, cespitose.</text>
      <biological_entity id="o39321" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 3–16 cm, puberulent to pubescent, not glaucous.</text>
      <biological_entity id="o39322" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="16" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s1" to="pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, 3–8 pairs, distals usually distinctly smaller than proximals, short-petiolate or sessile, 4–22 × 3–14 mm, blade round to elliptic or spatulate, base tapered to cuneate, margins ± serrate, apex rounded to obtuse, rarely acute, glabrous or hairy, usually glaucous.</text>
      <biological_entity id="o39323" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="8" />
      </biological_entity>
      <biological_entity id="o39324" name="distal" name_original="distals" src="d0_s2" type="structure">
        <character constraint="than proximals" constraintid="o39325" is_modifier="false" name="size" src="d0_s2" value="usually distinctly smaller" value_original="usually distinctly smaller" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="22" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39325" name="proximal" name_original="proximals" src="d0_s2" type="structure" />
      <biological_entity id="o39326" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s2" to="elliptic or spatulate" />
      </biological_entity>
      <biological_entity id="o39327" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity id="o39328" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o39329" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses continuous, ± secund, 1–3 cm, axis hairy proximally, pubescent or glandular-pubescent distally, verticillasters 1–3, cymes 1-flowered;</text>
      <biological_entity id="o39330" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o39331" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o39332" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o39333" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts elliptic to ovate, 3–9 × 2–6 mm;</text>
      <biological_entity constraint="proximal" id="o39334" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels ascending to erect, glandular-pubescent.</text>
      <biological_entity id="o39335" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s5" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o39336" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s5" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes lanceolate-elliptic to oblong, 6–11 × 2–2.8 mm, glandular-pubescent;</text>
      <biological_entity id="o39337" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o39338" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate-elliptic" name="shape" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla pink to red, pinkish lavender, or rose red, essentially unlined internally but abaxial ridges usually white, not personate, funnelform, 25–36 mm, glabrous externally, glabrous internally or sparsely white-lanate abaxially, tube 8–12 mm, throat 6–8 mm diam.;</text>
      <biological_entity id="o39339" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o39340" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s7" to="red pinkish lavender or rose red" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s7" to="red pinkish lavender or rose red" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s7" to="red pinkish lavender or rose red" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39341" name="ridge" name_original="ridges" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="essentially; internally; usually" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="personate" value_original="personate" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="36" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s7" value="or" value_original="or" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s7" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity id="o39342" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39343" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens: longer pair exserted, pollen-sacs 1–1.5 mm;</text>
      <biological_entity id="o39344" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o39345" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminode 13–15 mm, slightly flattened distally, 0.1 mm diam., tip straight, glabrous or distal 1–2 mm sparsely villous, hairs yellow, to 0.8 mm;</text>
      <biological_entity id="o39346" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity id="o39347" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; distally" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character name="diameter" src="d0_s9" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
      <biological_entity id="o39348" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o39349" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 26–30 mm.</text>
      <biological_entity id="o39350" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o39351" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="26" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 7–9 × 5–7 mm. 2n = 16.</text>
      <biological_entity id="o39352" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39353" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon rupicola occurs largely in the Siskiyou Mountains and Cascade Range from northern California to northern Washington. It is partly sympatric with and forms hybrids with P. cardwellii, P. davidsonii var. davidsonii, and P. fruticosus var. fruticosus (A. D. Every 1977). Hybrids between P. rupicola and P. davidsonii var. davidsonii are encountered frequently in the vicinity of Crater Lake National Park and Mt. Hood (Every).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs, rock outcrops.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>