<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Straw" date="1967" rank="genus">KECKIELLA</taxon_name>
    <taxon_name authority="(Torrey e× A. Gray) Straw" date="1967" rank="species">ternata</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>19: 204. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus keckiella;species ternata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Torrey e× A. Gray in W. H. Emory" date="1859" rank="species">ternatus</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 115. 1859</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species ternatus</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Scarlet keckiella</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems spreading to erect, 5–25 dm, glabrous when young, glaucous.</text>
      <biological_entity id="o116" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves whorled (3s), sometimes opposite;</text>
      <biological_entity id="o117" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear to narrowly oblanceolate or lanceolate, 15–60 mm, margins 4–10-toothed.</text>
      <biological_entity id="o118" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="narrowly oblanceolate or lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s2" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o119" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="4-10-toothed" value_original="4-10-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences panicles, glabrous or glandular-hairy.</text>
      <biological_entity constraint="inflorescences" id="o120" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: caly× 3.8–7.2 mm, lobes lanceolate to ovate;</text>
      <biological_entity id="o121" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character name="area" src="d0_s4" unit="mm" value="×3.8-7.2" value_original="×3.8-7.2" />
      </biological_entity>
      <biological_entity id="o122" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla red, 21–31 mm, tube plus indistinct throat 16–24 mm, adaxial lip 5.5–9.5 mm;</text>
      <biological_entity id="o123" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o124" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character char_type="range_value" from="21" from_unit="mm" name="some_measurement" src="d0_s5" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o125" name="tube" name_original="tube" src="d0_s5" type="structure" />
      <biological_entity id="o126" name="throat" name_original="throat" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s5" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o127" name="lip" name_original="lip" src="d0_s5" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s5" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen-sacs 0.8–1.1 mm;</text>
      <biological_entity id="o128" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o129" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s6" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminode densely yellow-hairy, included.</text>
      <biological_entity id="o130" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o131" name="staminode" name_original="staminode" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="yellow-hairy" value_original="yellow-hairy" />
        <character is_modifier="false" name="position" src="d0_s7" value="included" value_original="included" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces glabrous, 4.5–7.2 mm.</description>
      <determination>5a. Keckiella ternata var. ternata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces glandular, 3.8–5.5 mm.</description>
      <determination>5b. Keckiella ternata var. septentrionalis</determination>
    </key_statement>
  </key>
</bio:treatment>