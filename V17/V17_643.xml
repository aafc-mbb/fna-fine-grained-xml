<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GRATIOLA</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="species">floridana</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 103. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus gratiola;species floridana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Florida hedge-hyssop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o1855" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending or erect, simple or few to much-branched, 10–40 cm, glabrous proximally, glandular-pubescent distally.</text>
      <biological_entity id="o1856" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade oblanceolate to narrowly obovate or oval, 20–45 × 4–18 mm, margins with 1–6 pairs of teeth distally, apex obtuse to acute, surfaces glabrous.</text>
      <biological_entity id="o1857" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1858" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="narrowly obovate or oval" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="45" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1859" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <biological_entity id="o1860" name="pair" name_original="pairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o1861" name="tooth" name_original="teeth" src="d0_s2" type="structure" />
      <biological_entity id="o1862" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o1863" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o1859" id="r160" name="with" negation="false" src="d0_s2" to="o1860" />
      <relation from="o1860" id="r161" name="part_of" negation="false" src="d0_s2" to="o1861" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels slender, 20–45 (–55) mm, length 0.8–2 times bract, glabrate or finely glandular-pubescent;</text>
      <biological_entity id="o1864" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="55" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="45" to_unit="mm" />
        <character constraint="bract" constraintid="o1865" is_modifier="false" name="length" src="d0_s3" value="0.8-2 times bract" value_original="0.8-2 times bract" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o1865" name="bract" name_original="bract" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracteoles 2, 3–6 mm.</text>
      <biological_entity id="o1866" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals distinct, linear to narrowly lanceolate or narrowly oblong, 3–6 mm;</text>
      <biological_entity id="o1867" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1868" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate or narrowly oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla 14–25 mm, tube yellowish green, veins reddish purple to brownish yellow, limb white;</text>
      <biological_entity id="o1869" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1870" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1871" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
      <biological_entity id="o1872" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="reddish purple" name="coloration" src="d0_s6" to="brownish yellow" />
      </biological_entity>
      <biological_entity id="o1873" name="limb" name_original="limb" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 5–7 mm.</text>
      <biological_entity id="o1874" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1875" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules ovoid, 3–6 × 2.8–3.2 mm.</text>
      <biological_entity id="o1876" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s8" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 0.6–0.8 mm.</text>
      <biological_entity id="o1877" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Low wet woods, wooded stream banks, open meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="low wet woods" />
        <character name="habitat" value="wooded stream banks" />
        <character name="habitat" value="meadows" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>