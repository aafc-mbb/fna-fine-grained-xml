<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Glabri</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="species">leiophyllus</taxon_name>
    <taxon_name authority="(Clokey) N. H. Holmgren in A. Cronquist et al." date="1984" rank="variety">keckii</taxon_name>
    <place_of_publication>
      <publication_title>Intermount. Fl.</publication_title>
      <place_in_publication>4: 432. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section glabri;species leiophyllus;variety keckii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Clokey" date="1937" rank="species">keckii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>4: 128. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species keckii</taxon_hierarchy>
  </taxon_identification>
  <number>111c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–24 (–30) cm.</text>
      <biological_entity id="o3931" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="24" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="24" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves 16–56 × 2–5 (–10) mm, blade lanceolate to linear, usually folded lengthwise and curved.</text>
      <biological_entity constraint="cauline" id="o3932" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s1" to="56" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3933" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="linear" />
        <character is_modifier="false" modifier="usually; lengthwise" name="architecture_or_shape" src="d0_s1" value="folded" value_original="folded" />
        <character is_modifier="false" name="course" src="d0_s1" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: corolla 18–22 mm, white-lanate, rarely glabrous, internally abaxially.</text>
      <biological_entity id="o3934" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o3935" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s2" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="white-lanate" value_original="white-lanate" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety keckii is known from the Spring Mountains in Clark County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" modifier="pine" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–3500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>