<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="D. A. Sutton" date="1988" rank="genus">SAIROCARPUS</taxon_name>
    <taxon_name authority="(Kellogg) D. A. Sutton" date="1988" rank="species">vexillocalyculatus</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Antirrhineae,</publication_title>
      <place_in_publication>472. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus sairocarpus;species vexillocalyculatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antirrhinum</taxon_name>
    <taxon_name authority="Kellogg" date="1855" rank="species">vexillocalyculatum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 27. 1855</place_in_publication>
      <other_info_on_pub>(as vexillo-calyculatum)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus antirrhinum;species vexillocalyculatum</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Wiry snapdragon</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o7623" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 7–170 cm, not self-supporting, glabrous, eglandular-hairy, or glandular-hairy;</text>
      <biological_entity id="o7624" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="170" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="self-supporting" value_original="self-supporting" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches twining.</text>
      <biological_entity id="o7625" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="twining" value_original="twining" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite proximally, alternate distally;</text>
      <biological_entity id="o7626" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to narrowly elliptic, 10–60 × 1–20 mm, surfaces glabrous or hairy.</text>
      <biological_entity id="o7627" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="narrowly elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7628" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, racemes or flowers solitary.</text>
      <biological_entity id="o7629" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o7630" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o7631" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–4 mm.</text>
      <biological_entity id="o7632" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers cleistogamous and chasmogamous;</text>
      <biological_entity id="o7633" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calyx lobes unequal, glandular-hairy, adaxial lobe 3.5–14 × 1–9 mm;</text>
      <biological_entity constraint="calyx" id="o7634" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7635" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla light purple to white, often dark-veined, 8–17 mm, base gibbous, mouth 2–4 mm diam., palate sometimes purple-veined, convex, 3.5–8 mm diam., puberulent.</text>
      <biological_entity id="o7636" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="light purple" name="coloration" src="d0_s9" to="white" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s9" value="dark-veined" value_original="dark-veined" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7637" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="gibbous" value_original="gibbous" />
      </biological_entity>
      <biological_entity id="o7638" name="mouth" name_original="mouth" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7639" name="palate" name_original="palate" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s9" value="purple-veined" value_original="purple-veined" />
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ovoid, 4–8 mm, glandular-hairy, abaxial locule with 1 pore.</text>
      <biological_entity id="o7640" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7641" name="locule" name_original="locule" src="d0_s10" type="structure" />
      <biological_entity id="o7642" name="pore" name_original="pore" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <relation from="o7641" id="r630" name="with" negation="false" src="d0_s10" to="o7642" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds dark-brown to black, 0.7–1.5 mm, tuberculate, reticulate-ridged.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 30.</text>
      <biological_entity id="o7643" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s11" to="black" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="reticulate-ridged" value_original="reticulate-ridged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7644" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies breweri and vexillocalyculatus intergrade where their distributions overlap in the coastal ranges of California. Subspecies intermedius is disjunct in the Sierra Nevada.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems proximally glabrous or eglandular-hairy; corollas 11–17 mm, adaxial lips 7–17 mm.</description>
      <determination>7a. Sairocarpus vexillocalyculatus subsp. vexillocalyculatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems proximally glandular-hairy and, sometimes, eglandular-hairy; corollas 8–15 mm, adaxial lips 2–5.5 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence branches with 1 leaf at each proximal node.</description>
      <determination>7b. Sairocarpus vexillocalyculatus subsp. breweri</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescence branches with 2 leaves at each proximal node.</description>
      <determination>7c. Sairocarpus vexillocalyculatus subsp. intermedius</determination>
    </key_statement>
  </key>
</bio:treatment>