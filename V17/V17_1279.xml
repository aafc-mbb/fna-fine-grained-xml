<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="N. S. Fraga" date="2012" rank="species">carsonensis</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>30: 59, figs. 17–21. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species carsonensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">rubellus</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="variety">latiflorus</taxon_name>
    <taxon_hierarchy>genus mimulus;species rubellus;variety latiflorus</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Carson Valley monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, taprooted, densely compact.</text>
      <biological_entity id="o18308" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" modifier="densely" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branched from basal nodes, 2–7 (–8) cm, minutely glandular, internodes shortened, not evident.</text>
      <biological_entity id="o18309" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from basal nodes" constraintid="o18310" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18310" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o18311" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="length" src="d0_s1" value="shortened" value_original="shortened" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, basal not persistent;</text>
      <biological_entity id="o18312" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18313" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0 mm;</text>
      <biological_entity id="o18314" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="0" value_original="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade palmately 3-veined (in broader ones), linear to spatulate, (3–) 5–23 × 1–5 mm, base truncate to truncate-cordate, clasping, margins entire, apex acute to obtuse, surfaces minutely glandular.</text>
      <biological_entity id="o18315" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="23" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18316" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="truncate-cordate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o18317" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18318" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o18319" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 1–35, from distal or medial to distal nodes.</text>
      <biological_entity id="o18321" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o18320" id="r1418" name="from" negation="false" src="d0_s5" to="o18321" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels (3–) 5–14 mm.</text>
      <biological_entity id="o18322" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o18320" id="r1419" name="fruiting" negation="false" src="d0_s6" to="o18322" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces campanulate to widely urceolate, 4–7 mm, margins distinctly toothed or lobed, minutely glandular, ribs thickened, lobes pronounced, erect.</text>
      <biological_entity id="o18320" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="35" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18323" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o18324" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="widely urceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18325" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o18326" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o18327" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="pronounced" value_original="pronounced" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o18320" id="r1420" name="fruiting" negation="false" src="d0_s7" to="o18323" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, palate red-dotted and 1 large central spot, bilaterally symmetric, strongly bilabiate;</text>
      <biological_entity id="o18328" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o18329" name="palate" name_original="palate" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-dotted" value_original="red-dotted" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity constraint="central" id="o18330" name="spot" name_original="spot" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube-throat cylindric, distinct from abruptly expanding throat, (5–) 7–11 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o18331" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character constraint="from throat" constraintid="o18332" is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_distance" notes="" src="d0_s9" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="distance" notes="" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18332" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="abruptly" name="size" src="d0_s9" value="expanding" value_original="expanding" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o18333" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o18331" id="r1421" name="exserted beyond" negation="false" src="d0_s9" to="o18333" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 7–12 (–15) mm, each lobe 2-fid, palate densely bearded.</text>
      <biological_entity id="o18334" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18335" name="lobe" name_original="lobe" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o18336" name="palate" name_original="palate" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o18337" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o18338" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 3–6 mm.</text>
      <biological_entity id="o18339" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe carsonensis is restricted to the Carson Valley, Eagle Valley, and Washoe Valley region of Nevada and adjacent California, with one known disjunct occurrence about 58 km to the north in Nevada. The species was previously included in the broader concept of E. montioides but can be distinguished by its much branched and compact habit, linear to spatulate leaves with clasping bases, calyx with glabrous margins, and larger corolla with one large red spot in the center. Erythranthe carsonensis has been impacted by agriculture, urbanization, and other anthropogenic changes.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in sage brush/bitterbrush scrub in sand of decomposed granite.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in sage" />
        <character name="habitat" value="sage" />
        <character name="habitat" value="scrub" modifier="brush\/bitterbrush" constraint="in sand of decomposed granite" />
        <character name="habitat" value="sand" modifier="in" constraint="of decomposed granite" />
        <character name="habitat" value="decomposed granite" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>