<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">543</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">AGALINIS</taxon_name>
    <taxon_name authority="(Nuttall) Rafinesque" date="1837" rank="species">filifolia</taxon_name>
    <place_of_publication>
      <publication_title>New Fl.</publication_title>
      <place_in_publication>2: 65. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus agalinis;species filifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gerardia</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">filifolia</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 48. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gerardia;species filifolia</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Florida false foxglove</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems often leaning, branched, 30–70 cm;</text>
      <biological_entity id="o33912" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="leaning" value_original="leaning" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches ascending to widely spreading, subterete proximally to obtusely quadrangular distally, glabrous or scabridulous.</text>
      <biological_entity id="o33913" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="widely spreading" />
        <character char_type="range_value" from="subterete" modifier="distally" name="shape" src="d0_s1" to="proximally obtusely quadrangular" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, spreading;</text>
      <biological_entity id="o33914" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade filiform, widened slightly distally, 10–20 (–30) x 0.2–0.6 (–1) mm, fleshy, margins entire, adaxial surface sparsely scabrous;</text>
      <biological_entity id="o33915" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="slightly distally; distally" name="width" src="d0_s3" value="widened" value_original="widened" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s3" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o33916" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles often equal to or longer than subtending leaves.</text>
      <biological_entity constraint="adaxial" id="o33917" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character constraint="than subtending leaves" constraintid="o33918" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o33918" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" modifier="often" name="variability" src="d0_s4" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes, elongate, flowers 1 or 2 per node;</text>
      <biological_entity constraint="inflorescences" id="o33919" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o33920" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o33921" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts shorter than pedicels.</text>
      <biological_entity id="o33922" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="than pedicels" constraintid="o33923" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o33923" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels spreading-ascending, 9–33 mm, glabrous.</text>
      <biological_entity id="o33924" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading-ascending" value_original="spreading-ascending" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="33" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx campanulate, tube 3–5 mm, glabrous, lobes subulate, 0.3–1.5 mm;</text>
      <biological_entity id="o33925" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o33926" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o33927" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33928" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla dark-pink to nearly rose, with 2 yellow lines and purple spots in abaxial throat, 15–30 mm, throat pilose externally and villous within across bases and sinus of adaxial lobes, lobes: abaxial spreading, adaxial reflexed-spreading, 5–10 mm, pilose externally;</text>
      <biological_entity id="o33929" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="dark-pink" name="coloration" src="d0_s9" to="nearly rose" />
        <character constraint="in abaxial throat" constraintid="o33931" is_modifier="false" name="coloration" notes="" src="d0_s9" value="purple spots" value_original="purple spots" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33930" name="line" name_original="lines" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33931" name="throat" name_original="throat" src="d0_s9" type="structure" />
      <biological_entity id="o33932" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o33933, o33934" is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o33933" name="base" name_original="bases" src="d0_s9" type="structure" />
      <biological_entity id="o33934" name="sinus" name_original="sinus" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o33935" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o33936" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o33937" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o33938" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed-spreading" value_original="reflexed-spreading" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o33929" id="r2578" name="with" negation="false" src="d0_s9" to="o33930" />
      <relation from="o33933" id="r2579" name="part_of" negation="false" src="d0_s9" to="o33935" />
      <relation from="o33934" id="r2580" name="part_of" negation="false" src="d0_s9" to="o33935" />
    </statement>
    <statement id="d0_s10">
      <text>proximal anthers parallel to filaments, distal perpendicular to filaments, pollen-sacs 2.5–3.7 mm;</text>
      <biological_entity id="o33939" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark-pink" name="coloration" src="d0_s10" to="nearly rose" />
        <character constraint="in abaxial throat" constraintid="o33941" is_modifier="false" name="coloration" notes="" src="d0_s10" value="purple spots" value_original="purple spots" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33940" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33941" name="throat" name_original="throat" src="d0_s10" type="structure" />
      <biological_entity id="o33942" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o33943, o33944" is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o33943" name="base" name_original="bases" src="d0_s10" type="structure" />
      <biological_entity id="o33944" name="sinus" name_original="sinus" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o33945" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o33946" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o33947" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="to filaments" constraintid="o33948" is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s10" value="distal" value_original="distal" />
        <character constraint="to filaments" constraintid="o33949" is_modifier="false" name="orientation" src="d0_s10" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o33948" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o33949" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o33950" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s10" to="3.7" to_unit="mm" />
      </biological_entity>
      <relation from="o33939" id="r2581" name="with" negation="false" src="d0_s10" to="o33940" />
      <relation from="o33943" id="r2582" name="part_of" negation="false" src="d0_s10" to="o33945" />
      <relation from="o33944" id="r2583" name="part_of" negation="false" src="d0_s10" to="o33945" />
    </statement>
    <statement id="d0_s11">
      <text>style exserted, 10–20 mm.</text>
      <biological_entity id="o33951" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark-pink" name="coloration" src="d0_s11" to="nearly rose" />
        <character constraint="in abaxial throat" constraintid="o33953" is_modifier="false" name="coloration" notes="" src="d0_s11" value="purple spots" value_original="purple spots" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33952" name="line" name_original="lines" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33953" name="throat" name_original="throat" src="d0_s11" type="structure" />
      <biological_entity id="o33954" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character constraint="within bases, sinus" constraintid="o33955, o33956" is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o33955" name="base" name_original="bases" src="d0_s11" type="structure" />
      <biological_entity id="o33956" name="sinus" name_original="sinus" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial" id="o33957" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o33958" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o33959" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
      <relation from="o33951" id="r2584" name="with" negation="false" src="d0_s11" to="o33952" />
      <relation from="o33955" id="r2585" name="part_of" negation="false" src="d0_s11" to="o33957" />
      <relation from="o33956" id="r2586" name="part_of" negation="false" src="d0_s11" to="o33957" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules globular, 3.9–5.3 mm.</text>
      <biological_entity id="o33960" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globular" value_original="globular" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s12" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds nearly black, 0.4–0.5 mm. 2n = 28.</text>
      <biological_entity id="o33961" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33962" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agalinis filifolia is largely a species of Florida, where it is found in xeric, open, sandy, upland sites throughout the state. The species will, however, tolerate hydric conditions in open, pine flatwoods of southeastern Florida and is more common in south-central Florida than in any other part of its range. In the southernmost portion of its range, it flowers from early June through November, with different populations showing distinct phenologies. Agalinis filifolia is uncommon to rare in the Florida Panhandle and in southeastern Georgia, reaching its westernmost distribution along the remaining coastal scrub habitats of southern Baldwin County, Alabama.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Xeric, sandy, open pine forests, open coastal scrub habitats, dunes, open areas of pine flatwoods, hydric soils of pine flatwoods.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="xeric" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="open pine forests" />
        <character name="habitat" value="coastal scrub habitats" modifier="open" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="open areas" constraint="of pine flatwoods , hydric soils of pine flatwoods" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="hydric soils" constraint="of pine flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>