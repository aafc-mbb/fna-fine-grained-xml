<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1878" rank="species">heterodoxus</taxon_name>
    <taxon_name authority="(Greene) N. H. Holmgren" date="1992" rank="variety">cephalophorus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>44: 482. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species heterodoxus;variety cephalophorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Greene" date="1904" rank="species">cephalophorus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 79. 1904</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species cephalophorus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">heterodoxus</taxon_name>
    <taxon_name authority="(Greene) D. D. Keck" date="unknown" rank="subspecies">cephalophorus</taxon_name>
    <taxon_hierarchy>genus p.;species heterodoxus;subspecies cephalophorus</taxon_hierarchy>
  </taxon_identification>
  <number>165b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 15–35 cm.</text>
      <biological_entity id="o35516" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal and proximal cauline (17–) 20–75 × 6–18 mm.</text>
      <biological_entity id="o35517" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="17" from_unit="mm" name="atypical_length" src="d0_s1" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s1" to="75" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s1" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Thyrses: axis moderately to densely glandular-pubescent, verticillasters 2–6.2n = 16.</text>
      <biological_entity id="o35518" name="thyrse" name_original="thyrses" src="d0_s2" type="structure" />
      <biological_entity id="o35519" name="axis" name_original="axis" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o35520" name="verticillaster" name_original="verticillasters" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35521" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety cephalophorus occurs at the southern end of the range of Penstemon heterodoxus; it has been documented in Alpine, Fresno, Inyo, Kern, Madera, Mono, and Tulare counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine and alpine meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" modifier="subalpine and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–3300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>