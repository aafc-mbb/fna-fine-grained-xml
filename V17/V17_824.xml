<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">484</other_info_on_meta>
    <other_info_on_meta type="mention_page">485</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OROBANCHE</taxon_name>
    <taxon_name authority="(A. Gray) A. Heller" date="1898" rank="species">cooperi</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">cooperi</taxon_name>
    <taxon_hierarchy>family orobanchaceae;genus orobanche;species cooperi;subspecies cooperi</taxon_hierarchy>
  </taxon_identification>
  <number>13a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants simple or branched, base sometimes enlarged.</text>
      <biological_entity id="o12506" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o12507" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s0" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves sometimes imbricate proximally;</text>
      <biological_entity id="o12508" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes; proximally" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade broadly lanceolate, 7–11 mm, apex obtuse.</text>
      <biological_entity id="o12509" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12510" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: bracts 8–11 (–13) mm, apex not or only slightly reflexed.</text>
      <biological_entity id="o12511" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o12512" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12513" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="only slightly" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Corollas (15–) 18–22 mm;</text>
    </statement>
    <statement id="d0_s5">
      <text>palatal folds pubescent;</text>
      <biological_entity id="o12514" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s4" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12515" name="fold" name_original="folds" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial lip 4–7 mm, adaxial lip erect, 5–9 mm, lobes with or without apiculate tooth.</text>
      <biological_entity constraint="abaxial" id="o12516" name="lip" name_original="lip" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12517" name="lip" name_original="lip" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12518" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <biological_entity id="o12519" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <relation from="o12518" id="r996" name="with or without" negation="false" src="d0_s6" to="o12519" />
    </statement>
    <statement id="d0_s7">
      <text>Anthers glabrous or sparsely villous, stalked glands present on dorsal surface, sometimes obscure, rarely absent.</text>
      <biological_entity id="o12520" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o12521" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="stalked" value_original="stalked" />
        <character constraint="on dorsal surface" constraintid="o12522" is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="prominence" notes="" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o12522" name="surface" name_original="surface" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Stigmas peltate, crateriform, or bilaminar, rarely 2-lobed.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 48, 72, 96.</text>
      <biological_entity id="o12523" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="peltate" value_original="peltate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="crateriform" value_original="crateriform" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12524" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="48" value_original="48" />
        <character name="quantity" src="d0_s9" value="72" value_original="72" />
        <character name="quantity" src="d0_s9" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies cooperi is most abundant in the Sonoran Desert. A single specimen collected recently in Santa Cruz County, Arizona (Carnahan 1365, ARIZ, MO) is anomalous in habitat (oak woodland), elevation (1525 m), flowering time (August), and apparent host (Artemisia). The specimen has pale flowers but otherwise keys closest to subsp. cooperi.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy desert, dry washes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy desert" />
        <character name="habitat" value="dry washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-50–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="50" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>