<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Coerulei</taxon_name>
    <taxon_name authority="Nuttall" date="1813" rank="species">grandiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Pl. Upper Louisiana, no.</publication_title>
      <place_in_publication>64. 1813</place_in_publication>
      <other_info_on_pub>(as grandiflorum)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section coerulei;species grandiflorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>38.</number>
  <other_name type="common_name">Shell-leaf beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, (40–) 50–95 (–120) cm, glabrous.</text>
      <biological_entity id="o13006" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="40" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="95" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="95" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, glabrous;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 30–160 × 6–50 mm, blade spatulate to obovate, base tapered, apex rounded to obtuse or acute;</text>
      <biological_entity id="o13007" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o13008" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o13009" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="obovate" />
      </biological_entity>
      <biological_entity id="o13010" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13011" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 4–8 pairs, sessile, 18–90 (–110) × 15–50 mm, blade spatulate to orbiculate, base clasping, apex rounded to obtuse.</text>
      <biological_entity constraint="cauline" id="o13012" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="110" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s3" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13013" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="orbiculate" />
      </biological_entity>
      <biological_entity id="o13014" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o13015" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, cylindric, 12–30 (–40) cm, axis glabrous, verticillasters 3–7 (–9), cymes 2–4-flowered;</text>
      <biological_entity id="o13016" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s4" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13017" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13018" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
      <biological_entity id="o13019" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-4-flowered" value_original="2-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate to elliptic or orbiculate, (9–) 16–83 × (9–) 16–54 mm;</text>
      <biological_entity constraint="proximal" id="o13020" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="elliptic or orbiculate" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s5" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s5" to="83" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_width" src="d0_s5" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="width" src="d0_s5" to="54" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glabrous.</text>
      <biological_entity id="o13021" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13022" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate to lanceolate, 7–11 × 2.5–4 mm, margins entire, rarely erose, herbaceous or narrowly scarious, glabrous;</text>
      <biological_entity id="o13023" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o13024" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="11" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13025" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla lavender to blue or pinkish blue, with magenta nectar guides, ampliate, 35–48 mm, glabrous externally, glabrous internally, tube 10–13 mm, throat abruptly inflated, 15–18 mm diam., rounded abaxially;</text>
      <biological_entity id="o13026" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13027" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="blue or pinkish blue" />
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s8" to="48" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o13028" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
      </biological_entity>
      <biological_entity id="o13029" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13030" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s8" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o13027" id="r1020" name="with" negation="false" src="d0_s8" to="o13028" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs opposite, 2.1–2.6 mm, sutures papillate;</text>
      <biological_entity id="o13031" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13032" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o13033" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="distance" src="d0_s9" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13034" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 16–21 mm, included or reaching orifice, 2–2.6 mm diam., tip recurved to coiled, distal 1–2 mm sparsely villous, hairs golden yellow, to 0.5 mm;</text>
      <biological_entity id="o13035" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13036" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s10" to="21" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character name="position" src="d0_s10" value="reaching orifice" value_original="reaching orifice" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s10" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13037" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13038" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o13039" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 19–30 mm.</text>
      <biological_entity id="o13040" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13041" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 16–20 (–25) × 8–15 mm. 2n = 16.</text>
      <biological_entity id="o13042" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13043" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Widely distributed in the western Midwest and Great Plains in the United States, Penstemon grandiflorus is cultivated as an ornamental for its showy flowers. Reports from Connecticut (D. W. Magee and H. E. Ahles 2007), Indiana (K. Yatskievych 2000), Massachusetts (Magee and Ahles), Michigan (E. G. Voss 1972–1996), and Ohio (T. S. Cooperrider 1995) appear to be based on introductions. Penstemon grandiflorus has been seeded along highways in Iowa and Nebraska, where it is also native.</discussion>
  <discussion>The validity of the name Penstemon grandiflorus has been debated owing to the meager diagnosis by Nuttall. The name is accepted here, making P. bradburyi Pursh, an illegitimate, superfluous replacement for P. grandiflorus.</discussion>
  <discussion>The Dakota, Kiowa, and Sioux tribes, centered in the Great Plains, use Penstemon grandiflorus as an analgesic, a gastrointestinal aid, and for fevers (D. E. Moerman 1998).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or calcareous soils, tallgrass, mixed-grass, and sand prairies.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="calcareous soils" />
        <character name="habitat" value="tallgrass" />
        <character name="habitat" value="mixed-grass" />
        <character name="habitat" value="sand prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1800(–2400) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Conn., Ill., Ind., Iowa, Kans., Mass., Mich., Minn., Mo., Mont., Nebr., N.Mex., N.Dak., Ohio, Okla., S.Dak., Tex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>