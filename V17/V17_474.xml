<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">198</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="D. D. Keck" date="1940" rank="species">cinicola</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>520: 294. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species cinicola;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>150.</number>
  <other_name type="common_name">Ash beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o11425" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, (8–) 10–40 cm, glabrous or retrorsely hairy, not glaucous.</text>
      <biological_entity id="o11426" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves essentially cauline, basal usually poorly developed, not leathery, glabrous;</text>
      <biological_entity id="o11427" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11428" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="essentially" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 10–40 (–65) × 1–6 mm, blade oblanceolate, base tapered, margins entire, apex obtuse to acute;</text>
      <biological_entity constraint="basal" id="o11429" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually poorly" name="development" src="d0_s2" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o11430" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o11431" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o11432" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o11433" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11434" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–5 pairs, sessile or proximals short-petiolate, 12–60 × 2–5 (–7) mm, blade oblanceolate to lanceolate or linear, base tapered, margins entire, apex acute.</text>
      <biological_entity constraint="cauline" id="o11435" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o11436" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11437" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o11438" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o11439" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11440" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, cylindric, 1–14 cm, axis glabrous or ± puberulent at axils, verticillasters (1 or) 2–6, cymes (1–) 3–7-flowered, 2 per node;</text>
      <biological_entity id="o11441" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11442" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="at axils" constraintid="o11443" is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o11443" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity id="o11444" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o11445" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(1-)3-7-flowered" value_original="(1-)3-7-flowered" />
        <character constraint="per node" constraintid="o11446" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11446" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate to linear, 8–30 (–45) × 1–7 mm, margins entire;</text>
      <biological_entity constraint="proximal" id="o11447" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="45" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11448" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels erect, glabrous.</text>
      <biological_entity id="o11449" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11450" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes obovate to ovate, 1–1.8 × 0.7–1.1 mm, apex truncate to cuspidate, glabrous;</text>
      <biological_entity id="o11451" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o11452" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11453" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="truncate" name="architecture_or_shape" src="d0_s8" to="cuspidate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla violet to blue or purple, without nectar guides, funnelform, 7–9 (–11) mm, glabrous externally, moderately yellowish or white-pilose internally abaxially, tube 3–4 mm, throat slightly inflated, 2–3 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o11454" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o11455" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s9" to="blue or purple" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-pilose" value_original="white-pilose" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o11456" name="guide" name_original="guides" src="d0_s9" type="structure" />
      <biological_entity id="o11457" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11458" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o11455" id="r920" name="without" negation="false" src="d0_s9" to="o11456" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included or longer pair reaching orifice, pollen-sacs opposite, explanate, 0.3–0.4 (–0.5) mm, dehiscing completely, connective splitting, sides glabrous, sutures smooth;</text>
      <biological_entity id="o11459" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11460" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o11461" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <biological_entity id="o11462" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s10" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="distance" src="d0_s10" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o11463" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o11464" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11465" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o11460" id="r921" name="reaching" negation="false" src="d0_s10" to="o11461" />
    </statement>
    <statement id="d0_s11">
      <text>staminode 4–6 mm, included, 0.3–0.4 mm diam., tip straight, distal 0.5–1 mm sparsely to moderately pilose, hairs yellow, to 0.4 mm;</text>
      <biological_entity id="o11466" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11467" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11468" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11469" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11470" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 5–6 mm.</text>
      <biological_entity id="o11471" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11472" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 2.5–4 × 1.8–2.5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16, 32.</text>
      <biological_entity id="o11473" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11474" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon cinicola occurs along the eastern flank of the Cascade Range in central Oregon (Crook, Deschutes, Douglas, Klamath, and Lake counties) south to northern California (Lassen, Modoc, Shasta, Siskiyou, and Tehama counties).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, volcanic soils in sagebrush openings in pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" constraint="in sagebrush openings in pine forests" />
        <character name="habitat" value="volcanic soils" constraint="in sagebrush openings in pine forests" />
        <character name="habitat" value="sagebrush openings" constraint="in pine forests" />
        <character name="habitat" value="pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>