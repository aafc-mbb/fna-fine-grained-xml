<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">391</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">392</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(S. Watson) G. L. Nesom &amp; N. S. Fraga" date="2012" rank="species">filicaulis</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 36. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species filicaulis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="S. Watson" date="1891" rank="species">filicaulis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>26: 125. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species filicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">biolettii</taxon_name>
    <taxon_hierarchy>genus m.;species biolettii</taxon_hierarchy>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Slender-stemmed monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, taprooted.</text>
      <biological_entity id="o16030" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branched at basal nodes, 4–30 cm, densely glandular-puberulent, hairs 0.05–0.1 mm, gland-tipped.</text>
      <biological_entity id="o16031" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="at basal nodes" constraintid="o16032" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16032" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o16033" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s1" to="0.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, basal usually not persistent;</text>
      <biological_entity id="o16034" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16035" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0 mm;</text>
      <biological_entity id="o16036" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="0" value_original="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 1-veined or palmately 3-veined, linear to oblanceolate, 6–15 × (1–) 2–3 mm, base attenuate, margins entire to remotely mucronulate, teeth 1–3 per side, apex acute, surfaces densely glandular-puberulent, hairs 0.05–0.1 mm, gland-tipped.</text>
      <biological_entity id="o16037" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16038" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o16039" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s4" to="remotely mucronulate" />
      </biological_entity>
      <biological_entity id="o16040" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o16041" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o16041" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o16042" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o16043" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o16044" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 2–8, from medial to distal nodes.</text>
      <biological_entity id="o16046" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o16045" id="r1253" name="from" negation="false" src="d0_s5" to="o16046" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 9–30 mm, densely glandular-puberulent, hairs 0.05–0.1 mm, gland-tipped.</text>
      <biological_entity id="o16045" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="8" />
      </biological_entity>
      <biological_entity id="o16047" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o16048" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <relation from="o16045" id="r1254" name="fruiting" negation="false" src="d0_s6" to="o16047" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces reddish brown-spotted, strongly angled, cylindric-campanulate, 7–9 mm, margins distinctly toothed or lobed, densely glandular-puberulent, hairs 0.05–0.1 mm, gland-tipped, ribs corky, lobes pronounced, spreading.</text>
      <biological_entity id="o16049" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s6" to="0.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o16050" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o16051" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish brown-spotted" value_original="reddish brown-spotted" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s7" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric-campanulate" value_original="cylindric-campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16052" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o16053" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s7" to="0.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o16054" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s7" value="corky" value_original="corky" />
      </biological_entity>
      <biological_entity id="o16055" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="pronounced" value_original="pronounced" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o16049" id="r1255" name="fruiting" negation="false" src="d0_s7" to="o16050" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas pink to purple, throat dark red-purple, palate ridges yellow, bilaterally or nearly radially symmetric, weakly bilabiate or nearly regular;</text>
      <biological_entity id="o16056" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink to purple" value_original="pink to purple" />
      </biological_entity>
      <biological_entity id="o16057" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark red-purple" value_original="dark red-purple" />
      </biological_entity>
      <biological_entity constraint="palate" id="o16058" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="bilaterally; nearly radially" name="architecture" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s8" value="regular" value_original="regular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube-throat cylindric-funnelform, 8–12 (–17) mm, exserted beyond calyx margin;</text>
      <biological_entity id="o16059" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s9" to="17" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="distance" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o16060" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o16059" id="r1256" name="exserted beyond" negation="false" src="d0_s9" to="o16060" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 10–14 mm, lobes notched, palate villous.</text>
      <biological_entity id="o16061" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16062" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o16063" name="palate" name_original="palate" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o16064" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, hirtellous.</text>
      <biological_entity id="o16065" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 5–7 mm. 2n = 16.</text>
      <biological_entity id="o16066" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16067" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe filicaulis is known only for certain from Mariposa and Tuolumne counties. Kappler 1691 (UCLA) from Madera County also may be this species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist open areas on gentle slopes, meadows, roadsides, gravelly soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist open areas" constraint="on gentle slopes , meadows , roadsides ," />
        <character name="habitat" value="gentle slopes" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="soils" modifier="gravelly" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>