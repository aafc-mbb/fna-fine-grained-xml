<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">599</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Rydberg" date="1900" rank="species">crista-galli</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>1: 355. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species crista-galli</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>23.</number>
  <other_name type="common_name">Cock’s-comb paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1–5 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o36875" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o36876" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o36875" id="r2827" name="with" negation="false" src="d0_s2" to="o36876" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few to several, erect or ascending, unbranched or branched, sometimes with short, leafy axillary shoots, hairy, sometimes glabrate proximally, hairs spreading to retrorse, medium length to long, soft, eglandular, often mixed distally with shorter stipitate-glandular ones.</text>
      <biological_entity id="o36877" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes; proximally" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o36878" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o36879" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="retrorse" />
        <character is_modifier="false" name="length" src="d0_s3" value="medium" value_original="medium" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character constraint="with shorter ones" constraintid="o36880" is_modifier="false" modifier="often" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o36880" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o36877" id="r2828" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o36878" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green, linear to narrowly lanceolate, 2–8 cm, not fleshy, margins plane, involute, 0–5-lobed, apex acute;</text>
      <biological_entity id="o36881" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o36882" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0-5-lobed" value_original="0-5-lobed" />
      </biological_entity>
      <biological_entity id="o36883" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lateral lobes spreading, linear, apex acuminate.</text>
      <biological_entity constraint="lateral" id="o36884" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o36885" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 3–6 (–11) × 1.5–6.5 cm;</text>
      <biological_entity id="o36886" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="11" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish, distally red, red-orange, or orange, sometimes yellow or dull salmon, narrowly to broadly lanceolate or oblong, 3–5-lobed;</text>
      <biological_entity id="o36887" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="salmon" value_original="salmon" />
        <character is_modifier="false" modifier="narrowly to broadly; broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending-spreading, linear-lanceolate, long, arising below mid length, central lobe apex rounded to obtuse, lateral ones acute.</text>
      <biological_entity id="o36888" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending-spreading" value_original="ascending-spreading" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character constraint="below mid lobes" constraintid="o36889" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o36889" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s8" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o36890" name="apex" name_original="apex" src="d0_s8" type="structure" constraint_original="central lobe">
        <character char_type="range_value" from="rounded" name="length" src="d0_s8" to="obtuse" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, (20–) 25–35 mm;</text>
      <biological_entity id="o36891" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o36892" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36892" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts (6–) 10–17 mm, 50% of calyx length, deeper than laterals, lateral (1–) 3–6 (–10) mm, 35% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o36893" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="17" to_unit="mm" />
        <character constraint="than laterals" constraintid="o36894" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="50%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36894" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes slender, triangular, apex acute.</text>
      <biological_entity id="o36895" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o36896" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight, (25–) 30–40 (–45) mm;</text>
      <biological_entity id="o36897" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="45" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s12" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 15–20 mm;</text>
      <biological_entity id="o36898" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s13" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>abaxial lip visible through front cleft, beak long-exserted from calyx;</text>
      <biological_entity constraint="abaxial" id="o36899" name="lip" name_original="lip" src="d0_s14" type="structure">
        <character constraint="through front" constraintid="o36900" is_modifier="false" name="prominence" src="d0_s14" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o36900" name="front" name_original="front" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o36901" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character constraint="from calyx" constraintid="o36902" is_modifier="false" name="position" src="d0_s14" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
      <biological_entity id="o36902" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>beak adaxially green or yellow-green, 16–21 mm;</text>
      <biological_entity id="o36903" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s15" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>abaxial lip proximally white or yellow-green, distally green, reduced, usually visible in front cleft, 3 mm, 20% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o36904" name="lip" name_original="lip" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s16" value="reduced" value_original="reduced" />
        <character constraint="in front" constraintid="o36905" is_modifier="false" modifier="usually" name="prominence" src="d0_s16" value="visible" value_original="visible" />
        <character name="some_measurement" notes="" src="d0_s16" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o36905" name="front" name_original="front" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o36906" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <relation from="o36904" id="r2829" modifier="20%" name="as long as" negation="false" src="d0_s16" to="o36906" />
    </statement>
    <statement id="d0_s17">
      <text>teeth incurved to ascending, green, 1 mm. 2n = 96.</text>
      <biological_entity id="o36907" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character char_type="range_value" from="incurved" name="orientation" src="d0_s17" to="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="green" value_original="green" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o36908" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja crista-galli is found in the Rocky Mountains of southwestern Montana and northwestern Wyoming. The extent of its distribution into adjacent Idaho is unresolved, in part because it is frequently confused with either C. linariifolia or C. miniata. Castilleja crista-galli appears to be morphologically intermediate between them, leading to speculation that it might be an allopolyploid derivative. A DNA study (S. Matthews and M. Lavin 1998) showed little support for a hybrid origin. Castilleja crista-galli may be separated with some difficulty from the other two species by the presence of at least some short hairs on the stems and the frequently three- to five-parted leaves. Castilleja linariifolia and C. miniata both usually have subglabrous stems and entire leaves, sometimes three-parted distally, near the inflorescence.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, talus, ridges, dry to moist, open, conifer forests, montane meadows.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="dry to moist" />
        <character name="habitat" value="open" />
        <character name="habitat" value="conifer forests" />
        <character name="habitat" value="meadows" modifier="montane" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>