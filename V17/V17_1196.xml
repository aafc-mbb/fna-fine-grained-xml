<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">669</other_info_on_meta>
    <other_info_on_meta type="mention_page">666</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Behr" date="1855" rank="genus">CHLOROPYRON</taxon_name>
    <taxon_name authority="(Munz &amp; J. C. Roos) Tank &amp; J. M. Egger" date="2009" rank="species">tecopense</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>34: 189. 2009</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus chloropyron;species tecopense</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cordylanthus</taxon_name>
    <taxon_name authority="Munz &amp; J. C. Roos" date="1950" rank="species">tecopensis</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>2: 233. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cordylanthus;species tecopensis</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Tecopa bird's-beak</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect to spreading, 5–60 cm, sparsely puberulent, hairs glandular and eglandular.</text>
      <biological_entity id="o11966" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o11967" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades linear to linear-lanceolate, 5–15 × 1–2 mm, margins entire.</text>
      <biological_entity id="o11968" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11969" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spikes 2–15 cm;</text>
      <biological_entity id="o11970" name="spike" name_original="spikes" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts often purple distally, linear to narrowly lanceolate, 10–15 mm, margins pinnately 3-lobed near middle.</text>
      <biological_entity id="o11971" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often; distally" name="coloration_or_density" src="d0_s3" value="purple" value_original="purple" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11972" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="near middle bracts" constraintid="o11973" is_modifier="false" modifier="pinnately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o11973" name="bract" name_original="bracts" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: calyx 10–13 mm;</text>
      <biological_entity id="o11974" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o11975" name="calyx" name_original="calyx" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla pale lavender to pale lilac, 10–15 mm, lobes 4–5 mm, occasionally marked with redbrown or purple-red lines;</text>
      <biological_entity id="o11976" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o11977" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s5" to="pale lilac" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11978" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11979" name="line" name_original="lines" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="occasionally" name="coloration" src="d0_s5" value="redbrown" value_original="redbrown" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="purple-red" value_original="purple-red" />
      </biological_entity>
      <relation from="o11978" id="r953" modifier="occasionally" name="marked with" negation="false" src="d0_s5" to="o11979" />
    </statement>
    <statement id="d0_s6">
      <text>stamens 2, each with 2 pollen-sacs;</text>
      <biological_entity id="o11980" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o11981" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11982" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <relation from="o11981" id="r954" name="with" negation="false" src="d0_s6" to="o11982" />
    </statement>
    <statement id="d0_s7">
      <text>staminodes 2.</text>
      <biological_entity id="o11983" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o11984" name="staminode" name_original="staminodes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules narrowly ellipsoid, 5–7 mm.</text>
      <biological_entity id="o11985" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 8–10, brown, ± reniform, 2–3 mm, without abaxial crest.</text>
      <biological_entity constraint="abaxial" id="o11987" name="crest" name_original="crest" src="d0_s9" type="structure" />
      <relation from="o11986" id="r955" name="without" negation="false" src="d0_s9" to="o11987" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 28.</text>
      <biological_entity id="o11986" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="10" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11988" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chloropyron tecopense has a delicate appearance because of their slender branches with relatively small, appressed leaves. The species is found in Inyo and eastern San Bernardino counties in California and in Esmeralda County in western Nevada.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline meadows and flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" modifier="alkaline" />
        <character name="habitat" value="flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>