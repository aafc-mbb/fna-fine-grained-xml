<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">640</other_info_on_meta>
    <other_info_on_meta type="mention_page">576</other_info_on_meta>
    <other_info_on_meta type="mention_page">637</other_info_on_meta>
    <other_info_on_meta type="mention_page">644</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Bongard" date="1832" rank="species">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Acad. Imp. Sci. St.-Pétersbourg, sér.</publication_title>
      <place_in_publication>6, Sci. Math. 2(2): 158. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species parviflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>82.</number>
  <other_name type="common_name">Mountain or rosy or small-flowered paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, (0.6–) 1–4 (–5) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot or stout, branched roots.</text>
      <biological_entity id="o13566" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o13567" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <biological_entity id="o13568" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o13566" id="r1063" name="with" negation="false" src="d0_s2" to="o13567" />
      <relation from="o13566" id="r1064" name="with" negation="false" src="d0_s2" to="o13568" />
    </statement>
    <statement id="d0_s3">
      <text>Stems several or many, erect or ascending, unbranched except for short, leafy axillary shoots, glabrate proximally, hairy distally, hairs sparse, spreading, ± matted, long, soft, minute-glandular.</text>
      <biological_entity id="o13569" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="many" value_original="many" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character constraint="except-for axillary shoots" constraintid="o13570" is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="proximally" name="pubescence" notes="" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o13570" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o13571" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s3" value="matted" value_original="matted" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="minute-glandular" value_original="minute-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green or gray-green to purple-tinged or deep purple, often blackening on drying, narrowly to broadly lanceolate or elliptic, rarely linear, 1.5–5 cm, not fleshy, margins plane, sometimes ± wavy, flat, (0–) 3–9-lobed, apex acute to acuminate or obtuse;</text>
      <biological_entity id="o13572" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s4" to="purple-tinged or deep purple" />
        <character constraint="on margins, apex" constraintid="o13573, o13574" is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="blackening" value_original="blackening" />
      </biological_entity>
      <biological_entity id="o13573" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="condition" src="d0_s4" value="drying" value_original="drying" />
        <character is_modifier="true" modifier="narrowly; broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="true" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(0-)3-9-lobed" value_original="(0-)3-9-lobed" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate or obtuse" />
      </biological_entity>
      <biological_entity id="o13574" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="condition" src="d0_s4" value="drying" value_original="drying" />
        <character is_modifier="true" modifier="narrowly; broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="true" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="(0-)3-9-lobed" value_original="(0-)3-9-lobed" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate or obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes spreading or ascending, linear, sometimes lanceolate, much narrower than terminal lobe, evenly spaced, short, apex acute.</text>
      <biological_entity id="o13575" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character constraint="than terminal lobe" constraintid="o13576" is_modifier="false" name="width" src="d0_s5" value="much narrower" value_original="much narrower" />
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o13576" name="lobe" name_original="lobe" src="d0_s5" type="structure" />
      <biological_entity id="o13577" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2–16 × 1–3.5 cm;</text>
      <biological_entity id="o13578" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="16" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish, dull, deep purple, or reddish purple, distally pink, pink-purple, magenta, deep rose, crimson, cream, or white, sometimes red, pale orange, or red-orange, lanceolate to broadly elliptic or ovate, 3–7-lobed;</text>
      <biological_entity id="o13579" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink-purple" value_original="pink-purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="crimson" value_original="crimson" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale orange" value_original="pale orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale orange" value_original="pale orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-orange" value_original="red-orange" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="broadly elliptic or ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-7-lobed" value_original="3-7-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes spreading to ascending, linear, lanceolate, or lanceolate-acuminate, short to medium length, arising at or near mid length, apex obtuse to acute, central lobes sometimes rounded.</text>
      <biological_entity id="o13580" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate-acuminate" value_original="lanceolate-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate-acuminate" value_original="lanceolate-acuminate" />
        <character is_modifier="false" name="length" src="d0_s8" value="short to medium" value_original="short to medium" />
        <character is_modifier="false" name="length" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o13581" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
      <biological_entity constraint="central" id="o13582" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, 12–28 mm;</text>
      <biological_entity id="o13583" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o13584" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13584" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 6–15 mm, 40–70% of calyx length, deeper than laterals, lateral 1–8 mm, 10–35% of calyx length;</text>
      <biological_entity constraint="calyx" id="o13585" name="cleft" name_original="clefts" src="d0_s10" type="structure" constraint_original="calyx abaxial and adaxial; calyx">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character constraint="than laterals" constraintid="o13587" is_modifier="false" name="length" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" name="length" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13586" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o13587" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
      <relation from="o13585" id="r1065" modifier="40-70%" name="part_of" negation="false" src="d0_s10" to="o13586" />
    </statement>
    <statement id="d0_s11">
      <text>lobes narrowly to broadly triangular, sometimes distally expanded and flaring, petaloid, apex obtuse or acute, sometimes rounded.</text>
      <biological_entity id="o13588" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="sometimes distally" name="size" src="d0_s11" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flaring" value_original="flaring" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o13589" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight or slightly curved, 12–30 mm;</text>
      <biological_entity id="o13590" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 8–19 mm;</text>
      <biological_entity id="o13591" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted or subequal to calyx, adaxially green-yellowish or red, 5.5–11 mm;</text>
      <biological_entity id="o13592" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character constraint="to calyx" constraintid="o13593" is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="green-yellowish" value_original="green-yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13593" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip green, brown, or yellow, sometimes purple, reduced, slightly or not inflated and pouched, 1–3 mm, 20–45% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o13594" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="slightly; not" name="shape" src="d0_s15" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="pouched" value_original="pouched" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13595" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o13594" id="r1066" modifier="20-45%" name="as long as" negation="false" src="d0_s15" to="o13595" />
    </statement>
    <statement id="d0_s16">
      <text>teeth erect, green, white, yellow, pink, or red, 0.5–2 mm. 2n = 24, 48.</text>
      <biological_entity id="o13596" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="red" value_original="red" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13597" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
        <character name="quantity" src="d0_s16" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja parviflora is a complex, geographically widespread, and often misunderstood species ranging from southeastern Alaska through much of British Columbia, southwestern Yukon, and the Rocky Mountains of extreme western Alberta and southward in the Cascade Range to central Oregon.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas (18–)20–30 mm; calyces 20–28 mm; leaves (0–)3(–5)-lobed; Oregon, Washington.</description>
      <determination>82d. Castilleja parviflora var. oreopola</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 12–20(–25) mm; calyces 12–20(–28) mm; leaves (0–)3–9-lobed; Washington to Alaska, Alberta, and Yukon.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bracts distally white to cream, sometimes suffused with pink to purple; herbs 0.6–2.7 dm; n Cascade Range, Washington and s British Columbia.</description>
      <determination>82b. Castilleja parviflora var. albida</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bracts distally pink-purple, magenta, deep rose, or crimson, rarely white; herbs 1.2–5 dm; Olympic Mountains, Washington, w Canada, se Alaska.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves (3–)5–9-lobed; corolla beaks 5.5–7 mm; Alberta, British Columbia, Yukon, Alaska.</description>
      <determination>82a. Castilleja parviflora var. parviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves (0–)3(–5)-lobed; corolla beaks (5.5–)7–9(–11) mm; s Vancouver Island, British Columbia, Olympic Mountains, Washington.</description>
      <determination>82c. Castilleja parviflora var. olympica</determination>
    </key_statement>
  </key>
</bio:treatment>