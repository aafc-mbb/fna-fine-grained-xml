<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">664</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">578</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="mention_page">581</other_info_on_meta>
    <other_info_on_meta type="mention_page">583</other_info_on_meta>
    <other_info_on_meta type="mention_page">665</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Elmer" date="1906" rank="species">wightii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>41: 322. 1906</place_in_publication>
      <other_info_on_pub>(as Castilleia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species wightii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wightii</taxon_name>
    <taxon_name authority="Pennell" date="unknown" rank="subspecies">rubra</taxon_name>
    <taxon_hierarchy>genus castilleja;species wightii;subspecies rubra</taxon_hierarchy>
  </taxon_identification>
  <number>117.</number>
  <other_name type="common_name">Wight’s paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, perennial, 3–8 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with woody, branching roots.</text>
      <biological_entity id="o23436" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o23438" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <relation from="o23436" id="r1802" name="with" negation="false" src="d0_s2" to="o23438" />
      <relation from="o23436" id="r1803" name="with" negation="false" src="d0_s2" to="o23438" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, ascending to erect, much-branched, usually with prominent leafy axillary shoots, hairs spreading, long, soft, mixed with shorter, stipitate-glandular ones.</text>
      <biological_entity id="o23439" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="much-branched" value_original="much-branched" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o23440" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o23441" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character constraint="with shorter , stipitate-glandular ones" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <relation from="o23439" id="r1804" modifier="usually" name="with" negation="false" src="d0_s3" to="o23440" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green-tinged or ± purple, sometimes yellow-green tinged, densely crowded, linear, lanceolate, oblong, elliptic, or ovate, 1–7 cm, thickened, not usually fleshy, margins plane to wavy, involute or flat, 0–3 (–5) -lobed, apex broadly rounded to obtuse or acute;</text>
      <biological_entity id="o23442" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green-tinged" value_original="green-tinged" />
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="yellow-green tinged" value_original="yellow-green tinged" />
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="not usually" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o23443" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s4" to="wavy involute or flat" />
        <character char_type="range_value" from="plane" name="shape" src="d0_s4" to="wavy involute or flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0-3(-5)-lobed" value_original="0-3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o23444" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s4" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes spreading-ascending, linear, narrowly lanceolate, triangular, or rounded, apex acute, obtuse, or rounded.</text>
      <biological_entity id="o23445" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o23446" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 3–22 × 1.5–3.5 cm;</text>
      <biological_entity id="o23447" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="22" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally green to deep purple, rarely light tan, distally red, scarlet, rose, red-orange, or yellow, sometimes orange, dull brownish orange, pale pinkish tan, yellow aging white, yellow aging pink, red with pink apices, magenta, or white, sometimes with a yellow to deep purple medial band, lanceolate, oblong, or narrowly ovate, (0–) 3–5 (–7+) -lobed, distal margins of central lobe and sometimes also side lobes with multiple shallow teeth, proximal often wavy-margined;</text>
      <biological_entity id="o23448" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep purple rarely light tan distally red scarlet rose red-orange or yellow" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep purple rarely light tan distally red scarlet rose red-orange or yellow" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep purple rarely light tan distally red scarlet rose red-orange or yellow" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep purple rarely light tan distally red scarlet rose red-orange or yellow" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep purple rarely light tan distally red scarlet rose red-orange or yellow" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep purple rarely light tan distally red scarlet rose red-orange or yellow" />
        <character char_type="range_value" from="proximally green" name="coloration" src="d0_s7" to="deep purple rarely light tan distally red scarlet rose red-orange or yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brownish orange" value_original="brownish orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale pinkish" value_original="pale pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character constraint="with apices" constraintid="o23449" is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="(0-)3-5(-7+)-lobed" value_original="(0-)3-5(-7+)-lobed" />
      </biological_entity>
      <biological_entity id="o23449" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity constraint="medial" id="o23450" name="band" name_original="band" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23451" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity constraint="central" id="o23452" name="lobe" name_original="lobe" src="d0_s7" type="structure" />
      <biological_entity constraint="side" id="o23453" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <biological_entity id="o23454" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="multiple" value_original="multiple" />
        <character is_modifier="true" name="depth" src="d0_s7" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23455" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="wavy-margined" value_original="wavy-margined" />
      </biological_entity>
      <relation from="o23448" id="r1805" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o23450" />
      <relation from="o23451" id="r1806" name="part_of" negation="false" src="d0_s7" to="o23452" />
      <relation from="o23453" id="r1807" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o23454" />
    </statement>
    <statement id="d0_s8">
      <text>lobes erect or ascending, linear to lanceolate or oblanceolate, short, arising in distal 2/3, apex obtuse to acute, central lobe often rounded to truncate.</text>
      <biological_entity id="o23456" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="lanceolate or oblanceolate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character constraint="in distal 2/3" constraintid="o23457" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23457" name="2/3" name_original="2/3" src="d0_s8" type="structure" />
      <biological_entity id="o23458" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
      <biological_entity constraint="central" id="o23459" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0 mm or nearly so.</text>
      <biological_entity id="o23460" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="0" value_original="0" />
        <character name="some_measurement" src="d0_s9" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Calyces colored as bracts, 15–28 mm;</text>
      <biological_entity id="o23461" name="calyx" name_original="calyces" src="d0_s10" type="structure">
        <character constraint="as bracts" constraintid="o23462" is_modifier="false" name="coloration" src="d0_s10" value="colored" value_original="colored" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23462" name="bract" name_original="bracts" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>abaxial and adaxial clefts 8–11 mm, 33–50% of calyx length, deeper than laterals, lateral 2–5.5 mm, 5–30% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o23463" name="cleft" name_original="clefts" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
        <character constraint="than laterals" constraintid="o23464" is_modifier="false" name="coloration_or_size" src="d0_s11" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="33-50%" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23464" name="lateral" name_original="laterals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lobes lanceolate to oblong to broadly triangular, apex acute, obtuse, or rounded.</text>
      <biological_entity id="o23465" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="oblong" />
      </biological_entity>
      <biological_entity id="o23466" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Corollas slightly curved proximally, 20–30 mm;</text>
      <biological_entity id="o23467" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly; proximally" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tube 10–12 mm;</text>
      <biological_entity id="o23468" name="tube" name_original="tube" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>distal portion of beak exserted, abaxial lip included;</text>
      <biological_entity constraint="beak" id="o23469" name="portion" name_original="portion" src="d0_s15" type="structure" constraint_original="beak distal; beak">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o23470" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <biological_entity constraint="abaxial" id="o23471" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
      </biological_entity>
      <relation from="o23469" id="r1808" name="part_of" negation="false" src="d0_s15" to="o23470" />
    </statement>
    <statement id="d0_s16">
      <text>beak adaxially green to yellow, 11.5–18 mm;</text>
      <biological_entity id="o23472" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character char_type="range_value" from="adaxially green" name="coloration" src="d0_s16" to="yellow" />
        <character char_type="range_value" from="11.5" from_unit="mm" name="some_measurement" src="d0_s16" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>abaxial lip deep green, sometimes to very deep purple, reduced, included, usually not visible through front cleft, 1–2 mm, 15–25% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o23473" name="lip" name_original="lip" src="d0_s17" type="structure">
        <character is_modifier="false" name="depth" src="d0_s17" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes to very" name="depth" src="d0_s17" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s17" value="purple" value_original="purple" />
        <character is_modifier="false" name="size" src="d0_s17" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="position" src="d0_s17" value="included" value_original="included" />
        <character constraint="through front" constraintid="o23474" is_modifier="false" modifier="usually not" name="prominence" src="d0_s17" value="visible" value_original="visible" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23474" name="front" name_original="front" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o23475" name="beak" name_original="beak" src="d0_s17" type="structure" />
      <relation from="o23473" id="r1809" modifier="15-25%" name="as long as" negation="false" src="d0_s17" to="o23475" />
    </statement>
    <statement id="d0_s18">
      <text>teeth erect, green to sometimes pink, 0.5–1.5 mm. 2n = 24, 48.</text>
      <biological_entity id="o23476" name="tooth" name_original="teeth" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="green to sometimes" value_original="green to sometimes" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s18" value="pink" value_original="pink" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23477" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="24" value_original="24" />
        <character name="quantity" src="d0_s18" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja wightii is found along or near the central and northern coast of California. Historical collections of C. wightii from Curry County, Oregon, are referable to other species, but it should be sought in the area. Reports from the south-central coast of California are referable to other species, particularly C. affinis. Castilleja wightii appears to intergrade with C. latifolia and perhaps C. affinis south of San Francisco. Despite much attention from botanists, the perennial paintbrushes along the coast between Monterey and San Francisco can be perplexing and difficult to identify. This situation is likely the result of introgression, but this complex is in need of meticulous genetic and morphological analysis. North of San Francisco, C. wightii is straightforward to recognize, with its abundantly stipitate-glandular stems and leaves. In addition, the leaves are often crowded on the stems, which bear axillary shoots. Most populations have either red or yellow bracts, with only occasional individual plants of the other color. Yellow populations are found primarily in Marin and southern Sonoma counties southward and are gradually replaced by red populations from northern Sonoma County northward. Mixed color populations occur in a few places, especially in San Mateo County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal scrub, damp thickets, stream banks, sea bluffs, canyon slopes, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="damp thickets" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="sea bluffs" />
        <character name="habitat" value="canyon slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>