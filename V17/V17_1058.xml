<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">603</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">581</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="N. H. Holmgren" date="1971" rank="species">dissitiflora</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>21(4): 46, figs. 6–8. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species dissitiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>27.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1.8–4 (–5) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o37228" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1.8" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o37229" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o37228" id="r2847" name="with" negation="false" src="d0_s2" to="o37229" />
    </statement>
    <statement id="d0_s3">
      <text>Stems several to many, erect to ascending, unbranched, sometimes branched, hairs spreading, long, soft, mixed with shorter stipitate-glandular ones.</text>
      <biological_entity id="o37230" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="several" name="quantity" src="d0_s3" to="many" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o37231" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character constraint="with shorter ones" constraintid="o37232" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o37232" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green, linear to narrowly or broadly lanceolate, (1–) 3–5 (–6) cm, not fleshy, margins wavy (obscure on many pressed specimens), involute, usually 0–3 (–5) -lobed, apex broadly acute to rounded;</text>
      <biological_entity id="o37233" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly or broadly lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o37234" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="0-3(-5)-lobed" value_original="0-3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o37235" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes widely spreading, linear to narrowly lanceolate, apex acute to acuminate.</text>
      <biological_entity id="o37236" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o37237" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2.5–10 × 2–5.5 cm;</text>
      <biological_entity id="o37238" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish, distally red to red-orange, narrowly lanceolate to broadly lanceolate, 3–5-lobed;</text>
      <biological_entity id="o37239" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="distally red" name="coloration" src="d0_s7" to="red-orange" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s7" to="broadly lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes spreading, distal, if present, ascending, linear or narrowly oblanceolate to triangular, proximals long, arising below mid-blade, distals short, sometimes mere teeth, near apex of central lobe, sometimes wavy-margined, apex obtuse to rounded, sometimes acute.</text>
      <biological_entity id="o37240" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="distal" id="o37241" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" notes="" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o37242" name="proximal" name_original="proximals" src="d0_s8" type="structure">
        <character is_modifier="true" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="narrowly oblanceolate" is_modifier="true" name="shape" src="d0_s8" to="triangular" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="wavy-margined" value_original="wavy-margined" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
      <biological_entity id="o37243" name="distal" name_original="distals" src="d0_s8" type="structure">
        <character is_modifier="true" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="narrowly oblanceolate" is_modifier="true" name="shape" src="d0_s8" to="triangular" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="wavy-margined" value_original="wavy-margined" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
      <biological_entity id="o37244" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="true" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="narrowly oblanceolate" is_modifier="true" name="shape" src="d0_s8" to="triangular" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="wavy-margined" value_original="wavy-margined" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
      <biological_entity id="o37245" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="narrowly oblanceolate" is_modifier="true" name="shape" src="d0_s8" to="triangular" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="wavy-margined" value_original="wavy-margined" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
      <biological_entity id="o37246" name="mid-blade" name_original="mid-blade" src="d0_s8" type="structure" />
      <biological_entity id="o37247" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity constraint="central" id="o37248" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <relation from="o37241" id="r2848" name="if" negation="false" src="d0_s8" to="o37242" />
      <relation from="o37241" id="r2849" name="if" negation="false" src="d0_s8" to="o37243" />
      <relation from="o37241" id="r2850" name="if" negation="false" src="d0_s8" to="o37244" />
      <relation from="o37241" id="r2851" name="if" negation="false" src="d0_s8" to="o37245" />
      <relation from="o37242" id="r2852" name="below" negation="false" src="d0_s8" to="o37246" />
      <relation from="o37243" id="r2853" name="below" negation="false" src="d0_s8" to="o37246" />
      <relation from="o37244" id="r2854" name="below" negation="false" src="d0_s8" to="o37246" />
      <relation from="o37245" id="r2855" name="below" negation="false" src="d0_s8" to="o37246" />
      <relation from="o37242" id="r2856" name="near" negation="false" src="d0_s8" to="o37247" />
      <relation from="o37243" id="r2857" name="near" negation="false" src="d0_s8" to="o37247" />
      <relation from="o37244" id="r2858" name="near" negation="false" src="d0_s8" to="o37247" />
      <relation from="o37245" id="r2859" name="near" negation="false" src="d0_s8" to="o37247" />
      <relation from="o37242" id="r2860" name="part_of" negation="false" src="d0_s8" to="o37248" />
      <relation from="o37243" id="r2861" name="part_of" negation="false" src="d0_s8" to="o37248" />
      <relation from="o37244" id="r2862" name="part_of" negation="false" src="d0_s8" to="o37248" />
      <relation from="o37245" id="r2863" name="part_of" negation="false" src="d0_s8" to="o37248" />
    </statement>
    <statement id="d0_s9">
      <text>Calyces whitish with green veins or green, sometimes purple, distally same color as bracts, sometimes with yellowish band below colored apices, 20–26 (–29) mm;</text>
      <biological_entity id="o37249" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="with " constraintid="o37251" is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="29" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37250" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o37251" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="true" modifier="sometimes" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="true" name="character" src="d0_s9" value="color" value_original="color" />
      </biological_entity>
      <biological_entity id="o37252" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o37253" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="true" modifier="sometimes" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="true" name="character" src="d0_s9" value="color" value_original="color" />
      </biological_entity>
      <biological_entity id="o37254" name="band" name_original="band" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o37255" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="below" name="coloration" src="d0_s9" value="colored" value_original="colored" />
      </biological_entity>
      <relation from="o37251" id="r2864" name="with" negation="false" src="d0_s9" to="o37252" />
      <relation from="o37251" id="r2865" name="with" negation="false" src="d0_s9" to="o37253" />
      <relation from="o37249" id="r2866" modifier="sometimes" name="with" negation="false" src="d0_s9" to="o37254" />
      <relation from="o37254" id="r2867" name="below colored" negation="false" src="d0_s9" to="o37255" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial clefts (8–) 13–16 (–19) mm, adaxial 7–12 (–14) mm, clefts 35–50% of calyx length, deeper than laterals, lateral 2–6 (–8) mm, 10–30% of calyx length;</text>
      <biological_entity constraint="abaxial" id="o37256" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="19" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o37257" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37258" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character constraint="than laterals" constraintid="o37259" is_modifier="false" name="coloration_or_size" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="35-50%" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37259" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lobes linear-lanceolate to lanceolate, apex obtuse to acute.</text>
      <biological_entity id="o37260" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s11" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o37261" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight to slightly curved, 24–38 mm;</text>
      <biological_entity id="o37262" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="straight" name="course" src="d0_s12" to="slightly curved" />
        <character char_type="range_value" from="24" from_unit="mm" name="some_measurement" src="d0_s12" to="38" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 14–21 mm;</text>
      <biological_entity id="o37263" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s13" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted from calyx, adaxially green, 11–16.5 (–18) mm;</text>
      <biological_entity id="o37264" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character char_type="range_value" from="16.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="18" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s14" to="16.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37265" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
      <relation from="o37264" id="r2868" name="exserted from" negation="false" src="d0_s14" to="o37265" />
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip green, reduced, visible or not through deep front cleft in calyx, 2 mm, 13% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o37266" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="visible" value_original="visible" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="not through deep not through deep front" />
        <character name="some_measurement" notes="" src="d0_s15" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o37267" name="front" name_original="front" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="not through" name="depth" src="d0_s15" value="deep" value_original="deep" />
        <character constraint="in calyx" constraintid="o37268" is_modifier="false" name="architecture_or_shape" src="d0_s15" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o37268" name="calyx" name_original="calyx" src="d0_s15" type="structure" />
      <biological_entity id="o37269" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o37266" id="r2869" name="through deep" negation="true" src="d0_s15" to="o37267" />
      <relation from="o37266" id="r2870" modifier="13%" name="as long as" negation="false" src="d0_s15" to="o37269" />
    </statement>
    <statement id="d0_s16">
      <text>teeth incurved, green, 1 mm. 2n = 48.</text>
      <biological_entity id="o37270" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o37271" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja dissitiflora is endemic to several mountain ranges in central and eastern Nevada, in the upper montane and lower subalpine zones. It has the deep abaxial calyx cleft of C. linariifolia and the stipitate-glandular, wavy-margined leaves of C. applegatei var. pinetorum. Based on morphological data, Holmgren suggested that it is an allopolyploid derived from hybridization of C. applegatei var. pinetorum and C. linariifolia. His proposal is plausible and should be further tested. Castilleja dissitiflora is a tetraploid, while both putative parental species have at least some diploid populations.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush slopes often rocky, montane to subalpine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush slopes" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–3300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>