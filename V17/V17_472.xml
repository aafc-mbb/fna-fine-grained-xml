<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">197</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">211</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Small" date="1898" rank="species">calycosus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 470. 1898</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species calycosus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>148.</number>
  <other_name type="common_name">Long-sepal beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o15505" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (40–) 60–120 cm, puberulent, at least proximally, slightly glaucous or not.</text>
      <biological_entity id="o15506" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="40" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="at-least proximally; proximally; slightly" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character name="pubescence" src="d0_s1" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal often withering by anthesis, not leathery, glabrous;</text>
      <biological_entity id="o15507" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 47–110 × 10–24 mm, blade oblanceolate, base tapered, margins entire or ± serrate, apex rounded to obtuse;</text>
      <biological_entity constraint="basal" id="o15508" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="by anthesis" is_modifier="false" modifier="often" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o15509" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o15510" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o15511" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o15512" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15513" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–7 pairs, sessile or proximals short-petiolate, (14–) 44–155 × (2–) 13–46 mm, blade lanceolate to ovate, proximals sometimes oblanceolate, base clasping or tapered, margins serrate, apex acute to acuminate.</text>
      <biological_entity constraint="cauline" id="o15514" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o15515" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_length" src="d0_s4" to="44" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="44" from_unit="mm" name="length" src="d0_s4" to="155" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s4" to="46" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15516" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
      </biological_entity>
      <biological_entity id="o15517" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o15518" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o15519" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15520" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, conic, (5–) 14–20 cm, axis sparsely puberulent and glandular-pubescent, rarely glabrous, verticillasters 2–5, cymes (1–) 5–15-flowered, 2 per node;</text>
      <biological_entity id="o15521" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="14" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="14" from_unit="cm" name="some_measurement" src="d0_s5" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15522" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15523" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o15524" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(1-)5-15-flowered" value_original="(1-)5-15-flowered" />
        <character constraint="per node" constraintid="o15525" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15525" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts ovate to lanceolate, sometimes linear, 13–80 × (2–) 5–32 mm, margins serrate, sometimes entire;</text>
      <biological_entity constraint="proximal" id="o15526" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="80" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s6" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="32" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15527" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels spreading to ascending, sparsely glandular-pubescent.</text>
      <biological_entity id="o15528" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="ascending" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o15529" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="ascending" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes lanceolate, 5–9 × 0.8–1.9 mm, apex acuminate, sparsely glandular-pubescent;</text>
      <biological_entity id="o15530" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o15531" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15532" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pale lavender to violet, with faint violet nectar guides, ventricose, 20–35 mm, glandular-pubescent externally, ± white-pubescent internally abaxially, tube 5–7 mm, throat abruptly inflated, 8–11 mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o15533" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15534" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s9" to="violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="more or less; internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-pubescent" value_original="white-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o15535" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="faint" value_original="faint" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o15536" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15537" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o15534" id="r1221" name="with" negation="false" src="d0_s9" to="o15535" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included or longer pair reaching orifice, pollen-sacs opposite, navicular, 1.2–1.4 mm, dehiscing completely, connective splitting, sides glabrous, rarely sparsely pubescent, hairs white, to 0.2 mm, sutures papillate;</text>
      <biological_entity id="o15538" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15539" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o15540" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <biological_entity id="o15541" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="distance" src="d0_s10" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o15542" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o15543" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15544" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15545" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o15539" id="r1222" name="reaching" negation="false" src="d0_s10" to="o15540" />
    </statement>
    <statement id="d0_s11">
      <text>staminode 11–13 mm, reaching orifice, 0.2–0.3 mm diam., tip straight to slightly recurved, distal 2–5 mm sparsely pubescent, hairs yellowish, to 1 mm;</text>
      <biological_entity id="o15546" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15547" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15548" name="orifice" name_original="orifice" src="d0_s11" type="structure" />
      <biological_entity id="o15549" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15550" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15551" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o15547" id="r1223" name="reaching" negation="false" src="d0_s11" to="o15548" />
    </statement>
    <statement id="d0_s12">
      <text>style 14–16 mm.</text>
      <biological_entity id="o15552" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15553" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s12" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 7–8 × 4–5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 96.</text>
      <biological_entity id="o15554" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15555" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon calycosus is concentrated in the Central Lowlands and Interior Low Plateaus; occurrences farther south and east, especially in New England, may be from introductions (F. W. Pennell 1935). The range lies almost entirely within that of P. digitalis, but historic ranges of both P. calycosus and P. digitalis are difficult to delimit due to both past and continuing introduction and spread of both species. Calyx lobe lengths to 12 mm are reported in the literature; those appear to be from fruiting calyces. Penstemon calycosus usually has glabrous anthers, but many specimens, especially from Illinois and Ohio, bear hairs on the adaxial surfaces of the anthers. Some populations from this region exhibit variation in vegetative (leaf size and bract shape and margins) and floral characters (calyx lobe shape and size, corolla color, and anther pubescence), suggesting possible hybridization with P. digitalis, an observation that would be consistent with the findings of A. C. Koelling (1964).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands, meadows, rocky slopes, stream banks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Conn., D.C., Ga., Ill., Ind., Ky., Maine, Md., Mass., Mich., Minn., Mo., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>