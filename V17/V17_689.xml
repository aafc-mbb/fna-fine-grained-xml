<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PLANTAGO</taxon_name>
    <taxon_name authority="Kunze ex Walpers" date="1843" rank="species">firma</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Actorum Acad. Caes. Leop. Carol. Nat. Cur.</publication_title>
      <place_in_publication>19, suppl. 1: 402. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus plantago;species firma</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Plantago</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="species">truncata</taxon_name>
    <taxon_name authority="(Kunze ex Walpers) Pilger" date="unknown" rank="subspecies">firma</taxon_name>
    <taxon_hierarchy>genus plantago;species truncata;subspecies firma</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Chilean plantain</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
      <biological_entity id="o33051" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots taproots, slender.</text>
      <biological_entity constraint="roots" id="o33052" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 0–10 mm.</text>
      <biological_entity id="o33053" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 20–50 × 2–15 mm;</text>
      <biological_entity id="o33054" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly obovate or narrowly elliptic, margins toothed, veins conspicuous, surfaces densely pilose or glabrate.</text>
      <biological_entity id="o33055" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o33056" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o33057" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o33058" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scapes 10–50 mm, hairy, hairs appressed or nearly patent.</text>
      <biological_entity id="o33059" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o33060" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s5" value="patent" value_original="patent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikes greenish or brownish, 40–60 mm, densely flowered;</text>
      <biological_entity id="o33061" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s6" to="60" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts triangular, 2–3.1 mm, length 0.7–1.1 times sepals.</text>
      <biological_entity id="o33062" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.1" to_unit="mm" />
        <character constraint="sepal" constraintid="o33063" is_modifier="false" name="length" src="d0_s7" value="0.7-1.1 times sepals" value_original="0.7-1.1 times sepals" />
      </biological_entity>
      <biological_entity id="o33063" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 1.8–2.8 mm;</text>
      <biological_entity id="o33064" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o33065" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s8" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla radially symmetric, lobes patent or erect, forming a beak, 1.7–3.1 mm, base obtuse;</text>
      <biological_entity id="o33066" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o33067" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o33068" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="patent" value_original="patent" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33069" name="beak" name_original="beak" src="d0_s9" type="structure" />
      <biological_entity id="o33070" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o33068" id="r2505" name="forming a" negation="false" src="d0_s9" to="o33069" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 4.</text>
      <biological_entity id="o33071" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33072" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 2, 1.5–2.2 mm, adaxial face flat.</text>
      <biological_entity id="o33073" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity constraint="adaxial" id="o33074" name="face" name_original="face" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33075" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plantago firma has been known in California, especially in Marin County, since at least 1896, but the most recent collection known was made in 1957.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; South America (Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>