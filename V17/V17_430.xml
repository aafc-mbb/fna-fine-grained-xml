<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Glabri</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="species">neomexicanus</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 172. 1913</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section glabri;species neomexicanus;</taxon_hierarchy>
  </taxon_identification>
  <number>117.</number>
  <other_name type="common_name">New Mexico beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, sometimes ascending, 30–63 cm, glabrous, not glaucous.</text>
      <biological_entity id="o39457" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="63" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, or basal absent, not leathery, glabrous or retrorsely hairy, not glaucous;</text>
      <biological_entity id="o39458" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 30–90 (–116) × 5–26 mm, blade oblanceolate, base tapered, margins entire, apex obtuse to acute;</text>
      <biological_entity id="o39459" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o39460" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o39461" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o39462" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o39463" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o39464" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 6–12 pairs, sessile, 25–113 × 3–13 mm, blade lanceolate to linear, base tapered to truncate, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o39465" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="12" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="113" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39466" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o39467" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s3" to="truncate" />
      </biological_entity>
      <biological_entity id="o39468" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous, sometimes interrupted, secund, 16–30 cm, axis glabrous, verticillasters 7–11, cymes 1–4-flowered, (1 or) 2 per node;</text>
      <biological_entity id="o39469" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s4" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o39470" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o39471" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="11" />
      </biological_entity>
      <biological_entity id="o39472" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-4-flowered" value_original="1-4-flowered" />
        <character constraint="per node" constraintid="o39473" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o39473" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate to linear, 10–72 × 1–12 mm;</text>
      <biological_entity constraint="proximal" id="o39474" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="72" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glabrous.</text>
      <biological_entity id="o39475" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o39476" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes lanceolate to elliptic, 3–5 × 2–2.6 mm, glabrous;</text>
      <biological_entity id="o39477" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o39478" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla lavender to violet, with reddish purple nectar guides, ventricose, 26–34 mm, glabrous externally, sparsely to densely white-villous internally abaxially, tube 7–8 mm, throat gradually to abruptly inflated, not constricted at orifice, 7–10 mm diam., rounded abaxially;</text>
      <biological_entity id="o39479" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o39480" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="26" from_unit="mm" name="some_measurement" src="d0_s8" to="34" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely to densely; internally abaxially; abaxially" name="pubescence" src="d0_s8" value="white-villous" value_original="white-villous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o39481" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o39482" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39483" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually to abruptly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o39484" is_modifier="false" modifier="not" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" notes="" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o39484" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o39480" id="r3048" name="with" negation="false" src="d0_s8" to="o39481" />
    </statement>
    <statement id="d0_s9">
      <text>stamens: longer pair exserted, pollen-sacs opposite, navicular, 1.4–1.9 mm, dehiscing completely, sides glabrous, sutures smooth or papillate;</text>
      <biological_entity id="o39485" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o39486" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s9" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="distance" src="d0_s9" to="1.9" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o39487" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o39488" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 14–16 mm, reaching orifice or exserted, 1–1.2 mm diam., tip straight, glabrous;</text>
      <biological_entity id="o39489" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o39490" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39491" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
      <biological_entity id="o39492" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o39490" id="r3049" name="reaching" negation="false" src="d0_s10" to="o39491" />
    </statement>
    <statement id="d0_s11">
      <text>style 14–18 mm.</text>
      <biological_entity id="o39493" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o39494" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s11" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 9–14 × 5–6 mm. 2n = 16.</text>
      <biological_entity id="o39495" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39496" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon neomexicanus is known from the Capitan, Sacramento, and White mountains in Lincoln and Otero counties. The species is also known from a single historic collection from near Colonia Garcia, Chihuahua. G. T. Nisbet and R. C. Jackson (1960) suggested that some plants from northern Lincoln and southern Torrance counties might be hybrids between P. neomexicanus and P. virgatus. Specimens from the vicinity of the Gallinas Mountains are referred here to P. neomexicanus.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes and clearings in pine, pine-spruce, and spruce-fir forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" constraint="in pine" />
        <character name="habitat" value="clearings" constraint="in pine" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="pine-spruce" />
        <character name="habitat" value="spruce-fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>