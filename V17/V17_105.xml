<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">29</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">LINARIA</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1768" rank="species">genistifolia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">genistifolia</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus linaria;species genistifolia;subspecies genistifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>2a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, reproducing vegetatively by rhizomes.</text>
      <biological_entity id="o38281" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o38282" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o38281" id="r2934" name="reproducing" negation="false" src="d0_s0" to="o38282" />
    </statement>
    <statement id="d0_s1">
      <text>Fertile stems usually erect, 18–60 cm;</text>
      <biological_entity id="o38283" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sterile stems usually absent.</text>
      <biological_entity id="o38284" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves of fertile stems: blade lanceolate or linear-lanceolate, flat, 5–45 (–85) × 2–10 (–35) mm, apex acute.</text>
      <biological_entity id="o38285" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o38286" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o38287" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="85" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="45" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38288" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o38285" id="r2935" name="part_of" negation="false" src="d0_s3" to="o38286" />
    </statement>
    <statement id="d0_s4">
      <text>Racemes 5–26-flowered, lax;</text>
      <biological_entity id="o38289" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-26-flowered" value_original="5-26-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate, 3–10 × 1–2 mm.</text>
      <biological_entity id="o38290" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels erect-patent to erect, 2–4 mm in flower, 3–8 mm in fruit.</text>
      <biological_entity id="o38291" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect-patent" name="orientation" src="d0_s6" to="erect" />
        <character char_type="range_value" constraint="in flower" constraintid="o38292" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o38293" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38292" name="flower" name_original="flower" src="d0_s6" type="structure" />
      <biological_entity id="o38293" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Calyx lobes ovatelanceolate to lanceolate, 2.5–5 × 0.7–1.5 mm in flower, 3–6 × 0.8–1.7 mm in fruit, apex acute.</text>
      <biological_entity constraint="calyx" id="o38294" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" constraint="in flower" constraintid="o38295" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38295" name="flower" name_original="flower" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" notes="" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o38296" from="0.8" from_unit="mm" name="width" notes="" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38296" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o38297" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas pale to bright-yellow, 16–20 (–23) mm;</text>
      <biological_entity id="o38298" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="bright-yellow" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="23" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube 4–5 mm wide, spurs straight, 7–9 (–10) mm, slightly shorter or subequal to rest of corolla, abaxial lip sinus 2–3 mm, adaxial lip sinus 2–3.5 mm.</text>
      <biological_entity id="o38299" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38300" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s9" value="shorter" value_original="shorter" />
        <character constraint="to rest" constraintid="o38301" is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o38301" name="rest" name_original="rest" src="d0_s9" type="structure" />
      <biological_entity id="o38302" name="corolla" name_original="corolla" src="d0_s9" type="structure" />
      <biological_entity constraint="lip" id="o38303" name="sinus" name_original="sinus" src="d0_s9" type="structure" constraint_original="abaxial lip">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lip" id="o38304" name="sinus" name_original="sinus" src="d0_s9" type="structure" constraint_original="adaxial lip">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <relation from="o38301" id="r2936" name="part_of" negation="false" src="d0_s9" to="o38302" />
    </statement>
    <statement id="d0_s10">
      <text>Styles simple;</text>
      <biological_entity id="o38305" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigma entire.</text>
      <biological_entity id="o38306" name="stigma" name_original="stigma" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules subglobular, 4–5 × 4–4.8 mm, glabrous;</text>
      <biological_entity id="o38307" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobular" value_original="subglobular" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="4.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>loculi equal.</text>
      <biological_entity id="o38308" name="loculus" name_original="loculi" src="d0_s13" type="structure">
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds black or blackish brown, subtrigonous or ± tetrahedral, 0.8–1.2 (–1.4) × 0.6–1 mm, with longitudinal marginal ridges and anastomosed ridges or tubercles on faces;</text>
      <biological_entity id="o38309" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="subtrigonous" value_original="subtrigonous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="tetrahedral" value_original="tetrahedral" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s14" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o38310" name="ridge" name_original="ridges" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o38311" name="ridge" name_original="ridges" src="d0_s14" type="structure" />
      <biological_entity id="o38312" name="tubercle" name_original="tubercles" src="d0_s14" type="structure" />
      <biological_entity id="o38313" name="face" name_original="faces" src="d0_s14" type="structure" />
      <relation from="o38309" id="r2937" name="with" negation="false" src="d0_s14" to="o38310" />
      <relation from="o38309" id="r2938" name="anastomosed ridges or tubercles" negation="false" src="d0_s14" to="o38311" />
      <relation from="o38309" id="r2939" name="anastomosed ridges or tubercles" negation="false" src="d0_s14" to="o38312" />
      <relation from="o38309" id="r2940" name="on" negation="false" src="d0_s14" to="o38313" />
    </statement>
    <statement id="d0_s15">
      <text>wing absent.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 12 (Europe).</text>
      <biological_entity id="o38314" name="wing" name_original="wing" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o38315" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Collections of subsp. genistifolia on ballast are known from New York (pre-1900) and Oregon (1912).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, disturbed places.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mass., Minn., N.Y., Ohio, Oreg., Utah, Wash.; se Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="se Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>