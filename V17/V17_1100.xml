<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">624</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="mention_page">581</other_info_on_meta>
    <other_info_on_meta type="mention_page">582</other_info_on_meta>
    <other_info_on_meta type="mention_page">629</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Pennell" date="1947" rank="species">litoralis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>99: 183. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species litoralis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">affinis</taxon_name>
    <taxon_name authority="(Pennell) T. I. Chuang &amp; Heckard" date="unknown" rank="subspecies">litoralis</taxon_name>
    <taxon_hierarchy>genus castilleja;species affinis;subspecies litoralis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Elmer" date="unknown" rank="species">wightii</taxon_name>
    <taxon_name authority="(Pennell) Munz" date="unknown" rank="subspecies">litoralis</taxon_name>
    <taxon_hierarchy>genus c.;species wightii;subspecies litoralis</taxon_hierarchy>
  </taxon_identification>
  <number>62.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1–9 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o15116" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="9" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o15117" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o15116" id="r1180" name="with" negation="false" src="d0_s2" to="o15117" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, usually decumbent proximally, becoming ascending-erect, sometimes ascending, branched, sometimes with small, leafy axillary shoots, glabrate or ± pubescent distally, hairs sparse to moderately dense, spreading to ± appressed, short, soft, sometimes mixed with short-glandular ones below inflorescence.</text>
      <biological_entity id="o15118" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" modifier="usually; proximally" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s3" value="ascending-erect" value_original="ascending-erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="more or less; distally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o15119" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o15120" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s3" to="moderately dense" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="more or less appressed" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character constraint="with short-glandular ones" constraintid="o15121" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="short-glandular" id="o15121" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o15122" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <relation from="o15118" id="r1181" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o15119" />
      <relation from="o15121" id="r1182" name="below" negation="false" src="d0_s3" to="o15122" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green, lanceolate to oblong or narrowly ovate, (0.5–) 3–8 cm, sometimes thickened, not fleshy, margins plane, sometimes ± wavy, flat to involute, 0 (–3) -lobed, apex acute to rounded;</text>
      <biological_entity id="o15123" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblong or narrowly ovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o15124" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s4" to="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0(-3)-lobed" value_original="0(-3)-lobed" />
      </biological_entity>
      <biological_entity id="o15125" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending or spreading, linear, narrowly lanceolate to oblong or triangular, short, apex acute to obtuse.</text>
      <biological_entity id="o15126" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s5" to="oblong or triangular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o15127" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2.5–21 × 3–5 cm;</text>
      <biological_entity id="o15128" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="21" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally green, distally bright red to crimson or orange-red, sometimes orange or pale yellow-orange, oblong to narrowly ovate or narrowly obovate, (0–) 3–5-lobed, sometimes with a pair of small teeth;</text>
      <biological_entity id="o15129" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character char_type="range_value" from="distally bright red" name="coloration" src="d0_s7" to="crimson or orange-red" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale yellow-orange" value_original="pale yellow-orange" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="narrowly ovate or narrowly obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="(0-)3-5-lobed" value_original="(0-)3-5-lobed" />
      </biological_entity>
      <biological_entity id="o15130" name="pair" name_original="pair" src="d0_s7" type="structure" />
      <biological_entity id="o15131" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
      </biological_entity>
      <relation from="o15129" id="r1183" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o15130" />
      <relation from="o15130" id="r1184" name="part_of" negation="false" src="d0_s7" to="o15131" />
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending, linear to oblong, medium length, arising in middle 1/3, central lobe apex obtuse to rounded or truncate, lateral ones ± acute.</text>
      <biological_entity id="o15132" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="oblong" />
        <character is_modifier="false" name="length" src="d0_s8" value="medium" value_original="medium" />
        <character constraint="in middle 1/3" constraintid="o15133" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="middle" id="o15133" name="1/3" name_original="1/3" src="d0_s8" type="structure" />
      <biological_entity constraint="lobe" id="o15134" name="apex" name_original="apex" src="d0_s8" type="structure" constraint_original="central lobe">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded or truncate" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0–6 mm.</text>
      <biological_entity id="o15135" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Calyces colored as bracts, 17–25 (–30) mm;</text>
      <biological_entity id="o15136" name="calyx" name_original="calyces" src="d0_s10" type="structure">
        <character constraint="as bracts" constraintid="o15137" is_modifier="false" name="coloration" src="d0_s10" value="colored" value_original="colored" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s10" to="30" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15137" name="bract" name_original="bracts" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>abaxial and adaxial clefts (5–) 7–15 (–18) mm, 33–55% of calyx length, deeper than laterals, lateral 1–3 (–5) mm, 5–10% of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o15138" name="cleft" name_original="clefts" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="18" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character constraint="than laterals" constraintid="o15139" is_modifier="false" name="coloration_or_size" src="d0_s11" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="33-55%" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15139" name="lateral" name_original="laterals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lobes broadly triangular to oblong, apex obtuse to acute or rounded.</text>
      <biological_entity id="o15140" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="broadly triangular" name="shape" src="d0_s12" to="oblong" />
      </biological_entity>
      <biological_entity id="o15141" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="acute or rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Corollas straight or slightly curved, 23–38 (–40) mm;</text>
      <biological_entity id="o15142" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character char_type="range_value" from="38" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="40" to_unit="mm" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s13" to="38" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tube 10–20 mm;</text>
      <biological_entity id="o15143" name="tube" name_original="tube" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip often visible through front cleft, very rarely almost exserted, beak exserted;</text>
      <biological_entity constraint="abaxial" id="o15144" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character constraint="through front" constraintid="o15145" is_modifier="false" modifier="often" name="prominence" src="d0_s15" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="very rarely almost" name="position" notes="" src="d0_s15" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o15145" name="front" name_original="front" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o15146" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak adaxially green or yellowish, 10–16 mm, surface inconspicuously puberulent;</text>
      <biological_entity id="o15147" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15148" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="inconspicuously" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>abaxial lip ascending, green, reduced, 1–2.5 mm, 10–20% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o15149" name="lip" name_original="lip" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s17" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15150" name="beak" name_original="beak" src="d0_s17" type="structure" />
      <relation from="o15149" id="r1185" modifier="10-20%" name="as long as" negation="false" src="d0_s17" to="o15150" />
    </statement>
    <statement id="d0_s18">
      <text>teeth erect or incurved, green or white, 1–2 mm. 2n = 120, 144.</text>
      <biological_entity id="o15151" name="tooth" name_original="teeth" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15152" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="120" value_original="120" />
        <character name="quantity" src="d0_s18" value="144" value_original="144" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja litoralis never ranges more than one to two kilometers from the sea, from Humboldt County, California, north to Pacific County, Washington, near the mouth of the Columbia River. It is a high polyploid complex, possibly incorporating the genomes of several species, including C. affinis, C. miniata, and possibly C. hispida. The coastal C. miniata var. dixonii is very similar ecologically and morphologically but replaces C. litoralis from southwestern Washington to southern British Columbia. Compared to C. litoralis, C. miniata var. dixonii usually has somewhat longer corollas and corolla beaks, the latter with a more conspicuously puberulent surface and deeper lateral calyx clefts. Castilleja litoralis has been included as a subspecies of C. affinis by some (for example, M. Wetherwax et al. 2012), but the morphological resemblance to that species is far more tenuous than it is to C. miniata var. dixonii. Considering their very similar morphologies, along with the fact that both C. litoralis (2n = 120, 144) and C. miniata var. dixonii (2n = 96, 144) apparently combine multiple genomes, strongly suggest that they would best be treated as a single entity. Should they be combined at the species level following additional research, the name C. dixonii has priority.</discussion>
  <discussion>Castilleja litoralis is often associated with salal, Gaultheria shallon, on which it is likely parasitic.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep rocky slopes, headlands, ledges, sea cliffs, coastal scrub, dune swales, roadcuts.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep rocky slopes" />
        <character name="habitat" value="headlands" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="sea cliffs" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="dune swales" />
        <character name="habitat" value="roadcuts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>