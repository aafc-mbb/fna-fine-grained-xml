<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">410</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">403</other_info_on_meta>
    <other_info_on_meta type="mention_page">408</other_info_on_meta>
    <other_info_on_meta type="mention_page">409</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="2012" rank="species">corallina</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 44. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species corallina</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Greene" date="1896" rank="species">corallinus</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>4: 21. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species corallinus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">minusculus</taxon_name>
    <taxon_hierarchy>genus m.;species minusculus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Regel" date="unknown" rank="species">tilingii</taxon_name>
    <taxon_name authority="(Greene) A. L. Grant" date="unknown" rank="variety">corallinus</taxon_name>
    <taxon_hierarchy>genus m.;species tilingii;variety corallinus</taxon_hierarchy>
  </taxon_identification>
  <number>61.</number>
  <other_name type="common_name">Coralline monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, rhizomatous, rhizomes often forming a mass, usually branching, filiform.</text>
      <biological_entity id="o31668" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o31669" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s0" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o31670" name="mass" name_original="mass" src="d0_s0" type="structure" />
      <relation from="o31669" id="r2402" name="forming a" negation="false" src="d0_s0" to="o31670" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect to ascending-erect, few-branched, 6–25 (–38) cm, moderately hirsute to hirtellous, hairs deflexed.</text>
      <biological_entity id="o31671" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="usually erect" name="orientation" src="d0_s1" to="ascending-erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="38" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="moderately hirsute" name="pubescence" src="d0_s1" to="hirtellous" />
      </biological_entity>
      <biological_entity id="o31672" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="deflexed" value_original="deflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, becoming larger distally or even-sized;</text>
      <biological_entity id="o31673" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="becoming; distally" name="size" src="d0_s2" value="larger" value_original="larger" />
        <character is_modifier="false" name="size" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0 mm or proximals 1–15 mm;</text>
      <biological_entity id="o31674" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="0" value_original="0" />
      </biological_entity>
      <biological_entity id="o31675" name="proximal" name_original="proximals" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade palmately 5-veined, ovate to broadly ovate, 15–45 mm, base mostly truncate to shallowly cordate, margins sharply dentate-serrate, apex obtuse, surfaces hirtellous to softly hirsute, hairs ascending, straight, dull gray, sharp-pointed, thick-walled, eglandular.</text>
      <biological_entity id="o31676" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="5-veined" value_original="5-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly ovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31677" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="mostly truncate" name="shape" src="d0_s4" to="shallowly cordate" />
      </biological_entity>
      <biological_entity id="o31678" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="dentate-serrate" value_original="dentate-serrate" />
      </biological_entity>
      <biological_entity id="o31679" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o31680" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s4" to="softly hirsute" />
      </biological_entity>
      <biological_entity id="o31681" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp-pointed" value_original="sharp-pointed" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 1–3 (–6), commonly solitary or from distal nodes.</text>
      <biological_entity constraint="distal" id="o31683" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o31682" id="r2403" name="from" negation="false" src="d0_s5" to="o31683" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels (10–) 25–75 mm, glabrous or puberulent proximally, hairs stipitate-glandular.</text>
      <biological_entity id="o31682" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="commonly" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="from distal nodes" value_original="from distal nodes" />
      </biological_entity>
      <biological_entity id="o31684" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o31685" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s6" to="75" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o31682" id="r2404" name="fruiting" negation="false" src="d0_s6" to="o31684" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces sometimes purple-spotted, broadly cylindric-campanulate, inflated, sagittally compressed, 11–15 mm, glabrous, throat not closing, proximal lobe pair slightly upcurving.</text>
      <biological_entity id="o31686" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o31687" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o31688" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="cylindric-campanulate" value_original="cylindric-campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="sagittally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31689" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="condition" src="d0_s7" value="closing" value_original="closing" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31690" name="lobe" name_original="lobe" src="d0_s7" type="structure" />
      <relation from="o31686" id="r2405" name="fruiting" negation="false" src="d0_s7" to="o31687" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, red-spotted, bilaterally symmetric, bilabiate;</text>
      <biological_entity id="o31691" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-spotted" value_original="red-spotted" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube-throat narrowly funnelform to broadly cylindric, 13–20 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o31692" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="13" from_unit="mm" name="distance" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o31693" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o31692" id="r2406" name="exserted beyond" negation="false" src="d0_s9" to="o31693" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 12–22 mm.</text>
      <biological_entity id="o31694" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s10" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles sparsely hirtellous.</text>
      <biological_entity id="o31695" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o31696" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, stipitate, 7–10 mm. 2n = 48, 56.</text>
      <biological_entity id="o31697" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31698" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="48" value_original="48" />
        <character name="quantity" src="d0_s13" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe corallina is a morphologically consistent entity that occurs only in the Sierra Nevada of California and adjacent Nevada (Washoe County and Carson City). Its chromosome number is reported as 2n = 48 and 56, compared to 2n = 28 and 56 in E. tilingii; identities of the E. corallina vouchers should be rechecked and additional counts made, since the occurrence of such wide dysploidy seems unlikely. Compared to the leaf blades of E. tilingii in the strict sense, those of E. corallina are relatively broader (broadly ovate to orbicular-ovate), the plants generally taller, and long-pedicellate flowers occasionally are produced from mid stem or even proximal nodes. The hirsutulous to hirsute vestiture of eglandular hairs on both leaf surfaces is a reliably diagnostic feature and usually easily observed with a 10× lens.</discussion>
  <discussion>Some plants of Erythranthe corallina from San Bernardino County, California, produce decumbent-ascending stems (4–10 cm) and ovate-triangular leaves (blade 5–10 × 3–6 mm), but the dense system of filiform rhizomes, flowers one to three, and hirtellous foliar vestiture serve to identify them.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Creek banks, moraine water courses, bogs, marshes, wet meadows, roadside ditches.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="creek banks" />
        <character name="habitat" value="moraine water courses" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="ditches" modifier="roadside" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1400–)1700–2700(–3000) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1700" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>