<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">252</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">palmeri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 379. 1868</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species palmeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>235.</number>
  <other_name type="common_name">Palmer’s or scented beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o20185" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, 43–140 cm, glaucous.</text>
      <biological_entity id="o20186" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="43" from_unit="cm" name="some_measurement" src="d0_s1" to="140" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline (28–) 52–120 (–145) × (5–) 25–50 (–60) mm, blade ovate, base tapered, margins coarsely dentate, apex obtuse to acute;</text>
      <biological_entity id="o20187" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="28" from_unit="mm" name="atypical_length" src="d0_s2" to="52" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="145" to_unit="mm" />
        <character char_type="range_value" from="52" from_unit="mm" name="length" src="d0_s2" to="120" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s2" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="60" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s2" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20188" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o20189" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o20190" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o20191" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 5–8 pairs, sessile or connate-perfoliate, 25–130 × 7–45 mm, blade ovate to triangular, base of distals auriculate-clasping or connate-perfoliate, margins coarsely dentate, distals sometimes entire, apex acute.</text>
      <biological_entity id="o20192" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o20193" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="connate-perfoliate" value_original="connate-perfoliate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="130" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s3" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20194" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="triangular" />
      </biological_entity>
      <biological_entity id="o20195" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="auriculate-clasping" value_original="auriculate-clasping" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="connate-perfoliate" value_original="connate-perfoliate" />
      </biological_entity>
      <biological_entity id="o20196" name="distal" name_original="distals" src="d0_s3" type="structure" />
      <biological_entity id="o20197" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o20198" name="distal" name_original="distals" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20199" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o20195" id="r1544" name="part_of" negation="false" src="d0_s3" to="o20196" />
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted, rarely continuous, secund, 7–63 cm, axis glabrous or glandular-pubescent, verticillasters (4–) 7–17, cymes (1 or) 2–5-flowered;</text>
      <biological_entity id="o20200" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="63" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20201" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o20202" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s4" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="17" />
      </biological_entity>
      <biological_entity id="o20203" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-5-flowered" value_original="2-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts ovate to lanceolate, 8–40 × 4–38 mm;</text>
      <biological_entity constraint="proximal" id="o20204" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="38" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels ascending to erect, glabrous or glandular-pubescent.</text>
      <biological_entity id="o20205" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o20206" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate, (3.4–) 4.2–6 (–7.5) × 1.9–3.6 mm, glabrous or glandular-pubescent;</text>
      <biological_entity id="o20207" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o20208" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="atypical_length" src="d0_s7" to="4.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="width" src="d0_s7" to="3.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla pale-pink to white or pinkish lavender, with reddish purple nectar guides, strongly bilabiate, ventricose-ampliate, 25–38 mm, glandular-pubescent externally, glandular-pubescent internally, sometimes also sparsely white-lanate abaxially, tube 4–8 mm, length 1–1.4 times calyx lobes, throat abruptly inflated, constricted at orifice, 12–21 mm diam., rounded abaxially;</text>
      <biological_entity id="o20209" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o20210" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s8" to="white or pinkish lavender" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="size" src="d0_s8" value="ventricose-ampliate" value_original="ventricose-ampliate" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s8" to="38" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes; sparsely; abaxially" name="pubescence" src="d0_s8" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o20211" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o20212" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character constraint="lobe" constraintid="o20213" is_modifier="false" name="length" src="d0_s8" value="1-1.4 times calyx lobes" value_original="1-1.4 times calyx lobes" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o20213" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o20214" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o20215" is_modifier="false" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" notes="" src="d0_s8" to="21" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20215" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o20210" id="r1545" name="with" negation="false" src="d0_s8" to="o20211" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included or longer pair reaching orifice, filaments of shorter pair glandular-puberulent proximally, pollen-sacs navicular, 1.6–2.4 mm, sutures smooth;</text>
      <biological_entity id="o20216" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20217" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o20218" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o20219" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o20220" name="pair" name_original="pair" src="d0_s9" type="structure" />
      <biological_entity id="o20221" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="distance" src="d0_s9" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20222" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o20217" id="r1546" name="reaching" negation="false" src="d0_s9" to="o20218" />
      <relation from="o20219" id="r1547" name="part_of" negation="false" src="d0_s9" to="o20220" />
    </statement>
    <statement id="d0_s10">
      <text>staminode 25–30 mm, exserted, 0.9–1.1 mm diam., tip strongly recurved to coiled, distal 8–10 mm, and especially tip, moderately to densely lanate, hairs yellow, to 3.5 mm, proximal 6–8 mm glandular-puberulent;</text>
      <biological_entity id="o20223" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o20224" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="diameter" src="d0_s10" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20225" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20226" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" notes="" src="d0_s10" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity id="o20227" name="tip" name_original="tip" src="d0_s10" type="structure" />
      <biological_entity id="o20228" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20229" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 19–23 mm, glabrous or sparsely glandular-pubescent proximally.</text>
      <biological_entity id="o20230" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20231" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="some_measurement" src="d0_s11" to="23" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s11" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 11–16 × 6–8 mm, sparsely glandular-pubescent distally.</text>
      <biological_entity id="o20232" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s12" to="16" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s12" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, N.Mex., Nev., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon ×bryantiae D. D. Keck, a hybrid between P. palmeri and P. spectabilis, was described based on plants that volunteered in the Rancho Santa Ana Botanic Garden in 1935 (D. D. Keck 1937b). Penstemon ×mirus A. Nelson, a putative hybrid between P. eatonii and P. palmeri, has been reported from Arizona (A. Nelson 1938).</discussion>
  <discussion>A poultice prepared from Penstemon palmeri is applied to snakebites by the Kayenta Navajo of northeastern Arizona (D. E. Moerman 1998).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla tubes 7–8 mm; distal cauline leaves sessile, sometimes connate-perfoliate.</description>
      <determination>235c. Penstemon palmeri var. macranthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla tubes 4–6 mm; distal cauline leaves connate-perfoliate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peduncles, pedicels, and calyx lobes glandular-pubescent; pollen sacs 1.8–2.4 mm.</description>
      <determination>235a. Penstemon palmeri var. palmeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peduncles, pedicels, and calyx lobes glabrous; pollen sacs 1.6–2(–2.2) mm.</description>
      <determination>235b. Penstemon palmeri var. eglandulosus</determination>
    </key_statement>
  </key>
</bio:treatment>