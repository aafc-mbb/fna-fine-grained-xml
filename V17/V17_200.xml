<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="genus">COLLINSIA</taxon_name>
    <taxon_name authority="Herder" date="1868" rank="species">corymbosa</taxon_name>
    <place_of_publication>
      <publication_title>Gartenflora</publication_title>
      <place_in_publication>1868: 33, plate 568. 1868</place_in_publication>
      <publication_title>Inde× Seminum (St. Petersburg)</publication_title>
      <place_in_publication>1866: 32. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus collinsia;species corymbosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>17.</number>
  <other_name type="common_name">Round-headed Chinese-houses</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals 5–25 cm.</text>
      <biological_entity id="o12028" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent.</text>
      <biological_entity id="o12029" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades lanceolate to ovate, margins crenate, abaxial surface glabrous, adaxial subglabrous or finely gray-hairy.</text>
      <biological_entity id="o12030" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
      </biological_entity>
      <biological_entity id="o12031" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12032" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12033" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="gray-hairy" value_original="gray-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences sparsely and finely glandular;</text>
      <biological_entity id="o12034" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>whorl 1 per branch;</text>
      <biological_entity id="o12035" name="whorl" name_original="whorl" src="d0_s4" type="structure">
        <character constraint="per branch" constraintid="o12036" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12036" name="branch" name_original="branch" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>nodes (1–) 3–10-flowered;</text>
      <biological_entity id="o12037" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(1-)3-10-flowered" value_original="(1-)3-10-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flowers crowded;</text>
      <biological_entity id="o12038" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distalmost bracts ovate, 5–9 mm.</text>
      <biological_entity constraint="distalmost" id="o12039" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels ascending to spreading, shorter than calyx, not or scarcely visible.</text>
      <biological_entity id="o12040" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="spreading" />
        <character constraint="than calyx" constraintid="o12041" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="not; scarcely" name="prominence" src="d0_s8" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o12041" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: caly× lobes oblong to ovate, surpassing capsule, ape× rounded;</text>
      <biological_entity id="o12042" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="ovate" />
      </biological_entity>
      <biological_entity id="o12043" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
      <relation from="o12042" id="r959" name="surpassing" negation="false" src="d0_s9" to="o12043" />
    </statement>
    <statement id="d0_s10">
      <text>corolla usually whitish, 14–22 mm, wings sparsely and finely glandular, not hairy;</text>
      <biological_entity id="o12044" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12045" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s10" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12046" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>banner length 0.1–0.3 (–0.4) times wings, lobe base without folds, reflexed portion 1 mm, shorter than basal portion, brownish, not red-banded;</text>
      <biological_entity id="o12047" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12048" name="banner" name_original="banner" src="d0_s11" type="structure">
        <character constraint="wing" constraintid="o12049" is_modifier="false" name="length" src="d0_s11" value="0.1-0.3(-0.4) times wings" value_original="0.1-0.3(-0.4) times wings" />
      </biological_entity>
      <biological_entity id="o12049" name="wing" name_original="wings" src="d0_s11" type="structure" />
      <biological_entity constraint="lobe" id="o12050" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o12051" name="fold" name_original="folds" src="d0_s11" type="structure" />
      <biological_entity id="o12052" name="portion" name_original="portion" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
        <character constraint="than basal portion" constraintid="o12053" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s11" value="red-banded" value_original="red-banded" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12053" name="portion" name_original="portion" src="d0_s11" type="structure" />
      <relation from="o12050" id="r960" name="without" negation="false" src="d0_s11" to="o12051" />
    </statement>
    <statement id="d0_s12">
      <text>stamens: filaments hairy, basal spur 0.</text>
      <biological_entity id="o12054" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o12055" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12056" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 8–16, oblong to oval, 2–2.5 mm, margins thickened, inrolled.</text>
      <biological_entity id="o12057" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s13" to="16" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s13" to="oval" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o12058" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s13" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12059" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Collinsia corymbosa is known from Mendocino County. Specimens from other sites identified as C. corymbosa are C. bartsiifolia var. hirsuta. A phylogenetic study using DNA showed evidence of a close relationship between C. corymbosa and C. bartsiifolia (B. G. Baldwin et al. 2011).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal sand dunes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal sand" />
        <character name="habitat" value="dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>