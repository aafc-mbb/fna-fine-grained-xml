<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Eastwood" date="1906" rank="species">copelandii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>41: 288, fig. 2. 1906</place_in_publication>
      <other_info_on_pub>(as copelandi)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species copelandii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Copeland's speedwell</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o37628" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, unbranched, 5–15 cm, densely glandular-hairy.</text>
      <biological_entity id="o37629" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade oblongelliptic, (5–) 10–15 (–35) × 4–8 mm, base cuneate, margins entire, apex short-acuminate, surfaces hairy.</text>
      <biological_entity id="o37630" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o37631" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s2" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37632" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o37633" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o37634" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <biological_entity id="o37635" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1, terminal, distalmost leaves often with 1 or 2 axillary flowers, 10–80 mm, (3–) 5–15-flowered, axis glandular-hairy;</text>
      <biological_entity id="o37636" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o37637" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="80" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="(3-)5-15-flowered" value_original="(3-)5-15-flowered" />
      </biological_entity>
      <biological_entity id="o37638" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o37639" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <relation from="o37637" id="r2896" name="with" negation="false" src="d0_s3" to="o37638" />
    </statement>
    <statement id="d0_s4">
      <text>bracts lanceolate, 3–5 mm.</text>
      <biological_entity id="o37640" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels erect, 5–8 mm, equal to ± longer than subtending bract, densely glandular-hairy.</text>
      <biological_entity id="o37641" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character constraint="than subtending bract" constraintid="o37642" is_modifier="false" name="length_or_size" src="d0_s5" value="more or less longer" value_original="more or less longer" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o37642" name="bract" name_original="bract" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 4 (or 5) -lobed, lobes (1–) 2–3 mm, apex obtuse, glandular-hairy;</text>
      <biological_entity id="o37643" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o37644" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="4-lobed" value_original="4-lobed" />
      </biological_entity>
      <biological_entity id="o37645" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37646" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla pale blue to purple, rotate, 8–10 mm diam.;</text>
      <biological_entity id="o37647" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o37648" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale blue" name="coloration" src="d0_s7" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" value_original="rotate" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 4–5 mm;</text>
      <biological_entity id="o37649" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o37650" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 7 mm.</text>
      <biological_entity id="o37651" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o37652" name="style" name_original="style" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules compressed in cross-section, broadly oblong, 5–6 × 3.5 mm, longer than wide, apex emarginate, glandular-hairy.</text>
      <biological_entity id="o37653" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="in cross-section" constraintid="o37654" is_modifier="false" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="3.5" value_original="3.5" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
      <biological_entity id="o37654" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o37655" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds number unknown, brown, ovoid, flat, 1–1.2 × 0.7–1.1 mm, thickness and texture unknown.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 18.</text>
      <biological_entity id="o37656" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="texture" src="d0_s11" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="texture" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o37657" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Veronica copelandii is sister to V. cusickii in the phylogenetic analysis by D. C. Albach et al. (2006). It occurs in the Klamath Ranges of northwestern California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine meadows, alpine slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine meadows" />
        <character name="habitat" value="alpine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>