<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Ambigui</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">ambiguus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>2: 228. 1827</place_in_publication>
      <other_info_on_pub>(as Pentstemon ambiguum)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section ambigui;species ambiguus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Gilia beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (20–) 30–40 (–60) cm, glabrous or scabrous proximally.</text>
      <biological_entity id="o8364" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves (7–) 10–25 pairs, (3–) 5–30 (–40) × 0.5–1 (–2.5) mm, blade base tapered, margins glabrous or scabrous, apex acuminate to mucronate.</text>
      <biological_entity id="o8365" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s1" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s1" to="25" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s1" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="blade" id="o8366" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o8367" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o8368" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s1" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Thyrses 6–15 cm, verticillasters 6–10, cymes 1–3-flowered;</text>
      <biological_entity id="o8369" name="thyrse" name_original="thyrses" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8370" name="verticillaster" name_original="verticillasters" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="10" />
      </biological_entity>
      <biological_entity id="o8371" name="cyme" name_original="cymes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal bracts linear, 6–27 (–33) × 0.3–1.5 mm;</text>
      <biological_entity constraint="proximal" id="o8372" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="33" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="27" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>peduncles and pedicels glabrous or scabrous.</text>
      <biological_entity id="o8373" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o8374" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx lobes ovate, 1.5–3.5 × 1–1.5 mm;</text>
      <biological_entity id="o8375" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="calyx" id="o8376" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla pink (limb milky pink or milky white), lined internally abaxially with reddish purple nectar guides, salverform, (14–) 16–22 (–28) mm, glandular-pubescent internally in 2 lines on abaxial surface, tube 3–4 mm, throat 3–5 mm diam.;</text>
      <biological_entity id="o8377" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o8378" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character constraint="with nectar guides" constraintid="o8379" is_modifier="false" modifier="internally abaxially" name="architecture" src="d0_s6" value="lined" value_original="lined" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="salverform" value_original="salverform" />
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="28" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s6" to="22" to_unit="mm" />
        <character constraint="in lines" constraintid="o8380" is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o8379" name="guide" name_original="guides" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o8380" name="line" name_original="lines" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8381" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <biological_entity id="o8382" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8383" name="throat" name_original="throat" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o8380" id="r687" name="on" negation="false" src="d0_s6" to="o8381" />
    </statement>
    <statement id="d0_s7">
      <text>stamens included, pollen-sacs 0.5–0.6 mm;</text>
      <biological_entity id="o8384" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o8385" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o8386" name="pollen-sac" name_original="pollen-sacs" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminode 7–9 mm;</text>
      <biological_entity id="o8387" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o8388" name="staminode" name_original="staminode" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 8–10 mm.</text>
      <biological_entity id="o8389" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8390" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 6–9 × 3–5 mm.</text>
      <biological_entity id="o8391" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., N.Mex., Nebr., Nev., Okla., Tex., Utah; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>G. T. Nisbet and R. C. Jackson (1960) reported natural hybrids between Penstemon ambiguus and P. thurberi from Lincoln and Socorro counties, New Mexico.</discussion>
  <discussion>Some Native American tribes used Penstemon ambiguus as a drug or ceremonial plant (D. E. Moerman 1998).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems scabrous proximally; leaf blade margins scabrous.</description>
      <determination>11a. Penstemon ambiguus var. ambiguus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous; leaf blade margins glabrous or scabrous.</description>
      <determination>11b. Penstemon ambiguus var. laevissimus</determination>
    </key_statement>
  </key>
</bio:treatment>