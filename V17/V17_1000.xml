<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">560</other_info_on_meta>
    <other_info_on_meta type="mention_page">561</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1819" rank="genus">DASISTOMA</taxon_name>
    <taxon_name authority="(Nuttall) Rafinesque" date="1837" rank="species">macrophyllum</taxon_name>
    <place_of_publication>
      <publication_title>New Fl.</publication_title>
      <place_in_publication>2: 67. 1837</place_in_publication>
      <other_info_on_pub>(as Dasistema macrophylla)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus dasistoma;species macrophyllum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Seymeria</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">macrophylla</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 49. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus seymeria;species macrophylla</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems branched distally, rarely unbranched, square to round, 80–200 cm.</text>
      <biological_entity id="o18967" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="square" name="shape" src="d0_s0" to="round" />
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal and cauline: petiole (5–) 15–100 mm;</text>
      <biological_entity id="o18968" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o18969" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s1" to="100" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade ovate to ovatelanceolate, 15–35 x 6–22 cm, segment margins irregularly serrate to irregularly dentate, surfaces scabrid-puberulent to pubescent;</text>
      <biological_entity id="o18970" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18971" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s2" to="22" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="segment" id="o18972" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="irregularly serrate" name="architecture_or_shape" src="d0_s2" to="irregularly dentate" />
      </biological_entity>
      <biological_entity id="o18973" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character char_type="range_value" from="scabrid-puberulent" name="pubescence" src="d0_s2" to="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal: petiole 2–20 (–35) mm;</text>
      <biological_entity constraint="distal" id="o18974" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o18975" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lanceolate to narrowly lanceolate, 7–21 x 2–14 cm, segment margins entire or crenate, irregularly serrate, or irregularly dentate, surfaces scabrid-puberulent to pubescent.</text>
      <biological_entity constraint="distal" id="o18976" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o18977" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s4" to="21" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="segment" id="o18978" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o18979" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="scabrid-puberulent" name="pubescence" src="d0_s4" to="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes 1–8, congested to interrupted, cylindric, 7–35 cm;</text>
      <biological_entity id="o18980" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="8" />
        <character char_type="range_value" from="congested" name="architecture" src="d0_s5" to="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s5" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts leafy, gradually smaller distally, 9–65 x 7–35 mm, margins entire or irregularly serrate to irregularly dentate, apex acute;</text>
      <biological_entity id="o18981" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s6" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="65" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18982" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o18983" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cymes 1-flowered.</text>
      <biological_entity id="o18984" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect, 1–4 mm, puberulent to retrorsely puberulent.</text>
      <biological_entity id="o18985" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s8" to="retrorsely puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx puberulent to pubescent, lobes (2–) 4–5.5 x 2.5–4.5 mm, margins entire, apex acute to obtuse, membranous;</text>
      <biological_entity id="o18986" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18987" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s9" to="pubescent" />
      </biological_entity>
      <biological_entity id="o18988" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18989" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18990" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="obtuse" />
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla 15–16 mm, glabrous externally, densely lanate internally;</text>
      <biological_entity id="o18991" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18992" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely; internally" name="pubescence" src="d0_s10" value="lanate" value_original="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tube 7–9 mm;</text>
      <biological_entity id="o18993" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18994" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>throat inflated, 5–7 mm diam.;</text>
      <biological_entity id="o18995" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18996" name="throat" name_original="throat" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lobes spreading, ovate-reniform, 6–7 mm;</text>
      <biological_entity id="o18997" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18998" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate-reniform" value_original="ovate-reniform" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens distinct, reaching orifice or barely exserted, pollen-sacs 2–2.8 mm.</text>
      <biological_entity id="o18999" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19000" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o19001" name="orifice" name_original="orifice" src="d0_s14" type="structure" />
      <biological_entity id="o19002" name="pollen-sac" name_original="pollen-sacs" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s14" to="2.8" to_unit="mm" />
      </biological_entity>
      <relation from="o19000" id="r1468" name="reaching" negation="false" src="d0_s14" to="o19001" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid to globular-ovoid, 8–12 x 7–9 mm, glabrous.</text>
      <biological_entity id="o19003" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="globular-ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s15" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds reticulate, 1.8–2.5 x 1–2 mm.</text>
      <biological_entity id="o19004" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s16" value="reticulate" value_original="reticulate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s16" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the greenhouse, Dasistoma macrophyllum formed haustorial connections with 18 species of gymnosperm and angiosperm trees, but it also could remain autotrophic (L. J. Musselman and W. F. Mann 1979); there is no evidence of autotrophy in the wild.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to mesic forests and woodlands, brushy tallgrass prairies, stream banks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to mesic forests" />
        <character name="habitat" value="dry to woodlands" />
        <character name="habitat" value="prairies" modifier="brushy tallgrass" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Ill., Ind., Iowa, Kans., Ky., La., Mich., Miss., Mo., Nebr., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>