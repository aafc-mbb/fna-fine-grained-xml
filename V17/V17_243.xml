<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="(Rafinesque) Pennell" date="1920" rank="subgenus">Dasanthera</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Erianthera</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray in A. Gray et al." date="1886" rank="species">lyallii</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 2(1): 440. 1886</place_in_publication>
      <other_info_on_pub>(as Pentstemon lyalli)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus dasanthera;section erianthera;species lyallii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">menziesii</taxon_name>
    <taxon_name authority="A. Gray" date="1862" rank="variety">lyallii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 76. 1862</place_in_publication>
      <other_info_on_pub>(as Pentstemon menziesii var. lyalli)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species menziesii;variety lyallii</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Lyall’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o35194" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (18–) 30–80 cm, puberulent or retrorsely hairy proximally, puberulent or glandular-pubescent distally, not glaucous.</text>
      <biological_entity id="o35195" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="18" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, 8–13 pairs, short-petiolate or sessile, 23–130 × 3–20 mm, blade lanceolate, rarely linear, base tapered, margins entire or remotely serrate, apex acute to acuminate, rarely obtuse, glabrous or puberulent to pubescent, not glaucous.</text>
      <biological_entity id="o35196" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="13" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="23" from_unit="mm" name="length" src="d0_s2" to="130" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35197" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o35198" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o35199" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o35200" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s2" to="pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Thyrses interrupted, ± secund, (3–) 6–15 (–28) cm, axis sparsely to moderately glandular-pubescent, verticillasters 3–7, cymes 2–7-flowered;</text>
      <biological_entity id="o35201" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="28" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o35202" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o35203" name="verticillaster" name_original="verticillasters" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity id="o35204" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-7-flowered" value_original="2-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts lanceolate to linear, (5–) 15–80 × (1–) 2–8 (–20) mm;</text>
      <biological_entity constraint="proximal" id="o35205" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s4" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncles and pedicels spreading or ascending, glandular-pubescent.</text>
      <biological_entity id="o35206" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o35207" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes lanceolate, 7–16 × 2–4 mm, glandular-pubescent;</text>
      <biological_entity id="o35208" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o35209" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="16" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla lavender to purple, unlined internally or lined with faint lavender nectar guides abaxially, nearly personate, funnelform, 35–46 mm, glabrous externally, moderately white-lanate internally abaxially, tube 8–14 mm, throat 8–12 mm diam.;</text>
      <biological_entity id="o35210" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o35211" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s7" to="purple" />
        <character constraint="with nectar guides" constraintid="o35212" is_modifier="false" modifier="internally" name="architecture" src="d0_s7" value="lined" value_original="lined" />
        <character is_modifier="false" modifier="nearly" name="architecture" notes="" src="d0_s7" value="personate" value_original="personate" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s7" to="46" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately; internally abaxially; abaxially" name="pubescence" src="d0_s7" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o35212" name="guide" name_original="guides" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="faint" value_original="faint" />
        <character is_modifier="true" name="coloration_or_odor" src="d0_s7" value="lavender" value_original="lavender" />
      </biological_entity>
      <biological_entity id="o35213" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35214" name="throat" name_original="throat" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens included, pollen-sacs 1.1–1.8 mm;</text>
      <biological_entity id="o35215" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o35216" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o35217" name="pollen-sac" name_original="pollen-sacs" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s8" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>staminode 10–13 mm, flattened distally, 0.2–0.3 mm diam., tip straight, glabrous;</text>
      <biological_entity id="o35218" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o35219" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35220" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 30–40 mm.</text>
      <biological_entity id="o35221" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o35222" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s10" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 10–14 × 5–7 mm.</text>
      <biological_entity id="o35223" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon lyallii is known from the Central, Northern, and Canadian Rocky mountains in southwestern Alberta, southeastern British Columbia, northern Idaho, northwestern Montana, and northeastern Washington.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, rock outcrops, gravel bars along streams.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="bars" modifier="gravel" constraint="along streams" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Idaho, Mont., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>