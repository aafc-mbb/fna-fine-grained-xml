<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">494</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHRASIA</taxon_name>
    <taxon_name authority="Raup" date="1934" rank="species">subarctica</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>36: 87, plate 278. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus euphrasia;species subarctica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphrasia</taxon_name>
    <taxon_name authority="Lange ex Rostrup" date="unknown" rank="species">arctica</taxon_name>
    <taxon_name authority="(B. Boivin) B. Boivin" date="unknown" rank="variety">dolosa</taxon_name>
    <taxon_hierarchy>genus euphrasia;species arctica;variety dolosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="Fernald &amp; Wiegand" date="unknown" rank="species">disjuncta</taxon_name>
    <taxon_name authority="B. Boivin" date="unknown" rank="variety">dolosa</taxon_name>
    <taxon_hierarchy>genus e.;species disjuncta;variety dolosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="Callen" date="unknown" rank="species">pennellii</taxon_name>
    <taxon_name authority="Callen" date="unknown" rank="variety">incana</taxon_name>
    <taxon_hierarchy>genus e.;species pennellii;variety incana</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Arctic eyebright</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple, sometimes branched, to 30 cm;</text>
      <biological_entity id="o28681" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 1 or 2 pairs, erect, from distal cauline nodes;</text>
      <biological_entity id="o28682" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" unit="or pairs" value="1" value_original="1" />
        <character name="quantity" src="d0_s1" unit="or pairs" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="distal cauline" id="o28683" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o28682" id="r2195" name="from" negation="false" src="d0_s1" to="o28683" />
    </statement>
    <statement id="d0_s2">
      <text>cauline internode lengths (1–) 2–5 times subtending leaves.</text>
      <biological_entity constraint="cauline" id="o28684" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o28685" is_modifier="false" name="length" src="d0_s2" value="(1-)2-5 times subtending leaves" value_original="(1-)2-5 times subtending leaves" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o28685" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade oblanceolate to broadly ovate, 2–6 (–11) mm, margins crenate, teeth 1–3 (–5) pairs, apices obtuse to subacute.</text>
      <biological_entity id="o28686" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28687" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="broadly ovate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28688" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o28689" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o28690" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences sparsely spicate, not 4-angled, beginning at node 3 or 4 (–6);</text>
      <biological_entity id="o28691" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s4" value="spicate" value_original="spicate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="4-angled" value_original="4-angled" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="6" />
        <character name="quantity" src="d0_s4" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o28692" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o28691" id="r2196" name="beginning at" negation="false" src="d0_s4" to="o28692" />
    </statement>
    <statement id="d0_s5">
      <text>proximal internode lengths 0.8–3 (–4) times bracts;</text>
      <biological_entity constraint="proximal" id="o28693" name="internode" name_original="internode" src="d0_s5" type="structure">
        <character constraint="bract" constraintid="o28694" is_modifier="false" name="length" src="d0_s5" value="0.8-3(-4) times bracts" value_original="0.8-3(-4) times bracts" />
      </biological_entity>
      <biological_entity id="o28694" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts green, often purplish adaxially, broader than leaves, ovate to obovate, length not more than 2 times width, 3–10 mm, base cuneate, surfaces sparsely hirsute and hairs eglandular or moreorless densely, sometimes sparsely, glandular-pilose and hairs glandular, stalks sometimes flexuous, 3–6-celled, 0.2–0.6 mm, teeth 2–6 pairs, longer than wide, apices subacute to acute, rarely aristate, sinuses between teeth acute.</text>
      <biological_entity id="o28695" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="often; adaxially" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character constraint="than leaves" constraintid="o28696" is_modifier="false" name="width" src="d0_s6" value="broader" value_original="broader" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="obovate" />
        <character is_modifier="false" modifier="not" name="l_w_ratio" src="d0_s6" value="2+" value_original="2+" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28696" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o28697" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o28698" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o28699" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character name="architecture" src="d0_s6" value="more or less densely" value_original="more or less densely" />
        <character is_modifier="false" modifier="sometimes sparsely; sparsely" name="pubescence" src="d0_s6" value="glandular-pilose" value_original="glandular-pilose" />
      </biological_entity>
      <biological_entity id="o28700" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o28701" name="stalk" name_original="stalks" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s6" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-6-celled" value_original="3-6-celled" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28702" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="6" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
      <biological_entity id="o28703" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity constraint="between teeth" constraintid="o28705" id="o28704" name="sinuse" name_original="sinuses" src="d0_s6" type="structure" constraint_original="between  teeth, ">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28705" name="tooth" name_original="teeth" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes straight, apex acute;</text>
      <biological_entity id="o28706" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o28707" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o28708" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla white to yellow, seldom with violet veins, 3–4 mm, lips moreorless equal.</text>
      <biological_entity id="o28709" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28710" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28711" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o28712" name="lip" name_original="lips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <relation from="o28710" id="r2197" modifier="seldom" name="with" negation="false" src="d0_s8" to="o28711" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules elliptic to oblong, (2.5–) 5–6.5 mm, apex truncate to retuse.</text>
      <biological_entity id="o28713" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28714" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s9" to="retuse" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The holotype (Raup &amp; Abbe 4633, GH) fixes the name Euphrasia subarctica to individuals with long, glandular hairs. Eglandular individuals are sometimes found growing mixed together with typical, glandular E. subarctica in Alaska and northwestern Canada. Similarly, eglandular and long glandular-pubescent individuals are intermixed in populations of otherwise morphologically similar, but not identical, E. disjuncta, including the specimens of its paratype (Fernald &amp; Wiegand 6166, CAN). Due to the clear tendency for the glandular forms to be more frequent in the west, while eglandular ones are more typical in the east, as well as other morphological differences, E. subarctica and E. disjuncta are treated as separate species here. However, it is likely that they represent a single lineage with a disjunct distribution, which could be a result of postglacial colonization from two separate refugia in western and eastern North America. Co-occurrence of glandular and eglandular states is documented in other diploids [for example, E. picta Wimmer in the Austrian Alps (P. F. Yeo 1978)]. Occasional individuals of E. subarctica with yellow corollas may be the result of introgression from E. mollis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, gravelly, or damp grassy places, stream banks, shores, thickets, heathlands, tundra.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy places" modifier="sandy gravelly or damp" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="heathlands" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Sask., Yukon; Alaska, Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>