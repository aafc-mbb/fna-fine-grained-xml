<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">538</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">AGALINIS</taxon_name>
    <taxon_name authority="Pennell" date="1922" rank="species">caddoensis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>73: 519. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus agalinis;species caddoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gerardia</taxon_name>
    <taxon_name authority="(Pennell) Pennell" date="unknown" rank="species">caddoensis</taxon_name>
    <taxon_hierarchy>genus gerardia;species caddoensis</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Caddo false foxglove</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems widely branched, 20–60 cm;</text>
      <biological_entity id="o25215" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="widely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches ascending, angular distally, scabridulous.</text>
      <biological_entity id="o25216" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distally" name="arrangement_or_shape" src="d0_s1" value="angular" value_original="angular" />
        <character is_modifier="false" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading or reflexed or recurved;</text>
      <biological_entity id="o25217" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade filiform, 15–35 x 0.3–0.6 mm, margins entire, siliceous, adaxial surface scabrellous;</text>
      <biological_entity id="o25218" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25219" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="siliceous" value_original="siliceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles absent.</text>
      <biological_entity constraint="adaxial" id="o25220" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes, flowers 1 or 2 per node;</text>
      <biological_entity constraint="inflorescences" id="o25221" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o25222" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25223" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts equal to or shorter than pedicels.</text>
      <biological_entity id="o25224" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character constraint="than pedicels" constraintid="o25225" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25225" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels spreading-ascending, 7–22 mm, proximally scabridulous.</text>
      <biological_entity id="o25226" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading-ascending" value_original="spreading-ascending" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx hemispheric-campanulate, tube 3.5–5.5 mm, glabrous, lobes triangular-subulate, 0.8–1.7 mm;</text>
      <biological_entity id="o25227" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25228" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric-campanulate" value_original="hemispheric-campanulate" />
      </biological_entity>
      <biological_entity id="o25229" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25230" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular-subulate" value_original="triangular-subulate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas rose-purple, with 2 yellow lines and red spots in abaxial throat, 17–30 mm, throat pilose externally and glabrous within across bases of adaxial lobes, lobes spreading, 6–9 (–11) mm, glabrous externally;</text>
      <biological_entity id="o25231" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25232" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="rose-purple" value_original="rose-purple" />
        <character constraint="in abaxial throat" constraintid="o25234" is_modifier="false" name="coloration" notes="" src="d0_s9" value="red spots" value_original="red spots" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25233" name="line" name_original="lines" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25234" name="throat" name_original="throat" src="d0_s9" type="structure" />
      <biological_entity id="o25235" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character constraint="within bases" constraintid="o25236" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25236" name="base" name_original="bases" src="d0_s9" type="structure" />
      <biological_entity id="o25237" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o25232" id="r1916" name="with" negation="false" src="d0_s9" to="o25233" />
    </statement>
    <statement id="d0_s10">
      <text>proximal anthers parallel to filaments, distal perpendicular to filaments, pollen-sacs 2.5–3.8 mm;</text>
      <biological_entity id="o25238" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o25239" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="to filaments" constraintid="o25240" is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s10" value="distal" value_original="distal" />
        <character constraint="to filaments" constraintid="o25241" is_modifier="false" name="orientation" src="d0_s10" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o25240" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o25241" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o25242" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s10" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style exserted, 12–16.5 mm.</text>
      <biological_entity id="o25243" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o25244" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="16.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules unknown.</text>
      <biological_entity id="o25245" name="capsule" name_original="capsules" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds unknown.</text>
      <biological_entity id="o25246" name="seed" name_original="seeds" src="d0_s13" type="structure" />
    </statement>
  </description>
  <discussion>Agalinis caddoensis is known only from two collections made by F. W. Pennell in 1913 and is most similar morphologically to A. navasotensis; see 22. A. navasotensis for a comparison. Agalinis caddoensis should be expected in northwestern Louisiana and eastern Texas and likely flowers in September as does A. navasotensis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak woods, dry loamy soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak woods" />
        <character name="habitat" value="dry loamy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>