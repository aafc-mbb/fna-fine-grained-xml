<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="D. D. Keck" date="1940" rank="species">nanus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>23: 607. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species nanus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>74.</number>
  <other_name type="common_name">Low beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending, 2–10 (–14) cm, densely retrorsely hairy.</text>
      <biological_entity id="o9541" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="14" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="densely retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, not leathery, densely retrorsely hairy;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, 12–35 (–50) × 3–10 mm, blade ovate to lanceolate, base tapered, margins entire, rarely obscurely dentate, apex obtuse to acute;</text>
      <biological_entity id="o9542" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="densely retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o9543" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o9544" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o9545" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o9546" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely obscurely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o9547" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 1–3 pairs, sessile, 13–34 × 2–6 mm, blade oblanceolate to lanceolate or linear, base tapered to slightly clasping, margins entire, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o9548" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s3" to="34" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9549" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o9550" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o9551" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9552" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous, cylindric, axis glandular-pubescent, 2–3 (–8) cm, verticillasters 1–5 (or 6), cymes 2–4-flowered, 2 per node;</text>
      <biological_entity id="o9553" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o9554" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9555" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o9556" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-4-flowered" value_original="2-4-flowered" />
        <character constraint="per node" constraintid="o9557" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9557" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate, 11–20 × 2–6 mm;</text>
      <biological_entity constraint="proximal" id="o9558" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-pubescent.</text>
      <biological_entity id="o9559" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o9560" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate to lanceolate, 4–6 × 1.8–2.2 mm, glandular-pubescent;</text>
      <biological_entity id="o9561" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o9562" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s7" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla violet to blue or purple, with reddish violet nectar guides, tubular-funnelform, 12–16 mm, sparsely yellow-lanate internally abaxially, tube 5–6 mm, throat gradually inflated, not constricted at orifice, 4–7 mm diam., rounded abaxially;</text>
      <biological_entity id="o9563" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9564" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s8" to="blue or purple" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s8" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; internally abaxially; abaxially" name="pubescence" src="d0_s8" value="yellow-lanate" value_original="yellow-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o9565" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish violet" value_original="reddish violet" />
      </biological_entity>
      <biological_entity id="o9566" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9567" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o9568" is_modifier="false" modifier="not" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" notes="" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9568" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o9564" id="r763" name="with" negation="false" src="d0_s8" to="o9565" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs parallel, navicular, 1.7–2 mm, dehiscing completely, sutures papillate;</text>
      <biological_entity id="o9569" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9570" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o9571" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="shape" src="d0_s9" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="distance" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o9572" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 8–9 mm, included or reaching orifice, 0.8–0.9 mm diam., tip straight, distal 5–6 mm moderately to densely pubescent, hairs orange, to 0.8 mm;</text>
      <biological_entity id="o9573" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9574" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character name="position" src="d0_s10" value="reaching orifice" value_original="reaching orifice" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="diameter" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9575" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9576" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9577" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 9–11 mm.</text>
      <biological_entity id="o9578" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9579" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 4–7 × 3–4.5 mm.</text>
      <biological_entity id="o9580" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon nanus is known from the ranges and dry valleys of western Beaver and Millard counties. Populations are concentrated in and near the Burbank Hills, Confusion Range, Halfway Hills, Mountain Home Range, and Tunnel Spring and Wah Wah mountains. The parallel pollen sacs and glandular-pubescent thyrse axes of P. nanus distinguish it from P. dolius.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pinyon-juniper woodlands, desert shrublands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="desert shrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>