<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1817" rank="genus">COLLINSIA</taxon_name>
    <taxon_name authority="(Newsom) B. G. Baldwin" date="2011" rank="species">latifolia</taxon_name>
    <place_of_publication>
      <publication_title>Kalisz &amp; Armbruster, Amer. J. Bot.</publication_title>
      <place_in_publication>98: 747. 2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus collinsia;species latifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Collinsia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">torreyi</taxon_name>
    <taxon_name authority="Newsom" date="1929" rank="variety">latifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>87: 299. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus collinsia;species torreyi;variety latifolia</taxon_hierarchy>
  </taxon_identification>
  <number>16.</number>
  <other_name type="common_name">Broad-leafed collinsia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals 5–25 cm.</text>
      <biological_entity id="o30080" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending.</text>
      <biological_entity id="o30081" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades elliptic to ovate, length usually 2–5 times width, margins entire or serrate.</text>
      <biological_entity id="o30082" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="ovate" />
        <character is_modifier="false" modifier="usually" name="l_w_ratio" src="d0_s2" value="2-5" value_original="2-5" />
      </biological_entity>
      <biological_entity id="o30083" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences densely glandular;</text>
      <biological_entity id="o30084" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes (1–) 3–6-flowered;</text>
      <biological_entity id="o30085" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="(1-)3-6-flowered" value_original="(1-)3-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers not crowded;</text>
      <biological_entity id="o30086" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distalmost bracts linear, 0–2 mm.</text>
      <biological_entity constraint="distalmost" id="o30087" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ascending to spreading, sometimes reflexed, pendent and/or sigmoid in fruit, usually longer than calyx, visible.</text>
      <biological_entity id="o30088" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="pendent" value_original="pendent" />
        <character constraint="in fruit" constraintid="o30089" is_modifier="false" name="course_or_shape" src="d0_s7" value="sigmoid" value_original="sigmoid" />
        <character constraint="than calyx" constraintid="o30090" is_modifier="false" name="length_or_size" notes="" src="d0_s7" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o30089" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o30090" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: caly× lobes lanceolate to ovate, equal to capsule, ape× subacute to rounded;</text>
      <biological_entity id="o30091" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="ovate" />
        <character constraint="to capsule" constraintid="o30092" is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o30092" name="capsule" name_original="capsule" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>corolla blue-violet to purple, banner white, cream, or pale lilac, 6–9 mm, sparsely glandular;</text>
      <biological_entity id="o30093" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o30094" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue-violet" name="coloration" src="d0_s9" to="purple" />
      </biological_entity>
      <biological_entity id="o30095" name="banner" name_original="banner" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale lilac" value_original="pale lilac" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale lilac" value_original="pale lilac" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>banner length 0.7–0.8 (–0.9) times wings, lobe base with folds bulging 0.5 mm away from throat opening at base of each lobe, wings equal to keel;</text>
      <biological_entity id="o30096" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o30097" name="banner" name_original="banner" src="d0_s10" type="structure">
        <character constraint="wing" constraintid="o30098" is_modifier="false" name="length" src="d0_s10" value="0.7-0.8(-0.9) times wings" value_original="0.7-0.8(-0.9) times wings" />
      </biological_entity>
      <biological_entity id="o30098" name="wing" name_original="wings" src="d0_s10" type="structure" />
      <biological_entity constraint="lobe" id="o30099" name="base" name_original="base" src="d0_s10" type="structure">
        <character constraint="away-from throat" constraintid="o30101" name="some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o30100" name="fold" name_original="folds" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s10" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity id="o30101" name="throat" name_original="throat" src="d0_s10" type="structure" />
      <biological_entity id="o30102" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o30103" name="lobe" name_original="lobe" src="d0_s10" type="structure" />
      <biological_entity id="o30104" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character constraint="to keel" constraintid="o30105" is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o30105" name="keel" name_original="keel" src="d0_s10" type="structure" />
      <relation from="o30099" id="r2286" name="with" negation="false" src="d0_s10" to="o30100" />
      <relation from="o30101" id="r2287" name="opening at" negation="false" src="d0_s10" to="o30102" />
      <relation from="o30101" id="r2288" name="opening at" negation="false" src="d0_s10" to="o30103" />
    </statement>
    <statement id="d0_s11">
      <text>stamens: abaxial filaments glabrous, adaxials glabrous or hairy at base, basal spur 0.</text>
      <biological_entity id="o30106" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial" id="o30107" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30108" name="adaxial" name_original="adaxials" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o30109" is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o30109" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity constraint="basal" id="o30110" name="spur" name_original="spur" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 2, ovate to oblong, often curled toward attachment side, 2–2.5 mm, margins thickened, inrolled.</text>
      <biological_entity id="o30111" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="oblong" />
        <character constraint="toward attachment side" constraintid="o30112" is_modifier="false" modifier="often" name="shape" src="d0_s12" value="curled" value_original="curled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="attachment" id="o30112" name="side" name_original="side" src="d0_s12" type="structure" />
      <biological_entity id="o30113" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s12" value="inrolled" value_original="inrolled" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Collinsia latifolia occurs on volcanic and metamorphic substrates in northern California and southern Oregon in the Klamath and Cascade ranges, and in the North Coast Ranges of California. Its flowers are nearly identical to those of C. torreyi, which differs in leaf shape: linear in C. torreyi and elliptic to ovate in C. latifolia.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed oak-conifer forests, openings near montane chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed oak-conifer forests" />
        <character name="habitat" value="openings" constraint="near montane chaparral" />
        <character name="habitat" value="montane chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>