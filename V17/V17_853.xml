<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHRASIA</taxon_name>
    <taxon_name authority="P. D. Sell &amp; Yeo" date="1962" rank="species">suborbicularis</taxon_name>
    <place_of_publication>
      <publication_title>Feddes Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>64: 203. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus euphrasia;species suborbicularis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Roundleaf eyebright</other_name>
  <other_name type="common_name">euphraise suborbiculaire</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems branched, sometimes simple, to 27 (–37) cm;</text>
      <biological_entity id="o7898" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="37" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="27" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 1–5 pairs, from distal cauline nodes;</text>
      <biological_entity id="o7899" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
      <biological_entity constraint="distal cauline" id="o7900" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o7899" id="r652" name="from" negation="false" src="d0_s1" to="o7900" />
    </statement>
    <statement id="d0_s2">
      <text>cauline internode lengths 1–3 times subtending leaves.</text>
      <biological_entity constraint="cauline" id="o7901" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o7902" is_modifier="false" name="length" src="d0_s2" value="1-3 times subtending leaves" value_original="1-3 times subtending leaves" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o7902" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade suborbiculate, rhombic, or obovate, 3–12 mm, margins crenate-serrate to serrate, teeth 1–4 pairs, apices obtuse to acute.</text>
      <biological_entity id="o7903" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7904" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7905" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="crenate-serrate to serrate" value_original="crenate-serrate to serrate" />
      </biological_entity>
      <biological_entity id="o7906" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o7907" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences sparsely spicate, not 4-angled, beginning at node 5–9;</text>
      <biological_entity id="o7908" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s4" value="spicate" value_original="spicate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="9" />
      </biological_entity>
      <biological_entity id="o7909" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o7908" id="r653" name="beginning at" negation="false" src="d0_s4" to="o7909" />
    </statement>
    <statement id="d0_s5">
      <text>proximal internode lengths 0.8–2.5 times bracts;</text>
      <biological_entity constraint="proximal" id="o7910" name="internode" name_original="internode" src="d0_s5" type="structure">
        <character constraint="bract" constraintid="o7911" is_modifier="false" name="length" src="d0_s5" value="0.8-2.5 times bracts" value_original="0.8-2.5 times bracts" />
      </biological_entity>
      <biological_entity id="o7911" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts green, broader than leaves, suborbiculate, length not more than 2 times width, 5–15 mm, surfaces hirsute and hairs eglandular, teeth 4 or 5 (or 6) pairs, shorter than wide, apices obtuse or acute.</text>
      <biological_entity id="o7912" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character constraint="than leaves" constraintid="o7913" is_modifier="false" name="width" src="d0_s6" value="broader" value_original="broader" />
        <character is_modifier="false" name="shape" src="d0_s6" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="not" name="l_w_ratio" src="d0_s6" value="2+" value_original="2+" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7913" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o7914" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o7915" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o7916" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or pairs" value="4" value_original="4" />
        <character name="quantity" src="d0_s6" unit="or pairs" value="5" value_original="5" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter than wide" value_original="shorter than wide" />
      </biological_entity>
      <biological_entity id="o7917" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: corolla white, adaxial lip sometimes tinged lilac, 5–7 mm, lips moreorless equal.</text>
      <biological_entity id="o7918" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7919" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7920" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="tinged lilac" value_original="tinged lilac" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7921" name="lip" name_original="lips" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules oval, (4–) 5–6.5 mm, apex retuse to emarginate.</text>
      <biological_entity id="o7922" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oval" value_original="oval" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7923" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="retuse" name="shape" src="d0_s8" to="emarginate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Turf and peat on calcareous barrens, rock and gravel near shorelines, sparsely wooded beach ridges.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="turf" constraint="on calcareous barrens , rock and gravel near shorelines" />
        <character name="habitat" value="peat" constraint="on calcareous barrens , rock and gravel near shorelines" />
        <character name="habitat" value="calcareous barrens" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="gravel near shorelines" />
        <character name="habitat" value="beach ridges" modifier="sparsely wooded" />
        <character name="habitat" value="wooded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nfld. and Labr., N.S., Ont., Que.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>