<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">303</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">297</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">SYNTHYRIS</taxon_name>
    <taxon_name authority="(Douglas ex Bentham) Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="species">reniformis</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>10: 454. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus synthyris;species reniformis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Wulfenia</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="1835" rank="species">reniformis</taxon_name>
    <place_of_publication>
      <publication_title>Scroph. Ind.,</publication_title>
      <place_in_publication>46. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus wulfenia;species reniformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veronica</taxon_name>
    <taxon_name authority="M. M. Martínez Ort. &amp; Albach" date="unknown" rank="species">regina-nivalis</taxon_name>
    <taxon_hierarchy>genus veronica;species regina-nivalis</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Round-leaf kittentail</other_name>
  <other_name type="common_name">snow queen</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves persistent, some withering in 2d year as new leaves expand;</text>
      <biological_entity id="o9069" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
        <character constraint="as leaves" constraintid="o9070" is_modifier="false" name="life_cycle" src="d0_s0" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o9070" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="new" value_original="new" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade cordate to reniform, 25+ mm wide, leathery, base cordate to lobate, margins incised-crenate, teeth apices obtuse to rounded, surfaces glabrous or hairy;</text>
      <biological_entity id="o9071" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s1" to="reniform" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s1" upper_restricted="false" />
        <character is_modifier="false" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o9072" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s1" to="lobate" />
      </biological_entity>
      <biological_entity id="o9073" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="incised-crenate" value_original="incised-crenate" />
      </biological_entity>
      <biological_entity constraint="teeth" id="o9074" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="rounded" />
      </biological_entity>
      <biological_entity id="o9075" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal veins extending into distal 1/2 of blade, lateral-veins 2–4 on each side of midvein.</text>
      <biological_entity constraint="basal" id="o9076" name="vein" name_original="veins" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o9077" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o9078" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o9079" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o9080" from="2" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o9080" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o9081" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
      <relation from="o9076" id="r726" name="extending into" negation="false" src="d0_s2" to="o9077" />
      <relation from="o9077" id="r727" name="part_of" negation="false" src="d0_s2" to="o9078" />
      <relation from="o9080" id="r728" name="part_of" negation="false" src="d0_s2" to="o9081" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes decumbent, to 18 cm in fruit;</text>
      <biological_entity id="o9082" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" constraint="in fruit" constraintid="o9083" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9083" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sterile bracts usually 0;</text>
      <biological_entity id="o9084" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers 5–10, loosely aggregated.</text>
      <biological_entity id="o9085" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="10" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s5" value="aggregated" value_original="aggregated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sepals 4.</text>
      <biological_entity id="o9086" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Petals (3 or) 4 (or 5), apex entire or erose;</text>
      <biological_entity id="o9087" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o9088" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla blue, ± regular, campanulate, much longer than calyx, puberulent-villous in throat, tube conspicuous.</text>
      <biological_entity id="o9089" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="regular" value_original="regular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character constraint="than calyx" constraintid="o9090" is_modifier="false" name="length_or_size" src="d0_s8" value="much longer" value_original="much longer" />
        <character constraint="in throat" constraintid="o9091" is_modifier="false" name="pubescence" src="d0_s8" value="puberulent-villous" value_original="puberulent-villous" />
      </biological_entity>
      <biological_entity id="o9090" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
      <biological_entity id="o9091" name="throat" name_original="throat" src="d0_s8" type="structure" />
      <biological_entity id="o9092" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Stamens epipetalous.</text>
      <biological_entity id="o9093" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="epipetalous" value_original="epipetalous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ovaries: ovules 4.</text>
      <biological_entity id="o9094" name="ovary" name_original="ovaries" src="d0_s10" type="structure" />
      <biological_entity id="o9095" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules sparsely hairy.</text>
      <biological_entity id="o9096" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun; fruiting Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>