<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">405</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="(Heckard &amp; Shevock) G. L. Nesom" date="2012" rank="species">norrisii</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 39. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species norrisii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Heckard &amp; Shevock" date="1985" rank="species">norrisii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>32: 179, figs. 1, 2. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species norrisii</taxon_hierarchy>
  </taxon_identification>
  <number>51.</number>
  <other_name type="common_name">Norris’s monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, fibrous-rooted or filiform-taprooted.</text>
      <biological_entity id="o26743" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="filiform-taprooted" value_original="filiform-taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect-ascending, geniculate at nodes, usually branched from proximal nodes, 2–15 (–25) cm, villous-glandular.</text>
      <biological_entity id="o26744" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect-ascending" />
        <character constraint="at nodes" constraintid="o26745" is_modifier="false" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
        <character constraint="from proximal nodes" constraintid="o26746" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="villous-glandular" value_original="villous-glandular" />
      </biological_entity>
      <biological_entity id="o26745" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o26746" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o26747" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 5–10 (–15) mm;</text>
      <biological_entity id="o26748" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade palmately 3–5-veined, sometimes with 1–3 distal vein pairs diverging pinnately, elliptic to elliptic-obovate, 20–35 × 10–20 mm, base usually attenuate, margins subentire to distally denticulate, apex acute to obtuse, surfaces villous-glandular.</text>
      <biological_entity id="o26749" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s4" value="3-5-veined" value_original="3-5-veined" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s4" to="elliptic-obovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o26750" name="vein" name_original="vein" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="3" />
        <character is_modifier="false" modifier="pinnately" name="orientation" src="d0_s4" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o26751" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o26752" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="distally denticulate" />
      </biological_entity>
      <biological_entity id="o26753" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o26754" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="villous-glandular" value_original="villous-glandular" />
      </biological_entity>
      <relation from="o26749" id="r2049" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o26750" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 1–5, from medial to distal nodes.</text>
      <biological_entity id="o26756" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o26755" id="r2050" name="from" negation="false" src="d0_s5" to="o26756" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 20–35 (–50) mm, villous-glandular.</text>
      <biological_entity id="o26757" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o26755" id="r2051" name="fruiting" negation="false" src="d0_s6" to="o26757" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces red-dotted, campanulate, weakly inflated, 4–6 mm, margins distinctly toothed or lobed, villous-glandular, ribs rounded-thickened, lobes pronounced, erect, often incurved, linear-oblong to oblong-lanceolate, apex rounded to blunt.</text>
      <biological_entity id="o26755" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="villous-glandular" value_original="villous-glandular" />
      </biological_entity>
      <biological_entity id="o26758" name="calyx" name_original="calyces" src="d0_s7" type="structure" />
      <biological_entity id="o26759" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-dotted" value_original="red-dotted" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26760" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="villous-glandular" value_original="villous-glandular" />
      </biological_entity>
      <biological_entity id="o26761" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="rounded-thickened" value_original="rounded-thickened" />
      </biological_entity>
      <biological_entity id="o26762" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="pronounced" value_original="pronounced" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s7" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s7" to="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o26763" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="blunt" />
      </biological_entity>
      <relation from="o26755" id="r2052" name="fruiting" negation="false" src="d0_s7" to="o26758" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, base of each lobe with a prominent maroon splotch, abaxial limb with white patch at 2 sinus bases, weakly bilaterally or radially symmetric, weakly bilabiate or regular;</text>
      <biological_entity id="o26764" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o26765" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="weakly bilaterally; bilaterally; radially" name="architecture" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="regular" value_original="regular" />
      </biological_entity>
      <biological_entity id="o26766" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o26767" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="maroon splotch" value_original="maroon splotch" />
        <character constraint="at sinus bases" constraintid="o26768" is_modifier="false" name="coloration" src="d0_s8" value="white patch" value_original="white patch" />
      </biological_entity>
      <biological_entity constraint="sinus" id="o26768" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o26765" id="r2053" name="part_of" negation="false" src="d0_s8" to="o26766" />
      <relation from="o26765" id="r2054" name="with" negation="false" src="d0_s8" to="o26767" />
    </statement>
    <statement id="d0_s9">
      <text>tube-throat cylindric-funnelform, 12–16 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o26769" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="distance" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o26770" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o26769" id="r2055" name="exserted beyond" negation="false" src="d0_s9" to="o26770" />
    </statement>
    <statement id="d0_s10">
      <text>limb expanded 15–30 mm, lobes oblong-obovate to orbicular-obovate, apex rounded-truncate.</text>
      <biological_entity id="o26771" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26772" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong-obovate" name="shape" src="d0_s10" to="orbicular-obovate" />
      </biological_entity>
      <biological_entity id="o26773" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded-truncate" value_original="rounded-truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o26774" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, glabrous.</text>
      <biological_entity id="o26775" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules usually slightly exserted, 4–6 mm. 2n = 32.</text>
      <biological_entity id="o26776" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually slightly" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26777" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe norrisii is known only from the Kaweah River drainage; most populations are in Sequoia National Park in Tulare County. The species is characterized by its short-petiolate leaves with attenuate bases, very large corollas with red splotches at the base of each lobe and two white patches on the abaxial limb, and very short, purple-dotted calyces with rounded-thickened ribs and linear-oblong lobes incurved in fruit. The capsules often are slightly exserted.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep marble outcrops in soil pockets, moss covered marble and quartzite ledges, cracks, fractures, weathered faces, chamise chaparral or blue oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep marble outcrops" constraint="in soil pockets , moss" />
        <character name="habitat" value="soil pockets" />
        <character name="habitat" value="moss" />
        <character name="habitat" value="marble" modifier="covered" />
        <character name="habitat" value="quartzite ledges" />
        <character name="habitat" value="cracks" />
        <character name="habitat" value="fractures" />
        <character name="habitat" value="weathered faces" />
        <character name="habitat" value="chamise chaparral" />
        <character name="habitat" value="blue oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>