<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHRASIA</taxon_name>
    <taxon_name authority="Lange ex Rostrup" date="1870" rank="species">arctica</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Tidsskr.</publication_title>
      <place_in_publication>4: 47. 1870</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus euphrasia;species arctica</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Arctic eyebright</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or branched, to 25 (–35) cm;</text>
      <biological_entity id="o20578" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches 1–5 (or 6) pairs, from proximal, middle, or distal cauline nodes;</text>
      <biological_entity id="o20579" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
      <biological_entity id="o20580" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o20581" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity constraint="distal cauline" id="o20582" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o20579" id="r1562" name="from" negation="false" src="d0_s1" to="o20580" />
    </statement>
    <statement id="d0_s2">
      <text>cauline internode lengths 2–4 times subtending leaves.</text>
      <biological_entity constraint="cauline" id="o20583" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o20584" is_modifier="false" name="length" src="d0_s2" value="2-4 times subtending leaves" value_original="2-4 times subtending leaves" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o20584" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade usually broadly ovate, 3–16 mm, margins serrate, teeth 1–6 pairs, apices acute.</text>
      <biological_entity id="o20585" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o20586" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20587" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o20588" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o20589" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences beginning at node 3–5;</text>
      <biological_entity id="o20590" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o20591" name="node" name_original="node" src="d0_s4" type="structure" />
      <relation from="o20590" id="r1563" name="beginning at" negation="false" src="d0_s4" to="o20591" />
    </statement>
    <statement id="d0_s5">
      <text>bracts green or purplish adaxially, broader than leaves, broadly ovate or deltate, length not more than 2 times width, (4–) 5–9 (–11) mm, surfaces glabrous or setose and hairs eglandular, sometimes pubescent and hairs glandular, stalks 1-celled or 2-celled, 0.1–0.2 mm, teeth 3–6 pairs, as long as or much longer than wide, apices acute to acuminate.</text>
      <biological_entity id="o20592" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="purplish" value_original="purplish" />
        <character constraint="than leaves" constraintid="o20593" is_modifier="false" name="width" src="d0_s5" value="broader" value_original="broader" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="not" name="l_w_ratio" src="d0_s5" value="2+" value_original="2+" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20593" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o20594" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o20595" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20596" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o20597" name="stalk" name_original="stalks" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-celled" value_original="1-celled" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-celled" value_original="2-celled" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20598" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="much longer than wide" value_original="much longer than wide" />
      </biological_entity>
      <biological_entity id="o20599" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: corolla white, sometimes lilac, adaxial lip lilac, 6–11 (–13) mm, abaxial lip exceeding adaxial.</text>
      <biological_entity id="o20600" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o20601" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="lilac" value_original="lilac" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20602" name="lip" name_original="lip" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="lilac" value_original="lilac" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20603" name="lip" name_original="lip" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o20604" name="lip" name_original="lip" src="d0_s6" type="structure" />
      <relation from="o20603" id="r1564" name="exceeding" negation="false" src="d0_s6" to="o20604" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules oblong to elliptic or obovate, 4.5–8 mm, apex truncate to retuse.</text>
      <biological_entity id="o20605" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="elliptic or obovate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20606" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s7" to="retuse" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 5 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.B., N.S., Nfld. and Labr. (Nfld.), Ont., P.E.I., Que.; Maine; Europe, Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The other three subspecies of Euphrasia arctica occur in Europe (P. F. Yeo 1978).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem bases flexuous or decumbent; branches 1 or 2(–5) pairs, usually flexuous; bract surfaces glabrous or hairy, hairs eglandular bristles, teeth not much longer than wide; corollas 7–11(–13) mm.</description>
      <determination>3a. Euphrasia arctica subsp. arctica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem bases erect; branches 1–5(or 6) pairs, usually straight; bract surfaces hairy, hairs glandular and eglandular bristles, teeth much longer than wide; corollas 6–8(–10) mm.</description>
      <determination>3b. Euphrasia arctica subsp. borealis</determination>
    </key_statement>
  </key>
</bio:treatment>