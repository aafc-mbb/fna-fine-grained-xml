<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">674</other_info_on_meta>
    <other_info_on_meta type="mention_page">670</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Nuttall ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="genus">CORDYLANTHUS</taxon_name>
    <taxon_name authority="A. Gray" date="1883" rank="species">pringlei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 94. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus cordylanthus;species pringlei</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Pringle’s bird’s-beak</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or ascending, 30–120 (–150) cm, glabrous or puberulent.</text>
      <biological_entity id="o24645" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves puberulent or glabrous;</text>
      <biological_entity id="o24646" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal 10–40 mm, margins 3-lobed, lobes 1–2 mm wide;</text>
      <biological_entity constraint="proximal" id="o24647" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24648" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o24649" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal 5–20 × 1 mm, margins entire.</text>
      <biological_entity constraint="distal" id="o24650" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24651" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences capitate spikes, 2–4-flowered, 15–20 mm;</text>
      <biological_entity id="o24652" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="2-4-flowered" value_original="2-4-flowered" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24653" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 1–3, flabelliform, 5–8 mm, margins 3–7-lobed, lobes green, narrowly ovate.</text>
      <biological_entity id="o24654" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flabelliform" value_original="flabelliform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24655" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="3-7-lobed" value_original="3-7-lobed" />
      </biological_entity>
      <biological_entity id="o24656" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: bracteoles 8–10 mm, margins entire.</text>
      <biological_entity id="o24657" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o24658" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24659" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx 8–10 mm, tube 0 mm, apex 2-fid, cleft 0.5–1 mm;</text>
      <biological_entity id="o24660" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o24661" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24662" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="0" value_original="0" />
      </biological_entity>
      <biological_entity id="o24663" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla pale-yellow to yellow with purple markings, 8–9 mm, throat 4 mm diam., adaxial lip 3–4 mm, ca. equal to and appressed to adaxial;</text>
      <biological_entity id="o24664" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24665" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="with markings" constraintid="o24666" from="pale-yellow" name="coloration" src="d0_s8" to="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24666" name="marking" name_original="markings" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o24667" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character name="diameter" src="d0_s8" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24668" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24669" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4, filaments hairy, fertile pollen-sacs 2 per filament, unequal.</text>
      <biological_entity id="o24670" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24671" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o24672" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o24673" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character constraint="per filament" constraintid="o24674" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o24674" name="filament" name_original="filament" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Capsules oblong-ovoid, 5–8 mm.</text>
      <biological_entity id="o24675" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 4–6, dark-brown, ovoid to narrowly reniform, 2.5–3 mm, striate.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28.</text>
      <biological_entity id="o24676" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s11" to="narrowly reniform" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s11" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24677" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cordylanthus pringlei grows in the Coast Range of California. The species is distinctive because of its flabelliform inflorescence bracts and relatively short corollas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry openings in chaparral and mixed-evergreen forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry openings" constraint="in chaparral and mixed-evergreen forests" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="mixed-evergreen forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>