<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">633</other_info_on_meta>
    <other_info_on_meta type="mention_page">581</other_info_on_meta>
    <other_info_on_meta type="mention_page">632</other_info_on_meta>
    <other_info_on_meta type="mention_page">634</other_info_on_meta>
    <other_info_on_meta type="mention_page">636</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Eastwood" date="1909" rank="species">nelsonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>44: 579. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species nelsonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="Standley &amp; Blumer" date="unknown" rank="species">austromontana</taxon_name>
    <taxon_hierarchy>genus castilleja;species austromontana</taxon_hierarchy>
  </taxon_identification>
  <number>73.</number>
  <other_name type="common_name">Arizona or southern mountains paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 2.5–8 (–10) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot or branched root system.</text>
      <biological_entity id="o31727" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o31728" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <biological_entity constraint="root" id="o31729" name="system" name_original="system" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o31727" id="r2407" name="with" negation="false" src="d0_s2" to="o31728" />
      <relation from="o31727" id="r2408" name="with" negation="false" src="d0_s2" to="o31729" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, ascending to erect, unbranched or often strongly and diffusely branched distally, hairs sparse to dense, spreading to matted, long proximally on stem, becoming puberulent distally, ± stiff, eglandular, often mixed with retrorse shorter ones.</text>
      <biological_entity id="o31730" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character name="architecture" src="d0_s3" value="often strongly" value_original="often strongly" />
        <character is_modifier="false" modifier="diffusely; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o31731" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s3" to="dense" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form" src="d0_s3" value="matted" value_original="matted" />
        <character constraint="on stem" constraintid="o31732" is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" modifier="becoming; distally" name="pubescence" notes="" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character constraint="with shorter ones" constraintid="o31733" is_modifier="false" modifier="often" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o31732" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity constraint="shorter" id="o31733" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="retrorse" value_original="retrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green, linear-lanceolate or narrowly to broadly lanceolate, 2–6.5 (–8) cm, not fleshy, margins plane, flat to involute, 0 (–3) -lobed, apex acute;</text>
      <biological_entity id="o31734" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="linear-lanceolate or" name="shape" src="d0_s4" to="narrowly broadly lanceolate" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="6.5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o31735" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s4" to="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0(-3)-lobed" value_original="0(-3)-lobed" />
      </biological_entity>
      <biological_entity id="o31736" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending, lanceolate, apex acute to obtuse.</text>
      <biological_entity id="o31737" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o31738" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (2.5–) 5–15 × 2–4.5 cm;</text>
      <biological_entity id="o31739" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s6" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish, distally scarlet to red or orange-red, rarely yellow or crimson, veins usually yellow or yellow-green, contrasting conspicuously with base color, lanceolate or elliptic to oblanceolate or obovate, 0–3 (–5) -lobed;</text>
      <biological_entity id="o31740" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="distally scarlet" name="coloration" src="d0_s7" to="red or orange-red rarely yellow or crimson" />
        <character char_type="range_value" from="distally scarlet" name="coloration" src="d0_s7" to="red or orange-red rarely yellow or crimson" />
      </biological_entity>
      <biological_entity id="o31741" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="oblanceolate or obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="0-3(-5)-lobed" value_original="0-3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o31742" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="conspicuously" name="coloration" src="d0_s7" value="conspicuously" value_original="conspicuously" />
      </biological_entity>
      <relation from="o31741" id="r2409" name="contrasting" negation="false" src="d0_s7" to="o31742" />
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending, lanceolate to triangular, medium length, arising above mid length, apex rounded to obtuse.</text>
      <biological_entity id="o31743" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="triangular" />
        <character is_modifier="false" name="length" src="d0_s8" value="medium" value_original="medium" />
        <character constraint="above mid lobes" constraintid="o31744" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="mid" id="o31744" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o31745" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="length" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces mostly yellowish throughout, with a thin reddish apex, 15–27 mm;</text>
      <biological_entity id="o31746" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly; throughout" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31747" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="true" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="reddish" value_original="reddish" />
      </biological_entity>
      <relation from="o31746" id="r2410" name="with" negation="false" src="d0_s9" to="o31747" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial clefts (5–) 9–11 mm, adaxial 4.5–9.5 mm, clefts 25–50% of calyx length, deeper than laterals, lateral 2–4 mm, 10–20% of calyx length;</text>
      <biological_entity constraint="abaxial" id="o31748" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31749" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="9.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31750" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character constraint="than laterals" constraintid="o31752" is_modifier="false" name="length" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" name="length" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31751" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o31752" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
      <relation from="o31750" id="r2411" modifier="25-50%" name="part_of" negation="false" src="d0_s10" to="o31751" />
    </statement>
    <statement id="d0_s11">
      <text>lobes linear-lanceolate to triangular, apex acute to acuminate, rarely ± obtuse.</text>
      <biological_entity id="o31753" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s11" to="triangular" />
      </biological_entity>
      <biological_entity id="o31754" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
        <character is_modifier="false" modifier="rarely more or less" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas slightly curved, 15–35 mm, subequal to calyx or beak partially to strongly exserted;</text>
      <biological_entity id="o31755" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="35" to_unit="mm" />
        <character constraint="to beak" constraintid="o31757" is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o31756" name="calyx" name_original="calyx" src="d0_s12" type="structure" />
      <biological_entity id="o31757" name="beak" name_original="beak" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="partially to strongly" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 15–17 mm;</text>
      <biological_entity id="o31758" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s13" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak adaxially yellowish green, 10–16 mm;</text>
      <biological_entity id="o31759" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip green, reduced, ± pouched, 0.5–1.5 mm, 4–10% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o31760" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s15" value="pouched" value_original="pouched" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31761" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o31760" id="r2412" modifier="4-10%" name="as long as" negation="false" src="d0_s15" to="o31761" />
    </statement>
    <statement id="d0_s16">
      <text>teeth incurved, deep green, 0.7–1 mm. 2n = 24.</text>
      <biological_entity id="o31762" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="depth" src="d0_s16" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31763" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja nelsonii is fairly common in the upper elevations of the so-called sky island ranges from central and eastern Arizona to adjacent New Mexico, southward into the Sierra Madre Occidental, at least as far south as southern Chihuahua, where the type collection was obtained on Cerro Mohinora. Although it was long known in the United States as C. austromontana, the name C. nelsonii has priority. Some specimens from southern Coconino County, Arizona, approach C. miniata, but most material is easily separable. Castilleja nelsonii occasionally hybridizes with C. mogollonica in Apache County, Arizona, near the border of the range of the former.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, meadows, riparian zones, moist ground in open forests, montane to subalpine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="riparian zones" />
        <character name="habitat" value="moist ground" constraint="in open forests" />
        <character name="habitat" value="open forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–3100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Durango, Nayarit, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nayarit)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>