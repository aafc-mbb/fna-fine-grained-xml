<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Craig C. Freeman</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Nuttall ex A. Gray" date="1868" rank="genus">TONELLA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 378. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus tonella</taxon_hierarchy>
    <other_info_on_name type="etymology">Derivation unknown; perhaps a misspelling of Latin tenella, delicate, alluding to filiform branches</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>27.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o13450" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, glabrous proximally, glandular-hairy distally.</text>
      <biological_entity id="o13451" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, opposite;</text>
      <biological_entity id="o13452" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present or absent;</text>
      <biological_entity id="o13453" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade not fleshy, not leathery, margins entire, crenate, dentate, or serrate.</text>
      <biological_entity id="o13454" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o13455" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, racemes;</text>
      <biological_entity id="o13456" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o13457" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts present.</text>
      <biological_entity id="o13458" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present, glandular-hairy, sometimes glabrous or glabrate;</text>
      <biological_entity id="o13459" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles absent.</text>
      <biological_entity id="o13460" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o13461" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, proximally connate, calyx ± bilaterally symmetric, subrotate to campanulate, lobes triangular to lanceolate;</text>
      <biological_entity id="o13462" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o13463" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less bilaterally" name="shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="subrotate" name="shape" src="d0_s10" to="campanulate" />
      </biological_entity>
      <biological_entity id="o13464" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla white on tube and lobes proximally, blue to violet or lavender on lobes distally, with dark violet spots internally near bases of adaxial and lateral lobes, bilaterally symmetric, weakly bilabiate, subrotate, tube base gibbous adaxially, sometimes obscurely so, not spurred abaxially, throat not densely pilose internally, lobes 5, abaxial 3, middle lobe of abaxial lip not folded lengthwise, not enclosing stamens and style, adaxial 2;</text>
      <biological_entity id="o13465" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character constraint="on lobes" constraintid="o13467" is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" constraint="on lobes" constraintid="o13468" from="blue" name="coloration" notes="" src="d0_s11" to="violet or lavender" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" notes="" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subrotate" value_original="subrotate" />
      </biological_entity>
      <biological_entity id="o13466" name="tube" name_original="tube" src="d0_s11" type="structure" />
      <biological_entity id="o13467" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o13468" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o13469" name="spot" name_original="spots" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="dark violet" value_original="dark violet" />
      </biological_entity>
      <biological_entity id="o13470" name="base" name_original="bases" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial lateral and" id="o13471" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity constraint="tube" id="o13472" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s11" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" modifier="sometimes obscurely; obscurely; not; abaxially" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o13473" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not densely; internally" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o13474" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial lateral and" id="o13475" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="middle" id="o13476" name="lobe" name_original="lobe" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not; lengthwise" name="architecture_or_shape" src="d0_s11" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13477" name="lip" name_original="lip" src="d0_s11" type="structure" />
      <biological_entity id="o13478" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o13479" name="style" name_original="style" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial lateral and" id="o13480" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o13465" id="r1049" name="with" negation="false" src="d0_s11" to="o13469" />
      <relation from="o13469" id="r1050" name="near" negation="false" src="d0_s11" to="o13470" />
      <relation from="o13470" id="r1051" name="part_of" negation="false" src="d0_s11" to="o13471" />
      <relation from="o13476" id="r1052" name="part_of" negation="false" src="d0_s11" to="o13477" />
      <relation from="o13476" id="r1053" name="enclosing" negation="true" src="d0_s11" to="o13478" />
      <relation from="o13476" id="r1054" name="enclosing" negation="false" src="d0_s11" to="o13479" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, medially adnate to corolla, equal or ± didynamous, exserted, filaments glandular;</text>
      <biological_entity id="o13481" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character constraint="to corolla" constraintid="o13482" is_modifier="false" modifier="medially" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="variability" notes="" src="d0_s12" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s12" value="didynamous" value_original="didynamous" />
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o13482" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o13483" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s12" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 1, minute;</text>
      <biological_entity id="o13484" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="size" src="d0_s13" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o13485" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma linear.</text>
      <biological_entity id="o13486" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, dehiscence septicidal and loculicidal.</text>
      <biological_entity constraint="fruits" id="o13487" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 2 or 4, dark-brown to black, reniform to ovoid, wings absent.</text>
      <biological_entity id="o13488" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="count" value_original="count" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="list" value_original="list" />
        <character name="quantity" src="d0_s17" unit="or punct dark-brown to black" value="2" value_original="2" />
        <character name="quantity" src="d0_s17" unit="or punct dark-brown to black" value="4" value_original="4" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s17" to="ovoid" />
      </biological_entity>
      <biological_entity id="o13489" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Studies of vascular anatomy, organography, and gametophyte development (G. F. Schrock and B. F. Palser 1967) and molecular phylogenetic analyses (B. G. Baldwin et al. 2011) support the monophyly of Tonella and its sister-relationship to Collinsia.</discussion>
  <references>
    <reference>Baldwin, B. G., S. Kalisz, and W. S. Armbruster. 2011. Phylogenetic perspectives on diversification, biogeography, and floral evolution of Collinsia and Tonella (Plantaginaceae). Amer. J. Bot. 98: 731–753.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 5–7 × 6–12 mm; racemes: flowers 2–10 per node.</description>
      <determination>1. Tonella floribunda</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 2–2.5 × 2–4 mm; racemes: flowers 1–3 per node.</description>
      <determination>2. Tonella tenella</determination>
    </key_statement>
  </key>
</bio:treatment>