<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">133</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="mention_page">129</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">eriantherus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 737. 1813</place_in_publication>
      <other_info_on_pub>(as Pentstemon erianthera)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species eriantherus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>61.</number>
  <other_name type="common_name">Crested-tongue beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, (2–) 6–60 cm, retrorsely hairy, sometimes also villous or glandular-villous, or glabrate.</text>
      <biological_entity id="o39975" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, not leathery, glabrous or glabrate to retrorsely hairy and, sometimes, sparsely villous abaxially, glabrous or glabrate to retrorsely hairy and, sometimes, villous or glandular-villous adaxially;</text>
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, (14–) 22–130 (–180) × 3–30 (–40) mm, blade spatulate to obovate, oblanceolate, elliptic, or linear, base tapered, margins entire or ± dentate to serrate, apex rounded to obtuse or acute;</text>
      <biological_entity id="o39976" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s1" to="retrorsely hairy" />
        <character is_modifier="false" modifier="sometimes; sparsely; abaxially" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s1" to="retrorsely hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s1" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o39977" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o39978" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o39979" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o39980" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o39981" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="obtuse or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–6 pairs, sessile or proximals short-petiolate, 18–90 × 3–20 (–25) mm, blade oblanceolate to oblong, lanceolate, or linear, base tapered to clasping or cordate-clasping, margins entire or ± dentate to serrate, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o39982" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o39983" name="proximal" name_original="proximals" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39984" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="oblong lanceolate or linear" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="oblong lanceolate or linear" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="oblong lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o39985" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o39986" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o39987" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses continuous or interrupted, cylindric, (1–) 2–20 (–27) cm, axis glandular-pubescent to glandular-villous, sometimes also retrorsely hairy, verticillasters 2–7 (–9), cymes (1 or) 2–6-flowered, 2 per node;</text>
      <biological_entity id="o39988" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="27" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o39989" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character char_type="range_value" from="glandular-pubescent" name="pubescence" src="d0_s4" to="glandular-villous" />
        <character is_modifier="false" modifier="sometimes; retrorsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o39990" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
      <biological_entity id="o39991" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-6-flowered" value_original="2-6-flowered" />
        <character constraint="per node" constraintid="o39992" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o39992" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts oblanceolate to oblong or lanceolate, 12–75 × 2–23 mm;</text>
      <biological_entity constraint="proximal" id="o39993" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="oblong or lanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="75" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-villous or glandular-pubescent and, sometimes, also retrorsely hairy.</text>
      <biological_entity id="o39994" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes; retrorsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o39995" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes; retrorsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes lanceolate, 5.5–13 × 1.4–3 mm, glandular-pubescent to glandular-villous;</text>
      <biological_entity id="o39996" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o39997" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s7" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="glandular-pubescent" name="pubescence" src="d0_s7" to="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla lavender to violet, purple, pink, or blue, with reddish purple nectar guides, funnelform or ventricose-ampliate, 16–35 (–42) mm, sparsely to densely white or yellow-villous internally abaxially, sometimes glandular-pubescent laterally, tube 4–9 mm, throat gradually to abruptly inflated, constricted or not at orifice, 6–14 mm diam., rounded abaxially;</text>
      <biological_entity id="o39998" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o39999" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="violet purple pink or blue" />
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="violet purple pink or blue" />
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="violet purple pink or blue" />
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="violet purple pink or blue" />
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="ventricose-ampliate" value_original="ventricose-ampliate" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="42" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s8" value="yellow-villous" value_original="yellow-villous" />
        <character is_modifier="false" modifier="sometimes; laterally" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o40000" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o40001" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40002" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually to abruptly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character name="size" src="d0_s8" value="not" value_original="not" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" notes="" src="d0_s8" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o40003" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o39999" id="r3081" name="with" negation="false" src="d0_s8" to="o40000" />
      <relation from="o40002" id="r3082" name="at" negation="false" src="d0_s8" to="o40003" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included, pollen-sacs widely divergent to opposite, navicular to subexplanate or explanate, 0.8–1.9 mm, dehiscing completely, sutures smooth or papillate;</text>
      <biological_entity id="o40004" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o40005" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o40006" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character char_type="range_value" from="widely divergent" name="arrangement" src="d0_s9" to="opposite" />
        <character is_modifier="false" name="shape" src="d0_s9" value="navicular" value_original="navicular" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s9" to="1.9" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o40007" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminode 12–18 mm, exserted or prominently exserted, 0.3–0.9 mm diam., tip recurved, distal 6–15 mm sparsely to densely lanate, hairs yellow, yellowish, or orangish, to 4 mm, and medial hairs shorter, stiffer, and retrorse;</text>
      <biological_entity id="o40008" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o40009" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="prominently" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40010" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o40011" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s10" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity id="o40012" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangish" value_original="orangish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangish" value_original="orangish" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="medial" id="o40013" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiffer" value_original="stiffer" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="retrorse" value_original="retrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 9–20 mm, glabrous or proximal 1–10 mm sparsely glandular-pubescent.</text>
      <biological_entity id="o40014" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o40015" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o40016" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–13 × 4–6 mm, sparsely glandular-pubescent distally.</text>
      <biological_entity id="o40017" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s12" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Colo., Idaho, Mont., N.Dak., Nebr., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>D. D. Keck (1938) treated this complex as three species: Penstemon cleburnei, P. eriantherus (with three varieties), and P. whitedii (with three subspecies). Most authors followed A. Cronquist (1959), who employed a broader species concept; the approach by Cronquist is followed here.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas constricted at orifices; styles 9–12(–14) mm; pollen sacs 0.8–1.2 mm.</description>
      <determination>61c. Penstemon eriantherus var. cleburnei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas not constricted at orifices; styles 9–20 mm; pollen sacs 1–1.9 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems glabrate or retrorsely hairy, rarely glandular-villous, proximally, glandular-villous distally; leaves glabrous or glabrate abaxially, glabrate or villous to glandular-villous adaxially.</description>
      <determination>61e. Penstemon eriantherus var. whitedii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems retrorsely hairy and, sometimes, sparsely villous, glandular-villous, or glandular-pubescent distally; leaves retrorsely hairy and, sometimes, sparsely villous or glabrous abaxially, retrorsely hairy or puberulent and, sometimes, villous or glandular-villous adaxially.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas funnelform; staminodes sparsely lanate distally; leaves glabrous abaxially.</description>
      <determination>61b. Penstemon eriantherus var. argillosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas ventricose-ampliate; staminodes moderately to densely lanate distally; leaves retrorsely hairy, sparsely villous, or glabrous abaxially.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pollen sacs explanate; stems retrorsely hairy and, usually, sparsely villous or glandular-villous distally; corollas (20–)22–35(–42) mm.</description>
      <determination>61a. Penstemon eriantherus var. eriantherus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pollen sacs navicular to subexplanate; stems retrorsely hairy, sometimes also glandular-pubescent distally or wholly; corollas 18–25 mm.</description>
      <determination>61d. Penstemon eriantherus var. redactus</determination>
    </key_statement>
  </key>
</bio:treatment>