<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">17</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ANTIRRHINUM</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">majus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 617. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus antirrhinum;species majus</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Muflier commun</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems terete, 3–8 (–15) dm, glabrous or sparsely stipitate-glandular proximally, stipitate-glandular distally.</text>
      <biological_entity id="o10998" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="terete" value_original="terete" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade narrowly elliptic to lanceolate, 50–70 × 5–20 mm, glabrous or sparsely stipitate-glandular proximally.</text>
      <biological_entity id="o10999" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o11000" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s1" to="lanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s1" to="70" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences stipitate-glandular, sometimes glabrous;</text>
      <biological_entity id="o11001" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts similar to distal leaves.</text>
      <biological_entity id="o11002" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o11003" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o11002" id="r894" name="to" negation="false" src="d0_s3" to="o11003" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 1–7 mm, stipitate-glandular.</text>
      <biological_entity id="o11004" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 5–10 mm, stipitate-glandular;</text>
      <biological_entity id="o11005" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o11006" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla palate yellow;</text>
      <biological_entity id="o11007" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="corolla" id="o11008" name="palate" name_original="palate" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 20–40 mm.</text>
      <biological_entity id="o11009" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o11010" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 7–10 mm wide.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 16 (Europe).</text>
      <biological_entity id="o11011" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11012" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Antirrhinum majus is a popular garden plant grown as an annual; it occasionally escapes but is short-lived. Some cultivars have been developed with different growth forms, corolla colors, or open-throated flowers.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed ground.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ground" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Ont., Que.; Calif., Conn., D.C., Ill., Iowa, La., Mass., Mich., Mo., N.Y., Ohio, Oreg., Pa., Utah, Vt., Va., Wash., Wis.; sw Europe; introduced also in Mexico, West Indies, Central America, South America, Asia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="sw Europe" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>