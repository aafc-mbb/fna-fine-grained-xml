<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="O’Kane &amp; K. D. Heil" date="2014" rank="species">bleaklyi</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2014-61: 1, figs. 1–3. 2014</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species bleaklyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>146.</number>
  <other_name type="common_name">Bleakly’s beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o8773" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending, 2–8 cm, retrorsely hairy, not glaucous.</text>
      <biological_entity id="o8774" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, not leathery, glabrous;</text>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 15–35 × 4–15 mm, blade lanceolate to elliptic or oblanceolate, base tapered, margins entire or serrulate, apex obtuse to acute;</text>
      <biological_entity id="o8775" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o8776" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o8777" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="elliptic or oblanceolate" />
      </biological_entity>
      <biological_entity id="o8778" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o8779" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o8780" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–7 pairs, petiolate, 10–20 × 3–5 mm, blade lanceolate to linear-lanceolate, base tapered to cuneate, margins entire or serrulate, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o8781" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8782" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o8783" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s4" to="cuneate" />
      </biological_entity>
      <biological_entity id="o8784" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o8785" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses continuous, ± secund, 1–3 cm, axis glandular-pubescent, verticillasters 1 or 2, cymes (1 or) 2-flowered or 3-flowered, 1 or 2 per node;</text>
      <biological_entity id="o8786" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="continuous" value_original="continuous" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8787" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o8788" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8789" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-flowered" value_original="2-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-flowered" value_original="3-flowered" />
        <character name="quantity" src="d0_s5" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8790" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, 10–20 × 4–8 mm, margins entire;</text>
      <biological_entity constraint="proximal" id="o8791" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8792" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels ascending to erect, glandular-pubescent.</text>
      <biological_entity id="o8793" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o8794" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes lanceolate, 5–8 × 0.8–1.2 mm, glandular-pubescent;</text>
      <biological_entity id="o8795" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o8796" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla bluish lavender to lavender-purple, with violet nectar guides, tubular-funnelform, 12–25 mm, glandular-pubescent externally, densely white-lanate internally abaxially, tube 4–6 mm, throat gradually inflated, 6–8 mm diam., scarcely 2-ridged abaxially;</text>
      <biological_entity id="o8797" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8798" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="bluish lavender" name="coloration" src="d0_s9" to="lavender-purple" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="densely; internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o8799" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o8800" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8801" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="scarcely; abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o8798" id="r713" name="with" negation="false" src="d0_s9" to="o8799" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, subexplanate, 1.1–1.3 mm, dehiscing completely, connective splitting, sides sparsely puberulent, sutures smooth;</text>
      <biological_entity id="o8802" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8803" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o8804" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s10" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o8805" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o8806" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o8807" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 14–15 mm, exserted, 0.3–0.4 mm diam., tip slightly recurved, distal 0.8–1 mm sparsely lanate, hairs yellow, to 0.4 mm;</text>
      <biological_entity id="o8808" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8809" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8810" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8811" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="lanate" value_original="lanate" />
      </biological_entity>
      <biological_entity id="o8812" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 10–15 mm.</text>
      <biological_entity id="o8813" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8814" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–7 × 3–5 mm, glabrous.</text>
      <biological_entity id="o8815" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon bleaklyi is known only from the Culebra Range of the Sangre de Cristo Mountains in Taos County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine scree slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine scree" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3800–3900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3900" to_unit="m" from="3800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>