<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Kerry A. Barringer</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">16</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Rothmaler" date="1954" rank="genus">HOWELLIELLA</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>7: 115. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus howelliella</taxon_hierarchy>
    <other_info_on_name type="etymology">For John Thomas Howell, 1903–1994, California botanist</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o31227" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, filiform, twining branches absent, glandular-pubescent.</text>
      <biological_entity id="o31228" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o31229" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s1" value="twining" value_original="twining" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, opposite proximally, alternate distally;</text>
      <biological_entity id="o31230" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="proximally" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present proximally, nearly absent distally;</text>
      <biological_entity id="o31231" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="nearly; distally" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade not fleshy, not leathery, margins entire.</text>
      <biological_entity id="o31232" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o31233" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, racemes;</text>
      <biological_entity id="o31234" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o31235" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts present.</text>
      <biological_entity id="o31236" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present, not twining;</text>
      <biological_entity id="o31237" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s7" value="twining" value_original="twining" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles absent.</text>
      <biological_entity id="o31238" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o31239" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, basally connate, calyx bilaterally symmetric, tubular to cupulate, lobes ovate or lanceolate, adaxial lobe larger;</text>
      <biological_entity id="o31240" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o31241" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="tubular" name="shape" src="d0_s10" to="cupulate" />
      </biological_entity>
      <biological_entity id="o31242" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31243" name="lobe" name_original="lobe" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="larger" value_original="larger" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla creamy white to pale-pink, bilaterally symmetric, strongly bilabiate, tubular, tube base gibbous abaxially, not spurred, lobes 5, abaxial 3, adaxial 2;</text>
      <biological_entity id="o31244" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s11" to="pale-pink" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity constraint="tube" id="o31245" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s11" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o31246" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31247" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31248" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, basally adnate to corolla, didynamous, filaments glandular-hairy distally, pollen-sacs 2 per filament;</text>
      <biological_entity id="o31249" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character constraint="to corolla" constraintid="o31250" is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o31250" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o31251" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s12" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity id="o31252" name="pollen-sac" name_original="pollen-sacs" src="d0_s12" type="structure">
        <character constraint="per filament" constraintid="o31253" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o31253" name="filament" name_original="filament" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>staminode 0;</text>
      <biological_entity id="o31254" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o31255" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma punctiform.</text>
      <biological_entity id="o31256" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="punctiform" value_original="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, locules equal to subequal, dehiscence poricidal.</text>
      <biological_entity constraint="fruits" id="o31257" name="capsule" name_original="capsules" src="d0_s16" type="structure" />
      <biological_entity id="o31258" name="locule" name_original="locules" src="d0_s16" type="structure">
        <character is_modifier="false" name="variability" src="d0_s16" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="poricidal" value_original="poricidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 15–50, brown to black, ovoid to subconical, wings absent.</text>
      <biological_entity id="o31259" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s17" to="50" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="black" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="subconical" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 16.</text>
      <biological_entity id="o31260" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o31261" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Howelliella is rare and closely related to Sairocarpus, but the palate does not block the mouth of the corolla tube, the floor of the corolla tube has two longitudinal folds, and the abaxial lip is relatively small. The corolla tube is slightly curved in Howelliella; it is straight in related species. Morphological and molecular studies agree that Howelliella is most closely related to S. subcordatus and S. vexillocalyculatus, which also have greatly enlarged adaxial calyx lobes (D. M. Thompson 1988; R. K. Oyama and D. A. Baum 2004; M. Fernández-Mazuecos et al. 2013).</discussion>
  
</bio:treatment>