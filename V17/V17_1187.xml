<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Kerry A. Barringer</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">666</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">670</other_info_on_meta>
    <other_info_on_meta type="mention_page">679</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Behr" date="1855" rank="genus">CHLOROPYRON</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 61. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus chloropyron</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chloros, green or yellow-green, and pyros, fire, hence red or yellow, alluding to yellowish green plants</other_info_on_name>
  </taxon_identification>
  <number>23.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>hemiparasitic.</text>
      <biological_entity id="o8887" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="nutrition" src="d0_s1" value="hemiparasitic" value_original="hemiparasitic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, spreading, or decumbent, not fleshy, puberulent, hispid, or villous, sometimes glandular-hairy or glabrescent.</text>
      <biological_entity id="o8888" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glandular-hairy" value_original="glandular-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, alternate;</text>
      <biological_entity id="o8889" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o8890" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, not leathery, margins entire or pinnately 5-lobed or 7-lobed.</text>
      <biological_entity id="o8891" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o8892" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="7-lobed" value_original="7-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, spikes;</text>
      <biological_entity id="o8893" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o8894" name="spike" name_original="spikes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o8895" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels absent;</text>
      <biological_entity id="o8896" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles absent.</text>
      <biological_entity id="o8897" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 2, calyx bilaterally symmetric, spathelike, lobes narrowly triangular to triangular;</text>
      <biological_entity id="o8898" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8899" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8900" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spathe-like" value_original="spathelike" />
      </biological_entity>
      <biological_entity id="o8901" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s10" to="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, corolla white, yellow, pink, or lavender, often marked or tinted with pink to purple-red lines or spots, strongly bilabiate, club-shaped, abaxial lobes 3, middle lobe erect, not revolute, adaxial 2, adaxial lip galeate, rounded at apex, opening downward;</text>
      <biological_entity id="o8902" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8903" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o8904" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="lavender" value_original="lavender" />
        <character constraint="with spots" constraintid="o8906" is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="tinted" value_original="tinted" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <biological_entity id="o8905" name="line" name_original="lines" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink" is_modifier="true" name="coloration" src="d0_s11" to="purple-red" />
      </biological_entity>
      <biological_entity id="o8906" name="spot" name_original="spots" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial" id="o8907" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="middle" id="o8908" name="lobe" name_original="lobe" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8909" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8910" name="lip" name_original="lip" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="galeate" value_original="galeate" />
        <character constraint="at apex" constraintid="o8911" is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s11" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o8911" name="apex" name_original="apex" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 2 or 4, didynamous, filaments glabrous or sparsely pilose, pollen-sacs approximate, connective not elongate;</text>
      <biological_entity id="o8912" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8913" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s12" unit="or" value="4" value_original="4" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o8914" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o8915" name="pollen-sac" name_original="pollen-sacs" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="approximate" value_original="approximate" />
      </biological_entity>
      <biological_entity id="o8916" name="connective" name_original="connective" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminodes 0 or 2, peglike;</text>
      <biological_entity id="o8917" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8918" name="staminode" name_original="staminodes" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s13" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="peglike" value_original="peglike" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o8919" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o8920" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma slightly expanded at apex.</text>
      <biological_entity id="o8921" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o8922" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character constraint="at apex" constraintid="o8923" is_modifier="false" modifier="slightly" name="size" src="d0_s15" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o8923" name="apex" name_original="apex" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules: dehiscence loculicidal.</text>
      <biological_entity id="o8924" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 8–40, brown to dark-brown, ovoid to ± reniform, wings absent.</text>
      <biological_entity id="o8925" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s17" to="40" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="dark-brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="more or less reniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 7.</text>
      <biological_entity id="o8926" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o8927" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (4 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species of Chloropyron grow almost exclusively in saline and alkaline habitats.</discussion>
  <discussion>Chloropyron is similar to Cordylanthus and Dicranostegia and, like them, has upright flowers in which the abaxial corolla lip is usually held close to the galeate adaxial lip. The flowers appear to be in bud even when they are fully open.</discussion>
  <discussion>T. I. Chuang and L. R. Heckard (1973) placed species of Chloropyron in Cordylanthus as subg. Hemistegia (A. Gray) Jepson. With evidence from molecular data, D. C. Tank et al. (2009) have shown that Chloropyron is closely related to Dicranostegia and that both genera form a clade that is sister to Castilleja and Triphysaria, while Cordylanthus in the narrow sense is somewhat unresolved at the base of the subtribe Castillejinae clade.</discussion>
  <references>
    <reference>Chuang, T. I. and L. R. Heckard. 1973. Taxonomy of Cordylanthus subgenus Hemistegia (Scrophulariaceae). Brittonia 25: 135–158.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stamens 4, staminodes 0; bract margins entire or distally 2-toothed.</description>
      <determination>1. Chloropyron maritimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stamens 2, staminodes 2; bract margins pinnately lobed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 1–2 mm wide, linear to linear-lanceolate; styles puberulent.</description>
      <determination>4. Chloropyron tecopense</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 2–10 mm wide, narrowly lanceolate to lanceolate; styles glabrous.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Calyces 15–20 mm; seeds without abaxial crest; stems puberulent or hispid.</description>
      <determination>2. Chloropyron molle</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Calyces 12–15 mm; seeds with abaxial crest; stems sparsely pilose or glabrescent.</description>
      <determination>3. Chloropyron palmatum</determination>
    </key_statement>
  </key>
</bio:treatment>