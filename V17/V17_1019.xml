<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">585</other_info_on_meta>
    <other_info_on_meta type="mention_page">584</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1833" rank="species">ambigua</taxon_name>
    <taxon_name authority="(D. D. Keck) J. M. Egger" date="2008" rank="variety">humboldtiensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>90: 67. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species ambigua;variety humboldtiensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">castillejoides</taxon_name>
    <taxon_name authority="D. D. Keck" date="1927" rank="variety">humboldtiensis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 16: 536. 1927</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orthocarpus;species castillejoides;variety humboldtiensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ambigua</taxon_name>
    <taxon_name authority="(D. D. Keck) T. I. Chuang &amp; Heckard" date="unknown" rank="subspecies">humboldtiensis</taxon_name>
    <taxon_hierarchy>genus castilleja;species ambigua;subspecies humboldtiensis</taxon_hierarchy>
  </taxon_identification>
  <number>2b.</number>
  <other_name type="common_name">Humboldt Bay owl’s-clover</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ± short-decumbent proximally, becoming erect to ascending, unbranched or few-branched from mid-stem.</text>
      <biological_entity id="o29472" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less; proximally" name="growth_form_or_orientation" src="d0_s0" value="short-decumbent" value_original="short-decumbent" />
        <character char_type="range_value" from="erect" modifier="becoming" name="orientation" src="d0_s0" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="from mid-stem" constraintid="o29473" is_modifier="false" name="architecture" src="d0_s0" value="few-branched" value_original="few-branched" />
      </biological_entity>
      <biological_entity id="o29473" name="mid-stem" name_original="mid-stem" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves narrowly oblong to linear, oblong, or lanceolate, cupshaped, ± fleshy, apex rounded.</text>
      <biological_entity id="o29474" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s1" to="linear oblong or lanceolate" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s1" to="linear oblong or lanceolate" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s1" to="linear oblong or lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o29475" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bracts proximally green or brownish purple, distally pink to purple on lobe apices;</text>
      <biological_entity id="o29476" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="brownish purple distally pink" name="coloration" src="d0_s2" to="purple" />
        <character char_type="range_value" constraint="on lobe apices" constraintid="o29477" from="brownish purple distally pink" name="coloration" src="d0_s2" to="purple" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o29477" name="apex" name_original="apices" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>lobes ascending, oblong, 20–23 mm, usually arising above mid length.</text>
      <biological_entity id="o29478" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s3" to="23" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="length" src="d0_s3" value="arising" value_original="arising" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyces with all 4 clefts subequal;</text>
      <biological_entity id="o29479" name="calyx" name_original="calyces" src="d0_s4" type="structure" />
      <biological_entity id="o29480" name="cleft" name_original="clefts" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="4" value_original="4" />
        <character is_modifier="false" name="size" src="d0_s4" value="subequal" value_original="subequal" />
      </biological_entity>
      <relation from="o29479" id="r2240" name="with" negation="false" src="d0_s4" to="o29480" />
    </statement>
    <statement id="d0_s5">
      <text>lateral clefts 3 mm, ca. 15% of calyx length.</text>
      <biological_entity constraint="lateral" id="o29481" name="cleft" name_original="clefts" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 21–24 mm;</text>
      <biological_entity id="o29482" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="21" from_unit="mm" name="some_measurement" src="d0_s6" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>beak pink or purplish, 5–7 mm;</text>
      <biological_entity id="o29483" name="beak" name_original="beak" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>abaxial lip yellow, becoming orange or red-purple after anthesis;</text>
      <biological_entity constraint="abaxial" id="o29484" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character constraint="after anthesis" is_modifier="false" name="coloration" src="d0_s8" value="red-purple" value_original="red-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>teeth deep pinkish purple, often with whitish bases.</text>
      <biological_entity id="o29485" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish purple" value_original="pinkish purple" />
      </biological_entity>
      <biological_entity id="o29486" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
      </biological_entity>
      <relation from="o29485" id="r2241" modifier="often" name="with" negation="false" src="d0_s9" to="o29486" />
    </statement>
  </description>
  <discussion>Variety humboldtiensis has purplish flowers and bract apices and is limited to the upper edge of Salicornia-dominated salt marshes in Humboldt and Mendocino counties, where it replaces var. ambigua. Most populations are around Humboldt Bay. Reports of var. humboldtiensis from Tomales Bay in Marin County are based on plants of the salt marsh form of var. ambigua and have yellowish flowers. Variety humboldtiensis is threatened by coastal development.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Upper margins of Salicornia salt marshes, saline flats.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="upper margins" constraint="of salicornia salt marshes" />
        <character name="habitat" value="salicornia salt marshes" />
        <character name="habitat" value="flats" modifier="saline" />
        <character name="habitat" value="saline" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>