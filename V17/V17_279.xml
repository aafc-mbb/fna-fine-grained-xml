<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Caespitosi</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">linarioides</taxon_name>
    <taxon_name authority="(A. Nelson) C. C. Freeman" date="2017" rank="variety">coloradoensis</taxon_name>
    <place_of_publication>
      <publication_title>PhytoKeys</publication_title>
      <place_in_publication>80: 35. 2017</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section caespitosi;species linarioides;variety coloradoensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="A. Nelson" date="1899" rank="species">coloradoensis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 355. 1899</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species coloradoensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">linarioides</taxon_name>
    <taxon_name authority="(A. Nelson) D. D. Keck" date="unknown" rank="subspecies">coloradoensis</taxon_name>
    <taxon_hierarchy>genus p.;species linarioides;subspecies coloradoensis</taxon_hierarchy>
  </taxon_identification>
  <number>20b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 10–30 cm.</text>
      <biological_entity id="o30270" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves moderately to densely retrorsely hairy, hairs appressed, white, scalelike, cauline 4–23 × 0.8–2 mm, blade oblanceolate to linear.</text>
      <biological_entity id="o30271" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately to densely retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o30272" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s1" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o30273" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s1" to="23" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30274" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: staminode: distal 1 mm densely pilose, hairs yellow, to 0.8 mm, rest of distal 3–4 mm glabrous or sparsely pilose (with much shorter hairs).</text>
      <biological_entity id="o30275" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o30276" name="staminode" name_original="staminode" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o30277" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30279" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <relation from="o30278" id="r2296" name="part_of" negation="false" src="d0_s2" to="o30279" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 16.</text>
      <biological_entity id="o30278" name="hair" name_original="hairs" src="d0_s2" type="structure" constraint="flower" constraint_original="flower; flower">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30280" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety coloradoensis is known from northeastern Arizona (Apache and Navajo counties), southwestern Colorado (Dolores, La Plata, Montezuma, and San Miguel counties), and northwestern New Mexico (Rio Arriba and San Juan counties).</discussion>
  <discussion>A decoction made from var. coloradoensis is used as a gynecological aid by the Ramah Navajo of western New Mexico (D. E. Moerman 1998).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, pinyon-juniper and scrub oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="scrub oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>