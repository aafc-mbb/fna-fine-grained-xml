<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">291</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PLANTAGO</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">pusilla</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 100. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus plantago;species pusilla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Plantago</taxon_name>
    <taxon_name authority="W. P. C. Barton" date="unknown" rank="species">hybrida</taxon_name>
    <taxon_hierarchy>genus plantago;species hybrida</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pusilla</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">major</taxon_name>
    <taxon_hierarchy>genus p.;species pusilla;variety major</taxon_hierarchy>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Dwarf plantain</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
      <biological_entity id="o34869" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots taproots, slender.</text>
      <biological_entity constraint="roots" id="o34870" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 0–5 mm.</text>
      <biological_entity id="o34871" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 20–70 × 1–2 mm;</text>
      <biological_entity id="o34872" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="70" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, margins entire, sometimes toothed, veins conspicuous or not, surfaces hairy or glabrous.</text>
      <biological_entity id="o34873" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o34874" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o34875" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character name="prominence" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o34876" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scapes 15–60 mm, hairy, sometimes glabrous.</text>
      <biological_entity id="o34877" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="60" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikes greenish or brownish, 20–100 mm, loosely or densely flowered;</text>
      <biological_entity id="o34878" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="100" to_unit="mm" />
        <character is_modifier="false" modifier="loosely; densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts triangular-ovate, 1.5–2 mm, length 0.9–1.1 times sepals.</text>
      <biological_entity id="o34879" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character constraint="sepal" constraintid="o34880" is_modifier="false" name="length" src="d0_s7" value="0.9-1.1 times sepals" value_original="0.9-1.1 times sepals" />
      </biological_entity>
      <biological_entity id="o34880" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 1.5–2 mm;</text>
      <biological_entity id="o34881" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o34882" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla radially symmetric, lobes erect, forming a beak, 0.5 mm, base obtuse;</text>
      <biological_entity id="o34883" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o34884" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o34885" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o34886" name="beak" name_original="beak" src="d0_s9" type="structure" />
      <biological_entity id="o34887" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o34885" id="r2662" name="forming a" negation="false" src="d0_s9" to="o34886" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 2.</text>
      <biological_entity id="o34888" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34889" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 4, 0.8–1.3 mm. 2n = 12.</text>
      <biological_entity id="o34890" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" unit=",0.8-1.3 mm" />
        <character name="some_measurement" src="d0_s11" unit=",0.8-1.3 mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34891" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plantago pusilla is considered to be introduced in Oregon and Washington, and possibly in California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to moist, sandy, alluvial soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to moist" />
        <character name="habitat" value="dry to sandy" />
        <character name="habitat" value="dry to alluvial soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Calif., Conn., Del., D.C., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Mass., Miss., Mo., N.J., N.Y., N.C., Okla., Oreg., Pa., R.I., S.C., Tenn., Tex., Va., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>