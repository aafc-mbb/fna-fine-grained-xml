<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1846" rank="section">Saccanthera</taxon_name>
    <taxon_name authority="A. Gray" date="1859" rank="species">laetus</taxon_name>
    <taxon_name authority="(D. D. Keck) McMinn" date="1939" rank="variety">sagittatus</taxon_name>
    <place_of_publication>
      <publication_title>Man. Calif. Shrubs,</publication_title>
      <place_in_publication>518. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section saccanthera;species laetus;variety sagittatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laetus</taxon_name>
    <taxon_name authority="D. D. Keck" date="1932" rank="subspecies">sagittatus</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>16: 395, fig. 12. 1932</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species laetus;subspecies sagittatus</taxon_hierarchy>
  </taxon_identification>
  <number>211c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers: calyx lobes ovate to lanceolate, 4–6 × 1–2.3 mm;</text>
      <biological_entity id="o26788" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity constraint="calyx" id="o26789" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s0" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s0" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>anthers sagittate in outline, pollen-sacs 2.1–2.8 mm.</text>
      <biological_entity id="o26790" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o26791" name="anther" name_original="anthers" src="d0_s1" type="structure">
        <character constraint="in outline" constraintid="o26792" is_modifier="false" name="shape" src="d0_s1" value="sagittate" value_original="sagittate" />
      </biological_entity>
      <biological_entity id="o26792" name="outline" name_original="outline" src="d0_s1" type="structure" />
      <biological_entity id="o26793" name="pollen-sac" name_original="pollen-sacs" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="distance" src="d0_s1" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety sagittatus occurs in the Cascade Range of northern California and south-central Oregon. In addition to its sagittate anthers, it usually has glabrate leaves, though this character varies among populations.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, coniferous forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>