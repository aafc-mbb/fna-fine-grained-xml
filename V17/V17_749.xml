<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Link" date="1820" rank="species">stelleri</taxon_name>
    <place_of_publication>
      <publication_title>Jahrb. Gewächsk.</publication_title>
      <place_in_publication>1(3): 40. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species stelleri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Veronica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">stelleri</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="variety">glabrescens</taxon_name>
    <taxon_hierarchy>genus veronica;species stelleri;variety glabrescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="Roemer &amp; Schultes" date="unknown" rank="species">wormskjoldii</taxon_name>
    <taxon_name authority="(Link) S. L. Welsh" date="unknown" rank="variety">stelleri</taxon_name>
    <taxon_hierarchy>genus v.;species wormskjoldii;variety stelleri</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Steller's speedwell</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o20078" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, unbranched, (5–) 10–20 (–25) cm, eglandular-hairy.</text>
      <biological_entity id="o20079" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade ovate, 15–30 × 10–20 mm, base rounded, margins dentate or serrate, apex subacute to subobtuse, surfaces eglandular-hairy.</text>
      <biological_entity id="o20080" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20081" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20082" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20083" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o20084" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s2" to="subobtuse" />
      </biological_entity>
      <biological_entity id="o20085" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1, terminal, 10–60 mm, to 100 mm in fruit, 5–15-flowered, axis hairy;</text>
      <biological_entity id="o20086" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o20087" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="100" to_unit="mm" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="5-15-flowered" value_original="5-15-flowered" />
      </biological_entity>
      <biological_entity id="o20087" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
      <biological_entity id="o20088" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts lanceolate, 2–5 mm.</text>
      <biological_entity id="o20089" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels erect, 2–11 mm, equal to longer than subtending bract, crisp-hairy.</text>
      <biological_entity id="o20090" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character constraint="than subtending bract" constraintid="o20091" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="crisp-hairy" value_original="crisp-hairy" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o20091" name="bract" name_original="bract" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes 4–5 mm, eglandular-ciliate, apex acuminate, glabrous or sparsely hairy;</text>
      <biological_entity id="o20092" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o20093" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="eglandular-ciliate" value_original="eglandular-ciliate" />
      </biological_entity>
      <biological_entity id="o20094" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla pale blue or violet, rotate, 8–10 mm diam.;</text>
      <biological_entity id="o20095" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o20096" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale blue" value_original="pale blue" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="violet" value_original="violet" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" value_original="rotate" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 7 mm;</text>
      <biological_entity id="o20097" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o20098" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 3–6 mm.</text>
      <biological_entity id="o20099" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20100" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules compressed in cross-section, oblong, 5–6 (–8) × 3–4.5 mm, longer than wide, apex ± emarginate, glabrous proximally, hairy distally.</text>
      <biological_entity id="o20101" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="in cross-section" constraintid="o20102" is_modifier="false" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
      <biological_entity id="o20102" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o20103" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds number unknown, brown, ovoid, flat, 1–1.5 × 0.7–1.2 mm, thickness and texture unknown.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 18 (Japan, Russia).</text>
      <biological_entity id="o20104" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character char_type="range_value" from="1" from_unit="mm" name="texture" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="texture" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20105" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Veronica stelleri may be difficult to differentiate from 4b. V. wormskjoldii subsp. nutans where sympatric.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to moderately moist slopes, meadows in alpine regions, moraines.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to moist slopes" modifier="moderately" />
        <character name="habitat" value="meadows" constraint="in alpine regions , moraines" />
        <character name="habitat" value="alpine regions" />
        <character name="habitat" value="moraines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Asia (Japan, Korea, Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>