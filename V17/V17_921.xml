<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">525</other_info_on_meta>
    <other_info_on_meta type="mention_page">526</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="Fischer ex Steven" date="1822" rank="species">langsdorffii</taxon_name>
    <taxon_name authority="(R. Brown) Pennell ex Hultén" date="1968" rank="subspecies">arctica</taxon_name>
    <place_of_publication>
      <publication_title>Ark. Bot., n. s.</publication_title>
      <place_in_publication>7: 122. 1968</place_in_publication>
      <other_info_on_pub>(as langsdorfii)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species langsdorffii;subspecies arctica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pedicularis</taxon_name>
    <taxon_name authority="R. Brown" date="1823" rank="species">arctica</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Melvill.,</publication_title>
      <place_in_publication>22. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus pedicularis;species arctica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">langsdorffii</taxon_name>
    <taxon_name authority="(R. Brown) Polunin" date="unknown" rank="variety">arctica</taxon_name>
    <taxon_hierarchy>genus p.;species langsdorffii;variety arctica</taxon_hierarchy>
  </taxon_identification>
  <number>22b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Racemes: bracts moreorless tomentose.</text>
      <biological_entity id="o37041" name="raceme" name_original="racemes" src="d0_s0" type="structure" />
      <biological_entity id="o37042" name="bract" name_original="bracts" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: calyx moreorless tomentose.</text>
      <biological_entity id="o37043" name="flower" name_original="flowers" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>2n = 16.</text>
      <biological_entity id="o37044" name="calyx" name_original="calyx" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o37045" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Citing N. Polunin (1940), S. G. Aiken et al. (2007) reiterated that subsp. arctica grades into Pedicularis hirsuta. L. W. Macior (1975) showed partial fertility between P. langsdorffii and P. lanata, another species with densely lanate inflorescences. The apparent absence of strong reproductive barriers suggests the status of these taxa as species might be questioned and may explain the apparent character overlap between subsp. arctica and subsp. langsdorffii.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist heath, grassy or rocky meadows, dry arctic and alpine tundras, moist stream banks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist heath" />
        <character name="habitat" value="grassy" />
        <character name="habitat" value="rocky meadows" />
        <character name="habitat" value="dry arctic" />
        <character name="habitat" value="alpine tundras" />
        <character name="habitat" value="stream banks" modifier="moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.W.T., Nunavut, Yukon; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>