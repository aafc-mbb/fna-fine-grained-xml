<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">652</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(Jepson) T. I. Chuang &amp; Heckard" date="1991" rank="species">rubicundula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">rubicundula</taxon_name>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species rubicundula;variety rubicundula</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="A. Heller" date="unknown" rank="species">bicolor</taxon_name>
    <taxon_hierarchy>genus orthocarpus;species bicolor</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">lithospermoides</taxon_name>
    <taxon_name authority="(A. Heller) Jepson" date="unknown" rank="variety">bicolor</taxon_name>
    <taxon_hierarchy>genus o.;species lithospermoides;variety bicolor</taxon_hierarchy>
  </taxon_identification>
  <number>99a.</number>
  <other_name type="common_name">Pink cream-sacs</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs 0.6–6 dm.</text>
      <biological_entity id="o16983" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 0–7-lobed, lobes widely spreading.</text>
      <biological_entity id="o16984" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="0-7-lobed" value_original="0-7-lobed" />
      </biological_entity>
      <biological_entity id="o16985" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bracts 5–9-lobed.</text>
      <biological_entity id="o16986" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="5-9-lobed" value_original="5-9-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Corollas: abaxial lip white, in most populations quickly fading to pink or pink-purple, usually purple-dotted at base, teeth white.</text>
      <biological_entity id="o16987" name="corolla" name_original="corollas" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o16988" name="lip" name_original="lip" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o16989" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="quickly fading" is_modifier="true" name="coloration" src="d0_s3" to="pink or pink-purple" />
        <character is_modifier="true" modifier="usually" name="coloration" src="d0_s3" value="purple-dotted" value_original="purple-dotted" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o16990" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="quickly fading" is_modifier="true" name="coloration" src="d0_s3" to="pink or pink-purple" />
        <character is_modifier="true" modifier="usually" name="coloration" src="d0_s3" value="purple-dotted" value_original="purple-dotted" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
      <relation from="o16988" id="r1317" name="in" negation="false" src="d0_s3" to="o16989" />
      <relation from="o16988" id="r1318" name="in" negation="false" src="d0_s3" to="o16990" />
    </statement>
  </description>
  <discussion>Variety rubicundula grows in the northern and western portions of the Sacramento Valley, the North Coast Range, and San Francisco Bay area of California, often on serpentine substrates. It is far less common than var. lithospermoides and often occurs in somewhat more mesic conditions. Populations in the north-central Sacramento Valley contain unusually robust plants with especially large flowers, and were named Orthocarpus bicolor. Variety rubicundula is of conservation concern, threatened by habitat loss to development, agriculture, and grazing.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jun(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, damp meadows, springs, woodland edges, dry rocky slopes, coastal valleys and foothills, often on serpentine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="damp meadows" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="edges" modifier="woodland" />
        <character name="habitat" value="dry rocky slopes" />
        <character name="habitat" value="coastal valleys" />
        <character name="habitat" value="foothills" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>