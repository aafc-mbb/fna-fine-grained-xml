<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Coerulei</taxon_name>
    <taxon_name authority="S. Watson" date="1891" rank="species">haydenii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>16: 311. 1891</place_in_publication>
      <other_info_on_pub>(as Pentstemon haydeni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section coerulei;species haydenii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>40.</number>
  <other_name type="common_name">Blowout beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants: caudex rhizomelike.</text>
      <biological_entity id="o32185" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o32186" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomelike" value_original="rhizomelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to ascending, (15–) 20–48 cm, glabrous.</text>
      <biological_entity id="o32187" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="48" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves essentially cauline, basal absent or reduced, glabrous;</text>
      <biological_entity id="o32188" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o32189" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="essentially" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline (25–) 55–130 (–175) × 3–25 mm, blade linear to lanceolate, base tapered, apex acute to acuminate;</text>
      <biological_entity constraint="basal" id="o32190" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o32191" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o32192" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o32193" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o32194" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 2–10 pairs, sessile, 60–110 (–120) × 7–30 mm, blade lanceolate to linear, base clasping, apex acuminate to long-acuminate.</text>
      <biological_entity constraint="cauline" id="o32195" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="10" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="110" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="110" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32196" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
      </biological_entity>
      <biological_entity id="o32197" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o32198" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses continuous, cylindric, (2–) 6–21 (–34) cm, axis glabrous, verticillasters (2–) 6–10 (–17), cymes 1–8-flowered;</text>
      <biological_entity id="o32199" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="21" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="34" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="21" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32200" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32201" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="6" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="17" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o32202" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-8-flowered" value_original="1-8-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts ovate, 58–120 × 20–45 mm;</text>
      <biological_entity constraint="proximal" id="o32203" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="58" from_unit="mm" name="length" src="d0_s6" to="120" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s6" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels glabrous.</text>
      <biological_entity id="o32204" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o32205" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes lanceolate to linear, 8–13 × 1–3 mm, margins entire or erose, herbaceous or scarious, glabrous;</text>
      <biological_entity id="o32206" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o32207" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32208" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="erose" value_original="erose" />
        <character is_modifier="false" name="texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla lavender to bluish, usually with magenta nectar guides, ampliate, 21–28 mm, glabrous externally, glabrous internally, tube 7–9 mm, throat abruptly inflated, 9–11 mm diam., rounded abaxially;</text>
      <biological_entity id="o32209" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32210" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s9" to="bluish" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="21" from_unit="mm" name="some_measurement" src="d0_s9" to="28" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o32211" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="magenta" value_original="magenta" />
      </biological_entity>
      <biological_entity id="o32212" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32213" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o32210" id="r2442" modifier="usually" name="with" negation="false" src="d0_s9" to="o32211" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, 1.1–2 mm, sutures papillate;</text>
      <biological_entity id="o32214" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o32215" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o32216" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32217" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 13–16 mm, included, 1.2–3 mm diam., tip recurved, distal 0.1–5 mm sparsely to densely villous, hairs golden yellow, to 1 mm, rarely glabrous;</text>
      <biological_entity id="o32218" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o32219" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="diameter" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32220" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32221" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o32222" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 13–23 mm.</text>
      <biological_entity id="o32223" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o32224" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s12" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 8–12 × 5–9 mm. 2n = 16.</text>
      <biological_entity id="o32225" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s13" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32226" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon haydenii is known from the Nebraska Sandhills, where extant populations occur in Box Butte, Cherry, Garden, Hooker, Morrill, and Sheridan counties. Historic populations occurred in Thomas County (D. M. Sutherland 1988).</discussion>
  <discussion>Penstemon haydenii was discovered in northern Carbon County, Wyoming, in the late 1990s some 300 km west of the nearest Nebraska populations; it might have been collected in Wyoming as early as 1877 (W. Fertig 2001). A morphometric analysis of Nebraska and Wyoming plants revealed differences that could justify recognition of Wyoming populations as a distinct variety (C. C. Freeman 2015). It is listed as endangered by the U.S. Department of the Interior.</discussion>
  <references>
    <reference>Fertig, W. 2001. Survey for Blowout Penstemon (Penstemon haydenii) in Wyoming. Laramie.</reference>
    <reference>Freeman, C. C. 2015. Final Report on an Assessment of the Status of Blowout Beardtongue (Penstemon haydenii S. Watson, Plantaginaceae) Using Molecular and Morphometric Approaches. Lawrence, Kans.</reference>
    <reference>Sutherland, D. M. 1988. Historical notes on collections and taxonomy of Penstemon haydenii S. Wats. (blowout penstemon), Nebraska’s only endemic plant species. Trans. Nebraska Acad. Sci. 16: 191–194.</reference>
  </references>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Blowouts in sand dunes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="blowouts" constraint="in sand dunes" />
        <character name="habitat" value="sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nebr., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>