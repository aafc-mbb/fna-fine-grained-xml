<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Caespitosi</taxon_name>
    <taxon_name authority="A. Nelson" date="1899" rank="species">crandallii</taxon_name>
    <taxon_name authority="(A. Nelson) C. C. Freeman" date="2017" rank="variety">ramaleyi</taxon_name>
    <place_of_publication>
      <publication_title>PhytoKeys</publication_title>
      <place_in_publication>80: 35. 2017</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section caespitosi;species crandallii;variety ramaleyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="A. Nelson" date="1937" rank="species">ramaleyi</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Univ. Wyoming</publication_title>
      <place_in_publication>3: 106. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species ramaleyi</taxon_hierarchy>
  </taxon_identification>
  <number>18e.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 6–25 cm, puberulent to pubescent.</text>
      <biological_entity id="o5456" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s0" to="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves glabrate or scabrous to pubescent, 7–47 × 0.5–2 mm;</text>
      <biological_entity id="o5457" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s1" to="pubescent" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s1" to="47" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oblanceolate to linear.</text>
      <biological_entity id="o5458" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="linear" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety ramaleyi has been largely overlooked; D. D. Keck (1937) did not account for it. The variety is similar in habit to var. glabrescens but has spreading hairs on the stems and, usually, on the leaves; var. glabrescens typically bears relatively shorter, retrorse hairs. Also, the leaves are longer and the stems more stiffly erect in var. ramaleyi than in var. glabrescens. Variety ramaleyi is known from Hinsdale, Mineral, and Saguache counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush shrublands, pinyon-juniper and aspen woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush shrublands" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="aspen woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2400–3300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="2400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>