<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">26</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="genus">KICKXIA</taxon_name>
    <taxon_name authority="(Linnaeus) Dumortier" date="1827" rank="species">elatine</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Belg.,</publication_title>
      <place_in_publication>35. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus kickxia;species elatine</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antirrhinum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">elatine</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 612. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus antirrhinum;species elatine</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Sharpleaf cancerwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–120 cm.</text>
      <biological_entity id="o15341" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 2–6 mm;</text>
      <biological_entity id="o15342" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15343" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade: proximal 15–35 × 10–30 mm, base truncate or rounded to cuneate, distal 4–25 × 2–14 mm, base hastate to sagittate, villous with scattered glandular-hairs.</text>
      <biological_entity id="o15344" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o15345" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15346" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15347" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15348" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="hastate" name="shape" src="d0_s2" to="sagittate" />
        <character constraint="with glandular-hairs" constraintid="o15349" is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o15349" name="glandular-hair" name_original="glandular-hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 10–22 mm.</text>
      <biological_entity id="o15350" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepal lobes lanceolate, 3–6 × 1–2 mm, not accrescent, apex acuminate, villous;</text>
      <biological_entity id="o15351" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="sepal" id="o15352" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s4" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <biological_entity id="o15353" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla-tube 1–2 mm, palate yellow, inflated, hairy, spurs 5–6 mm, abaxial lobes yellow to violet, adaxial blue to violet, 2–3 mm;</text>
      <biological_entity id="o15354" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o15355" name="corolla-tube" name_original="corolla-tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15356" name="palate" name_original="palate" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s5" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o15357" name="spur" name_original="spurs" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15358" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s5" to="violet" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15359" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s5" to="violet" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial filaments 1.5–2 mm, adaxial 1–1.5 mm;</text>
      <biological_entity id="o15360" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o15361" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15362" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style included, 1.3–1.5 mm;</text>
      <biological_entity id="o15363" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15364" name="style" name_original="style" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="included" value_original="included" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigma straight.</text>
      <biological_entity id="o15365" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15366" name="stigma" name_original="stigma" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules ovoid-globular, 3–5 mm.</text>
      <biological_entity id="o15367" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid-globular" value_original="ovoid-globular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 1–1.5 mm, cristate-tuberculate.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 36.</text>
      <biological_entity id="o15368" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s10" value="cristate-tuberculate" value_original="cristate-tuberculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15369" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Kickxia elatine differs from K. spuria in leaf and calyx characters. Two subspecies of K. elatine have been described [subsp. elatine and subsp. crinita (Mabille) W. Greuter]; they are poorly defined (D. A. Sutton 1988; M. Ghebrehiwet 2001) and differentiated on the basis of indument density.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly or sandy disturbed sites, roadsides, stream banks, gravel bars, glades.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="sandy disturbed sites" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="bars" modifier="gravel" />
        <character name="habitat" value="glades" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Ont.; Ala., Ark., Calif., Conn., Del., Ga., Ill., Ind., Kans., Ky., La., Md., Mass., Mich., Mo., N.J., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., Tenn., Tex., Va., Wash., W.Va., Wis.; Eurasia; n Africa; introduced also in Mexico, Central America, South America, Atlantic Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>