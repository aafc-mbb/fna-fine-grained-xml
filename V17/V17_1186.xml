<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">665</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">577</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Pennell" date="1941" rank="species">xanthotricha</taxon_name>
    <place_of_publication>
      <publication_title>Notul. Nat. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>74: 5. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species xanthotricha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>119.</number>
  <other_name type="common_name">John Day or yellow-hairy paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1–2 (–3.8) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o28168" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3.8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o28169" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o28168" id="r2155" name="with" negation="false" src="d0_s2" to="o28169" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few to several, ± decumbent to erect or ascending, unbranched, sometimes with short, leafy axillary shoots, hairs erect to spreading, long, soft, eglandular, mixed with short-stipitate-glandular ones.</text>
      <biological_entity id="o28170" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="several" />
        <character char_type="range_value" from="less decumbent" name="orientation" src="d0_s3" to="erect or ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o28171" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o28172" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character constraint="with ones" constraintid="o28173" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o28173" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <relation from="o28170" id="r2156" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o28171" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green, linear, lanceolate to broadly lanceolate, oblong, or cuneate, 0.8–5 cm, not fleshy, margins plane to wavy, involute, 0–5-lobed, apex acute, sometimes rounded;</text>
      <biological_entity id="o28174" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="broadly lanceolate oblong or cuneate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="broadly lanceolate oblong or cuneate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="broadly lanceolate oblong or cuneate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o28175" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s4" to="wavy" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0-5-lobed" value_original="0-5-lobed" />
      </biological_entity>
      <biological_entity id="o28176" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes spreading, linear, arising below mid length, nearly as broad as center lobe, apex acute.</text>
      <biological_entity id="o28177" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character constraint="below mid lobes" constraintid="o28178" is_modifier="false" name="orientation" src="d0_s5" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o28178" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s5" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o28179" name="center" name_original="center" src="d0_s5" type="structure" />
      <biological_entity id="o28180" name="lobe" name_original="lobe" src="d0_s5" type="structure" />
      <biological_entity id="o28181" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="length" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o28177" id="r2157" modifier="nearly" name="as broad as" negation="false" src="d0_s5" to="o28179" />
      <relation from="o28177" id="r2158" modifier="nearly" name="as broad as" negation="false" src="d0_s5" to="o28180" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 3–14 × 1.5–4.5 cm;</text>
      <biological_entity id="o28182" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish, rarely dull reddish purple, distally white to cream, rarely pale-yellow or dull, pale-pink (sharply differentiated from proximal coloration), lanceolate or oblong to narrowly ovate, (3–) 5–7-lobed;</text>
      <biological_entity id="o28183" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish purple" value_original="reddish purple" />
        <character char_type="range_value" from="distally white" name="coloration" src="d0_s7" to="cream" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-pink" value_original="pale-pink" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="narrowly ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="(3-)5-7-lobed" value_original="(3-)5-7-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes ascending, linear to obovate, ± broadened distally, medium, long, proximal lobes arising below mid length, central lobe apex broadly rounded to truncate, others acute to rounded.</text>
      <biological_entity id="o28184" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="obovate" />
        <character is_modifier="false" modifier="more or less; distally" name="width" src="d0_s8" value="broadened" value_original="broadened" />
        <character is_modifier="false" name="size" src="d0_s8" value="medium" value_original="medium" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o28185" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o28186" name="apex" name_original="apex" src="d0_s8" type="structure" constraint_original="central lobe">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s8" to="truncate" />
      </biological_entity>
      <biological_entity id="o28187" name="other" name_original="others" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, 15–26 mm;</text>
      <biological_entity id="o28188" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o28189" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28189" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 3.5–7 mm, 25–50% of calyx length, deeper than laterals, lateral 2–5 mm, 12–25% of calyx length;</text>
      <biological_entity constraint="calyx" id="o28190" name="cleft" name_original="clefts" src="d0_s10" type="structure" constraint_original="calyx abaxial and adaxial; calyx">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character constraint="than laterals" constraintid="o28192" is_modifier="false" name="length" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" name="length" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28191" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o28192" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
      <relation from="o28190" id="r2159" modifier="25-50%" name="part_of" negation="false" src="d0_s10" to="o28191" />
    </statement>
    <statement id="d0_s11">
      <text>lobes linear, oblong, or narrowly triangular, center lobe apex usually rounded, lobes acute to rounded.</text>
      <biological_entity id="o28193" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o28194" name="apex" name_original="apex" src="d0_s11" type="structure" constraint_original="center lobe">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o28195" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas curved, 17–23 mm;</text>
      <biological_entity id="o28196" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s12" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 15–19 mm;</text>
      <biological_entity id="o28197" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s13" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted, adaxially green, 5–8 (–9) mm, puberulent, stipitate-glandular;</text>
      <biological_entity id="o28198" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip deep purple (color sometimes visible through calyx), green, pinkish, or pale-yellow, ± prominent, slightly inflated, usually hidden in calyx, sometimes right at top of calyx, 2 mm, ca. 50% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o28199" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="inflated" value_original="inflated" />
        <character constraint="in calyx" constraintid="o28200" is_modifier="false" modifier="usually" name="prominence" src="d0_s15" value="hidden" value_original="hidden" />
        <character name="some_measurement" notes="" src="d0_s15" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28200" name="calyx" name_original="calyx" src="d0_s15" type="structure" />
      <biological_entity id="o28201" name="top" name_original="top" src="d0_s15" type="structure" />
      <biological_entity id="o28202" name="calyx" name_original="calyx" src="d0_s15" type="structure" />
      <biological_entity id="o28203" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o28199" id="r2160" modifier="sometimes" name="at" negation="false" src="d0_s15" to="o28201" />
      <relation from="o28201" id="r2161" name="top of" negation="false" src="d0_s15" to="o28202" />
    </statement>
    <statement id="d0_s16">
      <text>teeth ascending, whitish, yellowish, pink, or green, 1–1.5 mm. 2n = 48.</text>
      <biological_entity id="o28204" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28205" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja xanthotricha is endemic to moderate elevations in the sagebrush hills of the John Day River drainage in north-central Oregon. N. H. Holmgren (1971) hypothesized that this tetraploid species is of allopolyploid hybrid origin between C. glandulifera and C. oresbia.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arid, rocky, sandy, or clay slopes of basaltic origin, sagebrush steppes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="arid rocky" constraint="of basaltic origin" />
        <character name="habitat" value="clay slopes" constraint="of basaltic origin" />
        <character name="habitat" value="basaltic origin" />
        <character name="habitat" value="steppes" modifier="sagebrush" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>