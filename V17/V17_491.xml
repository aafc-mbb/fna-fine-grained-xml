<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">194</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="(Piper) Pennell &amp; D. D. Keck" date="1940" rank="species">globosus</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>520: 294. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species globosus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Douglas" date="unknown" rank="species">confertus</taxon_name>
    <taxon_name authority="Piper" date="1900" rank="variety">globosus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>27: 397. 1900</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species confertus;variety globosus</taxon_hierarchy>
  </taxon_identification>
  <number>161.</number>
  <other_name type="common_name">Globe beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o28215" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (10–) 20–65 cm, glabrous or sparsely retrorsely hairy distally, not glaucous.</text>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, not leathery, glabrous;</text>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 28–160 (–235) × 8–25 (–35) mm, blade oblanceolate to elliptic, base tapered, margins entire, apex obtuse to acute;</text>
      <biological_entity id="o28217" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o28218" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o28219" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="elliptic" />
      </biological_entity>
      <biological_entity id="o28220" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o28221" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28222" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 2–5 pairs, sessile or proximals short-petiolate, 10–130 × 3–35 mm, blade oblong to ovate or lanceolate, base truncate to clasping, margins entire, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o28223" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o28224" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28225" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="ovate or lanceolate" />
      </biological_entity>
      <biological_entity id="o28226" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="architecture" src="d0_s4" to="clasping" />
      </biological_entity>
      <biological_entity id="o28227" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28228" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses continuous or interrupted, cylindric, (1–) 2–11 (–27) cm, axis glabrous or sparsely retrorsely hairy, verticillasters 1–3 (or 4), cymes (2–) 4–13-flowered, 2 per node;</text>
      <biological_entity id="o28229" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="27" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28230" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely retrorsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o28231" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o28232" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(2-)4-13-flowered" value_original="(2-)4-13-flowered" />
        <character constraint="per node" constraintid="o28233" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o28233" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, (5–) 16–66 × (1–) 4–19 mm, margins entire;</text>
      <biological_entity constraint="proximal" id="o28234" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s6" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s6" to="66" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28235" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels erect, glabrous.</text>
      <biological_entity id="o28236" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28237" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes oblanceolate to lanceolate, (4–) 5–9 × 1.4–3.5 mm, glabrous;</text>
      <biological_entity id="o28238" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o28239" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s8" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla blue to purple or violet, without nectar guides, tubular-funnelform, 14–21 mm, glabrous externally, moderately white or yellowish pilose internally abaxially, tube 5–6 mm, throat slightly inflated to slightly ventricose, 4–7 mm diam., 2-ridged abaxially;</text>
      <biological_entity id="o28240" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o28241" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="purple or violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s9" to="21" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o28242" name="guide" name_original="guides" src="d0_s9" type="structure" />
      <biological_entity id="o28243" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28244" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character char_type="range_value" from="slightly inflated" name="shape" src="d0_s9" to="slightly ventricose" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o28241" id="r2162" name="without" negation="false" src="d0_s9" to="o28242" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs divergent, saccate, 0.7–1.2 mm, dehiscing incompletely, distal 1/5–1/4 indehiscent, connective not splitting, sides glabrous, sutures papillate;</text>
      <biological_entity id="o28245" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28246" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o28247" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="distance" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="incompletely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
        <character is_modifier="false" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s10" to="1/4" />
        <character is_modifier="false" name="dehiscence" src="d0_s10" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o28248" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o28249" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28250" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 7–9 mm, included, 0.4–0.7 mm diam., tip straight to slightly recurved, distal 1–2 mm densely pilose to lanate, hairs golden yellow, to 1.4 mm, medial 2–4 mm sparsely lanate;</text>
      <biological_entity id="o28251" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28252" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="diameter" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28253" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28254" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="densely pilose" name="pubescence" src="d0_s11" to="lanate" />
      </biological_entity>
      <biological_entity id="o28255" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="medial" id="o28256" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="lanate" value_original="lanate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 8–10 mm.</text>
      <biological_entity id="o28257" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o28258" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 5–7 × 3–4 mm, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16, 32.</text>
      <biological_entity id="o28259" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28260" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The saccate anthers and relatively broad leaves of Penstemon globosus are diagnostic. D. V. Clark (1971) reported plants in northeastern Oregon (Baker, Union, and Wallowa counties) exhibiting degrees of morphologic intermediacy between P. globosus and P. rydbergii. These could be hybrids or introgressants involving those two species, though Clark did not observe any populations where hybridization was evident. Putative hybrids between P. globosus and P. confertus have been documented in Idaho County, Idaho (Clark).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to wet meadows, moist mountain slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to wet meadows" />
        <character name="habitat" value="moist mountain slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–3100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>