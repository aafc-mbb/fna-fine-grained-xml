<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="(Rafinesque) Pennell" date="1920" rank="subgenus">Dasanthera</taxon_name>
    <taxon_name authority="G. Don" date="1837" rank="section">Erianthera</taxon_name>
    <taxon_name authority="A. Gray in War Department [U.S.]" date="1858" rank="species">newberryi</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="1901" rank="variety">sonomensis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. W. Calif.,</publication_title>
      <place_in_publication>401. 1901</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus dasanthera;section erianthera;species newberryi;variety sonomensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Greene" date="1891" rank="species">sonomensis</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 218. 1891</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species sonomensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">newberryi</taxon_name>
    <taxon_name authority="(Greene) D. D. Keck" date="unknown" rank="subspecies">sonomensis</taxon_name>
    <taxon_hierarchy>genus p.;species newberryi;subspecies sonomensis</taxon_hierarchy>
  </taxon_identification>
  <number>8c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: distal slightly smaller than proximal.</text>
      <biological_entity id="o12525" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="distal" id="o12526" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="than proximal leaves" constraintid="o12527" is_modifier="false" name="size" src="d0_s0" value="slightly smaller" value_original="slightly smaller" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12527" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Flowers: corolla purple, 22–30 mm;</text>
      <biological_entity id="o12528" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o12529" name="corolla" name_original="corolla" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s1" value="purple" value_original="purple" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stamens: longer pair exserted, pollen-sacs 1.1–1.3 mm.</text>
      <biological_entity id="o12530" name="stamen" name_original="stamens" src="d0_s2" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
        <character is_modifier="false" name="position" src="d0_s2" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o12531" name="pollen-sac" name_original="pollen-sacs" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="distance" src="d0_s2" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety sonomensis is known from mountain peaks in Lake, Napa, and Sonoma counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops, talus slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="talus slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>