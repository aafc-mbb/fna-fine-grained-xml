<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">551</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="mention_page">542</other_info_on_meta>
    <other_info_on_meta type="mention_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">552</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Rafinesque" date="1837" rank="genus">AGALINIS</taxon_name>
    <taxon_name authority="(Linnaeus) Pennell" date="1913" rank="species">purpurea</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>40: 126. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus agalinis;species purpurea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gerardia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">purpurea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 610. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gerardia;species purpurea</taxon_hierarchy>
  </taxon_identification>
  <number>28.</number>
  <other_name type="common_name">Purple false foxglove</other_name>
  <other_name type="common_name">gérardie pourpre</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or branched, 7–120 cm;</text>
      <biological_entity id="o38982" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches ascending, spreading, or arching, quadrangular-ridged to winged distally, glabrate or scabridulous to sparsely scabrous, especially on angles or also on faces near nodes.</text>
      <biological_entity id="o38983" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" name="shape" src="d0_s1" value="quadrangular-ridged" value_original="quadrangular-ridged" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character name="pubescence" src="d0_s1" value="scabridulous to sparsely scabrous" value_original="scabridulous to sparsely scabrous" />
      </biological_entity>
      <biological_entity id="o38984" name="angle" name_original="angles" src="d0_s1" type="structure" />
      <biological_entity id="o38985" name="face" name_original="faces" src="d0_s1" type="structure" />
      <biological_entity id="o38986" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o38983" id="r3017" modifier="especially" name="on" negation="false" src="d0_s1" to="o38984" />
      <relation from="o38983" id="r3018" modifier="especially" name="on" negation="false" src="d0_s1" to="o38985" />
      <relation from="o38985" id="r3019" name="near" negation="false" src="d0_s1" to="o38986" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading to ascending;</text>
      <biological_entity id="o38987" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly linear to linear or linear-lanceolate, 7.5–50 x 0.5–4 (–5) mm, not fleshy, margins entire, abaxial midvein sometimes scabrous, adaxial surface scabrous;</text>
      <biological_entity id="o38989" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="7.5" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="true" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o38990" name="midvein" name_original="midvein" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="7.5" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="true" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o38991" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="7.5" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="true" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o38992" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o38993" name="margin" name_original="margins" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles absent or shorter than subtending leaves.</text>
      <biological_entity id="o38988" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="to margins, midvein, surface" constraintid="o38989, o38990, o38991" is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character constraint="than subtending leaves" constraintid="o38994" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o38994" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes, short-to-elongate, flowers 1 or 2 per node;</text>
      <biological_entity constraint="inflorescences" id="o38995" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character char_type="range_value" from="short" name="size" src="d0_s5" to="elongate" />
      </biological_entity>
      <biological_entity id="o38996" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o38997" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts longer than pedicels.</text>
      <biological_entity id="o38998" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character constraint="than pedicels" constraintid="o38999" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o38999" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels spreading-ascending, 1–5 (–6) mm, glabrous, rarely scabrous.</text>
      <biological_entity id="o39000" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading-ascending" value_original="spreading-ascending" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx tubular-campanulate to hemispheric, tube (2–) 3–5 mm, glabrous, rarely scabrous on major veins, lobes triangular, lanceolate, or triangular-subulate, not keeled, 0.8–3 (–5) mm;</text>
      <biological_entity id="o39001" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o39002" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="tubular-campanulate" name="shape" src="d0_s8" to="hemispheric" />
      </biological_entity>
      <biological_entity id="o39003" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character constraint="on veins" constraintid="o39004" is_modifier="false" modifier="rarely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o39004" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o39005" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular-subulate" value_original="triangular-subulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular-subulate" value_original="triangular-subulate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla pink to rosy pink, with 2 yellow lines and red spots in abaxial throat or these pale or absent, 9–36 mm, throat pilose externally and villous within across bases and sinus of adaxial lobes, lobes: abaxial spreading to reflexed or projected to somewhat spreading (small-flowered plants), adaxial erect to reflexed, 2.5–11 mm, pilose externally;</text>
      <biological_entity id="o39006" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="rosy pink" />
        <character constraint="in " constraintid="o39011" is_modifier="false" name="coloration" notes="" src="d0_s9" value="red spots" value_original="red spots" />
        <character constraint="within bases, sinus" constraintid="o39016, o39017" is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o39007" name="line" name_original="lines" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39008" name="throat" name_original="throat" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o39010" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="pale" value_original="pale" />
        <character is_modifier="true" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o39011" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="true" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character char_type="range_value" constraint="in " constraintid="o39015" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="36" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39012" name="throat" name_original="throat" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o39014" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="pale" value_original="pale" />
        <character is_modifier="true" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o39015" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="true" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o39016" name="base" name_original="bases" src="d0_s9" type="structure" />
      <biological_entity id="o39017" name="sinus" name_original="sinus" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o39018" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o39019" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o39020" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s9" to="reflexed" />
        <character name="orientation" src="d0_s9" value="projected to somewhat spreading , adaxial lobes" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="reflexed" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o39006" id="r3020" name="with" negation="false" src="d0_s9" to="o39007" />
      <relation from="o39016" id="r3021" name="part_of" negation="false" src="d0_s9" to="o39018" />
      <relation from="o39017" id="r3022" name="part_of" negation="false" src="d0_s9" to="o39018" />
    </statement>
    <statement id="d0_s10">
      <text>proximal anthers parallel to filaments, distal oblique or perpendicular to filaments (parallel in small-flowered plants), pollen-sacs 0.9–4.5 mm;</text>
      <biological_entity id="o39021" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="rosy pink" />
        <character constraint="in " constraintid="o39026" is_modifier="false" name="coloration" notes="" src="d0_s10" value="red spots" value_original="red spots" />
        <character constraint="within bases, sinus" constraintid="o39031, o39032" is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o39022" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39023" name="throat" name_original="throat" src="d0_s10" type="structure" />
      <biological_entity constraint="abaxial" id="o39025" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
        <character is_modifier="true" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o39026" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="true" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" constraint="in " constraintid="o39030" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="36" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39027" name="throat" name_original="throat" src="d0_s10" type="structure" />
      <biological_entity constraint="abaxial" id="o39029" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
        <character is_modifier="true" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o39030" name="throat" name_original="throat" src="d0_s10" type="structure">
        <character is_modifier="true" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o39031" name="base" name_original="bases" src="d0_s10" type="structure" />
      <biological_entity id="o39032" name="sinus" name_original="sinus" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o39033" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o39034" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o39035" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character constraint="to filaments" constraintid="o39036" is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s10" value="distal" value_original="distal" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="oblique" value_original="oblique" />
        <character constraint="to filaments" constraintid="o39037" is_modifier="false" name="orientation" src="d0_s10" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o39036" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o39037" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o39038" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <relation from="o39021" id="r3023" name="with" negation="false" src="d0_s10" to="o39022" />
      <relation from="o39031" id="r3024" name="part_of" negation="false" src="d0_s10" to="o39033" />
      <relation from="o39032" id="r3025" name="part_of" negation="false" src="d0_s10" to="o39033" />
    </statement>
    <statement id="d0_s11">
      <text>style included or exserted, 6.5–21 mm.</text>
      <biological_entity id="o39039" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="rosy pink" />
        <character constraint="in " constraintid="o39044" is_modifier="false" name="coloration" notes="" src="d0_s11" value="red spots" value_original="red spots" />
        <character constraint="within bases, sinus" constraintid="o39049, o39050" is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o39040" name="line" name_original="lines" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39041" name="throat" name_original="throat" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial" id="o39043" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="pale" value_original="pale" />
        <character is_modifier="true" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o39044" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="true" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" constraint="in " constraintid="o39048" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="36" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39045" name="throat" name_original="throat" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial" id="o39047" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="pale" value_original="pale" />
        <character is_modifier="true" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o39048" name="throat" name_original="throat" src="d0_s11" type="structure">
        <character is_modifier="true" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o39049" name="base" name_original="bases" src="d0_s11" type="structure" />
      <biological_entity id="o39050" name="sinus" name_original="sinus" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial" id="o39051" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o39052" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o39053" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s11" to="21" to_unit="mm" />
      </biological_entity>
      <relation from="o39039" id="r3026" name="with" negation="false" src="d0_s11" to="o39040" />
      <relation from="o39049" id="r3027" name="part_of" negation="false" src="d0_s11" to="o39051" />
      <relation from="o39050" id="r3028" name="part_of" negation="false" src="d0_s11" to="o39051" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules globular, 4–6 (–7) mm.</text>
      <biological_entity id="o39054" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globular" value_original="globular" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds dark-brown, 0.7–1.4 mm. 2n = 28.</text>
      <biological_entity id="o39055" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39056" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., N.S., Ont., Que.; Ala., Ark., Conn., D.C., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., N.C., N.H., N.J., N.Y., Nebr., Ohio, Pa., R.I., S.C., Tenn., Tex., Va., Vt., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Reports of Agalinis purpurea from the West Indies have not been confirmed.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 18–36 mm, lobes (4–)5–11 mm; styles strongly exserted; distal anthers perpendicular to filaments; stems 40–120 cm, branches ascending, spreading, or arching.</description>
      <determination>28a. Agalinis purpurea var. purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas 9–20 mm, lobes 2.5–5(–6) mm; styles included or slightly exserted; distal anthers oblique or parallel to filaments; stems 7–60 cm, simple or branches ascending.</description>
      <determination>28b. Agalinis purpurea var. parviflora</determination>
    </key_statement>
  </key>
</bio:treatment>