<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">248</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1920" rank="section">Spectabiles</taxon_name>
    <taxon_name authority="A. Gray" date="1876" rank="species">clevelandii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">clevelandii</taxon_name>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section spectabiles;species clevelandii;variety clevelandii;</taxon_hierarchy>
  </taxon_identification>
  <number>229a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Distal cauline leaves short-petiolate or sessile, base tapered or truncate.</text>
      <biological_entity constraint="distal cauline" id="o11696" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o11697" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="shape" src="d0_s0" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Thyrses: peduncles and pedicels glandular-pubescent, rarely glabrous.</text>
      <biological_entity id="o11698" name="thyrse" name_original="thyrses" src="d0_s1" type="structure" />
      <biological_entity id="o11699" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11700" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: calyx lobes glandular-pubescent, rarely glabrous;</text>
      <biological_entity id="o11701" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity constraint="calyx" id="o11702" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corolla glandular-pubescent internally abaxially;</text>
      <biological_entity id="o11703" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o11704" name="corolla" name_original="corolla" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pollen-sacs explanate, sutures smooth;</text>
      <biological_entity id="o11705" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o11706" name="pollen-sac" name_original="pollen-sacs" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="explanate" value_original="explanate" />
      </biological_entity>
      <biological_entity id="o11707" name="suture" name_original="sutures" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminode 9–11 mm.</text>
      <biological_entity id="o11708" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o11709" name="staminode" name_original="staminode" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, var. clevelandii is known from southern California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky to sandy slopes, pinyon-juniper woodlands, scrub, chaparral.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky to sandy slopes" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>