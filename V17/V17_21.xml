<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">SCROPHULARIACEAE</taxon_name>
    <taxon_name authority="Humboldt &amp; Bonpland" date="1812" rank="genus">LEUCOPHYLLUM</taxon_name>
    <taxon_name authority="(Berlandier) I. M. Johnston" date="1924" rank="species">frutescens</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>70: 89. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family scrophulariaceae;genus leucophyllum;species frutescens</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Terania</taxon_name>
    <taxon_name authority="Berlandier" date="1832" rank="species">frutescens</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Comis. Limites,</publication_title>
      <place_in_publication>4. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus terania;species frutescens</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Texas barometer-bush or silver-leaf</other_name>
  <other_name type="common_name">purple sage</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs erect, not intricately branched, rounded, 5–20 (–30) dm, not appearing thorny.</text>
      <biological_entity id="o2329" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not intricately" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Young stems densely canescent-tomentose, hairs conic to cylindric, dendritic, uneven in height.</text>
      <biological_entity id="o2330" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="young" value_original="young" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="canescent-tomentose" value_original="canescent-tomentose" />
      </biological_entity>
      <biological_entity id="o2331" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s1" to="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="dendritic" value_original="dendritic" />
        <character is_modifier="false" name="height" src="d0_s1" value="uneven" value_original="uneven" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, rarely opposite;</text>
      <biological_entity id="o2332" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–2 mm;</text>
      <biological_entity id="o2333" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade obovate to oblong-obovate or obovate-orbiculate, 10–25 (–35) mm, base cuneate, midvein and major lateral-veins raised abaxially, abaxial surface silvery gray, adaxial more greenish, hairs conic to cylindric, dendritic, uneven in height.</text>
      <biological_entity id="o2334" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="oblong-obovate or obovate-orbiculate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2335" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o2336" name="midvein" name_original="midvein" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s4" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o2337" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s4" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2338" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery gray" value_original="silvery gray" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2339" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o2340" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s4" to="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="dendritic" value_original="dendritic" />
        <character is_modifier="false" name="height" src="d0_s4" value="uneven" value_original="uneven" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx lobes oblong-lanceolate, 3–5 mm;</text>
      <biological_entity id="o2341" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="calyx" id="o2342" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla rose lavender to light violet, pink, and rose-pink, rarely white, campanulate, 18–26 mm, tube not notably narrowed.</text>
      <biological_entity id="o2343" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2344" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="rose lavender" name="coloration" src="d0_s6" to="light violet" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="rose-pink" value_original="rose-pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s6" to="26" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>2n = 34.</text>
      <biological_entity id="o2345" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not notably" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2346" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leucophyllum frutescens is widely cultivated; horticultural varieties differ in habit, vestiture, and corolla color. The plants are cold hardy and can withstand moderate frosts. As in most or all Leucophyllum species, plants usually flower in response to rain.</discussion>
  <discussion>In Texas, Leucophyllum frutescens is known from much of the southwestern half of the state.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)May–Sep(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky and gravelly hillsides, talus, arroyos, ridges, flats, roadcuts, clay dunes, scrub, chaparral, thorn scrub, riparian communities.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="gravelly hillsides" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="roadcuts" />
        <character name="habitat" value="clay dunes" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="thorn scrub" />
        <character name="habitat" value="riparian communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>