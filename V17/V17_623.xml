<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Kerry A. Barringer, Neil A. Harriman†</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">361</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">DIGITALIS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 621. 1753</place_in_publication>
      <publication_title>Gen Pl. ed.</publication_title>
      <place_in_publication>5, 272. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus digitalis</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin digitalis, finger of a glove, alluding to resemblance of tubular flowers to glove fingers</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>28.</number>
  <other_name type="common_name">Foxglove</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [shrubs], biennial or perennial.</text>
      <biological_entity id="o256" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branching from base, glabrous, glabrate, pilose, or villous.</text>
      <biological_entity id="o257" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o258" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o258" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, alternate, smaller distally;</text>
      <biological_entity id="o259" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent [present];</text>
      <biological_entity id="o260" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade not fleshy, not leathery, margins entire or serrate to coarsely doubly serrate.</text>
      <biological_entity id="o261" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o262" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="coarsely doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, racemes, often secund;</text>
      <biological_entity id="o263" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o264" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="secund" value_original="secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts present.</text>
      <biological_entity id="o265" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present;</text>
      <biological_entity id="o266" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles usually absent.</text>
      <biological_entity id="o267" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o268" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, distinct, narrowly triangular to lanceolate or ovatelanceolate, calyx ± bilaterally symmetric, campanulate;</text>
      <biological_entity id="o269" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s10" to="lanceolate or ovatelanceolate" />
      </biological_entity>
      <biological_entity id="o270" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla brown, yellow, pink to purple, or white, bilaterally symmetric, ± bilabiate, funnelform, tubular-funnelform, or globular to ovoid, tube base not spurred or gibbous, lobes 5, abaxial 3, adaxial 2;</text>
      <biological_entity id="o271" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="purple or white" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="purple or white" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character char_type="range_value" from="globular" name="shape" src="d0_s11" to="ovoid" />
        <character char_type="range_value" from="globular" name="shape" src="d0_s11" to="ovoid" />
      </biological_entity>
      <biological_entity constraint="tube" id="o272" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="spurred" value_original="spurred" />
        <character is_modifier="false" name="shape" src="d0_s11" value="gibbous" value_original="gibbous" />
      </biological_entity>
      <biological_entity id="o273" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o274" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o275" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, adnate to corolla, didynamous, filaments glabrous or hairy;</text>
      <biological_entity id="o276" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character constraint="to corolla" constraintid="o277" is_modifier="false" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o277" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o278" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 0;</text>
      <biological_entity id="o279" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o280" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma 2-lobed or punctiform.</text>
      <biological_entity id="o281" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="punctiform" value_original="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, dehiscence septicidal, sometimes secondarily loculicidal.</text>
      <biological_entity constraint="fruits" id="o282" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" modifier="sometimes secondarily" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 20–60, brown to black, prismatic or cylindric to ovoid, wings absent.</text>
      <biological_entity id="o283" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s17" to="60" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="black" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s17" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>× = 28.</text>
      <biological_entity id="o284" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 22 (4 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; Europe, w Asia; introduced also nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="w Asia" establishment_means="introduced" />
        <character name="distribution" value="also nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>All species of Digitalis are poisonous, containing cardiac glycosides including digitoxin. In addition to the following species, D. ferruginea Linnaeus is sometimes found in cultivation in North America. It has yellow to yellow-brown corollas, like D. lanata, but the corolla tubes are elongate, not globular.</discussion>
  <references>
    <reference>Aronson, J. K. 1986. An Account of the Foxglove and Its Medicinal Uses, 1785–1985. Oxford.</reference>
    <reference>Werner, K. 1965. Taxonomie und Phylogenie der Gattungen Isoplexis Lindl. und Digitalis L. Repert. Spec. Nov. Regni Veg. 70: 109–135.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla tubes globular to ovoid; leaf blade margins entire.</description>
      <determination>2. Digitalis lanata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corolla tubes funnelform or tubular-funnelform; leaf blade margins serrate or serrate at least distally.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla tubes 13–15 mm; throats 5–7 mm diam.</description>
      <determination>3. Digitalis lutea</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla tubes 25–60 mm; throats 14–25 mm diam.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla tubes pale yellow; leaf blade margins finely and evenly serrate distally.</description>
      <determination>1. Digitalis grandiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla tubes purple-pink to white; leaf blade margins coarsely serrate.</description>
      <determination>4. Digitalis purpurea</determination>
    </key_statement>
  </key>
</bio:treatment>