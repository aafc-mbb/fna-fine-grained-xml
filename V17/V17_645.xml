<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GRATIOLA</taxon_name>
    <taxon_name authority="H. Mason &amp; Bacigalupi" date="1954" rank="species">heterosepala</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>12: 150, figs. 1–8. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus gratiola;species heterosepala</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Boggs Lake hedge-hyssop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o28888" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, simple or few-branched, 2–12 cm, glabrous proximally, glabrous or glandular-puberulent distally.</text>
      <biological_entity id="o28889" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear-lanceolate to oblanceolate, elliptic, or oblong, 4–8 (–20) × 1.5–3.5 mm, margins entire, apex obtuse to rounded, surfaces glabrous.</text>
      <biological_entity id="o28890" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o28891" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="oblanceolate elliptic or oblong" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="oblanceolate elliptic or oblong" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="oblanceolate elliptic or oblong" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28892" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28893" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
      <biological_entity id="o28894" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels slender, 2–14 (–28) mm, length 0.9–2.3 times bract, sparsely to densely glandular-puberulent;</text>
      <biological_entity id="o28895" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="28" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="14" to_unit="mm" />
        <character constraint="bract" constraintid="o28896" is_modifier="false" name="length" src="d0_s3" value="0.9-2.3 times bract" value_original="0.9-2.3 times bract" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o28896" name="bract" name_original="bract" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>bracteoles 0.</text>
      <biological_entity id="o28897" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals connate proximally, calyx distinctly bilaterally symmetric, lobes elliptic-oblanceolate, 3.5–6 mm;</text>
      <biological_entity id="o28898" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o28899" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o28900" name="calyx" name_original="calyx" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distinctly bilaterally" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o28901" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic-oblanceolate" value_original="elliptic-oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla 4–7.5 (–9) mm, tube yellow or greenish yellow, veins purple, limb white abaxially, greenish yellow adaxially;</text>
      <biological_entity id="o28902" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o28903" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28904" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity id="o28905" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o28906" name="limb" name_original="limb" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 1.5–2 mm.</text>
      <biological_entity id="o28907" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o28908" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules subglobular to ovoid, 3.5–4.7 × 3–4.5 mm.</text>
      <biological_entity id="o28909" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="subglobular" name="shape" src="d0_s8" to="ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s8" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 0.5–0.7 mm.</text>
      <biological_entity id="o28910" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow water and exposed mud of vernal pools, wet meadows, lake margins.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mud" modifier="and exposed" constraint="of vernal pools , wet meadows , lake margins" />
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="water" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>