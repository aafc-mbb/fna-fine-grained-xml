<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="mention_page">391</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Spach" date="1840" rank="genus">ERYTHRANTHE</taxon_name>
    <taxon_name authority="G. L. Nesom" date="2017" rank="species">ptilota</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2017-17: 4. 2017</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species ptilota</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="unknown" rank="species">moschatus</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1886" rank="variety">sessilifolius</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 2(1): 447. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species moschatus;variety sessilifolius</taxon_hierarchy>
  </taxon_identification>
  <number>47.</number>
  <other_name type="common_name">Wing-leaf monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, rhizomatous, sometimes rooting at proximal nodes.</text>
      <biological_entity id="o35949" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character constraint="at proximal nodes" constraintid="o35950" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o35950" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, sometimes decumbent to ascending, few-branched, 20–80 cm, villous, hairs 1–2 mm, eglandular, sometimes mixed with much shorter stipitate-glandular ones, internodes evident.</text>
      <biological_entity id="o35951" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="decumbent" modifier="sometimes" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o35952" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character constraint="with ones" constraintid="o35953" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s1" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o35953" name="one" name_original="ones" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="much" name="height_or_length_or_size" src="d0_s1" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o35954" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, basal not persistent, often congested;</text>
      <biological_entity id="o35955" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o35956" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="often" name="architecture_or_arrangement" src="d0_s2" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0 mm, rarely 1–2 (–3) mm;</text>
      <biological_entity id="o35957" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="0" value_original="0" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" modifier="rarely" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="rarely" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade pinnately veined, oblong-lanceolate, 30–70 × 10–22 mm, base rounded, margins denticulate to dentate, apex acute, surfaces villous, hairs 1–2 mm, eglandular, sometimes mixed with much shorter stipitate-glandular ones.</text>
      <biological_entity id="o35958" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35959" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o35960" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s4" to="dentate" />
      </biological_entity>
      <biological_entity id="o35961" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o35962" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o35963" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character constraint="with ones" constraintid="o35964" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s4" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o35964" name="one" name_original="ones" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="much" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers herkogamous, 4–10, from medial to distal nodes.</text>
      <biological_entity id="o35966" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <relation from="o35965" id="r2751" name="from" negation="false" src="d0_s5" to="o35966" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels (15–) 22–50 mm, villous, hairs 1–2 mm, eglandular, sometimes mixed with much shorter stipitate-glandular ones.</text>
      <biological_entity id="o35965" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o35967" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o35968" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="22" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s6" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
      <relation from="o35965" id="r2752" name="fruiting" negation="false" src="d0_s6" to="o35967" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting calyces wing or plicate-angled, cylindric-campanulate, weakly inflated, 10–12 mm, villous-glandular, hairs gland-tipped, lobes distinctly spreading, strongly unequal, linear-lanceolate to narrowly triangular, 5–9 mm, apex long acuminate-apiculate.</text>
      <biological_entity id="o35969" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character constraint="with much shorter stipitate-glandular ones" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s6" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="calyces" id="o35970" name="wing" name_original="wing" src="d0_s7" type="structure" />
      <biological_entity id="o35971" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="plicate-angled" value_original="plicate-angled" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric-campanulate" value_original="cylindric-campanulate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="villous-glandular" value_original="villous-glandular" />
      </biological_entity>
      <biological_entity id="o35972" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o35973" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="narrowly triangular" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35974" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="acuminate-apiculate" value_original="acuminate-apiculate" />
      </biological_entity>
      <relation from="o35969" id="r2753" name="fruiting" negation="false" src="d0_s7" to="o35970" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas yellow, throat with fine blackish or brownish lines on all sides, weakly bilaterally or nearly radially symmetric, weakly bilabiate or nearly regular;</text>
      <biological_entity id="o35975" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o35976" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="weakly bilaterally; bilaterally; nearly radially" name="architecture" notes="" src="d0_s8" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s8" value="regular" value_original="regular" />
      </biological_entity>
      <biological_entity id="o35977" name="line" name_original="lines" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="fine" value_original="fine" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="blackish" value_original="blackish" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o35978" name="side" name_original="sides" src="d0_s8" type="structure" />
      <relation from="o35976" id="r2754" name="with" negation="false" src="d0_s8" to="o35977" />
      <relation from="o35977" id="r2755" name="on" negation="false" src="d0_s8" to="o35978" />
    </statement>
    <statement id="d0_s9">
      <text>tube-throat narrowly campanulate, 15–18 mm, exserted beyond calyx margin;</text>
      <biological_entity id="o35979" name="throat-tube" name_original="tube-throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="15" from_unit="mm" name="distance" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o35980" name="margin" name_original="margin" src="d0_s9" type="structure" />
      <relation from="o35979" id="r2756" name="exserted beyond" negation="false" src="d0_s9" to="o35980" />
    </statement>
    <statement id="d0_s10">
      <text>lobe apex rounded.</text>
      <biological_entity constraint="lobe" id="o35981" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Styles glabrous.</text>
      <biological_entity id="o35982" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Anthers included, finely hirtellous to hispidulous.</text>
      <biological_entity id="o35983" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character char_type="range_value" from="finely hirtellous" name="pubescence" src="d0_s12" to="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules included, 6–8 mm.</text>
      <biological_entity id="o35984" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erythranthe ptilota is recognized by its prostrate to decumbent or decumbent-ascending habit, large, consistently sessile leaves, densely villous vestiture, long pedicels, large calyces and corollas, hispid-hirtellous anthers, and particularly by its long, strongly unequal, linear-triangular calyx lobes usually distally falcate. Leaf bases typically are truncate to rounded or subcordate. Rarely the leaves are short-petiolate, but in such cases, the distinctive leaf bases, vestiture, calyx morphology, and pubescent anthers are diagnostic. Erythranthe ptilota is widely sympatric with E. moschata but usually occurs at lower elevations and characteristically in wetter habitats. The epithet ptilota (Greek ptilotos, winged) alludes to a fancied winglike aspect of the pairs of sessile leaves.</discussion>
  <discussion>A population system of Erythranthe ptilota-like plants occurs in southern California, about 480 km disjunct from the main range of the species. These plants have the prostrate habit, large leaves, long pedicels, and large corollas of E. ptilota, but the calyx lobes are variable in length and usually do not show the characteristic attenuate-apiculate apices. The southern California plants are identified here as E. moschata.</discussion>
  <discussion>Erythranthe ptilota is a new name at specific rank for Mimulus moschatus var. sessilifolius [not E. sessilifolia (Maximowicz) G. L. Nesom].</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Creek banks, gravel bars, flood plains, shallow ditches and natural drainages, swales, damp banks, wet sand, moist soils in coniferous woods, marshes, bogs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="creek banks" />
        <character name="habitat" value="gravel bars" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="shallow ditches" />
        <character name="habitat" value="natural drainages" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="banks" modifier="damp" />
        <character name="habitat" value="wet sand" />
        <character name="habitat" value="moist soils" constraint="in coniferous woods" />
        <character name="habitat" value="coniferous woods" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000(–1900) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>