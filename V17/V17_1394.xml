<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">445</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(Curran ex Greene) G. L. Nesom" date="2012" rank="species">kelloggii</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 32. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species kelloggii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eunanus</taxon_name>
    <taxon_name authority="Curran ex Greene" date="1885" rank="species">kelloggii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 100. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus eunanus;species kelloggii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="(Curran ex Greene) Curran ex A. Gray" date="unknown" rank="species">kelloggii</taxon_name>
    <taxon_hierarchy>genus mimulus;species kelloggii</taxon_hierarchy>
  </taxon_identification>
  <number>31.</number>
  <other_name type="common_name">Kellogg’s monkeyflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o24974" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, 10–310 (–370) mm, glandular-puberulent and/or glandular-pubescent.</text>
      <biological_entity id="o24975" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="310" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="370" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="310" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually basal, sometimes basal and cauline, relatively even-sized;</text>
      <biological_entity id="o24976" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="position" notes="" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="relatively" name="size" notes="" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
      <biological_entity id="o24977" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o24978" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole absent, larger with petiolelike extension;</text>
      <biological_entity id="o24979" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character constraint="with extension" constraintid="o24980" is_modifier="false" name="size" src="d0_s3" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o24980" name="extension" name_original="extension" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="petiole-like" value_original="petiolelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade obovate to elliptic, (4–) 6–40 (–52) × (2–) 3–17 (–26) mm, margins entire or crenate, plane, apex obtuse, surfaces often pubescent.</text>
      <biological_entity id="o24981" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s4" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="52" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="26" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24982" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o24983" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24984" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 2–6 (–10) mm in fruit, usually twisting to invert calyx.</text>
      <biological_entity id="o24985" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o24986" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character constraint="to calyx" constraintid="o24987" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s5" value="twisting" value_original="twisting" />
      </biological_entity>
      <biological_entity id="o24986" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
      <biological_entity id="o24987" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2 per node, or 1 or 2 per node on 1 plant, chasmogamous.</text>
      <biological_entity id="o24988" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="per node" constraintid="o24989" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o24989" name="node" name_original="node" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o24990" name="node" name_original="node" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o24991" name="plant" name_original="plant" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o24990" id="r1902" name="on" negation="false" src="d0_s6" to="o24991" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces distinctly asymmetrically attached to pedicel, not inflated in fruit, (7–) 8–16 (–17) mm, densely glandular-puberulent or glandular-pubescent, lobes subequal, apex obtuse, ribs purplish, intercostal areas whitish.</text>
      <biological_entity id="o24992" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicel" constraintid="o24993" is_modifier="false" modifier="distinctly asymmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o24994" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="17" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o24993" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <biological_entity id="o24994" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o24995" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o24996" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24997" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o24998" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas: throat dark purple, golden yellow at base with reddish speckling, limb magenta to reddish purple, lateral adaxial lobes each with a dark purple basal spot, palate ridges golden yellow, tube-throat (13–) 20–45 (–50) mm, limb 10–18 mm diam., bilabiate, abaxial lip smaller than adaxial.</text>
      <biological_entity id="o24999" name="corolla" name_original="corollas" src="d0_s8" type="structure" />
      <biological_entity id="o25000" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark purple" value_original="dark purple" />
        <character constraint="at base" constraintid="o25001" is_modifier="false" name="coloration" src="d0_s8" value="golden yellow" value_original="golden yellow" />
      </biological_entity>
      <biological_entity id="o25001" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o25002" name="speckling" name_original="speckling" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o25003" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s8" to="reddish purple" />
      </biological_entity>
      <biological_entity constraint="lateral adaxial" id="o25004" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o25005" name="spot" name_original="spot" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark purple" value_original="dark purple" />
      </biological_entity>
      <biological_entity constraint="palate" id="o25006" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="golden yellow" value_original="golden yellow" />
      </biological_entity>
      <biological_entity id="o25007" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_distance" src="d0_s8" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s8" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="distance" src="d0_s8" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25008" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="18" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25009" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character constraint="than adaxial lip" constraintid="o25010" is_modifier="false" name="size" src="d0_s8" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o25010" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <relation from="o25001" id="r1903" name="with" negation="false" src="d0_s8" to="o25002" />
      <relation from="o25004" id="r1904" name="with" negation="false" src="d0_s8" to="o25005" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers (distal pair) exserted, glabrous.</text>
      <biological_entity id="o25011" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Styles densely glandular-puberulent distally.</text>
      <biological_entity id="o25012" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas exserted, lobes unequal, abaxial 4–5 times adaxial.</text>
      <biological_entity id="o25013" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o25014" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25015" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character constraint="lobe" constraintid="o25016" is_modifier="false" name="size_or_quantity" src="d0_s11" value="4-5 times adaxial lobes" value_original="4-5 times adaxial lobes" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o25016" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules 6–12 (–13) mm, indehiscent.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o25017" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25018" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus kelloggii occurs in southwestern Oregon and broadly in northern California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Near water runoff areas, away from seeps or other areas with prolonged moisture.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="near water runoff" />
        <character name="habitat" value="near water runoff areas" constraint="with prolonged moisture" />
        <character name="habitat" value="from seeps" modifier="away" constraint="with prolonged moisture" />
        <character name="habitat" value="other areas" constraint="with prolonged moisture" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="other areas" />
        <character name="habitat" value="prolonged moisture" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>