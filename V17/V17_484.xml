<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">203</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Penstemon</taxon_name>
    <taxon_name authority="Pennell" date="1941" rank="species">elegantulus</taxon_name>
    <place_of_publication>
      <publication_title>Notul. Nat. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>71: 14. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species elegantulus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>156.</number>
  <other_name type="common_name">Rockvine beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o30693" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 10–31 cm, retrorsely hairy, not glaucous.</text>
      <biological_entity id="o30694" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="31" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, not leathery, glabrous or retrorsely hairy primarily along midvein and margins;</text>
      <biological_entity id="o30696" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
      <biological_entity id="o30697" name="margin" name_original="margins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline 14–75 × 4–16 mm, blade ovate to lanceolate or elliptic, base tapered, margins entire or ± serrate distally, apex obtuse to acute;</text>
      <biological_entity id="o30695" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="along margins" constraintid="o30697" is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o30698" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o30699" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lanceolate or elliptic" />
      </biological_entity>
      <biological_entity id="o30700" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o30701" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less; distally" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o30702" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 3–5 pairs, sessile or proximals short-petiolate, 15–45 × 2–8 mm, blade oblanceolate to lanceolate, base tapered, margins entire or ± serrate or ± dentate, primarily distally, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o30703" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o30704" name="proximal" name_original="proximals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30705" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o30706" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o30707" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="more or less; more or less" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o30708" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" modifier="primarily distally; distally" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Thyrses interrupted, cylindric, 4–18 cm, axis retrorsely hairy proximally, sparsely glandular-pubescent distally, verticillasters 2–5, cymes 2–6-flowered, (1 or) 2 per node;</text>
      <biological_entity id="o30709" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30710" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="retrorsely; proximally" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o30711" name="verticillaster" name_original="verticillasters" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o30712" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-6-flowered" value_original="2-6-flowered" />
        <character constraint="per node" constraintid="o30713" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o30713" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal bracts lanceolate, 9–38 × 2–12 mm, margins entire or ± serrate distally;</text>
      <biological_entity constraint="proximal" id="o30714" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="38" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30715" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less; distally" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles and pedicels ascending, sparsely glandular-pubescent.</text>
      <biological_entity id="o30716" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o30717" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx lobes ovate, 3–4 × 1–2 mm, sparsely glandular-pubescent;</text>
      <biological_entity id="o30718" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o30719" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla violet to blue or purple, with purple nectar guides, funnelform, 14–23 mm, glandular-pubescent externally, ± white-pubescent internally abaxially, tube 4–6 mm, throat gradually inflated, 5–6 mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o30720" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o30721" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s9" to="blue" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="23" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="more or less; internally abaxially; abaxially" name="pubescence" src="d0_s9" value="white-pubescent" value_original="white-pubescent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o30722" name="guide" name_original="guides" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o30723" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30724" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s9" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <relation from="o30721" id="r2325" name="with" negation="false" src="d0_s9" to="o30722" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sacs opposite, navicular, 0.9–1.2 mm, dehiscing completely, connective splitting, sides glabrous, sutures smooth;</text>
      <biological_entity id="o30725" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o30726" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o30727" name="pollen-sac" name_original="pollen-sacs" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s10" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o30728" name="connective" name_original="connective" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s10" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o30729" name="side" name_original="sides" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30730" name="suture" name_original="sutures" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode 9–12 mm, reaching orifice, 0.4–0.6 mm diam., tip straight, distal 0.5–2 mm pilose, hairs yellow or golden yellow, to 0.8 mm;</text>
      <biological_entity id="o30731" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o30732" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="diameter" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30733" name="orifice" name_original="orifice" src="d0_s11" type="structure" />
      <biological_entity id="o30734" name="tip" name_original="tip" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30735" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o30736" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o30732" id="r2326" name="reaching" negation="false" src="d0_s11" to="o30733" />
    </statement>
    <statement id="d0_s12">
      <text>style 9–13 mm.</text>
      <biological_entity id="o30737" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o30738" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 5–8 × 3.5–5 mm, glabrous.</text>
      <biological_entity id="o30739" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon elegantulus occurs largely in the Hell’s Canyon region of the Snake River in Idaho and Nez Perce counties, Idaho, and Wallowa County, Oregon. A specimen from near Silver City in Owyhee County, Idaho (Hitchcock &amp; Muhlick 22585, WTU), also appears to be this species. Penstemon elegantulus combines morphologic features of P. albertinus and P. humilis; it generally has obscurely serrate leaves as in the former, and retrorsely hairy leaves and stems as in the latter.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, granitic meadows and hillsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="granitic meadows" />
        <character name="habitat" value="hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>