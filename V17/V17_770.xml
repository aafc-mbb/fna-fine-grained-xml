<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Jacquin" date="1762" rank="species">fruticans</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Stirp. Vindob.</publication_title>
      <place_in_publication>2: 200. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species fruticans</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Rock speedwell</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o6981" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, densely branched from woody base, (5–) 10–15 (–30) cm, eglandular-hairy, sometimes glandular-hairy.</text>
      <biological_entity id="o6982" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character constraint="from base" constraintid="o6983" is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity id="o6983" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade obovate to ovate or spatulate, sometimes suborbiculate proximally, (7–) 8–20 (–25) × (2–) 3–6 (–7) mm, mostly shorter to equal to internodes, base cuneate, margins entire or ± crenate or serrate, apex acute, surfaces glabrous or glabrate.</text>
      <biological_entity id="o6984" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6985" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="ovate or spatulate" />
        <character is_modifier="false" modifier="sometimes; proximally" name="shape" src="d0_s2" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s2" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s2" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
        <character constraint="to internodes" constraintid="o6986" is_modifier="false" name="variability" src="d0_s2" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o6986" name="internode" name_original="internodes" src="d0_s2" type="structure" />
      <biological_entity id="o6987" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o6988" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o6989" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6990" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1–10 (–20), terminal, rarely with 1–4 axillary, 20–40 mm, (1–) 4–10 (–18) -flowered, axis glabrate to sparsely eglandular-hairy;</text>
      <biological_entity id="o6991" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="20" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="10" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="40" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="(1-)4-10(-18)-flowered" value_original="(1-)4-10(-18)-flowered" />
      </biological_entity>
      <biological_entity id="o6992" name="axillary" name_original="axillary" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o6993" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s3" to="sparsely eglandular-hairy" />
      </biological_entity>
      <relation from="o6991" id="r586" modifier="rarely" name="with" negation="false" src="d0_s3" to="o6992" />
    </statement>
    <statement id="d0_s4">
      <text>bracts linear or linear-lanceolate or long-ovate, (1–) 3.5–9 (–15) mm.</text>
      <biological_entity id="o6994" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-ovate" value_original="long-ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels erect, (2–) 5–7 (–15) mm, equal to ± longer than subtending bracts, eglandular-hairy.</text>
      <biological_entity id="o6995" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character constraint="than subtending bracts" constraintid="o6996" is_modifier="false" name="length_or_size" src="d0_s5" value="more or less longer" value_original="more or less longer" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o6996" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 4 (or 5) -lobed, lobes (2.5–) 4.5–6 (–8) mm, apex obtuse, eglandular-hairy, sometimes also glandular-hairy;</text>
      <biological_entity id="o6997" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6998" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="4-lobed" value_original="4-lobed" />
      </biological_entity>
      <biological_entity id="o6999" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7000" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla intense blue, sometimes with reddish or white center, rarely white, rotate, (6–) 10–14 (–15) mm diam.;</text>
      <biological_entity id="o7001" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7002" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="intense" value_original="intense" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="false" modifier="rarely" name="coloration" notes="" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rotate" value_original="rotate" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7003" name="center" name_original="center" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <relation from="o7002" id="r587" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o7003" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 5–7 mm;</text>
      <biological_entity id="o7004" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7005" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style (3–) 5–7 (–8.5) mm.</text>
      <biological_entity id="o7006" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7007" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ± compressed in cross-section, ovoid, (5–) 6–8 (–9) × (3–) 4–4.5 (–5.5) mm, longer than wide, apex attenuate to apiculate and acute, rarely rounded, not emarginate, eglandular-hairy, sometimes mixed with larger glandular-hairs.</text>
      <biological_entity id="o7008" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="in cross-section" constraintid="o7009" is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s10" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
      <biological_entity id="o7009" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o7010" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s10" to="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="eglandular-hairy" value_original="eglandular-hairy" />
        <character constraint="with larger glandular-hairs" constraintid="o7011" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s10" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="larger" id="o7011" name="glandular-hair" name_original="glandular-hairs" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds 35–62, orange, ellipsoid, flat, 0.9–1.5 × (0.4–) 0.9–1.2 mm, 0.2–0.3 mm thick, smooth.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 16.</text>
      <biological_entity id="o7012" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s11" to="62" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange" value_original="orange" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="thickness" src="d0_s11" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="thickness" src="d0_s11" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="thickness" src="d0_s11" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7013" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fissures, rocky places, scree.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fissures" />
        <character name="habitat" value="rocky places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>