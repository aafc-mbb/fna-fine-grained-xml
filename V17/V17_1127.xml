<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">636</other_info_on_meta>
    <other_info_on_meta type="mention_page">569</other_info_on_meta>
    <other_info_on_meta type="mention_page">637</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Eastwood" date="1909" rank="species">ornata</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>44: 571. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species ornata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>79.</number>
  <other_name type="common_name">Ornate paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 1.7–3.5 (–5) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>with a thin taproot or fibrous-root system.</text>
      <biological_entity id="o13044" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1.7" from_unit="dm" name="some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o13045" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="fibrous-root" id="o13046" name="system" name_original="system" src="d0_s1" type="structure" />
      <relation from="o13044" id="r1021" name="with" negation="false" src="d0_s1" to="o13045" />
      <relation from="o13044" id="r1022" name="with" negation="false" src="d0_s1" to="o13046" />
    </statement>
    <statement id="d0_s2">
      <text>Stems solitary or few to several, erect or ascending, often branched low on stem, unbranched distally, hairs appressed or retrorse, medium length, soft, eglandular, mixed with shorter stipitate-glandular ones.</text>
      <biological_entity id="o13047" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="few to several" value_original="few to several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character constraint="on stem" constraintid="o13048" is_modifier="false" name="position" src="d0_s2" value="low" value_original="low" />
        <character is_modifier="false" modifier="distally" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o13048" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o13049" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" name="length" src="d0_s2" value="medium" value_original="medium" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character constraint="with shorter ones" constraintid="o13050" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o13050" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves green or purple-tinged, proximal forming a rosette, linear-lanceolate to oblong or oblanceolate, 2–4 cm, not fleshy, clasping, margins wavy, sometimes plane, involute, 0-lobed, apex acuminate, acute, or obtuse.</text>
      <biological_entity id="o13051" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o13052" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="oblong or oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o13053" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <biological_entity id="o13054" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="wavy" value_original="wavy" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="0-lobed" value_original="0-lobed" />
      </biological_entity>
      <biological_entity id="o13055" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o13052" id="r1023" name="forming a" negation="false" src="d0_s3" to="o13053" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 3–24 × 1.5–3 cm;</text>
      <biological_entity id="o13056" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="24" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts proximally green, distally white, sometimes very pale-yellow, often aging dull pink or dull red-purple, spatulate, 0-lobed, sometimes seeming lobed due to wavy margins, apex obtuse to rounded.</text>
      <biological_entity id="o13057" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes very" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="0-lobed" value_original="0-lobed" />
      </biological_entity>
      <biological_entity id="o13058" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="true" name="shape" src="d0_s5" value="due-to-wavy" value_original="due-to-wavy" />
      </biological_entity>
      <biological_entity id="o13059" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" modifier="sometimes" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <relation from="o13057" id="r1024" modifier="sometimes" name="seeming" negation="false" src="d0_s5" to="o13058" />
    </statement>
    <statement id="d0_s6">
      <text>Calyces green throughout or distal margin white aging pink, 15–17 mm;</text>
      <biological_entity id="o13060" name="calyx" name_original="calyces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13061" name="margin" name_original="margin" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>abaxial and adaxial clefts 6–14 mm, 35–45% of calyx length, deeper than laterals, lateral 0 (–0.7) mm, 0 (–5) % of calyx length;</text>
      <biological_entity constraint="abaxial and adaxial" id="o13062" name="cleft" name_original="clefts" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
        <character constraint="than laterals" constraintid="o13063" is_modifier="false" name="coloration_or_size" src="d0_s7" value="deeper" value_original="deeper" />
        <character is_modifier="false" modifier="35-45%" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="0" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="0.7" to_unit="mm" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="0" value_original="0" />
      </biological_entity>
      <biological_entity id="o13063" name="lateral" name_original="laterals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>lobes short-triangular, abaxial segments longer than adaxials, apex acute to obtuse or rounded.</text>
      <biological_entity id="o13064" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="short-triangular" value_original="short-triangular" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13065" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character constraint="than adaxials" constraintid="o13066" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o13066" name="adaxial" name_original="adaxials" src="d0_s8" type="structure" />
      <biological_entity id="o13067" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse or rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas slightly curved, 22–24 mm;</text>
      <biological_entity id="o13068" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s9" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tube 10–13 mm;</text>
      <biological_entity id="o13069" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>beak exserted, adaxially green, 5–10 mm;</text>
      <biological_entity id="o13070" name="beak" name_original="beak" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>abaxial lip pale greenish, reduced, pouches 3, 0.5–1.5 mm, 5–10% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o13071" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale greenish" value_original="pale greenish" />
        <character is_modifier="false" name="size" src="d0_s12" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o13072" name="pouch" name_original="pouches" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13073" name="beak" name_original="beak" src="d0_s12" type="structure" />
      <relation from="o13072" id="r1025" modifier="5-10%" name="as long as" negation="false" src="d0_s12" to="o13073" />
    </statement>
    <statement id="d0_s13">
      <text>teeth slightly incurved, reduced, pale greenish to white, 0.3–0.7 mm. 2n = 24.</text>
      <biological_entity id="o13074" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s13" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="size" src="d0_s13" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="pale greenish" name="coloration" src="d0_s13" to="white" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13075" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja ornata is known from Chihuahua and northern Durango, Mexico, but much of its seasonally moist grassland habitat is now altered by grazing or agriculture, and there are no recent sightings of the species south of the United States border. There is a recently discovered population in southwestern New Mexico, in the southern Animas Valley of Hidalgo County. While very rare, C. ornata lacks federal protection. The small Animas Valley population is the last known extant occurrence, and this population was reduced to two individuals in a census conducted in 2017 (D. Roth, pers. comm.). The species appears to be critically endangered globally and in need of conservation management.</discussion>
  <discussion>The inflorescences of Castilleja ornata have pale greenish bracts with white apices when young, but the apices often become pale pink to dull reddish with age. Its pubescence, wavy-margined leaves, and unusual bract color also distinguish C. ornata. Castilleja exserta and C. minor are the only other annual paintbrushes in New Mexico and differ from C. ornata by the color of their floral bract apices, which are usually pink to red-purple in C. exserta and bright red in C. minor.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally damp ground, dry or sandy grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ground" modifier="seasonally damp" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="sandy grasslands" />
        <character name="habitat" value="damp" modifier="seasonally" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.; Mexico (Chihuahua, Durango).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>