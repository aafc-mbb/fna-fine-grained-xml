<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Kamal I. Mohamed, Lytton J. Musselman</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">508</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Loureiro" date="1790" rank="genus">STRIGA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Cochinch.</publication_title>
      <place_in_publication>1: 22. 1790</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus striga</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin strigosus, slender, alluding to habit</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Witchweed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>chlorophyllous or achlorophyllous, hemiparasitic or holoparasitic, haustoria either single and relatively large, or multiple, smaller, and formed on secondary-roots.</text>
      <biological_entity id="o20945" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="chlorophyllous" value_original="chlorophyllous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="achlorophyllous" value_original="achlorophyllous" />
        <character is_modifier="false" name="nutrition" src="d0_s1" value="hemiparasitic" value_original="hemiparasitic" />
        <character name="nutrition" src="d0_s1" value="holoparasitic" value_original="holoparasitic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o20946" name="haustorium" name_original="haustoria" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="either" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s1" value="large" value_original="large" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="size" src="d0_s1" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o20947" name="secondary-root" name_original="secondary-roots" src="d0_s1" type="structure" />
      <relation from="o20946" id="r1590" name="formed on" negation="false" src="d0_s1" to="o20947" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sometimes fleshy, hispid, puberulent, or glabrous.</text>
      <biological_entity id="o20948" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, opposite, subopposite, or alternate;</text>
      <biological_entity id="o20949" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o20950" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade not fleshy, not leathery, margins entire.</text>
      <biological_entity id="o20951" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o20952" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes or spikes;</text>
      <biological_entity id="o20953" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o20954" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o20955" name="spike" name_original="spikes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o20956" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present or absent;</text>
      <biological_entity id="o20957" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles present.</text>
      <biological_entity id="o20958" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 5 (–8), calyx radially or bilaterally symmetric, tubular, lobes lanceolate or subulate;</text>
      <biological_entity id="o20959" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o20960" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="8" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o20961" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o20962" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, corolla red, brownish red, or purple, rarely white or yellow, bilabiate, salverform, abaxial lobes 3, adaxial 2;</text>
      <biological_entity id="o20963" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20964" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o20965" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish red" value_original="brownish red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish red" value_original="brownish red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish red" value_original="brownish red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="salverform" value_original="salverform" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20966" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20967" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4, didynamous, filaments glabrous;</text>
      <biological_entity id="o20968" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o20969" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="didynamous" value_original="didynamous" />
      </biological_entity>
      <biological_entity id="o20970" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminode 0;</text>
      <biological_entity id="o20971" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o20972" name="staminode" name_original="staminode" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2-locular, placentation axile;</text>
      <biological_entity id="o20973" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o20974" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s14" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate.</text>
      <biological_entity id="o20975" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o20976" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules: dehiscence loculicidal.</text>
      <biological_entity id="o20977" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 400–600, brown or black, ovoid, wings absent.</text>
      <biological_entity id="o20978" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="400" name="quantity" src="d0_s17" to="600" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o20979" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 40 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; s Asia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Striga produces leaves of different sizes; typical proximal leaves are scalelike, and mid-stem leaves are larger. Striga is distinguished from its close relative Buchnera by its bilabiate corolla with an abruptly bent tube, one pollen sac, and glabrous filaments. Buchnera has a bilabiate corolla with a straight or slightly curved tube, two pollen sacs, and pilose filaments. Striga has been divided into three sections based on the number of ribs on the calyx tube (R. Wettstein 1891–1893): sect. Pentapleurae Wettstein with five, sect. Polypleurae Wettstein with ten, and sect. Tetrasepalum Engler with 15.</discussion>
  <discussion>Thirty-four species and subspecies of witchweeds occur in Africa; 22 are endemic (K. I. Mohamed et al. 2001). All Striga species parasitize hosts in the Poaceae except S. gesnerioides, which grows on hosts in Acanthaceae, Convolvulaceae, Euphorbiaceae, Fabaceae, and Solanaceae. Striga asiatica, S. aspera Bentham, S. forbesii Bentham, S. gesnerioides, and S. hermonthica (Delile) Bentham are of economic importance. Crops most affected by Striga include Digitaria exilis (fonio), Oryza subspp. (upland rice), Pennisetum glaucum (bulrush millet), Sorghum vulgare (sorghum), and Zea mays (maize). Striga gesnerioides is a serious pest on Vigna unguiculata (cowpea, Fabaceae) and a minor pest on other dicot crops. All species of witchweed are listed as noxious weeds by the United States Department of Agriculture and 11 state governments. New infestations of quarantine pests in the United States, such as witchweeds, should be reported to the State Plant Health Director in the appropriate state (http://www.aphis.usda.gov/services/report_pest_disease/report_pest_disease.shtml).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx ribs 10; mid-stem leaf blades ascending or spreading, linear or narrowly elliptic, 20–50 mm; bracts linear, longer than calyces; corollas red, rarely yellow, with yellow throats; parasitic on Poaceae.</description>
      <determination>1. Striga asiatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx ribs 5; mid-stem leaf blades appressed, lanceolate, 3–7 mm; bracts lanceolate, shorter than calyces; corollas brownish red or purple, rarely white; parasitic on dicots.</description>
      <determination>2. Striga gesnerioides</determination>
    </key_statement>
  </key>
</bio:treatment>