<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VERONICA</taxon_name>
    <taxon_name authority="Crantz" date="1769" rank="species">dillenii</taxon_name>
    <place_of_publication>
      <publication_title>Stirp. Austr. Fasc. ed.</publication_title>
      <place_in_publication>2, 2: 352. 1769</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus veronica;species dillenii</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Dillenius speedwell</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o7471" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, (8–) 10–20 (–40) cm, glandular and eglandular-hairy.</text>
      <biological_entity id="o7472" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade lanceolate to ovate, 7–19 (–21) × (3–) 5–12 mm, 1–2 times as long as wide, base cuneate, margins (proximal) crenate-dentate or (distal) ±palmatifid, lobes 3–7+, apex of central lobe obtuse, lateral ± acute, surfaces glandular and eglandular-hairy.</text>
      <biological_entity id="o7473" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7474" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="19" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="21" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="19" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="1-2" value_original="1-2" />
      </biological_entity>
      <biological_entity id="o7475" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o7476" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="palmatifid" value_original="palmatifid" />
      </biological_entity>
      <biological_entity id="o7477" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="7" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7478" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="position" src="d0_s2" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="central" id="o7479" name="lobe" name_original="lobe" src="d0_s2" type="structure" />
      <biological_entity id="o7480" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="eglandular-hairy" value_original="eglandular-hairy" />
      </biological_entity>
      <relation from="o7478" id="r624" name="part_of" negation="false" src="d0_s2" to="o7479" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes 1, terminal, 60–180 mm, 15–50 (–120) -flowered, axis eglandular and glandular-hairy;</text>
      <biological_entity id="o7481" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="60" from_unit="mm" name="some_measurement" src="d0_s3" to="180" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="15-50(-120)-flowered" value_original="15-50(-120)-flowered" />
      </biological_entity>
      <biological_entity id="o7482" name="axis" name_original="axis" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal bracts similar to leaves, distal ones linear-lanceolate, 3–5 mm.</text>
      <biological_entity constraint="proximal" id="o7483" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <biological_entity id="o7484" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o7485" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o7483" id="r625" name="to" negation="false" src="d0_s4" to="o7484" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels erect, 2–4 (–5) mm, shorter than subtending bract, length 1/2–1 times calyx, eglandular and glandular-hairy.</text>
      <biological_entity id="o7486" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
        <character constraint="than subtending bract" constraintid="o7487" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character constraint="calyx" constraintid="o7488" is_modifier="false" name="length" src="d0_s5" value="1/2-1 times calyx" value_original="1/2-1 times calyx" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o7487" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <biological_entity id="o7488" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx lobes 3.5–6 mm, apex acute, eglandular and glandular-hairy;</text>
      <biological_entity id="o7489" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="calyx" id="o7490" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7491" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla deep blue, 4–6 mm diam.;</text>
      <biological_entity id="o7492" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7493" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 0.8–1.5 mm;</text>
      <biological_entity id="o7494" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7495" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 0.8–1.5 mm, stigma violet.</text>
      <biological_entity id="o7496" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7497" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7498" name="stigma" name_original="stigma" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="violet" value_original="violet" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules compressed in cross-section, narrowly obcordiform, 3.5–4.5 × 4–6 mm, apex emarginate, angle of sinus ca. 90°, ciliate with glandular and eglandular hairs.</text>
      <biological_entity id="o7499" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="in cross-section" constraintid="o7500" is_modifier="false" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s10" value="obcordiform" value_original="obcordiform" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7500" name="cross-section" name_original="cross-section" src="d0_s10" type="structure" />
      <biological_entity id="o7501" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o7502" name="angle" name_original="angle" src="d0_s10" type="structure">
        <character name="degree" src="d0_s10" value="90°" value_original="90°" />
        <character constraint="with glandular hairs" constraintid="o7504" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7503" name="sinus" name_original="sinus" src="d0_s10" type="structure" />
      <biological_entity constraint="glandular" id="o7504" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o7502" id="r626" name="part_of" negation="false" src="d0_s10" to="o7503" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds 10–28, yellowish, ellipsoid, flat, 0.9–1.6 × 0.7–1.3 mm, 0.2–0.3 mm thick, smooth.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 16 (Eurasia).</text>
      <biological_entity id="o7505" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="28" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character name="thickness" src="d0_s11" unit="mm" value="0.9-1.6×0.7-1.3" value_original="0.9-1.6×0.7-1.3" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="thickness" src="d0_s11" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7506" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Veronica dillenii is closely related to V. verna but with larger flowers; it may have been overlooked and may be distributed more widely. Most herbarium specimens of V. dillenii blacken when dry due to the presence of aucubin, which distinguishes them from V. verna.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open pine and oak forests, rocky, dry, sandy slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open pine" />
        <character name="habitat" value="oak forests" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="sandy slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ill., Ind., Mich., N.Y., Va., Wis.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>