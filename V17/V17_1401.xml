<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">448</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schauer" date="unknown" rank="family">PHRYMACEAE</taxon_name>
    <taxon_name authority="Nuttall" date="1838" rank="genus">DIPLACUS</taxon_name>
    <taxon_name authority="(Coville &amp; A. L. Grant) G. L. Nesom &amp; N. S. Fraga" date="2012" rank="species">rupicola</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 27. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus diplacus;species rupicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mimulus</taxon_name>
    <taxon_name authority="Coville &amp; A. L. Grant" date="1936" rank="species">rupicola</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>26: 99, fig. s.n. [p. 100]. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species rupicola</taxon_hierarchy>
  </taxon_identification>
  <number>38.</number>
  <other_name type="common_name">Death Valley monkeyflower</other_name>
  <other_name type="common_name">rock midget</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with woody caudex.</text>
      <biological_entity id="o10645" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o10646" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o10645" id="r850" name="with" negation="false" src="d0_s0" to="o10646" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, sometimes pendent, 10–170 mm, densely and finely glandular-puberulent.</text>
      <biological_entity id="o10647" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="pendent" value_original="pendent" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="170" to_unit="mm" />
        <character is_modifier="false" modifier="densely; finely" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually basal rosettes and proximal cauline, relatively even-sized;</text>
      <biological_entity id="o10648" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10649" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s2" value="equal-sized" value_original="even-sized" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole absent, base gradually narrowed to broad, petiolelike extension;</text>
      <biological_entity id="o10650" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o10651" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o10652" name="extension" name_original="extension" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="petiole-like" value_original="petiolelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate, (10–) 18–60 (–80) × (1.5–) 3–15 (–26) mm, margins entire, plane, not ciliate, apex acute, surfaces glandular-puberulent.</text>
      <biological_entity id="o10653" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s4" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="26" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10654" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o10655" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o10656" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–3 mm in fruit.</text>
      <biological_entity id="o10657" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o10658" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10658" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1 or 2 per node, chasmogamous.</text>
      <biological_entity id="o10659" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or per" value="2" value_original="2" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o10660" name="node" name_original="node" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Calyces asymmetrically attached to pedicel, not inflated in fruit, 8–18 mm, densely glandular-puberulent, lobes unequal, apex acuminate, ribs green, intercostal areas pale green.</text>
      <biological_entity id="o10661" name="calyx" name_original="calyces" src="d0_s7" type="structure">
        <character constraint="to pedicel" constraintid="o10662" is_modifier="false" modifier="asymmetrically" name="fixation" src="d0_s7" value="attached" value_original="attached" />
        <character constraint="in fruit" constraintid="o10663" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="18" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o10662" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
      <biological_entity id="o10663" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o10664" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o10665" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o10666" name="rib" name_original="ribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o10667" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale green" value_original="pale green" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas: limb pinkish white to nearly white with a large magenta-purple round or 2-lobed blotch at base of each lobe, throat and palate ridges golden yellow with magenta speckling, palate ridges short-pilose, throat glabrous, tube-throat 17–35 mm, limb 8–21 mm diam., not bilabiate.</text>
      <biological_entity id="o10668" name="corolla" name_original="corollas" src="d0_s8" type="structure" />
      <biological_entity id="o10669" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="with blotch" constraintid="o10670" from="pinkish white" name="coloration" src="d0_s8" to="nearly white" />
      </biological_entity>
      <biological_entity id="o10670" name="blotch" name_original="blotch" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s8" value="magenta-purple" value_original="magenta-purple" />
        <character is_modifier="true" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="true" name="shape" src="d0_s8" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o10671" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="golden yellow with magenta" value_original="golden yellow with magenta" />
      </biological_entity>
      <biological_entity id="o10672" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <biological_entity id="o10673" name="throat" name_original="throat" src="d0_s8" type="structure" />
      <biological_entity constraint="palate" id="o10674" name="ridge" name_original="ridges" src="d0_s8" type="structure" />
      <biological_entity constraint="palate" id="o10675" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
      <biological_entity id="o10676" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10677" name="throat-tube" name_original="tube-throat" src="d0_s8" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="distance" src="d0_s8" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10678" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s8" to="21" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="bilabiate" value_original="bilabiate" />
      </biological_entity>
      <relation from="o10670" id="r851" name="at" negation="false" src="d0_s8" to="o10671" />
      <relation from="o10671" id="r852" name="part_of" negation="false" src="d0_s8" to="o10672" />
      <relation from="o10671" id="r853" name="part_of" negation="false" src="d0_s8" to="o10673" />
      <relation from="o10671" id="r854" name="part_of" negation="false" src="d0_s8" to="o10674" />
    </statement>
    <statement id="d0_s9">
      <text>Anthers included, glabrous or slightly puberulent at base.</text>
      <biological_entity id="o10679" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o10680" is_modifier="false" modifier="slightly" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o10680" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Styles glandular-puberulent.</text>
      <biological_entity id="o10681" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Stigmas included, lobes equal.</text>
      <biological_entity id="o10682" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o10683" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 3–8 mm, indehiscent until senescence of pedicel, then opening along both sutures only after wetting.</text>
      <biological_entity id="o10685" name="pedicel" name_original="pedicel" src="d0_s12" type="structure" />
      <biological_entity id="o10686" name="suture" name_original="sutures" src="d0_s12" type="structure" />
      <relation from="o10686" id="r855" modifier="after wetting" name="opening along" negation="false" src="d0_s12" to="o10686" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity id="o10684" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character constraint="of pedicel" constraintid="o10685" is_modifier="false" name="dehiscence" src="d0_s12" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10687" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Diplacus rupicola is known from Inyo County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices in limestone cliffs and walls, limestone ridge tops and slopes, wash edges, gravelly slopes, canyon sides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="in limestone cliffs and walls" />
        <character name="habitat" value="limestone cliffs" />
        <character name="habitat" value="walls" />
        <character name="habitat" value="limestone ridge tops" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="edges" modifier="wash" />
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="canyon sides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>