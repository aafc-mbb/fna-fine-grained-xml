<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">132</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="(Rydberg) Pennell" date="1920" rank="section">Cristati</taxon_name>
    <taxon_name authority="D. D. Keck" date="1940" rank="species">concinnus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>23: 608. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;species concinnus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>57.</number>
  <other_name type="common_name">Tunnel Springs beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, 7–18 (–24) cm, retrorsely hairy.</text>
      <biological_entity id="o4599" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="24" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, not leathery, glabrous or sparsely retrorsely hairy on petiole and, rarely, midvein;</text>
      <biological_entity id="o4600" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="on petiole" constraintid="o4601" is_modifier="false" modifier="sparsely retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4601" name="petiole" name_original="petiole" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline petiolate, 30–55 (–70) × 2–5 (–8) mm, blade oblanceolate to lanceolate or linear, base tapered, margins entire or obscurely and remotely dentate, apex obtuse to acute;</text>
      <biological_entity id="o4602" name="midvein" name_original="midvein" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o4603" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o4604" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o4605" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o4606" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s2" value="obscurely" value_original="obscurely" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o4607" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 3–5 pairs, short-petiolate or sessile, 17–60 × 1–5 mm, blade lanceolate to linear, base tapered to clasping, margins entire or remotely dentate, apex acute.</text>
      <biological_entity constraint="cauline" id="o4608" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4609" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o4610" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o4611" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o4612" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses interrupted or continuous, cylindric, 2–11 cm, axis glandular-pubescent, verticillasters 3–6, cymes 2–6-flowered, 2 per node;</text>
      <biological_entity id="o4613" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4614" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o4615" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o4616" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-6-flowered" value_original="2-6-flowered" />
        <character constraint="per node" constraintid="o4617" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4617" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate to linear, 12–55 × 2–9 mm;</text>
      <biological_entity constraint="proximal" id="o4618" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="55" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-pubescent.</text>
      <biological_entity id="o4619" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o4620" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes lanceolate, 6–8 × 1–1.4 mm, glandular-pubescent;</text>
      <biological_entity id="o4621" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o4622" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla violet to lavender or purple, with dark violet nectar guides, bilabiate, tubular-funnelform, 8–11 mm, white-lanate internally abaxially, tube 4–5 mm, throat gradually inflated, not constricted at orifice, 3.5–4.5 mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o4623" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4624" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s8" to="lavender or purple" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="bilabiate" value_original="bilabiate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="internally abaxially; abaxially" name="pubescence" src="d0_s8" value="white-lanate" value_original="white-lanate" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o4625" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="dark violet" value_original="dark violet" />
      </biological_entity>
      <biological_entity id="o4626" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4627" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character constraint="at orifice" constraintid="o4628" is_modifier="false" modifier="not" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" notes="" src="d0_s8" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s8" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
      <biological_entity id="o4628" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <relation from="o4624" id="r408" name="with" negation="false" src="d0_s8" to="o4625" />
    </statement>
    <statement id="d0_s9">
      <text>stamens included or longer pair reaching orifice or slightly exserted, pollen-sacs opposite, explanate, 0.5–0.9 mm, dehiscing completely, sutures papillate;</text>
      <biological_entity id="o4629" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4630" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o4631" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o4632" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="explanate" value_original="explanate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s9" to="0.9" to_unit="mm" />
        <character is_modifier="false" modifier="completely" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o4633" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o4630" id="r409" name="reaching" negation="false" src="d0_s9" to="o4631" />
    </statement>
    <statement id="d0_s10">
      <text>staminode 6–8 mm, exserted, 0.3–0.4 mm diam., tip recurved to coiled, distal 4–5 mm densely pilose, hairs white or pale-yellow, to 1 mm;</text>
      <biological_entity id="o4634" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4635" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4636" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4637" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o4638" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 5–7 mm.</text>
      <biological_entity id="o4639" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4640" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 4–6 × 3–4 mm.</text>
      <biological_entity id="o4641" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Penstemon concinnus is known from the Snake Range in Lincoln and White Pine counties, Nevada, and in the Burbank Hills, Needle Range, and Tunnel Spring and Wah Wah mountains in Beaver and Millard counties, Utah.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly soils, desert shrublands, pinyon-juniper woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="desert shrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>