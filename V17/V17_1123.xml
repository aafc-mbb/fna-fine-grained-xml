<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">634</other_info_on_meta>
    <other_info_on_meta type="mention_page">568</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="mention_page">635</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="Pennell &amp; Ownbey" date="1950" rank="species">nivea</taxon_name>
    <place_of_publication>
      <publication_title>Notul. Nat. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>227: 2. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species nivea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>75.</number>
  <other_name type="common_name">Snow or snowy paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.5–1.6 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o3714" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="1.6" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o3715" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o3714" id="r338" name="with" negation="false" src="d0_s2" to="o3715" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few to several, erect to ascending, decumbent at base, unbranched except for small, leafy axillary shoots, hairs weakly spreading to appressed, ± matted, especially distally on stem, fairly short and sparse proximally, longer and denser distally, soft, eglandular, becoming woolly, often obscuring surface.</text>
      <biological_entity id="o3716" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="several" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
        <character constraint="at base" constraintid="o3717" is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character constraint="except-for axillary shoots" constraintid="o3718" is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o3717" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="axillary" id="o3718" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o3719" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="weakly spreading" name="orientation" src="d0_s3" to="appressed" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s3" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="fairly" name="height_or_length_or_size" notes="" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" modifier="proximally" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="distally" name="density" src="d0_s3" value="denser" value_original="denser" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s3" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o3720" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity id="o3721" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o3719" id="r339" modifier="especially distally; distally" name="on" negation="false" src="d0_s3" to="o3720" />
      <relation from="o3719" id="r340" modifier="often" name="obscuring" negation="false" src="d0_s3" to="o3721" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves gray with hairs, surface green to purple, linear to narrowly lanceolate, 1–3.8 cm, not fleshy, margins plane, sometimes ± wavy, involute, 0–3-lobed, apex acute;</text>
      <biological_entity id="o3722" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="with hairs" constraintid="o3723" is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o3723" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o3724" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purple" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3.8" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o3725" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0-3-lobed" value_original="0-3-lobed" />
      </biological_entity>
      <biological_entity id="o3726" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending-spreading, linear to narrowly lanceolate, apex acute to obtuse.</text>
      <biological_entity id="o3727" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending-spreading" value_original="ascending-spreading" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o3728" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 2.5–6 × 1–2.5 cm;</text>
      <biological_entity id="o3729" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts greenish to pale yellow-green or very pale, dull purplish throughout, lanceolate to oblong, (0–) 3 (–5) -lobed;</text>
      <biological_entity id="o3730" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s7" to="pale yellow-green" />
        <character is_modifier="false" modifier="very" name="coloration" src="d0_s7" value="pale" value_original="pale" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="(0-)3(-5)-lobed" value_original="(0-)3(-5)-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes usually ascending, linear, medium length to long, arising near mid length, apex acute.</text>
      <biological_entity id="o3731" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="length" src="d0_s8" value="medium" value_original="medium" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character constraint="near mid lobes" constraintid="o3732" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="mid" id="o3732" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o3733" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces yellow, color mostly obscured by whitish hairs, 15–22 mm;</text>
      <biological_entity id="o3734" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character constraint="by hairs" constraintid="o3735" is_modifier="false" modifier="mostly" name="coloration" src="d0_s9" value="obscured" value_original="obscured" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3735" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>abaxial, adaxial, and lateral clefts (5.5–) 7–12 mm, 35–55% of calyx length, often appearing shorter because matted hairs stitch proximal part of clefts shut, all 4 clefts subequal;</text>
      <biological_entity constraint="abaxial" id="o3736" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial" id="o3737" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character constraint="because hairs" constraintid="o3739" is_modifier="false" modifier="35-55%; often" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="adaxial lateral" id="o3738" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character constraint="because hairs" constraintid="o3739" is_modifier="false" modifier="35-55%; often" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o3739" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s10" value="matted" value_original="matted" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3740" name="part" name_original="part" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o3741" name="cleft" name_original="clefts" src="d0_s10" type="structure" />
      <biological_entity id="o3742" name="cleft" name_original="clefts" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
      <relation from="o3740" id="r341" name="part_of" negation="false" src="d0_s10" to="o3741" />
    </statement>
    <statement id="d0_s11">
      <text>lobes broadly linear, apex acute.</text>
      <biological_entity id="o3743" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o3744" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight, 18–25 mm;</text>
      <biological_entity id="o3745" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s12" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 3.5–5.5 mm;</text>
    </statement>
    <statement id="d0_s14">
      <text>subequal to calyx, or beak and sometimes abaxial lip exserted;</text>
      <biological_entity id="o3746" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
        <character constraint="to beak" constraintid="o3748" is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o3747" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
      <biological_entity id="o3748" name="beak" name_original="beak" src="d0_s14" type="structure" />
      <biological_entity id="o3749" name="lip" name_original="lip" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s14" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak adaxially yellow, 6–8 mm, hairs moderately long, matted on midline, very short-glandular on sides;</text>
      <biological_entity id="o3750" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3751" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="moderately" name="length_or_size" src="d0_s15" value="long" value_original="long" />
        <character constraint="on midline" constraintid="o3752" is_modifier="false" name="growth_form" src="d0_s15" value="matted" value_original="matted" />
        <character constraint="on sides" constraintid="o3753" is_modifier="false" modifier="very" name="architecture_or_function_or_pubescence" notes="" src="d0_s15" value="short-glandular" value_original="short-glandular" />
      </biological_entity>
      <biological_entity id="o3752" name="midline" name_original="midline" src="d0_s15" type="structure" />
      <biological_entity id="o3753" name="side" name_original="sides" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>abaxial lip green, inconspicuous, slightly pouched, 3.5–5.5 mm, 60–90% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o3754" name="lip" name_original="lip" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s16" value="pouched" value_original="pouched" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3755" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <relation from="o3754" id="r342" modifier="60-90%" name="as long as" negation="false" src="d0_s16" to="o3755" />
    </statement>
    <statement id="d0_s17">
      <text>teeth erect, white or yellow, 0.5–3 mm. 2n = 24.</text>
      <biological_entity id="o3756" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3757" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja nivea is endemic to alpine habitats in the mountains of northwestern Wyoming and adjacent Montana. It forms occasional hybrids with C. pulchella, which often shares its habitat, as on the Beartooth Plateau in northwestern Wyoming.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly slopes and flats, turf and fellfields, mostly alpine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="turf" />
        <character name="habitat" value="fellfields" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–3600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>