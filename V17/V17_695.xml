<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PLANTAGO</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">lanceolata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 113. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus plantago;species lanceolata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Plantago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lanceolata</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus plantago;species lanceolata;variety angustifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lanceolata</taxon_name>
    <taxon_name authority="Mertens &amp; W. D. J. Koch" date="unknown" rank="variety">sphaerostachya</taxon_name>
    <taxon_hierarchy>genus p.;species lanceolata;variety sphaerostachya</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <other_name type="common_name">Narrow-leaf or English plantain</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o38436" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex hairy or glabrous;</text>
      <biological_entity id="o38437" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots fibrous, slender.</text>
      <biological_entity id="o38438" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems 0–20 mm.</text>
      <biological_entity id="o38439" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 30–300 × 5–25 (–45) mm;</text>
      <biological_entity id="o38440" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="300" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade lanceolate to oblanceolate, margins entire or toothed, veins conspicuous, surfaces glabrous or sericeous.</text>
      <biological_entity id="o38441" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o38442" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o38443" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o38444" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scapes 300–400 mm, groove-angled, hairy.</text>
      <biological_entity id="o38445" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" from="300" from_unit="mm" name="some_measurement" src="d0_s6" to="400" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="groove-angled" value_original="groove-angled" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikes grayish, whitish, or yellowish, (5–) 100–450 (–1000) mm, usually shorter than scape, densely flowered, shiny;</text>
      <biological_entity id="o38446" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="100" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="450" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1000" to_unit="mm" />
        <character char_type="range_value" from="100" from_unit="mm" name="some_measurement" src="d0_s7" to="450" to_unit="mm" />
        <character constraint="than scape" constraintid="o38447" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s7" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity id="o38447" name="scape" name_original="scape" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>corolla lobes of neighboring flowers often overlapping;</text>
      <biological_entity constraint="flower" id="o38448" name="lobe" name_original="lobes" src="d0_s8" type="structure" constraint_original="flower corolla; flower">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="neighboring" id="o38449" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <relation from="o38448" id="r2950" name="part_of" negation="false" src="d0_s8" to="o38449" />
    </statement>
    <statement id="d0_s9">
      <text>bracts broadly ovate, 2 mm, length 0.8–1 times sepals.</text>
      <biological_entity id="o38450" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character constraint="sepal" constraintid="o38451" is_modifier="false" name="length" src="d0_s9" value="0.8-1 times sepals" value_original="0.8-1 times sepals" />
      </biological_entity>
      <biological_entity id="o38451" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals 2–2.5 mm, adaxial 2 connate;</text>
      <biological_entity id="o38452" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o38453" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="adaxial" value_original="adaxial" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla radially symmetric, lobes reflexed, 2–2.5 mm, base obtuse;</text>
      <biological_entity id="o38454" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o38455" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o38456" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38457" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 4.</text>
      <biological_entity id="o38458" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o38459" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds (1 or) 2, 2–3 (–4) mm.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 12.</text>
      <biological_entity id="o38460" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o38461" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plantago lanceolata is known from historic collections in Manitoba and Saskatchewan.</discussion>
  <discussion>The name Plantago altissima Linnaeus sometimes has been misapplied to North American plants of P. lanceolata.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, trails, lawns, urban areas, other disturbed sites.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="trails" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="urban areas" />
        <character name="habitat" value="other disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Greenland; St. Pierre and Miquelon; B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; introduced also in Central America, South America, Asia, Africa, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also in Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>