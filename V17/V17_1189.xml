<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">667</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Behr" date="1855" rank="genus">CHLOROPYRON</taxon_name>
    <taxon_name authority="(Nuttall ex Bentham) A. Heller" date="1907" rank="species">maritimum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">maritimum</taxon_name>
    <taxon_hierarchy>family orobanchaceae;genus chloropyron;species maritimum;subspecies maritimum</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>1a.</number>
  <other_name type="common_name">Saltmarsh bird's-beak</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 15–30 cm, puberulent and often villous or short-pilose;</text>
      <biological_entity id="o19355" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branches decumbent to ascending, distals usually overtopping central spike.</text>
      <biological_entity id="o19356" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
      <biological_entity id="o19357" name="distal" name_original="distals" src="d0_s1" type="structure" />
      <biological_entity constraint="central" id="o19358" name="spike" name_original="spike" src="d0_s1" type="structure" />
      <relation from="o19357" id="r1497" name="overtopping" negation="false" src="d0_s1" to="o19358" />
    </statement>
    <statement id="d0_s2">
      <text>Spikes: bract margins entire or slightly notched.</text>
      <biological_entity id="o19359" name="spike" name_original="spikes" src="d0_s2" type="structure" />
      <biological_entity constraint="bract" id="o19360" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: corolla-tube and abaxial lip pale-yellow to white, galea purple-red or brown-red.</text>
      <biological_entity id="o19361" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o19362" name="corolla-tube" name_original="corolla-tube" src="d0_s3" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s3" to="white" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19363" name="lip" name_original="lip" src="d0_s3" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s3" to="white" />
      </biological_entity>
      <biological_entity id="o19364" name="galea" name_original="galea" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="purple-red" value_original="purple-red" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown-red" value_original="brown-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seeds 15–20, 1.5–2.5 mm. 2n = 30.</text>
      <biological_entity id="o19365" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" unit=",1.5-2.5 mm" />
        <character char_type="range_value" from="15" name="some_measurement" src="d0_s4" to="20" unit=",1.5-2.5 mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19366" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants intermediate between the coastal subsp. maritimum and the inland subsp. canescens have been found in southern California.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal salt marshes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal salt" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>