<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">626</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="mention_page">632</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus f." date="1782" rank="genus">CASTILLEJA</taxon_name>
    <taxon_name authority="(Eastwood) Pennell" date="1947" rank="species">mendocinensis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>99: 184. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus castilleja;species mendocinensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Castilleja</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">latifolia</taxon_name>
    <taxon_name authority="Eastwood" date="1936" rank="subspecies">mendocinensis</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>1: 238. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus castilleja;species latifolia;subspecies mendocinensis</taxon_hierarchy>
  </taxon_identification>
  <number>65.</number>
  <other_name type="common_name">Mendocino Coast paintbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1.7–6.5 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from a woody caudex;</text>
    </statement>
    <statement id="d0_s2">
      <text>with a taproot.</text>
      <biological_entity id="o35053" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1.7" from_unit="dm" name="some_measurement" src="d0_s0" to="6.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o35054" name="taproot" name_original="taproot" src="d0_s2" type="structure" />
      <relation from="o35053" id="r2679" name="with" negation="false" src="d0_s2" to="o35054" />
    </statement>
    <statement id="d0_s3">
      <text>Stems few-to-many, decumbent to ascending, much-branched, with leafy axillary shoots, villous, hairs spreading, long, stiff to soft, eglandular, mixed with short-glandular ones.</text>
      <biological_entity id="o35055" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o35056" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o35057" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
        <character constraint="with short-glandular ones" constraintid="o35058" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="short-glandular" id="o35058" name="one" name_original="ones" src="d0_s3" type="structure" />
      <relation from="o35055" id="r2680" name="with" negation="false" src="d0_s3" to="o35056" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves gray-green becoming ± purple, or green, ± cupshaped, oblong to narrowly elliptic or suborbicular, 0.5–2 (–5) cm, ± fleshy, cupulate throughout, sometimes obscurely so on distal portion of stem, margins plane, flat to involute, 0–3-lobed, apex rounded;</text>
      <biological_entity id="o35059" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" modifier="becoming more or less" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming more or less" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="narrowly elliptic or suborbicular" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s4" value="cupulate" value_original="cupulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o35060" name="portion" name_original="portion" src="d0_s4" type="structure" />
      <biological_entity id="o35061" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity id="o35062" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s4" to="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="0-3-lobed" value_original="0-3-lobed" />
      </biological_entity>
      <biological_entity id="o35063" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o35059" id="r2681" modifier="sometimes obscurely; obscurely" name="on" negation="false" src="d0_s4" to="o35060" />
      <relation from="o35060" id="r2682" name="part_of" negation="false" src="d0_s4" to="o35061" />
    </statement>
    <statement id="d0_s5">
      <text>lobes ascending, oblong to rounded, apex truncate or rounded, sometimes ± acute.</text>
      <biological_entity id="o35064" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o35065" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 5–23 × 1.5–3.5 cm;</text>
      <biological_entity id="o35066" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="23" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts proximally greenish, distally bright red or red-orange, sometimes orange, oblong, ovate, or widely cuneate to widely obovate to suborbiculate, sometimes cupshaped, 0–3-lobed, sometimes with 3 additional shallow teeth at tip of central lobe;</text>
      <biological_entity id="o35067" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="bright red" value_original="bright red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="widely cuneate" name="shape" src="d0_s7" to="widely obovate" />
        <character char_type="range_value" from="widely cuneate" name="shape" src="d0_s7" to="widely obovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="cup-shaped" value_original="cup-shaped" />
        <character is_modifier="false" name="shape" src="d0_s7" value="0-3-lobed" value_original="0-3-lobed" />
      </biological_entity>
      <biological_entity id="o35068" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="additional" value_original="additional" />
        <character is_modifier="true" name="depth" src="d0_s7" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o35069" name="tip" name_original="tip" src="d0_s7" type="structure" />
      <biological_entity constraint="central" id="o35070" name="lobe" name_original="lobe" src="d0_s7" type="structure" />
      <relation from="o35067" id="r2683" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o35068" />
      <relation from="o35068" id="r2684" name="at" negation="false" src="d0_s7" to="o35069" />
      <relation from="o35069" id="r2685" name="part_of" negation="false" src="d0_s7" to="o35070" />
    </statement>
    <statement id="d0_s8">
      <text>lobes erect, oblong to broadly triangular, short, arising above mid length, apex truncate, rounded, or obtuse, lateral ones sometimes acute.</text>
      <biological_entity id="o35071" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="broadly triangular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character constraint="above mid lobes" constraintid="o35072" is_modifier="false" name="orientation" src="d0_s8" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="mid" id="o35072" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o35073" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyces colored as bracts, often with a yellow central band, 20–31 mm;</text>
      <biological_entity id="o35074" name="calyx" name_original="calyces" src="d0_s9" type="structure">
        <character constraint="as bracts" constraintid="o35075" is_modifier="false" name="coloration" src="d0_s9" value="colored" value_original="colored" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35075" name="bract" name_original="bracts" src="d0_s9" type="structure" />
      <biological_entity constraint="central" id="o35076" name="band" name_original="band" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o35074" id="r2686" modifier="often" name="with" negation="false" src="d0_s9" to="o35076" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial and adaxial clefts 8–12 mm, ca. 50% of calyx length, deeper than laterals, lateral 2–6 mm, 10–15% of calyx length;</text>
      <biological_entity constraint="calyx" id="o35077" name="cleft" name_original="clefts" src="d0_s10" type="structure" constraint_original="calyx abaxial and adaxial; calyx">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character constraint="than laterals" constraintid="o35079" is_modifier="false" name="length" src="d0_s10" value="deeper" value_original="deeper" />
        <character is_modifier="false" name="length" src="d0_s10" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35078" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o35079" name="lateral" name_original="laterals" src="d0_s10" type="structure" />
      <relation from="o35077" id="r2687" modifier="50%" name="part_of" negation="false" src="d0_s10" to="o35078" />
    </statement>
    <statement id="d0_s11">
      <text>lobes oblong to broadly or narrowly triangular, apex obtuse or rounded, sometimes acute.</text>
      <biological_entity id="o35080" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="broadly or narrowly triangular" />
      </biological_entity>
      <biological_entity id="o35081" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Corollas straight or slightly curved, 28–45 mm;</text>
      <biological_entity id="o35082" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="28" from_unit="mm" name="some_measurement" src="d0_s12" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tube 18–20 mm;</text>
      <biological_entity id="o35083" name="tube" name_original="tube" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s13" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>beak exserted, adaxially green or yellow-green, 15–25 mm;</text>
      <biological_entity id="o35084" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s14" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>abaxial lip deep green, reduced, visible in front cleft, 1–1.5 mm, 10% as long as beak;</text>
      <biological_entity constraint="abaxial" id="o35085" name="lip" name_original="lip" src="d0_s15" type="structure">
        <character is_modifier="false" name="depth" src="d0_s15" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="green" value_original="green" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
        <character constraint="in front" constraintid="o35086" is_modifier="false" name="prominence" src="d0_s15" value="visible" value_original="visible" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35086" name="front" name_original="front" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o35087" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <relation from="o35085" id="r2688" modifier="10%" name="as long as" negation="false" src="d0_s15" to="o35087" />
    </statement>
    <statement id="d0_s16">
      <text>teeth incurved, green, 0.5–1 mm. 2n = 72.</text>
      <biological_entity id="o35088" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35089" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Castilleja mendocinensis is a coastal plant from Mendocino County, California, northward to Curry County, Oregon. Its close relative, C. latifolia, occurs in similar habitats south of San Francisco Bay. There is one known population in Oregon, and a number of California localities are threatened by coastal development.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal scrub, headlands, sea bluffs, over sandstone or serpentine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="headlands" />
        <character name="habitat" value="sea bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>