<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">528</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PEDICULARIS</taxon_name>
    <taxon_name authority="Smith in A. Rees" date="1813" rank="species">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>Cycl.</publication_title>
      <place_in_publication>26: Pedicularis no. 4. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus pedicularis;species parviflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pedicularis</taxon_name>
    <taxon_name authority="Richardson" date="unknown" rank="species">macrodontis</taxon_name>
    <taxon_hierarchy>genus pedicularis;species macrodontis</taxon_hierarchy>
  </taxon_identification>
  <number>28.</number>
  <other_name type="common_name">Small-flowered lousewort</other_name>
  <other_name type="common_name">pédiculaire parviflore</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 7–65 cm.</text>
      <biological_entity id="o15186" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="65" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 0–2, blade elliptic, 3–7 x 2–5 mm, 1-pinnatifid or 2-pinnatifid, margins of adjacent lobes nonoverlapping or slightly overlapping distally, dentate, surfaces glabrous;</text>
      <biological_entity id="o15187" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o15188" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="2" />
      </biological_entity>
      <biological_entity id="o15189" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o15190" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="nonoverlapping" value_original="nonoverlapping" />
        <character is_modifier="false" modifier="slightly; distally" name="arrangement" src="d0_s1" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o15191" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o15192" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o15190" id="r1192" name="part_of" negation="false" src="d0_s1" to="o15191" />
    </statement>
    <statement id="d0_s2">
      <text>cauline 0–12, blade lanceolate or elliptic to deltate, 5–50 x 3–20 mm, 1-pinnatifid or 2-pinnatifid, margins of adjacent lobes nonoverlapping or slightly overlapping distally, serrate, surfaces glabrous.</text>
      <biological_entity id="o15193" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o15194" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="12" />
      </biological_entity>
      <biological_entity id="o15195" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="deltate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o15196" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="nonoverlapping" value_original="nonoverlapping" />
        <character is_modifier="false" modifier="slightly; distally" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15197" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o15198" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o15196" id="r1193" name="part_of" negation="false" src="d0_s2" to="o15197" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes simple or paniculate, 1–4, exceeding basal leaves, each 3–12-flowered;</text>
      <biological_entity id="o15199" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-12-flowered" value_original="3-12-flowered" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15200" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o15199" id="r1194" name="exceeding" negation="false" src="d0_s3" to="o15200" />
    </statement>
    <statement id="d0_s4">
      <text>bracts deltate to trullate, 5–30 x 3–15 mm, 1-pinnatifid or 2-pinnatifid, margins serrate, surfaces glabrous or slightly arachnoid.</text>
      <biological_entity id="o15201" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s4" to="trullate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o15202" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15203" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s4" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–2.5 mm.</text>
      <biological_entity id="o15204" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 5.5–8 mm, glabrous, lobes 2 (–4), deltate, 2–3.5 mm, apex pinnatifid, sometimes 2-fid into triangular lobes, entire, glabrous;</text>
      <biological_entity id="o15205" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15206" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15207" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="4" />
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15208" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character constraint="into lobes" constraintid="o15209" is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15209" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla 8–16 mm, tube light pink to purple, 5–11 mm;</text>
      <biological_entity id="o15210" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15211" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15212" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character char_type="range_value" from="light pink" name="coloration" src="d0_s7" to="purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>galea purple, 3–6 mm, beakless, margins 1-toothed medially, entire distally, apex arching slightly over abaxial lip;</text>
      <biological_entity id="o15213" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15214" name="galea" name_original="galea" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="beakless" value_original="beakless" />
      </biological_entity>
      <biological_entity id="o15215" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="medially" name="shape" src="d0_s8" value="1-toothed" value_original="1-toothed" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15216" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character constraint="over abaxial lip" constraintid="o15217" is_modifier="false" name="orientation" src="d0_s8" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15217" name="lip" name_original="lip" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial lip pink to purple, sometimes purple-spotted, 2–5 mm.</text>
      <biological_entity id="o15218" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o15219" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="purple-spotted" value_original="purple-spotted" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pedicularis parviflora belongs to a complex of taxa [including P. parviflora var. macrodontis (Richardson) S. L. Welsh, and P. pennellii] that have traditionally been treated as species, subspecies, or varieties. Two features unite this group: a highly branched paniculate raceme and a tooth on each medial margin of the galea covered with pyriform glands on the inner surface. Lack of apical teeth sets P. parviflora apart from P. palustris and P. pennellii, which have both sets of teeth.</discussion>
  <discussion>The distinction of Pedicularis macrodontis is not clear. With a galea that lacks apical teeth, it is clearly associated with P. parviflora, but there are no unique characters to set it apart as a distinct species; all foliar and floral features are very similar. A number of intermediate specimens were seen, suggesting a lack of reproductive barriers; treatment as a variety of P. parviflora, following S. L. Welsh (1974), may be warranted, but further research is required before recognizing it as a taxon.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Muskegs, boggy flood plains, gravel stream bars, moist meadows, sedge meadows, fens, bogs, black spruce-tamarack wetlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="muskegs" />
        <character name="habitat" value="boggy flood plains" />
        <character name="habitat" value="stream bars" modifier="gravel" />
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="sedge meadows" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="black spruce-tamarack wetlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Nunavut, Ont., Sask., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>