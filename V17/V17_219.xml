<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Straw" date="1967" rank="genus">KECKIELLA</taxon_name>
    <taxon_name authority="(Lindley) Straw" date="1967" rank="species">breviflora</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>19: 203. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus keckiella;species breviflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Penstemon</taxon_name>
    <taxon_name authority="Lindley" date="1946" rank="species">breviflorus</taxon_name>
    <place_of_publication>
      <publication_title>Edwards's Bot. Reg.</publication_title>
      <place_in_publication>23: plate 1946. 1837</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species breviflorus</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Bush beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, spreading with age, 5–20 dm, glabrous when young, glaucous.</text>
      <biological_entity id="o15840" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character constraint="with age" constraintid="o15841" is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o15841" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite, subsessile;</text>
      <biological_entity id="o15842" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oblanceolate or lanceolate, 10–40 mm, margins 4–12-toothed.</text>
      <biological_entity id="o15843" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15844" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="4-12-toothed" value_original="4-12-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences thyrses, glabrous or sticky-hairy.</text>
      <biological_entity constraint="inflorescences" id="o15845" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="sticky-hairy" value_original="sticky-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: caly× 4–8 mm, lobes lanceolate to ovate;</text>
      <biological_entity id="o15846" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character name="area" src="d0_s4" unit="mm" value="×4-8" value_original="×4-8" />
      </biological_entity>
      <biological_entity id="o15847" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla white or cream, lobes sometimes rose tinged, lined purplish or pinkish, 12–18 mm, tube plus distinct throat 4–8 mm, shorter than adaxial lip, adaxial lip 8–12 mm;</text>
      <biological_entity id="o15848" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o15849" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="cream" value_original="cream" />
      </biological_entity>
      <biological_entity id="o15850" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="rose tinged" value_original="rose tinged" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="lined purplish" value_original="lined purplish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15851" name="tube" name_original="tube" src="d0_s5" type="structure" />
      <biological_entity id="o15852" name="throat" name_original="throat" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character constraint="than adaxial lip" constraintid="o15853" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15853" name="lip" name_original="lip" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o15854" name="lip" name_original="lip" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pollen-sacs 0.6–0.8 mm;</text>
      <biological_entity id="o15855" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15856" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminode glabrous, slightly exserted.</text>
      <biological_entity id="o15857" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15858" name="staminode" name_original="staminode" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces glandular.</description>
      <determination>4a. Keckiella breviflora var. breviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces glabrous.</description>
      <determination>4b. Keckiella breviflora var. glabrisepala</determination>
    </key_statement>
  </key>
</bio:treatment>