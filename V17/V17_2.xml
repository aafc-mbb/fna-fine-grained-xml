<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Craig C. Freeman</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">454</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">453</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Nakai" date="unknown" rank="family">PAULOWNIACEAE</taxon_name>
    <taxon_name authority="Siebold &amp; Zuccarini" date="1835" rank="genus">PAULOWNIA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Jap.</publication_title>
      <place_in_publication>1: 25. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family paulowniaceae;genus paulownia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Anna Paulowna Romanov, 1795–1865, Grand Duchess of Russia and daughter of Czar Paul I, Hereditary Princess of the Netherlands</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Thyrses: bracts absent.</text>
      <biological_entity id="o30210" name="thyrse" name_original="thyrses" src="d0_s0" type="structure" />
      <biological_entity id="o30211" name="bract" name_original="bracts" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pedicels present;</text>
      <biological_entity id="o30212" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bracteoles absent.</text>
      <biological_entity id="o30213" name="bracteole" name_original="bracteoles" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: calyx campanulate, lobes ovate to broadly ovate or oblong;</text>
      <biological_entity id="o30214" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o30215" name="calyx" name_original="calyx" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o30216" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly ovate or oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corolla lavender, pinkish purple, or purple externally, whitish or yellowish internally on palate and lined with reddish purple nectar guides, abaxial lobes 3, adaxial 2;</text>
      <biological_entity id="o30217" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o30218" name="corolla" name_original="corolla" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration_or_odor" src="d0_s4" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pinkish purple" value_original="pinkish purple" />
        <character is_modifier="false" modifier="externally" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="whitish" value_original="whitish" />
        <character constraint="on palate" constraintid="o30219" is_modifier="false" name="coloration" src="d0_s4" value="yellowish" value_original="yellowish" />
        <character constraint="with nectar guides" constraintid="o30220" is_modifier="false" name="architecture" notes="" src="d0_s4" value="lined" value_original="lined" />
      </biological_entity>
      <biological_entity id="o30219" name="palate" name_original="palate" src="d0_s4" type="structure" />
      <biological_entity constraint="nectar" id="o30220" name="guide" name_original="guides" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30221" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30222" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens: filaments glabrous;</text>
      <biological_entity id="o30223" name="stamen" name_original="stamens" src="d0_s5" type="structure" />
      <biological_entity id="o30224" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stigma capitate.</text>
      <biological_entity id="o30225" name="stamen" name_original="stamens" src="d0_s6" type="structure" />
      <biological_entity id="o30226" name="stigma" name_original="stigma" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds: margins winged, wings clear or tan.</text>
      <biological_entity id="o30227" name="seed" name_original="seeds" src="d0_s7" type="structure" />
      <biological_entity id="o30228" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>x = 20.</text>
      <biological_entity id="o30229" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="clear" value_original="clear" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity constraint="x" id="o30230" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 7 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; Asia (China); introduced also in Europe, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia (China)" establishment_means="introduced" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paulownias long have held mythical, spiritual, cultural, and economic significance in China and Japan (Hu S. Y. 1959, 1961). The wood of some species is highly prized in Asia; Paulownia tomentosa is grown in the United States in plantations for wood that is exported to Japan.</discussion>
  <discussion>The fossil record provides evidence of Paulownia in North America and Europe during the Tertiary (C. J. Smiley 1961).</discussion>
  
</bio:treatment>