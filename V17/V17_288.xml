<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">PLANTAGINACEAE</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">PENSTEMON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Penstemon</taxon_name>
    <taxon_name authority="Crosswhite" date="1966" rank="section">Chamaeleon</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">dasyphyllus</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 112. 1859</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section chamaeleon;species dasyphyllus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Thick-leaf beardtongue</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20–50 cm, puberulent or retrorsely hairy proximally, puberulent and glandular-pubescent distally.</text>
      <biological_entity id="o13904" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves puberulent or retrorsely hairy, sometimes only along midveins and margins;</text>
      <biological_entity id="o13906" name="midvein" name_original="midveins" src="d0_s1" type="structure" />
      <biological_entity id="o13907" name="margin" name_original="margins" src="d0_s1" type="structure" />
      <relation from="o13905" id="r1085" modifier="sometimes only; only" name="along" negation="false" src="d0_s1" to="o13906" />
      <relation from="o13905" id="r1086" modifier="sometimes only; only" name="along" negation="false" src="d0_s1" to="o13907" />
    </statement>
    <statement id="d0_s2">
      <text>basal and proximal cauline 28–68 × 3–9 mm, blade oblanceolate to lanceolate or linear, base tapered, margins entire, apex obtuse to acute;</text>
      <biological_entity id="o13905" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o13908" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o13909" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o13910" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13911" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13912" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 6–9 pairs, 32–95 × 4–12 mm, blade lanceolate to linear, base tapered, apex acute.</text>
      <biological_entity constraint="cauline" id="o13913" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="9" />
        <character char_type="range_value" from="32" from_unit="mm" name="length" src="d0_s3" to="95" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13914" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o13915" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13916" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Thyrses ± secund, 6–28 cm, axis glandular-pubescent, verticillasters 4–11, cymes 1-flowered or 2-flowered;</text>
      <biological_entity id="o13917" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="28" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13918" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o13919" name="verticillaster" name_original="verticillasters" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="11" />
      </biological_entity>
      <biological_entity id="o13920" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-flowered" value_original="1-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-flowered" value_original="2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal bracts lanceolate to linear, 4–35 × 1–4 mm;</text>
      <biological_entity constraint="proximal" id="o13921" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels glandular-pubescent.</text>
      <biological_entity id="o13922" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o13923" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: calyx lobes ovate to lanceolate, 5–8 × 2–2.8 mm, glandular-pubescent;</text>
      <biological_entity id="o13924" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o13925" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corolla violet to lavender to purple, lined internally abaxially with purple nectar guides, ventricose, 25–35 mm, glabrous internally, tube 7–9 mm, throat abruptly inflated, 9–10 mm diam., slightly 2-ridged abaxially;</text>
      <biological_entity id="o13926" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13927" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="violet" name="coloration" src="d0_s8" to="lavender" />
        <character constraint="with nectar guides" constraintid="o13928" is_modifier="false" modifier="internally abaxially" name="architecture" src="d0_s8" value="lined" value_original="lined" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="internally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o13928" name="guide" name_original="guides" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o13929" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13930" name="throat" name_original="throat" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; abaxially" name="shape" src="d0_s8" value="2-ridged" value_original="2-ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens: longer pair reaching orifice, pollen-sacs divergent, 1.8–2.2 mm, sutures denticulate, teeth to 0.3 mm;</text>
      <biological_entity id="o13931" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o13932" name="orifice" name_original="orifice" src="d0_s9" type="structure" />
      <biological_entity id="o13933" name="pollen-sac" name_original="pollen-sacs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="distance" src="d0_s9" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13934" name="suture" name_original="sutures" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o13935" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <relation from="o13931" id="r1087" name="reaching" negation="false" src="d0_s9" to="o13932" />
    </statement>
    <statement id="d0_s10">
      <text>staminode 17–19 mm, included, 0.6–1 mm diam.;</text>
      <biological_entity id="o13936" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o13937" name="staminode" name_original="staminode" src="d0_s10" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s10" to="19" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="diameter" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 15–20 mm.</text>
      <biological_entity id="o13938" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o13939" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 11–15 × 7–9 mm. 2n = 16.</text>
      <biological_entity id="o13940" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s12" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13941" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, Penstemon dasyphyllus is known from southeastern Arizona (Cochise, Gila, Pima, Pinal, and Santa Cruz counties), southwestern New Mexico (Hidalgo and Luna counties), and western Texas (Brewster, Crockett, Pecos, Presidio, and Terrell counties). Populations are documented southward to northern Durango, Mexico.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky ridges, gravelly slopes, desert grasslands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="desert grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–1700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>