<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">687</other_info_on_meta>
    <other_info_on_meta type="mention_page">685</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">OROBANCHACEAE</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="1836" rank="genus">TRIPHYSARIA</taxon_name>
    <taxon_name authority="(Bentham) T. I. Chuang &amp; Heckard" date="1991" rank="species">pusilla</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>16: 661. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus triphysaria;species pusilla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthocarpus</taxon_name>
    <taxon_name authority="Bentham" date="1835" rank="species">pusillus</taxon_name>
    <place_of_publication>
      <publication_title>Scroph. Ind.,</publication_title>
      <place_in_publication>12. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus orthocarpus;species pusillus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">O.</taxon_name>
    <taxon_name authority="D. Y. Hong" date="unknown" rank="species">chinensis</taxon_name>
    <taxon_hierarchy>genus o.;species chinensis</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Dwarf owl’s-clover or Johnny-tuck</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple or with 1–6 decumbent to ascending branches proximally, (2–) 4–15 (–30) cm, glabrous proximally, retrorsely short-strigose distally.</text>
      <biological_entity id="o3837" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="with branches" constraintid="o3838" is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="with 1-6 decumbent to ascending branches" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="retrorsely; distally" name="pubescence" src="d0_s0" value="short-strigose" value_original="short-strigose" />
      </biological_entity>
      <biological_entity id="o3838" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s0" to="6" />
        <character char_type="range_value" from="decumbent" is_modifier="true" name="orientation" src="d0_s0" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves puberulent;</text>
      <biological_entity id="o3839" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal cauline: blade linear, 5–30 mm;</text>
      <biological_entity constraint="cauline proximal" id="o3840" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3841" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline: blade ± ovate to obovate, 5–30 mm, base sessile, margins pinnatifid, rarely bipinnatifid, lateral lobes 2–8, threadlike.</text>
      <biological_entity constraint="cauline" id="o3842" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3843" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="less ovate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3844" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3845" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s3" value="bipinnatifid" value_original="bipinnatifid" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o3846" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" name="shape" src="d0_s3" value="thread-like" value_original="threadlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelike racemes interrupted, open distally, (1.5–) 2.5–15 cm;</text>
      <biological_entity id="o3847" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle absent;</text>
      <biological_entity id="o3848" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts pinnatifid, rarely bipinnatifid, ± obovate, 5–19 mm, lateral lobes 2–6 (–10), threadlike.</text>
      <biological_entity id="o3849" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s6" value="bipinnatifid" value_original="bipinnatifid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o3850" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="6" />
        <character is_modifier="false" name="shape" src="d0_s6" value="thread-like" value_original="threadlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.3–1 mm, glabrous.</text>
      <biological_entity id="o3851" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx (3–) 5–7 mm, puberulent, tube 2–3 mm, lobes triangular to narrowly lanceolate, (1.5–) 2–3.5 × 1.5–2 mm;</text>
      <biological_entity id="o3852" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3853" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3854" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3855" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="narrowly lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s8" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla dark purple, rarely yellow, 4–7 mm, glabrate, beak dark purple, hooked, abaxial lobes spreading, 0.5–1 mm, throat not abruptly indented, not forming a fold under abaxial corolla lip, adaxial lobes projecting;</text>
      <biological_entity id="o3856" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3857" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o3858" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="shape" src="d0_s9" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3859" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3860" name="throat" name_original="throat" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not abruptly" name="shape" src="d0_s9" value="indented" value_original="indented" />
      </biological_entity>
      <biological_entity id="o3861" name="fold" name_original="fold" src="d0_s9" type="structure" />
      <biological_entity constraint="corolla abaxial" id="o3862" name="lip" name_original="lip" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o3863" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="projecting" value_original="projecting" />
      </biological_entity>
      <relation from="o3860" id="r353" name="forming a" negation="true" src="d0_s9" to="o3861" />
      <relation from="o3860" id="r354" name="under" negation="true" src="d0_s9" to="o3862" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included, pollen-sac yellow, 0.7–0.8 mm, glabrous, dehiscing longitudinally;</text>
      <biological_entity id="o3864" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3865" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
      <biological_entity id="o3866" name="pollen-sac" name_original="pollen-sac" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="distance" src="d0_s10" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 2.5–5 mm, glabrous;</text>
      <biological_entity id="o3867" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3868" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigma filiform or subcapitate.</text>
      <biological_entity id="o3869" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3870" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subcapitate" value_original="subcapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 3.5–5 × 2.5–3.5 mm, glabrous.</text>
      <biological_entity id="o3871" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 10–30, ovoid to ellipsoid, 0.7–1.2 mm. 2n = 22.</text>
      <biological_entity id="o3872" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="30" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3873" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, lawns, pastures, roadsides, edges of vernal pools, woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="edges" constraint="of vernal pools" />
        <character name="habitat" value="vernal pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.; introduced in Asia (China).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="in Asia (China)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>