<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 16:46:57</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">336</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">SCROPHULARIACEAE</taxon_name>
    <taxon_name authority="Solander ex G. Forster" date="1786" rank="genus">MYOPORUM</taxon_name>
    <taxon_name authority="G. Forster" date="1786" rank="species">laetum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Ins. Austr.,</publication_title>
      <place_in_publication>44. 1786</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family scrophulariaceae;genus myoporum;species laetum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Ngaio tree</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, broadly spreading, 30–100 dm.</text>
      <biological_entity id="o1115" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="broadly" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" modifier="broadly" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to prostrate, much branched;</text>
      <biological_entity id="o1117" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="prostrate" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twig tips and young leaves bronze green, sticky.</text>
      <biological_entity constraint="twig" id="o1118" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bronze green" value_original="bronze green" />
        <character is_modifier="false" name="texture" src="d0_s2" value="sticky" value_original="sticky" />
      </biological_entity>
      <biological_entity constraint="twig" id="o1119" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="young" value_original="young" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="bronze green" value_original="bronze green" />
        <character is_modifier="false" name="texture" src="d0_s2" value="sticky" value_original="sticky" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade bright green, lanceolate, 5–12.5 × 1.5–3 cm, margins finely serrate distal to middle, embedded glands conspicuous.</text>
      <biological_entity id="o1120" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1121" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1122" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely" name="position" src="d0_s3" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="distal" name="position" src="d0_s3" to="middle" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o1123" name="gland" name_original="glands" src="d0_s3" type="structure" />
      <relation from="o1122" id="r92" name="embedded" negation="false" src="d0_s3" to="o1123" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers 2–4 per axil;</text>
      <biological_entity id="o1124" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per axil" constraintid="o1125" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o1125" name="axil" name_original="axil" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>corolla white with purple spots on lobes and distal tube, tube 3.5–4.5 mm, lobes equal, 4–5.5 mm, densely long-hairy adaxially;</text>
      <biological_entity id="o1126" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character constraint="on lobes" constraintid="o1127" is_modifier="false" name="coloration" src="d0_s5" value="purple spots" value_original="purple spots" />
      </biological_entity>
      <biological_entity id="o1127" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o1128" name="tube" name_original="tube" src="d0_s5" type="structure" />
      <biological_entity id="o1129" name="tube" name_original="tube" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1130" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="densely; adaxially" name="pubescence" src="d0_s5" value="long-hairy" value_original="long-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers well exserted from tube;</text>
      <biological_entity id="o1131" name="anther" name_original="anthers" src="d0_s6" type="structure" />
      <biological_entity id="o1132" name="tube" name_original="tube" src="d0_s6" type="structure" />
      <relation from="o1131" id="r93" modifier="well" name="exserted from" negation="false" src="d0_s6" to="o1132" />
    </statement>
    <statement id="d0_s7">
      <text>ovary smooth.</text>
      <biological_entity id="o1133" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules pale to dark reddish purple, ovoid, 5–10 mm.</text>
      <biological_entity id="o1134" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="dark reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds oblong, 3–3.5 mm. 2n = 108 (New Zealand).</text>
      <biological_entity id="o1135" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1136" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="108" value_original="108" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Myoporum laetum is commonly cultivated in coastal areas of California. Although first collected outside of cultivation in 1949, it was not recognized as an introduced element of local and regional floras until the 1970s. It has naturalized mostly in southern California to San Luis Obispo County with some populations north along the coast to the San Francisco Bay area.</discussion>
  <discussion>Myoporum insulare R. Brown, also cultivated in California, is similar to M. laetum, and some reports of M. laetum are possibly M. insulare. Myoporum insulare has leaves that are lighter green when young, and the translucent glands of the mature leaves are less conspicuous. The flowers are slightly smaller with anthers that are only slightly exserted from the tubes, and the fruits are smaller and globular.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jan–)Mar–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas in grasslands, scrub, riparian habitats, generally coastal.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" constraint="in grasslands , scrub ," />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="habitats" modifier="riparian" />
        <character name="habitat" value="coastal" modifier="generally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Pacific Islands (New Zealand); introduced also in s South America (Argentina, Uruguay).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value=" also in s South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value=" also in s South America (Uruguay)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>