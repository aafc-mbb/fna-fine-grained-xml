<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="mention_page">273</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="treatment_page">277</other_info_on_meta>
    <other_info_on_meta type="illustration_page">276</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subfamily">racomitrioideae</taxon_name>
    <taxon_name authority="Roivainen" date="1972" rank="genus">bucklandiella</taxon_name>
    <taxon_name authority="(Kindberg) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="section">laevifoliae</taxon_name>
    <taxon_name authority="(Ireland &amp; J. R. Spence) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="species">pacifica</taxon_name>
    <place_of_publication>
      <publication_title>in R. Ochyra et al., Cens. Cat. Polish Mosses,</publication_title>
      <place_in_publication>147. 2003,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily racomitrioideae;genus bucklandiella;section laevifoliae;species pacifica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075424</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Racomitrium</taxon_name>
    <taxon_name authority="Ireland &amp; J. R. Spence" date="unknown" rank="species">pacificum</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>65: 859, figs. 1–10. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Racomitrium;species pacificum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to rather large, coarse and rigid, loosely or densely caespitose, somewhat lustrous, dark green, olivaceous to yellow or brownish green in the distal part, brown proximally.</text>
      <biological_entity id="o405" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized to rather" value_original="medium-sized to rather" />
        <character is_modifier="false" modifier="rather" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="relief" src="d0_s0" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="texture" src="d0_s0" value="rigid" value_original="rigid" />
        <character is_modifier="false" modifier="loosely; densely" name="growth_form" src="d0_s0" value="caespitose" value_original="caespitose" />
        <character is_modifier="false" modifier="somewhat" name="reflectance" src="d0_s0" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark green" value_original="dark green" />
        <character char_type="range_value" constraint="in distal part" constraintid="o406" from="olivaceous" name="coloration" src="d0_s0" to="yellow or brownish green" />
        <character is_modifier="false" modifier="proximally" name="coloration" notes="" src="d0_s0" value="brown" value_original="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o406" name="part" name_original="part" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems (1–) 2–5 (–7) cm, creeping, curved-ascending to erect, irregularly branched or almost simple.</text>
      <biological_entity id="o407" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="curved-ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="almost; almost" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-appressed when dry, erect-patent when moist, straight to curved, ovatelanceolate, carinate distally, broadly canaliculate proximally, 2.2–3 × (0.5–) 0.7–1 mm;</text>
      <biological_entity id="o408" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-patent" value_original="erect-patent" />
        <character char_type="range_value" from="straight" name="course" src="d0_s2" to="curved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="carinate" value_original="carinate" />
        <character is_modifier="false" modifier="broadly; proximally" name="shape" src="d0_s2" value="canaliculate" value_original="canaliculate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_width" src="d0_s2" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margin broadly recurved to 2/3–3/4 of the way up the leaf on one side, more narrowly recurved to mid leaf on the other side, 1-stratose, entire;</text>
      <biological_entity id="o409" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character char_type="range_value" constraint="of way" constraintid="o410" from="2/3" name="quantity" src="d0_s3" to="3/4" />
        <character is_modifier="false" modifier="narrowly" name="orientation" notes="" src="d0_s3" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o410" name="way" name_original="way" src="d0_s3" type="structure" />
      <biological_entity id="o411" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o412" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o413" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o414" name="side" name_original="side" src="d0_s3" type="structure" />
      <relation from="o410" id="r99" name="up" negation="false" src="d0_s3" to="o411" />
      <relation from="o411" id="r100" name="on" negation="false" src="d0_s3" to="o412" />
      <relation from="o413" id="r101" name="on" negation="false" src="d0_s3" to="o414" />
    </statement>
    <statement id="d0_s4">
      <text>apices acute to narrowly obtuse, muticous, usually with minute teeth owing to protruding cell ends;</text>
      <biological_entity id="o415" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="narrowly obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="muticous" value_original="muticous" />
      </biological_entity>
      <biological_entity id="o416" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity constraint="cell" id="o417" name="end" name_original="ends" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="owing-to-protruding" value_original="owing-to-protruding" />
      </biological_entity>
      <relation from="o415" id="r102" modifier="usually" name="with" negation="false" src="d0_s4" to="o416" />
    </statement>
    <statement id="d0_s5">
      <text>costa subpercurrent to percurrent, in transverse-section strongly convex abaxially, (70–) 80–120 (–160) µm wide near the base, 50–65 µm distally, 2-stratose in the distal and median parts, with 2–4 enlarged adaxial cells, reniform, 2–3 (–4) -stratose, with 3–6 (–7) adaxial cells proximally;</text>
      <biological_entity id="o418" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character constraint="in distal median parts" constraintid="o421" is_modifier="false" name="architecture" src="d0_s5" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-3(-4)-stratose" value_original="2-3(-4)-stratose" />
      </biological_entity>
      <biological_entity id="o419" name="transverse-section" name_original="transverse-section" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly; abaxially" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character char_type="range_value" from="70" from_unit="um" name="width" notes="alterIDs:o419" src="d0_s5" to="80" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="um" name="width" notes="alterIDs:o419" src="d0_s5" to="160" to_unit="um" />
        <character char_type="range_value" constraint="near base" constraintid="o420" from="80" from_unit="um" name="width" notes="alterIDs:o419" src="d0_s5" to="120" to_unit="um" />
      </biological_entity>
      <biological_entity id="o420" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="50" from_unit="um" modifier="distally" name="some_measurement" notes="" src="d0_s5" to="65" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal and median" id="o421" name="part" name_original="parts" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o422" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="true" name="size" src="d0_s5" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="reniform" value_original="reniform" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o423" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o418" id="r103" name="in" negation="false" src="d0_s5" to="o419" />
      <relation from="o418" id="r104" name="with" negation="false" src="d0_s5" to="o422" />
      <relation from="o418" id="r105" name="with" negation="false" src="d0_s5" to="o423" />
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 1-stratose throughout, smooth to pseudopapillose;</text>
      <biological_entity constraint="laminal" id="o424" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s6" to="pseudopapillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal laminal cells elongate, 35–65 × 7–9 µm, sinuose-nodulose, moderately thick-walled;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells usually differentiated in 3–5 cell rows, thick-walled, porose, yellowish-brown to orange, enlarged, quadrate to short-rectangular;</text>
      <biological_entity constraint="basal laminal" id="o425" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="35" from_unit="um" name="length" src="d0_s7" to="65" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s7" to="9" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sinuose-nodulose" value_original="sinuose-nodulose" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s7" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity constraint="cell" id="o427" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>supra-alar cells scarcely differentiated or undifferentiated, not forming a marginal border;</text>
      <biological_entity id="o426" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="in cell rows" constraintid="o427" is_modifier="false" modifier="usually" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="porose" value_original="porose" />
        <character char_type="range_value" from="yellowish-brown" name="coloration" src="d0_s8" to="orange" />
        <character is_modifier="false" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s8" to="short-rectangular" />
      </biological_entity>
      <biological_entity id="o428" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="scarcely" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o429" name="border" name_original="border" src="d0_s9" type="structure" />
      <relation from="o428" id="r106" name="forming a" negation="true" src="d0_s9" to="o429" />
    </statement>
    <statement id="d0_s10">
      <text>medial laminal cells rectangular, 25–40 × 7–9 µm; distal laminal cells isodiametric, quadrate, oblate or roundedquadrate to short-rectangular, 7–15 × 7–9 mm.</text>
      <biological_entity constraint="medial laminal" id="o430" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s10" to="40" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s10" to="9" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal laminal" id="o431" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="roundedquadrate" name="shape" src="d0_s10" to="short-rectangular" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inner perichaetial leaves strongly differentiated, ovate, hyaline.</text>
      <biological_entity constraint="inner perichaetial" id="o432" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="strongly" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta brownish, 5–8.5 mm.</text>
      <biological_entity id="o433" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule brown, obloid to long-cylindric, 2–3.7 × 0.45–0.7 mm;</text>
      <biological_entity id="o434" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character char_type="range_value" from="obloid" name="shape" src="d0_s13" to="long-cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s13" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.45" from_unit="mm" name="width" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum straight, 1–1.5 mm;</text>
      <biological_entity id="o435" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome teeth 300–600 µm, deeply 2-fid, papillose, erect to widely flaring when dry, arising from a fairly short, 40–50 µm, basal membrane.</text>
      <biological_entity constraint="peristome" id="o436" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character char_type="range_value" from="300" from_unit="um" name="some_measurement" src="d0_s15" to="600" to_unit="um" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s15" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s15" value="flaring" value_original="flaring" />
        <character constraint="from a fairly short" is_modifier="false" name="orientation" src="d0_s15" value="arising" value_original="arising" />
        <character char_type="range_value" from="40" from_unit="um" name="some_measurement" src="d0_s15" to="50" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="basal" id="o437" name="membrane" name_original="membrane" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 14–21 µm.</text>
      <biological_entity id="o438" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s16" to="21" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or seasonally submerged to occasionally irrigated, usually exposed boulders, cliffs, less often on perennially wet outcrops and moist, diffusely lit boulders and rock ledges, as well as on soil over rocks and sometimes on sandy soil, predominantly on acidic, less often on basic but not strongly calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry or seasonally submerged to occasionally irrigated" />
        <character name="habitat" value="boulders" modifier="usually exposed" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="wet outcrops" modifier="less often on perennially" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="diffusely" />
        <character name="habitat" value="boulders" modifier="lit" />
        <character name="habitat" value="rock ledges" />
        <character name="habitat" value="soil" modifier="as well as on" constraint="over rocks" />
        <character name="habitat" value="rocks" />
        <character name="habitat" value="sandy soil" modifier="and sometimes on" />
        <character name="habitat" value="predominantly on acidic" />
        <character name="habitat" value="basic but calcareous substrates" modifier="less often on not strongly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1100 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Bucklandiella pacifica occurs at low elevations from southern Vancouver Island southwards to central California. It is the only species of the genus in North America that has consistently muticous leaves lacking a hyaline hair-point. In addition, the species is readily distinguished by the entirely unistratose laminal cells, including leaf margins, narrowly obtuse to acute leaf apex, absence of a basal marginal border, differentiated alar cells sometimes forming an auriculate group, and long-cylindrical capsules with prominently flaring peristome teeth. Bucklandiella pacifica has often been mistaken for Codriophorus depressus, a narrow endemic of California, Nevada, and Oregon (for differences see under 5. Codriophorus depressus), and it is likely to be mistaken for C. ryszardii and muticous ecads of C. varius. However, those species are immediately distinct by having distinctly papillose laminal cells with large, flat papillae. The former is additionally easily distinct in having erose-dentate and cristate leaf apices, longer leaves (3.2–4 mm), and stronger costa that is 4-stratose with 5–8 large adaxial cells in the basal part, whereas the latter has a distinct 1–2-seriate basal marginal border and distinctly plicate leaf base.</discussion>
  
</bio:treatment>