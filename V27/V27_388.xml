<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="treatment_page">283</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subfamily">racomitrioideae</taxon_name>
    <taxon_name authority="Roivainen" date="1972" rank="genus">bucklandiella</taxon_name>
    <taxon_name authority="(Bednarek-Ochyra) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="section">sudeticae</taxon_name>
    <taxon_name authority="(Kindberg in Macoun) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="species">macounii</taxon_name>
    <taxon_name authority="(E. Lawton) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subspecies">alpina</taxon_name>
    <place_of_publication>
      <publication_title>in R. Ochyra et al., Cens. Cat. Polish Mosses,</publication_title>
      <place_in_publication>146. 2003,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily racomitrioideae;genus bucklandiella;section sudeticae;species macounii;subspecies alpina</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075602</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Racomitrium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sudeticum</taxon_name>
    <place_of_publication>
      <publication_title>Moss Fl. Pacif. N.W.,</publication_title>
      <place_in_publication>147, plate 77, figs. 5–8. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Racomitrium;species sudeticum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants less robust, more reddish-brown or reddish green, lustrous.</text>
      <biological_entity id="o3069" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="less" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="lustrous" value_original="lustrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves straight, erect-imbricate, not contorted, (1.5–) 2.4–3 (–3.2) × (0.3–) 0.5–0.7 mm;</text>
      <biological_entity id="o3070" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="erect-imbricate" value_original="erect-imbricate" />
        <character is_modifier="false" modifier="not" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s1" to="2.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s1" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>awns often reddish or yellowish hyaline, to 0.2 mm, moderately spinulose, recurved when dry;</text>
      <biological_entity id="o3071" name="awn" name_original="awns" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish hyaline" value_original="yellowish hyaline" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="0.2" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="architecture_or_shape" src="d0_s2" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa 60–85 µm wide near the base, 40–65 µm wide distally;</text>
      <biological_entity id="o3072" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="near base" constraintid="o3073" from="60" from_unit="um" name="width" src="d0_s3" to="85" to_unit="um" />
      </biological_entity>
      <biological_entity id="o3073" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="40" from_unit="um" modifier="distally" name="width" notes="" src="d0_s3" to="65" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal border consisting of about 20 short, transparent, not or moderately sinuose cells;</text>
      <biological_entity constraint="basal marginal" id="o3074" name="border" name_original="border" src="d0_s4" type="structure" />
      <biological_entity id="o3075" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="20" value_original="20" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="transparent" value_original="transparent" />
        <character is_modifier="true" modifier="not; moderately" name="architecture" src="d0_s4" value="sinuose" value_original="sinuose" />
      </biological_entity>
      <relation from="o3074" id="r698" name="consisting of about" negation="false" src="d0_s4" to="o3075" />
    </statement>
    <statement id="d0_s5">
      <text>medial and distal laminal cells quadrate to short-rectangular, 7–23 × 8–9 µm, smooth or moderately pseudopapillose.</text>
      <biological_entity constraint="medial and distal laminal" id="o3076" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s5" to="short-rectangular" />
        <character char_type="range_value" from="7" from_unit="um" name="length" src="d0_s5" to="23" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s5" to="9" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="moderately" name="relief" src="d0_s5" value="pseudopapillose" value_original="pseudopapillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule ovoid, 1–1.75 × 0.5–0.7 mm;</text>
      <biological_entity id="o3077" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="1.75" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peristome teeth to 380–450 µm.</text>
      <biological_entity constraint="peristome" id="o3078" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="380" from_unit="um" name="some_measurement" src="d0_s7" to="450" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hydrophilous, acidic or slightly calciferous rocks and stones along mountain rivers and streams, sloping rocks with trickling water and stony ground in late snow patches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hydrophilous" />
        <character name="habitat" value="acidic" constraint="along mountain rivers and streams" />
        <character name="habitat" value="calciferous rocks" modifier="slightly" constraint="along mountain rivers and streams" />
        <character name="habitat" value="stones" constraint="along mountain rivers and streams" />
        <character name="habitat" value="mountain rivers" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="rocks" constraint="with trickling water and stony ground in late snow patches" />
        <character name="habitat" value="water" constraint="in late snow patches" />
        <character name="habitat" value="stony ground" constraint="in late snow patches" />
        <character name="habitat" value="late snow patches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (700-2300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="700" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C.; Alaska, Colo., Mont., Oreg., Wash.; Europe; e Asia (Japan); Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10b.</number>
  <discussion>Subspecies alpina has a distribution in the Pacific Northwest similar to that of the type subspecies, although its geographical range extends to the Aleutian Islands. This subspecies shares most structural characters of subsp. macounii but it is generally a less robust plant, with narrower costae, longer awns, distal laminal cells, and peristome teeth, and shorter capsules. It differs largely in the overall appearance of the plants, which are usually distinctly glistening and more reddish, and the leaves are not crispate.</discussion>
  
</bio:treatment>