<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="illustration_page">166</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">timmiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">timmia</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">austriaca</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>176, plate 42, figs. 1–7. 1801,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family timmiaceae;genus timmia;species austriaca</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001586</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Timmia</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">arctica</taxon_name>
    <taxon_hierarchy>genus Timmia;species arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Timmia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">austriaca</taxon_name>
    <taxon_name authority="(Kindberg) Arnell" date="unknown" rank="variety">arctica</taxon_name>
    <taxon_hierarchy>genus Timmia;species austriaca;variety arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Timmia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">austriaca</taxon_name>
    <taxon_name authority="Renauld &amp; Cardot" date="unknown" rank="variety">brevifolia</taxon_name>
    <taxon_hierarchy>genus Timmia;species austriaca;variety brevifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Timmia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">austriaca</taxon_name>
    <taxon_name authority="Hesselbo" date="unknown" rank="variety">papillosa</taxon_name>
    <taxon_hierarchy>genus Timmia;species austriaca;variety papillosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants without deciduous distal leaves.</text>
      <biological_entity id="o214" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o215" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <relation from="o214" id="r74" name="without" negation="false" src="d0_s0" to="o215" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf apex acute (but with terminal 4–5 cells often truncated);</text>
      <biological_entity constraint="leaf" id="o216" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa subpercurrent;</text>
      <biological_entity id="o217" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath orange but appearing red when still on stem;</text>
      <biological_entity id="o218" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="orange" value_original="orange" />
      </biological_entity>
      <biological_entity id="o219" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="red" value_original="red" />
      </biological_entity>
      <relation from="o218" id="r75" name="appearing" negation="false" src="d0_s3" to="o219" />
    </statement>
    <statement id="d0_s4">
      <text>limbs green, pellucid;</text>
      <biological_entity id="o220" name="limb" name_original="limbs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pellucid" value_original="pellucid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>limb-sheath transition abrupt, sharply angled;</text>
      <biological_entity id="o221" name="limb-sheath" name_original="limb-sheath" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="transition" value_original="transition" />
        <character is_modifier="false" name="shape" src="d0_s5" value="abrupt" value_original="abrupt" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s5" value="angled" value_original="angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>limb margins strongly dentate in distal 1/3–1/2, less dentate to entire proximally, occasionally crenulate or entire throughout;</text>
      <biological_entity constraint="limb" id="o222" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character constraint="in distal 1/3-1/2 , less dentate dentate to entire proximally , occasionally crenulate or entire throughout" is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>limb lamina cells 8–14 (–16) × 8–12 (–13) µm, with low conical or rounded mamillae on the adaxial surface, abaxial surface smooth;</text>
      <biological_entity constraint="lamina" id="o223" name="bud" name_original="cells" src="d0_s7" type="structure" constraint_original="limb lamina">
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s7" to="16" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="length" src="d0_s7" to="14" to_unit="um" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s7" to="13" to_unit="um" />
        <character char_type="range_value" constraint="with mamillae" constraintid="o224" from="8" from_unit="um" name="width" src="d0_s7" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity id="o224" name="mamilla" name_original="mamillae" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s7" value="conical" value_original="conical" />
        <character is_modifier="true" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o225" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o226" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o224" id="r76" name="on" negation="false" src="d0_s7" to="o225" />
    </statement>
    <statement id="d0_s8">
      <text>distal sheath lamina cells smooth or with 1–6 (–8) large round, often verrucose papillae over the abaxial surfaces of lumen;</text>
      <biological_entity constraint="lamina" id="o227" name="bud" name_original="cells" src="d0_s8" type="structure" constraint_original="sheath lamina">
        <character is_modifier="true" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="with 1-6(-8) large round , often verrucose papillae" />
      </biological_entity>
      <biological_entity id="o228" name="papilla" name_original="papillae" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="8" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="true" name="size" src="d0_s8" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="true" modifier="often" name="relief" src="d0_s8" value="verrucose" value_original="verrucose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o229" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o230" name="lumen" name_original="lumen" src="d0_s8" type="structure" />
      <relation from="o227" id="r77" name="with" negation="false" src="d0_s8" to="o228" />
      <relation from="o228" id="r78" name="over" negation="false" src="d0_s8" to="o229" />
      <relation from="o229" id="r79" name="part_of" negation="false" src="d0_s8" to="o230" />
    </statement>
    <statement id="d0_s9">
      <text>cells at leaf insertion not hyaline and fragile;</text>
      <biological_entity id="o231" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s9" value="fragile" value_original="fragile" />
      </biological_entity>
      <biological_entity id="o232" name="leaf" name_original="leaf" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="insertion" src="d0_s9" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o231" id="r80" name="at" negation="false" src="d0_s9" to="o232" />
    </statement>
    <statement id="d0_s10">
      <text>abaxial surface of costa limb smooth, toothed or papillose near apex, adaxial cells with conical or rounded mamillae.</text>
      <biological_entity constraint="limb" id="o233" name="surface" name_original="surface" src="d0_s10" type="structure" constraint_original="limb abaxial; limb">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s10" value="toothed" value_original="toothed" />
        <character constraint="near apex" constraintid="o235" is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="costa" id="o234" name="limb" name_original="limb" src="d0_s10" type="structure" />
      <biological_entity id="o235" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o237" name="mamilla" name_original="mamillae" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="conical" value_original="conical" />
        <character is_modifier="true" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o233" id="r81" name="part_of" negation="false" src="d0_s10" to="o234" />
      <relation from="o236" id="r82" name="with" negation="false" src="d0_s10" to="o237" />
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="adaxial" id="o236" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perichaetial leaves more strongly dentate than vegetative leaves, sheath up to half the leaf length.</text>
      <biological_entity constraint="perichaetial" id="o238" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character constraint="than vegetative leaves" constraintid="o239" is_modifier="false" name="architecture_or_shape" src="d0_s12" value="more strongly dentate" value_original="more strongly dentate" />
      </biological_entity>
      <biological_entity id="o239" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o240" name="sheath" name_original="sheath" src="d0_s12" type="structure" />
      <biological_entity id="o241" name="leaf" name_original="leaf" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Calyptra without longitudinal split, not persistent on seta.</text>
      <biological_entity id="o242" name="calyptra" name_original="calyptra" src="d0_s13" type="structure">
        <character constraint="on seta" constraintid="o244" is_modifier="false" modifier="not" name="duration" notes="" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o243" name="split" name_original="split" src="d0_s13" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s13" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o244" name="seta" name_original="seta" src="d0_s13" type="structure" />
      <relation from="o242" id="r83" name="without" negation="false" src="d0_s13" to="o243" />
    </statement>
    <statement id="d0_s14">
      <text>Capsule spirally plicate (even when young and operculate);</text>
      <biological_entity id="o245" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="spirally" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s14" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exothecial cell-walls sinuose;</text>
      <biological_entity constraint="exothecial" id="o246" name="cell-wall" name_original="cell-walls" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="sinuose" value_original="sinuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stomata mostly on the neck;</text>
      <biological_entity id="o247" name="stoma" name_original="stomata" src="d0_s16" type="structure" />
      <biological_entity id="o248" name="neck" name_original="neck" src="d0_s16" type="structure" />
      <relation from="o247" id="r84" name="on" negation="false" src="d0_s16" to="o248" />
    </statement>
    <statement id="d0_s17">
      <text>endostome cilia without appendiculations on the interior surfaces.</text>
      <biological_entity constraint="endostome" id="o249" name="cilium" name_original="cilia" src="d0_s17" type="structure" />
      <biological_entity id="o250" name="appendiculation" name_original="appendiculations" src="d0_s17" type="structure" />
      <biological_entity constraint="interior" id="o251" name="surface" name_original="surfaces" src="d0_s17" type="structure" />
      <relation from="o249" id="r85" name="without" negation="false" src="d0_s17" to="o250" />
      <relation from="o250" id="r86" name="on" negation="false" src="d0_s17" to="o251" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporophytes sporadic throughout the range.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>but more frequent in non-Arctic regions, various habitats including dry exposed ridges, wet river edges or forested valleys</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="non-arctic regions" modifier="but more frequent in" />
        <character name="habitat" value="various habitats" />
        <character name="habitat" value="dry exposed ridges" />
        <character name="habitat" value="wet river edges" />
        <character name="habitat" value="forested valleys" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.B., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Que., Sask., Yukon; Alaska, Calif., Colo., Idaho, Mich., Mont., Nev., N.Mex., Oreg., S.Dak., Wash., Wyo.; Eurasia (Alps, Greece, Italy, Pyrenees, Scandinavia, United Kingdom); Atlantic Islands (Iceland); e Asia (Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia (Alps)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Greece)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Italy)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Pyrenees)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Scandinavia)" establishment_means="native" />
        <character name="distribution" value="Eurasia (United Kingdom)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
        <character name="distribution" value="e Asia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>A polymorphic yet remarkably well-defined species, Timmia austriaca has the most well-delimited sheath of all Timmia taxa, with an abrupt change in color and sharp angle at the limb-sheath transition, and it is the only taxon where the costa widens slightly just at the limb-sheath transition. Its distinct habit and leaf angularity, dioicous sexuality, non-appendiculate endostome cilia, and orange sheaths separate it from T. megapolitana, while its habit, leaf angularity, and non-deciduous leaves will separate it from T. norvegica and T. sibirica. The perigonial leaves are short and broad, consisting mostly of sheath.</discussion>
  
</bio:treatment>