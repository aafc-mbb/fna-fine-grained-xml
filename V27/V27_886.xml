<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">592</other_info_on_meta>
    <other_info_on_meta type="treatment_page">612</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Juratzka" date="1882" rank="genus">crossidium</taxon_name>
    <taxon_name authority="Holzinger &amp; E. B. Bartram" date="1924" rank="species">aberrans</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>27: 4, plate 2. 1924,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus crossidium;species aberrans</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075548</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crossidium</taxon_name>
    <taxon_name authority="Holzinger &amp; E. B. Bartram" date="unknown" rank="species">spatulifolium</taxon_name>
    <taxon_hierarchy>genus Crossidium;species spatulifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–3.5 mm.</text>
      <biological_entity id="o9062" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="3.5" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves lingulate, lanceolate or ovate, 0.7–1.4 mm, margins recurved from near apex to near base, apex rounded or emarginate, piliferous;</text>
      <biological_entity id="o9063" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lingulate" value_original="lingulate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s1" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9064" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character constraint="from apex" constraintid="o9065" is_modifier="false" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o9065" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o9066" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o9067" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s1" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="piliferous" value_original="piliferous" />
      </biological_entity>
      <relation from="o9065" id="r2150" name="to" negation="false" src="d0_s1" to="o9066" />
    </statement>
    <statement id="d0_s2">
      <text>costa excurrent, with an abaxial epidermis, filaments of 1–4 cells, cells subspheric, with several papillae per cell, terminal cell subspheric;</text>
      <biological_entity id="o9068" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9069" name="epidermis" name_original="epidermis" src="d0_s2" type="structure" />
      <biological_entity id="o9070" name="filament" name_original="filaments" src="d0_s2" type="structure" />
      <biological_entity id="o9071" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o9072" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subspheric" value_original="subspheric" />
      </biological_entity>
      <biological_entity id="o9073" name="papilla" name_original="papillae" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o9074" name="cell" name_original="cell" src="d0_s2" type="structure" />
      <biological_entity constraint="terminal" id="o9075" name="cell" name_original="cell" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subspheric" value_original="subspheric" />
      </biological_entity>
      <relation from="o9068" id="r2151" name="with" negation="false" src="d0_s2" to="o9069" />
      <relation from="o9070" id="r2152" name="consist_of" negation="false" src="d0_s2" to="o9071" />
      <relation from="o9072" id="r2153" name="with" negation="false" src="d0_s2" to="o9073" />
      <relation from="o9073" id="r2154" name="per" negation="false" src="d0_s2" to="o9074" />
    </statement>
    <statement id="d0_s3">
      <text>cells of leaf base 13–51 µm, medial and distal cells 11–20 µm, smooth, with 1–2 abaxial papillae or on either side in distal cells.</text>
      <biological_entity id="o9076" name="cell" name_original="cells" src="d0_s3" type="structure" constraint="base" constraint_original="base; base">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s3" to="51" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o9077" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o9079" name="side" name_original="side" src="d0_s3" type="structure" />
      <relation from="o9076" id="r2155" name="part_of" negation="false" src="d0_s3" to="o9077" />
      <relation from="o9078" id="r2156" name="with 1-2 abaxial papillae or on" negation="false" src="d0_s3" to="o9079" />
    </statement>
    <statement id="d0_s4">
      <text>Sexual condition dioicous or cladautoicous.</text>
      <biological_entity constraint="medial and distal" id="o9078" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s3" to="20" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="cladautoicous" value_original="cladautoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta 8–16 mm.</text>
      <biological_entity id="o9080" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule urn ovoid-cylindric, 1.3–2 mm;</text>
      <biological_entity constraint="capsule" id="o9081" name="urn" name_original="urn" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>operculum 0.5–1 mm;</text>
      <biological_entity id="o9082" name="operculum" name_original="operculum" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome strongly twisted, 650–980 µm. Spores spheric, ovoid or oblong, finely papillose, 9–13 µm.</text>
      <biological_entity id="o9083" name="peristome" name_original="peristome" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="650" from_unit="um" name="some_measurement" src="d0_s8" to="980" to_unit="um" />
      </biological_entity>
      <biological_entity id="o9084" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s8" to="13" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsule mature Jan–Jun(-Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Jun" from="Jan" />
        <character name="capsules maturing time" char_type="atypical_range" to="Aug" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil and rocks under shrubs, shaded banks or in open sites in dry washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" constraint="under shrubs , shaded banks" />
        <character name="habitat" value="rocks" constraint="under shrubs , shaded banks" />
        <character name="habitat" value="shrubs" />
        <character name="habitat" value="shaded banks" />
        <character name="habitat" value="open sites" modifier="or in" constraint="in dry washes" />
        <character name="habitat" value="dry washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (600-1500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="600" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., Tex., Utah; Mexico (Baja California, Sonora); n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Crossidium aberrans is easily distinguished by the smooth or 1-papillose cells of the distal leaf blade and by the low filaments with subglobose-papillose terminal cells.</discussion>
  
</bio:treatment>