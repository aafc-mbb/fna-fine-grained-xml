<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">251</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="treatment_page">250</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="Limpricht" date="1889" rank="subgenus">rhabdogrimmia</taxon_name>
    <taxon_name authority="Bruch ex Balsamo-Crivelli &amp; De Notaris" date="1838" rank="species">elatior</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Reale Accad. Sci. Torino</publication_title>
      <place_in_publication>40: 340. 1838,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus rhabdogrimmia;species elatior</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001271</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Cardot &amp; Thérot" date="unknown" rank="species">cognata</taxon_name>
    <taxon_hierarchy>genus Grimmia;species cognata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">grandis</taxon_name>
    <taxon_hierarchy>genus Grimmia;species grandis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="(Warnstorf) Kindberg" date="unknown" rank="species">papillosa</taxon_name>
    <taxon_hierarchy>genus Grimmia;species papillosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in robust, dark green to blackish green, loose, hoary, readily disintegrating tufts.</text>
      <biological_entity id="o6894" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6895" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character char_type="range_value" from="dark green" is_modifier="true" name="coloration" src="d0_s0" to="blackish green" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="pubescence" src="d0_s0" value="hoary" value_original="hoary" />
        <character is_modifier="true" modifier="readily" name="dehiscence" src="d0_s0" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <relation from="o6894" id="r1642" name="in" negation="false" src="d0_s0" to="o6895" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5 cm, central strand absent.</text>
      <biological_entity id="o6896" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o6897" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves loosely appressed to slightly twisted when dry, erectopatent when moist, lanceolate to ovatelanceolate, tapering to acute apex, 2–3.0 × 0.5–0.7 mm, keeled, margin broadly recurved on one side, awns short to long and weakly denticulate, costa weak at base, channeled distally, projecting on abaxial side;</text>
      <biological_entity id="o6898" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erectopatent" value_original="erectopatent" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovatelanceolate tapering" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="ovatelanceolate tapering" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" notes="" src="d0_s2" to="3.0" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s2" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o6899" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o6900" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character constraint="on side" constraintid="o6901" is_modifier="false" modifier="broadly" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o6901" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o6902" name="awn" name_original="awns" src="d0_s2" type="structure">
        <character is_modifier="false" name="size_or_length" src="d0_s2" value="short to long" value_original="short to long" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o6903" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character constraint="at base" constraintid="o6904" is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s2" value="channeled" value_original="channeled" />
        <character constraint="on abaxial side" constraintid="o6905" is_modifier="false" name="orientation" src="d0_s2" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o6904" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial" id="o6905" name="side" name_original="side" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells short to long-rectangular, sinuose-nodulose, thick-walled;</text>
      <biological_entity constraint="basal" id="o6906" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o6907" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s3" value="long-rectangular" value_original="long-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sinuose-nodulose" value_original="sinuose-nodulose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells quadrate to short-rectangular, thin to thick-walled;</text>
      <biological_entity constraint="basal marginal laminal" id="o6908" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="short-rectangular" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells quadrate to short-rectangular, sinuose, thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o6909" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s5" to="short-rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 2-stratose with thick, prominent multistratose bands, margins multistratose and thick, areolation very opaque with rounded thick-walled cells, occasionally papillose.</text>
      <biological_entity constraint="distal laminal" id="o6910" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character constraint="with bands" constraintid="o6911" is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o6911" name="band" name_original="bands" src="d0_s6" type="structure">
        <character is_modifier="true" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="multistratose" value_original="multistratose" />
      </biological_entity>
      <biological_entity id="o6912" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="multistratose" value_original="multistratose" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o6913" name="areolation" name_original="areolation" src="d0_s6" type="structure">
        <character constraint="with cells" constraintid="o6914" is_modifier="false" modifier="very" name="coloration" src="d0_s6" value="opaque" value_original="opaque" />
        <character is_modifier="false" modifier="occasionally" name="relief" notes="" src="d0_s6" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o6914" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Gemmae absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o6915" name="gemma" name_original="gemmae" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta arcuate, 2–3 mm.</text>
      <biological_entity id="o6916" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule occasionally present, emergent to exserted, brown, obloid, striate, exothecial cells thin-walled, annulus present, operculum rostrate, peristome teeth purple, deeply split, papillose.</text>
      <biological_entity id="o6917" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="location" src="d0_s10" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obloid" value_original="obloid" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s10" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o6918" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o6919" name="annulus" name_original="annulus" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6920" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rostrate" value_original="rostrate" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o6921" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="relief" notes="" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o6922" name="split" name_original="split" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Calyptra mitrate.</text>
      <biological_entity id="o6923" name="calyptra" name_original="calyptra" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="mitrate" value_original="mitrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed, dry acidic rock and occasionally basic limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry acidic rock" modifier="exposed" />
        <character name="habitat" value="basic limestone" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (500-4500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="4500" to_unit="m" from="500" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Yukon; Colo., Mont., N.J., Oreg., S.Dak., Wyo.; Eurasia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <discussion>Grimmia elatior is fairly common in the Canadian Rockies and in the western United States. It is predominantly bound to the Rocky Mountain area in Colorado, Wyoming, and Montana. In eastern North America it is known only from a site in New Jersey. It can be recognized easily by its robust habit, usually growing in dark green, extended patches on various types of acidic rock, like gneiss, granite, and sandstone. While it is often described as having papillae, most North American specimens do not have them. E. M. Mair (2002) reported that papillae were strongly expressed in harsh conditions but many specimens from the alpine in both the Yukon and Colorado lack papillae. The widespread but uncommon western species G. leibergii is commonly mistaken for G. elatior. However, the former has both leaf margins recurved, its lamina is 1-stratose with only 2-stratose margins, and its basal juxtacostal cells are elongate to linear; in G. elatior there is only one recurved leaf margin, its lamina is 2-stratose with multistratose bands and margins, and its basal juxtacostal cells are only short- to long-rectangular. The length of the awn in G. elatior is quite variable; plants with nearly muticous leaves and plants with very long awns may be found growing close together. There is also some resemblance to G. pilifera, widespread in Asia and eastern North America. The latter, however, has immersed capsules, both margins are recurved, and its lamina does not have multistratose bands.</discussion>
  
</bio:treatment>