<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">211</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="(Bridel) Podpera" date="1911" rank="species">rivulare</taxon_name>
    <place_of_publication>
      <publication_title>Beih. Bot. Centralbl.</publication_title>
      <place_in_publication>28(2): 207.  1911,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species rivulare</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075411</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">rivularis</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Schrader)</publication_title>
      <place_in_publication>1800(1): 276. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species rivularis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">alpicola</taxon_name>
    <taxon_name authority="(Bridel) Wahlenberg" date="unknown" rank="variety">rivularis</taxon_name>
    <taxon_hierarchy>genus Grimmia;species alpicola;variety rivularis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="(Mitten) Persson" date="unknown" rank="species">platyphylla</taxon_name>
    <taxon_hierarchy>genus Grimmia;species platyphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schistidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">platyphyllum</taxon_name>
    <taxon_hierarchy>genus Schistidium;species platyphyllum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schistidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rivulare</taxon_name>
    <taxon_name authority="(J. E. Zetterstedt) Bremer" date="unknown" rank="subspecies">latifolium</taxon_name>
    <taxon_hierarchy>genus Schistidium;species rivulare;subspecies latifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schistidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">submuticum</taxon_name>
    <taxon_name authority="H. H. Blom" date="unknown" rank="subspecies">arcticum</taxon_name>
    <taxon_hierarchy>genus Schistidium;species submuticum;subspecies arcticum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in open to compact, often extensive, tufts or mats, dark green, olivaceous, or brown, often yellow green distally, rarely nearly black.</text>
      <biological_entity id="o7361" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s0" value="yellow green" value_original="yellow green" />
        <character is_modifier="false" modifier="rarely nearly" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7362" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character char_type="range_value" from="open" is_modifier="true" name="architecture" src="d0_s0" to="compact" />
        <character is_modifier="true" modifier="often" name="size" src="d0_s0" value="extensive" value_original="extensive" />
      </biological_entity>
      <biological_entity id="o7363" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o7361" id="r2072" name="in" negation="false" src="d0_s0" to="o7362" />
      <relation from="o7361" id="r2073" name="in" negation="false" src="d0_s0" to="o7363" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.2–10 (–18) cm, central strand distinct.</text>
      <biological_entity id="o7364" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="18" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o7365" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, often curved towards stem when dry and somewhat imbricate, sometimes secund, often slightly contorted when dry, ovatelanceolate to ovate-triangular, keeled or concave proximally, keeled distally, 1.2–3.2 mm, 1-stratose, occasionally with 2-stratose striae or patches distally;</text>
      <biological_entity id="o7366" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="towards stem" constraintid="o7367" is_modifier="false" modifier="often" name="course" src="d0_s2" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o7367" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry somewhat imbricate; when dry somewhat imbricate; sometimes" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="ovate-triangular keeled or concave proximally" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="ovate-triangular keeled or concave proximally" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s2" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o7368" name="stria" name_original="striae" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o7369" name="patch" name_original="patches" src="d0_s2" type="structure" />
      <relation from="o7367" id="r2074" modifier="occasionally" name="with" negation="false" src="d0_s2" to="o7368" />
      <relation from="o7367" id="r2075" modifier="occasionally" name="with" negation="false" src="d0_s2" to="o7369" />
    </statement>
    <statement id="d0_s3">
      <text>margins usually recurved to past mid leaf, sometimes to near apex, rarely plane, denticulate or smooth distally, 1-stratose or 2-stratose, rarely 3-stratose, often in more than one row;</text>
      <biological_entity id="o7370" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s3" value="3-stratose" value_original="3-stratose" />
      </biological_entity>
      <biological_entity constraint="mid" id="o7371" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s3" value="past" value_original="past" />
      </biological_entity>
      <biological_entity id="o7372" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o7374" name="row" name_original="row" src="d0_s3" type="structure" />
      <biological_entity id="o7373" name="row" name_original="row" src="d0_s3" type="structure" />
      <relation from="o7370" id="r2076" modifier="sometimes" name="to" negation="false" src="d0_s3" to="o7372" />
      <relation from="o7373" id="r2077" modifier="often; often; often; often" name="in" negation="false" src="d0_s3" to="o7374" />
    </statement>
    <statement id="d0_s4">
      <text>apices acute or obtuse, sometimes ending in a series of pellucid cells;</text>
      <biological_entity id="o7375" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o7376" name="series" name_original="series" src="d0_s4" type="structure" />
      <biological_entity id="o7377" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <relation from="o7375" id="r2078" name="ending in" negation="false" src="d0_s4" to="o7376" />
      <relation from="o7376" id="r2079" modifier="sometimes" name="consist_of" negation="false" src="d0_s4" to="o7377" />
    </statement>
    <statement id="d0_s5">
      <text>costa subpercurrent or percurrent, sometimes as a short, smooth or weakly denticulate awn, smooth;</text>
      <biological_entity id="o7378" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7379" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="true" modifier="weakly" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <relation from="o7378" id="r2080" modifier="sometimes" name="as" negation="false" src="d0_s5" to="o7379" />
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells mostly quadrate;</text>
      <biological_entity constraint="basal marginal" id="o7380" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells angular, isodiametric or short-rectangular, 7–9 (–11) µm, smooth, slightly sinuose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o7381" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="11" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s7" to="9" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule redbrown or yellowish green, rarely almost black, short-cylindric, cupulate, or campanulate, 0.6–1.1 mm;</text>
      <biological_entity id="o7382" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="rarely almost" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells isodiametric, usually irregularly angular, or elongate, sometimes rounded, unevenly thickened or thick-walled, often trigonous;</text>
      <biological_entity constraint="exothecial" id="o7383" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" modifier="usually irregularly" name="shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="usually irregularly" name="shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="unevenly" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s10" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rim usually darker than capsule wall;</text>
      <biological_entity id="o7384" name="rim" name_original="rim" src="d0_s11" type="structure">
        <character constraint="than capsule wall" constraintid="o7385" is_modifier="false" name="coloration" src="d0_s11" value="usually darker" value_original="usually darker" />
      </biological_entity>
      <biological_entity constraint="capsule" id="o7385" name="wall" name_original="wall" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stomata present;</text>
      <biological_entity id="o7386" name="stoma" name_original="stomata" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome patent, squarrose-recurved, or revolute, 350–570 µm, dark red, densely papillose, usually weakly perforated.</text>
      <biological_entity id="o7387" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="patent" value_original="patent" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="squarrose-recurved" value_original="squarrose-recurved" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s13" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="350" from_unit="um" name="some_measurement" src="d0_s13" to="570" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark red" value_original="dark red" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="usually weakly" name="architecture" src="d0_s13" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 14–24 µm, granulose.</text>
      <biological_entity id="o7388" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s14" to="24" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s14" value="granulose" value_original="granulose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet to dry rocks in or along water courses and lakes, sometimes along seasonally irrigated ledges or cliffs, rarely on concrete or mineral soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet to dry rocks" />
        <character name="habitat" value="water courses" modifier="in or along" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="irrigated ledges" modifier="sometimes along seasonally" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="concrete" modifier="rarely on" />
        <character name="habitat" value="mineral soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-4600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="4600" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.Dak., Tenn., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; South America; Eurasia; Africa; Atlantic Islands; Australia; Antarctic.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="Antarctic" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <discussion>Schistidium rivulare is the most variable and widely distributed species of the genus in North America. A myriad of forms have been recognized at various taxonomic levels, but none more than the smaller and more triangular-leaved form (e.g., S. platyphyllum and S. rivulare subsp. latifolium). H. H. Blom (1998) separated S. platyphyllum mainly using the thickness and shape of the exothecial cells, with S. rivulare having short, thick-walled, irregular cells and S. platyphyllum having more or less regular, thin-walled, and elongate cells. These features, however, at least within North American specimens, vary from place to place and sometimes between capsules in the same collection. Other characters used to separate the taxa, such as the more keeled leaves of S. rivulare and its absence of an awn, also are variable. Differences between S. rivulare and the closely related S. subjulaceum are discussed under the latter species. Schistidium rivulare is sometimes confused with S. agassizii, also found along watercourses, but it is readily separated by its more ovate, distally keeled leaves with recurved margins. Schistidium alpicola Hedwig is a name that has been frequently applied to this complex, but is no longer useful (see Blom for further discussion).</discussion>
  
</bio:treatment>