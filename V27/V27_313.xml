<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="treatment_page">231</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="Schimper" date="1856" rank="subgenus">Grimmia</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">plagiopodia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>78, plate 15,  figs. 6–13. 1801,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus Grimmia;species plagiopodia</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001286</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Austin" date="unknown" rank="species">brandegeei</taxon_name>
    <taxon_hierarchy>genus Grimmia;species brandegeei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense cushions to hoary tufts, dark green to brown.</text>
      <biological_entity id="o6514" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" notes="" src="d0_s0" to="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6515" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o6516" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s0" value="hoary" value_original="hoary" />
      </biological_entity>
      <relation from="o6514" id="r1555" name="in" negation="false" src="d0_s0" to="o6515" />
      <relation from="o6515" id="r1556" name="to" negation="false" src="d0_s0" to="o6516" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.3–0.5 (–1) cm.</text>
      <biological_entity id="o6517" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s1" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves oblong-ovate, 1–1.7 × 0.4–0.8 mm, concave-keeled, awn 0.3–1 mm;</text>
      <biological_entity id="o6518" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s2" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s2" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave-keeled" value_original="concave-keeled" />
      </biological_entity>
      <biological_entity id="o6519" name="awn" name_original="awn" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells quadrate to short-rectangular, straight, thin-walled;</text>
      <biological_entity constraint="basal" id="o6520" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o6521" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s3" to="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells quadrate to short-rectangular, straight, thin-walled;</text>
      <biological_entity constraint="basal marginal laminal" id="o6522" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells quadrate to short-rectangular, slightly sinuose, slightly thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o6523" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s5" to="short-rectangular" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s5" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 1-stratose, marginal cells 1–2-stratose.</text>
      <biological_entity constraint="distal laminal" id="o6524" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition gonioautoicous.</text>
      <biological_entity constraint="marginal" id="o6525" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-2-stratose" value_original="1-2-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta sigmoid, 0.2–0.3 mm.</text>
      <biological_entity id="o6526" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s8" value="sigmoid" value_original="sigmoid" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule usually present, exothecial cells thin-walled, annulus absent, operculum mammillate, peristome present, fully developed, perforated and split in distal half.</text>
      <biological_entity id="o6527" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o6528" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o6529" name="annulus" name_original="annulus" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6530" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="mammillate" value_original="mammillate" />
      </biological_entity>
      <biological_entity id="o6531" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="fully" name="development" src="d0_s9" value="developed" value_original="developed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="perforated" value_original="perforated" />
      </biological_entity>
      <biological_entity id="o6532" name="split" name_original="split" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o6533" name="half" name_original="half" src="d0_s9" type="structure" />
      <relation from="o6532" id="r1557" name="in" negation="false" src="d0_s9" to="o6533" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed calcareous sandstone, limestone, occasionally concrete, and glacio-lacustrine silt</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous sandstone" modifier="exposed" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="concrete" modifier="occasionally" />
        <character name="habitat" value="glacio-lacustrine silt" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (50-2400 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="50" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.W.T., Nunavut, Ont., Sask.; Alaska, Calif., Colo., Idaho, Ill., Iowa, Minn., Mont., Nebr., Nev., N.Mex., N.Dak., S.Dak., Utah, Wis., Wyo.; South America; Eurasia; Pacific Islands (New Zealand); Antarctic.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Antarctic" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Grimmia plagiopodia has a widespread and continuous distribution on calcareous rock across the northern Great Plains, reaching as far east as Illinois. It is rare in eastern North America, with a disjunct site in southern Ontario. In the west it reaches into the mountains on limestone and basic sandstone deposits, but its continuous range does not extend west of a line from Utah to south-central British Columbia. There is a disjunct location near Carson City, Nevada and Lake Tahoe, California. In the Arctic it is known from a few scattered localities extending from northwestern Greenland and nearby Ellesmere Island to the North Slope of Alaska. Compared to G. anodon, G. plagiopodia tends to occupy more prairie-like sites and is typically found at lower elevations. Commonly fertile, it is recognized by its immersed, peristomate capsule on a sigmoid seta with fully-developed teeth that are perforated and split distally. Grimmia americana is similar but has a short, straight to arcuate seta and a large annulus. The other widespread species in the group, G. anodon, has an annulus and is gymnostomous.</discussion>
  
</bio:treatment>