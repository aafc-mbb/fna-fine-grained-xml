<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="treatment_page">546</other_info_on_meta>
    <other_info_on_meta type="illustration_page">546</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">didymodon</taxon_name>
    <taxon_name authority="(X. J. Li) R. H. Zander" date="1993" rank="species">anserinocapitatus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Buffalo Soc. Nat. Sci.</publication_title>
      <place_in_publication>32: 162. 1993,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus didymodon;species anserinocapitatus</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002025</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="X. J. Li" date="unknown" rank="species">anserinocapitata</taxon_name>
    <place_of_publication>
      <publication_title>Acta Bot. Yunnan.</publication_title>
      <place_in_publication>3: 103, plate 2, figs. 1–9. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Barbula;species anserinocapitata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green to reddish green.</text>
      <biological_entity id="o4631" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="reddish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 1.5 cm, central strand present.</text>
      <biological_entity id="o4632" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o4633" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect-appressed when dry, spreading and not keeled when moist, monomorphic, lanceolate, adaxially weakly concave across leaf, 0.7–1.1 mm (absent tip) to 2 mm whole, base scarcely differentiated in shape, margins recurved at mid leaf, entire, apex thickened, long-cylindric to clavate, usually soon deciduous, usually absent in mature leaves;</text>
      <biological_entity id="o4634" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character constraint="across leaf" constraintid="o4635" is_modifier="false" modifier="adaxially weakly" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="1.1" to_unit="mm" unit="0.7-1.1 mm-" />
        <character name="some_measurement" notes="" src="d0_s2" unit="0.7-1.1 mm-" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4635" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity id="o4636" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s2" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o4637" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="at mid leaf" constraintid="o4638" is_modifier="false" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="mid" id="o4638" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity id="o4639" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="long-cylindric" name="shape" src="d0_s2" to="clavate" />
        <character is_modifier="false" modifier="usually soon" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="in leaves" constraintid="o4640" is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4640" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa excurrent, excurrency absent in mature leaves, not much widened or tapering at mid leaf but swollen in excurrency, pad of cells absent, adaxial costal cells quadrate, 4–6 cells wide at mid leaf, guide cells in 2 layers;</text>
      <biological_entity id="o4641" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o4642" name="excurrency" name_original="excurrency" src="d0_s3" type="structure">
        <character constraint="in leaves" constraintid="o4643" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="not much" name="width" notes="" src="d0_s3" value="widened" value_original="widened" />
        <character constraint="at mid leaf" constraintid="o4644" is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o4643" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity constraint="mid" id="o4644" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character constraint="in excurrency" constraintid="o4645" is_modifier="false" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o4645" name="excurrency" name_original="excurrency" src="d0_s3" type="structure" />
      <biological_entity id="o4646" name="pad" name_original="pad" src="d0_s3" type="structure" />
      <biological_entity id="o4647" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial costal" id="o4648" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o4649" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character constraint="at mid leaf" constraintid="o4650" is_modifier="false" name="width" src="d0_s3" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="mid" id="o4650" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity constraint="guide" id="o4651" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity id="o4652" name="layer" name_original="layers" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <relation from="o4646" id="r1099" name="consist_of" negation="false" src="d0_s3" to="o4647" />
      <relation from="o4651" id="r1100" name="in" negation="false" src="d0_s3" to="o4652" />
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells differentiated medially, walls thin;</text>
      <biological_entity constraint="basal laminal" id="o4653" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="medially" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o4654" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells 8–10 µm wide, 1: 1, papillae essentially absent, lumens angular, walls thin, weakly convex on both sides, 1-stratose except in deciduous apex.</text>
      <biological_entity constraint="distal laminal" id="o4655" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s5" to="10" to_unit="um" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4656" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="essentially" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4657" name="lumen" name_original="lumens" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="angular" value_original="angular" />
      </biological_entity>
      <biological_entity id="o4659" name="side" name_original="sides" src="d0_s5" type="structure" />
      <biological_entity id="o4660" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by the deciduous leaf apex.</text>
      <biological_entity id="o4658" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character constraint="on sides" constraintid="o4659" is_modifier="false" modifier="weakly" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character constraint="except-in apex" constraintid="o4660" is_modifier="false" name="architecture" notes="" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o4661" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition and sporophytes unknown.</text>
      <biological_entity id="o4662" name="sporophyte" name_original="sporophytes" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Distal lamina KOH color reaction reddish orange.</text>
      <biological_entity constraint="distal" id="o4663" name="lamina" name_original="lamina" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish orange" value_original="reddish orange" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Red sandstone cliff, near river</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="red sandstone cliff" constraint="near river" />
        <character name="habitat" value="river" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1500-2200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.; Asia (w China, Kazakhstan, Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Asia (w China)" establishment_means="native" />
        <character name="distribution" value="Asia (Kazakhstan)" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>In the flora area, Didymodon anserinocapitatus is known only from Colorado and New Mexico (R. H. Zander and W. A. Weber 1997). The arctic species D. johansenii is similar in the swollen, deciduous apex, but differs mainly by its distal laminal cells 13–15 µm wide, and guide cells in only a single layer. Didymodon rigidulus var. icmadophilus, a widespread montane taxon, is similar in appearance but its leaf apices are never swollen though sometimes fragile. The American material differs from the Asian type (China: Tibet, Nan Xian, Zang Mu 1704, isotype–NY) in being less robust, the leaves reaching only 1.8 mm, and the proximal cells quadrate to short-rectangular (leaves to 2 mm and proximal cells to 4:1 in the Asian specimen).</discussion>
  
</bio:treatment>