<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gary L. Smith Merrill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="P. Beauvois" date="1804" rank="genus">POGONATUM</taxon_name>
    <place_of_publication>
      <publication_title>Mag. Encycl.</publication_title>
      <place_in_publication>5: 329. 1804  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus POGONATUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pogon, beard, alluding to hairy calyptra</other_info_on_name>
    <other_info_on_name type="fna_id">126256</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium to large, in loose pure tufts or growing among other bryophytes, or individual stems small and scattered over a persistent protonemal mat.</text>
      <biological_entity id="o4622" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium" name="size" src="d0_s0" to="large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4623" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o4624" name="bryophyte" name_original="bryophytes" src="d0_s0" type="structure" />
      <biological_entity constraint="individual" id="o4625" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character constraint="over protonemal, mat" constraintid="o4626, o4627" is_modifier="false" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o4626" name="protonemal" name_original="protonemal" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o4627" name="mat" name_original="mat" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o4622" id="r1278" name="in" negation="false" src="d0_s0" to="o4623" />
      <relation from="o4622" id="r1279" name="in" negation="false" src="d0_s0" to="o4624" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or branched by subfloral innovations.</text>
      <biological_entity id="o4628" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="by innovations" constraintid="o4629" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o4629" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="subfloral" value_original="subfloral" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves with a sheathing base merging gradually or ± abruptly contracted to the blade, the sheath entire (toothed in P. contortum), with or without incrassate hinge-cells at the shoulders, not hyaline-margined (except in P. urnigerum);</text>
      <biological_entity id="o4630" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="to blade" constraintid="o4632" is_modifier="false" modifier="gradually; more or less abruptly" name="condition_or_size" notes="" src="d0_s2" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o4631" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o4632" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o4633" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s2" value="hyaline-margined" value_original="hyaline-margined" />
      </biological_entity>
      <biological_entity id="o4634" name="hinge-bud" name_original="hinge-cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <relation from="o4630" id="r1280" name="with" negation="false" src="d0_s2" to="o4631" />
      <relation from="o4633" id="r1281" name="with or without" negation="false" src="d0_s2" to="o4634" />
    </statement>
    <statement id="d0_s3">
      <text>margins serrate, toothed, or entire, without a differentiated border of elongated cells;</text>
      <biological_entity id="o4635" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4636" name="border" name_original="border" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o4637" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="length" src="d0_s3" value="elongated" value_original="elongated" />
      </biological_entity>
      <relation from="o4635" id="r1282" name="without" negation="false" src="d0_s3" to="o4636" />
      <relation from="o4636" id="r1283" name="part_of" negation="false" src="d0_s3" to="o4637" />
    </statement>
    <statement id="d0_s4">
      <text>adaxial lamellae numerous and compact, occupying the full width of the blade, or somewhat fewer with an evident marginal lamina, marginal cells not differentiated, or strongly differentiated, thick-walled and coarsely papillose.</text>
      <biological_entity constraint="adaxial" id="o4638" name="lamella" name_original="lamellae" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="full" value_original="full" />
        <character constraint="with marginal lamina" constraintid="o4640" is_modifier="false" modifier="somewhat" name="width" notes="" src="d0_s4" value="fewer" value_original="fewer" />
      </biological_entity>
      <biological_entity id="o4639" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity constraint="marginal" id="o4640" name="lamina" name_original="lamina" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="marginal" id="o4641" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="strongly" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="not" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="strongly" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>male plants similar to females in appearance, or budlike and inconspicuous.</text>
      <biological_entity id="o4642" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="male" value_original="male" />
        <character constraint="in appearance" constraintid="o4643" is_modifier="false" name="reproduction" src="d0_s6" value="female" value_original="female" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s6" value="budlike" value_original="budlike" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4643" name="appearance" name_original="appearance" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Seta smooth.</text>
      <biological_entity id="o4644" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule ovoid to short-cylindric, ± regular to somewhat asymmetric, terete, sometimes with 4 or more indistinct angles or ridges;</text>
      <biological_entity id="o4645" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s8" to="short-cylindric" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="regular" value_original="regular" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="regular to somewhat" value_original="regular to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s8" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o4646" name="angle" name_original="angles" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o4647" name="ridge" name_original="ridges" src="d0_s8" type="structure" />
      <relation from="o4645" id="r1284" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o4646" />
      <relation from="o4645" id="r1285" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o4647" />
    </statement>
    <statement id="d0_s9">
      <text>hypophysis not differentiated, tapering;</text>
      <biological_entity id="o4648" name="hypophysis" name_original="hypophysis" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stomata none;</text>
      <biological_entity id="o4649" name="stoma" name_original="stomata" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>exothecium mammillose to scabrous, the exothecial cells mamillate or with a single papillate projection of the outer wall;</text>
      <biological_entity id="o4650" name="exothecium" name_original="exothecium" src="d0_s11" type="structure">
        <character char_type="range_value" from="mammillose" name="relief" src="d0_s11" to="scabrous" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o4651" name="bud" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="mamillate" value_original="mamillate" />
        <character is_modifier="false" name="relief" src="d0_s11" value="with a single papillate projection" />
      </biological_entity>
      <biological_entity id="o4652" name="projection" name_original="projection" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="single" value_original="single" />
        <character is_modifier="true" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4653" name="wall" name_original="wall" src="d0_s11" type="structure" />
      <relation from="o4651" id="r1286" name="with" negation="false" src="d0_s11" to="o4652" />
      <relation from="o4652" id="r1287" name="part_of" negation="false" src="d0_s11" to="o4653" />
    </statement>
    <statement id="d0_s12">
      <text>operculum rostrate from a convex base;</text>
      <biological_entity id="o4654" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character constraint="from base" constraintid="o4655" is_modifier="false" name="shape" src="d0_s12" value="rostrate" value_original="rostrate" />
      </biological_entity>
      <biological_entity id="o4655" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>epiphragm persistent, attached to the peristome teeth;</text>
      <biological_entity id="o4656" name="epiphragm" name_original="epiphragm" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character constraint="to peristome teeth" constraintid="o4657" is_modifier="false" name="fixation" src="d0_s13" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o4657" name="tooth" name_original="teeth" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>peristome deeply reddish pigmented (at least in the median line), the teeth 32, compound, with median sinus narrow or almost obliterated.</text>
      <biological_entity id="o4658" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish pigmented" value_original="reddish pigmented" />
      </biological_entity>
      <biological_entity id="o4659" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="compound" value_original="compound" />
        <character is_modifier="false" modifier="almost" name="prominence" src="d0_s14" value="obliterated" value_original="obliterated" />
      </biological_entity>
      <biological_entity constraint="median" id="o4660" name="sinus" name_original="sinus" src="d0_s14" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o4659" id="r1288" name="with" negation="false" src="d0_s14" to="o4660" />
    </statement>
    <statement id="d0_s15">
      <text>Calyptra with a densely matted felt of hairs, covering most or all of the capsule.</text>
      <biological_entity id="o4661" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="with; densely" name="growth_form" src="d0_s15" value="matted" value_original="matted" />
        <character constraint="of capsule" constraintid="o4663" is_modifier="false" name="position_relational" src="d0_s15" value="covering" value_original="covering" />
      </biological_entity>
      <biological_entity id="o4662" name="hair" name_original="hairs" src="d0_s15" type="structure" />
      <biological_entity id="o4663" name="capsule" name_original="capsule" src="d0_s15" type="structure" />
      <relation from="o4661" id="r1289" name="felt of" negation="false" src="d0_s15" to="o4662" />
    </statement>
    <statement id="d0_s16">
      <text>Spores finely papillose.</text>
      <biological_entity id="o4664" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, tropical America, Europe, Africa, Asia, Australasia, widespread in the tropics of both hemispheres, with only a few North temperate representatives.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="tropical America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Australasia" establishment_means="native" />
        <character name="distribution" value="widespread in the tropics of both hemispheres" establishment_means="native" />
        <character name="distribution" value="with only a few North temperate representatives" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Species 52 (5 in the flora).</discussion>
  <discussion>North American species of Pogonatum vary greatly in size and habit from tall, laxly tufted plants to protonema mosses with individual plants scattered and only a few millimeters high. Pogonatum contortum of the Pacific Northwest, with leaves strongly crisped and contorted when dry, is the most “typical” of the genus as a whole. Pogonatum brachyphyllum and P. pensilvanicum are protonema-mosses, the gametophyte consisting of a persistent felted mat of protonema and leafy plants small and scattered. The other two species are distinctly polytrichoid in habit, with the margins of the lamellae thick-walled and coarsely papillose. Pogonatum dentatum is an arctic-montane species, whereas P. urnigerum has a somewhat more southerly distribution and occurs as well in the Himalayas and New Guinea. The sporophytes of our species are more uniform, with a scabrous exothecium, deeply pigmented peristome with compound peristome teeth, and no stomata. The exothecial “papillae” are projections of the cell wall, unlike the wart-like cuticular papillae often seen on the leaf surfaces of many Polytrichaceae.</discussion>
  <references>
    <reference>Hyvönen, J. 1989. A synopsis of genus Pogonatum (Polytrichaceae, Musci). Acta Bot. Fenn. 138: 1–87.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants to 0.6 cm, scattered over a green, persistent protonema</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 3-8(-12) cm, in loose pure tufts or growing among other bryophytes</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lamellae 25-40, compact, the leaf appearing thick and fleshy; leaf margins entire.</description>
      <determination>2 Pogonatum brachyphyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lamellae 11-16, the leaf membranous; leaf margins irregularly notched.</description>
      <determination>3 Pogonatum pensilvanicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves toothed from the apex nearly to the base; marginal cells of lamellae not differentiated, smooth</description>
      <determination>1 Pogonatum contortum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves toothed above the shoulders, the margins of the sheath entire; marginal cells of lamellae thick-walled and coarsely papillose</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf margins with multicellular, hooked teeth; leaf sheath not hyaline-margined; marginal cells of lamellae ± rectangular, flat-topped, the lumen quadrate; peristome divided nearly to the base.</description>
      <determination>4 Pogonatum dentatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf margins with mostly 1-cellular teeth; leaf sheath hyaline-margined; marginal cells of lamellae rounded to transversely elliptical, the lumen pentagonal; peristome divided to ca. 0.6.</description>
      <determination>5 Pogonatum urnigerum</determination>
    </key_statement>
  </key>
</bio:treatment>