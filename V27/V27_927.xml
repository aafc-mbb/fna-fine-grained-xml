<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">630</other_info_on_meta>
    <other_info_on_meta type="illustration_page">625</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Schimper" date="1860" rank="genus">microbryum</taxon_name>
    <taxon_name authority="(Smith) R. H. Zander" date="1993" rank="species">davallianum</taxon_name>
    <taxon_name authority="(Schwägrichen) R. H. Zander" date="1993" rank="variety">conicum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Buffalo Soc. Nat. Sci.</publication_title>
      <place_in_publication>32: 240. 1993,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus microbryum;species davallianum;variety conicum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075625</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnostomum</taxon_name>
    <taxon_name authority="Wareham" date="unknown" rank="species">conicum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>1(1): 26, plate 9. 1811</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gymnostomum;species conicum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pottia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">starckeana</taxon_name>
    <taxon_name authority="(Schwägrichen) D. P. Chamberlain" date="unknown" rank="subspecies">conica</taxon_name>
    <taxon_hierarchy>genus Pottia;species starckeana;subspecies conica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pottia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">texana</taxon_name>
    <taxon_hierarchy>genus Pottia;species texana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Capsule narrow-mouthed when dehisced, with usually 2 or more rows of thickened cells below mouth;</text>
      <biological_entity id="o1319" name="capsule" name_original="capsule" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="when dehisced" name="architecture" src="d0_s0" value="narrow-mouthed" value_original="narrow-mouthed" />
        <character constraint="of cells" constraintid="o1320" modifier="usually" name="quantity" src="d0_s0" unit="or morerows" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1320" name="cell" name_original="cells" src="d0_s0" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o1321" name="mouth" name_original="mouth" src="d0_s0" type="structure" />
      <relation from="o1320" id="r313" name="below" negation="false" src="d0_s0" to="o1321" />
    </statement>
    <statement id="d0_s1">
      <text>peristome rudimentary or absent.</text>
      <biological_entity id="o1322" name="peristome" name_original="peristome" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Spores papillose, 27–34 µm.</text>
      <biological_entity id="o1323" name="spore" name_original="spores" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="27" from_unit="um" name="some_measurement" src="d0_s2" to="34" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late winter and spring (Feb–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="late winter" />
        <character name="capsules maturing time" char_type="range_value" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nebr., Okla., Tex.; Mexico (Baja California); Europe; sw Asia; w, s Africa; Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2c.</number>
  <discussion>The San Marcos, Texas specimen (MICH) of var. conicum has a flaring mouth like that of var. davallianum. Another Texas specimen (Orcutt 5565, CANM, WTU) has a weak peristome (a thin line of proximal membrane).</discussion>
  
</bio:treatment>