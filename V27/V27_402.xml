<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ryszard Ochyra,Halina Bednarek-Ochyra</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">294</other_info_on_meta>
    <other_info_on_meta type="mention_page">295</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subfamily">racomitrioideae</taxon_name>
    <taxon_name authority="Bridel" date="1818" rank="genus">RACOMITRIUM</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Recent., suppl.</publication_title>
      <place_in_publication>4: 78. 1818  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily racomitrioideae;genus RACOMITRIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek rhakos, rag or remnant, and mitra, turban, alluding to calyptra frazzled or lobed at base</other_info_on_name>
    <other_info_on_name type="fna_id">127887</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Bridel" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="(Bridel) Müller Hal." date="unknown" rank="section">Racomitrium</taxon_name>
    <taxon_hierarchy>genus Grimmia;section Racomitrium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Lorentz" date="unknown" rank="genus">Racomitrium</taxon_name>
    <taxon_name authority="(Kindberg) Noguchi" date="unknown" rank="section">Lanuginosa</taxon_name>
    <taxon_hierarchy>genus Racomitrium;section Lanuginosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhacomitrium</taxon_name>
    <place_of_publication>
      <publication_title>orthographic variant</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Rhacomitrium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trichostomum</taxon_name>
    <taxon_name authority="(Bridel) Duby" date="unknown" rank="section">Racomitrium</taxon_name>
    <taxon_hierarchy>genus Trichostomum;section Racomitrium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, coarse and rigid, usually hoary, grayish, brownish or yellowish green, yellow or yellow to blackish brown.</text>
      <biological_entity id="o6632" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="relief" src="d0_s0" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="texture" src="d0_s0" value="rigid" value_original="rigid" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="hoary" value_original="hoary" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character char_type="range_value" from="yellowish green yellow or yellow" name="coloration" src="d0_s0" to="blackish brown" />
        <character char_type="range_value" from="yellowish green yellow or yellow" name="coloration" src="d0_s0" to="blackish brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly pinnately branched, with many short, lateral branchlets.</text>
      <biological_entity id="o6633" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly pinnately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6634" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="many" value_original="many" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o6633" id="r1573" name="with" negation="false" src="d0_s1" to="o6634" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect to slightly secund when dry, loosely erect to erect-spreading or spreading-recurved when wet, narrowly ovate to linear-lanceolate;</text>
      <biological_entity id="o6635" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character char_type="range_value" from="loosely erect" modifier="when wet" name="orientation" src="d0_s2" to="erect-spreading or spreading-recurved" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s2" to="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins 1-stratose, recurved to revolute, entire proximally, coarsely dentate along the hyaline border;</text>
      <biological_entity id="o6636" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character constraint="along border" constraintid="o6637" is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o6637" name="border" name_original="border" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apices gradually tapering to a long, slender, hyaline acumen;</text>
      <biological_entity id="o6638" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character constraint="to acumen" constraintid="o6639" is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6639" name="acumen" name_original="acumen" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="true" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>awns densely papillose, erose-dentate, long-decurrent, with the decurrencies flat or ruffled;</text>
      <biological_entity id="o6640" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="erose-dentate" value_original="erose-dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
      <biological_entity id="o6641" name="decurrency" name_original="decurrencies" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character name="prominence_or_shape" src="d0_s5" value="ruffled" value_original="ruffled" />
      </biological_entity>
      <relation from="o6640" id="r1574" name="with" negation="false" src="d0_s5" to="o6641" />
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent, in transverse-section 2-stratose, becoming 3-stratose in the proximal portion;</text>
      <biological_entity id="o6642" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character constraint="in proximal portion" constraintid="o6644" is_modifier="false" modifier="becoming" name="architecture" notes="" src="d0_s6" value="3-stratose" value_original="3-stratose" />
      </biological_entity>
      <biological_entity id="o6643" name="transverse-section" name_original="transverse-section" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6644" name="portion" name_original="portion" src="d0_s6" type="structure" />
      <relation from="o6642" id="r1575" name="in" negation="false" src="d0_s6" to="o6643" />
    </statement>
    <statement id="d0_s7">
      <text>laminal cells 1-stratose, sinuose, dull and opaque, distinctly papillose with large, flat papillae covering the longitudinal walls and almost the whole of the lumina except for a narrow central groove;</text>
      <biological_entity constraint="lumen" id="o6645" name="cell" name_original="cells" src="d0_s7" type="structure" constraint_original="lumen laminal; lumen">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="opaque" value_original="opaque" />
        <character constraint="with papillae" constraintid="o6646" is_modifier="false" modifier="distinctly" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o6646" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="large" value_original="large" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o6647" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o6648" name="lumen" name_original="lumina" src="d0_s7" type="structure" />
      <biological_entity constraint="central" id="o6649" name="groove" name_original="groove" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o6646" id="r1576" name="covering the" negation="false" src="d0_s7" to="o6647" />
      <relation from="o6645" id="r1577" modifier="almost" name="part_of" negation="false" src="d0_s7" to="o6648" />
      <relation from="o6645" id="r1578" name="except for" negation="false" src="d0_s7" to="o6649" />
    </statement>
    <statement id="d0_s8">
      <text>basal marginal laminal cells long-rectangular, forming 1 (–2) -seriate band, consisting of to 30 rectangular, translucent, not sinuose cells;</text>
      <biological_entity id="o6651" name="band" name_original="band" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s8" value="1(-2)-seriate" value_original="1(-2)-seriate" />
      </biological_entity>
      <biological_entity id="o6652" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="true" name="shape" src="d0_s8" value="rectangular" value_original="rectangular" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s8" value="translucent" value_original="translucent" />
        <character is_modifier="true" modifier="not" name="architecture" src="d0_s8" value="sinuose" value_original="sinuose" />
      </biological_entity>
      <relation from="o6650" id="r1579" name="forming" negation="false" src="d0_s8" to="o6651" />
      <relation from="o6650" id="r1580" name="consisting of" negation="false" src="d0_s8" to="o6652" />
    </statement>
    <statement id="d0_s9">
      <text>alar cells not differentiated;</text>
      <biological_entity constraint="basal marginal laminal" id="o6650" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="long-rectangular" value_original="long-rectangular" />
      </biological_entity>
      <biological_entity id="o6653" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>medial cells long-rectangular;</text>
      <biological_entity constraint="medial" id="o6654" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="long-rectangular" value_original="long-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>distal cells short-rectangular.</text>
      <biological_entity constraint="distal" id="o6655" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Inner perichaetial leaves hyaline, oblong, oblong-lanceolate to elliptical, abruptly constricted into a short, smooth or weakly papillose awn.</text>
      <biological_entity constraint="inner perichaetial" id="o6656" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s12" to="elliptical" />
        <character constraint="into awn" constraintid="o6657" is_modifier="false" modifier="abruptly" name="size" src="d0_s12" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o6657" name="awn" name_original="awn" src="d0_s12" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="true" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="true" modifier="weakly" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta single or often 2–3 per perichaetium, sinistrorse when dry, strongly papillose.</text>
      <biological_entity id="o6658" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="single" value_original="single" />
        <character name="quantity" src="d0_s13" value="often" value_original="often" />
        <character char_type="range_value" constraint="per perichaetium" constraintid="o6659" from="2" name="quantity" src="d0_s13" to="3" />
        <character is_modifier="false" modifier="when dry" name="orientation" notes="" src="d0_s13" value="sinistrorse" value_original="sinistrorse" />
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o6659" name="perichaetium" name_original="perichaetium" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsule straight, ovoid to ovoid-cylindric, somewhat ventricose in the base;</text>
      <biological_entity id="o6660" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="ovoid-cylindric" />
        <character constraint="in base" constraintid="o6661" is_modifier="false" modifier="somewhat" name="shape" src="d0_s14" value="ventricose" value_original="ventricose" />
      </biological_entity>
      <biological_entity id="o6661" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>annulus revoluble, 2–4-seriate;</text>
      <biological_entity id="o6662" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="revoluble" value_original="revoluble" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s15" value="2-4-seriate" value_original="2-4-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum long-rostrate;</text>
      <biological_entity id="o6663" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>peristome teeth, long, reddish-brown, split nearly to the base into 2 filiform, strongly papillose branches.</text>
      <biological_entity constraint="peristome" id="o6664" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s17" value="long" value_original="long" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity id="o6665" name="split" name_original="split" src="d0_s17" type="structure" />
      <biological_entity id="o6666" name="base" name_original="base" src="d0_s17" type="structure" />
      <biological_entity id="o6667" name="branch" name_original="branches" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="filiform" value_original="filiform" />
        <character is_modifier="true" modifier="strongly" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
      </biological_entity>
      <relation from="o6665" id="r1581" name="to" negation="false" src="d0_s17" to="o6666" />
      <relation from="o6666" id="r1582" name="into" negation="false" src="d0_s17" to="o6667" />
    </statement>
    <statement id="d0_s18">
      <text>Calyptra conic-mitrate to cucullate, naked.</text>
      <biological_entity id="o6668" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character char_type="range_value" from="conic-mitrate" name="shape" src="d0_s18" to="cucullate" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores spherical, pale-yellow, finely roughened.</text>
      <biological_entity id="o6669" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="spherical" value_original="spherical" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="finely" name="relief_or_texture" src="d0_s19" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Central America, South America, Europe, Asia, South Africa, Atlantic Islands, Indian Ocean Islands, Pacific Islands, Australia, Antarctica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="South Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="Antarctica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Species 3 (1 in the flora).</discussion>
  <discussion>Traditionally, Racomitrium has been considered as a homogeneous genus characterized by its laminal cells having thick and strongly sinuose to nodulose longitudinal cell walls. This characteristic leaf areolation was typically coupled with a peristome of linear teeth arising from a low or high basal membrane and divided nearly to the base into two filiform, somewhat paired segments that are equally thickened and less prominently trabeculate on both external and internal sides. In addition, the genus was characterized by the consistent lack of a central strand, usual presence of the prostome, sinuose-walled epidermal cells of the vaginula, and cladocarpous sexual condition. This combination of characters made Racomitrium readily recognizable. Revisionary studies of the genus showed that in its traditional circumscription it was an artificial, heterogeneous taxon and, as a result, it has been recently split into four genera, Racomitrium in the narrow sense, Codriophorus, Niphotrichum, and Bucklandiella. Racomitrium in the narrow sense is characterized by: distinctly papillose setae twisted to the left; long, hyaline, strongly papillose awns that are long-decurrent and erose-dentate; large, flat papillae with small secondary papillae densely covering the longitudinal cell walls and almost the whole lumina except for a narrow groove in the middle; peristome teeth divided to the base into 2(–3) filiform branches; and capsules slightly ventricose at the base. The papillosity of the setae is a unique feature of this genus, and is unknown in other acrocarpous mosses. That character, the unusual shape of the awns, and the ventricose capsule make the narrowly conceived Racomitrium readily distinguishable from its segregates.</discussion>
  <references>
    <reference>Tallis, J. H. 1959. Studies in the biology and ecology of Rhacomitrium lanuginosum Brid. J. Ecol. 47: 325–350.</reference>
    <reference>Tallis, J. H. 1964. Growth studies on Rhacomitrium lanuginosum. Bryologist 67: 417–422.</reference>
    <reference>Vitt, D. H. and C. Marsh. 1988. Population variation and phytogeography of Racomitrium lanuginosum and R. pruinosum. Beih. Nova Hedwigia 90: 235–260.</reference>
  </references>
  
</bio:treatment>