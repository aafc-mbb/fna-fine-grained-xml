<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">382</other_info_on_meta>
    <other_info_on_meta type="treatment_page">381</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper in W. P. Schimper" date="1856" rank="genus">cynodontium</taxon_name>
    <taxon_name authority="(Hedwig) Schimper" date="1856" rank="species">polycarpon</taxon_name>
    <place_of_publication>
      <publication_title>Coroll. Bryol. Eur.,</publication_title>
      <place_in_publication>12. 1856,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus cynodontium;species polycarpon</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075580</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fissidens</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">polycarpos</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>159. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Fissidens;species polycarpos;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 5 cm.</text>
      <biological_entity id="o3877" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves to 5 mm, rather broadly lanceolate, long-acuminate;</text>
      <biological_entity id="o3878" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="to 5 mm; rather broadly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>distal leaf margin broadly recurved, at least on one side, 2-stratose;</text>
      <biological_entity constraint="leaf" id="o3879" name="margin" name_original="margin" src="d0_s2" type="structure" constraint_original="distal leaf">
        <character is_modifier="false" modifier="broadly" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o3880" name="side" name_original="side" src="d0_s2" type="structure" />
      <relation from="o3879" id="r894" modifier="at-least" name="on" negation="false" src="d0_s2" to="o3880" />
    </statement>
    <statement id="d0_s3">
      <text>cells of lamina smooth, scattered mammillose or distinctly mammillose distally, mammillae well developed especially on the adaxial side, distal cells 10 µm wide or more.</text>
      <biological_entity id="o3881" name="cell" name_original="cells" src="d0_s3" type="structure" constraint="lamina" constraint_original="lamina; lamina">
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="relief" src="d0_s3" value="mammillose" value_original="mammillose" />
        <character is_modifier="false" modifier="distinctly; distally" name="relief" src="d0_s3" value="mammillose" value_original="mammillose" />
      </biological_entity>
      <biological_entity id="o3882" name="lamina" name_original="lamina" src="d0_s3" type="structure" />
      <biological_entity id="o3883" name="mammilla" name_original="mammillae" src="d0_s3" type="structure">
        <character constraint="on adaxial side" constraintid="o3884" is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3884" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o3885" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character name="width" src="d0_s3" unit="um" value="10" value_original="10" />
      </biological_entity>
      <relation from="o3881" id="r895" name="part_of" negation="false" src="d0_s3" to="o3882" />
    </statement>
    <statement id="d0_s4">
      <text>Perigonium sessile.</text>
      <biological_entity id="o3886" name="perigonium" name_original="perigonium" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta ca. 0.8 cm, straight wet or dry.</text>
      <biological_entity id="o3887" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="cm" value="0.8" value_original="0.8" />
        <character is_modifier="false" name="condition" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="condition" src="d0_s5" value="wet" value_original="wet" />
        <character is_modifier="false" name="condition" src="d0_s5" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule symmetrical, erect, not strumose;</text>
      <biological_entity id="o3888" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="symmetrical" value_original="symmetrical" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="strumose" value_original="strumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>annulus of large, deciduous cells.</text>
      <biological_entity id="o3889" name="annulus" name_original="annulus" src="d0_s7" type="structure" constraint="cell" constraint_original="cell; cell" />
      <biological_entity id="o3890" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="large" value_original="large" />
        <character is_modifier="true" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <relation from="o3889" id="r896" name="part_of" negation="false" src="d0_s7" to="o3890" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acid rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Yukon; Alaska, Colo., Idaho, Mich., Minn., Mont., Wyo.; n Eurasia; Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Specimens of Cynodontium polycarpon generally found in American herbaria upon re-examination will likely be C. jenneri instead. The latter was once considered to be a variety of C. polycarpon, accounting for many distributional records of C. polycarpon in North America; for example, in the most recent Canadian checklist (R. R. Ireland et al. 1987), of 10 provincial reports only that for the Northwest Territories could be verified by the authors, and the remaining Canadian distribution cited above must be viewed, then, with skepticism. E. Nyholm (1986+, fasc. 1) stated that both C. polycarpon and C. strumiferum have somewhat smaller, less regularly quadrate cells in the distal portion of the leaf and a broadly recurved margin on at least one side of the leaf as opposed to the case of C. jenneri, which has rather regularly quadrate, transparent cells. According to Nyholm, laminal cells of C. polycarpon are 10–12(–14) µm wide, whereas those of C. jenneri are larger: 12–14(–20) µm. H. A. Crum and L. E. Anderson (1981) considered this species doubtfully present in North America. Cynodontium polycarpon subsp. fallax (Limpricht) Kindberg was excluded by Anderson et al. (1990).</discussion>
  
</bio:treatment>