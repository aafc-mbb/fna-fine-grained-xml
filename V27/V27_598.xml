<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="treatment_page">421</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="I. Hagen" date="1915" rank="genus">kiaeria</taxon_name>
    <taxon_name authority="(Weber &amp; D. Mohr) I. Hagen" date="1915" rank="species">starkei</taxon_name>
    <place_of_publication>
      <publication_title>Kongel. Norske Vidensk. Selsk. Skr. (Trondheim)</publication_title>
      <place_in_publication>1914: 114. 1915,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus kiaeria;species starkei</taxon_hierarchy>
    <other_info_on_name type="fna_id">240000068</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Weber &amp; D. Mohr" date="unknown" rank="species">starkei</taxon_name>
    <place_of_publication>
      <publication_title>Neues Bot. Taschenb. Anfänger Wiss. Apothekerkunst</publication_title>
      <place_in_publication>1807: 189. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species starkei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arctoa</taxon_name>
    <taxon_name authority="(Weber &amp; D. Mohr) Loeske" date="unknown" rank="species">starkei</taxon_name>
    <taxon_hierarchy>genus Arctoa;species starkei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose tufts, green to yellow, mostly shiny.</text>
      <biological_entity id="o1528" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="yellow" />
        <character is_modifier="false" modifier="mostly" name="reflectance" src="d0_s0" value="shiny" value_original="shiny" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1529" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o1528" id="r359" name="in" negation="false" src="d0_s0" to="o1529" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–2 (–4) cm.</text>
      <biological_entity id="o1530" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly falcate-secund, curled at tips when dry, lanceolate, gradually subulate, 2–4.5 mm, margins distally 1-stratose;</text>
      <biological_entity id="o1531" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character constraint="at tips" constraintid="o1532" is_modifier="false" name="shape" src="d0_s2" value="curled" value_original="curled" />
      </biological_entity>
      <biological_entity id="o1532" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1533" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa 50–60 µm wide at base;</text>
      <biological_entity id="o1534" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o1535" from="50" from_unit="um" name="width" src="d0_s3" to="60" to_unit="um" />
      </biological_entity>
      <biological_entity id="o1535" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>distal laminal cells mostly elongate (2–4:1), occasionally subquadrate, 7–9 µm wide, smooth or weakly mammillose-roughened;</text>
      <biological_entity constraint="distal laminal" id="o1536" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s4" to="9" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="relief" src="d0_s4" value="mammillose-roughened" value_original="mammillose-roughened" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells elongate, smooth, sometimes porose, alar cells strongly inflated and differentiated from the bordering small quadrate cells.</text>
      <biological_entity constraint="distal laminal" id="o1537" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o1538" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity id="o1539" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s5" value="inflated" value_original="inflated" />
        <character constraint="from cells" constraintid="o1540" is_modifier="false" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o1540" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Perichaetial leaves similar to the cauline.</text>
      <biological_entity constraint="perichaetial" id="o1541" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perigonia sessile, located just below perichaetia.</text>
      <biological_entity id="o1542" name="perigonium" name_original="perigonia" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o1543" name="perichaetium" name_original="perichaetia" src="d0_s7" type="structure" />
      <relation from="o1542" id="r360" modifier="just below" name="located" negation="false" src="d0_s7" to="o1543" />
    </statement>
    <statement id="d0_s8">
      <text>Capsule distinctly ribbed when dry, urn 1.3–2 mm.</text>
      <biological_entity id="o1544" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when dry" name="architecture_or_shape" src="d0_s8" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o1545" name="urn" name_original="urn" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 13–21 µm.</text>
      <biological_entity id="o1546" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s9" to="21" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid rock or sandy soil in rock crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" modifier="acid rock or" constraint="in rock crevices" />
        <character name="habitat" value="rock crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>subalpine to alpine elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Nfld. and Labr. (Nfld.), N.S., Que.; Alaska, Idaho, Mont., N.H., Oreg., Wash.; South America; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Kiaeria starkei occurs frequently on vertical rock surfaces, and is distinguished by the sessile male inflorescence, which is located close to the perichaetia. It can be separated in the field from the more terrestrial K. blyttii by its shiny, falcate leaves. Subalpine forms of K. falcata are more commonly found on horizontal rock surfaces and do not have grooved sporangia.</discussion>
  <references>
    <reference>Woolgrove, C. E. and S. J. Woodin. 1996. Ecophysiology of a snow-bed bryophyte—Kiaeria starkei—during snowmelt and uptake of nitrate from meltwater. Canad. J. Bot. 74: 1095–1103.</reference>
  </references>
  
</bio:treatment>