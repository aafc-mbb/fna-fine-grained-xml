<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="treatment_page">131</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="G. L. Smith" date="1971" rank="genus">polytrichastrum</taxon_name>
    <taxon_name authority="(Hedwig) G. L. Smith" date="1971" rank="species">formosum</taxon_name>
    <taxon_name authority="(Mitten) Z. Iwatsuki &amp; A. Noguchi" date="1973" rank="variety">densifolium</taxon_name>
    <place_of_publication>
      <publication_title>J. Hattori Bot. Lab.</publication_title>
      <place_in_publication>37: 389. 1973,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus polytrichastrum;species formosum;variety densifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065251</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">densifolium</taxon_name>
    <place_of_publication>
      <publication_title>J. Proc. Linn. Soc., Bot., suppl.</publication_title>
      <place_in_publication>2: 155. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polytrichum;species densifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">formosum</taxon_name>
    <taxon_name authority="(Mitten) Osada &amp; K. Yano" date="unknown" rank="variety">densifolium</taxon_name>
    <taxon_hierarchy>genus Polytrichum;species formosum;variety densifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium, rather slender.</text>
      <biological_entity id="o6701" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium" value_original="medium" />
        <character is_modifier="false" modifier="rather" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–6 (–10) cm.</text>
      <biological_entity id="o6702" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 6–8 (–10) mm, spreading when moist;</text>
      <biological_entity id="o6703" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>marginal lamina 3–5 (–10) cells wide, plane to erect or somewhat inflexed;</text>
      <biological_entity constraint="marginal" id="o6704" name="lamina" name_original="lamina" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o6705" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="wide" value_original="wide" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s3" value="inflexed" value_original="inflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lamellae (3–) 4–6 cells high, entire or distantly and finely serrulate, the marginal cells narrowly elliptic, not or only slightly thickened.</text>
      <biological_entity id="o6706" name="lamella" name_original="lamellae" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o6707" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="height" src="d0_s4" value="high" value_original="high" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="distantly" value_original="distantly" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition polygamous;</text>
    </statement>
    <statement id="d0_s6">
      <text>males intermingled with the female or male and female inflorescences very rarely observed on the same stem.</text>
      <biological_entity constraint="marginal" id="o6708" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="not; only slightly" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="polygamous" value_original="polygamous" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="male" value_original="male" />
      </biological_entity>
      <biological_entity id="o6709" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="female" value_original="female" />
        <character is_modifier="true" name="reproduction" src="d0_s6" value="male" value_original="male" />
        <character is_modifier="true" name="reproduction" src="d0_s6" value="female" value_original="female" />
      </biological_entity>
      <biological_entity id="o6710" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o6709" id="r1867" name="intermingled with" negation="false" src="d0_s6" to="o6709" />
      <relation from="o6710" id="r1868" name="observed on" negation="false" src="d0_s6" to="o6710" />
    </statement>
    <statement id="d0_s7">
      <text>Capsule rather slender, pale yellowish-brown;</text>
      <biological_entity id="o6711" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="rather" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale yellowish-brown" value_original="pale yellowish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypophysis cylindric but not sharply delimited;</text>
      <biological_entity id="o6712" name="hypophysis" name_original="hypophysis" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="not sharply" name="prominence" src="d0_s8" value="delimited" value_original="delimited" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>peristome teeth 50–64, somewhat irregular in size and shape.</text>
      <biological_entity constraint="peristome" id="o6713" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s9" to="64" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s9" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil or humus in damp to wet, chiefly coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" constraint="in damp to wet , chiefly coniferous forests" />
        <character name="habitat" value="humus" constraint="in damp to wet , chiefly coniferous forests" />
        <character name="habitat" value="wet" modifier="damp" />
        <character name="habitat" value="coniferous forests" modifier="chiefly" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que.; Alaska, Calif., Colo., Maine, Minn., N.H., N.J., N.Y., Pa., Tenn., Wash., Wis., Vt.; s, e Asia (Japan, Nepal).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="s" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="e Asia (Nepal)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  <discussion>Variety densifolium is absent from the Arctic, but widespread across northern North America. Most of the eastern North American plants that have been called Polytrichastrum formosum probably belong to this variety, although many of the older herbarium specimens so named are P. ohioense. Variety densifolium is evidently monoicous (see T. Osada 1966), although admittedly this is difficult to demonstrate.</discussion>
  
</bio:treatment>