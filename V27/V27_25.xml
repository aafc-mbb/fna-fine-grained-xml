<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="treatment_page">58</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="(Russow) Schimper" date="1876" rank="section">Squarrosa</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Eur. ed.</publication_title>
      <place_in_publication>2, 835. 1876,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section Squarrosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">316299</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="Russow" date="unknown" rank="unranked">Squarrosa</taxon_name>
    <place_of_publication>
      <publication_title>Beitr. Torfm.,</publication_title>
      <place_in_publication>33. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sphagnum;unranked Squarrosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate-sized to robust, with distinct capitulum;</text>
      <biological_entity id="o2148" name="capitulum" name_original="capitulum" src="d0_s0" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s0" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o2147" id="r585" name="with" negation="false" src="d0_s0" to="o2148" />
    </statement>
    <statement id="d0_s1">
      <text>green,yellowish-brown.</text>
      <biological_entity id="o2147" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="moderate-sized" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowish-brown" value_original="yellowish-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems green to dark reddish-brown, superficial cortex of 2–4 layers of efibrillose, nonornamented, enlarged, thin-walled, aporose, rectangular cells.</text>
      <biological_entity id="o2149" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="dark reddish-brown" />
      </biological_entity>
      <biological_entity constraint="superficial" id="o2150" name="cortex" name_original="cortex" src="d0_s2" type="structure" />
      <biological_entity id="o2151" name="layer" name_original="layers" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o2152" name="bud" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="nonornamented" value_original="nonornamented" />
        <character is_modifier="true" name="size" src="d0_s2" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="true" name="shape" src="d0_s2" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <relation from="o2150" id="r586" name="consist_of" negation="false" src="d0_s2" to="o2151" />
      <relation from="o2151" id="r587" name="part_of" negation="false" src="d0_s2" to="o2152" />
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves ovate, ovate-lingulate to lingulate;</text>
    </statement>
    <statement id="d0_s4">
      <text>with broad, fringed apex;</text>
      <biological_entity id="o2154" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character is_modifier="true" name="shape" src="d0_s4" value="fringed" value_original="fringed" />
      </biological_entity>
      <relation from="o2153" id="r588" name="with" negation="false" src="d0_s4" to="o2154" />
    </statement>
    <statement id="d0_s5">
      <text>little or no border along margins or base;</text>
      <biological_entity id="o2153" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="ovate-lingulate" name="shape" src="d0_s3" to="lingulate" />
      </biological_entity>
      <biological_entity id="o2155" name="border" name_original="border" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="no" value_original="no" />
      </biological_entity>
      <biological_entity id="o2156" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o2157" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o2155" id="r589" name="along" negation="false" src="d0_s5" to="o2156" />
      <relation from="o2155" id="r590" name="along" negation="false" src="d0_s5" to="o2157" />
    </statement>
    <statement id="d0_s6">
      <text>hyaline cells rhomboid, efibrillose, nonornamented, aporose, usually nonseptate, with resorption gaps on exterior surface.</text>
      <biological_entity id="o2158" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="nonornamented" value_original="nonornamented" />
      </biological_entity>
      <biological_entity id="o2159" name="resorption" name_original="resorption" src="d0_s6" type="structure" />
      <biological_entity id="o2160" name="gap" name_original="gaps" src="d0_s6" type="structure" />
      <biological_entity constraint="exterior" id="o2161" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <relation from="o2158" id="r591" modifier="usually" name="with" negation="false" src="d0_s6" to="o2159" />
      <relation from="o2158" id="r592" modifier="usually" name="with" negation="false" src="d0_s6" to="o2160" />
      <relation from="o2159" id="r593" name="on" negation="false" src="d0_s6" to="o2161" />
      <relation from="o2160" id="r594" name="on" negation="false" src="d0_s6" to="o2161" />
    </statement>
    <statement id="d0_s7">
      <text>Branches strongly dimorphic, pendent branches thinner but about same length as spreading branches.</text>
      <biological_entity id="o2162" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="growth_form" src="d0_s7" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o2163" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="pendent" value_original="pendent" />
        <character constraint="as branches" constraintid="o2164" is_modifier="false" name="length" src="d0_s7" value="thinner" value_original="thinner" />
      </biological_entity>
      <biological_entity id="o2164" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch stems green, with cortex surrounded by 1–2 layers of efibrillose, nonornamented, enlarged, thin-walled cells, some cells apically porose with inconspicuous necks.</text>
      <biological_entity constraint="branch" id="o2165" name="stem" name_original="stems" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o2166" name="cortex" name_original="cortex" src="d0_s8" type="structure" />
      <biological_entity id="o2167" name="layer" name_original="layers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o2168" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="true" name="pubescence" src="d0_s8" value="nonornamented" value_original="nonornamented" />
        <character is_modifier="true" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="thin-walled" value_original="thin-walled" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o2169" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character constraint="with necks" constraintid="o2170" is_modifier="false" modifier="apically" name="architecture" src="d0_s8" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity id="o2170" name="neck" name_original="necks" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o2165" id="r595" name="with" negation="false" src="d0_s8" to="o2166" />
      <relation from="o2166" id="r596" name="surrounded by" negation="false" src="d0_s8" to="o2167" />
      <relation from="o2166" id="r597" name="surrounded by" negation="false" src="d0_s8" to="o2168" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves strongly squarrose or spreading, ovate, ovate-hastate or ovatelanceolate, margins entire, apex involute and smooth;</text>
      <biological_entity constraint="branch" id="o2171" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s9" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate-hastate" value_original="ovate-hastate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity id="o2172" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2173" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hyaline cells fibrillose;</text>
    </statement>
    <statement id="d0_s11">
      <text>with large, round pores at cell ends and along commissures, sometimes with faint papillae on interior walls;</text>
      <biological_entity id="o2174" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s10" value="fibrillose" value_original="fibrillose" />
      </biological_entity>
      <biological_entity id="o2175" name="pore" name_original="pores" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s11" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o2176" name="commissure" name_original="commissures" src="d0_s11" type="structure" />
      <biological_entity id="o2177" name="papilla" name_original="papillae" src="d0_s11" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s11" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity constraint="interior" id="o2178" name="wall" name_original="walls" src="d0_s11" type="structure" />
      <relation from="o2174" id="r598" name="with" negation="false" src="d0_s11" to="o2175" />
      <relation from="o2175" id="r599" name="at cell ends and along" negation="false" src="d0_s11" to="o2176" />
      <relation from="o2177" id="r600" name="on" negation="false" src="d0_s11" to="o2178" />
    </statement>
    <statement id="d0_s12">
      <text>chlorophyllous cells ovate-triangular, elliptical to ovate-elliptical in transverse-section, more broadly exposed on convex surface, end walls not thickened.</text>
      <biological_entity id="o2179" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="chlorophyllous" value_original="chlorophyllous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate-triangular" value_original="ovate-triangular" />
        <character char_type="range_value" constraint="in transverse-section" constraintid="o2180" from="elliptical" name="arrangement_or_shape" src="d0_s12" to="ovate-elliptical" />
        <character constraint="on surface" constraintid="o2181" is_modifier="false" modifier="broadly" name="prominence" notes="" src="d0_s12" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o2180" name="transverse-section" name_original="transverse-section" src="d0_s12" type="structure" />
      <biological_entity id="o2181" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition monoicous or dioicous.</text>
      <biological_entity constraint="end" id="o2182" name="wall" name_original="walls" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s12" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="monoicous" value_original="monoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule 2 mm or more, with scattered pseudomata.</text>
      <biological_entity id="o2183" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2184" name="pseudomatum" name_original="pseudomata" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o2183" id="r601" name="with" negation="false" src="d0_s14" to="o2184" />
    </statement>
    <statement id="d0_s15">
      <text>Spores less than 30 µm, both surfaces smooth to finely papillose;</text>
      <biological_entity id="o2185" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" src="d0_s15" to="30" to_unit="um" />
      </biological_entity>
      <biological_entity id="o2186" name="surface" name_original="surfaces" src="d0_s15" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s15" to="finely papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>proximal laesura more than 0.5 spore radius.</text>
      <biological_entity constraint="proximal" id="o2187" name="laesurum" name_original="laesura" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" name="quantity" src="d0_s16" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2188" name="spore" name_original="spore" src="d0_s16" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1d.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branch leaves markedly truncate; chlorophyll cells ovate-triangular in transverse section, with the widest part at or near the convex surface.</description>
      <determination>19 Sphagnum tundrae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branch leaves not truncate; chlorophyll cells elliptical to elliptical-ovate with the broadest part typically some distance from the convex surface</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branch leaves strongly squarrose (terete in tundra forms), large (1.9-3 mm); hyaline cells of branch leaves with ringed elliptic pores on concave surface and unringed pores on convex surface; stem leaves 1/2-2/3 as long as branch leaves (1.1-1.9 mm)</description>
      <determination>17 Sphagnum squarrosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branch leaves imbricate (squarrose in shade forms), moderate in size (1-1.5 mm); hyaline cells of branch leaves with unringed pores on concave and convex surfaces; stem leaves as long or longer than branch leaves (1-1.7 mm)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Branch leaf hyaline cells near leaf base on convex surface mostly aporose and on concave surface with large, faint pores; 1-2 hanging branches.</description>
      <determination>16 Sphagnum mirum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Branch leaf hyaline cells near leaf base on convex surface with large, round pores and on concave surface mostly aporose; 2-3 hanging branches.</description>
      <determination>18 Sphagnum teres</determination>
    </key_statement>
  </key>
</bio:treatment>