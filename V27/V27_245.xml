<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="treatment_page">186</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">funariaceae</taxon_name>
    <taxon_name authority="Schwägrichen" date="1823" rank="genus">entosthodon</taxon_name>
    <taxon_name authority="(Bartram) Grout" date="1935" rank="species">planoconvexus</taxon_name>
    <place_of_publication>
      <publication_title>Moss Fl. N. Amer.</publication_title>
      <place_in_publication>2: 80. 1935,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family funariaceae;genus entosthodon;species planoconvexus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075511</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Funaria</taxon_name>
    <taxon_name authority="Bartram" date="unknown" rank="species">planoconvexa</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>31: 94, plate 9, figs. G–L. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Funaria;species planoconvexa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–7 mm, yellowish green, the main axis often arising from a short fleshy rhizome.</text>
      <biological_entity id="o7979" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="main" id="o7980" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character constraint="from rhizome" constraintid="o7981" is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o7981" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves variously contorted when dry, oblong to obovate or spathulate, imbricate, somewhat concave, mostly 2–3 mm;</text>
      <biological_entity id="o7982" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s1" to="obovate or spathulate" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character char_type="range_value" from="2" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins serrulate distally;</text>
      <biological_entity id="o7983" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>apices acute, abruptly narrowing to a short filiform point about 250 µm in length;</text>
      <biological_entity id="o7984" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character constraint="to point" constraintid="o7985" is_modifier="false" modifier="abruptly" name="width" src="d0_s3" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o7985" name="point" name_original="point" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character name="length" src="d0_s3" unit="um" value="250" value_original="250" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa ending 5–9 cells before the apiculus;</text>
      <biological_entity id="o7986" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o7987" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="9" />
      </biological_entity>
      <biological_entity id="o7988" name="apiculu" name_original="apiculus" src="d0_s4" type="structure" />
      <relation from="o7986" id="r2230" name="ending" negation="false" src="d0_s4" to="o7987" />
      <relation from="o7986" id="r2231" name="before" negation="false" src="d0_s4" to="o7988" />
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells rectangular (50–55 × 20–35 µm), distal cells irregularly quadrate to elongate-hexagonal to short-rectangular, little differentiated along the margins.</text>
      <biological_entity constraint="basal laminal" id="o7989" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7990" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="irregularly quadrate" name="shape" src="d0_s5" to="elongate-hexagonal" />
        <character constraint="along margins" constraintid="o7991" is_modifier="false" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o7991" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Seta 6–10 mm, straight, twisted, hygroscopic.</text>
      <biological_entity id="o7992" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="hygroscopic" value_original="hygroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule yellowish to brownish, ovoid-pyriform with about half the length in the apophysis, 2–3 mm, sulcate when dry and empty;</text>
      <biological_entity id="o7993" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s7" to="brownish" />
        <character constraint="in apophysis" constraintid="o7994" is_modifier="false" name="length" src="d0_s7" value="ovoid-pyriform" value_original="ovoid-pyriform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="when dry and empty" name="architecture" src="d0_s7" value="sulcate" value_original="sulcate" />
      </biological_entity>
      <biological_entity id="o7994" name="apophysis" name_original="apophysis" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>exothecial cells scarcely thickened, narrowly oblong and transversely elongate in 5–7 rows proximal to the mouth;</text>
      <biological_entity constraint="exothecial" id="o7995" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character constraint="in rows" constraintid="o7996" is_modifier="false" modifier="transversely" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o7996" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="7" />
        <character constraint="to mouth" constraintid="o7997" is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o7997" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>operculum planoconvex;</text>
      <biological_entity id="o7998" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="planoconvex" value_original="planoconvex" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome well developed, exostome teeth reddish-brown to brownish yellow, lanceolate, striate papillose, trabeculate basally, terminating in a coarsely papillose tip, endostome evanescent with low, broad, segments.</text>
      <biological_entity id="o7999" name="peristome" name_original="peristome" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o8000" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s10" to="brownish yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s10" value="striate" value_original="striate" />
        <character is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s10" value="trabeculate" value_original="trabeculate" />
      </biological_entity>
      <biological_entity id="o8001" name="tip" name_original="tip" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="coarsely" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o8002" name="endostome" name_original="endostome" src="d0_s10" type="structure">
        <character constraint="with segments" constraintid="o8003" is_modifier="false" name="duration" src="d0_s10" value="evanescent" value_original="evanescent" />
      </biological_entity>
      <biological_entity id="o8003" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="true" name="position" src="d0_s10" value="low" value_original="low" />
        <character is_modifier="true" name="width" src="d0_s10" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o8000" id="r2232" name="terminating in" negation="false" src="d0_s10" to="o8001" />
    </statement>
    <statement id="d0_s11">
      <text>Calyptra cucullate, long-beaked, inflated around the capsule, large, smooth.</text>
      <biological_entity id="o8004" name="calyptra" name_original="calyptra" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="long-beaked" value_original="long-beaked" />
        <character constraint="around capsule" constraintid="o8005" is_modifier="false" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8005" name="capsule" name_original="capsule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Spores 20–25 µm, bacculate-insulate.</text>
      <biological_entity id="o8006" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s12" to="25" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s12" value="bacculate-insulate" value_original="bacculate-insulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soil, canyons and desert washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="desert washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  
</bio:treatment>