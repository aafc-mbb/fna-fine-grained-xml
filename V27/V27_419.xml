<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="mention_page">309</other_info_on_meta>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">ptychomitriaceae</taxon_name>
    <taxon_name authority="Fürnrohr" date="1829" rank="genus">PTYCHOMITRIUM</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>12(Ergänzungsbl.): 19. 1829,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ptychomitriaceae;genus PTYCHOMITRIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ptyx, fold, and mitra, turban, alluding to plicate calyptra</other_info_on_name>
    <other_info_on_name type="fna_id">127592</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to robust, tufted or caspitose, dark green to blackish.</text>
      <biological_entity id="o5465" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="arrangement_or_pubescence" src="d0_s0" value="caspitose" value_original="caspitose" />
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s0" to="blackish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or repent.</text>
      <biological_entity id="o5466" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="repent" value_original="repent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect to crispate when dry, margins entire to serrulate or serrate;</text>
      <biological_entity id="o5467" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o5468" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s2" to="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>medial cells smooth or slightly papillose.</text>
    </statement>
    <statement id="d0_s4">
      <text>Specialized asexual reproduction rare, by 1-seriate gemmae on branched axillary filaments.</text>
      <biological_entity constraint="medial" id="o5469" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="development" src="d0_s4" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o5470" name="gemma" name_original="gemmae" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="rare" value_original="rare" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s4" value="1-seriate" value_original="1-seriate" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o5471" name="filament" name_original="filaments" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o5470" id="r1295" name="on" negation="false" src="d0_s4" to="o5471" />
    </statement>
    <statement id="d0_s5">
      <text>Seta straight.</text>
      <biological_entity id="o5472" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule ovoid to cylindric, symmetric or slightly curved, smooth to wrinkled or ribbed when dry.</text>
      <biological_entity id="o5473" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s6" to="wrinkled" />
        <character is_modifier="false" modifier="when dry" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyptra mitrate, more or less plicate, lobed proximally.</text>
      <biological_entity id="o5474" name="calyptra" name_original="calyptra" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s7" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spores smooth to papillose.</text>
      <biological_entity id="o5475" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s8" to="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, mostly in temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="mostly in temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Species 40–50 (5 in the flora).</discussion>
  <discussion>Excluded Species:</discussion>
  <discussion>Glyphomitrium canadense Mitten</discussion>
  <discussion>This taxon was included for western Canada by G. N. Jones (1933), but was excluded from North America by H. A. Crum (1972) and L. E. Anderson et al. (1990). Crum considered Glyphomitrium canadense to be a synonym of the British G. daviesii (Withering) Bridel and suggested that the specimen on which Mitten based the name came from Great Britain.</discussion>
  <references>
    <reference>Cao, T. and D. H. Vitt. 1994. North American–East Asian similarities in the genus Ptychomitrium (Bryopsida). Bryologist 97: 34–41.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves coarsely serrate distally</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves entire or obscurely irregularly serrulate distally</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves slenderly long-acuminate, 4-6 mm; basal leaf margins broadly recurved on one or both sides; calyptra deeply lobed proximally, lobes half length of calyptra.</description>
      <determination>1 Ptychomitrium gardneri</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves broadly acuminate, 3-4 mm; basal leaf margins plane and erect or irregularly narrowly recurved on one side proximally; calyptra shallowly lobed proximally, lobes less than 1/2 length of calyptra.</description>
      <determination>2 Ptychomitrium serratum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants dull, mostly corticolous; leaves straight or slightly contorted but not crispate when dry, margins mostly obscurely serrulate distally.</description>
      <determination>5 Ptychomitrium drummondii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants glossy, mostly on rock; leaves crispate when dry, margins entire</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Longest leaves mostly 2.5-4 mm.</description>
      <determination>3 Ptychomitrium sinense</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Longest leaves mostly 2 mm.</description>
      <determination>4 Ptychomitrium incurvum</determination>
    </key_statement>
  </key>
</bio:treatment>