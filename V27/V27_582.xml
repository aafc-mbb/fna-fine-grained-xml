<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">400</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">410</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="treatment_page">411</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">dicranum</taxon_name>
    <taxon_name authority="(Lindberg &amp; Arnell) C. E. O. Jensen in H. Weimarck" date="1937" rank="species">acutifolium</taxon_name>
    <place_of_publication>
      <publication_title>in H. Weimarck, Förtekn. Skand. Växt., Moss. ed.</publication_title>
      <place_in_publication>2, 18. 1937,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranum;species acutifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443676</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bergeri</taxon_name>
    <taxon_name authority="Lindberg &amp; Arnell" date="unknown" rank="variety">acutifolium</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Svenska Vetensk. Acad. Handl., n. s.</publication_title>
      <place_in_publication>23(10): 79. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species bergeri;variety acutifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense to loose tufts, light green to light-brown, dull.</text>
      <biological_entity id="o6924" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light green" name="coloration" notes="" src="d0_s0" to="light-brown" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6925" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o6924" id="r1643" name="in" negation="false" src="d0_s0" to="o6925" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.5–7 cm, generally branched above, somewhat tomentose with reddish-brown rhizoids.</text>
      <biological_entity id="o6926" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="generally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="with rhizoids" constraintid="o6927" is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o6927" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-spreading, slightly curled to ± straight when dry, usually with a few undulations in distal part, (3.5–) 5–8 (–10) mm × 0.5–1 mm, concave below, keeled above, sometimes margins ± involute, lanceolate, narrowly acute to acuminate;</text>
      <biological_entity id="o6928" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="curled" value_original="curled" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="below" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o6929" name="undulation" name_original="undulations" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6930" name="part" name_original="part" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" notes="alterIDs:o6930" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" notes="alterIDs:o6930" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="alterIDs:o6930" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="alterIDs:o6930" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6931" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
      <relation from="o6928" id="r1644" modifier="usually" name="with" negation="false" src="d0_s2" to="o6929" />
      <relation from="o6929" id="r1645" name="in" negation="false" src="d0_s2" to="o6930" />
    </statement>
    <statement id="d0_s3">
      <text>margins serrulate to serrate above;</text>
      <biological_entity id="o6932" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="serrulate" name="architecture_or_shape" src="d0_s3" to="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>laminae 1-stratose or with few 2-stratose regions on or near the distal margins;</text>
      <biological_entity id="o6933" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="with few 2-stratose regions" value_original="with few 2-stratose regions" />
      </biological_entity>
      <biological_entity id="o6934" name="region" name_original="regions" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6935" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <relation from="o6933" id="r1646" name="with" negation="false" src="d0_s4" to="o6934" />
      <relation from="o6934" id="r1647" name="on or near" negation="false" src="d0_s4" to="o6935" />
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent to shortly excurrent, 1/6–1/4 the width of the leaves at base, smooth to slightly papillose above on abaxial surface, abaxial ridges absent, with a row of guide cells, two stereid bands extending to the apex, adaxial epidermal layer of cells not differentiated, the abaxial layer differentiated;</text>
      <biological_entity id="o6936" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s5" to="shortly excurrent" />
        <character char_type="range_value" from="1/6" name="quantity" src="d0_s5" to="1/4" />
      </biological_entity>
      <biological_entity id="o6937" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o6938" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="above " constraintid="o6939" from="smooth" name="width" notes="" src="d0_s5" to="slightly papillose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6939" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o6940" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6941" name="row" name_original="row" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="guide" id="o6942" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o6943" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity id="o6944" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial epidermal" id="o6945" name="layer" name_original="layer" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o6946" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o6947" name="layer" name_original="layer" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o6937" id="r1648" name="at" negation="false" src="d0_s5" to="o6938" />
      <relation from="o6940" id="r1649" name="with" negation="false" src="d0_s5" to="o6941" />
      <relation from="o6941" id="r1650" name="part_of" negation="false" src="d0_s5" to="o6942" />
      <relation from="o6943" id="r1651" name="extending to" negation="false" src="d0_s5" to="o6944" />
      <relation from="o6945" id="r1652" name="part_of" negation="false" src="d0_s5" to="o6946" />
    </statement>
    <statement id="d0_s6">
      <text>cell-walls between lamina cells not or slightly bulging;</text>
      <biological_entity constraint="between cells" constraintid="o6949" id="o6948" name="cell-wall" name_original="cell-walls" src="d0_s6" type="structure" constraint_original="between  cells, ">
        <character is_modifier="false" modifier="slightly" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity constraint="lamina" id="o6949" name="cell" name_original="cells" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>leaf cells smooth to weakly papillose above on abaxial surface;</text>
      <biological_entity constraint="abaxial" id="o6951" name="surface" name_original="surface" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>alar cells 2-stratose, differentiated, often extending to costa;</text>
      <biological_entity constraint="leaf" id="o6950" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="above " constraintid="o6951" from="smooth" name="relief" src="d0_s7" to="weakly papillose" />
      </biological_entity>
      <biological_entity id="o6952" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o6953" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <relation from="o6952" id="r1653" modifier="often" name="extending to" negation="false" src="d0_s8" to="o6953" />
    </statement>
    <statement id="d0_s9">
      <text>proximal laminal cells linear-rectangular, pitted, (22–) 41–60 (–90) × (5–) 6–8 (–12) µm; distal laminal cells short-rectangular, irregularly angled or rounded, not pitted, (7–) 12–20 (–32) × (4–) 8–9 (–14) µm. Sexual condition pseudomonoicous;</text>
      <biological_entity constraint="proximal laminal" id="o6954" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-rectangular" value_original="linear-rectangular" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="22" from_unit="um" name="atypical_length" src="d0_s9" to="41" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s9" to="90" to_unit="um" />
        <character char_type="range_value" from="41" from_unit="um" name="length" src="d0_s9" to="60" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="atypical_width" src="d0_s9" to="6" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s9" to="12" to_unit="um" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s9" to="8" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>dwarf males on stem rhizoids of female plants;</text>
      <biological_entity constraint="distal laminal" id="o6955" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="7" from_unit="um" name="atypical_length" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s9" to="32" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="length" src="d0_s9" to="20" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="atypical_width" src="d0_s9" to="8" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s9" to="14" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s9" to="9" to_unit="um" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="pseudomonoicous" value_original="pseudomonoicous" />
        <character is_modifier="false" name="height" src="d0_s10" value="dwarf" value_original="dwarf" />
      </biological_entity>
      <biological_entity constraint="stem" id="o6956" name="rhizoid" name_original="rhizoids" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="male" value_original="male" />
      </biological_entity>
      <biological_entity id="o6957" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="female" value_original="female" />
      </biological_entity>
      <relation from="o6956" id="r1654" name="part_of" negation="false" src="d0_s10" to="o6957" />
    </statement>
    <statement id="d0_s11">
      <text>interior perichaetial leaves abruptly short-acuminate, convolute-sheathing.</text>
      <biological_entity constraint="interior perichaetial" id="o6958" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s11" value="short-acuminate" value_original="short-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="convolute-sheathing" value_original="convolute-sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 1.2–2.5 cm, solitary, yellow to reddish yellow.</text>
      <biological_entity id="o6959" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s12" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s12" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="reddish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule 2–2.7 mm, arcuate, inclined to horizontal, furrowed when dry, rarely slightly strumose, brown to reddish-brown;</text>
      <biological_entity id="o6960" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="course_or_shape" src="d0_s13" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s13" to="horizontal" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s13" value="furrowed" value_original="furrowed" />
        <character is_modifier="false" modifier="rarely slightly" name="shape" src="d0_s13" value="strumose" value_original="strumose" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum 1.5–2.5 mm.</text>
      <biological_entity id="o6961" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 14–28 µm.</text>
      <biological_entity id="o6962" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s15" to="28" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Somewhat calcareous soil, boulders, rock outcrops and cliff ledges, sometimes humus, rarely rotten logs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous soil" modifier="somewhat" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="cliff ledges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Ont., Que., Yukon; Alaska, Maine, Mont., N.H., N.Mex., N.Y., Wyo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Dicranum acutifolium has been reported from Colorado by W. A. Weber (1973), but has been deleted from the flora (Weber, pers. comm.). It has also been reported from New Mexico, New York, and Wyoming by W. L. Peterson (1979). This arctic-alpine species is recognized by its erect-spreading leaves, slightly curled when dry, keeled and often weakly undulate above, lanceolate, narrowly acute to acuminate, by its percurrent to shortly excurrent costae, smooth to slightly papillose above, and by its leaf cross section that shows only the abaxial row of cells differentiated, a few 2-stratose marginal cells and the cell walls between lamina cells smooth to slightly bulging. Dicranum acutifolium has often been confused with D. fuscescens but the latter species has a longer, rougher subula that in leaf cross section in the distal half reveals very large and obvious papillae, compared to the smaller and less distinct ones in the former, and margins that are almost completely 2-stratose, while those of D. acutifolium are mostly 1-stratose. It has also been confused with 13. D. brevifolium and for distinctions see discussion thereunder.</discussion>
  
</bio:treatment>