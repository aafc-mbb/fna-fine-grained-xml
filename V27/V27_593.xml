<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="mention_page">418</other_info_on_meta>
    <other_info_on_meta type="mention_page">420</other_info_on_meta>
    <other_info_on_meta type="treatment_page">417</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">dicranum</taxon_name>
    <taxon_name authority="(Sullivant &amp; Lesquereux) Lindberg" date="1863" rank="species">viride</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>2: 70. 1863,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranum;species viride</taxon_hierarchy>
    <other_info_on_name type="fna_id">240000059</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Campylopus</taxon_name>
    <taxon_name authority="Sullivant &amp; Lesquereux" date="unknown" rank="species">viridis</taxon_name>
    <place_of_publication>
      <publication_title>in W. S. Sullivant, Musc. Hepat. U.S.,</publication_title>
      <place_in_publication>103. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Campylopus;species viridis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fulvum</taxon_name>
    <taxon_name authority="(Sullivant &amp; Lesquereux) Lindberg" date="unknown" rank="subspecies">viride</taxon_name>
    <taxon_hierarchy>genus Dicranum;species fulvum;subspecies viride;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fulvum</taxon_name>
    <taxon_name authority="(Sullivant &amp; Lesquereux) Frye" date="unknown" rank="variety">viride</taxon_name>
    <taxon_hierarchy>genus Dicranum;species fulvum;variety viride;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose to dense tufts, yellowish to dark green, glossy to dull.</text>
      <biological_entity id="o3370" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" notes="" src="d0_s0" to="dark green" />
        <character char_type="range_value" from="glossy" name="reflectance" src="d0_s0" to="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3371" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o3370" id="r769" name="in" negation="false" src="d0_s0" to="o3371" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.5–4.5 cm, sparsely tomentose with whitish to reddish-brown rhizoids.</text>
      <biological_entity id="o3372" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="4.5" to_unit="cm" />
        <character constraint="with rhizoids" constraintid="o3373" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o3373" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character char_type="range_value" from="whitish" is_modifier="true" name="coloration" src="d0_s1" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-spreading or somewhat falcate-secund, rigid, erect below and flexuose above when dry, smooth, (3–) 4–6 (–7) × 0.5–0.8 mm, most of the leaf tips broken off, concave below to canaliculate above, from a lanceolate base to a long-acuminate, acute apex (when present);</text>
      <biological_entity id="o3374" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" modifier="below" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="above when dry" name="course" src="d0_s2" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s2" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="condition_or_fragility" src="d0_s2" value="broken" value_original="broken" />
        <character constraint="below to canaliculate above" is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o3375" name="tip" name_original="tips" src="d0_s2" type="structure" />
      <biological_entity id="o3376" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o3377" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o3374" id="r770" name="part_of" negation="false" src="d0_s2" to="o3375" />
      <relation from="o3374" id="r771" name="from" negation="false" src="d0_s2" to="o3376" />
    </statement>
    <statement id="d0_s3">
      <text>margins entire, sometimes slightly denticulate at apex;</text>
      <biological_entity id="o3378" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character constraint="at apex" constraintid="o3379" is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o3379" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>laminae 1-stratose or with some 2-stratose regions in patches between margin and costa;</text>
      <biological_entity id="o3380" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character constraint="with regions" constraintid="o3381" is_modifier="false" name="architecture" src="d0_s4" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="with some 2-stratose regions" value_original="with some 2-stratose regions" />
      </biological_entity>
      <biological_entity id="o3381" name="region" name_original="regions" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="between margin and costa" constraintid="o3383-o3384" id="o3382" name="patch" name_original="patches" src="d0_s4" type="structure" constraint_original="between  margin and  costa, " />
      <biological_entity id="o3383" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o3384" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <relation from="o3381" id="r772" name="in" negation="false" src="d0_s4" to="o3382" />
    </statement>
    <statement id="d0_s5">
      <text>costa excurrent, 1/5–1/4 the width of the leaves at base, smooth or slightly rough on abaxial surface in distal half, abaxial ridges absent, with a row of guide cells, two weakly developed stereid bands above and below, not extending above the leaf middle, adaxial and abaxial epidermal layers of cells not differentiated or with a few cells enlarged in both layers;</text>
      <biological_entity id="o3385" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s5" to="1/4" />
        <character is_modifier="false" name="width" notes="" src="d0_s5" value="smooth" value_original="smooth" />
        <character constraint="on abaxial surface" constraintid="o3388" is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s5" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o3386" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o3387" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o3388" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o3389" name="half" name_original="half" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o3390" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3391" name="row" name_original="row" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="guide" id="o3392" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o3393" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity id="o3394" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial and epidermal abaxial" id="o3397" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="middle" value_original="middle" />
        <character is_modifier="true" modifier="not" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="abaxial and adaxial epidermal" id="o3398" name="layer" name_original="layers" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="middle" value_original="middle" />
        <character is_modifier="true" modifier="not" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s5" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <relation from="o3386" id="r773" name="at" negation="false" src="d0_s5" to="o3387" />
      <relation from="o3388" id="r774" name="in" negation="false" src="d0_s5" to="o3389" />
      <relation from="o3390" id="r775" name="with" negation="false" src="d0_s5" to="o3391" />
      <relation from="o3391" id="r776" name="part_of" negation="false" src="d0_s5" to="o3392" />
      <relation from="o3390" id="r777" modifier="weakly" name="developed stereid" negation="false" src="d0_s5" to="o3393" />
      <relation from="o3390" id="r778" modifier="below" name="extending above the" negation="true" src="d0_s5" to="o3394" />
      <relation from="o3390" id="r779" modifier="below" name="extending above the" negation="false" src="d0_s5" to="o3397" />
      <relation from="o3390" id="r780" modifier="below" name="extending above the" negation="false" src="d0_s5" to="o3398" />
    </statement>
    <statement id="d0_s6">
      <text>cell-walls between lamina cells not or weakly bulging;</text>
      <biological_entity constraint="between cells" constraintid="o3400" id="o3399" name="cell-wall" name_original="cell-walls" src="d0_s6" type="structure" constraint_original="between  cells, ">
        <character is_modifier="false" modifier="weakly" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity constraint="lamina" id="o3400" name="cell" name_original="cells" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>leaf cells usually smooth;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells 1-stratose or with a few 2-stratose regions, well-differentiated, often extending to costa;</text>
      <biological_entity constraint="leaf" id="o3401" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o3402" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="with a few 2-stratose regions" />
        <character is_modifier="false" name="variability" notes="" src="d0_s8" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
      <biological_entity id="o3403" name="region" name_original="regions" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o3404" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <relation from="o3402" id="r781" name="with" negation="false" src="d0_s8" to="o3403" />
      <relation from="o3402" id="r782" modifier="often" name="extending to" negation="false" src="d0_s8" to="o3404" />
    </statement>
    <statement id="d0_s9">
      <text>proximal laminal cells rectangular to short-rectangular, not pitted or with few pits, (11–) 33–42 (–51) × (7–) 9–10 (–13) µm; median laminal cells regularly quadrate, not pitted, (9–) 15–22 (–26) × (5–) 7–8 (–11) µm; distal laminal cells small, quadrate, not pitted.</text>
      <biological_entity constraint="proximal laminal" id="o3405" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s9" to="short-rectangular" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="relief" src="d0_s9" value="with few pits" value_original="with few pits" />
      </biological_entity>
      <biological_entity id="o3406" name="pit" name_original="pits" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character char_type="range_value" from="11" from_unit="um" name="atypical_length" notes="alterIDs:o3406" src="d0_s9" to="33" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="42" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o3406" src="d0_s9" to="51" to_unit="um" />
        <character char_type="range_value" from="33" from_unit="um" name="length" notes="alterIDs:o3406" src="d0_s9" to="42" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="atypical_width" notes="alterIDs:o3406" src="d0_s9" to="9" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o3406" src="d0_s9" to="13" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" notes="alterIDs:o3406" src="d0_s9" to="10" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="median laminal" id="o3407" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="regularly" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="9" from_unit="um" name="atypical_length" src="d0_s9" to="15" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s9" to="26" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s9" to="22" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="atypical_width" src="d0_s9" to="7" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s9" to="11" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s9" to="8" to_unit="um" />
      </biological_entity>
      <relation from="o3405" id="r783" name="with" negation="false" src="d0_s9" to="o3406" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="distal laminal" id="o3408" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="small" value_original="small" />
        <character is_modifier="false" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>male plants as large as females;</text>
      <biological_entity id="o3409" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="male" value_original="male" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="female" value_original="female" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>interior perichaetial leaves abruptly long-acuminate, convolute-sheathing.</text>
      <biological_entity constraint="interior perichaetial" id="o3410" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s12" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="convolute-sheathing" value_original="convolute-sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta 1–1.6 cm, solitary, yellow to reddish-brown.</text>
      <biological_entity id="o3411" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s13" to="1.6" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s13" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule 1.5–2.5 mm, straight, erect, smooth, slightly furrowed when dry, brown to reddish-brown;</text>
      <biological_entity id="o3412" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s14" value="furrowed" value_original="furrowed" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s14" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum 1–1.5 mm.</text>
      <biological_entity id="o3413" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 9–22 µm.</text>
      <biological_entity id="o3414" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s16" to="22" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature in spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Commonly growing on the base of trees (usually deciduous but sometimes coniferous, especially Thuja), rotten logs, stumps, rarely soil and acidic or limestone rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="the base" modifier="commonly growing on" constraint="of trees" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="deciduous but coniferous" modifier="usually sometimes" />
        <character name="habitat" value="stumps" modifier=") rotten logs" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="acidic" />
        <character name="habitat" value="limestone rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Alaska, Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., N.H., N.Y., N.C., Ohio, Pa., Tenn., Vt., Va., W.Va., Wis.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion>Dicranum viride has been reported from Alaska by I. A. Worley and Z. Iwatsuki (1970) and from Kentucky by J. A. Snider et al. (1988). It is a not uncommon species of the eastern United States and southeastern Canada. It rarely produces sporophytes and is distinctive because of the fragile, deciduous leaf tips, which probably serve as a means of asexual reproduction. It has on occasion been confused with 22. D. fulvum, which has nearly the same distribution, and rarely with 21. D. fragilifolium where their ranges overlap in the Great Lakes region. For distinctions see the discussions under those species.</discussion>
  
</bio:treatment>