<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="treatment_page">429</other_info_on_meta>
    <other_info_on_meta type="illustration_page">429</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1846" rank="genus">rhabdoweisia</taxon_name>
    <taxon_name authority="(Mitten) H. Jameson" date="1890" rank="species">crenulata</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol.</publication_title>
      <place_in_publication>17: 6. 1890,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus rhabdoweisia;species crenulata</taxon_hierarchy>
    <other_info_on_name type="fna_id">240000091</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Didymodon</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">crenulatus</taxon_name>
    <place_of_publication>
      <publication_title>J. Proc. Linn. Soc., Bot., suppl.</publication_title>
      <place_in_publication>1: 23. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Didymodon;species crenulatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–3 cm.</text>
      <biological_entity id="o4459" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves lingulate, crisped when dry, 2–4 mm, broadly acute to obtuse, margins irregularly dentate distally, distal cells 14–20 µm wide, irregularly hexagonal, rather thin-walled.</text>
      <biological_entity id="o4460" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lingulate" value_original="lingulate" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s1" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
      <biological_entity id="o4461" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly; distally" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4462" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="width" src="d0_s1" to="20" to_unit="um" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s1" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s1" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Seta 3–4 mm.</text>
      <biological_entity id="o4463" name="seta" name_original="seta" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsule 0.6–1 mm;</text>
      <biological_entity id="o4464" name="capsule" name_original="capsule" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>peristome teeth narrowly lanceolate to subulate, smooth to faintly striolate, to 500 µm. Spores 18–20 µm.</text>
      <biological_entity constraint="peristome" id="o4465" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s4" to="subulate" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s4" to="faintly striolate" />
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" src="d0_s4" to="500" to_unit="um" />
      </biological_entity>
      <biological_entity id="o4466" name="spore" name_original="spores" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s4" to="20" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, shaded ledge</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="shaded ledge" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (ca. 1100 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="1100" from_unit="m" constraint="moderate elevations (ca " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.; Europe; Asia; Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>On the North American continent, this species is known from a single locality in the Appalachian Mountains (R. H. Zander 1966).</discussion>
  
</bio:treatment>