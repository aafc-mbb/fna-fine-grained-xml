<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="mention_page">316</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="treatment_page">319</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">archidiaceae</taxon_name>
    <taxon_name authority="Bridel" date="1827" rank="genus">archidium</taxon_name>
    <taxon_name authority="Mitten" date="1864" rank="species">tenerrimum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 17. 1864,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family archidiaceae;genus archidium;species tenerrimum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443515</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Archidium</taxon_name>
    <taxon_name authority="Austin" date="unknown" rank="species">ravenelii</taxon_name>
    <taxon_hierarchy>genus Archidium;species ravenelii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–10 mm, perennial, solitary or in dense mats, green or yellow-green.</text>
      <biological_entity id="o4898" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="in dense mats" value_original="in dense mats" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4899" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o4898" id="r1128" name="in" negation="false" src="d0_s0" to="o4899" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple to more often branched, varying from short to tall and somewhat flexuose, sterile innovations to 10 mm, becoming prostrate with age.</text>
      <biological_entity id="o4900" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="to innovations" constraintid="o4901" is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="with age" constraintid="o4902" is_modifier="false" modifier="becoming" name="growth_form_or_orientation" notes="" src="d0_s1" value="prostrate" value_original="prostrate" />
      </biological_entity>
      <biological_entity id="o4901" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="often" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="true" name="variability" src="d0_s1" value="varying" value_original="varying" />
        <character char_type="range_value" from="short" is_modifier="true" name="height" src="d0_s1" to="tall" />
        <character is_modifier="true" modifier="somewhat" name="course" src="d0_s1" value="flexuose" value_original="flexuose" />
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4902" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves small and distant on short plants to longer, erect-spreading on taller plants, ovate, ovatelanceolate to lanceolate, acuminate to subulate, 0.5-1.5 mm;</text>
      <biological_entity id="o4903" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
        <character constraint="on plants" constraintid="o4904" is_modifier="false" name="arrangement" src="d0_s2" value="distant" value_original="distant" />
        <character constraint="on taller plants" constraintid="o4905" is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="lanceolate acuminate" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="lanceolate acuminate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4904" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="taller" id="o4905" name="plant" name_original="plants" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent to short excurrent;</text>
      <biological_entity id="o4906" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s3" to="short excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>laminal margins plane, smooth or finely serrulate distally;</text>
      <biological_entity constraint="laminal" id="o4907" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely; distally" name="architecture" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>median laminal cells rhomboidal, 3–5: 1, 50–90 × 14–25 µm, becoming somewhat shorter distally, proximal cells rectangular, similar to median cells, cells in alar region short-rectangular to quadrate, 1–2: 1, in 2–3 rows extending from 3–10 cells distally from base along margin;</text>
      <biological_entity constraint="median laminal" id="o4908" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="50" from_unit="um" name="length" src="d0_s5" to="90" to_unit="um" />
        <character char_type="range_value" from="14" from_unit="um" name="width" src="d0_s5" to="25" to_unit="um" />
        <character is_modifier="false" modifier="becoming somewhat; distally" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4909" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="median" id="o4910" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o4911" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4912" name="alar" name_original="alar" src="d0_s5" type="structure">
        <character char_type="range_value" from="short-rectangular" name="shape" src="d0_s5" to="quadrate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity id="o4913" name="region" name_original="region" src="d0_s5" type="structure">
        <character char_type="range_value" from="short-rectangular" name="shape" src="d0_s5" to="quadrate" />
      </biological_entity>
      <biological_entity id="o4914" name="row" name_original="rows" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o4915" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o4916" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o4917" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <relation from="o4909" id="r1129" name="to" negation="false" src="d0_s5" to="o4910" />
      <relation from="o4911" id="r1130" name="in" negation="false" src="d0_s5" to="o4912" />
      <relation from="o4911" id="r1131" name="in" negation="false" src="d0_s5" to="o4913" />
      <relation from="o4911" id="r1132" name="in" negation="false" src="d0_s5" to="o4914" />
      <relation from="o4914" id="r1133" name="extending from" negation="false" src="d0_s5" to="o4915" />
      <relation from="o4915" id="r1134" name="from" negation="false" src="d0_s5" to="o4916" />
      <relation from="o4916" id="r1135" name="along" negation="false" src="d0_s5" to="o4917" />
    </statement>
    <statement id="d0_s6">
      <text>leaves of innovations similar to stem-leaves but smaller, gradually reduced abaxially.</text>
      <biological_entity constraint="median laminal" id="o4918" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity id="o4919" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="gradually; abaxially" name="size" notes="" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4920" name="innovation" name_original="innovations" src="d0_s6" type="structure" />
      <biological_entity id="o4921" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o4919" id="r1136" name="part_of" negation="false" src="d0_s6" to="o4920" />
      <relation from="o4919" id="r1137" name="to" negation="false" src="d0_s6" to="o4921" />
    </statement>
    <statement id="d0_s7">
      <text>Perichaetial leaves variable in shape, erect-spreading to spreading, somewhat concave, sometimes flexuose, ovate to obovate, acute to short-acuminate or ovatelanceolate and long-acuminate, 1–3 mm;</text>
      <biological_entity constraint="perichaetial" id="o4922" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="variable" value_original="variable" />
        <character char_type="range_value" from="erect-spreading" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s7" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s7" value="flexuose" value_original="flexuose" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="obovate acute" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="obovate acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="long-acuminate" value_original="long-acuminate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa percurrent to short-excurrent;</text>
      <biological_entity id="o4923" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s8" to="short-excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminal margins smooth, mostly recurved in longer leaves;</text>
      <biological_entity constraint="laminal" id="o4924" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character constraint="in longer leaves" constraintid="o4925" is_modifier="false" modifier="mostly" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="longer" id="o4925" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>median laminal cells rhomboidal to prosenchymatous, 3–5: 1, 55–130 × 15–35 µm, somewhat shorter distally, proximal cells lax, rectangular, 4–6: 1, 70–200 × 22–32 µm, hyaline in the alar region.</text>
      <biological_entity constraint="median laminal" id="o4926" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="to prosenchymatous" constraintid="o4927" is_modifier="false" name="shape" src="d0_s10" value="rhomboidal" value_original="rhomboidal" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character char_type="range_value" from="55" from_unit="um" name="length" src="d0_s10" to="130" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s10" to="35" to_unit="um" />
        <character is_modifier="false" modifier="somewhat; distally" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4927" name="prosenchymatou" name_original="prosenchymatous" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o4929" name="alar" name_original="alar" src="d0_s10" type="structure" />
      <biological_entity id="o4930" name="region" name_original="region" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition paroicous;</text>
      <biological_entity constraint="proximal" id="o4928" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="6" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character char_type="range_value" from="70" from_unit="um" name="length" src="d0_s10" to="200" to_unit="um" />
        <character char_type="range_value" from="22" from_unit="um" name="width" src="d0_s10" to="32" to_unit="um" />
        <character constraint="in alar, region" constraintid="o4929, o4930" is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="development" src="d0_s11" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="paroicous" value_original="paroicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>antheridia typically naked or rarely 1–2 small bracts present.</text>
      <biological_entity id="o4931" name="antheridium" name_original="antheridia" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="typically" name="architecture" src="d0_s13" value="naked" value_original="naked" />
        <character name="architecture" src="d0_s13" value="rarely" value_original="rarely" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="2" />
      </biological_entity>
      <biological_entity id="o4932" name="bract" name_original="bracts" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="small" value_original="small" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule terminal, sometimes appearing lateral due to rapid innovation growth, 450–800 µm. Spores typically 24 (4–48) per capsule, rounded to polyhedral, 125–225 µm, smooth, pale-yellow to yellow-orange.</text>
      <biological_entity id="o4933" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="450" from_unit="um" modifier="sometimes; sometimes" name="some_measurement" src="d0_s14" to="800" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="innovation" id="o4934" name="growth" name_original="growth" src="d0_s14" type="structure">
        <character is_modifier="true" name="position" src="d0_s14" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o4935" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="24" value_original="24" />
        <character char_type="range_value" constraint="per capsule" constraintid="o4936" from="4" name="atypical_quantity" src="d0_s14" to="48" />
        <character char_type="range_value" from="rounded" name="shape" notes="" src="d0_s14" to="polyhedral" />
        <character char_type="range_value" from="125" from_unit="um" name="some_measurement" src="d0_s14" to="225" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s14" to="yellow-orange" />
      </biological_entity>
      <biological_entity id="o4936" name="capsule" name_original="capsule" src="d0_s14" type="structure" />
      <relation from="o4933" id="r1138" modifier="sometimes" name="appearing" negation="false" src="d0_s14" to="o4934" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late fall to early spring (Oct–Mar).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early spring" from="late fall" />
        <character name="capsules maturing time" char_type="range_value" to="Mar" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Common to uncommon on dry to moist sand, soil, in clearings, along roadsides, creek banks, open forests, pastures, often on soil of limestone and sandstone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="common to uncommon moist" />
        <character name="habitat" value="dry to moist sand" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="clearings" modifier="in" />
        <character name="habitat" value="roadsides" modifier="along" />
        <character name="habitat" value="creek banks" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="soil" modifier="often on" constraint="of limestone and sandstone outcrops" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandstone outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., N.C., S.C., Tex.; Mexico; Europe; n Africa; Atlantic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Archidium tenerrimum is very closely related to A. alternifolium, but is more variable in leaf and stem characters. It can usually be identified by its spreading perichaetial leaves with wider and more lax median lamina cells and the recurved margin. Short, dense and branching forms with broader leaves from drier habitats have been named A. ravenelii. However, these forms seem to intergrade with the taller narrower-leaved forms. More work is needed on the distinction between these two forms, as they tend to have different distributions as well as ecology.</discussion>
  
</bio:treatment>