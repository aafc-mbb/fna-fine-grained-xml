<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="treatment_page">405</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">dicranum</taxon_name>
    <taxon_name authority="Turner" date="1804" rank="species">majus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">majus</taxon_name>
    <taxon_hierarchy>family dicranaceae;genus dicranum;species majus;variety majus</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443666</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose tufts, green to light green, glossy.</text>
      <biological_entity id="o3631" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="light green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3632" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o3631" id="r842" name="in" negation="false" src="d0_s0" to="o3632" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves falcate-secund, flexuose, 8–11.5 (–15) mm;</text>
      <biological_entity id="o3633" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="course" src="d0_s1" value="flexuose" value_original="flexuose" />
        <character char_type="range_value" from="11.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s1" to="11.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins serrate in distal half;</text>
      <biological_entity id="o3634" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="in distal margins" constraintid="o3635" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3635" name="margin" name_original="margins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent to shortly excurrent, toothed distally on abaxial surface, with a double row of guide cells.</text>
      <biological_entity id="o3636" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s3" to="shortly excurrent" />
        <character constraint="on abaxial surface" constraintid="o3637" is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3637" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <biological_entity id="o3638" name="row" name_original="row" src="d0_s3" type="structure" />
      <biological_entity constraint="guide" id="o3639" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <relation from="o3636" id="r843" name="with" negation="false" src="d0_s3" to="o3638" />
      <relation from="o3638" id="r844" name="part_of" negation="false" src="d0_s3" to="o3639" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Humus, soil, soil over rock, and rotten wood in coniferous woods, bogs, and tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humus" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="soil" constraint="over rock" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="rotten wood" constraint="in coniferous woods , bogs , and tundra" />
        <character name="habitat" value="coniferous woods" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Yukon; Alaska, Maine, Mass., R.I., Wash.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6a.</number>
  <discussion>Variety majus is known by its glossy, long (8–15 mm), falcate-secund leaves with serrate margins in the distal half, by its costae with two rows of guide cells (seen in cross section), the abaxial surface toothed and not ridged distally, and by its aggregate setae (2–5 per perichaetium).</discussion>
  
</bio:treatment>