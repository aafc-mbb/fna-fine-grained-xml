<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="treatment_page">363</other_info_on_meta>
    <other_info_on_meta type="illustration_page">361</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1846" rank="genus">arctoa</taxon_name>
    <taxon_name authority="(Gunnerus ex Withering) Bruch &amp; Schimper" date="1846" rank="species">hyperborea</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>1: 157. 1846,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus arctoa;species hyperborea</taxon_hierarchy>
    <other_info_on_name type="fna_id">200000890</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Gunnerus ex Withering" date="unknown" rank="species">hyperboreum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Arr. Brit. Pl. ed.</publication_title>
      <place_in_publication>4, 3: 811. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bryum;species hyperboreum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cynodontium</taxon_name>
    <taxon_name authority="(Gunnerus ex Withering) Hagen" date="unknown" rank="species">hyperboreum</taxon_name>
    <taxon_hierarchy>genus Cynodontium;species hyperboreum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="(Gunnerus ex Withering) Smith" date="unknown" rank="species">hyperboreum</taxon_name>
    <taxon_hierarchy>genus Dicranum;species hyperboreum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in compact, dark green tufts.</text>
      <biological_entity id="o4174" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4175" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="dark green" value_original="dark green" />
      </biological_entity>
      <relation from="o4174" id="r954" name="in" negation="false" src="d0_s0" to="o4175" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 (–5) cm.</text>
      <biological_entity id="o4176" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-spreading, lanceolate, subulate, 2–3 mm;</text>
      <biological_entity id="o4177" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa 30–55 µm wide at base, short-excurrent, rough near tip;</text>
      <biological_entity id="o4178" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o4179" from="30" from_unit="um" name="width" src="d0_s3" to="55" to_unit="um" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="short-excurrent" value_original="short-excurrent" />
        <character constraint="near tip" constraintid="o4180" is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o4179" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o4180" name="tip" name_original="tip" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>distal laminal cells mostly subquadrate (1–2:1), incrassate;</text>
      <biological_entity constraint="distal laminal" id="o4181" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="size" src="d0_s4" value="incrassate" value_original="incrassate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells elongate, alar cells differentiated, quadrate or slightly enlarged.</text>
      <biological_entity constraint="distal laminal" id="o4182" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o4183" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o4184" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s5" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seta 4–6 (–8) mm.</text>
      <biological_entity id="o4185" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule exserted, slightly curved, obscurely to distinctly ribbed when dry, annulus developed, separating;</text>
      <biological_entity id="o4186" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="when dry" name="architecture_or_shape" src="d0_s7" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o4187" name="annulus" name_original="annulus" src="d0_s7" type="structure">
        <character is_modifier="false" name="development" src="d0_s7" value="developed" value_original="developed" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="separating" value_original="separating" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome large, not spreading outward when dry.</text>
      <biological_entity id="o4188" name="peristome" name_original="peristome" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="large" value_original="large" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 18–30 µm.</text>
      <biological_entity id="o4189" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s9" to="30" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Siliceous rock or soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" modifier="siliceous" />
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high-alpine elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Arctoa hyperborea is a rare arctic-alpine moss found on rock ledges or crevices at high elevations. It is distinguished from other species of the genus by its short-excurrent costa, shorter seta (the length is highly variable), and peristome not spreading when dry.</discussion>
  
</bio:treatment>