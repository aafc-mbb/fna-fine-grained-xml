<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">369</other_info_on_meta>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bridel" date="1818" rank="genus">campylopus</taxon_name>
    <taxon_name authority="(Müller Hal.) J.-P. Frahm" date="1997" rank="species">sinensis</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>34: 202. 1997,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus campylopus;species sinensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443579</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">sinense</taxon_name>
    <place_of_publication>
      <publication_title>Nuovo Giorn. Bot. Ital., n. s.</publication_title>
      <place_in_publication>4: 249. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species sinense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Campylopus</taxon_name>
    <taxon_name authority="Brotherus" date="unknown" rank="species">japonicus</taxon_name>
    <taxon_hierarchy>genus Campylopus;species japonicus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranodontium</taxon_name>
    <taxon_name authority="(Müller Hal.) Paris" date="unknown" rank="species">sinense</taxon_name>
    <taxon_hierarchy>genus Dicranodontium;species sinense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 3 cm, in dense tufts, blackish proximally, golden green distally.</text>
      <biological_entity id="o5575" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="coloration" notes="" src="d0_s0" value="blackish" value_original="blackish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s0" value="golden green" value_original="golden green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5576" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o5575" id="r1318" name="in" negation="false" src="d0_s0" to="o5576" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 5–10 mm, the distal ones longest, erect patent when wet, appressed when dry, narrowly lanceolate, long-subulate, ending in a straight, fine, almost entire apex, piliferous at least in the distalmost leaves and plants from exposed habitats, rarely subhyaline;</text>
      <biological_entity id="o5577" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5579" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="true" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
        <character is_modifier="true" modifier="almost" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o5580" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o5581" name="plant" name_original="plants" src="d0_s1" type="structure" />
      <relation from="o5578" id="r1319" name="ending in" negation="false" src="d0_s1" to="o5579" />
    </statement>
    <statement id="d0_s2">
      <text>alar cells reddish-brown, inflated;</text>
      <biological_entity constraint="distal" id="o5578" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when wet" name="orientation" src="d0_s1" value="patent" value_original="patent" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="long-subulate" value_original="long-subulate" />
        <character constraint="in plants" constraintid="o5581" is_modifier="false" name="shape" notes="" src="d0_s1" value="piliferous" value_original="piliferous" />
        <character is_modifier="false" modifier="from exposed habitats; rarely" name="coloration_or_reflectance" notes="" src="d0_s1" value="subhyaline" value_original="subhyaline" />
      </biological_entity>
      <biological_entity id="o5582" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal laminal cells thick-walled, rectangular, narrower at margins, thin-walled in perichaetial leaves;</text>
      <biological_entity constraint="basal laminal" id="o5583" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character constraint="at margins" constraintid="o5584" is_modifier="false" name="width" src="d0_s3" value="narrower" value_original="narrower" />
        <character constraint="in perichaetial, leaves" constraintid="o5585, o5586" is_modifier="false" name="architecture" notes="" src="d0_s3" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o5584" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o5585" name="perichaetial" name_original="perichaetial" src="d0_s3" type="structure" />
      <biological_entity id="o5586" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>distal laminal cells shortly rectangular or oblique, 3–5: 1;</text>
      <biological_entity constraint="distal laminal" id="o5587" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblique" value_original="oblique" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa filling 1/2–3/4 of leaf width, excurrent, in transverse-section showing abaxial groups of stereids and adaxial firm-walled hyalocysts, slightly abaxially ridged.</text>
      <biological_entity constraint="distal laminal" id="o5588" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblique" value_original="oblique" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o5590" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character char_type="range_value" from="1/2" is_modifier="true" name="quantity" src="d0_s5" to="3/4" />
      </biological_entity>
      <biological_entity id="o5591" name="transverse-section" name_original="transverse-section" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o5592" name="group" name_original="groups" src="d0_s5" type="structure" />
      <biological_entity id="o5593" name="stereid" name_original="stereids" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o5594" name="hyalocyst" name_original="hyalocysts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="firm-walled" value_original="firm-walled" />
      </biological_entity>
      <relation from="o5589" id="r1320" name="filling" negation="false" src="d0_s5" to="o5590" />
      <relation from="o5589" id="r1321" name="in" negation="false" src="d0_s5" to="o5591" />
      <relation from="o5591" id="r1322" name="showing" negation="false" src="d0_s5" to="o5592" />
      <relation from="o5589" id="r1323" name="part_of" negation="false" src="d0_s5" to="o5593" />
      <relation from="o5589" id="r1324" name="part_of" negation="false" src="d0_s5" to="o5594" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by deciduous stem tips or deciduous leaves.</text>
      <biological_entity id="o5589" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="slightly abaxially" name="shape" src="d0_s5" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o5595" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character constraint="by" is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o5596" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="true" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o5597" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character is_modifier="true" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sporophytes not known from the flora area.</text>
      <biological_entity id="o5598" name="sporophyte" name_original="sporophytes" src="d0_s7" type="structure" />
      <biological_entity id="o5599" name="flora" name_original="flora" src="d0_s7" type="structure" />
      <biological_entity id="o5600" name="area" name_original="area" src="d0_s7" type="structure" />
      <relation from="o5598" id="r1325" name="known from the" negation="false" src="d0_s7" to="o5599" />
      <relation from="o5598" id="r1326" name="known from the" negation="false" src="d0_s7" to="o5600" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Usually on soil and rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" modifier="usually on" />
        <character name="habitat" value="rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 60 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="60" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Mexico; Asia (China, Japan, Korea, Vietnam); Pacific Islands (Tahiti); Australia (Queensland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Vietnam)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Tahiti)" establishment_means="native" />
        <character name="distribution" value="Australia (Queensland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>In North America north of Mexico Campylopus sinensis has been found only once, in a depauperate condition in a blanket bog in the Queen Charlotte Islands. The species shows a distinct gradient from large to small plants in the tropical to the subtropical or temperate-oceanic parts of its range in East Asia, which seems to be matched also for the North American populations with regard to specimens from Mexico and from British Columbia. It is not evident whether the record from Queen Charlotte Islands is the result of a long distance dispersal or a relict from the Tertiary, as supposed from some other bryophyte species with amphi-Pacific range or disjunct occurrence in East Asia and Mexico. It is also possible that C. sinensis was hitherto overlooked in North America and (as frequently in China) confused with the similar 3. C. atrovirens (for differences see discussion under the latter species).</discussion>
  
</bio:treatment>