<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subfamily">Racomitrioideae</taxon_name>
    <taxon_name authority="(Bednarek-Ochyra) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="genus">niphotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">niphotrichum</taxon_name>
    <taxon_name authority="(Müller Hal.) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="species">panschii</taxon_name>
    <place_of_publication>
      <publication_title>in R. Ochyra et al., Cens. Cat. Polish Mosses,</publication_title>
      <place_in_publication>138. 2003,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily racomitrioideae;genus niphotrichum;section niphotrichum;species panschii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075451</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">panschii</taxon_name>
    <place_of_publication>
      <publication_title>in K. Koldewey et al., Zweite Deutsche Nordpolarfahrt</publication_title>
      <place_in_publication>2(1): 72. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species panschii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized and gracile to fairly large, olivaceous to green in the distal part, brown to blackish brown proximally.</text>
      <biological_entity id="o1218" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character char_type="range_value" from="gracile" name="size" src="d0_s0" to="fairly large" />
        <character char_type="range_value" constraint="in distal part" constraintid="o1219" from="olivaceous" name="coloration" src="d0_s0" to="green" />
        <character char_type="range_value" from="brown" modifier="proximally" name="coloration" notes="" src="d0_s0" to="blackish brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1219" name="part" name_original="part" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems (2–) 4–6 (–11) cm, ascending to erect, mostly unbranched, rarely irregularly sparingly or pinnately branched.</text>
      <biological_entity id="o1220" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="11" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="rarely irregularly; irregularly sparingly; sparingly; pinnately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves imbricate and closely appressed on drying, erect-spreading on wetting, straight, ovatelanceolate to elliptical, 1.9–2.8 × 0.9–1.3 mm;</text>
      <biological_entity id="o1221" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character constraint="on drying" is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character constraint="on wetting" is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="elliptical" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s2" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s2" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins broadly recurved from base to apex;</text>
      <biological_entity id="o1223" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o1224" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <relation from="o1223" id="r295" name="to" negation="false" src="d0_s3" to="o1224" />
    </statement>
    <statement id="d0_s4">
      <text>gradually tapering towards the apex, mostly terminated with a hyaline awn, awn mostly short and broad, less often long and narrow, slightly flexuose, irregularly denticulate and spinulose, papillose proximally, with narrow but fairly tall and irregularly arranged papillae on the abaxial side, epapillose or nearly so in the distal part;</text>
      <biological_entity id="o1222" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o1223" is_modifier="false" modifier="broadly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character constraint="towards apex" constraintid="o1225" is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o1225" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o1226" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o1227" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly; mostly" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="less often" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s4" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="proximally" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o1228" name="papilla" name_original="papillae" src="d0_s4" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="true" modifier="fairly" name="height" src="d0_s4" value="tall" value_original="tall" />
        <character is_modifier="true" modifier="irregularly" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1229" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o1230" name="part" name_original="part" src="d0_s4" type="structure" />
      <relation from="o1227" id="r296" name="with" negation="false" src="d0_s4" to="o1228" />
      <relation from="o1228" id="r297" name="on" negation="false" src="d0_s4" to="o1229" />
      <relation from="o1227" id="r298" modifier="nearly" name="in" negation="false" src="d0_s4" to="o1230" />
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells elongate, 40–70 × 5–7 µm, epapillose or nearly so in 2–4 cell rows at the insertion, with thick and nodulose longitudinal walls;</text>
      <biological_entity constraint="cell" id="o1232" name="row" name_original="rows" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o1233" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="true" name="shape" src="d0_s5" value="nodulose" value_original="nodulose" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s5" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o1231" id="r299" modifier="nearly" name="in" negation="false" src="d0_s5" to="o1232" />
      <relation from="o1231" id="r300" name="with" negation="false" src="d0_s5" to="o1233" />
    </statement>
    <statement id="d0_s6">
      <text>alar cells hyaline, forming small to medium-sized, rounded and somewhat inflated group of 2–4 cell rows;</text>
      <biological_entity constraint="basal laminal" id="o1231" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s5" to="70" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s5" to="7" to_unit="um" />
      </biological_entity>
      <biological_entity id="o1235" name="group" name_original="group" src="d0_s6" type="structure">
        <character char_type="range_value" from="small" is_modifier="true" name="size" src="d0_s6" to="medium-sized" />
        <character is_modifier="true" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="true" modifier="somewhat" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="cell" id="o1236" name="row" name_original="rows" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <relation from="o1234" id="r301" name="forming" negation="false" src="d0_s6" to="o1235" />
      <relation from="o1234" id="r302" name="consist_of" negation="false" src="d0_s6" to="o1236" />
    </statement>
    <statement id="d0_s7">
      <text>supra-alar cells not sinuose, thick-walled, forming a pellucid marginal border, consisting of to 10 (–20) cells;</text>
      <biological_entity id="o1234" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o1237" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o1238" name="border" name_original="border" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <biological_entity id="o1239" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="20" />
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <relation from="o1237" id="r303" name="forming a" negation="false" src="d0_s7" to="o1238" />
      <relation from="o1237" id="r304" name="consisting of" negation="false" src="d0_s7" to="o1239" />
    </statement>
    <statement id="d0_s8">
      <text>medial and distal laminal cells rectangular, 15–25 × 7–12 µm, distinctly papillose.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sterile.</text>
      <biological_entity constraint="medial and distal laminal" id="o1240" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s8" to="25" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s8" to="12" to_unit="um" />
        <character is_modifier="false" modifier="distinctly" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry and exposed or moist to wet sites, acidic or seldom calciferous soil and rocky ground, between boulders, on scree, talus and rocky seepage slopes, cliffs and ledges, late snow beds and various communities of polar tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry and exposed or moist to wet sites" />
        <character name="habitat" value="acidic" />
        <character name="habitat" value="calciferous soil" modifier="seldom" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="boulders" modifier="between" />
        <character name="habitat" value="scree" modifier="on" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="rocky seepage slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="late snow beds" />
        <character name="habitat" value="various communities" />
        <character name="habitat" value="polar tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr. (Labr.); Nunavut, Que., Yukon; Alaska; Europe (Spitsbergen); Arctic and temperate Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Europe (Spitsbergen)" establishment_means="native" />
        <character name="distribution" value="Arctic and temperate Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Niphotrichum panschii is fairly frequent in the high Arctic of Alaska, the Yukon Territory, and Nunavut, as well as in Greenland (except for the southernmost part), only occasionally southwards to latitude ca. 55º N in Labrador. It is closely related to N. canescens, but the possibility of confusion with the typical subspecies of the latter is rather minimal since that is a temperate taxon and its range does not overlap that of N. panschii. The taxa have a similar leaf shape, although N. panschii always has straight leaves that are not hyaline in the distal part and it is less robust than subsp. canescens. Niphotrichum panschii is externally more similar to N. canescens subsp. latifolium, and they are more likely to be confused, especially because they often grow together. However, they can be recognized by their awns, which in N. panschii are less papillose but more strongly spinulose and serrate at the apex, relatively short, less flexuose, and directed upwards. In contrast, the awns of subsp. latifolium are strongly papillose throughout and only faintly serrulate, long, distinctly flexuose, and consequently spreading from the shoot. The stems of N. panschii are sparingly branched to almost unbranched, and the branches are usually erect and fragile, whereas those of subsp. latifolium are more profusely branched, and the branches are non-fragile.</discussion>
  
</bio:treatment>