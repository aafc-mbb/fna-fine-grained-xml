<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="mention_page">452</other_info_on_meta>
    <other_info_on_meta type="mention_page">456</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="treatment_page">457</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Limpricht" date="unknown" rank="family">ditrichaceae</taxon_name>
    <taxon_name authority="Hampe" date="1867" rank="genus">ditrichum</taxon_name>
    <taxon_name authority="Grout" date="1927" rank="species">tortuloides</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>30: 4. 1927,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ditrichaceae;genus ditrichum;species tortuloides</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443749</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green to yellowish green, becoming brown with age, dull, in loose to somewhat dense tufts.</text>
      <biological_entity id="o2554" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="yellowish green" />
        <character constraint="with age" constraintid="o2555" is_modifier="false" modifier="becoming" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2555" name="age" name_original="age" src="d0_s0" type="structure" />
      <biological_entity id="o2556" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" modifier="somewhat" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o2554" id="r612" name="in" negation="false" src="d0_s0" to="o2556" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–5 mm, simple or seldom branched, with a few dark red rhizoids near the base.</text>
      <biological_entity id="o2557" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="seldom" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o2558" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark red" value_original="dark red" />
      </biological_entity>
      <biological_entity id="o2559" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o2557" id="r613" name="with" negation="false" src="d0_s1" to="o2558" />
      <relation from="o2558" id="r614" name="near" negation="false" src="d0_s1" to="o2559" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-spreading, slightly crisped to falcate-secund when dry, 1–3 mm, lanceolate to linear-lanceolate, channelled, lamina 1-stratose except near margins;</text>
      <biological_entity id="o2560" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o2561" name="lamina" name_original="lamina" src="d0_s2" type="structure">
        <character constraint="except margins" constraintid="o2562" is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o2562" name="margin" name_original="margins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins plane to narrowly recurved from just beyond the leaf base to mid leaf, serrate to strongly serrate from mid leaf to the acute apex, 2-stratose distally and rarely a cell inward;</text>
      <biological_entity id="o2563" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character constraint="just beyond leaf base" constraintid="o2564" is_modifier="false" modifier="narrowly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character char_type="range_value" constraint="from mid leaf" constraintid="o2566" from="serrate" name="architecture_or_shape" notes="" src="d0_s3" to="strongly serrate" />
        <character is_modifier="false" modifier="distally" name="architecture" notes="" src="d0_s3" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o2564" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o2565" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o2566" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o2567" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o2568" name="cell" name_original="cell" src="d0_s3" type="structure" />
      <relation from="o2564" id="r615" name="to" negation="false" src="d0_s3" to="o2565" />
      <relation from="o2566" id="r616" name="to" negation="false" src="d0_s3" to="o2567" />
    </statement>
    <statement id="d0_s4">
      <text>costa distinct, percurrent, occupying 1/6–1/3 width of the leaf base;</text>
      <biological_entity id="o2569" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="percurrent" value_original="percurrent" />
        <character char_type="range_value" constraint="of leaf base" constraintid="o2570" from="1/6" modifier="occupying" name="quantity" src="d0_s4" to="1/3" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o2570" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="character" src="d0_s4" value="width" value_original="width" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lamina cells thick-walled, distal cells 8–24 × 8 µm, becoming slightly broader and longer in the base.</text>
      <biological_entity constraint="lamina" id="o2571" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o2573" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction unknown.</text>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="distal" id="o2572" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="length" src="d0_s5" to="24" to_unit="um" />
        <character name="width" src="d0_s5" unit="um" value="8" value_original="8" />
        <character is_modifier="false" modifier="becoming slightly" name="width" src="d0_s5" value="broader" value_original="broader" />
        <character constraint="in base" constraintid="o2573" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta orange-yellow, brownish or reddish, 0.8–1.2 cm, erect.</text>
      <biological_entity id="o2574" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule erect, yellow or light-brown, curved and asymmetric, 1–2 (–2.5) mm, often swollen at the base;</text>
      <biological_entity id="o2575" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character constraint="at base" constraintid="o2576" is_modifier="false" modifier="often" name="shape" src="d0_s9" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o2576" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>operculum rostrate, 0.5–0.8 mm;</text>
      <biological_entity id="o2577" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rostrate" value_original="rostrate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>peristome 200 µm, 2-fid, the filaments ± unequal, linear, somewhat twisted when dry, strongly papillose to spiculose.</text>
      <biological_entity id="o2578" name="peristome" name_original="peristome" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="um" value="200" value_original="200" />
        <character is_modifier="false" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o2579" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s11" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="spiculose" value_original="spiculose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores round, 9–13 µm, appearing smooth to minutely papillose.</text>
      <biological_entity id="o2580" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="round" value_original="round" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s12" to="13" to_unit="um" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s12" to="minutely papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring (Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
        <character name="capsules maturing time" char_type="atypical_range" to="Jun" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Primarily on serpentine soils, clearings along roads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine soils" modifier="primarily on" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="roads" modifier="along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (ca. 400 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="400" from_unit="m" constraint="moderate elevations (ca " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md., N.H., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Ditrichum tortuloides was synonymized by H. A. Crum and L. E. Anderson (1981) with D. ambiguum. However, R. R. Ireland and H. Robinson (2001) discussed reasons for distinguishing the two species. For a discussion of the similarities and distinguishing characteristics, see under 1. D. ambiguum.</discussion>
  
</bio:treatment>