<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="mention_page">589</other_info_on_meta>
    <other_info_on_meta type="treatment_page">598</other_info_on_meta>
    <other_info_on_meta type="illustration_page">597</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">tortula</taxon_name>
    <taxon_name authority="R. H. Zander" date="1993" rank="species">modica</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Buffalo Soc. Nat. Sci.</publication_title>
      <place_in_publication>32: 226. 1993,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus tortula;species modica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065183</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnostomum</taxon_name>
    <taxon_name authority="Turner" date="unknown" rank="species">intermedium</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Hibern. Spic.,</publication_title>
      <place_in_publication>7, plate 1, figs. a–c. 1801,</place_in_publication>
      <other_info_on_pub>not Tortula intermedia (Bridel) De Notaris 1838</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Gymnostomum;species intermedium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pottia</taxon_name>
    <taxon_name authority="(Turner) Fürnrohr" date="unknown" rank="species">intermedia</taxon_name>
    <taxon_hierarchy>genus Pottia;species intermedia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pottia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">truncata</taxon_name>
    <taxon_name authority="(F. Weber &amp; D. Mohr) Bruch &amp; Schimper" date="unknown" rank="variety">major</taxon_name>
    <taxon_hierarchy>genus Pottia;species truncata;variety major;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves obovate to spatulate, apex broadly acute or occasionally rounded, short-awned, margins weakly recurved proximally or occasionally plane, weakly bordered distally with 2–4 rows of slightly thicker-walled cells;</text>
      <biological_entity id="o9144" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s0" to="spatulate" />
      </biological_entity>
      <biological_entity id="o9145" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s0" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="short-awned" value_original="short-awned" />
      </biological_entity>
      <biological_entity id="o9146" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="weakly; proximally" name="orientation" src="d0_s0" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="occasionally; occasionally" name="shape" src="d0_s0" value="plane" value_original="plane" />
        <character constraint="with rows" constraintid="o9147" is_modifier="false" modifier="weakly" name="architecture" src="d0_s0" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o9147" name="row" name_original="rows" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s0" to="4" />
      </biological_entity>
      <biological_entity id="o9148" name="cell" name_original="cells" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="slightly" name="architecture" src="d0_s0" value="thicker-walled" value_original="thicker-walled" />
      </biological_entity>
      <relation from="o9147" id="r2171" name="part_of" negation="false" src="d0_s0" to="o9148" />
    </statement>
    <statement id="d0_s1">
      <text>costa excurrent, lacking an adaxial pad of cells, distally narrow, 2–3 cells across the convex adaxial surface;</text>
      <biological_entity id="o9149" name="costa" name_original="costa" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9150" name="pad" name_original="pad" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="distally" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
      <biological_entity id="o9151" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity id="o9152" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity constraint="adaxial" id="o9153" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="convex" value_original="convex" />
      </biological_entity>
      <relation from="o9150" id="r2172" name="part_of" negation="false" src="d0_s1" to="o9151" />
      <relation from="o9152" id="r2173" name="across" negation="false" src="d0_s1" to="o9153" />
    </statement>
    <statement id="d0_s2">
      <text>distal laminal cells irregularly hexagonal or sometimes rectangular, width 20–25 (–29) µm wide, 1 (–2):1, smooth or 1-papillose (best seen in section).</text>
    </statement>
    <statement id="d0_s3">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o9154" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s2" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" modifier="sometimes; sometimes" name="shape" src="d0_s2" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="um" name="width" src="d0_s2" to="29" to_unit="um" />
        <character char_type="range_value" from="20" from_unit="um" name="width" src="d0_s2" to="25" to_unit="um" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="2" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s2" value="1-papillose" value_original="1-papillose" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporophytes exerted.</text>
      <biological_entity id="o9155" name="sporophyte" name_original="sporophytes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Seta 0.4–0.6 cm.</text>
      <biological_entity id="o9156" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s5" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule stegocarpic, not systylius, short-cylindric, erect and nearly straight, urn 1–1.5 mm;</text>
      <biological_entity id="o9157" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="systylius" value_original="systylius" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o9158" name="urn" name_original="urn" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peristome absent or rudimentary;</text>
      <biological_entity id="o9159" name="peristome" name_original="peristome" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>operculum ca. 0.5 mm.</text>
      <biological_entity id="o9160" name="operculum" name_original="operculum" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 30–35 µm, spheric, densely papillose.</text>
      <biological_entity id="o9161" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s9" to="35" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature fall–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous soil, old fields, pastures, rocky areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous soil" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="rocky areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low and moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; N.Y., Pa.; Europe; Asia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>A report of Tortula modica from Mexico was excluded by R. H. Zander (1993). The distribution and characters of this uncommon species were discussed by H. A. Crum and L. E. Anderson (1981), P. M. Eckel (1987), and C. Williams (1966b). Essentially, Tortula modica is a high polyploid (n = 52) taxon distinguished from T. truncata by the larger habit, leaf margins usually recurved proximally, capsules slightly longer, short-cylindric, not tapering to the base, and peristome sometimes present though rudimentary.</discussion>
  
</bio:treatment>