<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">655</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kindberg" date="unknown" rank="family">calymperaceae</taxon_name>
    <taxon_name authority="Schwägrichen" date="1824" rank="genus">SYRRHOPODON</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>2(12): 110. 1824  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calymperaceae;genus SYRRHOPODON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek syrrepo, to close the eye, and odon, tooth, alluding to narrow, connivent, horizontal peristome teeth of some species closing capsule mouth upon drying</other_info_on_name>
    <other_info_on_name type="fna_id">132151</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, gregarious to tufted or caespitose, green to yellowish-brown, sometimes with pink tinge.</text>
      <biological_entity id="o3120" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="caespitose" value_original="caespitose" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="yellowish-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o3121" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sometimes dimorphic with vegetative leaves mostly grading into gemmiferous leaves, teniolae absent (teniola-like features rarely present in a few taxa);</text>
      <biological_entity id="o3122" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="with leaves" constraintid="o3123" is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s2" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o3123" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o3124" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="gemmiferous" value_original="gemmiferous" />
      </biological_entity>
      <biological_entity id="o3125" name="teniola" name_original="teniolae" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o3123" id="r747" name="into" negation="false" src="d0_s2" to="o3124" />
    </statement>
    <statement id="d0_s3">
      <text>margins of distal lamina mostly thickened and toothed, rarely 1-stratose, in many taxa regularly bordered entirely or in part with elongate hyaline cells;</text>
      <biological_entity id="o3126" name="margin" name_original="margins" src="d0_s3" type="structure" constraint="lamina" constraint_original="lamina; lamina">
        <character is_modifier="false" modifier="mostly" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3127" name="lamina" name_original="lamina" src="d0_s3" type="structure" />
      <biological_entity id="o3128" name="taxon" name_original="taxa" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="many" value_original="many" />
        <character is_modifier="false" modifier="regularly; entirely" name="architecture" src="d0_s3" value="bordered" value_original="bordered" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="in part" value_original="in part" />
      </biological_entity>
      <biological_entity id="o3129" name="part" name_original="part" src="d0_s3" type="structure" />
      <biological_entity id="o3130" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o3126" id="r748" name="part_of" negation="false" src="d0_s3" to="o3127" />
      <relation from="o3126" id="r749" name="in" negation="false" src="d0_s3" to="o3128" />
      <relation from="o3126" id="r750" name="in" negation="false" src="d0_s3" to="o3129" />
      <relation from="o3129" id="r751" name="with" negation="false" src="d0_s3" to="o3130" />
    </statement>
    <statement id="d0_s4">
      <text>costa showing two bands of stereids in cross-section;</text>
      <biological_entity id="o3131" name="costa" name_original="costa" src="d0_s4" type="structure" constraint="stereid; stereid" />
      <biological_entity id="o3132" name="band" name_original="bands" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o3133" name="stereid" name_original="stereids" src="d0_s4" type="structure" />
      <biological_entity id="o3134" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <relation from="o3131" id="r752" name="showing" negation="false" src="d0_s4" to="o3132" />
      <relation from="o3131" id="r753" name="part_of" negation="false" src="d0_s4" to="o3133" />
      <relation from="o3131" id="r754" name="in" negation="false" src="d0_s4" to="o3134" />
    </statement>
    <statement id="d0_s5">
      <text>medial leaf cells isodiametric.</text>
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by gemmae common on often modified leaves, borne on apices or adaxially and sometimes abaxially along costa of distal lamina, clavate-fusiform or filamentous.</text>
      <biological_entity id="o3136" name="gemma" name_original="gemmae" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character constraint="on leaves" constraintid="o3137" is_modifier="false" name="quantity" src="d0_s6" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o3137" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="often" name="development" src="d0_s6" value="modified" value_original="modified" />
      </biological_entity>
      <biological_entity id="o3138" name="apex" name_original="apices" src="d0_s6" type="structure" />
      <biological_entity id="o3139" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="clavate-fusiform" value_original="clavate-fusiform" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3140" name="lamina" name_original="lamina" src="d0_s6" type="structure" />
      <relation from="o3137" id="r755" name="borne on" negation="false" src="d0_s6" to="o3138" />
      <relation from="o3139" id="r756" name="part_of" negation="false" src="d0_s6" to="o3140" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition mostly dioicous, rarely apparently monoicous.</text>
      <biological_entity constraint="leaf" id="o3135" name="cell" name_original="cells" src="d0_s5" type="structure" constraint_original="medial leaf">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="texture" src="d0_s6" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" modifier="mostly" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" modifier="rarely apparently" name="reproduction" src="d0_s7" value="monoicous" value_original="monoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta single, rarely 2–3 per perichaetium.</text>
      <biological_entity id="o3141" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="single" value_original="single" />
        <character char_type="range_value" constraint="per perichaetium" constraintid="o3142" from="2" modifier="rarely" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o3142" name="perichaetium" name_original="perichaetium" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Capsule mostly exserted, sometimes immersed, mostly cylindric;</text>
      <biological_entity id="o3143" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s9" value="immersed" value_original="immersed" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome present or absent.</text>
      <biological_entity id="o3144" name="peristome" name_original="peristome" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Calyptra deciduous, mostly cucullate, rarely conic-mitrate.</text>
      <biological_entity id="o3145" name="calyptra" name_original="calyptra" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s11" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="conic-mitrate" value_original="conic-mitrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide except Antarctica, mostly tropical and subtropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide except Antarctica" establishment_means="native" />
        <character name="distribution" value="mostly tropical and subtropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species ca. 80–90 (6 in the flora).</discussion>
  <discussion>Syrrhopodon is characterized by its erect stems, leaves bordered with hyaline cells or with thickened and toothed margins, cancellinae, and mostly cucullate calyptra. It differs consistently from Calymperes in the latter character. Gemmae are not as uniformly present in species of Syrrhopodon as in Calymperes.</discussion>
  <references>
    <reference>Reese, W. D. 1987. World ranges, implications for patterns of historical dispersal and speciation, and comments on phylogeny of Syrrhopodon (Calymperaceae). Mem. New York Bot. Gard. 45: 426–445.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves without border of elongate hyaline cells.</description>
      <determination>6 Syrrhopodon incompletus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves bordered entirely or in part with elongate hyaline cells</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves regularly conspicuously toothed at shoulders.</description>
      <determination>1 Syrrhopodon texanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves entire or irregularly and inconspicuously toothed at shoulders</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves completely bordered all around with hyaline cells</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves incompletely bordered; hyaline cells absent on portions of the margins</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal lamina more or less crispate when dry, folded when moist.</description>
      <determination>2 Syrrhopodon gaudichaudii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal lamina straight or somewhat contorted when dry but never crispate, plane when moist.</description>
      <determination>3 Syrrhopodon prolifer</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Distal lamina ligulate; cancellinae rounded distally.</description>
      <determination>4 Syrrhopodon ligulatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Distal lamina acuminate; cancellinae narrowly acute.</description>
      <determination>5 Syrrhopodon parasiticus</determination>
    </key_statement>
  </key>
</bio:treatment>