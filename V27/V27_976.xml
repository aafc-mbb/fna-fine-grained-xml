<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">662</other_info_on_meta>
    <other_info_on_meta type="treatment_page">660</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kindberg" date="unknown" rank="family">calymperaceae</taxon_name>
    <taxon_name authority="Swartz in F. Weber" date="1814" rank="genus">calymperes</taxon_name>
    <taxon_name authority="Müller Hal." date="1848" rank="species">erosum</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>21: 182. 1848,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calymperaceae;genus calymperes;species erosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002014</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calymperes</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">emersum</taxon_name>
    <taxon_hierarchy>genus Calymperes;species emersum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants gregarious or tufted, green to brownish, often somewhat uncinate-curved at tips when dry, to 5 mm.</text>
      <biological_entity id="o1083" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="brownish" />
        <character constraint="at tips" constraintid="o1084" is_modifier="false" modifier="often somewhat" name="course" src="d0_s0" value="uncinate-curved" value_original="uncinate-curved" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1084" name="tip" name_original="tips" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" modifier="when dry" name="some_measurement" src="d0_s0" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves dimorphic, the vegetative 2–3 mm;</text>
      <biological_entity id="o1085" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="vegetative" value_original="vegetative" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>distal lamina broadly to narrowly lanceolate;</text>
      <biological_entity constraint="distal" id="o1086" name="lamina" name_original="lamina" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins somewhat thickened and toothed distally;</text>
      <biological_entity id="o1087" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa in cross-section showing ad and abxaxial bands of stereid cells;</text>
      <biological_entity id="o1088" name="costa" name_original="costa" src="d0_s4" type="structure" constraint="cell" constraint_original="cell; cell" />
      <biological_entity id="o1089" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity id="o1090" name="band" name_original="bands" src="d0_s4" type="structure" />
      <biological_entity id="o1091" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <relation from="o1088" id="r258" name="in" negation="false" src="d0_s4" to="o1089" />
      <relation from="o1089" id="r259" name="showing" negation="false" src="d0_s4" to="o1090" />
      <relation from="o1088" id="r260" name="part_of" negation="false" src="d0_s4" to="o1091" />
    </statement>
    <statement id="d0_s5">
      <text>medial cells obscure or distinct, 5–6 µm, minutely papillose ad and abaxially;</text>
      <biological_entity constraint="medial" id="o1092" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="5" from_unit="um" name="some_measurement" src="d0_s5" to="6" to_unit="um" />
        <character is_modifier="false" modifier="minutely; abaxially" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>teniolae usually distinct at leaf shoulders and beyond but sometimes weak or interrupted;</text>
      <biological_entity id="o1093" name="teniola" name_original="teniolae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes" name="fragility" src="d0_s6" value="weak" value_original="weak" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cancellinae usually broadly rounded distally, adaxial distal cells distinctly mammillose;</text>
      <biological_entity id="o1094" name="cancellina" name_original="cancellinae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually broadly; distally" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="adaxial distal" id="o1095" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="relief" src="d0_s7" value="mammillose" value_original="mammillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>gemmiferous leaves with apex of costa excurrent, bearing gemmae all around on apex of costa.</text>
      <biological_entity id="o1096" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="gemmiferous" value_original="gemmiferous" />
      </biological_entity>
      <biological_entity id="o1097" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o1098" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity id="o1099" name="gemma" name_original="gemmae" src="d0_s8" type="structure" />
      <biological_entity id="o1100" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o1101" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <relation from="o1096" id="r261" name="with" negation="false" src="d0_s8" to="o1097" />
      <relation from="o1097" id="r262" name="part_of" negation="false" src="d0_s8" to="o1098" />
      <relation from="o1096" id="r263" name="bearing" negation="false" src="d0_s8" to="o1099" />
      <relation from="o1096" id="r264" modifier="around" name="on" negation="false" src="d0_s8" to="o1100" />
      <relation from="o1100" id="r265" name="part_of" negation="false" src="d0_s8" to="o1101" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Not producing sporophytes in the flora area. Tree bark, bases, and logs, hammocks and forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sporophytes" modifier="not producing" constraint="in the flora area" />
        <character name="habitat" value="the flora area" />
        <character name="habitat" value="tree bark" />
        <character name="habitat" value="bases" />
        <character name="habitat" value="hammocks" modifier="and logs" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations (0 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" constraint="low elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America; Europe; Asia; Africa; Atlantic Islands; Indian Ocean Islands; Pacific Islands; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Calymperes erosum occurs rarely in peninsular Florida, but it is common in other parts of its range outside of the flora area. It may superficially resemble C. afzelii but can be easily distinguished by the mammillose distal adaxial cells of its cancellinae, and by bearing gemmae all around on the excurrent apex of the costa of gemmiferous leaves. The distal ends of the cancellinae are usually rounded, and the plants often have a pinkish tinge.</discussion>
  
</bio:treatment>