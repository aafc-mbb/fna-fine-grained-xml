<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">625</other_info_on_meta>
    <other_info_on_meta type="mention_page">627</other_info_on_meta>
    <other_info_on_meta type="treatment_page">624</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="genus">syntrichia</taxon_name>
    <taxon_name authority="(De Notaris) Mitten" date="1859" rank="species">princeps</taxon_name>
    <place_of_publication>
      <publication_title>J. Proc. Linn. Soc., Bot., suppl.</publication_title>
      <place_in_publication>1: 39. 1859,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus syntrichia;species princeps</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002109</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="De Notaris" date="unknown" rank="species">princeps</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Reale Accad. Sci. Torino</publication_title>
      <place_in_publication>40: 288. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tortula;species princeps;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–20 mm.</text>
      <biological_entity id="o7076" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s0" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually in distinct whorls, infolded, somewhat contorted, and weakly to strongly twisted around the stem when dry, widespreading to slightly recurved when moist, concave, spatulate, 2–4 × 1–1.5 mm;</text>
      <biological_entity id="o7077" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s1" value="infolded" value_original="infolded" />
        <character is_modifier="false" modifier="somewhat" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character constraint="around stem" constraintid="o7079" is_modifier="false" modifier="weakly to strongly" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o7078" name="whorl" name_original="whorls" src="d0_s1" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o7079" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character char_type="range_value" from="widespreading" modifier="when moist" name="orientation" src="d0_s1" to="slightly recurved" />
        <character is_modifier="false" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s1" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o7077" id="r1687" name="in" negation="false" src="d0_s1" to="o7078" />
    </statement>
    <statement id="d0_s2">
      <text>margins revolute in the proximal 1/2–3/4, entire;</text>
      <biological_entity id="o7080" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="in proximal 1/2-3/4" constraintid="o7081" is_modifier="false" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7081" name="1/2-3/4" name_original="1/2-3/4" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apices acute or sometimes truncate;</text>
      <biological_entity id="o7082" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa excurrent into a long, serrate, hyaline awn (reddish at base), often strongly papillose abaxially and serrulate near the apex because of projecting cell ends, red;</text>
      <biological_entity id="o7083" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="into awn" constraintid="o7084" is_modifier="false" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="often strongly; abaxially" name="relief" notes="" src="d0_s4" value="papillose" value_original="papillose" />
        <character constraint="near apex" constraintid="o7085" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o7084" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o7085" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity constraint="cell" id="o7086" name="end" name_original="ends" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="projecting" value_original="projecting" />
      </biological_entity>
      <relation from="o7085" id="r1688" name="because of" negation="false" src="d0_s4" to="o7086" />
    </statement>
    <statement id="d0_s5">
      <text>basal-cells abruptly differentiated, long-rectangular, 45–80 × 20–30 µm, shortrectangular to quadrate at the margins;</text>
      <biological_entity id="o7087" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s5" value="long-rectangular" value_original="long-rectangular" />
        <character char_type="range_value" from="45" from_unit="um" name="length" src="d0_s5" to="80" to_unit="um" />
        <character char_type="range_value" from="20" from_unit="um" name="width" src="d0_s5" to="30" to_unit="um" />
        <character constraint="at margins" constraintid="o7088" is_modifier="false" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o7088" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal cells quadrate to hexagonal, 12–17 µm, slightly bulging, bearing 4–6 papillae per cell.</text>
      <biological_entity id="o7090" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o7091" name="cell" name_original="cell" src="d0_s6" type="structure" />
      <relation from="o7089" id="r1689" name="bearing" negation="false" src="d0_s6" to="o7090" />
      <relation from="o7089" id="r1690" name="per" negation="false" src="d0_s6" to="o7091" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition synoicous (apparently rarely dioicous).</text>
      <biological_entity constraint="distal" id="o7089" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s6" to="hexagonal" />
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s6" to="17" to_unit="um" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta red, 10–18 mm.</text>
      <biological_entity id="o7092" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule brownish red, 3–4 mm, slightly curved, with a distinct neck;</text>
      <biological_entity id="o7093" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish red" value_original="brownish red" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s10" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o7094" name="neck" name_original="neck" src="d0_s10" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o7093" id="r1691" name="with" negation="false" src="d0_s10" to="o7094" />
    </statement>
    <statement id="d0_s11">
      <text>operculum 1.5–2 mm, brown;</text>
      <biological_entity id="o7095" name="operculum" name_original="operculum" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome ca. 1.5 mm, the distal divisions twisted about 2 turns, red, the basal membrane white, 1/2–2/3 the total length.</text>
      <biological_entity id="o7096" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7097" name="division" name_original="divisions" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="twisted" value_original="twisted" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7098" name="membrane" name_original="membrane" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="1/2" name="length" src="d0_s12" to="2/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 9–13 µm, papillose.</text>
      <biological_entity id="o7099" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s13" to="13" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Humus, soil, rock, tree bark</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humus" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="tree bark" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Idaho, Mont., Nev., Oreg., Utah, Wash.; Mexico; w, s South America; Europe; w Asia; Africa; Pacific Islands (Hawaii, New Zealand); Australia; Antarctica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="w Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="Antarctica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>The synoicous condition of Syntrichia princeps is diagnostic if present, but otherwise one must rely on wider basal leaf cells, costal hydroids, and the stem central strand to separate this species from S. ruralis, S. papillosissima, and S. norvegica. The more acute leaves with cells generally smaller, and costa reddish and serrulate separate it from S. obtusissima.</discussion>
  
</bio:treatment>