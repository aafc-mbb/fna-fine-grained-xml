<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="treatment_page">239</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">grimmia</taxon_name>
    <taxon_name authority="(Hampe) Schimper" date="1856" rank="subgenus">guembelia</taxon_name>
    <taxon_name authority="Limpricht" date="1884" rank="species">teretinervis</taxon_name>
    <place_of_publication>
      <publication_title>Jahresber. Schles. Ges. Vaterl. Cult.</publication_title>
      <place_in_publication>61: 216. 1884,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus guembelia;species teretinervis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443407</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schistidium</taxon_name>
    <taxon_name authority="(Limpricht) Limpricht" date="unknown" rank="species">teretinerve</taxon_name>
    <taxon_hierarchy>genus Schistidium;species teretinerve;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose tufts, green-brown to reddish-brown, shiny.</text>
      <biological_entity id="o1547" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green-brown" name="coloration" notes="" src="d0_s0" to="reddish-brown" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="shiny" value_original="shiny" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1548" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o1547" id="r361" name="in" negation="false" src="d0_s0" to="o1548" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–3 cm, central strand strong.</text>
      <biological_entity id="o1549" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o1550" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ovate-cordate to lanceolate, 0.6–1.2 × 0.2–0.5 mm, keeled, not plicate, margins plane, awn to 0.3 mm, often just hyaline-tipped, commonly long-decurrent, costal transverse-section prominent, circular distally;</text>
      <biological_entity id="o1551" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate-cordate" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s2" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o1552" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o1553" name="awn" name_original="awn" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="often just" name="architecture" src="d0_s2" value="hyaline-tipped" value_original="hyaline-tipped" />
        <character is_modifier="false" modifier="commonly" name="shape" src="d0_s2" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
      <biological_entity constraint="costal" id="o1554" name="transverse-section" name_original="transverse-section" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="distally" name="arrangement_or_shape" src="d0_s2" value="circular" value_original="circular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal juxtacostal laminal cells quadrate to short-rectangular, straight, thin to thick-walled;</text>
      <biological_entity constraint="basal" id="o1555" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="laminal" id="o1556" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s3" to="short-rectangular" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal laminal cells oblate to quadrate, straight, thick-walled, not hyaline;</text>
      <biological_entity constraint="basal marginal laminal" id="o1557" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblate" name="shape" src="d0_s4" to="quadrate" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells roundedquadrate, thick-walled;</text>
      <biological_entity constraint="medial laminal" id="o1558" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="roundedquadrate" value_original="roundedquadrate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 2-stratose, bulging, marginal cells 2-stratose, bulging.</text>
      <biological_entity constraint="distal laminal" id="o1559" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous, perichaetial leaves unknown.</text>
      <biological_entity constraint="marginal" id="o1560" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity id="o1561" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
      <biological_entity constraint="perichaetial" id="o1562" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Seta unknown.</text>
      <biological_entity id="o1563" name="seta" name_original="seta" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Capsule unknown.</text>
      <biological_entity id="o1564" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist calcareous sandstone, limestone and dolomite outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="sandstone" modifier="calcareous" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="dolomite outcrops" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (200-1700 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="200" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Ont., Que., Sask., Yukon; Alaska, Ark., Colo., Minn., Mo., Mont., Nev., Okla., Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>As reported by R. R. Ireland (1982b), Grimmia teretinervis is widely scattered across North America, but nowhere is it common. R. I. Hastings (2002) added several more Western collection locations to those reported by Ireland. Based on field observations and by correlating collecting localities with bedrock geology,  Hastings proposed that the distribution of G. teretinervis in North America is largely correlated with the boundaries of ancient epicontinental seaways. These deposits have subsequently undergone faulting or were subjected to glacial-fluvial erosion. The ancient oceans provided the calcareous sediments, and the faulting and erosion created the steep exposures preferred by G. teretinervis. Sporophytes have never been observed for this species and, until reported by Ireland, antheridial plants also were unknown. Despite the lack of sporophytes, this species is readily identified by its unique costal structure, which is circular in transverse section. It commonly has thick-walled, bulging laminal cells and very short awns that are none-the-less often long-decurrent. These features give the plants a blackish brown, shiny thread-like appearance.</discussion>
  
</bio:treatment>