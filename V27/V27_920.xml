<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">629</other_info_on_meta>
    <other_info_on_meta type="treatment_page">628</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Schimper" date="1860" rank="genus">microbryum</taxon_name>
    <taxon_name authority="(Hedwig) R. H. Zander" date="1993" rank="species">starckeanum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Buffalo Soc. Nat. Sci.</publication_title>
      <place_in_publication>32: 240. 1993,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus microbryum;species starckeanum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075544</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Weissia</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">starckeana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>65. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Weissia;species starckeana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pottia</taxon_name>
    <taxon_name authority="(Hedwig) Müller Hal." date="unknown" rank="species">starckeana</taxon_name>
    <taxon_hierarchy>genus Pottia;species starckeana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Distal laminal cells weakly convex superficially, adaxial surface of the costa not mammillose.</text>
      <biological_entity constraint="distal laminal" id="o6156" name="cell" name_original="cells" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="weakly; superficially" name="shape" src="d0_s0" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6157" name="surface" name_original="surface" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s0" value="mammillose" value_original="mammillose" />
      </biological_entity>
      <biological_entity id="o6158" name="costa" name_original="costa" src="d0_s0" type="structure" />
      <relation from="o6157" id="r1451" name="part_of" negation="false" src="d0_s0" to="o6158" />
    </statement>
    <statement id="d0_s1">
      <text>Seta elongate.</text>
      <biological_entity id="o6159" name="seta" name_original="seta" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Capsule stegocarpous or occasionally cleistocarpous, cylindrical.</text>
      <biological_entity id="o6160" name="capsule" name_original="capsule" src="d0_s2" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s2" value="stegocarpous" value_original="stegocarpous" />
        <character is_modifier="false" modifier="occasionally" name="dehiscence" src="d0_s2" value="cleistocarpous" value_original="cleistocarpous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spores smooth or tuberculate (seldom also weakly papillose), 22–30 µm.</text>
      <biological_entity id="o6161" name="spore" name_original="spores" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="tuberculate" value_original="tuberculate" />
        <character char_type="range_value" from="22" from_unit="um" name="some_measurement" src="d0_s3" to="30" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North Temperate Zone, disjunct to austral areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North Temperate Zone" establishment_means="native" />
        <character name="distribution" value="disjunct to austral areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 6 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules cleistocarpous (seldom operculum weakly differentiated but not dehiscent), spores both tuberculate and papillose, occasionally nearly smooth</description>
      <determination>1c Microbryum starckeanum var. fosbergii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules stegocarpous, spores smooth or tuberculate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peristome present, usually well-developed but truncate apically, leaves stoutly mucronate to short-awned</description>
      <determination>1a Microbryum starckeanum var. starckeanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peristome absent or rudimentary, leaves very shortly mucronate</description>
      <determination>1b Microbryum starckeanum var. brachyodus</determination>
    </key_statement>
  </key>
</bio:treatment>