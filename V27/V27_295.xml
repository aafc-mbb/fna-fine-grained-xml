<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="H. H. Blom" date="1996" rank="species">frisvollianum</taxon_name>
    <place_of_publication>
      <publication_title>Bryophyt. Biblioth.</publication_title>
      <place_in_publication>49: 87, fig. 25. 1996,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species frisvollianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075431</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in cushions or tufts, orangebrown or olivaceous.</text>
      <biological_entity id="o2608" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="orangebrown" value_original="orangebrown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2609" name="cushion" name_original="cushions" src="d0_s0" type="structure" />
      <biological_entity id="o2610" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o2608" id="r716" name="in" negation="false" src="d0_s0" to="o2609" />
      <relation from="o2608" id="r717" name="in" negation="false" src="d0_s0" to="o2610" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–2 (–3.5) cm, central strand distinct.</text>
      <biological_entity id="o2611" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o2612" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect or slightly curved when dry, ovatelanceolate to ovate-triangular, sharply keeled distally, 1.3–2.4 mm, 1-stratose;</text>
      <biological_entity id="o2613" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="ovate-triangular" />
        <character is_modifier="false" modifier="sharply; distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s2" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins usually recurved to apex, often sharply denticulate distally, 1-stratose or 2-stratose;</text>
      <biological_entity id="o2614" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="to apex" constraintid="o2615" is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="often sharply; distally" name="shape" notes="" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o2615" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apices acute;</text>
      <biological_entity id="o2616" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa excurrent as a coarsely denticulate, usually decurrent, straight or flexuose awn, rarely percurrent, abaxial surface of the awn usually papillose;</text>
      <biological_entity id="o2617" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character constraint="as a coarsely denticulate" is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s5" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="rarely" name="architecture" notes="" src="d0_s5" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o2618" name="awn" name_original="awn" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o2619" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o2620" name="awn" name_original="awn" src="d0_s5" type="structure" />
      <relation from="o2619" id="r718" name="part_of" negation="false" src="d0_s5" to="o2620" />
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells rectangular, quadrate, or ovate;</text>
      <biological_entity constraint="basal marginal" id="o2621" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells quadrate or short-rectangular, 8–11 µm wide, papillose, strongly sinuose often with pale yellowish walls.</text>
      <biological_entity id="o2623" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="pale yellowish" value_original="pale yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o2622" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s7" to="11" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
        <character constraint="with walls" constraintid="o2623" is_modifier="false" modifier="strongly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule orangebrown or yellow when old, sometimes redbrown, ovoid-cylindric, 0.65–1.1 mm, occasionally striate;</text>
      <biological_entity id="o2624" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s9" value="orangebrown" value_original="orangebrown" />
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character char_type="range_value" from="0.65" from_unit="mm" name="some_measurement" src="d0_s9" to="1.1" to_unit="mm" />
        <character is_modifier="false" modifier="occasionally" name="coloration_or_pubescence_or_relief" src="d0_s9" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells isodiametric or oblate, often somewhat angular, thin-walled;</text>
      <biological_entity constraint="exothecial" id="o2625" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblate" value_original="oblate" />
        <character is_modifier="false" modifier="often somewhat" name="arrangement_or_shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stomata present;</text>
      <biological_entity id="o2626" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome patent revolute, 220–330 µm, red, densely papillose, usually weakly perforated.</text>
      <biological_entity id="o2627" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="patent" value_original="patent" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s12" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="220" from_unit="um" name="some_measurement" src="d0_s12" to="330" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="usually weakly" name="architecture" src="d0_s12" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 8–11 µm, granulose or nearly smooth.</text>
      <biological_entity id="o2628" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s13" to="11" to_unit="um" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="granulose" value_original="granulose" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fissures, limestone and dolomites in Arctic fellfields, polygon fields, and on dry ridges in tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fissures" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="dolomites" />
        <character name="habitat" value="arctic fellfields" />
        <character name="habitat" value="polygon fields" />
        <character name="habitat" value="dry ridges" />
        <character name="habitat" value="tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Nunavut, Yukon; Alaska; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <discussion>Schistidium frisvollianum is the most ornamented species of the genus in the flora area. Its strongly papillose lamina, decurrent and usually spiny-denticulate awns, and denticulate leaf margins combine with its distinctive orange-brown color to make it an easily recognizable taxon. See the comments under 5. Schistidium boreale regarding differences among the North American species of Schistidium with papillose laminal cells.</discussion>
  
</bio:treatment>