<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">395</other_info_on_meta>
    <other_info_on_meta type="treatment_page">394</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1847" rank="genus">dicranodontium</taxon_name>
    <taxon_name authority="(Mitten) Brotherus in H. G. A. Engler and K. Prantl" date="1901" rank="species">asperulum</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>208[I,3]: 336. 1901,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranodontium;species asperulum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200000933</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">asperulum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot., suppl.</publication_title>
      <place_in_publication>1: 22. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species asperulum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Campylopus</taxon_name>
    <taxon_name authority="(Austin) Lesquereux &amp; James" date="unknown" rank="species">virginicus</taxon_name>
    <taxon_hierarchy>genus Campylopus;species virginicus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Austin" date="unknown" rank="species">virginicum</taxon_name>
    <taxon_hierarchy>genus Dicranum;species virginicum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants brown proximally, yellowish to dark green distally, dull to glossy, in loose tufts.</text>
      <biological_entity id="o8023" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character char_type="range_value" from="yellowish" modifier="distally" name="coloration" src="d0_s0" to="dark green" />
        <character char_type="range_value" from="dull" name="reflectance" src="d0_s0" to="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8024" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o8023" id="r1920" name="in" negation="false" src="d0_s0" to="o8024" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–6 (–8) cm, radiculose proximally with whitish to reddish-brown rhizoids.</text>
      <biological_entity id="o8025" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8026" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character char_type="range_value" from="whitish" is_modifier="true" name="coloration" src="d0_s1" to="reddish-brown" />
      </biological_entity>
      <relation from="o8025" id="r1921" modifier="proximally" name="with" negation="false" src="d0_s1" to="o8026" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-patent, flexuose, spreading at ca. 45º or more when wet, occasionally falcate-secund at stem apices, 3–8 mm, often deciduous, abruptly narrowed from an ovate base into a setaceous subula, subtubulose proximally, channeled distally, margins serrulate to serrate at shoulders, usually strongly serrate distally, apex acute;</text>
      <biological_entity id="o8027" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-patent" value_original="erect-patent" />
        <character is_modifier="false" name="course" src="d0_s2" value="flexuose" value_original="flexuose" />
        <character constraint="at " constraintid="o8028" is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="from base" constraintid="o8029" is_modifier="false" modifier="abruptly" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="proximally; distally" name="shape" notes="" src="d0_s2" value="channeled" value_original="channeled" />
      </biological_entity>
      <biological_entity constraint="stem" id="o8028" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="45" value_original="45" />
        <character is_modifier="true" name="condition" src="d0_s2" value="wet" value_original="wet" />
        <character is_modifier="true" modifier="occasionally" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
      </biological_entity>
      <biological_entity id="o8029" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o8030" name="subulum" name_original="subula" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="setaceous" value_original="setaceous" />
      </biological_entity>
      <biological_entity id="o8031" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="at shoulders" constraintid="o8032" from="serrulate" name="architecture_or_shape" src="d0_s2" to="serrate" />
        <character is_modifier="false" modifier="usually strongly; distally" name="architecture_or_shape" notes="" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o8032" name="shoulder" name_original="shoulders" src="d0_s2" type="structure" />
      <biological_entity id="o8033" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o8029" id="r1922" name="into" negation="false" src="d0_s2" to="o8030" />
    </statement>
    <statement id="d0_s3">
      <text>costa distinct, occupying ca. 1/3 of leaf base;</text>
      <biological_entity id="o8034" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character constraint="of leaf base" constraintid="o8035" name="quantity" src="d0_s3" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o8035" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>cells not thick-walled or pitted, distal cells linear, 24–56 × 5–6 µm, basal-cells broadly rectangular, hyaline, ca. 9 µm wide, alar cells sometimes forming indistinct auricles, hyaline.</text>
      <biological_entity id="o8036" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="relief" src="d0_s4" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8037" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="24" from_unit="um" name="length" src="d0_s4" to="56" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s4" to="6" to_unit="um" />
      </biological_entity>
      <biological_entity id="o8038" name="basal-cell" name_original="basal-cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character name="width" src="d0_s4" unit="um" value="9" value_original="9" />
      </biological_entity>
      <biological_entity id="o8039" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8040" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <relation from="o8039" id="r1923" name="forming" negation="false" src="d0_s4" to="o8040" />
    </statement>
    <statement id="d0_s5">
      <text>Seta 15–20 mm, erect-sinuose.</text>
      <biological_entity id="o8041" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="erect-sinuose" value_original="erect-sinuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule 1.5–2 mm;</text>
      <biological_entity id="o8042" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>operculum not seen, reportedly shorter than the capsule.</text>
      <biological_entity id="o8043" name="operculum" name_original="operculum" src="d0_s7" type="structure">
        <character constraint="than the capsule" constraintid="o8044" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="reportedly shorter" value_original="reportedly shorter" />
      </biological_entity>
      <biological_entity id="o8044" name="capsule" name_original="capsule" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spores 9–14 µm.</text>
      <biological_entity id="o8045" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s8" to="14" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, shaded, acidic cliff faces and cliff shelves, occasionally on earth of overturned tree roots</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="damp" />
        <character name="habitat" value="shaded" />
        <character name="habitat" value="acidic cliff" />
        <character name="habitat" value="cliff shelves" modifier="faces" />
        <character name="habitat" value="earth" modifier="occasionally on" constraint="of overturned tree roots" />
        <character name="habitat" value="overturned tree roots" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Ga., Ky., N.C., Tenn., Va., W.Va.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Sporophytes of Dicranodontium asperulum were seen in North America only in one British Columbia specimen. The wide-spreading leaves, standing out from the stem at 45° or more when wet, and the serrate to serrulate shoulders of the leaf bases make D. asperulum the most distinctive North American species of the genus. The leaves of the other species are appressed or weakly spreading when wet, with entire bases.</discussion>
  
</bio:treatment>