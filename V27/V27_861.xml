<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="mention_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">602</other_info_on_meta>
    <other_info_on_meta type="treatment_page">600</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">tortula</taxon_name>
    <taxon_name authority="(Bridel) Montagne" date="1832" rank="species">inermis</taxon_name>
    <place_of_publication>
      <publication_title>Arch. Bot. (Leipzig)</publication_title>
      <place_in_publication>1: 136. 1832,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus tortula;species inermis</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001220</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Syntrichia</taxon_name>
    <taxon_name authority="(Bridel) Bruch" date="unknown" rank="species">subulata</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="variety">inermis</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 581. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Syntrichia;species subulata;variety inermis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Syntrichia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">inermis</taxon_name>
    <taxon_hierarchy>genus Syntrichia;species inermis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves lingulate, apex rounded-acute, apiculate to short-mucronate, margins recurved from base to near the apex, not bordered;</text>
      <biological_entity id="o3651" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="lingulate" value_original="lingulate" />
      </biological_entity>
      <biological_entity id="o3652" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded-acute" value_original="rounded-acute" />
        <character char_type="range_value" from="apiculate" name="shape" src="d0_s0" to="short-mucronate" />
      </biological_entity>
      <biological_entity id="o3653" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character constraint="from base" constraintid="o3654" is_modifier="false" name="orientation" src="d0_s0" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s0" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o3654" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o3655" name="apex" name_original="apex" src="d0_s0" type="structure" />
      <relation from="o3654" id="r890" name="near" negation="false" src="d0_s0" to="o3655" />
    </statement>
    <statement id="d0_s1">
      <text>costa subpercurrent, percurrent or very short-excurrent, lacking an adaxial pad of cells but distally narrowing or weakly thickened, 3–4 (–5) cells across the convex adaxial surface;</text>
      <biological_entity id="o3656" name="costa" name_original="costa" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s1" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3657" name="pad" name_original="pad" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="distally" name="width" src="d0_s1" value="narrowing" value_original="narrowing" />
        <character is_modifier="false" modifier="weakly" name="width" src="d0_s1" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="4" />
      </biological_entity>
      <biological_entity id="o3658" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity id="o3659" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity constraint="adaxial" id="o3660" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="convex" value_original="convex" />
      </biological_entity>
      <relation from="o3657" id="r891" name="part_of" negation="false" src="d0_s1" to="o3658" />
      <relation from="o3659" id="r892" name="across" negation="false" src="d0_s1" to="o3660" />
    </statement>
    <statement id="d0_s2">
      <text>distal laminal cells hexagonal, (12–) 15–18 µm wide, 1: 1, strongly papillose with 4–5 2-fid papillae.</text>
      <biological_entity id="o3662" name="papilla" name_original="papillae" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="4-5-2-fid" value_original="4-5-2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sexual condition gonioautoicous.</text>
      <biological_entity constraint="distal laminal" id="o3661" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="hexagonal" value_original="hexagonal" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s2" to="15" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s2" to="18" to_unit="um" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character constraint="with papillae" constraintid="o3662" is_modifier="false" modifier="strongly" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporophytes exerted.</text>
      <biological_entity id="o3663" name="sporophyte" name_original="sporophytes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Seta 1.2–1.5 (–2.5) cm.</text>
      <biological_entity id="o3664" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s5" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule stegocarpic, not systylius, cylindric, erect and nearly straight, urn usually 3–4 mm;</text>
      <biological_entity id="o3665" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="systylius" value_original="systylius" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o3666" name="urn" name_original="urn" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peristome length 1000–1500 µm, teeth of 32 filaments twisted at least one full turn, basal membrane 300–600 µm; operculum 1.3–1.6 (–2) mm.</text>
      <biological_entity id="o3667" name="peristome" name_original="peristome" src="d0_s7" type="structure">
        <character char_type="range_value" from="1000" from_unit="um" name="length" src="d0_s7" to="1500" to_unit="um" />
      </biological_entity>
      <biological_entity id="o3668" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="full" value_original="full" />
      </biological_entity>
      <biological_entity id="o3669" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="32" value_original="32" />
        <character is_modifier="false" modifier="at least" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3670" name="membrane" name_original="membrane" src="d0_s7" type="structure">
        <character char_type="range_value" from="300" from_unit="um" name="some_measurement" src="d0_s7" to="600" to_unit="um" />
      </biological_entity>
      <biological_entity id="o3671" name="operculum" name_original="operculum" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="1.6" to_unit="mm" />
      </biological_entity>
      <relation from="o3668" id="r893" name="consist_of" negation="false" src="d0_s7" to="o3669" />
    </statement>
    <statement id="d0_s8">
      <text>Spores 11–15 µm, spheric, finely papillose.</text>
      <biological_entity id="o3672" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s8" to="15" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s8" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil, rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., S.Dak., Tex., Utah, Wyo.; Mexico (Baja California, Chihuahua, Sonora); Europe; s Asia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="s Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion>Tortula inermis has the aspect of a Syntrichia with its ligulate, apiculate leaves and strong costa, but the plant is yellow or orange in KOH solution, and the costal section reveals a rounded stereid band. It is related to T. subulata and T. mucronifolia but the lack of a strong mucro and the narrow but nearly complete recurving of the leaf margins are diagnostic.</discussion>
  
</bio:treatment>