<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">539</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="treatment_page">547</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">didymodon</taxon_name>
    <taxon_name authority="(Müller Hal.) R. H. Zander" date="1978" rank="species">umbrosus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>41: 22. 1978,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus didymodon;species umbrosus</taxon_hierarchy>
    <other_info_on_name type="fna_id">242444212</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">umbrosa</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>42: 340. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Barbula;species umbrosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Didymodon</taxon_name>
    <taxon_name authority="Cardot" date="unknown" rank="species">australasiae</taxon_name>
    <taxon_name authority="(Müller Hal.) R. H. Zander" date="unknown" rank="variety">umbrosus</taxon_name>
    <taxon_hierarchy>genus Didymodon;species australasiae;variety umbrosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trichostomopsis</taxon_name>
    <taxon_name authority="(Müller Hal.) H. Robinson" date="unknown" rank="species">crispifolia</taxon_name>
    <taxon_hierarchy>genus Trichostomopsis;species crispifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trichostomopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">umbrosa</taxon_name>
    <taxon_hierarchy>genus Trichostomopsis;species umbrosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually green to very bright green distally, contrasting with light tan proximally.</text>
      <biological_entity id="o4340" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="very; distally" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character constraint="with" is_modifier="false" modifier="proximally" name="coloration" src="d0_s0" value="light tan" value_original="light tan" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 1 (–1.5) cm, central strand present.</text>
      <biological_entity id="o4341" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o4342" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves spreading-incurved and twisted to incurved-appressed when dry, spreading to spreading-recurved and not keeled when moist, monomorphic, longlanceolate, broadly concave adaxially across leaf, usually 1–2.5 (–4) mm, base sharply differentiated in shape, ovate, margins usually plane, entire, apex narrowly acute, not fragile;</text>
      <biological_entity id="o4343" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading-incurved" value_original="spreading-incurved" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="incurved-appressed" value_original="incurved-appressed" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="spreading-recurved" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character constraint="across leaf" constraintid="o4344" is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" modifier="usually" name="atypical_distance" notes="" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="usually" name="distance" notes="" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4344" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity id="o4345" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s2" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o4346" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4347" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s2" value="fragile" value_original="fragile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent or ending a few cells below the apex, broader at mid leaf, occasionally weakly spurred, with a low adaxial pad of cells, adaxial costal cells rectangular, 4–6 cells wide at mid leaf, guide cells in 1–2 layers;</text>
      <biological_entity id="o4348" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
        <character name="architecture" src="d0_s3" value="ending a few cells" value_original="ending a few cells" />
        <character constraint="at mid leaf" constraintid="o4350" is_modifier="false" name="width" notes="" src="d0_s3" value="broader" value_original="broader" />
        <character is_modifier="false" modifier="occasionally weakly" name="architecture_or_shape" notes="" src="d0_s3" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o4349" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o4350" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o4351" name="pad" name_original="pad" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o4352" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial costal" id="o4353" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o4354" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character constraint="at mid leaf" constraintid="o4355" is_modifier="false" name="width" src="d0_s3" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="mid" id="o4355" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity constraint="guide" id="o4356" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity id="o4357" name="layer" name_original="layers" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <relation from="o4348" id="r1038" name="below" negation="false" src="d0_s3" to="o4349" />
      <relation from="o4348" id="r1039" name="with" negation="false" src="d0_s3" to="o4351" />
      <relation from="o4351" id="r1040" name="part_of" negation="false" src="d0_s3" to="o4352" />
      <relation from="o4356" id="r1041" name="in" negation="false" src="d0_s3" to="o4357" />
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells strongly differentiated medially, rectangular, walls very thin or hyaline, often perforated by transverse-slits;</text>
      <biological_entity constraint="basal laminal" id="o4358" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly; medially" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o4359" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="very" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character constraint="by transverse-slits" constraintid="o4360" is_modifier="false" modifier="often" name="architecture" src="d0_s4" value="perforated" value_original="perforated" />
      </biological_entity>
      <biological_entity id="o4360" name="transverse-slit" name_original="transverse-slits" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells in rather distinct rows, 7–12 wide, 1: 1 or occasionally longitudinally elongate, papillae usually absent, lumens oval to roundedquadrate or rounded-rectangular, walls convex on both sides of lamina, 2-stratose in one or more rows along margins.</text>
      <biological_entity constraint="distal laminal" id="o4361" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" modifier="occasionally longitudinally" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o4362" name="row" name_original="rows" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="rather" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="7" name="width" src="d0_s5" to="12" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4363" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4364" name="lumen" name_original="lumens" src="d0_s5" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s5" to="roundedquadrate or rounded-rectangular" />
      </biological_entity>
      <biological_entity id="o4366" name="side" name_original="sides" src="d0_s5" type="structure" />
      <biological_entity id="o4367" name="lamina" name_original="lamina" src="d0_s5" type="structure" />
      <biological_entity id="o4368" name="row" name_original="rows" src="d0_s5" type="structure" />
      <biological_entity id="o4369" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <relation from="o4361" id="r1042" name="in" negation="false" src="d0_s5" to="o4362" />
      <relation from="o4366" id="r1043" name="part_of" negation="false" src="d0_s5" to="o4367" />
      <relation from="o4368" id="r1044" name="along" negation="false" src="d0_s5" to="o4369" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by multicellular tubers on proximal rhizoids.</text>
      <biological_entity id="o4365" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character constraint="on sides" constraintid="o4366" is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character constraint="in rows" constraintid="o4368" is_modifier="false" name="architecture" notes="" src="d0_s5" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o4370" name="tuber" name_original="tubers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4371" name="rhizoid" name_original="rhizoids" src="d0_s6" type="structure" />
      <relation from="o4370" id="r1045" name="on" negation="false" src="d0_s6" to="o4371" />
    </statement>
    <statement id="d0_s7">
      <text>Seta 0.7–1 cm.</text>
      <biological_entity id="o4372" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule 1–1.9 mm;</text>
      <biological_entity id="o4373" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>peristome teeth 32, linear, weakly twisted, to 600 µm. Spores 11–15 µm. Distal laminal KOH reaction variously negative or yellow or orange or redbrown.</text>
      <biological_entity constraint="peristome" id="o4374" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="32" value_original="32" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s9" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" src="d0_s9" to="600" to_unit="um" />
      </biological_entity>
      <biological_entity id="o4375" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s9" to="15" to_unit="um" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s9" value="laminal" value_original="laminal" />
        <character is_modifier="false" modifier="variously" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules maturity not determined.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, lava, dolomite, cliff face, rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="lava" />
        <character name="habitat" value="dolomite" />
        <character name="habitat" value="cliff face" />
        <character name="habitat" value="rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (200-1900 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="200" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., N.Mex., N.Y., Tex.; Mexico; South America; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Didymodon umbrosus is one of a number of mundivagant taxa whose distribution is associated with human activities (P. M. Eckel 1986). The proximal marginal cells are elongate and in distinctive rows. The transversely slit basal cells are distinctive in many specimens though also found in taxa of the Dicranaceae (R. H. Zander 1981b), some of which may belong in Pottiaceae (O. Werner et al. 2004).</discussion>
  
</bio:treatment>