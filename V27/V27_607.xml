<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
    <other_info_on_meta type="illustration_page">426</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bridel" date="1826" rank="genus">oreas</taxon_name>
    <taxon_name authority="(Hooker) Bridel" date="1826" rank="species">martiana</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 383. 1826,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus oreas;species martiana</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001007</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Weissia</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">martiana</taxon_name>
    <place_of_publication>
      <publication_title>Musci Exot.</publication_title>
      <place_in_publication>2: plate 104. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Weissia;species martiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1.5–8.5 mm.</text>
      <biological_entity id="o1761" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s0" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 1.3–2 mm, distal laminal cells 6–10 µm. Perichaetial leaves 2–2.5 mm.</text>
      <biological_entity id="o1762" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal laminal" id="o1763" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="um" name="some_measurement" src="d0_s1" to="10" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="perichaetial" id="o1764" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Seta 3–7 mm.</text>
      <biological_entity id="o1765" name="seta" name_original="seta" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsule 0.6–0.9 mm;</text>
      <biological_entity id="o1766" name="capsule" name_original="capsule" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s3" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>annulus of three rows of pale, thick-walled, quadrate cells;</text>
      <biological_entity id="o1767" name="annulus" name_original="annulus" src="d0_s4" type="structure" />
      <biological_entity id="o1768" name="row" name_original="rows" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1769" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="pale" value_original="pale" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="true" name="shape" src="d0_s4" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <relation from="o1767" id="r408" name="consist_of" negation="false" src="d0_s4" to="o1768" />
      <relation from="o1768" id="r409" name="part_of" negation="false" src="d0_s4" to="o1769" />
    </statement>
    <statement id="d0_s5">
      <text>operculum 0.4–0.7 mm;</text>
      <biological_entity id="o1770" name="operculum" name_original="operculum" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peristome teeth 190–210 µm. Spores 16 µm.</text>
      <biological_entity constraint="peristome" id="o1771" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="190" from_unit="um" name="some_measurement" src="d0_s6" to="210" to_unit="um" />
      </biological_entity>
      <biological_entity id="o1772" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="um" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine tundra, moderate to high elevations (ca. 0–4200 m)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" modifier="alpine" />
        <character name="habitat" value="high elevations" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C.; Alaska, Colo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Oreas martiana is easily recognized by its golden color, the very thick and deep polsters with only a few millimeters of living leaves and much brown material below. In fruit it is unmistakable, with pendent, cygneous setae and strongly ribbed capsules. In the Front Range of Colorado it appears to be particularly abundant.</discussion>
  
</bio:treatment>