<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">185</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">funariaceae</taxon_name>
    <taxon_name authority="Schwägrichen" date="1823" rank="genus">entosthodon</taxon_name>
    <taxon_name authority="Steere" date="1938" rank="species">wigginsii</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>41: 36, figs. 1–8. 1938,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family funariaceae;genus entosthodon;species wigginsii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075464</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–5 mm, yellow green, the main axis often arising from a fleshy rhizome.</text>
      <biological_entity id="o1737" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow green" value_original="yellow green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="main" id="o1738" name="axis" name_original="axis" src="d0_s0" type="structure">
        <character constraint="from rhizome" constraintid="o1739" is_modifier="false" modifier="often" name="orientation" src="d0_s0" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o1739" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves shriveled and distorted when dry, obovate to oblong-spathulate, imbricate, somewhat concave, mostly 2–3 mm;</text>
      <biological_entity id="o1740" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="shriveled" value_original="shriveled" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s1" value="distorted" value_original="distorted" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s1" to="oblong-spathulate" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character char_type="range_value" from="2" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins serrulate by projecting ends of thin-walled cells;</text>
      <biological_entity id="o1741" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="by ends" constraintid="o1742" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o1742" name="end" name_original="ends" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o1743" name="bud" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <relation from="o1742" id="r478" name="part_of" negation="false" src="d0_s2" to="o1743" />
    </statement>
    <statement id="d0_s3">
      <text>apices acute, sometimes with a short apiculus;</text>
      <biological_entity id="o1744" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o1745" name="apiculu" name_original="apiculus" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <relation from="o1744" id="r479" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o1745" />
    </statement>
    <statement id="d0_s4">
      <text>costa ending 4–6 cells before the apex;</text>
      <biological_entity id="o1746" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o1747" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o1748" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o1746" id="r480" name="ending" negation="false" src="d0_s4" to="o1747" />
      <relation from="o1746" id="r481" name="before" negation="false" src="d0_s4" to="o1748" />
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells rectangular (85–120 × 25–30 µm), distal cells irregularly polygonal to hexagonal or oblong-rectangular, a little longer but not otherwise differentiated at the margins.</text>
      <biological_entity constraint="basal laminal" id="o1749" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1750" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="irregularly polygonal" name="shape" src="d0_s5" to="hexagonal or oblong-rectangular" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
        <character constraint="at margins" constraintid="o1751" is_modifier="false" modifier="not otherwise" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o1751" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Seta brownish basally and paler distally, 5–8 mm, straight, not hygroscopic.</text>
      <biological_entity id="o1752" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s6" value="brownish basally and paler" value_original="brownish basally and paler" />
        <character char_type="range_value" from="5" from_unit="mm" modifier="distally" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="hygroscopic" value_original="hygroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule pyriform to elongate-pyriform from an apophysis about half the total length, 1.5–2 mm, becoming sulcate when dry and empty;</text>
      <biological_entity id="o1753" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="from apophysis" constraintid="o1754" from="pyriform" name="shape" src="d0_s7" to="elongate-pyriform" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" notes="" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="when dry and empty" name="architecture" src="d0_s7" value="sulcate" value_original="sulcate" />
      </biological_entity>
      <biological_entity id="o1754" name="apophysis" name_original="apophysis" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>exothecial cells thickened, narrowly oblong and transversely elongate in 4–5 rows proximal to the mouth;</text>
      <biological_entity constraint="exothecial" id="o1755" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character constraint="in rows" constraintid="o1756" is_modifier="false" modifier="transversely" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o1756" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
        <character constraint="to mouth" constraintid="o1757" is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o1757" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>operculum planoconvex;</text>
      <biological_entity id="o1758" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="planoconvex" value_original="planoconvex" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome double, exostome teeth bright red becoming paler at the tip, lanceolate with 7–8 articulations, obliquely striate basally, papillose at the tips, endostome well developed from a pale basal membrane bearing irregular segments.</text>
      <biological_entity id="o1759" name="peristome" name_original="peristome" src="d0_s10" type="structure" />
      <biological_entity constraint="exostome" id="o1760" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="bright red" value_original="bright red" />
        <character constraint="at tip" constraintid="o1761" is_modifier="false" modifier="becoming" name="coloration" src="d0_s10" value="paler" value_original="paler" />
        <character constraint="with articulations" constraintid="o1762" is_modifier="false" name="shape" notes="" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="obliquely; basally" name="coloration_or_pubescence_or_relief" notes="" src="d0_s10" value="striate" value_original="striate" />
        <character constraint="at tips" constraintid="o1763" is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o1761" name="tip" name_original="tip" src="d0_s10" type="structure" />
      <biological_entity id="o1762" name="articulation" name_original="articulations" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s10" to="8" />
      </biological_entity>
      <biological_entity id="o1763" name="tip" name_original="tips" src="d0_s10" type="structure" />
      <biological_entity id="o1764" name="endostome" name_original="endostome" src="d0_s10" type="structure">
        <character constraint="from basal membrane" constraintid="o1765" is_modifier="false" modifier="well" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1765" name="membrane" name_original="membrane" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o1766" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s10" value="irregular" value_original="irregular" />
      </biological_entity>
      <relation from="o1765" id="r482" name="bearing" negation="false" src="d0_s10" to="o1766" />
    </statement>
    <statement id="d0_s11">
      <text>Calyptra cucullate, long-beaked, inflated around the capsule, large, smooth.</text>
      <biological_entity id="o1767" name="calyptra" name_original="calyptra" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="long-beaked" value_original="long-beaked" />
        <character constraint="around capsule" constraintid="o1768" is_modifier="false" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1768" name="capsule" name_original="capsule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Spores 20–24 µm, bacculate-insulate often in persistent tetrads at maturity.</text>
      <biological_entity id="o1769" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s12" to="24" to_unit="um" />
        <character constraint="in tetrads" constraintid="o1770" is_modifier="false" name="relief" src="d0_s12" value="bacculate-insulate" value_original="bacculate-insulate" />
      </biological_entity>
      <biological_entity id="o1770" name="tetrad" name_original="tetrads" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fine soil, crevices of rocks, base of cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fine soil" />
        <character name="habitat" value="crevices" constraint="of rocks" />
        <character name="habitat" value="rocks" />
        <character name="habitat" value="base" constraint="of cliffs" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>