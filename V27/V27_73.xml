<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">79</other_info_on_meta>
    <other_info_on_meta type="treatment_page">82</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="(Lindberg) Schimper" date="1876" rank="section">subsecunda</taxon_name>
    <taxon_name authority="Andrus" date="2007" rank="species">oregonense</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>110: 123, figs. 1–4. 2007,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section subsecunda;species oregonense</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075481</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, green to light-brown;</text>
      <biological_entity id="o5905" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="light-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>capitulum moderately well defined.</text>
      <biological_entity id="o5906" name="capitulum" name_original="capitulum" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately well" name="prominence" src="d0_s1" value="defined" value_original="defined" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems green;</text>
      <biological_entity id="o5907" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>superficial cortex of 1 layer of well-differentiated, enlarged and thin-walled cells.</text>
      <biological_entity constraint="superficial" id="o5908" name="cortex" name_original="cortex" src="d0_s3" type="structure" />
      <biological_entity id="o5909" name="layer" name_original="layer" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5910" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="well-differentiated" value_original="well-differentiated" />
        <character is_modifier="true" name="size" src="d0_s3" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <relation from="o5908" id="r1648" name="consist_of" negation="false" src="d0_s3" to="o5909" />
      <relation from="o5909" id="r1649" name="part_of" negation="false" src="d0_s3" to="o5910" />
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves lingulate, 1–1.2 mm, apex entire to somewhat erose;</text>
      <biological_entity id="o5911" name="leaf-stem" name_original="stem-leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lingulate" value_original="lingulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s4" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="entire to somewhat" value_original="entire to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o5912" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="entire to somewhat" value_original="entire to somewhat" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>hyaline cells nonseptate;</text>
    </statement>
    <statement id="d0_s6">
      <text>fibrillose and porose in apical region.</text>
      <biological_entity id="o5913" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s6" value="fibrillose" value_original="fibrillose" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5914" name="region" name_original="region" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="porose" value_original="porose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Branches slender with small spreading leaves.</text>
      <biological_entity id="o5915" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character constraint="with leaves" constraintid="o5916" is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o5916" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch fascicles with 2 spreading and 2 pendent branches.</text>
      <biological_entity id="o5917" name="branch" name_original="branch" src="d0_s8" type="structure">
        <character constraint="with branches" constraintid="o5918" is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o5918" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves ovatelanceolate, 1.4–1.6 mm, straight to slightly subsecund, weakly undulate, often recurved in capitulum branches;</text>
      <biological_entity constraint="branch" id="o5919" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s9" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="arrangement" src="d0_s9" value="subsecund" value_original="subsecund" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s9" value="undulate" value_original="undulate" />
        <character constraint="in capitulum branches" constraintid="o5920" is_modifier="false" modifier="often" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="capitulum" id="o5920" name="branch" name_original="branches" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>hyaline cells on convex surface with up to 5 small round faint pores per cell in the basal portion of the cell and free from the cell margins, concave surface aporose.</text>
      <biological_entity id="o5921" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character constraint="from cell margins" constraintid="o5927" is_modifier="false" name="fusion" notes="" src="d0_s10" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o5922" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o5923" name="pore" name_original="pores" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s10" value="round" value_original="round" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity id="o5924" name="cell" name_original="cell" src="d0_s10" type="structure" />
      <biological_entity constraint="basal" id="o5925" name="portion" name_original="portion" src="d0_s10" type="structure" />
      <biological_entity id="o5926" name="cell" name_original="cell" src="d0_s10" type="structure" />
      <biological_entity constraint="cell" id="o5927" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <relation from="o5921" id="r1650" name="on" negation="false" src="d0_s10" to="o5922" />
      <relation from="o5922" id="r1651" name="with" negation="false" src="d0_s10" to="o5923" />
      <relation from="o5923" id="r1652" name="per" negation="false" src="d0_s10" to="o5924" />
      <relation from="o5924" id="r1653" name="in" negation="false" src="d0_s10" to="o5925" />
      <relation from="o5925" id="r1654" name="part_of" negation="false" src="d0_s10" to="o5926" />
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition unknown.</text>
      <biological_entity id="o5928" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="concave" value_original="concave" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule not seen.</text>
      <biological_entity id="o5929" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Spores not seen.</text>
      <biological_entity id="o5930" name="spore" name_original="spores" src="d0_s13" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>58.</number>
  <discussion>Sphagnum oregonense is currently known only from the type locality. Sporophytes of it are unknown. It is associated with other minerotrophic bryophytes such as Meesia triquetra, Calliergon cordifolium, and Campylium polygamum. This is a curious species that has an obvious close relationship with sect. Cuspidata. When wet it is similar in appearance to S. subsecundum but upon drying the sightly undulate and recurved branch leaves give it the charactereistic appearance of this section. The branch leave porosity is also more similar to that of species in sect. Cuspidata than that found in sect. Subsecunda.</discussion>
  
</bio:treatment>