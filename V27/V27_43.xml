<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Lindberg" date="1862" rank="section">cuspidata</taxon_name>
    <taxon_name authority="Flatberg" date="1992" rank="species">isoviitae</taxon_name>
    <place_of_publication>
      <publication_title>J. Bryol.</publication_title>
      <place_in_publication>17: 2, figs. 1, 2. 1992,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section cuspidata;species isoviitae</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443218</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate-sized and moderately weak-stemmed to moderately stiff;</text>
    </statement>
    <statement id="d0_s1">
      <text>green, brownish green to brown;</text>
      <biological_entity id="o5597" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="moderate-sized" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s0" value="weak-stemmed" value_original="weak-stemmed" />
        <character is_modifier="false" modifier="moderately" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="brownish green" name="coloration" src="d0_s1" to="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>capitulum flattopped and 5-radiate, terminal bud often visible.</text>
      <biological_entity id="o5598" name="capitulum" name_original="capitulum" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="5-radiate" value_original="5-radiate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o5599" name="bud" name_original="bud" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s2" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems pale green, rarely with red coloration, superficial cortex of 2 layers of moderately to well differentiated cells.</text>
      <biological_entity id="o5600" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity constraint="superficial" id="o5601" name="cortex" name_original="cortex" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="true" name="character" src="d0_s3" value="coloration" value_original="coloration" />
      </biological_entity>
      <biological_entity id="o5602" name="layer" name_original="layers" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5603" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="moderately; well" name="variability" src="d0_s3" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o5600" id="r1571" modifier="rarely" name="with" negation="false" src="d0_s3" to="o5601" />
      <relation from="o5601" id="r1572" name="part_of" negation="false" src="d0_s3" to="o5602" />
      <relation from="o5601" id="r1573" name="part_of" negation="false" src="d0_s3" to="o5603" />
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves triangular to lingulate-triangular, equal to or more than 0.8 mm, spreading to appressed;</text>
      <biological_entity id="o5604" name="leaf-stem" name_original="stem-leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s4" to="lingulate-triangular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s4" upper_restricted="false" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acute to apiculate, hyaline cells mostly efibrillose and nonseptate.</text>
      <biological_entity id="o5605" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="apiculate" />
      </biological_entity>
      <biological_entity id="o5606" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s5" value="efibrillose" value_original="efibrillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Branches ± straight and somewhat tapered, usually 5-ranked, leaves not greatly elongated at branch distal end.</text>
      <biological_entity id="o5607" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s6" value="5-ranked" value_original="5-ranked" />
      </biological_entity>
      <biological_entity id="o5608" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character constraint="at branch" constraintid="o5609" is_modifier="false" modifier="not greatly" name="length" src="d0_s6" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o5609" name="branch" name_original="branch" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o5610" name="end" name_original="end" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Branch fascicles with 2 spreading and 2–3 pendent branches.</text>
      <biological_entity id="o5611" name="branch" name_original="branch" src="d0_s7" type="structure">
        <character constraint="with branches" constraintid="o5612" is_modifier="false" name="arrangement" src="d0_s7" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o5612" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch stems green and often reddish at proximal end, with cortex enlarged with conspicuous retort cells.</text>
      <biological_entity constraint="branch" id="o5613" name="stem" name_original="stems" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character constraint="at proximal end" constraintid="o5614" is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5614" name="end" name_original="end" src="d0_s8" type="structure" />
      <biological_entity id="o5615" name="cortex" name_original="cortex" src="d0_s8" type="structure">
        <character constraint="with retort cells" constraintid="o5616" is_modifier="false" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="retort" id="o5616" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o5613" id="r1574" name="with" negation="false" src="d0_s8" to="o5615" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves narrowly ovatelanceolate, greater than 1.2 mm, straight, slightly undulate and weakly recurved when dry, margins entire;</text>
      <biological_entity constraint="branch" id="o5617" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" upper_restricted="false" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o5618" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hyaline cells on convex surface with 1 pore per cell in apical end, on concave surface with round wall-thinnings in the cell ends and angles;</text>
      <biological_entity id="o5619" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o5620" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o5621" name="pore" name_original="pore" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5622" name="cell" name_original="cell" src="d0_s10" type="structure" />
      <biological_entity constraint="apical" id="o5623" name="end" name_original="end" src="d0_s10" type="structure" />
      <biological_entity id="o5624" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o5625" name="wall-thinning" name_original="wall-thinnings" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="cell" id="o5626" name="end" name_original="ends" src="d0_s10" type="structure" />
      <biological_entity id="o5627" name="angle" name_original="angles" src="d0_s10" type="structure" />
      <relation from="o5619" id="r1575" name="on" negation="false" src="d0_s10" to="o5620" />
      <relation from="o5620" id="r1576" name="with" negation="false" src="d0_s10" to="o5621" />
      <relation from="o5621" id="r1577" name="per" negation="false" src="d0_s10" to="o5622" />
      <relation from="o5622" id="r1578" name="in" negation="false" src="d0_s10" to="o5623" />
      <relation from="o5619" id="r1579" name="on" negation="false" src="d0_s10" to="o5624" />
      <relation from="o5624" id="r1580" name="with" negation="false" src="d0_s10" to="o5625" />
      <relation from="o5625" id="r1581" name="in" negation="false" src="d0_s10" to="o5626" />
      <relation from="o5625" id="r1582" name="in" negation="false" src="d0_s10" to="o5627" />
    </statement>
    <statement id="d0_s11">
      <text>chlorophyllous cells in transverse-section triangular to ovate-triangular and well-enclosed on the concave surface.</text>
      <biological_entity id="o5629" name="transverse-section" name_original="transverse-section" src="d0_s11" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s11" to="ovate-triangular" />
      </biological_entity>
      <biological_entity id="o5630" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="concave" value_original="concave" />
      </biological_entity>
      <relation from="o5628" id="r1583" name="in" negation="false" src="d0_s11" to="o5629" />
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o5628" name="bud" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="chlorophyllous" value_original="chlorophyllous" />
        <character constraint="on surface" constraintid="o5630" is_modifier="false" name="position" src="d0_s11" value="well-enclosed" value_original="well-enclosed" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 24–33 µm; finely papillose on the superficial surface.</text>
      <biological_entity id="o5631" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="24" from_unit="um" name="some_measurement" src="d0_s13" to="33" to_unit="um" />
        <character constraint="on superficial surface" constraintid="o5632" is_modifier="false" modifier="finely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="superficial" id="o5632" name="surface" name_original="surface" src="d0_s13" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forming carpets in a wide variety of poor to medium fen habitats of both mire edge and mire wide character, not found in ombrotrophic mires</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="carpets" modifier="forming" constraint="in a wide variety of poor to medium fen habitats of both mire edge and mire" />
        <character name="habitat" value="a wide variety" constraint="of poor to medium fen habitats of both mire edge and mire" />
        <character name="habitat" value="medium fen habitats" constraint="of both mire" />
        <character name="habitat" value="both mire" />
        <character name="habitat" value="poor to medium fen habitats of both mire edge" />
        <character name="habitat" value="mire" />
        <character name="habitat" value="wide character" />
        <character name="habitat" value="ombrotrophic mires" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Nfld. and Labr. (Nfld.), N.S., Que.; Conn., Ind., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., Vt., Va., W.Va.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <discussion>Sporophytes are uncommon in Sphagnum isoviitae. See discussion under 26. S. brevifolium and 28. S. fallax for distinction from these similar species. Sphagnum isoviitae has no range overlap with S. pacificum, the other North American species of the S. recurvum complex with apiculate stem leaves; the sharply recurved branch leaves of the latter, however, would separate it easily in any case. Spore features are those given by Flatberg.</discussion>
  
</bio:treatment>