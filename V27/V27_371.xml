<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ryszard Ochyra</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subfamily">Racomitrioideae</taxon_name>
    <place_of_publication>
      <publication_title>in R. Ochyra et al., Cens. Cat. Polish Mosses,</publication_title>
      <place_in_publication>135. 2003,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily Racomitrioideae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20784</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cladocarpous or rarely acrocarpous.</text>
      <biological_entity id="o7964" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem creeping, ascending, to erect, central strand absent.</text>
      <biological_entity id="o7965" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
      <biological_entity constraint="central" id="o7966" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect or spreading, straight or curved, sometimes recurved when wet, lanceolate to oblong-lanceolate, less often elliptic, ovate to lingulate, keeled to canaliculate-concave;</text>
      <biological_entity id="o7967" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="when wet" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="oblong-lanceolate" />
        <character is_modifier="false" modifier="less often; often" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lingulate keeled" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lingulate keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins recurved to revolute, entire or erose-dentate, serrate or cristate at the apex, costa rarely spurred or forked distally, sometimes ending in mid leaf, subpercurrent or excurrent, smooth or papillose, in transverse-section reniform to elliptical, sometimes semiterete or strongly flattened, with (2–) 3–15 adaxial cells near base, much larger than abaxial cells, often excurrent as an awn, awn smooth, or toothed or papillose or both;</text>
      <biological_entity id="o7968" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="erose-dentate" value_original="erose-dentate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character constraint="at apex" constraintid="o7969" is_modifier="false" name="shape" src="d0_s3" value="cristate" value_original="cristate" />
      </biological_entity>
      <biological_entity id="o7969" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o7970" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="forked" value_original="forked" />
        <character is_modifier="false" modifier="sometimes" name="position" notes="" src="d0_s3" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="semiterete" value_original="semiterete" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character constraint="than abaxial cells" constraintid="o7975" is_modifier="false" name="size" notes="" src="d0_s3" value="much larger" value_original="much larger" />
        <character constraint="as awn" constraintid="o7976" is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="mid" id="o7971" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o7972" name="transverse-section" name_original="transverse-section" src="d0_s3" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s3" to="elliptical" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7973" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s3" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <biological_entity id="o7974" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o7975" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity id="o7976" name="awn" name_original="awn" src="d0_s3" type="structure" />
      <biological_entity id="o7977" name="awn" name_original="awn" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
        <character name="relief" src="d0_s3" value="both" value_original="both" />
      </biological_entity>
      <relation from="o7970" id="r1902" name="ending in" negation="false" src="d0_s3" to="o7971" />
      <relation from="o7970" id="r1903" name="in" negation="false" src="d0_s3" to="o7972" />
      <relation from="o7970" id="r1904" name="with" negation="false" src="d0_s3" to="o7973" />
      <relation from="o7973" id="r1905" name="near" negation="false" src="d0_s3" to="o7974" />
    </statement>
    <statement id="d0_s4">
      <text>laminal cells smooth, pseudopapillose, or papillose;</text>
      <biological_entity constraint="laminal" id="o7978" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="pseudopapillose" value_original="pseudopapillose" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s4" value="pseudopapillose" value_original="pseudopapillose" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal-cells rectangular to linear, nodulose-porose, usually thick-walled, always with spiral thickenings forming a colored strip along the insertion;</text>
      <biological_entity id="o7979" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s5" to="linear" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="nodulose-porose" value_original="nodulose-porose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o7980" name="thickening" name_original="thickenings" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement_or_course" src="d0_s5" value="spiral" value_original="spiral" />
      </biological_entity>
      <biological_entity id="o7981" name="strip" name_original="strip" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="colored" value_original="colored" />
      </biological_entity>
      <relation from="o7979" id="r1906" modifier="always" name="with" negation="false" src="d0_s5" to="o7980" />
      <relation from="o7980" id="r1907" name="forming a" negation="false" src="d0_s5" to="o7981" />
    </statement>
    <statement id="d0_s6">
      <text>mid leaf cells quadrate to elongate, mostly strongly sinuose-nodulose.</text>
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction very rare by gemmae arising from the base of the costa on the abaxial side.</text>
      <biological_entity id="o7983" name="gemma" name_original="gemmae" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" modifier="very" name="quantity" src="d0_s7" value="rare" value_original="rare" />
        <character constraint="from base" constraintid="o7984" is_modifier="false" name="orientation" src="d0_s7" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o7984" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o7985" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o7986" name="side" name_original="side" src="d0_s7" type="structure" />
      <relation from="o7984" id="r1908" name="part_of" negation="false" src="d0_s7" to="o7985" />
      <relation from="o7984" id="r1909" name="on" negation="false" src="d0_s7" to="o7986" />
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="leaf" id="o7982" name="cell" name_original="cells" src="d0_s6" type="structure" constraint_original="mid leaf">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s6" to="elongate" />
        <character is_modifier="false" modifier="mostly strongly" name="shape" src="d0_s6" value="sinuose-nodulose" value_original="sinuose-nodulose" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta usually long, straight or rarely slightly arcuate, smooth or papillose, one to several per perichaetium;</text>
      <biological_entity id="o7987" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="length_or_size" src="d0_s9" value="long" value_original="long" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely slightly" name="course" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
        <character char_type="range_value" constraint="per perichaetium" constraintid="o7988" from="one" name="quantity" src="d0_s9" to="several" />
      </biological_entity>
      <biological_entity id="o7988" name="perichaetium" name_original="perichaetium" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>vaginula with sinuose-nodulose epidermal-cells.</text>
      <biological_entity id="o7989" name="vaginulum" name_original="vaginula" src="d0_s10" type="structure" />
      <biological_entity id="o7990" name="epidermal-cell" name_original="epidermal-cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="sinuose-nodulose" value_original="sinuose-nodulose" />
      </biological_entity>
      <relation from="o7989" id="r1910" name="with" negation="false" src="d0_s10" to="o7990" />
    </statement>
    <statement id="d0_s11">
      <text>Capsule erect, exserted, symmetric, ovoid, obloid to cylindric, usually smooth or obscurely striate;</text>
      <biological_entity id="o7991" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="obloid" name="shape" src="d0_s11" to="cylindric" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s11" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stomates present;</text>
      <biological_entity id="o7992" name="stomate" name_original="stomates" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>annulus present, deciduous;</text>
      <biological_entity id="o7993" name="annulus" name_original="annulus" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum long-rostrate;</text>
      <biological_entity id="o7994" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome mostly with basal membrane and preperistome, equally thickened and weakly trabeculate both adaxially and abaxially, irregularly split into 2–3 branches to the middle or regularly divided into two filaments nearly to the base.</text>
      <biological_entity id="o7995" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="equally" name="size_or_width" notes="" src="d0_s15" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s15" value="trabeculate" value_original="trabeculate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7996" name="membrane" name_original="membrane" src="d0_s15" type="structure" />
      <biological_entity id="o7997" name="preperistome" name_original="preperistome" src="d0_s15" type="structure" />
      <biological_entity id="o7998" name="split" name_original="split" src="d0_s15" type="structure" />
      <biological_entity id="o7999" name="branch" name_original="branches" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="3" />
      </biological_entity>
      <biological_entity id="o8000" name="middle" name_original="middle" src="d0_s15" type="structure" />
      <biological_entity id="o8001" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="regularly" name="shape" src="d0_s15" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o8002" name="base" name_original="base" src="d0_s15" type="structure" />
      <relation from="o7995" id="r1911" name="with" negation="false" src="d0_s15" to="o7996" />
      <relation from="o7995" id="r1912" name="with" negation="false" src="d0_s15" to="o7997" />
      <relation from="o7998" id="r1913" modifier="adaxially; abaxially" name="into" negation="false" src="d0_s15" to="o7999" />
      <relation from="o7999" id="r1914" name="to" negation="false" src="d0_s15" to="o8000" />
      <relation from="o7999" id="r1915" name="to" negation="false" src="d0_s15" to="o8001" />
      <relation from="o8001" id="r1916" name="to" negation="false" src="d0_s15" to="o8002" />
    </statement>
    <statement id="d0_s16">
      <text>Calyptra conic-mitrate, not plicate, often papillose at the apex, covering operculum to 1/2 of capsule.</text>
      <biological_entity id="o8003" name="calyptra" name_original="calyptra" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="conic-mitrate" value_original="conic-mitrate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s16" value="plicate" value_original="plicate" />
        <character constraint="at apex" constraintid="o8004" is_modifier="false" modifier="often" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
        <character char_type="range_value" constraint="of capsule" constraintid="o8006" from="0" name="quantity" src="d0_s16" to="1/2" />
      </biological_entity>
      <biological_entity id="o8004" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity id="o8005" name="operculum" name_original="operculum" src="d0_s16" type="structure" />
      <biological_entity id="o8006" name="capsule" name_original="capsule" src="d0_s16" type="structure" />
      <relation from="o8003" id="r1917" name="covering" negation="false" src="d0_s16" to="o8005" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15b.</number>
  <discussion>Genera 4, species ca. 75 (4 genera, 28 species in the flora).</discussion>
  <discussion>Subfamily Racomitrioideae is characterized by a Racomitrium-type peristome, consistently sinuose-nodulose walls of the laminal cells and epidermal cells of the vaginula, absence of stem central strand, non-plicate calyptrae, and cladocarpous arrangement of the perichaetia. Taxa belonging to this subfamily have sometimes been associated with Ptychomitrium and Campylostelium, and placed in the subfamily Ptychomitrioideae in the Grimmiaceae. Despite their overall morphological similarity, these taxa seem to be only remotely related. In Ptychomitrium the laminal cell walls are straight or weakly sinuose, the calyptrae deeply plicate, and the plants acrocarpous. In addition, the preperistome is absent, and the peristome teeth lack trabeculae and have a characteristic air gap at their base. Moreover, Ptychomitrium is cryptoicous because the male branches are small and arise from the base of vaginula inside the perichaetial leaf circle. The Racomitrioideae consists of four genera that are segregates from the large and heterogeneous Racomitrium in the broad sense.</discussion>
  <references>
    <reference>Allen, B. H. 1994b. The Grimmiaceae (Musci) in Maine. III. Racomitrium. Evansia 11: 41–54.</reference>
    <reference>Frye, T. C. 1917–1918. The rhacomitriums of western North America. Bryologist 20: 91–98; 21: 1–16.</reference>
    <reference>Lawton, E. 1972. The genus Rhacomitrium in America and Japan. J. Hattori Bot. Lab. 35: 252–262.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Laminal cells smooth or pseudopapillose; peristome teeth short, divided to the middle, rarely deeper, into 2-3 irregular prongs.</description>
      <determination>6 Bucklandiella</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Laminal cells papillose; peristome teeth long, split at least to the middle into 2(-3) filiform, ± regular filaments</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Laminal cells with tall, conical papillae situated over the lumina; alar cells hyaline or yellowish hyaline, thin-walled, forming prominent, decurrent auricles.</description>
      <determination>7 Niphotrichum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Laminal cells with large, flat papillae situated over the longitudinal walls; alar cells absent or distinct, brown to yellowish orange, not hyaline, thick-walled</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hyaline hair-point always present, usually long, strongly papillose eroso-dentate, long decurrent down the leaf margins; seta papillose, sinistrorse when dry; costa percurrent, unbranched; capsule slightly ventricose at base; calyptra smooth or minutely roughened.</description>
      <determination>8 Racomitrium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hyaline hair-point absent or present, rarely long, smooth to denticulate, never papillose or decurrent; seta smooth, dextrorse when dry (in C. fascicularis and C. corrugatus only once twisted to the left immediately below the capsule and proximally twisted to the left); costa ending well before the apex, often branched and spurred distally; capsule never bulging at base; calyptra distinctly verrucose to papillose.</description>
      <determination>9 Codriophorus</determination>
    </key_statement>
  </key>
</bio:treatment>