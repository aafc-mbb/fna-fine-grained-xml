<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="treatment_page">384</other_info_on_meta>
    <other_info_on_meta type="illustration_page">385</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Schimper" date="1856" rank="genus">dichodontium</taxon_name>
    <taxon_name authority="Renauld &amp; Cardot" date="1892" rank="species">olympicum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>17: 296. 1892,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dichodontium;species olympicum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075578</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.4–0.8 cm.</text>
      <biological_entity id="o2890" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s0" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 1–2.2 mm, apex rounded-obtuse to obtusely acute, occasionally truncate;</text>
      <biological_entity id="o2891" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2892" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded-obtuse" name="shape" src="d0_s1" to="obtusely acute" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s1" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaf margins mostly plane at base, rarely some leaves recurved, prominently papillose near insertion, margins of leaf apex strongly papillose but without teeth;</text>
      <biological_entity constraint="leaf" id="o2893" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="at base" constraintid="o2894" is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o2894" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o2895" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="prominently" name="insertion" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o2896" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="without teeth" constraintid="o2898" is_modifier="false" modifier="strongly" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o2897" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o2898" name="tooth" name_original="teeth" src="d0_s2" type="structure" />
      <relation from="o2896" id="r665" name="part_of" negation="false" src="d0_s2" to="o2897" />
    </statement>
    <statement id="d0_s3">
      <text>laminal cells at margins strongly papillose by projecting distal cell ends to within 6–10 cells of the insertion;</text>
      <biological_entity constraint="laminal" id="o2899" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity id="o2900" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="by distal cell ends" constraintid="o2901" is_modifier="false" modifier="strongly" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="cell" id="o2901" name="end" name_original="ends" src="d0_s3" type="structure" constraint_original="distal cell">
        <character is_modifier="true" name="orientation" src="d0_s3" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o2902" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <relation from="o2899" id="r666" name="at" negation="false" src="d0_s3" to="o2900" />
      <relation from="o2901" id="r667" name="to" negation="false" src="d0_s3" to="o2902" />
    </statement>
    <statement id="d0_s4">
      <text>median leaf cells with horned (sometimes branched) papillae on both surfaces;</text>
      <biological_entity constraint="leaf" id="o2903" name="cell" name_original="cells" src="d0_s4" type="structure" constraint_original="median leaf" />
      <biological_entity id="o2904" name="papilla" name_original="papillae" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="horned" value_original="horned" />
      </biological_entity>
      <biological_entity id="o2905" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <relation from="o2903" id="r668" name="with" negation="false" src="d0_s4" to="o2904" />
      <relation from="o2904" id="r669" name="on" negation="false" src="d0_s4" to="o2905" />
    </statement>
    <statement id="d0_s5">
      <text>costal cells adaxially quadrate or short-rectangular (1–2:1), papillose, scarcely different from adjacent laminal cells.</text>
      <biological_entity id="o2907" name="laminal" name_original="laminal" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o2908" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o2906" id="r670" modifier="scarcely" name="from" negation="false" src="d0_s5" to="o2907" />
      <relation from="o2906" id="r671" modifier="scarcely" name="from" negation="false" src="d0_s5" to="o2908" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction unknown.</text>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition autoicous, perigonium located just below the perichaetium.</text>
      <biological_entity constraint="costal" id="o2906" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity id="o2909" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="autoicous" value_original="autoicous" />
      </biological_entity>
      <biological_entity id="o2910" name="perigonium" name_original="perigonium" src="d0_s7" type="structure" />
      <biological_entity id="o2911" name="perichaetium" name_original="perichaetium" src="d0_s7" type="structure" />
      <relation from="o2910" id="r672" name="just below" negation="false" src="d0_s7" to="o2911" />
    </statement>
    <statement id="d0_s8">
      <text>Seta 4–8 mm.</text>
      <biological_entity id="o2912" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule 1–1.4 mm, strumose, irregularly furrowed when dry.</text>
      <biological_entity id="o2913" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="strumose" value_original="strumose" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s9" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores 10–15 µm.</text>
      <biological_entity id="o2914" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s10" to="15" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer–fall (Jul–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="fall" from="summer" />
        <character name="capsules maturing time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil or soil over rock, montane areas, especially associated with melting snow</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" constraint="over rock , montane areas , especially associated with melting snow" />
        <character name="habitat" value="soil" constraint="over rock , montane areas , especially associated with melting snow" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="montane areas" />
        <character name="habitat" value="melting snow" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1000-2200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1000" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Idaho, Mont., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Rarely, one or both leaf margins may be recurved to various extents in some of the larger leaves of Dichodontium olympicum. The costa near the leaf insertion is 50–62 µm across, while that of D. pellucidum varies from 30 to 100 µm. The leaves are strongly papillose-mammillose. The margins are evenly denticulate, whereas in D. pellucidum similar denticulations are interrupted at intervals by teeth with the outer edge curving toward the apex. The papillae salients tend to become more prominent on the leaf margins approaching the leaf insertion. There does not appear to be a hydroid strand in the costa in the proximal leaf section, which could help differentiate this species from D. pellucidum, which often possesses this feature in robust (longer) stems and leaves.</discussion>
  <discussion>Dichodontium olympicum is often cited as dioicous in past literature, while J.-P. Frahm et al. (1998) correctly noted its autoicous sexuality but did not include this useful character in their key to species. Since the sexuality of the genus is often said to in part differentiate Dichodontium from other genera, such as Cynodontium, which has autoicous species, one wonders how artificial Dichodontium and other closely related genera are, as several characters, such as strumiferous capsules and the striking “horned” papillae and variously undifferentiated costa anatomy, seem to vary independently of generic limits.</discussion>
  <discussion>Dichodontium integrum Sakurai has been cited by Gao C. et al. (1999) for the flora of China, without observation of specimens, but indicating that the species, based on a translation of part of the protologue, “is similar to the North American Dichodontium olympicum.”  The description given, however, lacks the essential distinctions of D. olympicum. Further inquiry is needed.</discussion>
  
</bio:treatment>