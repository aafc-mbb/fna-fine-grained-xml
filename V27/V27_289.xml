<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">214</other_info_on_meta>
    <other_info_on_meta type="treatment_page">215</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="H. H. Blom ex B. H. Allen" date="2005" rank="species">crassithecium</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>93: 254, figs. 135, 136. 2005,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species crassithecium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075460</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in compact cushions or tufts, blackish olivaceous.</text>
      <biological_entity id="o7522" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="blackish olivaceous" value_original="blackish olivaceous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7523" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o7524" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o7522" id="r2117" name="in" negation="false" src="d0_s0" to="o7523" />
      <relation from="o7522" id="r2118" name="in" negation="false" src="d0_s0" to="o7524" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.4–1.2 cm, central strand distinct.</text>
      <biological_entity id="o7525" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s1" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o7526" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect or erect-incurved, sometimes with apices recurved when dry, ovatelanceolate, keeled distally, 1.5–2 mm, 1-stratose or sometimes 2-stratose in striae distally;</text>
      <biological_entity id="o7527" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-incurved" value_original="erect-incurved" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
        <character constraint="in striae" constraintid="o7529" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o7528" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o7529" name="stria" name_original="striae" src="d0_s2" type="structure" />
      <relation from="o7527" id="r2119" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o7528" />
    </statement>
    <statement id="d0_s3">
      <text>margins plane throughout or sometimes narrowly recurved proximally, smooth, 2-stratose or multistratose distally;</text>
      <biological_entity id="o7530" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes narrowly; sometimes narrowly; proximally" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="multistratose" value_original="multistratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apices acute, usually ending in a short, fleshy, multistratose apiculus, usually rounded, but sometimes acute and hyaline-tipped;</text>
      <biological_entity id="o7531" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="usually; usually" name="shape" notes="" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="hyaline-tipped" value_original="hyaline-tipped" />
      </biological_entity>
      <biological_entity id="o7532" name="apiculu" name_original="apiculus" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="multistratose" value_original="multistratose" />
      </biological_entity>
      <relation from="o7531" id="r2120" name="ending in" negation="false" src="d0_s4" to="o7532" />
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent or short-excurrent, smooth;</text>
      <biological_entity id="o7533" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-excurrent" value_original="short-excurrent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate or short-rectangular;</text>
      <biological_entity constraint="basal marginal" id="o7534" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells rounded, subquadrate, or short-rectangular, 5–10 µm wide, sometimes weakly bulging-mammillose, straight or weakly sinuose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="distal" id="o7535" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s7" to="10" to_unit="um" />
        <character is_modifier="false" modifier="sometimes weakly" name="relief" src="d0_s7" value="bulging-mammillose" value_original="bulging-mammillose" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule light-brown, cupulate, or short-cylindric, 0.7–1 mm, sometimes lightly furrowed when dry;</text>
      <biological_entity id="o7536" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s9" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells mostly irregularly elongate, mixed with patches of unevenly angular cells, very thick-walled;</text>
      <biological_entity constraint="exothecial" id="o7537" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="mostly irregularly" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character constraint="with patches" constraintid="o7538" is_modifier="false" name="arrangement" src="d0_s10" value="mixed" value_original="mixed" />
        <character is_modifier="false" modifier="very" name="architecture" notes="" src="d0_s10" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o7538" name="patch" name_original="patches" src="d0_s10" type="structure" />
      <biological_entity id="o7539" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="unevenly" name="arrangement_or_shape" src="d0_s10" value="angular" value_original="angular" />
      </biological_entity>
      <relation from="o7538" id="r2121" name="part_of" negation="false" src="d0_s10" to="o7539" />
    </statement>
    <statement id="d0_s11">
      <text>rim red;</text>
      <biological_entity id="o7540" name="rim" name_original="rim" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stomata present;</text>
      <biological_entity id="o7541" name="stoma" name_original="stomata" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome squarrose, 250–300 µm, reddish orange, densely to finely papillose, strongly perforated.</text>
      <biological_entity id="o7542" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="squarrose" value_original="squarrose" />
        <character char_type="range_value" from="250" from_unit="um" name="some_measurement" src="d0_s13" to="300" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" modifier="densely to finely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s13" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 14–16 µm, smooth or granulose.</text>
      <biological_entity id="o7543" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s14" to="16" to_unit="um" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="granulose" value_original="granulose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone rocks, often near streams and rivers but also in glades and on exposed bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocks" modifier="limestone" />
        <character name="habitat" value="streams" modifier="often near" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="exposed bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (200-400 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="200" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ark., Ill., Kans., Maine, Mo., Nebr., Ohio, Pa., Tex., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Schistidium crassithecium is one of the most easily recognized species of the genus. Even though usually sterile, its thick leaf margins, appearing club-like in transverse-section, but resembling marginal curvatures macroscopically, and the fleshy apiculi that terminate the leaves are distinctive characters. The fleshy apiculus, its laminal areolation, and features of the sporophytes appear to relate this species to S. cinclidodonteum and S. occidentale.</discussion>
  
</bio:treatment>