<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="treatment_page">95</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Wilson" date="1855" rank="section">acutifolia</taxon_name>
    <taxon_name authority="Dozy &amp; Molkenboer" date="1854" rank="species">junghuhnianum</taxon_name>
    <place_of_publication>
      <publication_title>Verh. Kon. Akad. Wetensch., Afd. Natuurk.</publication_title>
      <place_in_publication>2: 8. 1854,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section acutifolia;species junghuhnianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200000808</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="Warnstorf" date="unknown" rank="species">junghuhnianum</taxon_name>
    <taxon_name authority="(Warnstorf) H. Suzuki" date="unknown" rank="subspecies">pseudomolle</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species junghuhnianum;subspecies pseudomolle;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pseudomolle</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species pseudomolle;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate-sized, soft, loosely tufted, slender, capitulum flattopped to rounded;</text>
      <biological_entity id="o3729" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="moderate-sized" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>pale, dirty green, yellowish to brownish;</text>
    </statement>
    <statement id="d0_s2">
      <text>without metallic lustre when dry.</text>
      <biological_entity id="o3730" name="capitulum" name_original="capitulum" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="dirty green" value_original="dirty green" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s1" to="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems brown to reddish-brown;</text>
      <biological_entity id="o3731" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s3" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>superficial cortical cells usually aporose, but some have a single round to elliptical pore in the distal portion of the cell free from the cell wall.</text>
      <biological_entity constraint="superficial cortical" id="o3732" name="bud" name_original="cells" src="d0_s4" type="structure" />
      <biological_entity id="o3733" name="pore" name_original="pore" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="single" value_original="single" />
        <character char_type="range_value" from="round" is_modifier="true" name="shape" src="d0_s4" to="elliptical" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3734" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character constraint="from cell wall" constraintid="o3736" is_modifier="false" name="fusion" src="d0_s4" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o3735" name="cell" name_original="cell" src="d0_s4" type="structure" />
      <biological_entity constraint="cell" id="o3736" name="wall" name_original="wall" src="d0_s4" type="structure" />
      <relation from="o3732" id="r999" name="have a" negation="false" src="d0_s4" to="o3733" />
      <relation from="o3732" id="r1000" name="in" negation="false" src="d0_s4" to="o3734" />
      <relation from="o3734" id="r1001" name="part_of" negation="false" src="d0_s4" to="o3735" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves triangular-lingulate, 1.2–1.6 mm, broadly apex acute to narrowly truncate and toothed, border narrow or indistinct at base (less than 0.25 the width);</text>
      <biological_entity id="o3737" name="leaf-stem" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular-lingulate" value_original="triangular-lingulate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="distance" src="d0_s5" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3738" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="narrowly truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o3739" name="border" name_original="border" src="d0_s5" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
        <character constraint="at base" constraintid="o3740" is_modifier="false" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o3740" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>hyaline cells rhomboid, mostly 0–1-septate;</text>
      <biological_entity id="o3741" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s6" value="0-1-septate" value_original="0-1-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>convex surface with membrane pleats, concave surface with 1–3 rounded membrane gaps occupying most of cell.</text>
      <biological_entity id="o3742" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="membrane" id="o3743" name="pleat" name_original="pleats" src="d0_s7" type="structure" />
      <biological_entity id="o3744" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity constraint="membrane" id="o3745" name="gap" name_original="gaps" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="true" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o3746" name="cell" name_original="cell" src="d0_s7" type="structure" />
      <relation from="o3742" id="r1002" name="with" negation="false" src="d0_s7" to="o3743" />
      <relation from="o3744" id="r1003" name="with" negation="false" src="d0_s7" to="o3745" />
      <relation from="o3745" id="r1004" name="occupying most" negation="false" src="d0_s7" to="o3746" />
    </statement>
    <statement id="d0_s8">
      <text>Branches somewhat 5-ranked.</text>
      <biological_entity id="o3747" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s8" value="5-ranked" value_original="5-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch fascicles with 2 spreading and 1–2 pendent branches.</text>
      <biological_entity id="o3748" name="branch" name_original="branch" src="d0_s9" type="structure">
        <character constraint="with branches" constraintid="o3749" is_modifier="false" name="arrangement" src="d0_s9" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o3749" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Branch leaves ovatelanceolate, 1.3–2 mm, strongly concave, apex strongly involute;</text>
      <biological_entity constraint="branch" id="o3750" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o3751" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s10" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>margins entire to somewhat toothed near apex, hyaline cells on convex surface with numerous ringed elliptic pores (6–10) along commissures, concave surface mostly aporose except near margins;</text>
      <biological_entity id="o3752" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire to somewhat" value_original="entire to somewhat" />
        <character constraint="near apex" constraintid="o3753" is_modifier="false" modifier="somewhat" name="shape" src="d0_s11" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o3753" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity id="o3754" name="bud" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o3755" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o3756" name="pore" name_original="pores" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="relief" src="d0_s11" value="ringed" value_original="ringed" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s11" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" constraint="along commissures" constraintid="o3757" from="6" name="atypical_quantity" src="d0_s11" to="10" />
      </biological_entity>
      <biological_entity id="o3757" name="commissure" name_original="commissures" src="d0_s11" type="structure" />
      <biological_entity id="o3759" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <relation from="o3754" id="r1005" name="on" negation="false" src="d0_s11" to="o3755" />
      <relation from="o3755" id="r1006" name="with" negation="false" src="d0_s11" to="o3756" />
      <relation from="o3758" id="r1007" name="except" negation="false" src="d0_s11" to="o3759" />
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous or monoicous.</text>
      <biological_entity id="o3758" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="concave" value_original="concave" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="monoicous" value_original="monoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 21–23 µm; minutely papillose.</text>
      <biological_entity id="o3760" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="21" from_unit="um" name="some_measurement" src="d0_s13" to="23" to_unit="um" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shady, seepy cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seepy cliffs" modifier="shady" />
        <character name="habitat" value="shady" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>76.</number>
  <discussion>Sphagnum junghuhnianum in the flora area is known only from the Queen Charlotte Islands.</discussion>
  <discussion>Sporophytes of Sphagnum junghuhnianum were not seen. Three other large, brown species of sect. Acutifolia have stem leaves without fimbriate to lacerate apices, S. subnitens (forms without red color), S. subfulvum, and S. flavicomans. Sphagnum flavicomans has a more pointed stem leaf and a darker brown color as well as a strongly different ecology and range. Both S. subnitens and S. subfulvum have a glossy sheen when dry that is lacking in S. junghuhnianum. Sexual condition and spore characters were taken from H. A. Crum (1984).</discussion>
  
</bio:treatment>