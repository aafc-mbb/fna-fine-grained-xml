<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Rodney D. Seppelt,Robert R. Ireland Jr.,Harold Robinson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">463</other_info_on_meta>
    <other_info_on_meta type="mention_page">467</other_info_on_meta>
    <other_info_on_meta type="treatment_page">450</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Limpricht" date="unknown" rank="family">ditrichaceae</taxon_name>
    <taxon_name authority="Hampe" date="1867" rank="genus">DITRICHUM</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>50: 181. 1867,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ditrichaceae;genus DITRICHUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek di-, two, and trichos, hair, alluding to peristome split longitudinally into two segments</other_info_on_name>
    <other_info_on_name type="fna_id">110696</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose to dense tufts, green to yellowish green distally, yellowbrown to brown proximally.</text>
      <biological_entity id="o8417" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" modifier="proximally" name="coloration" notes="" src="d0_s0" to="yellowish green distally" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8418" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o8417" id="r2015" name="in" negation="false" src="d0_s0" to="o8418" />
    </statement>
    <statement id="d0_s1">
      <text>Stems short or reaching 2 cm or more, simple or sometimes with a few branches;</text>
      <biological_entity id="o8419" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character name="height_or_length_or_size" src="d0_s1" value="reaching 2 cm" value_original="reaching 2 cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s1" value="sometimes" value_original="sometimes" />
      </biological_entity>
      <biological_entity id="o8420" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="few" value_original="few" />
      </biological_entity>
      <relation from="o8419" id="r2016" name="with" negation="false" src="d0_s1" to="o8420" />
    </statement>
    <statement id="d0_s2">
      <text>rhizoids at base, smoth.</text>
      <biological_entity id="o8421" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure" />
      <biological_entity id="o8422" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o8421" id="r2017" name="at" negation="false" src="d0_s2" to="o8422" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves rigid to flexuose or sometimes somewhat falcate when dry, erect-spreading when wet, lanceolate to subulate from a more or less sheathing base;</text>
      <biological_entity id="o8423" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="course" src="d0_s3" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="when wet" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" constraint="from base" constraintid="o8424" from="lanceolate" name="shape" src="d0_s3" to="subulate" />
      </biological_entity>
      <biological_entity id="o8424" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire throughout or denticulate near the apex;</text>
      <biological_entity id="o8425" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character constraint="near apex" constraintid="o8426" is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o8426" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent or excurrent, occupying most of subula, 1/6–1/3 width of leaf base, 1 row of guide cells, 2 stereid bands, adaxial stereid band sometimes weak, rarely absent;</text>
      <biological_entity id="o8427" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="1/6 width of leaf base" name="width" src="d0_s5" to="1/3 width of leaf base" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8428" name="subulum" name_original="subula" src="d0_s5" type="structure" />
      <biological_entity id="o8429" name="row" name_original="row" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8430" name="guide" name_original="guide" src="d0_s5" type="structure" />
      <biological_entity id="o8431" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o8432" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o8433" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity id="o8434" name="band" name_original="band" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="fragility" src="d0_s5" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o8427" id="r2018" name="occupying most" negation="false" src="d0_s5" to="o8428" />
      <relation from="o8429" id="r2019" name="part_of" negation="false" src="d0_s5" to="o8430" />
      <relation from="o8429" id="r2020" name="part_of" negation="false" src="d0_s5" to="o8431" />
    </statement>
    <statement id="d0_s6">
      <text>medial lamina cells quadrate to shortrectangular, becoming longer and thinner-walled proximally toward margins, smooth or rarely papillose at both ends.</text>
      <biological_entity id="o8436" name="shortrectangular" name_original="shortrectangular" src="d0_s6" type="structure" />
      <biological_entity id="o8437" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o8438" name="end" name_original="ends" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction occasionally by rhizoidal tubers.</text>
      <biological_entity id="o8439" name="rhizoidal" name_original="rhizoidal" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity id="o8440" name="tuber" name_original="tubers" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition monoicous or dioicous;</text>
      <biological_entity constraint="lamina" id="o8435" name="cell" name_original="cells" src="d0_s6" type="structure" constraint_original="medial lamina">
        <character constraint="to shortrectangular" constraintid="o8436" is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="becoming" name="length_or_size" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character constraint="toward margins" constraintid="o8437" is_modifier="false" name="architecture" src="d0_s6" value="thinner-walled" value_original="thinner-walled" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character constraint="at ends" constraintid="o8438" is_modifier="false" modifier="rarely" name="relief" src="d0_s6" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="monoicous" value_original="monoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perichaetial leaves usually with a longer and more or less sheathing base and shorter subulate than stem-leaves.</text>
      <biological_entity constraint="perichaetial" id="o8441" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o8442" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character is_modifier="true" modifier="more or less" name="architecture_or_shape" src="d0_s9" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o8443" name="stem-leaf" name_original="stem-leaves" src="d0_s9" type="structure" />
      <relation from="o8441" id="r2021" name="with" negation="false" src="d0_s9" to="o8442" />
    </statement>
    <statement id="d0_s10">
      <text>Seta pale-yellow to dark reddish-brown, elongate, erect or flexuose.</text>
      <biological_entity id="o8444" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s10" to="dark reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s10" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule mostly erect and symmetric, sometimes ± inclined and arcuate, exserted, ovoid to cylindric, smooth;</text>
      <biological_entity id="o8445" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="sometimes more or less" name="orientation" src="d0_s11" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="course_or_shape" src="d0_s11" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s11" to="cylindric" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>annulus present, deciduous;</text>
      <biological_entity id="o8446" name="annulus" name_original="annulus" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>operculum conic to short-rostrate;</text>
      <biological_entity id="o8447" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s13" to="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome single, teeth16, split into 2 filiform segments or sometimes irregularly perforate or split, with or without a short basal membrane, papillose to spiculose.</text>
      <biological_entity id="o8448" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o8449" name="split" name_original="split" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" notes="" src="d0_s14" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="spiculose" value_original="spiculose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8450" name="membrane" name_original="membrane" src="d0_s14" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
      <relation from="o8449" id="r2022" name="into 2 filiform segments or sometimes irregularly perforate or split , with or without" negation="false" src="d0_s14" to="o8450" />
    </statement>
    <statement id="d0_s15">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o8451" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores globose, very finely papillose, verrucose, or with somewhat vermicular ornamentation.</text>
      <biological_entity id="o8452" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="very finely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s16" value="verrucose" value_original="verrucose" />
      </biological_entity>
      <biological_entity id="o8453" name="vermicular" name_original="vermicular" src="d0_s16" type="structure" />
      <relation from="o8452" id="r2023" name="with" negation="false" src="d0_s16" to="o8453" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, including maritime Antarctic region.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="including maritime Antarctic region" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Species ca. 90 (11 in the flora).</discussion>
  <discussion>Ditrichum occurs from near sea level up to montane regions, on a wide range of soils, but is found occasionally on rock; some species are calciphilic.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants densely tufted, stems ± tomentose proximally</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants loosely to densely tufted or gregarious; stems, if tomentose, only so at extreme base</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems 1-4 cm; leaves to 3 mm, from an ovate-sheathing base sharply contracted to the subula; costa abaxially strongly convex; lamina cells near costa with weakly nodulose longitudinal walls; plants commonly fruiting.</description>
      <determination>2 Ditrichum flexicaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems to 7 cm or more; leaves from an elongate-ovate base tapering gradually to the long slender subula; costa abaxially weakly convex; basal laminal cells with weakly to strongly nodulose longitudinal walls; rarely found fruiting</description>
      <determination>3 Ditrichum gracile</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">In cross section, distal leaf lamina partially 1-stratose with 2-stratose margins</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">In cross section, distal leaf lamina 2-stratose from costa to margins</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves erect-appressed to erect-patent when moist</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves erect-spreading when moist</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves oblong-lanceolate, widest proximally to middle, tapering to a blunt apex; lamina cells thin-walled, rectangular throughout, slightly shorter and narrower towards apex; seta to 2.5 cm, yellowish to orange-brown, erect; peristome teeth 200-220 µm, perforate, divided into 2 adhering filaments, finely papillose.</description>
      <determination>5 Ditrichum lineare</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves lanceolate to linear-lanceolate, gradually acuminate; lamina cells ± incrassate, subquadrate to short-rectangular distally, elongate-rectangular proximally; seta to 1.5 cm, reddish with age; peristome teeth 200-250 µm, the filaments finely and obliquely ridged and lightly papillose.</description>
      <determination>8 Ditrichum pusillum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems fastigiately branched from base, dichotomous distally; leaves linear-lanceolate, margins subserrulate distally, apex coarsely toothed; seta to 2.5 cm, pale yellow; capsule straight and erect or slightly curved, orange- to dark red-brown at maturity, elliptical, narrowed at mouth, 2-3 mm, flattened when dry; peristome teeth 600-800 µm, spiculose-papillose; autoicous.</description>
      <determination>6 Ditrichum montanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stem branching distally from base; leaves lanceolate to linear-lanceolate, margins entire to serrulate distally, apex weakly toothed; seta shorter, 0.8-2 cm, red to orange-brown; capsule symmetric to slightly curved, 1-3 mm, not flattened when dry; peristome teeth 200-500 µm, papillose to spiculose; dioicous</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems to 2 cm, often branched; leaves 1.5-4.5 mm, apex entire or serrulate, margins broadly recurved from base to leaf middle; leaf cross section 2-stratose on margins and sometimes near costa; seta 1.5-2 cm, red; capsule usually symmetric, straight and erect, rarely curved, 1.5-3 mm long, dark brown to reddish; peristome teeth 200-500 µm, twisted when dry.</description>
      <determination>1 Ditrichum ambiguum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems to 0.5 cm, seldom branched; leaves 1-3 mm, apex serrulate, margins plane or narrowly recurved from above leaf base to leaf middle; leaf cross section 2-stratose only at or near margins; seta 0.8-1.2 cm, orange-yellow, brownish or reddish; capsule usually asymmetric, erect, curved, 1-2 mm, yellow or light brown; peristome teeth 200 µm, nearly straight.</description>
      <determination>11 Ditrichum tortuloides</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stem leaves 1.5-3 mm, from an ovate to oblong base gradually tapered to a channelled subula, erect-patent to subsecund; dioicous.</description>
      <determination>4 Ditrichum heteromallum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stem leaves longer, 3-7 mm, long-subulate from an ovate to short-rectangular base, spreading, flexuose to subsecund; autoicous</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves to 5 mm; seta to 2.5 cm, orange-yellow, becoming reddened at maturity; capsule ovoid-cylindric, ± asymmetric and slightly curved, pale brown, suberect, flattened when dry; peristome teeth to 1500 µm, densely spiculose-papillose; spores yellow-brown, 11-19 µm, vermicular papillose-verrucose</description>
      <determination>9 Ditrichum rhynchostegium</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves 3-7 mm; seta 1-3 cm, yellow or becoming reddish brown at the base when mature; capsules subcylindric to cylindric, not flattened when dry; peristome teeth to 800 µm, finely papillose to spiculose-papillose; spores brown, 15-30 µm, coarsely roughened-papillose</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves long-subulate from a short-ovoid sheathing base; seta to 2.5 cm, yellow, becoming reddened at the base with maturity; capsule suberect, subcylindric, 1-2.5 mm; peristome teeth pale brown to orange-brown, 300-800 µm, spiculose- papillose; spores 15-30 µm.</description>
      <determination>7 Ditrichum pallidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves narrow filiform-subulate from an ovate-lanceolate sheathing base; seta 1-3 cm, yellow; capsule cylindric, 1.5-3 mm; peristome teeth pale yellow-brown, to 300 µm, irregularly perforate, finely papillose; spores 20-30 µm.</description>
      <determination>10 Ditrichum schimperi</determination>
    </key_statement>
  </key>
</bio:treatment>