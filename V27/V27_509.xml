<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jan-Peter Frahm</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="treatment_page">363</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Cardot" date="1908" rank="genus">CAMPYLOPODIELLA</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Herb. Boissier, sér.</publication_title>
      <place_in_publication>2, 8: 90. 1908  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus CAMPYLOPODIELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Campylopus and Latin -ella, diminutive</other_info_on_name>
    <other_info_on_name type="fna_id">105470</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose to compact tufts, 5–30 mm.</text>
      <biological_entity id="o2860" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s0" to="30" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2861" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character char_type="range_value" from="loose" is_modifier="true" name="architecture" src="d0_s0" to="compact" />
      </biological_entity>
      <relation from="o2860" id="r656" name="in" negation="false" src="d0_s0" to="o2861" />
    </statement>
    <statement id="d0_s1">
      <text>Stems radiculose.</text>
      <biological_entity id="o2862" name="stem" name_original="stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-patent, lanceolate, ending in a long acumen, entire or with a few teeth at tips;</text>
      <biological_entity id="o2863" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-patent" value_original="erect-patent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o2864" name="acumen" name_original="acumen" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o2865" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o2866" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <relation from="o2863" id="r657" name="ending" negation="false" src="d0_s2" to="o2864" />
      <relation from="o2863" id="r658" name="ending" negation="false" src="d0_s2" to="o2865" />
      <relation from="o2863" id="r659" name="ending" negation="false" src="d0_s2" to="o2866" />
    </statement>
    <statement id="d0_s3">
      <text>costa filling 1/2–2/3 of the leaf base, excurrent, in transverse-section with large adaxial and abaxial hyalocysts, a median band of chlorocysts and 2–4 adaxial stereids;</text>
      <biological_entity id="o2867" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="of leaf base" constraintid="o2868" from="1/2" name="quantity" src="d0_s3" to="2/3" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o2868" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o2869" name="transverse-section" name_original="transverse-section" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial and abaxial" id="o2870" name="hyalocyst" name_original="hyalocysts" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o2872" name="chlorocyst" name_original="chlorocysts" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o2873" name="stereid" name_original="stereids" src="d0_s3" type="structure" />
      <relation from="o2867" id="r660" name="in" negation="false" src="d0_s3" to="o2869" />
      <relation from="o2869" id="r661" name="with" negation="false" src="d0_s3" to="o2870" />
      <relation from="o2871" id="r662" name="part_of" negation="false" src="d0_s3" to="o2872" />
    </statement>
    <statement id="d0_s4">
      <text>alar cells weakly developed;</text>
      <biological_entity constraint="chlorocyst" id="o2871" name="band" name_original="band" src="d0_s3" type="structure" constraint_original="median chlorocyst; chlorocyst">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o2874" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="weakly" name="development" src="d0_s4" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>laminal cells long-rectangular.</text>
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="laminal" id="o2875" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="long-rectangular" value_original="long-rectangular" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perigonial leaves from broader base suddenly contracted into a slender subula.</text>
      <biological_entity constraint="perigonial" id="o2876" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="broader" id="o2877" name="base" name_original="base" src="d0_s7" type="structure">
        <character constraint="into slender subula" constraintid="o2878" is_modifier="false" modifier="suddenly" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="slender" id="o2878" name="subulum" name_original="subula" src="d0_s7" type="structure" />
      <relation from="o2876" id="r663" name="from" negation="false" src="d0_s7" to="o2877" />
    </statement>
    <statement id="d0_s8">
      <text>Seta yellowish or brownish in age, erect, twisted distally.</text>
      <biological_entity id="o2879" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
        <character constraint="in age" constraintid="o2880" is_modifier="false" name="coloration" src="d0_s8" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o2880" name="age" name_original="age" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Capsule erect, elliptic to cylindric, yellowish, without stomata;</text>
      <biological_entity id="o2881" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="cylindric" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o2882" name="stoma" name_original="stomata" src="d0_s9" type="structure" />
      <relation from="o2881" id="r664" name="without" negation="false" src="d0_s9" to="o2882" />
    </statement>
    <statement id="d0_s10">
      <text>operculum long-rostrate;</text>
      <biological_entity id="o2883" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>annulus present;</text>
      <biological_entity id="o2884" name="annulus" name_original="annulus" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome teeth 16, divided nearly to the base.</text>
      <biological_entity constraint="peristome" id="o2885" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
        <character constraint="to base" constraintid="o2886" is_modifier="false" name="shape" src="d0_s12" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o2886" name="base" name_original="base" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Calyptra cucullate, fimbriate or entire at base.</text>
      <biological_entity id="o2887" name="calyptra" name_original="calyptra" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="fimbriate" value_original="fimbriate" />
        <character constraint="at base" constraintid="o2888" is_modifier="false" name="shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2888" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Spores 11–19 µm.</text>
      <biological_entity id="o2889" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s14" to="19" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Species 4 (2 in the flora).</discussion>
  <discussion>The species listed here were previously included in the genus Atractylocarpus Mitten because of considerable confusion involved with the use of that generic name (J.-P. Frahm 2000). This was caused by the fact that the type species of Atractylocarpus turned out to belong to a genus later described as Campylopodiella. Therefore Frahm and P. Isoviita (1986) proposed conservation of the name Atractylocarpus, which was, however, rejected by the nomenclature committee at the International Botanical Congress in Tokyo. That decision was later corrected at the Congress in St. Louis, thus preserving the use of the name as proposed before, making new combinations unnecessary.</discussion>
  <discussion>Campylopodiella comprises three species in the Himalayas, Central America, and northern South America, of which two have been found also in the flora area, each with one record. One species is also known from Eocene Baltic amber, which is presumably identical with the extant species from the Himalayas. The genus is characterized by a unique transverse section of the costa with a median band of chlorocysts and a few ventral stereids, which can be seen under the microscope without transverse section as dark band in the middle of the costa.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Alar cells distinct, inflated; leaves 5-8 times as long as wide; basal laminal cells in about 6 rows</description>
      <determination>1 Campylopodiella flagellacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Alar cells indistinct; leaves 10-13 times longer than wide; basal laminal cells in 15-18 rows</description>
      <determination>2 Campylopodiella stenocarpa</determination>
    </key_statement>
  </key>
</bio:treatment>