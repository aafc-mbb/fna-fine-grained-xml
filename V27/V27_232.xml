<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="treatment_page">178</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">encalyptaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">encalypta</taxon_name>
    <taxon_name authority="Müller Hal." date="1849" rank="species">spathulata</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Frond.</publication_title>
      <place_in_publication>1: 519. 1849,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family encalyptaceae;genus encalypta;species spathulata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250064824</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 10 mm, central strand weakly differentiated, cells very thin-walled.</text>
      <biological_entity id="o4900" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o4901" name="strand" name_original="strand" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s0" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o4902" name="bud" name_original="cells" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s0" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves oblong to lingulate or ligulate, 1.5–3 mm, apices obtuse to broadly acute, hair-pointed, margins plane, weakly bordered;</text>
      <biological_entity id="o4903" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s1" to="lingulate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4904" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="broadly acute" />
        <character is_modifier="false" name="shape" src="d0_s1" value="hair-pointed" value_original="hair-pointed" />
      </biological_entity>
      <biological_entity id="o4905" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s1" value="bordered" value_original="bordered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa excurrent, awns shorter than leaf lamina, smooth, narrow;</text>
      <biological_entity id="o4906" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o4907" name="awn" name_original="awns" src="d0_s2" type="structure">
        <character constraint="than leaf lamina" constraintid="o4908" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o4908" name="lamina" name_original="lamina" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>laminal cells 12–18 µm; basal-cells rectangular, 30–70 µm, smooth;</text>
      <biological_entity constraint="laminal" id="o4909" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s3" to="18" to_unit="um" />
      </biological_entity>
      <biological_entity id="o4910" name="basal-bud" name_original="basal-cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s3" to="70" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal marginal cells weakly differentiated, in 12–18 rows, longer than laminal cells.</text>
      <biological_entity id="o4912" name="row" name_original="rows" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s4" to="18" />
      </biological_entity>
      <biological_entity constraint="laminal" id="o4913" name="bud" name_original="cells" src="d0_s4" type="structure" />
      <relation from="o4911" id="r1363" name="in" negation="false" src="d0_s4" to="o4912" />
    </statement>
    <statement id="d0_s5">
      <text>Specialized asexual reproduction absent.</text>
      <biological_entity constraint="basal marginal" id="o4911" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character constraint="than laminal cells" constraintid="o4913" is_modifier="false" name="length_or_size" notes="" src="d0_s4" value="longer" value_original="longer" />
        <character is_modifier="false" name="development" src="d0_s5" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seta 3–8 mm, dark red.</text>
      <biological_entity id="o4914" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark red" value_original="dark red" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule short-cylindric, 1–2 mm, weakly striate to ribbed, gymnostomous, yellowish-brown, exothecial cells rectangular to linear, walls thickened;</text>
      <biological_entity id="o4915" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-cylindric" value_original="short-cylindric" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="weakly" name="coloration_or_pubescence_or_relief" src="d0_s7" value="striate" value_original="striate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish-brown" value_original="yellowish-brown" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o4916" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s7" to="linear" />
      </biological_entity>
      <biological_entity id="o4917" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome absent;</text>
      <biological_entity id="o4918" name="peristome" name_original="peristome" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>operculum 0.5 mm.</text>
      <biological_entity id="o4919" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Calyptra 2–4 mm, lacerate at base, smooth.</text>
      <biological_entity id="o4920" name="calyptra" name_original="calyptra" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character constraint="at base" constraintid="o4921" is_modifier="false" name="shape" src="d0_s10" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o4921" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spores 26–35 µm, warty, brown.</text>
      <biological_entity id="o4922" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="26" from_unit="um" name="some_measurement" src="d0_s11" to="35" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="warty" value_original="warty" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forming extensive mats on calcareous soils of disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="extensive mats" modifier="forming" constraint="on calcareous soils of disturbed sites" />
        <character name="habitat" value="calcareous soils" modifier="on" constraint="of disturbed sites" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Idaho, Mont., Wyo.; Europe; c Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="c Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Encalypta spathulata is very similar to E. rhaptocarpa but differs in the fringed calyptra base and weakly striate, eperistomate capsule. The gymnostomous capsules and long awn will separate E. spathulata from other species of the genus.</discussion>
  
</bio:treatment>