<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">37</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="treatment_page">54</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">sphagnum</taxon_name>
    <taxon_name authority="Hampe" date="1852" rank="species">portoricense</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>25: 359. 1852,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section sphagnum;species portoricense</taxon_hierarchy>
    <other_info_on_name type="fna_id">240000106</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="Austin" date="unknown" rank="species">sullivantianum</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species sullivantianum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate-sized to often quite robust, ± weak-stemmed, lax;</text>
    </statement>
    <statement id="d0_s1">
      <text>green, bluish green, green and brown to dark golden brown, often speckled in appearance;</text>
      <biological_entity id="o8330" name="appearance" name_original="appearance" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="punct" value_original="punct" />
        <character char_type="range_value" from="green and brown" name="coloration" src="d0_s1" to="dark golden" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="speckled" value_original="speckled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>found submerged in shallow water, stranded along shore lines in loose carpets.</text>
      <biological_entity id="o8329" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="moderate-sized" />
        <character is_modifier="false" modifier="often quite" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s0" value="weak-stemmed" value_original="weak-stemmed" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="lax" value_original="lax" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="bluish green" value_original="bluish green" />
        <character constraint="in shallow water" is_modifier="false" name="location" src="d0_s2" value="submerged" value_original="submerged" />
        <character constraint="along shore, lines" constraintid="o8331, o8332" is_modifier="false" name="condition" src="d0_s2" value="stranded" value_original="stranded" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8331" name="shore" name_original="shore" src="d0_s2" type="structure" />
      <biological_entity id="o8332" name="line" name_original="lines" src="d0_s2" type="structure" />
      <biological_entity id="o8333" name="carpet" name_original="carpets" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o8331" id="r2299" name="in" negation="false" src="d0_s2" to="o8333" />
      <relation from="o8332" id="r2300" name="in" negation="false" src="d0_s2" to="o8333" />
    </statement>
    <statement id="d0_s3">
      <text>Stems brown, superficial cortical layer with spiral reinforcing fibrils clearly visible, usually many pores per cell (1–6), comb-fibrils on interior wall.</text>
      <biological_entity id="o8334" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="superficial cortical" id="o8335" name="layer" name_original="layer" src="d0_s3" type="structure" />
      <biological_entity id="o8336" name="fibril" name_original="fibrils" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_course" src="d0_s3" value="spiral" value_original="spiral" />
        <character is_modifier="false" modifier="clearly" name="prominence" src="d0_s3" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o8337" name="pore" name_original="pores" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="usually" name="quantity" src="d0_s3" value="many" value_original="many" />
      </biological_entity>
      <biological_entity id="o8338" name="cell" name_original="cell" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o8339" name="comb-fibril" name_original="comb-fibrils" src="d0_s3" type="structure" />
      <biological_entity constraint="interior" id="o8340" name="wall" name_original="wall" src="d0_s3" type="structure" />
      <relation from="o8335" id="r2301" name="with" negation="false" src="d0_s3" to="o8336" />
      <relation from="o8337" id="r2302" name="per" negation="false" src="d0_s3" to="o8338" />
      <relation from="o8339" id="r2303" name="on" negation="false" src="d0_s3" to="o8340" />
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves 1.1 × 1 mm;</text>
    </statement>
    <statement id="d0_s5">
      <text>rarely hemiisophyllous;</text>
      <biological_entity id="o8341" name="stem-leaf" name_original="stem-leaves" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="mm" value="1.1" value_original="1.1" />
        <character name="width" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s5" value="hemiisophyllous" value_original="hemiisophyllous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hyaline cells nonornamented, frequently septate.</text>
      <biological_entity id="o8342" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="nonornamented" value_original="nonornamented" />
        <character is_modifier="false" modifier="frequently" name="architecture" src="d0_s6" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Branches clavate and rounded at distal end.</text>
      <biological_entity id="o8343" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="clavate" value_original="clavate" />
        <character constraint="at distal end" constraintid="o8344" is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8344" name="end" name_original="end" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Branch fascicles with 2 spreading and 2 pendent branches.</text>
      <biological_entity id="o8345" name="branch" name_original="branch" src="d0_s8" type="structure">
        <character constraint="with branches" constraintid="o8346" is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o8346" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch stems with hyaline cell comb-lamellae visible on interior cortex wall, cortical cell end walls with conspicuous funnel projections more than 1/2 length of cell, superficial cortical wall aporose.</text>
      <biological_entity constraint="branch" id="o8347" name="stem" name_original="stems" src="d0_s9" type="structure" />
      <biological_entity constraint="cell" id="o8348" name="comb-lamella" name_original="comb-lamellae" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character constraint="on interior cortex wall" constraintid="o8349" is_modifier="false" name="prominence" src="d0_s9" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity constraint="cortex" id="o8349" name="wall" name_original="wall" src="d0_s9" type="structure" constraint_original="interior cortex" />
      <biological_entity constraint="end" id="o8350" name="wall" name_original="walls" src="d0_s9" type="structure" constraint_original="cell cortical end" />
      <biological_entity id="o8351" name="projection" name_original="projections" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="shape" src="d0_s9" value="funnel" value_original="funnel" />
        <character name="length" src="d0_s9" value="1 length of cell" value_original="1 length of cell" />
        <character name="length" src="d0_s9" value="/2 length of cell" value_original="/2 length of cell" />
      </biological_entity>
      <biological_entity constraint="superficial cortical" id="o8352" name="wall" name_original="wall" src="d0_s9" type="structure" />
      <relation from="o8347" id="r2304" name="with" negation="false" src="d0_s9" to="o8348" />
      <relation from="o8350" id="r2305" name="with" negation="false" src="d0_s9" to="o8351" />
    </statement>
    <statement id="d0_s10">
      <text>Branch leaves broadly ovate, 2.4 × 1.7 mm;</text>
      <biological_entity constraint="branch" id="o8353" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character name="length" src="d0_s10" unit="mm" value="2.4" value_original="2.4" />
        <character name="width" src="d0_s10" unit="mm" value="1.7" value_original="1.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hyaline cells on convex surface with numerous round pores along the commissures, comb-lamellae on hyaline cell-walls where overlying chlorophyllous cells;</text>
      <biological_entity id="o8354" name="bud" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8355" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o8356" name="pore" name_original="pores" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="shape" src="d0_s11" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o8357" name="commissure" name_original="commissures" src="d0_s11" type="structure" />
      <biological_entity id="o8358" name="comb-lamella" name_original="comb-lamellae" src="d0_s11" type="structure" />
      <biological_entity id="o8359" name="cell-wall" name_original="cell-walls" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8360" name="bud" name_original="cells" src="d0_s11" type="structure">
        <character constraint="on hyaline cell-walls" constraintid="o8359" is_modifier="true" name="architecture" src="d0_s11" value="chlorophyllous" value_original="chlorophyllous" />
      </biological_entity>
      <relation from="o8354" id="r2306" name="on" negation="false" src="d0_s11" to="o8355" />
      <relation from="o8355" id="r2307" name="with" negation="false" src="d0_s11" to="o8356" />
      <relation from="o8356" id="r2308" name="along" negation="false" src="d0_s11" to="o8357" />
      <relation from="o8358" id="r2309" name="on" negation="false" src="d0_s11" to="o8359" />
      <relation from="o8359" id="r2310" name="overlying" negation="false" src="d0_s11" to="o8360" />
    </statement>
    <statement id="d0_s12">
      <text>chlorophyllous cells broadly triangular in transverse-section and well-enclosed on the convex surface.</text>
      <biological_entity id="o8362" name="transverse-section" name_original="transverse-section" src="d0_s12" type="structure" />
      <biological_entity id="o8363" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o8361" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="chlorophyllous" value_original="chlorophyllous" />
        <character constraint="in transverse-section" constraintid="o8362" is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
        <character constraint="on surface" constraintid="o8363" is_modifier="false" name="position" notes="" src="d0_s12" value="well-enclosed" value_original="well-enclosed" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule with pseudostomata.</text>
      <biological_entity id="o8364" name="capsule" name_original="capsule" src="d0_s14" type="structure" />
      <biological_entity id="o8365" name="pseudostomatum" name_original="pseudostomata" src="d0_s14" type="structure" />
      <relation from="o8364" id="r2311" name="with" negation="false" src="d0_s14" to="o8365" />
    </statement>
    <statement id="d0_s15">
      <text>Spores 22–29 µm; finely papillose on both surfaces;</text>
      <biological_entity id="o8367" name="surface" name_original="surfaces" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>indistinct triradiate ridge on distal surface;</text>
      <biological_entity id="o8366" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="22" from_unit="um" name="some_measurement" src="d0_s15" to="29" to_unit="um" />
        <character constraint="on surfaces" constraintid="o8367" is_modifier="false" modifier="finely" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o8368" name="ridge" name_original="ridge" src="d0_s16" type="structure" />
      <biological_entity constraint="distal" id="o8369" name="surface" name_original="surface" src="d0_s16" type="structure" />
      <relation from="o8368" id="r2312" name="on" negation="false" src="d0_s16" to="o8369" />
    </statement>
    <statement id="d0_s17">
      <text>proximal laesura 0.5–0.6 spore radius.</text>
      <biological_entity constraint="proximal" id="o8370" name="laesurum" name_original="laesura" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.5" name="quantity" src="d0_s17" to="0.6" />
      </biological_entity>
      <biological_entity id="o8371" name="spore" name_original="spore" src="d0_s17" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream channels, shallow ponds, coniferous and hardwood swamps and pocosins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream channels" />
        <character name="habitat" value="shallow ponds" />
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="hardwood swamps" />
        <character name="habitat" value="pocosins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., La., N.J., N.Y., N.C., S.C., Tex.; Mexico; West Indies; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Sphagnum portoricense is normally very easily distinguished because of its wet growing habit and strongly clavate branches.</discussion>
  
</bio:treatment>