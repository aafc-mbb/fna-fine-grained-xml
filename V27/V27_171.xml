<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="mention_page">140</other_info_on_meta>
    <other_info_on_meta type="treatment_page">139</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">polytrichum</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="species">strictum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Schrader)</publication_title>
      <place_in_publication>1800(1): 286. 1801,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus polytrichum;species strictum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065140</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="Funck" date="unknown" rank="species">affine</taxon_name>
    <taxon_hierarchy>genus Polytrichum;species affine;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">juniperinum</taxon_name>
    <taxon_name authority="(Funck) Bridel" date="unknown" rank="variety">affine</taxon_name>
    <taxon_hierarchy>genus Polytrichum;species juniperinum;variety affine;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polytrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">juniperinum</taxon_name>
    <taxon_name authority="Wahlenberg" date="unknown" rank="variety">gracilius</taxon_name>
    <taxon_hierarchy>genus Polytrichum;species juniperinum;variety gracilius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, green to whitish green, dark brownish with age, in deep, compact tufts.</text>
      <biological_entity id="o6752" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="whitish green" />
        <character constraint="with age" constraintid="o6753" is_modifier="false" name="coloration" src="d0_s0" value="dark brownish" value_original="dark brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6753" name="age" name_original="age" src="d0_s0" type="structure" />
      <biological_entity id="o6754" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="depth" src="d0_s0" value="deep" value_original="deep" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
      </biological_entity>
      <relation from="o6752" id="r1885" name="in" negation="false" src="d0_s0" to="o6754" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 6–12 (–20) cm, simple, densely matted with wooly whitish to light-brownish tomentum.</text>
      <biological_entity id="o6755" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="12" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="with tomentum" constraintid="o6756" is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="matted" value_original="matted" />
      </biological_entity>
      <biological_entity id="o6756" name="tomentum" name_original="tomentum" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="wooly" value_original="wooly" />
        <character char_type="range_value" from="whitish" is_modifier="true" name="coloration" src="d0_s1" to="light-brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2–5 (–6) mm, erect to closely appressed when dry, erect-spreading when moist;</text>
      <biological_entity id="o6757" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="erect" modifier="when dry" name="orientation" src="d0_s2" to="closely appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath oblong-rectangular, brownish, ± abruptly contracted to the blade;</text>
      <biological_entity id="o6758" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-rectangular" value_original="oblong-rectangular" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
        <character constraint="to blade" constraintid="o6759" is_modifier="false" modifier="more or less abruptly" name="condition_or_size" src="d0_s3" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o6759" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly lanceolate, acuminate, flat, with sharply infolded margins;</text>
      <biological_entity id="o6760" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o6761" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sharply" name="shape" src="d0_s4" value="infolded" value_original="infolded" />
      </biological_entity>
      <relation from="o6760" id="r1886" name="with" negation="false" src="d0_s4" to="o6761" />
    </statement>
    <statement id="d0_s5">
      <text>marginal lamina 6–7 cells wide, 1-stratose, entire to finely crenulate above, membranous and transparent, abruptly infolded and enclosing the lamellae and overlapping towards the apex;</text>
      <biological_entity constraint="marginal" id="o6762" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="7" />
      </biological_entity>
      <biological_entity id="o6763" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="wide" value_original="wide" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="finely crenulate" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="transparent" value_original="transparent" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s5" value="infolded" value_original="infolded" />
        <character constraint="towards apex" constraintid="o6765" is_modifier="false" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o6764" name="lamella" name_original="lamellae" src="d0_s5" type="structure" />
      <biological_entity id="o6765" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o6763" id="r1887" name="enclosing the" negation="false" src="d0_s5" to="o6764" />
    </statement>
    <statement id="d0_s6">
      <text>costa toothed abaxially towards the apex, short-excurrent as a short, reddish-brown awn;</text>
      <biological_entity id="o6766" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character constraint="towards apex" constraintid="o6767" is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character constraint="as awn" constraintid="o6768" is_modifier="false" name="architecture" notes="" src="d0_s6" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity id="o6767" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o6768" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lamellae bluntly crenate in profile, 5–8 cells high, the marginal cells in section pyriform, thick-walled, ending in a thickened knob, end cells of lateral lamellae ovoid and scarcely thickened at the apex;</text>
      <biological_entity id="o6769" name="lamella" name_original="lamellae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="bluntly; in profile" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <biological_entity id="o6770" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="height" src="d0_s7" value="high" value_original="high" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o6771" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o6772" name="section" name_original="section" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity id="o6773" name="knob" name_original="knob" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="end" id="o6774" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="end lateral" id="o6775" name="lamella" name_original="lamellae" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="end lateral" id="o6776" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="true" modifier="scarcely" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o6771" id="r1888" name="in" negation="false" src="d0_s7" to="o6772" />
      <relation from="o6771" id="r1889" name="ending in a" negation="false" src="d0_s7" to="o6773" />
      <relation from="o6771" id="r1890" name="ending in a" negation="false" src="d0_s7" to="o6774" />
      <relation from="o6771" id="r1891" name="ending in a" negation="false" src="d0_s7" to="o6775" />
      <relation from="o6771" id="r1892" name="ending in a" negation="false" src="d0_s7" to="o6776" />
    </statement>
    <statement id="d0_s8">
      <text>sheath cells 45–80 × 7–10 µm, elongate-rectangular (5–7:1), narrower toward the margin;</text>
      <biological_entity constraint="sheath" id="o6777" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="45" from_unit="um" name="length" src="d0_s8" to="80" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s8" to="10" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate-rectangular" value_original="elongate-rectangular" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="7" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character constraint="toward margin" constraintid="o6778" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o6778" name="margin" name_original="margin" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>cells of the marginal lamina transversely elongate, shorter and obliquely oriented towards the margins, very thick-walled and colorless.</text>
      <biological_entity constraint="sheath" id="o6779" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="45" from_unit="um" name="length" src="d0_s9" to="80" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s9" to="10" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate-rectangular" value_original="elongate-rectangular" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o6781" name="lamina" name_original="lamina" src="d0_s9" type="structure" />
      <biological_entity id="o6782" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <relation from="o6780" id="r1893" name="part_of" negation="false" src="d0_s9" to="o6781" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous;</text>
      <biological_entity id="o6780" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character constraint="towards margins" constraintid="o6782" is_modifier="false" modifier="obliquely" name="orientation" src="d0_s9" value="oriented" value_original="oriented" />
        <character is_modifier="false" modifier="very" name="architecture" notes="" src="d0_s9" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="colorless" value_original="colorless" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaves somewhat longer than the stem-leaves, ending in a slender awn.</text>
      <biological_entity constraint="perichaetial" id="o6783" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character constraint="than the stem-leaves" constraintid="o6784" is_modifier="false" name="length_or_size" src="d0_s11" value="somewhat longer" value_original="somewhat longer" />
      </biological_entity>
      <biological_entity id="o6784" name="stem-leaf" name_original="stem-leaves" src="d0_s11" type="structure" />
      <biological_entity constraint="slender" id="o6785" name="awn" name_original="awn" src="d0_s11" type="structure" />
      <relation from="o6783" id="r1894" name="ending in" negation="false" src="d0_s11" to="o6785" />
    </statement>
    <statement id="d0_s12">
      <text>Seta 2–4 cm, yellowish to reddish-brown.</text>
      <biological_entity id="o6786" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s12" to="4" to_unit="cm" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s12" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule 2–3 mm, short-rectangular to almost cubic (1–1.5:1), brownish, sharply 4-angled and prismatic, suberect, becoming horizontal when ripe;</text>
      <biological_entity id="o6787" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="short-rectangular to almost" value_original="short-rectangular to almost" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s13" value="cubic" value_original="cubic" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="1.5" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s13" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="when ripe" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome 200–230 µm, divided to 0.8, the teeth 64, obtuse.</text>
      <biological_entity id="o6788" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="short-rectangular to almost" value_original="short-rectangular to almost" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s14" value="cubic" value_original="cubic" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s14" to="1.5" />
      </biological_entity>
      <biological_entity id="o6789" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character char_type="range_value" from="200" from_unit="um" name="some_measurement" src="d0_s14" to="230" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s14" value="divided" value_original="divided" />
        <character name="quantity" src="d0_s14" value="0.8" value_original="0.8" />
      </biological_entity>
      <biological_entity id="o6790" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="64" value_original="64" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Calyptra dirty white to light-brown, enclosing the capsule.</text>
      <biological_entity id="o6791" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character char_type="range_value" from="dirty white" name="coloration" src="d0_s15" to="light-brown" />
      </biological_entity>
      <biological_entity id="o6792" name="capsule" name_original="capsule" src="d0_s15" type="structure" />
      <relation from="o6791" id="r1895" name="enclosing the" negation="false" src="d0_s15" to="o6792" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 7–9 (–15) µm.</text>
      <biological_entity id="o6793" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s16" to="15" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s16" to="9" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sphagnum bogs, wet heaths and tundra, muskeg, sedge meadows, moist alpine tundra, also on local elevations and on rotten stumps in wet spruce forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sphagnum bogs" />
        <character name="habitat" value="wet heaths" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="sedge meadows" modifier="muskeg" />
        <character name="habitat" value="alpine tundra" modifier="moist" />
        <character name="habitat" value="local elevations" modifier="also on" />
        <character name="habitat" value="rotten stumps" modifier="and on" constraint="in wet" />
        <character name="habitat" value="forests" modifier="wet spruce" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Calif., Colo., Conn., Ga., Ill., Ind., Iowa, Maine, Md., Mass., Mich., Minn., Mont., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., R.I., Utah, Vt., Wash., W.Va., Wis., Wyo.; South America; n Europe (Scandinavia, Svalbard); n, e Asia (Russia, Japan); Atlantic Islands (Faroes, Iceland); Antarctica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="n Europe (Scandinavia)" establishment_means="native" />
        <character name="distribution" value="n Europe (Svalbard)" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="e Asia (Russia)" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Faroes)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
        <character name="distribution" value="Antarctica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Polytrichum strictum is widespread in the boreal regions of the Holarctic, and is one of the commonest low arctic representatives of the family (D. G. Long 1985), with survivals southward in relict bogs, for example in northern Indiana, northern Illinois, and northwestern Iowa, also in alpine situations in the eastern mountains to the Carolinas and Georgia. In Nunavut, it is known from Baffin, Bathurst, and Devon islands. Its characteristic habitat is on hummocks in Sphagnum bogs, in deep masses tightly bound together by dirty-white, wooly tomentum, with short, stiffly erect leaves, and cubical capsules, a clear correlation between a distinctive morphology, distribution, and ecology.</discussion>
  
</bio:treatment>