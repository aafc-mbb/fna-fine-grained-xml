<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Dale H. Vitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="treatment_page">320</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">seligeriaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1856" rank="genus">SELIGERIA</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>2: 7. 1856  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family seligeriaceae;genus SELIGERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Ignaz Seliger, 1752–1812, Silesian pastor</other_info_on_name>
    <other_info_on_name type="fna_id">130030</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants of calcareous rocks, light green to black.</text>
      <biological_entity id="o8305" name="rock" name_original="rocks" src="d0_s0" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s0" value="calcareous" value_original="calcareous" />
      </biological_entity>
      <relation from="o8304" id="r1985" name="part_of" negation="false" src="d0_s0" to="o8305" />
    </statement>
    <statement id="d0_s1">
      <text>Alar cells not differentiated to colored and enlarged.</text>
      <biological_entity id="o8304" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s0" to="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sexual condition autoicous.</text>
      <biological_entity id="o8306" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Capsule smooth;</text>
      <biological_entity id="o8307" name="capsule" name_original="capsule" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>annulus ill-formed;</text>
      <biological_entity id="o8308" name="annulus" name_original="annulus" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="ill-formed" value_original="ill-formed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peristome teeth smooth.</text>
      <biological_entity constraint="peristome" id="o8309" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyptra cucullate.</text>
      <biological_entity id="o8310" name="calyptra" name_original="calyptra" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species ca. 20 (13 in the flora).</discussion>
  <discussion>Seligeria is characterized as tiny acrocarpous plants, with more or less ovate, smooth capsules and linear-lanceolate leaves, and a calcareous substrate as habitat. Seligeria trifaria (Bridel) Lindberg is excluded.</discussion>
  <references>
    <reference>Vitt, D. H. 1976. The genus Seligeria in North America. Lindbergia 3: 241–275.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsule without a peristome, margins of leaf in proximal 1/3 denticulate.</description>
      <determination>7 Seligeria donniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsule with a peristome, margins subentire to entire</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spores 17-30 µm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spores 10-16 µm</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spores (20-)23-30 µm; peristome of 16 reduced teeth, each of 5-8 cells.</description>
      <determination>8 Seligeria oelandica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Spores 17-25(-27) µm; peristome of 16 fully developed teeth</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsule ovate, sometimes with flaring mouth when old and dry; seta 2-3 mm; columella short, not persistent, not attached to opercula; leaves spreading- recurved, evenly spaced around stem, not trifarious</description>
      <determination>9 Seligeria polaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsule turbinate when old and dry; seta 1-1.5 mm; columella elongate with opercula tending to remain attached; leaves often trifarious.</description>
      <determination>13 Seligeria tristichoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Costa excurrent, filling subulate apex</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Costa (except in perichaetial leaves) ending just before apex, lamina present into apex</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seta curved 30-90° when wet.</description>
      <determination>11 Seligeria recurvata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Seta straight when wet</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves wiry, stiffly flexuose, long-subulate, light green to dull-green; cells of leaf lamina longer than broad, pellucid; seta less than 1 mm; capsule turbinate when old, emergent.</description>
      <determination>5 Seligeria careyana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves recurved-twisted to stiffly erect, stoutly subulate from a wide base, dark-green to black; cells of distal leaf lamina usually as wide as long, opaque; seta greater than 1 mm; capsule ± ovate to oblong when old, emergent to exserted</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves stiffly-erect when dry; plants dark-green, gregarious; spores 14-16 µm; capsule contracted to seta through a wrinkled neck when old; eastern North America.</description>
      <determination>3 Seligeria calcarea</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves twisted when dry; plants blackish, forming turfs; spores 10-14 µm; capsule ± sharply contracted to seta when old; western North America.</description>
      <determination>12 Seligeria subimmersa</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants olive green to dark green; cells of distal leaf lamina ± opaque, usually as broad as long; leaves oblong to lanceolate; capsule ovate to oblong, not clearly widest at mouth</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants light green; cells of leaf lamina pellucid, tending to be longer than broad; vegetative leaves linear, sometimes ± subulate; capsule turbinate to obovate, widest at mouth</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves curved and loosely arranged when dry, narrowed to obtuse, plane, erect acumen; perichaetial leaves usually not clasping, similar to vegetative leaves; capsule wider at middle than at mouth; seta thin, curved 30-90° when wet.</description>
      <determination>4 Seligeria campylopoda</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves stiffly erect when dry, narrowed to an obtuse, incurved, channeled acumen; perichaetial leaves clasping and rapidly narrowed to stout subula; capsule ± as wide at middle as at mouth; seta ± thick, straight to slightly curved when wet.</description>
      <determination>6 Seligeria diversifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Perichaetial leaves subulate from an ovate, sheathing base, abruptly differentiated from vegetative leaves.</description>
      <determination>1 Seligeria acutifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Perichaetial and vegetative leaves similar, sometimes distal leaves larger, but always similar to proximal leaves in shape</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaves short, ligulate; apex broadly acute to obtuse; capsule ± pyriform when old; peristome large.</description>
      <determination>2 Seligeria brevifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaves long, subulate; apex narrowly acute; capsule obovate to pyriform when old; peristome small.</description>
      <determination>10 Seligeria pusilla</determination>
    </key_statement>
  </key>
</bio:treatment>