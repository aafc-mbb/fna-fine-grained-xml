<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="treatment_page">88</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Wilson" date="1855" rank="section">acutifolia</taxon_name>
    <taxon_name authority="R. E. Andrus" date="1980" rank="species">andersonianum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>83: 60, figs. 1–7. 1980,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section acutifolia;species andersonianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443293</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, soft and slender;</text>
      <biological_entity id="o5931" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>capitulum small;</text>
    </statement>
    <statement id="d0_s2">
      <text>pale-pink to pale purplish red;</text>
    </statement>
    <statement id="d0_s3">
      <text>without metallic lustre when dry.</text>
      <biological_entity id="o5932" name="capitulum" name_original="capitulum" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="small" value_original="small" />
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s2" to="pale purplish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems pale-brown, green or red, rarely purplish red;</text>
      <biological_entity id="o5933" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="red" value_original="red" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s4" value="purplish red" value_original="purplish red" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>superficial cortical cells aporose.</text>
      <biological_entity constraint="superficial cortical" id="o5934" name="bud" name_original="cells" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves lingulate, 0.9–1.2 (–1.4) mm, apex rounded-erose to sometimes slightly apiculate, border moderately strong and broadened at the base (to 0.8 the width);</text>
      <biological_entity id="o5935" name="leaf-stem" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lingulate" value_original="lingulate" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s6" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5936" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="rounded-erose to sometimes" value_original="rounded-erose to sometimes" />
        <character is_modifier="false" modifier="sometimes; slightly" name="architecture_or_shape" src="d0_s6" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o5937" name="border" name_original="border" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately" name="fragility" src="d0_s6" value="strong" value_original="strong" />
        <character constraint="at base" constraintid="o5938" is_modifier="false" name="width" src="d0_s6" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o5938" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>hyaline cells rhombic, 0–1-septate, usually efibrillose.</text>
      <biological_entity id="o5939" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="0-1-septate" value_original="0-1-septate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s7" value="efibrillose" value_original="efibrillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branches not or slightly 5-ranked, lax.</text>
      <biological_entity id="o5940" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="arrangement" src="d0_s8" value="5-ranked" value_original="5-ranked" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch fascicles with 2 spreading and 1–2 pendent branches.</text>
      <biological_entity id="o5941" name="branch" name_original="branch" src="d0_s9" type="structure">
        <character constraint="with branches" constraintid="o5942" is_modifier="false" name="arrangement" src="d0_s9" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o5942" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Branch leaves ovate, 0.8–1.4 mm, distinctly concave, straight, apex dentate and involute;</text>
      <biological_entity constraint="branch" id="o5943" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s10" value="concave" value_original="concave" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o5944" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s10" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>border entire;</text>
      <biological_entity id="o5945" name="border" name_original="border" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hyaline cells on convex surface with elliptic pores along the commissures, grading from smaller pores near the apex to larger pores at the base, concave surface with large round pores on the proximal portions of the leaf.</text>
      <biological_entity id="o5946" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o5947" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o5948" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o5949" name="commissure" name_original="commissures" src="d0_s12" type="structure" />
      <biological_entity constraint="smaller" id="o5950" name="pore" name_original="pores" src="d0_s12" type="structure" />
      <biological_entity id="o5951" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity constraint="larger" id="o5952" name="pore" name_original="pores" src="d0_s12" type="structure" />
      <biological_entity id="o5953" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o5955" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s12" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5956" name="portion" name_original="portions" src="d0_s12" type="structure" />
      <biological_entity id="o5957" name="leaf" name_original="leaf" src="d0_s12" type="structure" />
      <relation from="o5946" id="r1655" name="on" negation="false" src="d0_s12" to="o5947" />
      <relation from="o5947" id="r1656" name="with" negation="false" src="d0_s12" to="o5948" />
      <relation from="o5948" id="r1657" name="along" negation="false" src="d0_s12" to="o5949" />
      <relation from="o5946" id="r1658" name="from" negation="false" src="d0_s12" to="o5950" />
      <relation from="o5950" id="r1659" name="near" negation="false" src="d0_s12" to="o5951" />
      <relation from="o5951" id="r1660" name="to" negation="false" src="d0_s12" to="o5952" />
      <relation from="o5952" id="r1661" name="at" negation="false" src="d0_s12" to="o5953" />
      <relation from="o5954" id="r1662" name="with" negation="false" src="d0_s12" to="o5955" />
      <relation from="o5955" id="r1663" name="on" negation="false" src="d0_s12" to="o5956" />
      <relation from="o5956" id="r1664" name="part_of" negation="false" src="d0_s12" to="o5957" />
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o5954" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="concave" value_original="concave" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 18–24 µm; coarsely papillose on both surfaces;</text>
      <biological_entity id="o5958" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s14" to="24" to_unit="um" />
        <character constraint="on surfaces" constraintid="o5959" is_modifier="false" modifier="coarsely" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o5959" name="surface" name_original="surfaces" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>proximal laesura less than 0.5 the length of the spore radius.</text>
      <biological_entity constraint="proximal" id="o5960" name="laesurum" name_original="laesura" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s15" to="0.5" />
      </biological_entity>
      <biological_entity id="o5961" name="spore" name_original="spore" src="d0_s15" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Weakly minerotrophic</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="weakly minerotrophic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Nfld. and Labr. (Nfld.), N.S., Que.; Alaska, Conn., Maine, Mass., N.H., N.Y., R.I., Vt.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>65.</number>
  <discussion>Sporophytes are uncommon in Sphagnum andersonianum. Its distribution is uncertain due to past taxonomic confusion with S. rubellum, S. capillifolium, and other closely related species in sect. Acutifolia. This is one of the most hydrophytic species of the section and is often associated with S. angustifolium, S. fallax, and S. rubellum. It is often not easily distinguished from S. rubellum, with which it may intergrade in a manner similar to that already demonstrated for S. rubellum and S. capillifolium by N. Cronberg (1997, 1998). Phenotypically S. andersonianum is a softer, paler plant with branch leaves that are more ovate and concave on branches that are less 5-ranked. Sphagnum rubellum also has the branch leaves sometimes subsecund while in S. andersonianum they are straight. Ecologically S. rubellum is an open mire species of usually ombrotrophic conditions, where it often forms hummocks, while S. andersonianum occurs in more sheltered sites where there is some mineral influence. Where they occur together, S. rubellum is on the tops of hummocks and S. andersonianum is on the hummock sides and bases. Microscopically S. rubellum has stem leaves with at least some of the hyaline cells twice septate, while in S. andersonianum none are twice septate.</discussion>
  
</bio:treatment>