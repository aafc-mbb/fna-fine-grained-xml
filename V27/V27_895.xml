<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">615</other_info_on_meta>
    <other_info_on_meta type="treatment_page">616</other_info_on_meta>
    <other_info_on_meta type="illustration_page">614</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="(Müller Hal.) Kindberg" date="1882" rank="genus">aloina</taxon_name>
    <taxon_name authority="(Müller Hal.) Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="species">hamulus</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>214[I,3]: 428. 1902,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus aloina;species hamulus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250061625</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">hamulus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Herb. Boissier</publication_title>
      <place_in_publication>5: 192. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Barbula;species hamulus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 6 mm.</text>
      <biological_entity id="o7474" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="6" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves lingulate, 0.6–2 mm, margins entire to irregularly crenulate, undifferentiated at base, apex cucullate;</text>
      <biological_entity id="o7475" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lingulate" value_original="lingulate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7476" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s1" to="irregularly crenulate" />
        <character constraint="at base" constraintid="o7477" is_modifier="false" name="prominence" src="d0_s1" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
      <biological_entity id="o7477" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o7478" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa subpercurrent to percurrent, filaments of 3–7 cells, cells cylindric to subspheric;</text>
      <biological_entity id="o7479" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o7480" name="filament" name_original="filaments" src="d0_s2" type="structure" />
      <biological_entity id="o7481" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity id="o7482" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s2" to="subspheric" />
      </biological_entity>
      <relation from="o7480" id="r1783" name="consist_of" negation="false" src="d0_s2" to="o7481" />
    </statement>
    <statement id="d0_s3">
      <text>cells of leaf base 13–59 µm, medial and distal cells 9–26 µm, large solid papillae on abaxial surface distally.</text>
      <biological_entity id="o7483" name="cell" name_original="cells" src="d0_s3" type="structure" constraint="base" constraint_original="base; base">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s3" to="59" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o7484" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="medial and distal" id="o7485" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s3" to="26" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7487" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o7483" id="r1784" name="part_of" negation="false" src="d0_s3" to="o7484" />
      <relation from="o7486" id="r1785" name="on" negation="false" src="d0_s3" to="o7487" />
    </statement>
    <statement id="d0_s4">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o7486" name="papilla" name_original="papillae" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="large" value_original="large" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta 7–16 mm.</text>
      <biological_entity id="o7488" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule urn ovoid-cylindric, 1.1–3 mm;</text>
      <biological_entity constraint="capsule" id="o7489" name="urn" name_original="urn" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>operculum conical to long rostrate, erect or inclined, 0.5–1.2 mm;</text>
      <biological_entity id="o7490" name="operculum" name_original="operculum" src="d0_s7" type="structure">
        <character char_type="range_value" from="conical" name="shape" src="d0_s7" to="long rostrate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="inclined" value_original="inclined" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome to 900 µm, twisted to nearly straight.</text>
      <biological_entity id="o7491" name="peristome" name_original="peristome" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" src="d0_s8" to="900" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 9–13 µm.</text>
      <biological_entity id="o7492" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s9" to="13" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporophytes unknown in flora area.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone or calcareous rocks, sandy soil in dry sunny sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="calcareous rocks" />
        <character name="habitat" value="sandy soil" constraint="in dry sunny sites" />
        <character name="habitat" value="dry sunny sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (700-1800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="700" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Ill., Kans., La., Nebr., Okla., Tex.; Mexico; Central America (El Salvador, Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (El Salvador)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>The undifferentiated marginal cells of the leaf base and the large abaxial solid papillae near the leaf tip distinguish Aloina hamulus from A. rigida, which, in addition, has a strongly twisted and longer peristome. The ovoid-cylindric capsule and abaxially papillose leaves distinguish this species from A. aloides var. ambigua. Young leaves should be checked when papillae are not evident in mature leaves.</discussion>
  
</bio:treatment>