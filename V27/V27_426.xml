<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="S. P. Churchill" date="unknown" rank="family">scouleriaceae</taxon_name>
    <taxon_name authority="Hooker" date="1829" rank="genus">SCOULERIA</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Misc.</publication_title>
      <place_in_publication>1: 33. 1829,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family scouleriaceae;genus SCOULERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For John Scouler, 1804–1871, physician, botanical collector, and naturalist</other_info_on_name>
    <other_info_on_name type="fna_id">129898</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="(Hooker) Müller Hal." date="unknown" rank="section">Scouleria</taxon_name>
    <taxon_hierarchy>genus Grimmia;section Scouleria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="(Hooker) Lesquereux &amp; James" date="unknown" rank="subgenus">Scouleria</taxon_name>
    <taxon_hierarchy>genus Grimmia;subgenus Scouleria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants clustered in loose to compact tufts, black or brown with age.</text>
      <biological_entity id="o1605" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="in tufts" constraintid="o1606" is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character constraint="with age" constraintid="o1607" is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1606" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character char_type="range_value" from="loose" is_modifier="true" name="architecture" src="d0_s0" to="compact" />
      </biological_entity>
      <biological_entity id="o1607" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ca. 4–10 cm, central strand absent;</text>
      <biological_entity id="o1608" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o1609" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids forming a “hold-fast,” occasionally extending from stem and abaxial surface of leaves.</text>
      <biological_entity id="o1610" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure" />
      <biological_entity id="o1611" name="hold-fast" name_original="hold-fast" src="d0_s2" type="structure" />
      <biological_entity id="o1612" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial" id="o1613" name="surface" name_original="surface" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial" id="o1614" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o1610" id="r369" name="forming a “" negation="false" src="d0_s2" to="o1611" />
      <relation from="o1610" id="r370" modifier="occasionally" name="extending from" negation="false" src="d0_s2" to="o1612" />
      <relation from="o1610" id="r371" modifier="occasionally" name="extending from" negation="false" src="d0_s2" to="o1613" />
      <relation from="o1610" id="r372" modifier="occasionally" name="extending from" negation="false" src="d0_s2" to="o1614" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves erect to patent when dry, when present pseudocosta marginal [submarginal];</text>
      <biological_entity id="o1615" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" modifier="when dry" name="orientation" src="d0_s3" to="patent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex often cucullate.</text>
      <biological_entity id="o1616" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Outer perichaetial leaves one-third shorter than inner leaves.</text>
      <biological_entity constraint="outer perichaetial" id="o1617" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1/3" value_original="1/3" />
        <character constraint="than inner leaves" constraintid="o1618" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1618" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Vaginula forming a sheath around proximal third or fourth of seta.</text>
      <biological_entity id="o1619" name="vaginulum" name_original="vaginula" src="d0_s6" type="structure" />
      <biological_entity id="o1620" name="sheath" name_original="sheath" src="d0_s6" type="structure" />
      <biological_entity id="o1621" name="third" name_original="third" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="around" name="position" src="d0_s6" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o1622" name="seta" name_original="seta" src="d0_s6" type="structure" />
      <relation from="o1619" id="r373" name="forming a" negation="false" src="d0_s6" to="o1620" />
      <relation from="o1619" id="r374" name="around proximal" negation="false" src="d0_s6" to="o1621" />
      <relation from="o1619" id="r375" name="around proximal" negation="false" src="d0_s6" to="o1622" />
    </statement>
    <statement id="d0_s7">
      <text>Seta 1/3–1/2 capsule length.</text>
      <biological_entity id="o1623" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s7" to="1/2" />
      </biological_entity>
      <biological_entity id="o1624" name="capsule" name_original="capsule" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Capsule glossy black with age, columella when dry appearing longitudinally ribbed, when wet expanding, becoming stoutly cylindrical or ovoid.</text>
      <biological_entity id="o1625" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s8" value="glossy" value_original="glossy" />
        <character constraint="with age" constraintid="o1626" is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o1626" name="age" name_original="age" src="d0_s8" type="structure" />
      <biological_entity id="o1627" name="columella" name_original="columella" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when dry longitudinally ribbed; when dry longitudinally ribbed; when wet" name="size" src="d0_s8" value="expanding" value_original="expanding" />
        <character is_modifier="false" modifier="becoming stoutly" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 30–55 µm.</text>
      <biological_entity id="o1628" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s9" to="55" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America, temperate South America, n Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America" establishment_means="native" />
        <character name="distribution" value="temperate South America" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Scouleria patagonica) is the only taxon found in South America.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margin 1-stratose or partially (rarely fully) 2-stratose; peristome present.</description>
      <determination>1 Scouleria aquatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margin multistratose; peristome absent.</description>
      <determination>2 Scouleria marginata</determination>
    </key_statement>
  </key>
</bio:treatment>