<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Lindberg" date="1862" rank="section">cuspidata</taxon_name>
    <taxon_name authority="Ångström" date="1864" rank="species">riparium</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Kongl. Vetensk.-Akad. Förh.</publication_title>
      <place_in_publication>21: 198. 1864,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section cuspidata;species riparium</taxon_hierarchy>
    <other_info_on_name type="fna_id">200000825</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stiff and upright, large;</text>
    </statement>
    <statement id="d0_s1">
      <text>green to pale green to brownish, capitulum large and flat, with a conspicuous terminal bud.</text>
      <biological_entity id="o1188" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="upright" value_original="upright" />
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="pale green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1189" name="capitulum" name_original="capitulum" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="large" value_original="large" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1190" name="bud" name_original="bud" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o1189" id="r328" name="with" negation="false" src="d0_s1" to="o1190" />
    </statement>
    <statement id="d0_s2">
      <text>Stems pale green, superficial cortex of 3–4 layers of weakly differentiated cells.</text>
      <biological_entity id="o1191" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity constraint="superficial" id="o1192" name="cortex" name_original="cortex" src="d0_s2" type="structure" />
      <biological_entity id="o1193" name="layer" name_original="layers" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o1194" name="bud" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="weakly" name="variability" src="d0_s2" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o1192" id="r329" name="consist_of" negation="false" src="d0_s2" to="o1193" />
      <relation from="o1193" id="r330" name="part_of" negation="false" src="d0_s2" to="o1194" />
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves triangular-lingulate, 1.2–1.4 mm;</text>
      <biological_entity id="o1195" name="leaf-stem" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular-lingulate" value_original="triangular-lingulate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="distance" src="d0_s3" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex with a deep lacerate split;</text>
      <biological_entity id="o1196" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o1197" name="split" name_original="split" src="d0_s4" type="structure">
        <character is_modifier="true" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="true" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <relation from="o1196" id="r331" name="with" negation="false" src="d0_s4" to="o1197" />
    </statement>
    <statement id="d0_s5">
      <text>hyaline cells aporose, efibrillose and often septate.</text>
      <biological_entity id="o1198" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Branches unranked to rarely 5-ranked, branch leaves only weakly undulate, but sharply recurved at the apex, leaves not much elongated at distal end.</text>
      <biological_entity id="o1199" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="unranked" name="arrangement" src="d0_s6" to="rarely 5-ranked" />
      </biological_entity>
      <biological_entity constraint="branch" id="o1200" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="only weakly" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
        <character constraint="at apex" constraintid="o1201" is_modifier="false" modifier="sharply" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o1201" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o1202" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character constraint="at distal end" constraintid="o1203" is_modifier="false" modifier="not much" name="length" src="d0_s6" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1203" name="end" name_original="end" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Branch fascicles with 2 spreading and 2 pendent branches.</text>
      <biological_entity id="o1204" name="branch" name_original="branch" src="d0_s7" type="structure">
        <character constraint="with branches" constraintid="o1205" is_modifier="false" name="arrangement" src="d0_s7" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o1205" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch stem green, cortex enlarged with retort cells.</text>
      <biological_entity constraint="branch" id="o1206" name="stem" name_original="stem" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o1207" name="cortex" name_original="cortex" src="d0_s8" type="structure">
        <character constraint="with retort cells" constraintid="o1208" is_modifier="false" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="retort" id="o1208" name="bud" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves ovatelanceolate;</text>
    </statement>
    <statement id="d0_s10">
      <text>2–2.6 mm;</text>
    </statement>
    <statement id="d0_s11">
      <text>straight;</text>
    </statement>
    <statement id="d0_s12">
      <text>weakly undulate but strongly recurved, hyaline cells on convex surface with very large irregular pores (formed from the confluence of several smaller pores) at the cell apex, concave surface with large round wall-thinnings in the cell angles;</text>
      <biological_entity constraint="branch" id="o1209" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s12" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o1210" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o1211" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o1212" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="very" name="size" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s12" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity constraint="cell" id="o1213" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o1214" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o1215" name="wall-thinning" name_original="wall-thinnings" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s12" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="cell" id="o1216" name="angle" name_original="angles" src="d0_s12" type="structure" />
      <relation from="o1210" id="r332" name="on" negation="false" src="d0_s12" to="o1211" />
      <relation from="o1211" id="r333" name="with" negation="false" src="d0_s12" to="o1212" />
      <relation from="o1212" id="r334" name="at" negation="false" src="d0_s12" to="o1213" />
      <relation from="o1214" id="r335" name="with" negation="false" src="d0_s12" to="o1215" />
      <relation from="o1215" id="r336" name="in" negation="false" src="d0_s12" to="o1216" />
    </statement>
    <statement id="d0_s13">
      <text>chlorophyllous cells triangular to trapezoidal in transverse-section, apex normally slightly exposed on concave surface.</text>
      <biological_entity id="o1217" name="bud" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="chlorophyllous" value_original="chlorophyllous" />
        <character char_type="range_value" constraint="in transverse-section" constraintid="o1218" from="triangular" name="shape" src="d0_s13" to="trapezoidal" />
      </biological_entity>
      <biological_entity id="o1218" name="transverse-section" name_original="transverse-section" src="d0_s13" type="structure" />
      <biological_entity id="o1220" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o1219" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character constraint="on surface" constraintid="o1220" is_modifier="false" modifier="normally slightly" name="prominence" src="d0_s13" value="exposed" value_original="exposed" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 22–28 µm; proximal surface noticeably papillose, distal surface smooth or with fewer papillae;</text>
      <biological_entity id="o1221" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="22" from_unit="um" name="some_measurement" src="d0_s15" to="28" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1222" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="noticeably" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1223" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="with fewer papillae" value_original="with fewer papillae" />
      </biological_entity>
      <biological_entity id="o1224" name="papilla" name_original="papillae" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="fewer" value_original="fewer" />
      </biological_entity>
      <relation from="o1223" id="r337" name="with" negation="false" src="d0_s15" to="o1224" />
    </statement>
    <statement id="d0_s16">
      <text>proximal laesura more than 0.5 the length of the radius.</text>
      <biological_entity constraint="proximal" id="o1225" name="laesurum" name_original="laesura" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" name="quantity" src="d0_s16" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1226" name="radius" name_original="radius" src="d0_s16" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forming often extensive carpets in weakly minerotrophic mires</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="extensive carpets" modifier="forming often" constraint="in weakly minerotrophic mires" />
        <character name="habitat" value="weakly minerotrophic mires" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Conn., Ind., Maine, Mass., Mich., Minn., Mont., N.H., N.J., N.Y., Ohio, Pa., Vt., Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <discussion>Sporophytes are uncommon in Sphagnum riparium. This species is typically very easily recognized in the field with its pale green color, strong terminal bud, and unranked branch leaves.</discussion>
  
</bio:treatment>