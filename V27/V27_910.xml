<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">620</other_info_on_meta>
    <other_info_on_meta type="mention_page">621</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="treatment_page">623</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="genus">syntrichia</taxon_name>
    <taxon_name authority="(Taylor) Ochyra" date="1992" rank="species">fragilis</taxon_name>
    <place_of_publication>
      <publication_title>Fragm. Florist. Geobot.</publication_title>
      <place_in_publication>37: 212. 1992,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus syntrichia;species fragilis</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002107</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="Taylor" date="unknown" rank="species">fragilis</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>6: 333. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tortula;species fragilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (5–) 10–25 mm.</text>
      <biological_entity id="o2877" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s0" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves crowded, longitudinally folded and spirally twisted around the stem (but little crisped) when dry, widespreading when moist, oblong-lingulate to spatulate, 2–3.5 × 0.75–1 mm, fragile, with sheets of cells breaking off, sometimes along lines of weakness;</text>
      <biological_entity id="o2878" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="longitudinally" name="architecture_or_shape" src="d0_s1" value="folded" value_original="folded" />
        <character constraint="around stem" constraintid="o2879" is_modifier="false" modifier="spirally" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o2879" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s1" value="widespreading" value_original="widespreading" />
        <character char_type="range_value" from="oblong-lingulate" name="shape" src="d0_s1" to="spatulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.75" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="fragile" value_original="fragile" />
      </biological_entity>
      <biological_entity id="o2880" name="sheet" name_original="sheets" src="d0_s1" type="structure" />
      <biological_entity id="o2881" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity id="o2882" name="line" name_original="lines" src="d0_s1" type="structure" />
      <biological_entity id="o2883" name="weakness" name_original="weakness" src="d0_s1" type="structure" />
      <relation from="o2879" id="r683" name="with" negation="false" src="d0_s1" to="o2880" />
      <relation from="o2880" id="r684" name="part_of" negation="false" src="d0_s1" to="o2881" />
      <relation from="o2880" id="r685" name="breaking off" negation="false" src="d0_s1" to="o2882" />
      <relation from="o2879" id="r686" name="part_of" negation="false" src="d0_s1" to="o2883" />
    </statement>
    <statement id="d0_s2">
      <text>margins revolute before to as much as 1/2 the leaf length, to plane, entire or crenulate;</text>
      <biological_entity id="o2884" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="before leaf" constraintid="o2885" is_modifier="false" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="length" notes="" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <biological_entity id="o2885" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>apices truncate to acute;</text>
      <biological_entity id="o2886" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa percurrent or slightly excurrent as a mucro 1–3 (–5) cells long, yellow or red, finely to strongly papillose abaxially, abruptly tapered;</text>
      <biological_entity id="o2887" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="percurrent" value_original="percurrent" />
        <character constraint="as mucro" constraintid="o2888" is_modifier="false" modifier="slightly" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o2888" name="mucro" name_original="mucro" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o2889" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="red" value_original="red" />
        <character is_modifier="false" modifier="finely to strongly; abaxially" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal-cells abruptly differentiated, becoming rather abruptly shortrectangular near the margins;</text>
      <biological_entity id="o2890" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o2891" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <relation from="o2890" id="r687" modifier="becoming rather; rather abruptly; abruptly" name="near" negation="false" src="d0_s5" to="o2891" />
    </statement>
    <statement id="d0_s6">
      <text>distal cells quadrate-hexagonal, 9–15 µm, bulging, with 3–8 papillae per cell, with moderately thick walls, not particularly collenchymatous;</text>
      <biological_entity constraint="distal" id="o2892" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate-hexagonal" value_original="quadrate-hexagonal" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s6" to="15" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
        <character is_modifier="false" modifier="not particularly" name="architecture" notes="" src="d0_s6" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity id="o2893" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
      <biological_entity id="o2894" name="cell" name_original="cell" src="d0_s6" type="structure" />
      <biological_entity id="o2895" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="moderately" name="width" src="d0_s6" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o2892" id="r688" name="with" negation="false" src="d0_s6" to="o2893" />
      <relation from="o2893" id="r689" name="per" negation="false" src="d0_s6" to="o2894" />
      <relation from="o2892" id="r690" name="with" negation="false" src="d0_s6" to="o2895" />
    </statement>
    <statement id="d0_s7">
      <text>distal marginal cells not or weakly to strongly differentiated as a border of brownish, thicker-walled cells in about 2–3 rows.</text>
      <biological_entity id="o2897" name="border" name_original="border" src="d0_s7" type="structure" constraint="cell" constraint_original="cell; cell" />
      <biological_entity id="o2898" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="brownish" value_original="brownish" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="thicker-walled" value_original="thicker-walled" />
      </biological_entity>
      <biological_entity id="o2899" name="row" name_original="rows" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o2897" id="r691" name="part_of" negation="false" src="d0_s7" to="o2898" />
      <relation from="o2897" id="r692" name="in" negation="false" src="d0_s7" to="o2899" />
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="distal marginal" id="o2896" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character constraint="as border" constraintid="o2897" is_modifier="false" modifier="weakly to strongly" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta red, 10–15 mm.</text>
      <biological_entity id="o2900" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule red, (1.5–) 2–4 mm, slightly curved, rather gradually tapered to the seta or with a distinct neck;</text>
      <biological_entity id="o2901" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character constraint="to " constraintid="o2903" is_modifier="false" modifier="rather gradually" name="shape" src="d0_s11" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o2902" name="seta" name_original="seta" src="d0_s11" type="structure" />
      <biological_entity id="o2903" name="neck" name_original="neck" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum 1–1.5 mm, red;</text>
      <biological_entity id="o2904" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome 1–1.5 mm, teeth twisted about 1/2 turn, the distal divisions yellow, the basal membrane pale, about 1/4 the total length.</text>
      <biological_entity id="o2905" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2906" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="twisted" value_original="twisted" />
        <character name="quantity" src="d0_s13" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2907" name="division" name_original="divisions" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2908" name="membrane" name_original="membrane" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale" value_original="pale" />
        <character name="length" src="d0_s13" value="1/4" value_original="1/4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 13–20 µm, densely papillose.</text>
      <biological_entity id="o2909" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s14" to="20" to_unit="um" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bark of trees, rock (usually calcareous), occasionally soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bark" constraint="of trees , rock ( usually calcareous ) , occasionally soil" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="calcareous" modifier="usually" />
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., N.C., Okla., Tenn., Tex., Va., W.Va.; Mexico; Central America; South America; Europe; Asia (China, Iran); Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Iran)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Syntrichia fragilis shows considerable variability in size and leaf shape (apparently correlated with habitat conditions). In some small, corticolous plants, extreme fragmentation is evident, with an abnormal development of the lamina beyond the usual outline of the leaf. In such plants, several regions of each leaf seem to remain meristematic and produce scalloped or ruffled extensions that eventually break off. Fragility of leaves should not be considered an infallibly diagnostic character, however, since some plants of S. fragilis have quite firm leaves, and the leaves of some other species, particularly when old, may become broken.</discussion>
  
</bio:treatment>