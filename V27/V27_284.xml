<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="(Schimper) Limpricht" date="1889" rank="species">atrofuscum</taxon_name>
    <place_of_publication>
      <publication_title>Laubm. Deutschl.</publication_title>
      <place_in_publication>1: 713. 1889,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species atrofuscum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075494</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="species">atrofusca</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Eur. ed.</publication_title>
      <place_in_publication>2, 240. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species atrofusca;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in compact cushions or tufts, dark-brown (-olivaceous) or near black.</text>
      <biological_entity id="o5299" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="near black" is_modifier="false" name="coloration" notes="" src="d0_s0" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="near black" value_original="near black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5300" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o5301" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o5299" id="r1477" name="in" negation="false" src="d0_s0" to="o5300" />
      <relation from="o5299" id="r1478" name="in" negation="false" src="d0_s0" to="o5301" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.6–2.5 cm, central strand distinct.</text>
      <biological_entity id="o5302" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o5303" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually erect, occasionally slightly curved, sometimes towards stem when dry, ovate-triangular to ovatelanceolate, moderately keeled distally, 1.2–2.1 mm, with 2-stratose patches or striae or 2-stratose distally;</text>
      <biological_entity id="o5304" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="occasionally slightly" name="course" src="d0_s2" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o5305" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate-triangular" modifier="when dry" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character is_modifier="false" modifier="moderately; distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s2" to="2.1" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o5306" name="patch" name_original="patches" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o5307" name="stria" name_original="striae" src="d0_s2" type="structure" />
      <relation from="o5304" id="r1479" modifier="sometimes" name="towards" negation="false" src="d0_s2" to="o5305" />
      <relation from="o5305" id="r1480" name="with" negation="false" src="d0_s2" to="o5306" />
      <relation from="o5305" id="r1481" name="with" negation="false" src="d0_s2" to="o5307" />
    </statement>
    <statement id="d0_s3">
      <text>margins weakly recurved to just before the apex, distal half often plane, proximally often more broadly recurved on one side of leaf, 2-stratose or 3-stratose;</text>
      <biological_entity id="o5308" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="just before apex" constraintid="o5309" is_modifier="false" modifier="weakly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o5309" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o5310" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character constraint="on side" constraintid="o5311" is_modifier="false" modifier="proximally often; often; broadly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-stratose" value_original="3-stratose" />
      </biological_entity>
      <biological_entity id="o5311" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o5312" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <relation from="o5311" id="r1482" name="part_of" negation="false" src="d0_s3" to="o5312" />
    </statement>
    <statement id="d0_s4">
      <text>apices obtuse or subacute;</text>
      <biological_entity id="o5313" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa subpercurrent or percurrent, rarely excurrent as a tiny awn, smooth;</text>
      <biological_entity id="o5314" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character constraint="as awn" constraintid="o5315" is_modifier="false" modifier="rarely" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o5315" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="tiny" value_original="tiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate with transverse walls sometimes thicker than longitudinal walls;</text>
      <biological_entity constraint="basal marginal" id="o5316" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character constraint="with walls" constraintid="o5317" is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character constraint="than longitudinal walls" constraintid="o5318" is_modifier="false" name="width" src="d0_s6" value="sometimes thicker" value_original="sometimes thicker" />
      </biological_entity>
      <biological_entity id="o5317" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s6" value="transverse" value_original="transverse" />
        <character constraint="than longitudinal walls" constraintid="o5318" is_modifier="false" name="width" src="d0_s6" value="sometimes thicker" value_original="sometimes thicker" />
      </biological_entity>
      <biological_entity id="o5318" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s6" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells usually rounded or short-rectangular, 6–9 µm wide, smooth, weakly sinuose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o5319" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s7" to="9" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule orange or redbrown, often yellowish, short cylindric or cupulate, 0.75–1 mm;</text>
      <biological_entity id="o5320" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="0.75" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells primarily elongate mixed with patches of irregularly angular isodiametric cells, walls sometimes unevenly thickened and curved;</text>
      <biological_entity constraint="exothecial" id="o5321" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="primarily" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character constraint="with patches" constraintid="o5322" is_modifier="false" name="arrangement" src="d0_s10" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o5322" name="patch" name_original="patches" src="d0_s10" type="structure" />
      <biological_entity id="o5323" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="irregularly" name="arrangement_or_shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o5324" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes unevenly" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
      </biological_entity>
      <relation from="o5322" id="r1483" name="part_of" negation="false" src="d0_s10" to="o5323" />
    </statement>
    <statement id="d0_s11">
      <text>stomata absent;</text>
      <biological_entity id="o5325" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome patent, very short, 30–100 µm, or rudimentary, orange or orange-red, densely papillose, strongly perforated and unevenly margined.</text>
      <biological_entity id="o5326" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="patent" value_original="patent" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s12" to="100" to_unit="um" />
        <character is_modifier="false" name="prominence" src="d0_s12" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s12" value="perforated" value_original="perforated" />
        <character is_modifier="false" modifier="unevenly" name="architecture" src="d0_s12" value="margined" value_original="margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 11–15 (–19) µm, granulose or smooth.</text>
      <biological_entity id="o5327" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s13" to="19" to_unit="um" />
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s13" to="15" to_unit="um" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="granulose" value_original="granulose" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open to somewhat shaded limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open to shaded limestone" modifier="somewhat" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1500-2300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1500" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., N.Mex.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Schistidium atrofuscum may be the rarest North American species of the genus. It is characterized by its dark brown or nearly black, compact cushions or tufts, mostly erect, moderately keeled leaves, often 2-stratose distal lamina, and the short or rudimentary, strongly perforated peristome. G. N. Jones (1933) listed this species from Tennessee, but that has not been confirmed.</discussion>
  
</bio:treatment>