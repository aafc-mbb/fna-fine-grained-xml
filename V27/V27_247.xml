<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">funariaceae</taxon_name>
    <taxon_name authority="Schwägrichen" date="1823" rank="genus">entosthodon</taxon_name>
    <taxon_name authority="(Bartram) Grout" date="1935" rank="species">tucsonii</taxon_name>
    <place_of_publication>
      <publication_title>Moss Fl. N. Amer.</publication_title>
      <place_in_publication>2: 81. 1935,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family funariaceae;genus entosthodon;species tucsonii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075509</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Funaria</taxon_name>
    <taxon_name authority="Bartram" date="unknown" rank="species">tucsonii</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>31: 91, plate 8, figs. G–N. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Funaria;species tucsonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physcomitrium</taxon_name>
    <taxon_name authority="Grout" date="unknown" rank="species">haringiae</taxon_name>
    <taxon_hierarchy>genus Physcomitrium;species haringiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–5 mm, pale yellow-green.</text>
      <biological_entity id="o7745" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale yellow-green" value_original="pale yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves variously contorted when dry, oblong-lanceolate to narrowly obovate, imbricate, somewhat concave, mostly 2.5–4 mm;</text>
      <biological_entity id="o7746" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s1" to="narrowly obovate" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character char_type="range_value" from="2.5" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins serrulate to serrate by projecting ends of thin-walled cells;</text>
      <biological_entity id="o7747" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="by ends" constraintid="o7748" from="serrulate" name="architecture_or_shape" src="d0_s2" to="serrate" />
      </biological_entity>
      <biological_entity id="o7748" name="end" name_original="ends" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o7749" name="bud" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <relation from="o7748" id="r2158" name="part_of" negation="false" src="d0_s2" to="o7749" />
    </statement>
    <statement id="d0_s3">
      <text>apices gradually narrowed to an acumen bearing a filiform tip of 3–4 cells;</text>
      <biological_entity id="o7750" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character constraint="to acumen" constraintid="o7751" is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o7751" name="acumen" name_original="acumen" src="d0_s3" type="structure" />
      <biological_entity id="o7752" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o7753" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <relation from="o7751" id="r2159" name="bearing a" negation="false" src="d0_s3" to="o7752" />
      <relation from="o7750" id="r2160" name="consist_of" negation="false" src="d0_s3" to="o7753" />
    </statement>
    <statement id="d0_s4">
      <text>costa ending 4–7 cells before the awn;</text>
      <biological_entity id="o7754" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o7755" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
      <biological_entity id="o7756" name="awn" name_original="awn" src="d0_s4" type="structure" />
      <relation from="o7754" id="r2161" name="ending" negation="false" src="d0_s4" to="o7755" />
      <relation from="o7754" id="r2162" name="before" negation="false" src="d0_s4" to="o7756" />
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells rectangular (85–110 × 32–40 µm), distal cells irregularly rectangular to oblong-rectangular, or polygonal, somewhat rhomboid near the margins.</text>
      <biological_entity constraint="basal laminal" id="o7757" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7758" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="irregularly rectangular" name="shape" src="d0_s5" to="oblong-rectangular or polygonal" />
        <character char_type="range_value" from="irregularly rectangular" name="shape" src="d0_s5" to="oblong-rectangular or polygonal" />
        <character constraint="near margins" constraintid="o7759" is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="rhomboid" value_original="rhomboid" />
      </biological_entity>
      <biological_entity id="o7759" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Seta brownish, 6–10 mm, straight, not hygroscopic.</text>
      <biological_entity id="o7760" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="hygroscopic" value_original="hygroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule ovoid-pyriform from a long neck approximating half the total length, 2–3 mm, sulcate especially in the neck when dry and empty;</text>
      <biological_entity id="o7761" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character constraint="from neck" constraintid="o7762" is_modifier="false" name="shape" src="d0_s7" value="ovoid-pyriform" value_original="ovoid-pyriform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character constraint="in neck" constraintid="o7763" is_modifier="false" name="architecture" src="d0_s7" value="sulcate" value_original="sulcate" />
      </biological_entity>
      <biological_entity id="o7762" name="neck" name_original="neck" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o7763" name="neck" name_original="neck" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>exothecial cells scarcely thickened, oblong (2–3:1) and transversely elongate in 5–7 rows proximal to the mouth;</text>
      <biological_entity constraint="exothecial" id="o7764" name="bud" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character constraint="in rows" constraintid="o7765" is_modifier="false" modifier="transversely" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o7765" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="7" />
        <character constraint="to mouth" constraintid="o7766" is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o7766" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>operculum rounded-conic;</text>
      <biological_entity constraint="exothecial" id="o7767" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <biological_entity id="o7768" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded-conic" value_original="rounded-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome rudimentary or absent, exostome teeth brownish, irregularly blunt-tipped, striate-papillose, endostome segments shortly rounded triangular, extending to about the tooth middle, smooth.</text>
      <biological_entity constraint="exothecial" id="o7769" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity id="o7770" name="peristome" name_original="peristome" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o7771" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s10" value="blunt-tipped" value_original="blunt-tipped" />
        <character is_modifier="false" name="relief" src="d0_s10" value="striate-papillose" value_original="striate-papillose" />
      </biological_entity>
      <biological_entity constraint="endostome" id="o7772" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7773" name="tooth" name_original="tooth" src="d0_s10" type="structure">
        <character is_modifier="true" name="position" src="d0_s10" value="middle" value_original="middle" />
      </biological_entity>
      <relation from="o7772" id="r2163" name="extending to about the" negation="false" src="d0_s10" to="o7773" />
    </statement>
    <statement id="d0_s11">
      <text>Calyptra cucullate, long-beaked, inflated around the capsule, large, smooth.</text>
      <biological_entity id="o7774" name="calyptra" name_original="calyptra" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="long-beaked" value_original="long-beaked" />
        <character constraint="around capsule" constraintid="o7775" is_modifier="false" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7775" name="capsule" name_original="capsule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Spores 32–40 µm, papillose.</text>
      <biological_entity id="o7776" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="32" from_unit="um" name="some_measurement" src="d0_s12" to="40" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, along intermittent desert streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" constraint="along intermittent desert streams" />
        <character name="habitat" value="intermittent desert streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  
</bio:treatment>