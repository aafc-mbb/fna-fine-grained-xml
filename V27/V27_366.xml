<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Spence</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="treatment_page">262</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Thériot" date="1928" rank="genus">JAFFUELIOBRYUM</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol., n. s.</publication_title>
      <place_in_publication>1: 192, plate 8, figs. 1–5. 1928  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus JAFFUELIOBRYUM</taxon_hierarchy>
    <other_info_on_name type="etymology">For Félix Jafuell, 1857–1931, clergyman who collected plants in South America, and Greek bryum, moss</other_info_on_name>
    <other_info_on_name type="fna_id">116720</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–15 (–20) mm, in dense cushions, dark olive, yellow-green, or dull green to olivaceous.</text>
      <biological_entity id="o7653" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s0" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="dark olive" value_original="dark olive" />
        <character char_type="range_value" from="dull green" name="coloration" src="d0_s0" to="olivaceous" />
        <character char_type="range_value" from="dull green" name="coloration" src="d0_s0" to="olivaceous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7654" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o7653" id="r1823" name="in" negation="false" src="d0_s0" to="o7654" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves broadly ovate to obovate, concave or somewhat keeled, margins plane, smooth or weakly serrate to erose distally, distal lamina mostly 1-stratose, rarely with 2-stratose streaks, specialized laminal and marginal chlorophyllose structures absent, awn short to twice lamina length, distal leaves typically with longer awns;</text>
      <biological_entity id="o7655" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s1" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s1" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o7656" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character char_type="range_value" from="weakly serrate" modifier="distally" name="architecture" src="d0_s1" to="erose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7657" name="lamina" name_original="lamina" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="development" notes="" src="d0_s1" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="structure_in_adjective_form" notes="" src="d0_s1" value="laminal" value_original="laminal" />
      </biological_entity>
      <biological_entity id="o7658" name="streak" name_original="streaks" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o7659" name="structure" name_original="structures" src="d0_s1" type="structure">
        <character is_modifier="true" name="position" src="d0_s1" value="marginal" value_original="marginal" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7660" name="awn" name_original="awn" src="d0_s1" type="structure">
        <character is_modifier="false" name="length" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7661" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="longer" id="o7662" name="awn" name_original="awns" src="d0_s1" type="structure" />
      <relation from="o7657" id="r1824" modifier="rarely" name="with" negation="false" src="d0_s1" to="o7658" />
      <relation from="o7657" id="r1825" modifier="rarely" name="with" negation="false" src="d0_s1" to="o7659" />
      <relation from="o7661" id="r1826" name="with" negation="false" src="d0_s1" to="o7662" />
    </statement>
    <statement id="d0_s2">
      <text>basal-cells elongate-rectangular to oblong, with straight, somewhat lax, sometimes weakly hyaline walls;</text>
      <biological_entity id="o7663" name="basal-cell" name_original="basal-cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="elongate-rectangular" name="shape" src="d0_s2" to="oblong" />
      </biological_entity>
      <biological_entity id="o7664" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="true" modifier="somewhat" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="true" modifier="sometimes weakly" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o7663" id="r1827" name="with" negation="false" src="d0_s2" to="o7664" />
    </statement>
    <statement id="d0_s3">
      <text>mid leaf and distal cells isodiametric, oval to irregularly rhomboidal;</text>
      <biological_entity constraint="mid" id="o7665" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s3" to="irregularly rhomboidal" />
      </biological_entity>
      <biological_entity constraint="mid distal" id="o7666" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s3" to="irregularly rhomboidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal cells becoming hyaline with age.</text>
      <biological_entity constraint="distal" id="o7667" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character constraint="with age" constraintid="o7668" is_modifier="false" modifier="becoming" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o7668" name="age" name_original="age" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Gemmae absent.</text>
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition autoicous or rarely cryptoicous;</text>
      <biological_entity id="o7669" name="gemma" name_original="gemmae" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s6" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perichaetial leaves enlarged, long-awned.</text>
      <biological_entity constraint="perichaetial" id="o7670" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="long-awned" value_original="long-awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta short, straight.</text>
      <biological_entity id="o7671" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule erect, immersed, symmetric, globose to ovoid;</text>
      <biological_entity id="o7672" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="shape" src="d0_s9" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s9" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>annulus differentiated, typically persistent;</text>
      <biological_entity id="o7673" name="annulus" name_original="annulus" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="typically" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>operculum conic, short to long-rostrate, falling detached from columella.</text>
      <biological_entity id="o7674" name="operculum" name_original="operculum" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s11" value="long-rostrate" value_original="long-rostrate" />
        <character is_modifier="false" name="life_cycle" src="d0_s11" value="falling" value_original="falling" />
        <character constraint="from columella" constraintid="o7675" is_modifier="false" name="fusion" src="d0_s11" value="detached" value_original="detached" />
      </biological_entity>
      <biological_entity id="o7675" name="columella" name_original="columella" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Calyptra campanulate, mitrate, sometimes erose or lobed at base, large, covering 1/2–3/4 of capsule, plicate.</text>
      <biological_entity id="o7676" name="calyptra" name_original="calyptra" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_relief" src="d0_s12" value="erose" value_original="erose" />
        <character constraint="at base" constraintid="o7677" is_modifier="false" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="size" notes="" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="false" name="position_relational" src="d0_s12" value="covering" value_original="covering" />
        <character char_type="range_value" constraint="of capsule" constraintid="o7678" from="1/2" name="quantity" src="d0_s12" to="3/4" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" notes="" src="d0_s12" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o7677" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o7678" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, South America (Bolivia), c Eurasia (China, Mongolia, Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="c Eurasia (China)" establishment_means="native" />
        <character name="distribution" value="c Eurasia (Mongolia)" establishment_means="native" />
        <character name="distribution" value="c Eurasia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Jaffueliobryum is distributed in cold to hot and arid regions, and usually is found on calcareous rock. It appears to be closest to Coscinodon, from which it differs by the broader, less strongly keeled, mostly 1-stratose leaves, strictly autoicous-cryptoicous sexuality, and calciphilous ecology. The third species, J. arsenei (Thériot) Thériot, larger than our species, is endemic to Mexico and is characterized by a long-rostrate operculum and an exserted capsule.</discussion>
  <references>
    <reference>Churchill, S. P. 1987. Systematics and biogeography of Jaffueliobryum (Grimmiaceae). Mem. New York Bot. Gard. 45: 691–708.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal lamina acute to acuminate, leaves distinctly keeled, proximal stem leaves mostly spreading</description>
      <determination>1 Jaffueliobryum raui</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal lamina broadly acute-rounded, leaves not keeled, proximal stem leaves mostly appressed</description>
      <determination>2 Jaffueliobryum wrightii</determination>
    </key_statement>
  </key>
</bio:treatment>