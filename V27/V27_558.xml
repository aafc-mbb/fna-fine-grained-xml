<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">393</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="(Müller Hal.) Schimper" date="1856" rank="genus">dicranella</taxon_name>
    <taxon_name authority="(Hedwig) Schimper" date="1856" rank="species">crispa</taxon_name>
    <place_of_publication>
      <publication_title>Coroll. Bryol. Eur.,</publication_title>
      <place_in_publication>13. 1856,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranella;species crispa</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443644</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">crispum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>132. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species crispum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anisothecium</taxon_name>
    <taxon_name authority="(Hedwig) Lindberg" date="unknown" rank="species">crispum</taxon_name>
    <taxon_hierarchy>genus Anisothecium;species crispum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anisothecium</taxon_name>
    <taxon_name authority="(Withering) Loeske" date="unknown" rank="species">vaginale</taxon_name>
    <taxon_hierarchy>genus Anisothecium;species vaginale;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Dickson" date="unknown" rank="species">vaginale</taxon_name>
    <taxon_hierarchy>genus Bryum;species vaginale;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to about 5 mm, yellow-green.</text>
      <biological_entity id="o593" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="to about 5 mm" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 0.6–2 mm, the distal leaves squarrose and linear-subulate from a sheathing base, the proximal spreading-flexuose and lanceolate;</text>
      <biological_entity id="o594" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o595" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="squarrose" value_original="squarrose" />
        <character constraint="from base" constraintid="o596" is_modifier="false" name="shape" src="d0_s1" value="linear-subulate" value_original="linear-subulate" />
      </biological_entity>
      <biological_entity id="o596" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o597" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="spreading-flexuose" value_original="spreading-flexuose" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins plane, usually serrulate at the apex;</text>
      <biological_entity id="o598" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character constraint="at apex" constraintid="o599" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o599" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent (nearly filling the slender apex);</text>
      <biological_entity id="o600" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal cells 2-stratose, long and narrow (6–8:1).</text>
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition dioicous (also reported to autoicous).</text>
      <biological_entity constraint="distal" id="o601" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="8" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Perichaetial leaves 2–3 mm, plainly squarrose.</text>
      <biological_entity constraint="perichaetial" id="o602" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="plainly" name="orientation" src="d0_s6" value="squarrose" value_original="squarrose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta red, 5–18 mm.</text>
      <biological_entity id="o603" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule erect or nearly so, tapered to the base and ribbed when dry, 0.7–0.9 mm;</text>
      <biological_entity id="o604" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s8" value="nearly" value_original="nearly" />
        <character constraint="to base" constraintid="o605" is_modifier="false" name="shape" src="d0_s8" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="when dry" name="architecture_or_shape" notes="" src="d0_s8" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o605" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>annulus of 2 rows of cells, deciduous or persistent;</text>
      <biological_entity id="o606" name="annulus" name_original="annulus" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o607" name="row" name_original="rows" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o608" name="cell" name_original="cells" src="d0_s9" type="structure" />
      <relation from="o606" id="r151" name="consist_of" negation="false" src="d0_s9" to="o607" />
      <relation from="o607" id="r152" name="part_of" negation="false" src="d0_s9" to="o608" />
    </statement>
    <statement id="d0_s10">
      <text>operculum rostrate, sometimes curved;</text>
      <biological_entity id="o609" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rostrate" value_original="rostrate" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s10" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>peristome teeth 300–350 µm, divided 1/2 of the length distally.</text>
      <biological_entity constraint="peristome" id="o610" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character char_type="range_value" from="300" from_unit="um" name="some_measurement" src="d0_s11" to="350" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s11" value="divided" value_original="divided" />
        <character modifier="distally" name="length" src="d0_s11" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores 17–20 µm, papillose to smooth.</text>
      <biological_entity id="o611" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="17" from_unit="um" name="some_measurement" src="d0_s12" to="20" to_unit="um" />
        <character char_type="range_value" from="papillose" name="relief" src="d0_s12" to="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring and summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, often sandy or silty soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="sandy" modifier="often" />
        <character name="habitat" value="silty soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>medium to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Ont., Que., Yukon; Alaska, Calif., Colo., Idaho, Mont., Oreg., Wash., Wyo.; n, c Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>The leaves of Dicranella crispa, particularly the distal, are wide-spreading from a clasping base, while the slender capsules are erect, tapered to the base, and distinctly striate. According to R. S. Williams (1913), the male inflorescence may be on a separate branch or separate plant. The distribution above includes reports by E. Lawton (1971) and D. H. Norris and J. R. Shevock (2004).</discussion>
  
</bio:treatment>