<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">119</other_info_on_meta>
    <other_info_on_meta type="illustration_page">117</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">buxbaumiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">buxbaumia</taxon_name>
    <taxon_name authority="Best" date="1893" rank="species">piperi</taxon_name>
    <place_of_publication>
      <publication_title>Bull.Torrey Bot. Club</publication_title>
      <place_in_publication>20: 116. 1893,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family buxbaumiaceae;genus buxbaumia;species piperi</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443348</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Seta 3–6 mm, straight to somewhat arcuate.</text>
      <biological_entity id="o4521" name="seta" name_original="seta" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s0" to="6" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight to somewhat" value_original="straight to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="course_or_shape" src="d0_s0" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Capsule when mature ovoid to narrowly ovoid, 3–5 mm, upper face with perimeter ridge differentiating it from lower face, dull brown to greenish brown, not glossy.</text>
      <biological_entity id="o4522" name="capsule" name_original="capsule" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" modifier="when mature ovoid to narrowly ovoid" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o4523" name="face" name_original="face" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s1" value="dull" value_original="dull" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s1" to="greenish brown" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s1" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity id="o4524" name="perimeter" name_original="perimeter" src="d0_s1" type="structure" />
      <biological_entity id="o4525" name="ridge" name_original="ridge" src="d0_s1" type="structure" />
      <biological_entity constraint="lower" id="o4526" name="face" name_original="face" src="d0_s1" type="structure" />
      <relation from="o4523" id="r1243" name="with" negation="false" src="d0_s1" to="o4524" />
      <relation from="o4523" id="r1244" name="with" negation="false" src="d0_s1" to="o4525" />
      <relation from="o4524" id="r1245" name="from" negation="false" src="d0_s1" to="o4526" />
      <relation from="o4525" id="r1246" name="from" negation="false" src="d0_s1" to="o4526" />
    </statement>
    <statement id="d0_s2">
      <text>Spores 7–12 µm.</text>
      <biological_entity id="o4527" name="spore" name_original="spores" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s2" to="12" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Commonly on rotten decorticated logs, but also on humus banks, mainly subalpine but also occasionally to near sea level, mainly in coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rotten decorticated" modifier="commonly on" />
        <character name="habitat" value="humus banks" modifier="logs but also on" />
        <character name="habitat" value="subalpine but also occasionally to near sea level" />
        <character name="habitat" value="coniferous forests" modifier="mainly in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (10-1500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="10" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Alaska, Calif., Colo., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>This endemic species may resemble Buxbaumia aphylla. The sporangium however, is never glossy chestnut when mature, but is dull and greenish brown; the exostome is in 2–3 rows rather than one row as in B. aphylla.</discussion>
  <references>
    <reference>Ligrone, R. R. et al. 1982. Gametophyte and sporophyte ultrastructure in Buxbaumia piperi (Buxaumiaceae, Musci). J. Hattori Bot. Lab. 52: 465–499.</reference>
  </references>
  
</bio:treatment>