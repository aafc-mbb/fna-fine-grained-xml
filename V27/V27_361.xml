<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="treatment_page">259</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Sprengel" date="1804" rank="genus">coscinodon</taxon_name>
    <taxon_name authority="(Drummond) C. E. O. Jensen in N. C. Kindberg" date="1897" rank="species">calyptratus</taxon_name>
    <place_of_publication>
      <publication_title>in N. C. Kindberg, Eur. N. Amer. Bryin.</publication_title>
      <place_in_publication>2: 241. 1897,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus coscinodon;species calyptratus</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443375</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Drummond" date="unknown" rank="species">calyptrata</taxon_name>
    <place_of_publication>
      <publication_title>Musc. Amer.,</publication_title>
      <place_in_publication>60. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species calyptrata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coscinodon</taxon_name>
    <taxon_name authority="Hampe" date="unknown" rank="species">hookeri</taxon_name>
    <taxon_hierarchy>genus Coscinodon;species hookeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">columbica</taxon_name>
    <taxon_hierarchy>genus Grimmia;species columbica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 7–10 mm, olivaceous.</text>
      <biological_entity id="o2183" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves ovate to ovatelanceolate, 1.4–2.4 × 0.4–0.7 mm, margins plane or one margin recurved at mid leaf, apex plane, awn 0.4–1.4 mm, lamina non-plicate;</text>
      <biological_entity id="o2184" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="ovatelanceolate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s1" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s1" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2185" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character name="shape" src="d0_s1" value="one" value_original="one" />
      </biological_entity>
      <biological_entity id="o2186" name="margin" name_original="margin" src="d0_s1" type="structure">
        <character constraint="at mid leaf" constraintid="o2187" is_modifier="false" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="mid" id="o2187" name="leaf" name_original="leaf" src="d0_s1" type="structure" />
      <biological_entity id="o2188" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o2189" name="awn" name_original="awn" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s1" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2190" name="lamina" name_original="lamina" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s1" value="non-plicate" value_original="non-plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal juxtacostal laminal cells long-rectangular, 15–80 × 7–10 µm, evenly thin-walled;</text>
      <biological_entity constraint="basal" id="o2191" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="laminal" id="o2192" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="long-rectangular" value_original="long-rectangular" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s2" to="80" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s2" to="10" to_unit="um" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal marginal laminal cells quadrate to long-rectangular, 16–60 × 7–15 µm, thin or thick end walls and thin lateral walls;</text>
      <biological_entity constraint="basal marginal laminal" id="o2193" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s3" to="long-rectangular" />
        <character char_type="range_value" from="16" from_unit="um" name="length" src="d0_s3" to="60" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s3" to="15" to_unit="um" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity constraint="end" id="o2195" name="wall" name_original="walls" src="d0_s3" type="structure" constraint_original="lateral end">
        <character is_modifier="true" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>medial laminal cells 1-stratose;</text>
      <biological_entity constraint="medial laminal" id="o2196" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells 1-stratose.</text>
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o2197" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta 1.4–2.4 mm.</text>
      <biological_entity id="o2198" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s7" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule exserted, cylindric, commonly constricted at rim;</text>
      <biological_entity id="o2199" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character constraint="at rim" constraintid="o2200" is_modifier="false" modifier="commonly" name="size" src="d0_s8" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o2200" name="rim" name_original="rim" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>peristome present, solid, xerocastique.</text>
      <biological_entity id="o2201" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="solid" value_original="solid" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="xerocastique" value_original="xerocastique" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Most common on dry sandstone and granitic boulders and bedrock exposures but also limestone and volcanic outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sandstone" modifier="most common on" />
        <character name="habitat" value="granitic boulders" />
        <character name="habitat" value="bedrock exposures" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="volcanic outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (300-3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="300" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Ariz., Calif., Colo., Idaho, Minn., Mont., Nev., N.Mex., Oreg., S.Dak., Utah, Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Coscinodon calyptratus is common and widespread in the dry interior mountain areas of western North America. To the east it is largely bounded by the front ranges of the Rocky Mountains and acidic outliers such as the Black Hills of South Dakota. Two disjunct sites in Wisconsin and Minnesota define the eastward extent of the species. It is not found along the west coast mountains. Specimens from central Asia and New Zealand attributed to this North American endemic by J. Muñoz (1998c) instead represent an undescribed species of Coscinodon and a Grimmia. The non-plicate, 1-stratose leaves with plane or one revolute margin, and exserted capsule clearly separate this species from all other North American Coscinodon. The autoicous sexual condition is unique in the genus but often difficult to ascertain on any particular specimen. This species is more commonly confused with G. longirostris (formerly G. affinis) than with any other Coscinodon. Both species are autoicous, have a similar robust habit, have leaves that are strongly keeled and may have recurved margins, and have long-exserted capsules. If fertile and with capsules, the species are easily distinguished, as C. calyptratus has large, plicate calyptra and has a thin-walled filmy annulus. In contrast, G. longirostris has smaller, smooth calyptra and a large, prominent annulus. Gametophytically, C. calyptratus has 1-stratose areolation across the distal and medial lamina except for a 2-stratose margin. Its basal cells tend to be evenly thin-walled. Grimmia longirostris has a 2-stratose distal lamina and a wide 2-stratose margin at mid leaf. Its basal cells tend to be thicker walled.</discussion>
  
</bio:treatment>