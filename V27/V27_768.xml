<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="treatment_page">533</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">barbula</taxon_name>
    <taxon_name authority="(Mitten) A. Jaeger" date="1873" rank="species">amplexifolia</taxon_name>
    <place_of_publication>
      <publication_title>Ber. Thätigk. St. Gallischen Naturwiss. Ges.</publication_title>
      <place_in_publication>1871–1872: 424. 1873,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus barbula;species amplexifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443941</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">amplexifolia</taxon_name>
    <place_of_publication>
      <publication_title>J. Proc. Linn. Soc., Bot., suppl.</publication_title>
      <place_in_publication>1: 29. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tortula;species amplexifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="(Cardot) K. Saito" date="unknown" rank="species">coreensis</taxon_name>
    <taxon_hierarchy>genus Barbula;species coreensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="H. A. Crum" date="unknown" rank="species">haringae</taxon_name>
    <taxon_hierarchy>genus Barbula;species haringae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 1 cm.</text>
      <biological_entity id="o2986" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves firm when wet, short-lanceolate to lanceolate, 1.1–1.5 mm, base long-oblong to elliptic and sheathing, margins plane or rarely recurved at mid leaf, apex acute, strongly mucronate;</text>
      <biological_entity id="o2987" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when wet" name="texture" src="d0_s1" value="firm" value_original="firm" />
        <character char_type="range_value" from="short-lanceolate" name="shape" src="d0_s1" to="lanceolate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s1" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2988" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="long-oblong" name="shape" src="d0_s1" to="elliptic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o2989" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character constraint="at mid leaf" constraintid="o2990" is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="mid" id="o2990" name="leaf" name_original="leaf" src="d0_s1" type="structure" />
      <biological_entity id="o2991" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa short-excurrent, abaxial costal surface smooth or occasionally doubly prorate, rarely simply papillose near apex, hydroids absent;</text>
      <biological_entity id="o2992" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity constraint="abaxial costal" id="o2993" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s2" value="occasionally doubly" value_original="occasionally doubly" />
        <character constraint="near apex" constraintid="o2994" is_modifier="false" modifier="rarely simply" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o2994" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o2995" name="hydroid" name_original="hydroids" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal laminal cells firm-walled, quadrate, 7–10 µm wide, 1: 1, papillose.</text>
    </statement>
    <statement id="d0_s4">
      <text>Specialized asexual reproduction by clusters of redbrown ovoid gemmae borne on stalks in leaf-axils, 40–100 (–150) µm long.</text>
      <biological_entity constraint="distal laminal" id="o2996" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s3" to="10" to_unit="um" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="development" src="d0_s4" value="specialized" value_original="specialized" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="um" name="length" src="d0_s4" to="150" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s4" to="100" to_unit="um" />
      </biological_entity>
      <biological_entity id="o2997" name="gemma" name_original="gemmae" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="redbrown" value_original="redbrown" />
        <character is_modifier="true" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o2998" name="stalk" name_original="stalks" src="d0_s4" type="structure" />
      <biological_entity id="o2999" name="leaf-axil" name_original="leaf-axils" src="d0_s4" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="um" name="length" src="d0_s4" to="150" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s4" to="100" to_unit="um" />
      </biological_entity>
      <relation from="o2997" id="r722" name="borne on" negation="false" src="d0_s4" to="o2998" />
    </statement>
    <statement id="d0_s5">
      <text>Sporophytes absent in range of the flora.</text>
      <biological_entity id="o3000" name="sporophyte" name_original="sporophytes" src="d0_s5" type="structure">
        <character constraint="of flora" constraintid="o3001" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3001" name="flora" name_original="flora" src="d0_s5" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, often limestone, occasionally sandstone, usually in moist areas, mountain slopes, cliffs, tundra, mist zone of waterfalls</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist areas" modifier="rock" />
        <character name="habitat" value="limestone" modifier="often" />
        <character name="habitat" value="sandstone" modifier="occasionally" />
        <character name="habitat" value="in moist areas" modifier="usually" />
        <character name="habitat" value="mountain slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="mist zone" constraint="of waterfalls" />
        <character name="habitat" value="waterfalls" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (700-1800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="700" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.W.T.; Alaska, Ariz., Calif.; Asia (India, Japan, Korea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Barbula amplexifolia belongs in sect. Convolutae by the convolute-sheathing perichaetial leaves, lack of hydroid cells in costa, and occasional prorulae on abaxial surface of costa. It is distinguished from B. convoluta most immediately by the stiff and sharply pointed leaves with a smooth, short mucro. On further study, the distinctions between B. coreensis (Cardot) K. Saito and this species made by M. S. Ignatov and R. H. Zander (1993) do not hold; e.g., the type of B. haringae has short, 65–75 µm, propagula and rough costal abaxial surface; a specimen from the Northwest Territories (Steere 76-605, NY) has rough costae and propagula variably 90–150 µm; the type of B. amplexifolia has a smooth costa and intermediate-sized propagula, ca. 90 µm. Barbula convoluta var. eustegia (as in the type of B. whitehouseae) may have a similarly stout costa, but the percurrent costa is apiculate only by a single, usually papillose cell and has narrower, nonsheathing leaves.</discussion>
  
</bio:treatment>