<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard H. Zander</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">610</other_info_on_meta>
    <other_info_on_meta type="treatment_page">609</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="M. Fleischer" date="1904" rank="genus">GYMNOSTOMIELLA</taxon_name>
    <place_of_publication>
      <publication_title>Musc. Buitenzorg</publication_title>
      <place_in_publication>1: 309. 1904,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus GYMNOSTOMIELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Gymnostomum and Latin -ella, diminutive</other_info_on_name>
    <other_info_on_name type="fna_id">114320</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants growing in dense tufts or mats, light green to blackish green distally, brown proximally.</text>
      <biological_entity id="o9256" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light green" name="coloration" notes="" src="d0_s0" to="blackish green distally" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9257" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o9258" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o9256" id="r2193" name="growing in" negation="false" src="d0_s0" to="o9257" />
      <relation from="o9256" id="r2194" name="growing in" negation="false" src="d0_s0" to="o9258" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.5–6 mm;</text>
      <biological_entity id="o9259" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, sclerodermis absent or weak, central strand present, large;</text>
      <biological_entity id="o9260" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9261" name="scleroderm" name_original="sclerodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity constraint="central" id="o9262" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s2" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>axillary hairs ca. 3 cells in length, moniliform, proximal cell thicker walled.</text>
      <biological_entity constraint="axillary" id="o9263" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o9264" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="moniliform" value_original="moniliform" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9265" name="cell" name_original="cell" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thicker" value_original="thicker" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="walled" value_original="walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves erect or laxly spreading when dry, erect to spreading and lax when moist;</text>
    </statement>
    <statement id="d0_s5">
      <text>obovate to oblong-obovate, adaxial surface broadly channeled to nearly flat, larger distally, 0.3–0.4 mm;</text>
      <biological_entity id="o9266" name="stem-leaf" name_original="stem-leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s4" to="spreading" />
        <character is_modifier="false" modifier="when moist" name="architecture_or_arrangement" src="d0_s4" value="lax" value_original="lax" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblong-obovate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9267" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly channeled" name="shape" src="d0_s5" to="nearly flat" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="larger" value_original="larger" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base not differentiated in shape, proximal margins not differentiated;</text>
      <biological_entity id="o9268" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9269" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal margins plane, crenulate distally by sharply bulging cell-walls, sometimes serrulate, often with 1–2 teeth above;</text>
      <biological_entity constraint="distal" id="o9270" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="plane" value_original="plane" />
        <character constraint="by cell-walls" constraintid="o9271" is_modifier="false" name="shape" src="d0_s7" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o9271" name="cell-wall" name_original="cell-walls" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="sharply" name="pubescence_or_shape" src="d0_s7" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity id="o9272" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <relation from="o9270" id="r2195" modifier="often" name="with" negation="false" src="d0_s7" to="o9272" />
    </statement>
    <statement id="d0_s8">
      <text>apex rounded or very broadly acute, occasionally apiculate;</text>
      <biological_entity id="o9273" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="very broadly" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s8" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa slender and ending at mid leaf or stronger and ending near apex, adaxial outgrowths absent, adaxial cells elongate, in 2 rows;</text>
      <biological_entity id="o9274" name="costa" name_original="costa" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s9" value="stronger" value_original="stronger" />
      </biological_entity>
      <biological_entity constraint="mid" id="o9275" name="leaf" name_original="leaf" src="d0_s9" type="structure" />
      <biological_entity id="o9276" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity constraint="adaxial" id="o9277" name="outgrowth" name_original="outgrowths" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9278" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o9279" name="row" name_original="rows" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o9274" id="r2196" name="ending at" negation="false" src="d0_s9" to="o9275" />
      <relation from="o9274" id="r2197" name="ending near" negation="false" src="d0_s9" to="o9276" />
      <relation from="o9278" id="r2198" name="in" negation="false" src="d0_s9" to="o9279" />
    </statement>
    <statement id="d0_s10">
      <text>transverse-section elliptic, adaxial epidermis absent, adaxial stereid band absent, guide cells 2 in 1 layer, hydroid strand absent, abaxial stereid band small, round in sectional shape, abaxial epidermis present;</text>
      <biological_entity id="o9280" name="transverse-section" name_original="transverse-section" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9281" name="epidermis" name_original="epidermis" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9282" name="epidermis" name_original="epidermis" src="d0_s10" type="structure" />
      <biological_entity id="o9283" name="band" name_original="band" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="guide" id="o9284" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="in layer" constraintid="o9285" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9285" name="layer" name_original="layer" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="hydroid" id="o9286" name="strand" name_original="strand" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9287" name="epidermis" name_original="epidermis" src="d0_s10" type="structure" />
      <biological_entity id="o9288" name="band" name_original="band" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="small" value_original="small" />
        <character is_modifier="false" name="shape" src="d0_s10" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9289" name="epidermis" name_original="epidermis" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>proximal cells weakly differentiated across lamina, rectangular, little wider than distal cells, 2–3: 1, walls of proximal cells thin;</text>
      <biological_entity constraint="proximal" id="o9290" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character constraint="across lamina" constraintid="o9291" is_modifier="false" modifier="weakly" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="rectangular" value_original="rectangular" />
        <character constraint="than distal cells" constraintid="o9292" is_modifier="false" name="width" src="d0_s11" value="wider" value_original="wider" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="3" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9291" name="lamina" name_original="lamina" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o9292" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <biological_entity id="o9293" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9294" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <relation from="o9293" id="r2199" name="part_of" negation="false" src="d0_s11" to="o9294" />
    </statement>
    <statement id="d0_s12">
      <text>distal medial cells quadrate to hexagonal, sometimes longer than broad, 12–18 µm wide, 1 (–2):1, 1-stratose;</text>
      <biological_entity constraint="distal medial" id="o9295" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s12" to="hexagonal" />
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="sometimes longer than broad" value_original="sometimes longer than broad" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s12" to="18" to_unit="um" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="2" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>papillae small, simple, hollow, conical, 1–3 per lumen, scattered, cell-walls thin, delicate, convex on both sides.</text>
      <biological_entity constraint="distal medial" id="o9296" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s13" to="hexagonal" />
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="sometimes longer than broad" value_original="sometimes longer than broad" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s13" to="18" to_unit="um" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="2" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9297" name="papilla" name_original="papillae" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="small" value_original="small" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="conical" value_original="conical" />
        <character char_type="range_value" constraint="per lumen" constraintid="o9298" from="1" name="quantity" src="d0_s13" to="3" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s13" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o9298" name="lumen" name_original="lumen" src="d0_s13" type="structure" />
      <biological_entity id="o9300" name="side" name_original="sides" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Specialized asexual reproduction by elliptic to clavate gemmae, to 280 µm, of about 12 cells, borne in leaf-axils or on adaxial surface of the leaf.</text>
      <biological_entity id="o9299" name="cell-wall" name_original="cell-walls" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s13" value="delicate" value_original="delicate" />
        <character constraint="on sides" constraintid="o9300" is_modifier="false" name="shape" src="d0_s13" value="convex" value_original="convex" />
        <character is_modifier="false" name="development" src="d0_s14" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o9302" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="12" value_original="12" />
      </biological_entity>
      <biological_entity id="o9303" name="leaf-axil" name_original="leaf-axils" src="d0_s14" type="structure" />
      <biological_entity constraint="adaxial" id="o9304" name="surface" name_original="surface" src="d0_s14" type="structure" />
      <biological_entity id="o9305" name="leaf" name_original="leaf" src="d0_s14" type="structure" />
      <relation from="o9301" id="r2200" name="consist_of" negation="false" src="d0_s14" to="o9302" />
      <relation from="o9301" id="r2201" name="borne" negation="false" src="d0_s14" to="o9303" />
      <relation from="o9301" id="r2202" name="borne" negation="false" src="d0_s14" to="o9304" />
      <relation from="o9301" id="r2203" name="part_of" negation="false" src="d0_s14" to="o9305" />
    </statement>
    <statement id="d0_s15">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o9301" name="gemma" name_original="gemmae" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="asexual" value_original="asexual" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s14" to="clavate" />
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" src="d0_s14" to="280" to_unit="um" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Perigonia terminal.</text>
      <biological_entity id="o9306" name="perigonium" name_original="perigonia" src="d0_s16" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s16" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Perichaetia terminal, interior leaves sheathing, ovate, to 0.7 mm, laminal cells laxly rectangular, smooth.</text>
      <biological_entity id="o9307" name="perichaetium" name_original="perichaetia" src="d0_s17" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s17" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="interior" id="o9308" name="leaf" name_original="leaves" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>[Seta 3–6 mm. Capsule stegocarpous, theca ovate to elliptic, 0.6–0.8 mm, annulus of about 2 rows of little differentiated cells; operculum obliquely rostrate from a low-conic base; peristome teeth absent. Calyptra cucullate. Spores 11–15 µm.] KOH laminal color reaction yellow or negative to black or pink to deep purple.</text>
      <biological_entity constraint="laminal" id="o9309" name="cell" name_original="cells" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="laxly" name="shape" src="d0_s17" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="laminal" value_original="laminal" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s18" to="deep purple" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., pantropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="pantropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <discussion>Species 6 (1 in the flora).</discussion>
  <discussion>In the absence of a peristome and no special trait characteristic of another family, Gymnostomiella is referred to the Pottiaceae largely because of its papillose leaves. Phylogenetic analysis supports a hypothesis of a relationship with Barbula and relatives. Distinctive are the central location of the stereid band, the moniliform axillary hairs, the often nearly spheric capsule, and the KOH color reaction of the lamina being variously negative, pink, yellow, black or purple. Cyanobacteria that are often associated with this genus are thought (T. Seki and C. Miyagi 1980) to be important in nitrogen fixation as “parasymbiosis.” J. L. de Sloover (1977) provided a key to all species of the genus.</discussion>
  <references>
    <reference>Sloover, J. L. de. 1977. Note de bryologie Africaine. IX. Andreaea, Racomitrium, Gymnostomiella, Thuidium. Bull. Jard. Bot. Natl. Belg. 47: 155–181.</reference>
  </references>
  
</bio:treatment>