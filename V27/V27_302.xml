<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="treatment_page">221</other_info_on_meta>
    <other_info_on_meta type="illustration_page">217</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="Culmann in J. Amann et al." date="1918" rank="species">papillosum</taxon_name>
    <place_of_publication>
      <publication_title>in J. Amann et al., Fl. Mouss. Suisse</publication_title>
      <place_in_publication>2: 386. 1918,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species papillosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075426</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in open tufts or mats, usually olivaceous, often with red, yellow, brown, or orange tones, rarely nearly black.</text>
      <biological_entity id="o4264" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" modifier="with red" name="coloration" src="d0_s0" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="orange tones" value_original="orange tones" />
        <character is_modifier="false" modifier="rarely nearly" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4265" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o4266" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o4264" id="r1168" name="in" negation="false" src="d0_s0" to="o4265" />
      <relation from="o4264" id="r1169" name="in" negation="false" src="d0_s0" to="o4266" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10 cm, central strand indistinct or absent.</text>
      <biological_entity id="o4267" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o4268" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect or curved when dry, ovatelanceolate, sharply keeled distally, (1.2–) 1.6–2.4 mm, 1-stratose, sometimes with 2-stratose striae distally;</text>
      <biological_entity id="o4269" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="sharply; distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="1.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s2" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o4270" name="stria" name_original="striae" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <relation from="o4269" id="r1170" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o4270" />
    </statement>
    <statement id="d0_s3">
      <text>margins recurved to near apex, often denticulate distally, usually 2-stratose, occasionally in more than one row;</text>
      <biological_entity id="o4271" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="to apex" constraintid="o4272" is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="often; distally" name="shape" notes="" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o4272" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o4274" name="row" name_original="row" src="d0_s3" type="structure" />
      <biological_entity id="o4273" name="row" name_original="row" src="d0_s3" type="structure" />
      <relation from="o4273" id="r1171" modifier="occasionally; occasionally; occasionally; occasionally" name="in" negation="false" src="d0_s3" to="o4274" />
    </statement>
    <statement id="d0_s4">
      <text>apices acute;</text>
      <biological_entity id="o4275" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent or excurrent as a denticulate or spinulose, often flexuose, usually non-decurrent awn, abaxial surface papillose;</text>
      <biological_entity id="o4276" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character constraint="as" is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s5" value="flexuose" value_original="flexuose" />
      </biological_entity>
      <biological_entity id="o4277" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="usually" name="shape" src="d0_s5" value="non-decurrent" value_original="non-decurrent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4278" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate or short-rectangular, usually trigonous;</text>
      <biological_entity constraint="basal marginal" id="o4279" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells short-rectangular, angular, or ovate, 8–10 µm wide, papillose, sinuose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal" id="o4280" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s7" to="10" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule dark redbrown or brown, short-cylindric, 0.9–1.4 (–1.75) mm;</text>
      <biological_entity id="o4281" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark redbrown" value_original="dark redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.75" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells isodiametric or short-elongate, thin-walled, sometimes with small trigones;</text>
      <biological_entity constraint="exothecial" id="o4282" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o4283" name="trigone" name_original="trigones" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
      </biological_entity>
      <relation from="o4282" id="r1172" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o4283" />
    </statement>
    <statement id="d0_s11">
      <text>stomata present;</text>
      <biological_entity id="o4284" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome patent to erect, twisted, red or orangebrown, 300–500 µm, papillose, entire or weakly perforated.</text>
      <biological_entity id="o4285" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character char_type="range_value" from="patent" name="orientation" src="d0_s12" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orangebrown" value_original="orangebrown" />
        <character char_type="range_value" from="300" from_unit="um" name="some_measurement" src="d0_s12" to="500" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s12" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 10–13 µm, granulose or verruculose.</text>
      <biological_entity id="o4286" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s13" to="13" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="granulose" value_original="granulose" />
        <character is_modifier="false" name="relief" src="d0_s13" value="verruculose" value_original="verruculose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, rarely on tree bark, in mesic habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree bark" modifier="rock rarely on" />
        <character name="habitat" value="mesic habitats" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Nunavut, Ont., Que., Yukon; Ala., Alaska, Colo., Idaho, Mich., Minn., Mont., N.Y., Oreg., Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <discussion>See comments under 5. Schistidium boreale regarding differences among the North American species of the genus with papillose laminal cells.</discussion>
  
</bio:treatment>