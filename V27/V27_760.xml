<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="treatment_page">530</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">barbula</taxon_name>
    <taxon_name authority="Müller Hal." date="1876" rank="species">orizabensis</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>40: 638. 1876,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus barbula;species orizabensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443921</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1.5–2.5 cm.</text>
      <biological_entity id="o3199" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves firm when wet, long-ligulate to broadly lanceolate from an oblong base, 1.5–2 mm, base oblong but not strongly sheathing, margins recurved or revolute to apex or nearly so, apex broadly acute to rounded;</text>
      <biological_entity id="o3200" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when wet" name="texture" src="d0_s1" value="firm" value_original="firm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-ligulate" value_original="long-ligulate" />
        <character constraint="from base" constraintid="o3201" is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3201" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o3202" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="not strongly" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o3203" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
        <character constraint="to " constraintid="o3205" is_modifier="false" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o3204" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o3205" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o3206" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity id="o3207" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s1" to="rounded" />
      </biological_entity>
      <relation from="o3205" id="r772" name="to" negation="false" src="d0_s1" to="o3206" />
      <relation from="o3205" id="r773" name="to" negation="false" src="d0_s1" to="o3207" />
    </statement>
    <statement id="d0_s2">
      <text>costa excurrent as a stout mucro, abaxial costal surface with scattered solid papillae, hydroids present;</text>
      <biological_entity id="o3208" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character constraint="as mucro" constraintid="o3209" is_modifier="false" name="architecture" src="d0_s2" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o3209" name="mucro" name_original="mucro" src="d0_s2" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity constraint="abaxial costal" id="o3210" name="surface" name_original="surface" src="d0_s2" type="structure" />
      <biological_entity id="o3211" name="papilla" name_original="papillae" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="solid" value_original="solid" />
      </biological_entity>
      <biological_entity id="o3212" name="hydroid" name_original="hydroids" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o3210" id="r774" name="with" negation="false" src="d0_s2" to="o3211" />
    </statement>
    <statement id="d0_s3">
      <text>distal laminal cells firm-walled, quadrate, 7–9 µm wide, 1: 1, papillose.</text>
    </statement>
    <statement id="d0_s4">
      <text>Specialized asexual reproduction by spheric or occasionally elliptic gemmae on stalks in leaf-axils, 30–35 µm long.</text>
      <biological_entity constraint="distal laminal" id="o3213" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s3" to="9" to_unit="um" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="development" src="d0_s4" value="specialized" value_original="specialized" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s4" to="35" to_unit="um" />
      </biological_entity>
      <biological_entity id="o3214" name="gemma" name_original="gemmae" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="shape" src="d0_s4" value="spheric" value_original="spheric" />
        <character is_modifier="true" modifier="occasionally" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o3215" name="stalk" name_original="stalks" src="d0_s4" type="structure" />
      <biological_entity id="o3216" name="leaf-axil" name_original="leaf-axils" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s4" to="35" to_unit="um" />
      </biological_entity>
      <relation from="o3214" id="r775" name="on" negation="false" src="d0_s4" to="o3215" />
      <relation from="o3215" id="r776" name="in" negation="false" src="d0_s4" to="o3216" />
    </statement>
    <statement id="d0_s5">
      <text>[Perichaetial leaves weakly differentiated, antheridiate plants long-stemmed. Seta 0.9–1.5 cm. Theca 1–2.5 mm. Spores 8–11 µm.]</text>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disintegrating rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" modifier="disintegrating" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1000-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>In the flora area, Barbula orizabensis is known only from Santa Cruz County, Penna Dam, I. Haring 11984, 15 Feb. 1957 (CANM). This species replaces B. unguiculata at approximately the Mexican border. The presence of gemmae is apparently constant and will best distinguish it from the latter species.</discussion>
  
</bio:treatment>