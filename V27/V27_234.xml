<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">179</other_info_on_meta>
    <other_info_on_meta type="treatment_page">178</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">encalyptaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">encalypta</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper) Åongström" date="1844" rank="species">brevicollis</taxon_name>
    <place_of_publication>
      <publication_title>Nova Acta Regiae Soc. Sci. Upsal.</publication_title>
      <place_in_publication>12: 362. 1844,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family encalyptaceae;genus encalypta;species brevicollis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250064826</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Encalypta</taxon_name>
    <taxon_name authority="Bruch" date="unknown" rank="species">longicollis</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="variety">brevicollis</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>3: 28. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Encalypta;species longicollis;variety brevicollis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Encalypta</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">brevicollis</taxon_name>
    <taxon_name authority="D. G. Horton" date="unknown" rank="subspecies">crumiana</taxon_name>
    <taxon_hierarchy>genus Encalypta;species brevicollis;subspecies crumiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20–25 mm, central strand absent.</text>
      <biological_entity id="o8704" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s0" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o8705" name="strand" name_original="strand" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves oblong to narrowly spathulate, 2–6 mm, apices obtuse or broadly acute, hair-pointed, awn short;</text>
      <biological_entity id="o8706" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s1" to="narrowly spathulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8707" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s1" value="hair-pointed" value_original="hair-pointed" />
      </biological_entity>
      <biological_entity id="o8708" name="awn" name_original="awn" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins plane;</text>
      <biological_entity id="o8709" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa excurrent, awns shorter than leaf lamina;</text>
      <biological_entity id="o8710" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o8711" name="awn" name_original="awns" src="d0_s3" type="structure">
        <character constraint="than leaf lamina" constraintid="o8712" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o8712" name="lamina" name_original="lamina" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>laminal cells 15–20 µm; basal-cells rectangular, 20–90 µm, smooth;</text>
      <biological_entity constraint="laminal" id="o8713" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s4" to="20" to_unit="um" />
      </biological_entity>
      <biological_entity id="o8714" name="basal-bud" name_original="basal-cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s4" to="90" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal marginal cells differentiated, longer than laminal cells, in 8–12 rows.</text>
      <biological_entity constraint="laminal" id="o8716" name="bud" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o8717" name="row" name_original="rows" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <relation from="o8715" id="r2405" name="in" negation="false" src="d0_s5" to="o8717" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction absent.</text>
      <biological_entity constraint="basal marginal" id="o8715" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character constraint="than laminal cells" constraintid="o8716" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta 5–16 mm.</text>
      <biological_entity id="o8718" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule exserted, long-cylindric, 1.5–3.5 mm, smooth;</text>
      <biological_entity id="o8719" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s8" value="long-cylindric" value_original="long-cylindric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>exothecial cells rectangular, walls thickened;</text>
      <biological_entity constraint="exothecial" id="o8720" name="bud" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o8721" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome diplolepideous, not well developed, teeth irregular, narrowly lanceolate, 0.5 mm, papillose or rarely smooth, erect when wet or dry;</text>
      <biological_entity id="o8722" name="peristome" name_original="peristome" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="diplolepideous" value_original="diplolepideous" />
        <character is_modifier="false" modifier="not well" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o8723" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_course" src="d0_s10" value="irregular" value_original="irregular" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="rarely" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="when wet or dry" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>endostome segments attached to exostome teeth, papillose, as long as teeth;</text>
      <biological_entity constraint="endostome" id="o8724" name="segment" name_original="segments" src="d0_s11" type="structure">
        <character constraint="to exostome teeth" constraintid="o8725" is_modifier="false" name="fixation" src="d0_s11" value="attached" value_original="attached" />
        <character is_modifier="false" name="relief" notes="" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o8725" name="tooth" name_original="teeth" src="d0_s11" type="structure" />
      <biological_entity id="o8726" name="tooth" name_original="teeth" src="d0_s11" type="structure" />
      <relation from="o8724" id="r2406" name="as long as" negation="false" src="d0_s11" to="o8726" />
    </statement>
    <statement id="d0_s12">
      <text>operculum 2 mm.</text>
      <biological_entity id="o8727" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Calyptra 4–8 mm, lacerate at base, papillose distally.</text>
      <biological_entity id="o8728" name="calyptra" name_original="calyptra" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character constraint="at base" constraintid="o8729" is_modifier="false" name="shape" src="d0_s13" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="distally" name="relief" notes="" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o8729" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Spores 30–40 µm, warty, brown.</text>
      <biological_entity id="o8730" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s14" to="40" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="warty" value_original="warty" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil in open montane and alpine habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" constraint="in open montane and alpine habitats" />
        <character name="habitat" value="open montane" />
        <character name="habitat" value="alpine habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Oreg., Wash.; n Europe; c, n Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>The long awn on narrowly spathulate leaves and whitish peristome distinguish Encalypta brevicollis. There is some similarity to E. affinis or E. ciliata, but neither of these species displays an awn as long and prominent as that of E. brevicollis, or has the whitish peristome. Subspecies crumiana has been recognized primarily based on poorly developed, smooth peristome teeth, granulate ornamentation on top of ridged or warty spores, and a more gradual change from the calyptra body to the rostrum.</discussion>
  
</bio:treatment>