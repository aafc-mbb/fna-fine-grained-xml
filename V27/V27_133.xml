<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">114</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">tetraphidaceae</taxon_name>
    <taxon_name authority="Schwägrichen" date="1824" rank="genus">tetrodontium</taxon_name>
    <taxon_name authority="(Dickson) Schwägrichen" date="1824" rank="species">brownianum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>2(1,2): 102. 1824,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family tetraphidaceae;genus tetrodontium;species brownianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250064707</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Georgia</taxon_name>
    <taxon_name authority="(Dickson) Müller Hal." date="unknown" rank="species">browniana</taxon_name>
    <taxon_hierarchy>genus Georgia;species browniana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tetraphis</taxon_name>
    <taxon_name authority="(Dickson) Greville" date="unknown" rank="species">browniana</taxon_name>
    <taxon_hierarchy>genus Tetraphis;species browniana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants without flagelliform shoots.</text>
      <biological_entity id="o7736" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7737" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="flagelliform" value_original="flagelliform" />
      </biological_entity>
      <relation from="o7736" id="r2155" name="without" negation="false" src="d0_s0" to="o7737" />
    </statement>
    <statement id="d0_s1">
      <text>Thallose protonematal flaps narrowly linear-lanceolate acute, occasionally with a short mucronate point, margins usually entire, up to 2.5 mm.</text>
      <biological_entity constraint="protonematal" id="o7738" name="flap" name_original="flaps" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="thallose" value_original="thallose" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7739" name="point" name_original="point" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o7740" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o7738" id="r2156" modifier="occasionally" name="with" negation="false" src="d0_s1" to="o7739" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves with costa weak or sometimes absent, present only in distal leaves.</text>
      <biological_entity id="o7741" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character constraint="in distal leaves" constraintid="o7743" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7742" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7743" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o7741" id="r2157" name="with" negation="false" src="d0_s2" to="o7742" />
    </statement>
    <statement id="d0_s3">
      <text>Spores about 10–12 µm.</text>
      <biological_entity id="o7744" name="spore" name_original="spores" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s3" to="12" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Protonematal flaps narrow, linear, acute, to 2.5 mm, usually entire, sometimes with a short mucronate point; stem leaves acuminate, sharply pointed</description>
      <determination>2a Tetrodontium brownianum var. brownianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Protonematal flaps ovate-lingulate, less than 0.5 mm, dentate, with a large acute apical cell, often mixed in with linear, mucronate flaps; stem leaves obtuse</description>
      <determination>2b Tetrodontium brownianum var. ovatum</determination>
    </key_statement>
  </key>
</bio:treatment>