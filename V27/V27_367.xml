<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="treatment_page">263</other_info_on_meta>
    <other_info_on_meta type="illustration_page">262</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Thériot" date="1928" rank="genus">jaffueliobryum</taxon_name>
    <taxon_name authority="(Austin) Thériot" date="1928" rank="species">raui</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol., n. s.</publication_title>
      <place_in_publication>1: 193. 1928,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus jaffueliobryum;species raui</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065115</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Austin" date="unknown" rank="species">raui</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>6: 46. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species raui;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in small dense cushions or turfs, yellow-green to dark olivaceous, hoary.</text>
      <biological_entity id="o4234" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="yellow-green" name="coloration" notes="" src="d0_s0" to="dark olivaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hoary" value_original="hoary" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4235" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o4236" name="turf" name_original="turfs" src="d0_s0" type="structure" />
      <relation from="o4234" id="r966" name="in" negation="false" src="d0_s0" to="o4235" />
      <relation from="o4234" id="r967" name="in" negation="false" src="d0_s0" to="o4236" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–20 mm, sparsely branched.</text>
      <biological_entity id="o4237" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves crowded, ovate to obovate, imbricate to appressed-julaceous distally, somewhat spreading to squarrose-recurved proximally, 0.6–1.2 mm excluding awn, apex acute to acuminate, lamina 1-stratose to rarely 2-stratose in bands, awn length highly variable, 0.3–1.4 mm, hyaline;</text>
      <biological_entity id="o4238" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="obovate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s2" value="appressed-julaceous" value_original="appressed-julaceous" />
        <character char_type="range_value" from="spreading" modifier="somewhat; proximally" name="orientation" src="d0_s2" to="squarrose-recurved" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s2" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4239" name="awn" name_original="awn" src="d0_s2" type="structure" />
      <biological_entity id="o4240" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
      <biological_entity id="o4241" name="lamina" name_original="lamina" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in bands" constraintid="o4242" from="1-stratose" name="architecture" src="d0_s2" to="rarely 2-stratose" />
      </biological_entity>
      <biological_entity id="o4242" name="band" name_original="bands" src="d0_s2" type="structure" />
      <biological_entity id="o4243" name="awn" name_original="awn" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="highly" name="length" src="d0_s2" value="variable" value_original="variable" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s2" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o4238" id="r968" name="excluding" negation="false" src="d0_s2" to="o4239" />
    </statement>
    <statement id="d0_s3">
      <text>costa in transverse-section distinctly keeled;</text>
      <biological_entity id="o4244" name="costa" name_original="costa" src="d0_s3" type="structure" />
      <biological_entity id="o4245" name="transverse-section" name_original="transverse-section" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
      </biological_entity>
      <relation from="o4244" id="r969" name="in" negation="false" src="d0_s3" to="o4245" />
    </statement>
    <statement id="d0_s4">
      <text>proximal cells rectangular, 15–40 × 10–20 µm, often appearing lax;</text>
      <biological_entity constraint="proximal" id="o4246" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s4" to="40" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s4" to="20" to_unit="um" />
        <character is_modifier="false" modifier="often" name="architecture_or_arrangement" src="d0_s4" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid leaf cells isodiametric to short-oval, (5–) 8–12 (–20) µm; distal cells somewhat longer than mid leaf cells.</text>
      <biological_entity constraint="leaf" id="o4247" name="cell" name_original="cells" src="d0_s5" type="structure" constraint_original="mid leaf">
        <character char_type="range_value" from="isodiametric" name="shape" src="d0_s5" to="short-oval" />
        <character char_type="range_value" from="5" from_unit="um" name="atypical_some_measurement" src="d0_s5" to="8" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s5" to="20" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s5" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o4249" name="cell" name_original="cells" src="d0_s5" type="structure" constraint_original="mid leaf" />
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="distal" id="o4248" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character constraint="than mid leaf cells" constraintid="o4249" is_modifier="false" name="length_or_size" src="d0_s5" value="somewhat longer" value_original="somewhat longer" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perichaetial leaf lamina to 1.5 mm, awn 1–1.7 mm.</text>
      <biological_entity constraint="leaf" id="o4250" name="lamina" name_original="lamina" src="d0_s7" type="structure" constraint_original="perichaetial leaf">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4251" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 0.4–0.6 mm.</text>
      <biological_entity id="o4252" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule yellowbrown turning redbrown with age, ovoid to subglobose, 0.8–1 mm;</text>
      <biological_entity id="o4253" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character constraint="with age" constraintid="o4254" is_modifier="false" name="coloration" src="d0_s9" value="yellowbrown turning redbrown" value_original="yellowbrown turning redbrown" />
        <character char_type="range_value" from="ovoid" name="shape" notes="" src="d0_s9" to="subglobose" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4254" name="age" name_original="age" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>operculum short-rostrate, 0.5–0.6 mm.</text>
      <biological_entity id="o4255" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="short-rostrate" value_original="short-rostrate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring–summer depending on elevation.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" modifier="depending on elevati" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Widespread and locally common on dry sandstone or limestone rock, open arid to semi-arid shrub, woodland communities, grasslands, rarely on compacted sandy soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sandstone" modifier="widespread and locally common on" />
        <character name="habitat" value="limestone rock" />
        <character name="habitat" value="semi-arid shrub" />
        <character name="habitat" value="woodland communities" />
        <character name="habitat" value="open arid to semi-arid shrub" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="compacted sandy soil" modifier="rarely on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (200-2100 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="200" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; Ariz., Calif., Colo., Iowa, Kans., Minn., Mont., Nebr., Nev., N.Mex., Okla., S.Dak., Tex., Utah, Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Jaffueliobryum raui is a common species in drier parts of the United States, especially on the Great Plains and the Colorado Plateau, extending to the Mohave Desert of California. This species appears to be disjunct to southern Alberta, but the distribution is likely continuous, as the band of calcareous bedrock on which it occurs runs along the Rocky Mountain Front Range from Alberta well into Montana. Sites in the Driftless Area of Iowa, Minnesota, and Wisconsin may be truly disjunct. However, more collecting is likely to fill in gaps in its range in the northern Great Plains and clarify its status in the Great Basin.</discussion>
  <discussion>Jaffueliobryum wrightii is very closely related, and the two species have been treated as one for much of their taxonomic history. In addition to traits used in the key, there are a variety of other partially overlapping characters that separate the two, including leaf width at widest point, lamina length, costal width, lamina cell length and angle between the margin at the leaf widest point and the tip of the awn, and capsule and operculum length.</discussion>
  
</bio:treatment>