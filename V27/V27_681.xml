<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">469</other_info_on_meta>
    <other_info_on_meta type="illustration_page">469</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="H. Robinson" date="unknown" rank="family">rhachitheciaceae</taxon_name>
    <taxon_name authority="Le Jolis" date="1895" rank="genus">rhachithecium</taxon_name>
    <taxon_name authority="(Thwaites &amp; Mitten) Brotherus in H. G. A. Engler and K. Prantl" date="1909" rank="species">perpusillum</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>234/235[I,3]: 1199. 1909,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhachitheciaceae;genus rhachithecium;species perpusillum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001661</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zygodon</taxon_name>
    <taxon_name authority="Thwaites &amp; Mitten" date="unknown" rank="species">perpusillus</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>13: 303. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Zygodon;species perpusillus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnodon</taxon_name>
    <taxon_name authority="(Thwaites &amp; Mitten) Müller Hal." date="unknown" rank="species">perpusillus</taxon_name>
    <taxon_hierarchy>genus Hypnodon;species perpusillus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="Sharp" date="unknown" rank="species">propagulosa</taxon_name>
    <taxon_hierarchy>genus Tortula;species propagulosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rarely to 4 mm.</text>
      <biological_entity id="o1257" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="4" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems typically unbranched.</text>
      <biological_entity id="o1258" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="typically" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves acute, to 1 × 0.3 mm, margin entire to crenulate;</text>
      <biological_entity id="o1259" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s2" to="1" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s2" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1260" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s2" to="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal cells to 45 × 12 µm; medial and distal cells quadrate to hexagonal, somewhat bulging, 15–25 µm wide, marginal cells often smaller, 6–12 µm; costa between 2/3 and 3/4 of leaf length.</text>
      <biological_entity constraint="proximal" id="o1261" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="um" name="length" src="d0_s3" to="45" to_unit="um" />
        <character char_type="range_value" from="0" from_unit="um" name="width" src="d0_s3" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="medial and distal" id="o1262" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s3" to="hexagonal" />
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_shape" src="d0_s3" value="bulging" value_original="bulging" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s3" to="25" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o1263" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="6" from_unit="um" name="some_measurement" src="d0_s3" to="12" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Specialized asexual reproduction by gemmae, cylindrical to slightly club-shaped, 1–2-seriate, to 160 µm, borne near the base of the leaf.</text>
      <biological_entity id="o1265" name="gemma" name_original="gemmae" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="asexual" value_original="asexual" />
        <character char_type="range_value" from="cylindrical" name="shape" src="d0_s4" to="slightly club-shaped" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="1-2-seriate" value_original="1-2-seriate" />
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" src="d0_s4" to="160" to_unit="um" />
      </biological_entity>
      <biological_entity id="o1266" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o1267" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <relation from="o1265" id="r303" name="borne near the" negation="false" src="d0_s4" to="o1266" />
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition autoicous.</text>
      <biological_entity id="o1264" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" from="2/3" name="length" src="d0_s3" to="3/4" />
        <character is_modifier="false" name="development" src="d0_s4" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sporophytes absent in range of the flora.</text>
      <biological_entity id="o1268" name="sporophyte" name_original="sporophytes" src="d0_s6" type="structure">
        <character constraint="of flora" constraintid="o1269" is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1269" name="flora" name_original="flora" src="d0_s6" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or as on bark of deciduous trees, primarily elm</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bark" modifier="dry or as on" constraint="of deciduous trees" />
        <character name="habitat" value="deciduous trees" />
        <character name="habitat" value="elm" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., Tenn., W.Va.; Mexico; South America (Argentina, Brazil); Asia (China, India, Sri Lanka); Indian Ocean Islands (Madagascar).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="Asia (Sri Lanka)" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>A single population from North Carolina (Davidson, L. E. Anderson 7818, DUKE) was found to produce gametangia. The perichatium is apical and the perigonium terminates a short, subapical branch. Despite the presence of sex organs, no evidence of sexual reproduction has been found. Perichaetial leaves are not differentiated in this specimen, which may suggest that the differentiation (i.e., longer leaves) occurs following sexual reproduction. The species has only been collected once in recent years (Tennessee, J. A. Churchill 89025, 1989, DUKE). Most collections were made on elm, which has been much decimated in eastern North America. The species is distinguished by its small stature, crisped leaves when dry, thin-walled smooth cells, and abundant laminal gemmae.</discussion>
  
</bio:treatment>