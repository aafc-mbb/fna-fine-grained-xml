<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="treatment_page">92</other_info_on_meta>
    <other_info_on_meta type="illustration_page">93</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Wilson" date="1855" rank="section">acutifolia</taxon_name>
    <taxon_name authority="Wilson &amp; Hooker in J. D. Hooker" date="1847" rank="species">fimbriatum</taxon_name>
    <place_of_publication>
      <publication_title>in J. D. Hooker, Fl. Antarct.,</publication_title>
      <place_in_publication>398. 1847,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section acutifolia;species fimbriatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443308</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants typically small and slender, larger and compact in the Arctic, capitulum small to moderate-sized, often with a conspicuous terminal bud;</text>
      <biological_entity id="o178" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="typically" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s0" value="larger" value_original="larger" />
        <character constraint="in the arctic" is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o180" name="bud" name_original="bud" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s0" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o179" id="r61" modifier="often" name="with" negation="false" src="d0_s0" to="o180" />
    </statement>
    <statement id="d0_s1">
      <text>green, yellowish-brown to brown;</text>
    </statement>
    <statement id="d0_s2">
      <text>without metallic lustre when dry.</text>
      <biological_entity id="o179" name="capitulum" name_original="capitulum" src="d0_s0" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="moderate-sized" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="yellowish-brown" name="coloration" src="d0_s1" to="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems pale green to straw-colored;</text>
    </statement>
    <statement id="d0_s4">
      <text>superficial cortical with a large round pore in distal portion of cell free from cell wall.</text>
      <biological_entity id="o181" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s3" to="straw-colored" />
        <character is_modifier="false" name="position" src="d0_s4" value="superficial" value_original="superficial" />
      </biological_entity>
      <biological_entity id="o182" name="pore" name_original="pore" src="d0_s4" type="structure">
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s4" value="cortical" value_original="cortical" />
        <character is_modifier="true" name="size" src="d0_s4" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s4" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="distal" id="o183" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character constraint="from cell wall" constraintid="o185" is_modifier="false" name="fusion" src="d0_s4" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o184" name="cell" name_original="cell" src="d0_s4" type="structure" />
      <biological_entity constraint="cell" id="o185" name="wall" name_original="wall" src="d0_s4" type="structure" />
      <relation from="o182" id="r62" name="in" negation="false" src="d0_s4" to="o183" />
      <relation from="o183" id="r63" name="part_of" negation="false" src="d0_s4" to="o184" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves spatulate to broad-spatulate, 0.8–1.5 (–2) mm, strongly lacerate across the broad apex and often part-way down the margins, border scarcely to strongly broadened at base (0.25 width of base or less);</text>
      <biological_entity id="o186" name="leaf-stem" name_original="stem-leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="broad-spatulate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s5" to="1.5" to_unit="mm" />
        <character constraint="across apex" constraintid="o187" is_modifier="false" modifier="strongly" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
        <character constraint="down margins" constraintid="o188" is_modifier="false" modifier="often" name="dehiscence" notes="" src="d0_s5" value="part-way" value_original="part-way" />
      </biological_entity>
      <biological_entity id="o187" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o188" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o189" name="border" name_original="border" src="d0_s5" type="structure">
        <character constraint="at base" constraintid="o190" is_modifier="false" modifier="scarcely to strongly" name="width" src="d0_s5" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o190" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>hyaline cells rhomboid, efibrillose and often 1–2-septate.</text>
      <biological_entity id="o191" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s6" value="1-2-septate" value_original="1-2-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Branches not 5-ranked, quite terete, long, and slender Branch fascicles with 1– 2 spreading and 1–2 pendent branches.</text>
      <biological_entity id="o192" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="5-ranked" value_original="5-ranked" />
        <character is_modifier="false" modifier="quite" name="shape" src="d0_s7" value="terete" value_original="terete" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="long" value_original="long" />
      </biological_entity>
      <biological_entity constraint="slender" id="o193" name="branch" name_original="branch" src="d0_s7" type="structure">
        <character constraint="with branches" constraintid="o194" is_modifier="false" name="arrangement" src="d0_s7" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o194" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
        <character is_modifier="true" name="orientation" src="d0_s7" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch leaves ovate to ovatelanceolate;</text>
    </statement>
    <statement id="d0_s9">
      <text>1.1–1.5 (–2) mm, slightly concave, straight;</text>
      <biological_entity constraint="branch" id="o195" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="ovatelanceolate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="concave" value_original="concave" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>apex involute;</text>
      <biological_entity id="o196" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s10" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>margins entire;</text>
      <biological_entity id="o197" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hyaline cells on convex surface with numerous pores along the commissures grading from small pores near leaf apex to large pores at base, concave surface with large round pores at leaf apex and along margins.</text>
      <biological_entity id="o198" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o199" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o200" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o201" name="commissure" name_original="commissures" src="d0_s12" type="structure" />
      <biological_entity id="o202" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="small" value_original="small" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o203" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o204" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o205" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o207" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s12" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o208" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o209" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <relation from="o198" id="r64" name="on" negation="false" src="d0_s12" to="o199" />
      <relation from="o199" id="r65" name="with" negation="false" src="d0_s12" to="o200" />
      <relation from="o200" id="r66" name="along" negation="false" src="d0_s12" to="o201" />
      <relation from="o201" id="r67" name="from" negation="false" src="d0_s12" to="o202" />
      <relation from="o202" id="r68" name="near" negation="false" src="d0_s12" to="o203" />
      <relation from="o203" id="r69" name="to" negation="false" src="d0_s12" to="o204" />
      <relation from="o204" id="r70" name="at" negation="false" src="d0_s12" to="o205" />
      <relation from="o206" id="r71" name="with" negation="false" src="d0_s12" to="o207" />
      <relation from="o207" id="r72" name="at" negation="false" src="d0_s12" to="o208" />
      <relation from="o206" id="r73" name="along" negation="false" src="d0_s12" to="o209" />
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition often monoicous.</text>
      <biological_entity id="o206" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="concave" value_original="concave" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" modifier="often" name="reproduction" src="d0_s13" value="monoicous" value_original="monoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 20–27 µm, finely papillose on both surfaces;</text>
      <biological_entity id="o210" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s14" to="27" to_unit="um" />
        <character constraint="on surfaces" constraintid="o211" is_modifier="false" modifier="finely" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o211" name="surface" name_original="surfaces" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>proximal laesura less than 0.5 spore radius.</text>
      <biological_entity constraint="proximal" id="o212" name="laesurum" name_original="laesura" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s15" to="0.5" />
      </biological_entity>
      <biological_entity id="o213" name="spore" name_original="spore" src="d0_s15" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Eurasia, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>72.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants small and slender, capitulum small and with a conspicuous terminal bud; stem leaves fimbriate down the sides and weakly to moderately bordered at the base</description>
      <determination>72a Sphagnum fimbriatum subsp. fimbriatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants moderate-sized and compact, capitulum moderate sized and lacking a conspicuous terminal bud; stem leaves entire down the sides and strongly bordered at the base</description>
      <determination>72b Sphagnum fimbriatum subsp. concinnum</determination>
    </key_statement>
  </key>
</bio:treatment>