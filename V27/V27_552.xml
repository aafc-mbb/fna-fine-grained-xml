<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
    <other_info_on_meta type="illustration_page">388</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="(Müller Hal.) Schimper" date="1856" rank="genus">dicranella</taxon_name>
    <taxon_name authority="W. B. Schofield" date="1970" rank="species">pacifica</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>73: 703. 1970,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranella;species pacifica</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443615</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2.5–4 cm, yellowish, brownish, or dark green.</text>
      <biological_entity id="o7358" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark green" value_original="dark green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves erect-spreading and flexuose-crisped when dry, 2.5–4 mm, gradually narrowed from an ovate base to a long, slender, subtubulose subula;</text>
      <biological_entity id="o7359" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s1" value="flexuose-crisped" value_original="flexuose-crisped" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
        <character constraint="from base" constraintid="o7360" is_modifier="false" modifier="gradually" name="shape" src="d0_s1" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o7360" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o7361" name="subulum" name_original="subula" src="d0_s1" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="true" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
      <relation from="o7360" id="r1757" name="to" negation="false" src="d0_s1" to="o7361" />
    </statement>
    <statement id="d0_s2">
      <text>margins distinctly recurved below, erect or incurved above, serrulate at or near the slender apex;</text>
      <biological_entity id="o7362" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distinctly; below" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
        <character constraint="at or near slender apex" constraintid="o7363" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="slender" id="o7363" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent, filling most of the subula;</text>
      <biological_entity id="o7364" name="costa" name_original="costa" src="d0_s3" type="structure" constraint="subulum; subulum">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o7365" name="subulum" name_original="subula" src="d0_s3" type="structure" />
      <relation from="o7364" id="r1758" name="part_of" negation="false" src="d0_s3" to="o7365" />
    </statement>
    <statement id="d0_s4">
      <text>distal cells subquadrate, 1–2: 1 (ca. 17 × 10 µm), 2-stratose at the margins.</text>
      <biological_entity id="o7367" name="margin" name_original="margins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="distal" id="o7366" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="at margins" constraintid="o7367" is_modifier="false" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seta red, 5–8 mm.</text>
      <biological_entity id="o7368" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="red" value_original="red" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule inclined to horizontal, 0.8 mm, obovoid-oblong, curved, not strumose, smooth;</text>
      <biological_entity id="o7369" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s7" to="horizontal" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="0.8" value_original="0.8" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovoid-oblong" value_original="obovoid-oblong" />
        <character is_modifier="false" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="strumose" value_original="strumose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>annulus none;</text>
    </statement>
    <statement id="d0_s9">
      <text>operculum-conic;</text>
      <biological_entity id="o7370" name="annulus" name_original="annulus" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="0" value_original="none" />
        <character is_modifier="false" name="shape" src="d0_s9" value="operculum-conic" value_original="operculum-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome teeth ca. 425 µm, divided 1/2 length distally.</text>
      <biological_entity constraint="peristome" id="o7371" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="um" value="425" value_original="425" />
        <character is_modifier="false" name="shape" src="d0_s10" value="divided" value_original="divided" />
        <character modifier="distally" name="length" src="d0_s10" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores 15–17 µm, smooth.</text>
      <biological_entity id="o7372" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s11" to="17" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature fall and winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="winter" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, silty soil of roadside ditch banks and soil of cliff crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="silty soil" modifier="wet" constraint="of roadside ditch banks" />
        <character name="habitat" value="roadside ditch banks" />
        <character name="habitat" value="soil" constraint="of cliff crevices" />
        <character name="habitat" value="cliff crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to medium elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Distinctive features of Dicranella pacifica include slender, flexuose-crisped, subtubulose leaves with margins recurved below but 2-stratose and incurved above, high-conic opercula, and annulus absent.</discussion>
  
</bio:treatment>