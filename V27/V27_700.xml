<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard H. Zander</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">484</other_info_on_meta>
    <other_info_on_meta type="mention_page">640</other_info_on_meta>
    <other_info_on_meta type="treatment_page">483</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">merceyoideae</taxon_name>
    <taxon_name authority="(Mitten) Lindberg" date="1872" rank="genus">SCOPELOPHILA</taxon_name>
    <place_of_publication>
      <publication_title>Acta Soc. Sci. Fenn.</publication_title>
      <place_in_publication>10: 269. 1872  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily merceyoideae;genus SCOPELOPHILA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek skopelos, crag, and philia, fondness, alluding to characteristic rocky habitat</other_info_on_name>
    <other_info_on_name type="fna_id">129864</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Schimper" date="unknown" rank="genus">Weissia</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="section">Scopelophila</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>12: 135. 1869</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Weissia;section Scopelophila;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Merceya</taxon_name>
    <taxon_hierarchy>genus Merceya;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming a thin or thick turf, green to greenish yellow or brown distally, blackish or weakly iridescent to metallic tan or yellowbrown proximally.</text>
      <biological_entity id="o7186" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" modifier="proximally" name="coloration" src="d0_s0" to="greenish yellow or brown distally" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7187" name="turf" name_original="turf" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o7186" id="r1718" name="forming a" negation="false" src="d0_s0" to="o7187" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 4 cm, rounded-pentagonal in transverse-section, hyalodermis absent, sclerodermis absent, central strand absent;</text>
      <biological_entity id="o7188" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character constraint="in transverse-section" constraintid="o7189" is_modifier="false" name="shape" src="d0_s1" value="rounded-pentagonal" value_original="rounded-pentagonal" />
      </biological_entity>
      <biological_entity id="o7189" name="transverse-section" name_original="transverse-section" src="d0_s1" type="structure" />
      <biological_entity id="o7190" name="hyalodermi" name_original="hyalodermis" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7191" name="scleroderm" name_original="sclerodermis" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o7192" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hairs 3–5 cells in length, proximal cell usually brownish.</text>
      <biological_entity constraint="axillary" id="o7193" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <biological_entity id="o7194" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o7195" name="cell" name_original="cell" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves often crowded, incurved to spreading, contorted, carinate, occasionally with undulating margins when dry, spreading when moist;</text>
      <biological_entity id="o7197" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o7196" id="r1719" modifier="occasionally" name="with" negation="false" src="d0_s3" to="o7197" />
    </statement>
    <statement id="d0_s4">
      <text>lingulate to ligulate or oblanceolate, widest at mid leaf or beyond, adaxial surface narrowly and deeply grooved along costa or broadly channeled, to 2.5 mm;</text>
      <biological_entity constraint="cauline" id="o7196" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="incurved" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="carinate" value_original="carinate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lingulate" value_original="lingulate" />
      </biological_entity>
      <biological_entity constraint="mid" id="o7198" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character constraint="at " constraintid="o7199" is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7199" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="along costa" constraintid="o7200" is_modifier="false" modifier="deeply" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o7200" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>base scarcely differentiated in shape to long-elliptic, occasionally medially constricted, proximal margins occasionally slightly decurrent;</text>
      <biological_entity id="o7201" name="base" name_original="base" src="d0_s5" type="structure">
        <character constraint="in shape to long-elliptic" is_modifier="false" modifier="scarcely" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="occasionally medially" name="size" src="d0_s5" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7202" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="occasionally slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal margins plane or somewhat recurved proximally, entire to minutely crenulate or denticulate distally, often bordered by a few rows of thicker walled cells distally;</text>
      <biological_entity constraint="distal" id="o7203" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="somewhat; proximally" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="entire" modifier="distally" name="shape" src="d0_s6" to="minutely crenulate or denticulate" />
        <character constraint="by rows" constraintid="o7204" is_modifier="false" modifier="often" name="architecture" src="d0_s6" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o7204" name="row" name_original="rows" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="thicker" id="o7205" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="walled" value_original="walled" />
      </biological_entity>
      <relation from="o7204" id="r1720" name="part_of" negation="false" src="d0_s6" to="o7205" />
    </statement>
    <statement id="d0_s7">
      <text>apex broadly acute or obtuse, often with a broad apiculus, occasionally rounded;</text>
      <biological_entity id="o7206" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="occasionally" name="shape" notes="" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7207" name="apiculu" name_original="apiculus" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o7206" id="r1721" modifier="often" name="with" negation="false" src="d0_s7" to="o7207" />
    </statement>
    <statement id="d0_s8">
      <text>costa slender, percurrent or ending 2–8 cells before the apex, occasionally excurrent as a short mucro, adaxial outgrowths absent, adaxial cells quadrate to rectangular, in 2–4 rows;</text>
      <biological_entity id="o7208" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="percurrent" value_original="percurrent" />
        <character name="architecture" src="d0_s8" value="ending 2-8 cells" value_original="ending 2-8 cells" />
        <character constraint="as mucro" constraintid="o7210" is_modifier="false" modifier="occasionally" name="architecture" notes="" src="d0_s8" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o7209" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o7210" name="mucro" name_original="mucro" src="d0_s8" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7211" name="outgrowth" name_original="outgrowths" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7212" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s8" to="rectangular" />
      </biological_entity>
      <biological_entity id="o7213" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o7208" id="r1722" name="before" negation="false" src="d0_s8" to="o7209" />
      <relation from="o7212" id="r1723" name="in" negation="false" src="d0_s8" to="o7213" />
    </statement>
    <statement id="d0_s9">
      <text>transverse-section semicircular to round, adaxial epidermis absent or present, adaxial stereid band absent, guide cells 2 (–4) in 1 layer, hydroid strand absent, abaxial stereid band present, rounded in sectional shape, abaxial epidermis present, usually very distinct;</text>
      <biological_entity id="o7214" name="transverse-section" name_original="transverse-section" src="d0_s9" type="structure">
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s9" to="round" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7215" name="epidermis" name_original="epidermis" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7216" name="epidermis" name_original="epidermis" src="d0_s9" type="structure" />
      <biological_entity id="o7217" name="band" name_original="band" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="guide" id="o7218" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="4" />
        <character constraint="in layer" constraintid="o7219" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7219" name="layer" name_original="layer" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="hydroid" id="o7220" name="strand" name_original="strand" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7221" name="epidermis" name_original="epidermis" src="d0_s9" type="structure" />
      <biological_entity id="o7222" name="band" name_original="band" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character constraint="in sectional" constraintid="o7223" is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7223" name="sectional" name_original="sectional" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial" id="o7224" name="epidermis" name_original="epidermis" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="present" value_original="present" />
        <character is_modifier="false" modifier="usually very" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal cells differentiated across leaf, extending higher medially, rectangular, occasionally inflated, scarcely wider than distal cells to inflated, 2–3: 1, walls of proximal cells hyaline or deep brown;</text>
      <biological_entity constraint="proximal" id="o7225" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="across leaf" constraintid="o7226" is_modifier="false" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="medially" name="position" notes="" src="d0_s10" value="higher" value_original="higher" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character constraint="than distal cells" constraintid="o7227" is_modifier="false" name="width" src="d0_s10" value="scarcely wider" value_original="scarcely wider" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7226" name="leaf" name_original="leaf" src="d0_s10" type="structure" />
      <biological_entity constraint="distal" id="o7227" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity id="o7228" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7229" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <relation from="o7228" id="r1724" name="part_of" negation="false" src="d0_s10" to="o7229" />
    </statement>
    <statement id="d0_s11">
      <text>distal medial cells rounded-quadrate to hexagonal or shortrectangular, ca. 8–14 µm, often heterogeneous in size, 1 (–2):1, 1-stratose;</text>
      <biological_entity constraint="distal medial" id="o7230" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="rounded-quadrate" name="shape" src="d0_s11" to="hexagonal" />
        <character name="shape" src="d0_s11" value="shortrectangular" value_original="shortrectangular" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s11" to="14" to_unit="um" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s11" value="heterogeneous" value_original="heterogeneous" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="2" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>papillae usually lacking, occasionally low-verrucose, cell-walls thin to evenly thickened, thicker near margins, flat or somewhat bulging on adaxial surface.</text>
      <biological_entity constraint="distal medial" id="o7231" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="rounded-quadrate" name="shape" src="d0_s12" to="hexagonal" />
        <character name="shape" src="d0_s12" value="shortrectangular" value_original="shortrectangular" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s12" to="14" to_unit="um" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s12" value="heterogeneous" value_original="heterogeneous" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="2" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7232" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s12" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="occasionally" name="relief" src="d0_s12" value="low-verrucose" value_original="low-verrucose" />
      </biological_entity>
      <biological_entity id="o7234" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity constraint="adaxial" id="o7235" name="surface" name_original="surface" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Specialized asexual reproduction rare, on stalks from the stem, greenish brown, clavate to ellipsoid or filamentous and branching, ca. 12 µm, of 2 or more rounded cells.</text>
      <biological_entity id="o7236" name="stalk" name_original="stalks" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s13" value="rare" value_original="rare" />
      </biological_entity>
      <biological_entity id="o7237" name="stem" name_original="stem" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish brown" value_original="greenish brown" />
        <character char_type="range_value" from="clavate" name="shape" src="d0_s13" to="ellipsoid" />
      </biological_entity>
      <biological_entity id="o7238" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="branching" value_original="branching" />
        <character name="some_measurement" src="d0_s13" unit="um" value="12" value_original="12" />
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <relation from="o7236" id="r1725" name="from" negation="false" src="d0_s13" to="o7237" />
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o7233" name="cell-wall" name_original="cell-walls" src="d0_s12" type="structure">
        <character char_type="range_value" from="thin" name="width" src="d0_s12" to="evenly thickened" />
        <character constraint="near margins" constraintid="o7234" is_modifier="false" name="width" src="d0_s12" value="thicker" value_original="thicker" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s12" value="flat" value_original="flat" />
        <character constraint="on adaxial surface" constraintid="o7235" is_modifier="false" modifier="somewhat" name="pubescence_or_shape" src="d0_s12" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="development" src="d0_s13" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Perichaetia terminal, interior leaves little differentiated.</text>
      <biological_entity id="o7239" name="perichaetium" name_original="perichaetia" src="d0_s15" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s15" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>[Seta 2–6 mm. Capsule stegocarpous, theca shortly elliptic to cylindric, macrostomous, 0.6–2.2 mm, annulus weakly differentiated or of 1–4 rows of vesiculose cells, deciduous in fragments; operculum conic-rostrate, erect or oblique; peristome teeth absent. Calyptra cucullate. Spores 8–13 µm.] KOH distal laminal color reaction yellow to yellowish orange.</text>
      <biological_entity constraint="interior" id="o7240" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" name="variability" src="d0_s15" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="position_or_shape" src="d0_s16" value="distal" value_original="distal" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="laminal" value_original="laminal" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s16" to="yellowish orange" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Europe, Asia, c Africa, Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="c Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Scopelophila is a small genus (R. H. Zander 1993) of “copper mosses” (see reviews by A. J. Shaw and L. E. Anderson 1988; Shaw 1993) associated with mineralized soils. Crumia is similar but is quickly distinguished by its stem central strand, intramarginal laminal border, and distal laminal cells usually distinctly papillose.</discussion>
  <references>
    <reference>Zander, R. H. 1967. The New World distribution of Scopelophila (= Merceya). Bryologist 70: 405–413.</reference>
    <reference>Zander, R. H. 1994c. Scopelophila. In: A. J. Sharp et al., eds. The moss flora of Mexico. Mem. New York Bot. Gard. 69: 372–375.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems often red-tomentose; proximal leaves brown; leaves acute or short-acuminate, usually not bordered; costa with 2 layers of parenchymatous cells adaxial to the stereid band.</description>
      <determination>1 Scopelophila cataractae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems with sparse brownish rhizoids; proximal leaves brownish black; leaves obtuse to acute, often apiculate by a single cell, usually bordered by thick-walled cells (at least in older leaves); costa with 1 layer of parenchymatous cells adaxial to the stereids.</description>
      <determination>2 Scopelophila ligulata</determination>
    </key_statement>
  </key>
</bio:treatment>