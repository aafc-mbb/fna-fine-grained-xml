<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">214</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="treatment_page">221</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="(Lawton) Churchill" date="1981" rank="species">occidentale</taxon_name>
    <place_of_publication>
      <publication_title>Advances Cladist.</publication_title>
      <place_in_publication>1: 143. 1981,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species occidentale</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075427</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Lawton" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>94: 461, figs. 1–15. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in open to occasionally compact tufts or mats, olivaceous, often (red-) brown or nearly black.</text>
      <biological_entity id="o3433" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3434" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character char_type="range_value" from="open" is_modifier="true" name="architecture" src="d0_s0" to="occasionally compact" />
      </biological_entity>
      <biological_entity id="o3435" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o3433" id="r933" name="in" negation="false" src="d0_s0" to="o3434" />
      <relation from="o3433" id="r934" name="in" negation="false" src="d0_s0" to="o3435" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–6 cm, sometimes denuded of leaves at base, central strand absent.</text>
      <biological_entity id="o3436" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character constraint="of leaves" constraintid="o3437" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="denuded" value_original="denuded" />
      </biological_entity>
      <biological_entity id="o3437" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o3438" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity constraint="central" id="o3439" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o3437" id="r935" name="at" negation="false" src="d0_s1" to="o3438" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually curved to falcate, sometimes falcate-secund or erect when dry, linear-lanceolate to ovatelanceolate, concave proximally, weakly keeled or concave distally, 1.4–4 (–6) mm, 1-stratose with a few 2-stratose striae or patches distally;</text>
      <biological_entity id="o3440" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="ovatelanceolate concave proximally" />
        <character char_type="range_value" from="linear-lanceolate" modifier="distally" name="shape" src="d0_s2" to="ovatelanceolate concave proximally" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character constraint="with patches" constraintid="o3442" is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o3441" name="stria" name_original="striae" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o3442" name="patch" name_original="patches" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins plane or somewhat erect, rarely weakly recurved, smooth, 2-stratose;</text>
      <biological_entity id="o3443" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely weakly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apices rounded or acute, usually ending in a fleshy, multistratose apiculus;</text>
      <biological_entity id="o3444" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o3445" name="apiculu" name_original="apiculus" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="multistratose" value_original="multistratose" />
      </biological_entity>
      <relation from="o3444" id="r936" modifier="usually" name="ending in" negation="false" src="d0_s4" to="o3445" />
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent, rarely short-excurrent, smooth;</text>
      <biological_entity id="o3446" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s5" value="short-excurrent" value_original="short-excurrent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate or short-rectangular;</text>
      <biological_entity constraint="basal marginal" id="o3447" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells quadrate or rounded, often angular, 6–11 µm wide, smooth, sometimes weakly bulging-mammillose, weakly sinuose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal" id="o3448" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="often" name="arrangement_or_shape" src="d0_s7" value="angular" value_original="angular" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s7" to="11" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes weakly" name="relief" src="d0_s7" value="bulging-mammillose" value_original="bulging-mammillose" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule reddish-brown, ovoid, cupulate, or short-cylindric, 1.3–2 mm;</text>
      <biological_entity id="o3449" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells mostly short-elongate or isodiametric, usually irregularly angular, usually thin-walled, sometimes trigonous;</text>
      <biological_entity constraint="exothecial" id="o3450" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s10" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" modifier="usually irregularly" name="arrangement_or_shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rim darker colored than capsule wall, often red;</text>
      <biological_entity id="o3451" name="rim" name_original="rim" src="d0_s11" type="structure">
        <character constraint="than capsule wall" constraintid="o3452" is_modifier="false" name="coloration" src="d0_s11" value="darker colored" value_original="darker colored" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="capsule" id="o3452" name="wall" name_original="wall" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stomata absent;</text>
      <biological_entity id="o3453" name="stoma" name_original="stomata" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome patent, 350–550 µm, red or orange-red, finely papillose, entire or perforated.</text>
      <biological_entity id="o3454" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="patent" value_original="patent" />
        <character char_type="range_value" from="350" from_unit="um" name="some_measurement" src="d0_s13" to="550" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 9–12 µm, smooth.</text>
      <biological_entity id="o3455" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s14" to="12" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or dry rocks, often along intermittent watercourses</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="dry rocks" />
        <character name="habitat" value="intermittent watercourses" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations (2000-3500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2000" from_unit="m" constraint="high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Colo., Idaho, Mont., Oreg., Nev., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <discussion>See comments under 6. Schistidium cinclidodonteum.</discussion>
  
</bio:treatment>