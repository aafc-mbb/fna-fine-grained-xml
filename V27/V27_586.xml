<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">400</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="treatment_page">413</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">dicranum</taxon_name>
    <taxon_name authority="(J. W. Bailey) Ireland" date="1965" rank="species">pallidisetum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>68: 446. 1965,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranum;species pallidisetum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443683</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="forma pallidiseta J. W. Bailey" date="unknown" rank="species">fuscescens</taxon_name>
    <place_of_publication>
      <publication_title>in J. M. Holzinger, Musci Acroc. Bor.-Amer., no.</publication_title>
      <place_in_publication>653. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species fuscescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose tufts, dark green to yellowish green, dull to glossy.</text>
      <biological_entity id="o6106" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" notes="" src="d0_s0" to="yellowish green" />
        <character char_type="range_value" from="dull" name="reflectance" src="d0_s0" to="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6107" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o6106" id="r1459" name="in" negation="false" src="d0_s0" to="o6107" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–6 cm, tomentose with white or reddish-brown rhizoids.</text>
      <biological_entity id="o6108" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character constraint="with rhizoids" constraintid="o6109" is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o6109" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves often secund, straight or nearly so, often crisped when dry, smooth, 4–8 × 0.5–1 mm, lanceolate, acute, tubulose to apex;</text>
      <biological_entity id="o6110" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character name="course" src="d0_s2" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character constraint="to apex" constraintid="o6111" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="tubulose" value_original="tubulose" />
      </biological_entity>
      <biological_entity id="o6111" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins entire below, serrulate ot serrate near apex;</text>
      <biological_entity id="o6112" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="below" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
        <character constraint="near apex" constraintid="o6113" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o6113" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>laminae 1-stratose or sometimes 2-stratose on distal margins;</text>
      <biological_entity id="o6114" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-stratose" value_original="1-stratose" />
        <character constraint="on distal margins" constraintid="o6115" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6115" name="margin" name_original="margins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa excurrent, 1/5–1/3 the width of the leaves at base, abaxially papillose or toothed from leaf middle to apex, abaxial ridges absent, with a row of guide cells, stereid bands often present and well-developed in proximal part of leaf, absent in distal 1/4–1/3 of leaf, cells above and below guide cells large and thin-walled;</text>
      <biological_entity id="o6116" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s5" to="1/3" />
        <character is_modifier="false" modifier="abaxially" name="width" notes="" src="d0_s5" value="papillose" value_original="papillose" />
        <character constraint="from leaf" constraintid="o6119" is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o6117" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o6118" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o6119" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character constraint="to apex" constraintid="o6120" is_modifier="false" name="position" src="d0_s5" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o6120" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o6121" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6122" name="row" name_original="row" src="d0_s5" type="structure" />
      <biological_entity constraint="guide" id="o6123" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o6124" name="band" name_original="bands" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character constraint="in proximal part" constraintid="o6125" is_modifier="false" name="development" src="d0_s5" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6125" name="part" name_original="part" src="d0_s5" type="structure">
        <character constraint="in distal 1/4-1/3" constraintid="o6127" is_modifier="false" name="presence" notes="" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6126" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o6127" name="1/4-1/3" name_original="1/4-1/3" src="d0_s5" type="structure" />
      <biological_entity id="o6128" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o6129" name="cell" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity constraint="guide" id="o6130" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="large" value_original="large" />
      </biological_entity>
      <relation from="o6117" id="r1460" name="at" negation="false" src="d0_s5" to="o6118" />
      <relation from="o6121" id="r1461" name="with" negation="false" src="d0_s5" to="o6122" />
      <relation from="o6122" id="r1462" name="part_of" negation="false" src="d0_s5" to="o6123" />
      <relation from="o6125" id="r1463" name="part_of" negation="false" src="d0_s5" to="o6126" />
      <relation from="o6127" id="r1464" name="part_of" negation="false" src="d0_s5" to="o6128" />
      <relation from="o6129" id="r1465" modifier="above and below guide cells large and thin-walled" name="above and below" negation="false" src="d0_s5" to="o6130" />
    </statement>
    <statement id="d0_s6">
      <text>cell-walls between lamina cells not or slightly bulging;</text>
      <biological_entity constraint="between cells" constraintid="o6132" id="o6131" name="cell-wall" name_original="cell-walls" src="d0_s6" type="structure" constraint_original="between  cells, ">
        <character is_modifier="false" modifier="slightly" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity constraint="lamina" id="o6132" name="cell" name_original="cells" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>leaf cells smooth or often strongly papillose in distal half of leaf;</text>
      <biological_entity constraint="leaf" id="o6134" name="cell" name_original="cells" src="d0_s7" type="structure" constraint_original="distal leaf" />
      <biological_entity id="o6135" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
      <relation from="o6134" id="r1466" name="part_of" negation="false" src="d0_s7" to="o6135" />
    </statement>
    <statement id="d0_s8">
      <text>alar cells 2-stratose, differentiated, often extending to costa, abaxial ridges absent;</text>
      <biological_entity constraint="leaf" id="o6133" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character constraint="in distal leaf cells" constraintid="o6134" is_modifier="false" modifier="often strongly" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o6136" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o6137" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o6138" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o6136" id="r1467" modifier="often" name="extending to" negation="false" src="d0_s8" to="o6137" />
    </statement>
    <statement id="d0_s9">
      <text>proximal laminal cells linear to rectangular, with or without pits, (26–) 58–95 (–130) × (10–) 12–14 (–16) µm; distal laminal cells quadrate, rounded, irregularly angled or short-rectangular, not pitted, (10–) 15–26 (–38) × (8–) 9–11 (–12) µm. Sexual condition dioicous;</text>
      <biological_entity constraint="proximal laminal" id="o6139" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="rectangular" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o6140" name="pit" name_original="pits" src="d0_s9" type="structure">
        <character char_type="range_value" from="26" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="58" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="95" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="130" to_unit="um" />
        <character char_type="range_value" from="58" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="95" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="16" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="14" to_unit="um" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="relief" notes="alterIDs:o6140 o6141" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="15" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="38" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="26" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="9" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="11" to_unit="um" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="dioicous" value_original="dioicous" />
        <character char_type="range_value" from="26" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="58" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="95" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="130" to_unit="um" />
        <character char_type="range_value" from="58" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="95" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="16" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="14" to_unit="um" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="relief" notes="alterIDs:o6140 o6141" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="15" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="38" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="26" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="9" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="11" to_unit="um" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="dioicous" value_original="dioicous" />
        <character char_type="range_value" from="26" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="58" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="95" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="130" to_unit="um" />
        <character char_type="range_value" from="58" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="95" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="16" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="14" to_unit="um" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="relief" notes="alterIDs:o6140 o6141" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="15" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="38" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="26" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="9" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="11" to_unit="um" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
      <biological_entity id="o6141" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="26" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="58" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="95" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="130" to_unit="um" />
        <character char_type="range_value" from="58" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="95" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="16" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="14" to_unit="um" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="relief" notes="alterIDs:o6140 o6141" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="15" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="38" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="26" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="9" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="11" to_unit="um" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="dioicous" value_original="dioicous" />
        <character char_type="range_value" from="26" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="58" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="95" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="130" to_unit="um" />
        <character char_type="range_value" from="58" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="95" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="16" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="14" to_unit="um" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" notes="alterIDs:o6140 o6141" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="relief" notes="alterIDs:o6140 o6141" src="d0_s9" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="15" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o6140 o6141" src="d0_s9" to="38" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="length" notes="alterIDs:o6140 o6141" src="d0_s9" to="26" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="9" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o6140 o6141" src="d0_s9" to="12" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" notes="alterIDs:o6140 o6141" src="d0_s9" to="11" to_unit="um" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" notes="alterIDs:o6140 o6141" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6142" name="pit" name_original="pits" src="d0_s9" type="structure" />
      <biological_entity constraint="laminal" id="o6143" name="pit" name_original="pits" src="d0_s9" type="structure" />
      <relation from="o6139" id="r1468" name="with or without" negation="false" src="d0_s9" to="o6140" />
      <relation from="o6139" id="r1469" name="with or without" negation="false" src="d0_s9" to="o6141" />
    </statement>
    <statement id="d0_s10">
      <text>male plants as large as female plants;</text>
      <biological_entity id="o6144" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="male" value_original="male" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6145" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="female" value_original="female" />
      </biological_entity>
      <relation from="o6144" id="r1470" name="as large as" negation="false" src="d0_s10" to="o6145" />
    </statement>
    <statement id="d0_s11">
      <text>interior perichaetial leaves abruptly short-acuminate, convolute-sheathing.</text>
      <biological_entity constraint="interior perichaetial" id="o6146" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s11" value="short-acuminate" value_original="short-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="convolute-sheathing" value_original="convolute-sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 1–2.5 cm, solitary or sometimes 2, rarely 3, per perichaetium, yellow, rarely reddish yellow or brown with age.</text>
      <biological_entity id="o6147" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s12" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s12" value="sometimes" value_original="sometimes" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character modifier="rarely" name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="reddish yellow" value_original="reddish yellow" />
        <character constraint="with age" constraintid="o6149" is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o6148" name="perichaetium" name_original="perichaetium" src="d0_s12" type="structure" />
      <biological_entity id="o6149" name="age" name_original="age" src="d0_s12" type="structure" />
      <relation from="o6147" id="r1471" name="per" negation="false" src="d0_s12" to="o6148" />
    </statement>
    <statement id="d0_s13">
      <text>Capsule 1.5–3.5 mm, arcuate, inclined, often with a small struma, strongly furrowed when dry, contracted below mouth, yellow or yellowish-brown, rarely reddish yellow or brown with age;</text>
      <biological_entity id="o6150" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="course_or_shape" src="d0_s13" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="inclined" value_original="inclined" />
        <character is_modifier="false" modifier="when dry" name="architecture" notes="" src="d0_s13" value="furrowed" value_original="furrowed" />
        <character constraint="below mouth" constraintid="o6152" is_modifier="false" name="condition_or_size" src="d0_s13" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="reddish yellow" value_original="reddish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="reddish yellow" value_original="reddish yellow" />
        <character constraint="with age" constraintid="o6153" is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o6151" name="struma" name_original="struma" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o6152" name="mouth" name_original="mouth" src="d0_s13" type="structure" />
      <biological_entity id="o6153" name="age" name_original="age" src="d0_s13" type="structure" />
      <relation from="o6150" id="r1472" modifier="often" name="with" negation="false" src="d0_s13" to="o6151" />
    </statement>
    <statement id="d0_s14">
      <text>operculum 1.5–2.5 mm.</text>
      <biological_entity id="o6154" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 14–20 µm.</text>
      <biological_entity id="o6155" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s15" to="20" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature in summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Humus or soil over rock, rarely rotting logs or decayed wood</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humus" constraint="over rock , rarely rotting" />
        <character name="habitat" value="soil" constraint="over rock , rarely rotting" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="decayed wood" modifier="rotting logs or" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Dicranum pallidisetum has often been confused with D. fuscescens, being considered a form of it for many years (R. R. Ireland 1966). Unlike that species, which often occurs at low altitudes below 800 m, D. pallidisetum occurs predominately at high elevations, 800–2000 m, rarely being found as low as 500 m. It is distinguished from D. fuscescens by its tubulose leaves, usually 1-stratose or rarely 2-stratose on the margins, by its costa cross section that shows no stereid bands in the distal 1/3 of the leaf, and by its yellow or yellow-brown capsules that are sometimes 2–3 per perichaetium. In contrast, D. fuscescens has leaves keeled above, usually 2-stratose on both margins, rarely 1-stratose on one margin, stereid bands present throughout the leaf and dark brown to reddish brown capsules that are solitary or rarely 2 per perichaetium.</discussion>
  
</bio:treatment>