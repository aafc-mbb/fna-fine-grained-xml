<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">638</other_info_on_meta>
    <other_info_on_meta type="illustration_page">637</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Müller Hal." date="1847" rank="genus">acaulon</taxon_name>
    <taxon_name authority="(Sullivant) Sullivant in W. S. Sullivant and C. L. Lesquereux" date="1857" rank="species">schimperianum</taxon_name>
    <place_of_publication>
      <publication_title>in W. S. Sullivant and C. L. Lesquereux, Musc. Bor.-Amer., sched.</publication_title>
      <place_in_publication>8. 1857,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus acaulon;species schimperianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443892</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phascum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="species">schimperianum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 615. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Phascum;species schimperianum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants elliptic to globose, occasionally three-angled, 1–1.5 mm.</text>
      <biological_entity id="o1324" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s0" to="globose" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s0" value="3-angled" value_original="3-angled" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s0" to="1.5" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves awned, broadly channeled;</text>
      <biological_entity id="o1325" name="stem-leaf" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="awned" value_original="awned" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="channeled" value_original="channeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>laminal cells papillose abaxially.</text>
      <biological_entity constraint="laminal" id="o1326" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Seta as long as the diameter of the capsule.</text>
      <biological_entity id="o1327" name="seta" name_original="seta" src="d0_s3" type="structure" />
      <biological_entity id="o1328" name="capsule" name_original="capsule" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Spores spheric, 35-40 µm, papillose.</text>
      <biological_entity id="o1329" name="spore" name_original="spores" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="35" from_unit="um" name="some_measurement" src="d0_s4" to="40" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="winter" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, dry washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="dry washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Ill., Iowa, Kans., Tex.; Mexico (Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Acaulon schimperianum is awned and the distal margins are more strongly dentate than those of the other species.</discussion>
  <references>
    <reference>Crum, H. A. and L. E. Anderson. 1965. The taxonomy and distribution of Acaulon schimperianum. Bryologist 68: 208–211.</reference>
  </references>
  
</bio:treatment>