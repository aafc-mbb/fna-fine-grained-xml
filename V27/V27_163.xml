<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gary L. Smith Merrill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="mention_page">33</other_info_on_meta>
    <other_info_on_meta type="mention_page">38</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">132</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="treatment_page">133</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">POLYTRICHUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>88. 1801  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus POLYTRICHUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek polys, many, and thrix, hair; applied in antiquity to plants with fine, hairlike parts, including mosses; now alluding to hairy calyptra</other_info_on_name>
    <other_info_on_name type="fna_id">126484</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to tall and robust, in loose to compact tufts, arising from a horizontal underground rhizome.</text>
      <biological_entity id="o117" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" name="height" src="d0_s0" value="tall" value_original="tall" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character constraint="from rhizome" constraintid="o119" is_modifier="false" name="orientation" notes="" src="d0_s0" value="arising" value_original="arising" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o118" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character char_type="range_value" from="loose" is_modifier="true" name="architecture" src="d0_s0" to="compact" />
      </biological_entity>
      <biological_entity id="o119" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="true" name="location" src="d0_s0" value="underground" value_original="underground" />
      </biological_entity>
      <relation from="o117" id="r34" name="in" negation="false" src="d0_s0" to="o118" />
    </statement>
    <statement id="d0_s1">
      <text>Stems loosely to densely leafy distally, bracteate proximally, rhizoidous at base or rarely wooly-tomentose throughout.</text>
      <biological_entity id="o120" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="loosely to densely; distally" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="bracteate" value_original="bracteate" />
        <character constraint="at base" constraintid="o121" is_modifier="false" name="architecture" src="d0_s1" value="rhizoidous" value_original="rhizoidous" />
        <character is_modifier="false" modifier="rarely; throughout" name="pubescence" src="d0_s1" value="wooly-tomentose" value_original="wooly-tomentose" />
      </biological_entity>
      <biological_entity id="o121" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves with differentiated sheath and blade;</text>
      <biological_entity id="o122" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o123" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character is_modifier="true" name="variability" src="d0_s2" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o124" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <relation from="o122" id="r35" name="with" negation="false" src="d0_s2" to="o123" />
      <relation from="o122" id="r36" name="with" negation="false" src="d0_s2" to="o124" />
    </statement>
    <statement id="d0_s3">
      <text>sheath entire, hyaline-margined, often highly nitid (glossy), with a well-developed hinge-tissue at the junction of sheath and blade;</text>
      <biological_entity id="o125" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="hyaline-margined" value_original="hyaline-margined" />
        <character is_modifier="false" modifier="often highly" name="reflectance" src="d0_s3" value="nitid" value_original="nitid" />
      </biological_entity>
      <biological_entity id="o126" name="hinge-tissue" name_original="hinge-tissue" src="d0_s3" type="structure">
        <character is_modifier="true" name="development" src="d0_s3" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <biological_entity id="o127" name="junction" name_original="junction" src="d0_s3" type="structure" />
      <biological_entity id="o128" name="sheath" name_original="sheath" src="d0_s3" type="structure" />
      <biological_entity id="o129" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <relation from="o125" id="r37" name="with" negation="false" src="d0_s3" to="o126" />
      <relation from="o126" id="r38" name="at" negation="false" src="d0_s3" to="o127" />
      <relation from="o127" id="r39" name="part_of" negation="false" src="d0_s3" to="o128" />
      <relation from="o127" id="r40" name="part_of" negation="false" src="d0_s3" to="o129" />
    </statement>
    <statement id="d0_s4">
      <text>marginal lamina narrow, plane or erect, sharply toothed with stout, unicellular teeth, or entire, broadened and ± sharply inflexed, enclosing the lamellae;</text>
      <biological_entity constraint="marginal" id="o130" name="lamina" name_original="lamina" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character constraint="with teeth" constraintid="o131" is_modifier="false" modifier="sharply" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="width" src="d0_s4" value="broadened" value_original="broadened" />
        <character is_modifier="false" modifier="more or less sharply" name="orientation" src="d0_s4" value="inflexed" value_original="inflexed" />
      </biological_entity>
      <biological_entity id="o131" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <biological_entity id="o132" name="lamella" name_original="lamellae" src="d0_s4" type="structure" />
      <relation from="o130" id="r41" name="enclosing the" negation="false" src="d0_s4" to="o132" />
    </statement>
    <statement id="d0_s5">
      <text>costa short-excurrent or prolonged as a long, spinulose awn;</text>
      <biological_entity id="o133" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-excurrent" value_original="short-excurrent" />
        <character constraint="as awn" constraintid="o134" is_modifier="false" name="length" src="d0_s5" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o134" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="spinulose" value_original="spinulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lamellae numerous, closely-spaced, the marginal cells in section distinctly differentiated, pyriform, flattopped, or retuse, smooth.</text>
      <biological_entity id="o135" name="lamella" name_original="lamellae" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="closely-spaced" value_original="closely-spaced" />
      </biological_entity>
      <biological_entity id="o137" name="section" name_original="section" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distinctly" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o136" id="r42" name="in" negation="false" src="d0_s6" to="o137" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="marginal" id="o136" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>male plants with conspicuous rosettes formed by the broadly overlapping, apiculate perigonial bracts, commonly innovating and producing several successive male inflorescences per shoot;</text>
      <biological_entity id="o138" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="male" value_original="male" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o139" name="rosette" name_original="rosettes" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o140" name="perigonial" name_original="perigonial" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="broadly" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o141" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="broadly" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o142" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="several" value_original="several" />
        <character is_modifier="true" name="development" src="d0_s8" value="successive" value_original="successive" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="male" value_original="male" />
      </biological_entity>
      <biological_entity id="o143" name="shoot" name_original="shoot" src="d0_s8" type="structure" />
      <relation from="o138" id="r43" name="with" negation="false" src="d0_s8" to="o139" />
      <relation from="o139" id="r44" name="formed by the" negation="false" src="d0_s8" to="o140" />
      <relation from="o139" id="r45" name="formed by the" negation="false" src="d0_s8" to="o141" />
      <relation from="o138" id="r46" modifier="commonly" name="innovating" negation="false" src="d0_s8" to="o142" />
      <relation from="o138" id="r47" modifier="commonly" name="per" negation="false" src="d0_s8" to="o143" />
    </statement>
    <statement id="d0_s9">
      <text>perichaetial leaves typically long-sheathing, the sheath broadly hyaline-margined, with a weakly-developed and greatly abbreviated blade.</text>
      <biological_entity constraint="perichaetial" id="o144" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="typically" name="architecture_or_shape" src="d0_s9" value="long-sheathing" value_original="long-sheathing" />
      </biological_entity>
      <biological_entity id="o145" name="sheath" name_original="sheath" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s9" value="hyaline-margined" value_original="hyaline-margined" />
      </biological_entity>
      <biological_entity id="o146" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="true" name="development" src="d0_s9" value="weakly-developed" value_original="weakly-developed" />
        <character is_modifier="true" modifier="greatly" name="size" src="d0_s9" value="abbreviated" value_original="abbreviated" />
      </biological_entity>
      <relation from="o145" id="r48" name="with" negation="false" src="d0_s9" to="o146" />
    </statement>
    <statement id="d0_s10">
      <text>Seta solitary.</text>
      <biological_entity id="o147" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule 4 (–5) -angled, often somewhat broader toward the base, alate and prismatic with knife-edge angles after the operculum shed, reddish to purplish brown, glaucous in fresh capsules, suberect when young but becoming sharply bent at the attachment to the seta and almost horizontal;</text>
      <biological_entity id="o148" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="4(-5)-angled" value_original="4(-5)-angled" />
        <character constraint="toward base" constraintid="o149" is_modifier="false" modifier="often somewhat" name="width" src="d0_s11" value="broader" value_original="broader" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="alate" value_original="alate" />
        <character constraint="with knife-edge angles" constraintid="o150" is_modifier="false" name="shape" src="d0_s11" value="prismatic" value_original="prismatic" />
        <character char_type="range_value" from="reddish" name="coloration" notes="" src="d0_s11" to="purplish brown" />
        <character constraint="in capsules" constraintid="o152" is_modifier="false" name="pubescence" src="d0_s11" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="when young" name="orientation" notes="" src="d0_s11" value="suberect" value_original="suberect" />
        <character constraint="at attachment" constraintid="o153" is_modifier="false" modifier="becoming sharply" name="shape" src="d0_s11" value="bent" value_original="bent" />
        <character is_modifier="false" modifier="almost" name="orientation" notes="" src="d0_s11" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <biological_entity id="o149" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity constraint="knife-edge" id="o150" name="angle" name_original="angles" src="d0_s11" type="structure" />
      <biological_entity id="o151" name="operculum" name_original="operculum" src="d0_s11" type="structure" />
      <biological_entity id="o152" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="true" name="condition" src="d0_s11" value="fresh" value_original="fresh" />
      </biological_entity>
      <biological_entity id="o153" name="attachment" name_original="attachment" src="d0_s11" type="structure" />
      <biological_entity id="o154" name="seta" name_original="seta" src="d0_s11" type="structure" />
      <relation from="o150" id="r49" name="after" negation="false" src="d0_s11" to="o151" />
      <relation from="o153" id="r50" name="to" negation="false" src="d0_s11" to="o154" />
    </statement>
    <statement id="d0_s12">
      <text>hypophysis discoid, sharply delimited from the urn by a deep basal constriction;</text>
      <biological_entity id="o155" name="hypophysis" name_original="hypophysis" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="discoid" value_original="discoid" />
        <character constraint="from urn" constraintid="o156" is_modifier="false" modifier="sharply" name="prominence" src="d0_s12" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity id="o156" name="urn" name_original="urn" src="d0_s12" type="structure" />
      <biological_entity constraint="basal" id="o157" name="constriction" name_original="constriction" src="d0_s12" type="structure">
        <character is_modifier="true" name="depth" src="d0_s12" value="deep" value_original="deep" />
      </biological_entity>
      <relation from="o156" id="r51" name="by" negation="false" src="d0_s12" to="o157" />
    </statement>
    <statement id="d0_s13">
      <text>stomata rather few and confined to the constriction;</text>
      <biological_entity id="o158" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="rather" name="quantity" src="d0_s13" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o159" name="constriction" name_original="constriction" src="d0_s13" type="structure" />
      <relation from="o158" id="r52" name="confined to the" negation="false" src="d0_s13" to="o159" />
    </statement>
    <statement id="d0_s14">
      <text>exothecial cells bulging-mammillose, often transversely elongate, with a sharply defined, circular or slitlike pit in the outer wall;</text>
      <biological_entity constraint="exothecial" id="o160" name="bud" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="bulging-mammillose" value_original="bulging-mammillose" />
        <character is_modifier="false" modifier="often transversely" name="shape" src="d0_s14" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o161" name="pit" name_original="pit" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="sharply" name="prominence" src="d0_s14" value="defined" value_original="defined" />
        <character is_modifier="true" name="shape" src="d0_s14" value="circular" value_original="circular" />
        <character is_modifier="true" name="shape" src="d0_s14" value="slit-like" value_original="slitlike" />
      </biological_entity>
      <biological_entity constraint="outer" id="o162" name="wall" name_original="wall" src="d0_s14" type="structure" />
      <relation from="o160" id="r53" name="with" negation="false" src="d0_s14" to="o161" />
      <relation from="o161" id="r54" name="in" negation="false" src="d0_s14" to="o162" />
    </statement>
    <statement id="d0_s15">
      <text>operculum umbonate, with a short beak;</text>
      <biological_entity id="o163" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="umbonate" value_original="umbonate" />
      </biological_entity>
      <biological_entity id="o164" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
      </biological_entity>
      <relation from="o163" id="r55" name="with" negation="false" src="d0_s15" to="o164" />
    </statement>
    <statement id="d0_s16">
      <text>peristome teeth 64, pale, single, with a thin vertical keel and delicate spurlike projections in the inner face;</text>
      <biological_entity constraint="peristome" id="o165" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="64" value_original="64" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale" value_original="pale" />
        <character is_modifier="false" name="quantity" src="d0_s16" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o166" name="keel" name_original="keel" src="d0_s16" type="structure">
        <character is_modifier="true" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character is_modifier="true" name="orientation" src="d0_s16" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o167" name="projection" name_original="projections" src="d0_s16" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s16" value="delicate" value_original="delicate" />
        <character is_modifier="true" name="shape" src="d0_s16" value="spur-like" value_original="spurlike" />
      </biological_entity>
      <biological_entity constraint="inner" id="o168" name="face" name_original="face" src="d0_s16" type="structure" />
      <relation from="o165" id="r56" name="with" negation="false" src="d0_s16" to="o166" />
      <relation from="o165" id="r57" name="with" negation="false" src="d0_s16" to="o167" />
      <relation from="o167" id="r58" name="in" negation="false" src="d0_s16" to="o168" />
    </statement>
    <statement id="d0_s17">
      <text>epiphragm thin and delicate, remaining attached to the peristome teeth, the margin thicker and dissected into pendent lobes alternating with the peristome teeth.</text>
      <biological_entity id="o169" name="epiphragm" name_original="epiphragm" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s17" value="delicate" value_original="delicate" />
        <character constraint="to peristome teeth" constraintid="o170" is_modifier="false" name="fixation" src="d0_s17" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o170" name="tooth" name_original="teeth" src="d0_s17" type="structure" />
      <biological_entity id="o171" name="margin" name_original="margin" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thicker" value_original="thicker" />
        <character constraint="into lobes" constraintid="o172" is_modifier="false" name="shape" src="d0_s17" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity id="o172" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s17" value="pendent" value_original="pendent" />
        <character constraint="with peristome teeth" constraintid="o173" is_modifier="false" name="arrangement" src="d0_s17" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity constraint="peristome" id="o173" name="tooth" name_original="teeth" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Calyptra with a densely interwoven, matted felt of hairs, enveloping the whole capsule and entwined beneath.</text>
      <biological_entity id="o174" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="with a densely interwoven" name="growth_form" src="d0_s18" value="matted" value_original="matted" />
      </biological_entity>
      <biological_entity id="o175" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <biological_entity id="o176" name="capsule" name_original="capsule" src="d0_s18" type="structure" />
      <relation from="o174" id="r59" name="felt of" negation="false" src="d0_s18" to="o175" />
      <relation from="o174" id="r60" name="enveloping the whole" negation="false" src="d0_s18" to="o176" />
    </statement>
    <statement id="d0_s19">
      <text>Spores small, smooth (minutely echinulate with SEM).</text>
      <biological_entity id="o177" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character is_modifier="false" name="size" src="d0_s19" value="small" value_original="small" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Cosmopolitan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Cosmopolitan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Species ca. 70 (7 in the flora).</discussion>
  <discussion>Polytrichum is characterized by a unique set of tightly correlated sporophytic characters. The capsules are sharply angled with a discoid hypophysis separated from the body of the capsule by a deep basal constriction, and a glaucous appearance when fresh (see E. Lawton 1971, frontisp.). The exothecial cells are bulging-mammillose, with a sharply-defined pit, and the peristome and epiphragm are of the pterygodont type (S. O. Lindberg 1868; Gary L. Smith 1971, 1974). Polytrichum species are distinct genetically from other members of the family, suggesting an early origin for this lineage. Capsules with bulging-mammillose, pitted exothecial cells, discoid hypophysis, and spores echinulate with “Christmas-tree” projections are already present in the Late Cretaceous fossil genus Eopolytrichum (A. S. Konopka et al. 1997). Two groups of Polytrichum species are represented in our area, one with narrow, toothed, ± erect leaf margins (sect. Polytrichum), and the other with broad, entire, sharply inflexed leaf margins, enclosing and sheltering the adaxial lamellae (sect. Juniperifolia).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Marginal lamina narrow, mostly erect, the margins sharply toothed to subentire; cells of the marginal lamina ± isodiametric</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Marginal lamina broad, membranous, sharply inflexed and overlapping, enclosing the lamellae, the margins entire or minutely crenulate; cells of the marginal lamina transversely elongate-rectangular, thick-walled</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Marginal cells of lamellae in section single or geminate, usually broader than tall, rounded-quadrate, flat-topped or asymmetrical, the margin flat-topped or shallowly crenulate, without projecting knobs.</description>
      <determination>3 Polytrichum swartzii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Marginal cells of lamellae in section single, retuse to distinctly notched (rarely divided by a vertical partition), the margin distinctly grooved as seen from above, with 2 rows of paired, projecting knobs</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems (2-)5-10(-70) cm; leaves with blade squarrose-recurved when moist, not caducous, margins strongly toothed; capsule rectangular.</description>
      <determination>1 Polytrichum commune</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems 3-12 cm; leaves straight or weakly recurved when moist, the blade caducous, margins entire or finely serrulate; capsule short-rectangular to cubic.</description>
      <determination>2 Polytrichum jensenii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf sheath ovate; blade abruptly contracted to a long, hyaline awn.</description>
      <determination>7 Polytrichum piliferum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf sheath elliptic to rectangular; blade tapering to a short brownish or bicolored awn</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants fastigiately branched; awns roughly toothed at the base, bicolored, reddish brown at base, hyaline in distal 1/2; marginal cells of lamellae ovoid, ± thin-walled.</description>
      <determination>5 Polytrichum hyperboreum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants simple; awns roughened to subentire, concolorous, brownish throughout (or hyaline only at extreme tip); marginal cells of lamellae in section pyriform, thick-walled especially in the apex</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems brownish tomentose only near base; leaves longer, 3-6(-8) mm; capsules to 2 times longer than broad.</description>
      <determination>4 Polytrichum juniperinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems densely whitish tomentose; leaves short, 2-5(-6) mm; capsules cubic.</description>
      <determination>6 Polytrichum strictum</determination>
    </key_statement>
  </key>
</bio:treatment>