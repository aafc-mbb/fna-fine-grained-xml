<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="treatment_page">406</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">dicranum</taxon_name>
    <taxon_name authority="Sullivant" date="1849" rank="species">rhabdocarpum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 172. 1849,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus dicranum;species rhabdocarpum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443668</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Orthodicranum</taxon_name>
    <taxon_name authority="(Sullivant) Holzinger" date="unknown" rank="species">rhabdocarpum</taxon_name>
    <taxon_hierarchy>genus Orthodicranum;species rhabdocarpum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense tufts, green to yellowish green or brownish, ± glossy.</text>
      <biological_entity id="o5094" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="yellowish green or brownish" />
        <character is_modifier="false" modifier="more or less" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5095" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o5094" id="r1187" name="in" negation="false" src="d0_s0" to="o5095" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–8 cm, tomentose with reddish-brown rhizoids.</text>
      <biological_entity id="o5096" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character constraint="with rhizoids" constraintid="o5097" is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o5097" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves straight or nearly so, spreading, little changed when dry, smooth, 3–5.5 × 0.6–1.2 mm, concave or tubulose proximally, tubulose to slightly keeled above;</text>
    </statement>
    <statement id="d0_s3">
      <text>ovatelanceolate, apex obtusely acute;</text>
      <biological_entity id="o5098" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character name="course" src="d0_s2" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when dry" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s2" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s2" value="tubulose" value_original="tubulose" />
        <character char_type="range_value" from="tubulose" name="shape" src="d0_s2" to="slightly keeled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity id="o5099" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins serrate near apex;</text>
      <biological_entity id="o5100" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="near apex" constraintid="o5101" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o5101" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>laminae 1-stratose;</text>
      <biological_entity id="o5102" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa subpercurrent to percurrent, 1/10–1/8 the width of the leaves at base, smooth or weakly toothed on abaxial surface near apex, with a row of guide cells, two weak stereid bands, at least in basal part of leaf, adaxial and abaxial epidermal layers of cells not differentiated;</text>
      <biological_entity id="o5103" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character char_type="range_value" from="1/10" name="quantity" src="d0_s6" to="1/8" />
        <character is_modifier="false" name="width" notes="" src="d0_s6" value="smooth" value_original="smooth" />
        <character constraint="on abaxial surface" constraintid="o5106" is_modifier="false" modifier="weakly" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="fragility" src="d0_s6" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o5104" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o5105" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o5106" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <biological_entity id="o5107" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o5108" name="row" name_original="row" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="guide" id="o5109" name="cell" name_original="cells" src="d0_s6" type="structure" />
      <biological_entity id="o5110" name="band" name_original="bands" src="d0_s6" type="structure" />
      <biological_entity constraint="basal" id="o5111" name="part" name_original="part" src="d0_s6" type="structure" />
      <biological_entity id="o5112" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial adaxial and epidermal" id="o5113" name="layer" name_original="layers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o5114" name="cell" name_original="cells" src="d0_s6" type="structure" />
      <relation from="o5104" id="r1188" name="at" negation="false" src="d0_s6" to="o5105" />
      <relation from="o5106" id="r1189" name="near" negation="false" src="d0_s6" to="o5107" />
      <relation from="o5103" id="r1190" name="with" negation="false" src="d0_s6" to="o5108" />
      <relation from="o5108" id="r1191" name="part_of" negation="false" src="d0_s6" to="o5109" />
      <relation from="o5103" id="r1192" modifier="at-least" name="in" negation="false" src="d0_s6" to="o5111" />
      <relation from="o5111" id="r1193" name="part_of" negation="false" src="d0_s6" to="o5112" />
      <relation from="o5113" id="r1194" name="part_of" negation="false" src="d0_s6" to="o5114" />
    </statement>
    <statement id="d0_s7">
      <text>cell-walls between lamina cells not bulging;</text>
      <biological_entity constraint="between cells" constraintid="o5116" id="o5115" name="cell-wall" name_original="cell-walls" src="d0_s7" type="structure" constraint_original="between  cells, ">
        <character is_modifier="false" modifier="not" name="pubescence_or_shape" src="d0_s7" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity constraint="lamina" id="o5116" name="cell" name_original="cells" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>leaf cells smooth;</text>
    </statement>
    <statement id="d0_s9">
      <text>alar cells 1-stratose or 2-stratose in part, differentiated, not extending to costa;</text>
      <biological_entity constraint="leaf" id="o5117" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o5118" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-stratose" value_original="1-stratose" />
        <character constraint="in part" constraintid="o5119" is_modifier="false" name="architecture" src="d0_s9" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="variability" notes="" src="d0_s9" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o5119" name="part" name_original="part" src="d0_s9" type="structure" />
      <biological_entity id="o5120" name="costa" name_original="costa" src="d0_s9" type="structure" />
      <relation from="o5118" id="r1195" name="extending to" negation="true" src="d0_s9" to="o5120" />
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells linear-rectangular, pitted, (45–) 65–120 (–150) × (13–) 16–17 (–19) µm; distal laminal cells shorter, narrow, pitted or with few pits, (20–) 30–45 (–60) × (5–) 8–10 (–13) µm. Sexual condition dioicous;</text>
      <biological_entity constraint="proximal laminal" id="o5121" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-rectangular" value_original="linear-rectangular" />
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character char_type="range_value" from="45" from_unit="um" name="atypical_length" src="d0_s10" to="65" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s10" to="150" to_unit="um" />
        <character char_type="range_value" from="65" from_unit="um" name="length" src="d0_s10" to="120" to_unit="um" />
        <character char_type="range_value" from="13" from_unit="um" name="atypical_width" src="d0_s10" to="16" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s10" to="19" to_unit="um" />
        <character char_type="range_value" from="16" from_unit="um" name="width" src="d0_s10" to="17" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal laminal" id="o5122" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="relief" src="d0_s10" value="with few pits" value_original="with few pits" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
      <biological_entity id="o5123" name="pit" name_original="pits" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character char_type="range_value" from="20" from_unit="um" name="atypical_length" notes="alterIDs:o5123" src="d0_s10" to="30" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="um" name="atypical_length" notes="alterIDs:o5123" src="d0_s10" to="60" to_unit="um" />
        <character char_type="range_value" from="30" from_unit="um" name="length" notes="alterIDs:o5123" src="d0_s10" to="45" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="atypical_width" notes="alterIDs:o5123" src="d0_s10" to="8" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="um" name="atypical_width" notes="alterIDs:o5123" src="d0_s10" to="13" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" notes="alterIDs:o5123" src="d0_s10" to="10" to_unit="um" />
      </biological_entity>
      <relation from="o5122" id="r1196" name="with" negation="false" src="d0_s10" to="o5123" />
    </statement>
    <statement id="d0_s11">
      <text>male plants about as large as the female or slightly smaller;</text>
      <biological_entity id="o5124" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="male" value_original="male" />
        <character is_modifier="false" modifier="as-large-as the female; slightly" name="size" src="d0_s11" value="smaller" value_original="smaller" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>interior perichaetial leaves ± abruptly acuminate, convolute-sheathing.</text>
      <biological_entity constraint="interior perichaetial" id="o5125" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less abruptly" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="convolute-sheathing" value_original="convolute-sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta 1.5–3 cm, solitary, rarely two per perichaetium, yellow to reddish-brown.</text>
      <biological_entity id="o5126" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s13" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s13" value="solitary" value_original="solitary" />
        <character constraint="per perichaetium" constraintid="o5127" modifier="rarely" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character char_type="range_value" from="yellow" name="coloration" notes="" src="d0_s13" to="reddish-brown" />
      </biological_entity>
      <biological_entity id="o5127" name="perichaetium" name_original="perichaetium" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsule 1.5–4 mm, erect, straight or nearly so, furrowed when dry, brown;</text>
      <biological_entity id="o5128" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character name="course" src="d0_s14" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s14" value="furrowed" value_original="furrowed" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum 1.6–2.8 mm.</text>
      <biological_entity id="o5129" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s15" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 13–19 µm.</text>
      <biological_entity id="o5130" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s16" to="19" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, soil over rock, peaty soil or rotten wood</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="soil" constraint="over rock" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="peaty soil" />
        <character name="habitat" value="rotten wood" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Wyo.; Mexico (Chihuahua, Tamaulipas); West Indies (Dominican Republic); Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Dicranum rhabdocarpum is an easily recognized species that occurs in the flora area only in the Rocky Mountains and the mountains of Arizona. It is the only species in the section Dicranum that has erect, straight to weakly arcuate capsules. Other important distinguishing features are the ovate-lanceolate, straight, obtusely acute, 1-stratose leaves with alar cells 1- or 2-stratose in part and the subpercurrent to percurrent costae that are smooth or weakly toothed on the abaxial surface near the leaf apex. Dicranum rhabdocarpum has been placed in the segregate genus Orthodicranum by J. M. Holzinger (1925b) and other bryologists mainly because of its straight and erect capsules. However, it differs from taxa commonly placed in that genus, i.e., D. flagellare, D. fulvum, D. montanum, D. strictum, and D. viride, by the elongate, pitted cells and the alar cells that are sometimes 2-stratose.</discussion>
  
</bio:treatment>