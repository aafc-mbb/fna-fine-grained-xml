<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Wilson" date="1855" rank="section">acutifolia</taxon_name>
    <taxon_name authority="H. A. Crum in N. L. Britton et al." date="1984" rank="species">wilfii</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl., ser.</publication_title>
      <place_in_publication>2, 11: 90, fig. 57. 1984,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section acutifolia;species wilfii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443343</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely tufted, capitulum ± flattopped;</text>
      <biological_entity id="o4115" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>typically red;</text>
    </statement>
    <statement id="d0_s2">
      <text>forms small tufts and hummocks in shaded and open sites.</text>
      <biological_entity id="o4116" name="capitulum" name_original="capitulum" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="typically" name="coloration" src="d0_s1" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o4117" name="tuft" name_original="tufts" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
        <character constraint="in sites" constraintid="o4118" is_modifier="false" name="habitat" src="d0_s2" value="hummocks" value_original="hummocks" />
      </biological_entity>
      <biological_entity id="o4118" name="site" name_original="sites" src="d0_s2" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s2" value="shaded" value_original="shaded" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems red;</text>
      <biological_entity id="o4119" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>superficial cortical cells aporose.</text>
      <biological_entity constraint="superficial cortical" id="o4120" name="bud" name_original="cells" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves 1.2 mm or more, broadly triangular to triangular-lingulate, 1.2 or more, apex acute, border broad at base (more than 0.25 width);</text>
      <biological_entity id="o4121" name="leaf-stem" name_original="stem-leaves" src="d0_s5" type="structure">
        <character name="distance" src="d0_s5" unit="mm" value="1.2" value_original="1.2" />
        <character char_type="range_value" from="broadly triangular" name="shape" src="d0_s5" to="triangular-lingulate" />
        <character name="quantity" src="d0_s5" value="1.2" value_original="1.2" />
      </biological_entity>
      <biological_entity id="o4122" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o4123" name="border" name_original="border" src="d0_s5" type="structure">
        <character constraint="at base" constraintid="o4124" is_modifier="false" name="width" src="d0_s5" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o4124" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>hyaline cells mostly efibrillose, 1–2-septate.</text>
      <biological_entity id="o4125" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s6" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-2-septate" value_original="1-2-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Branches uncrowded, 5-ranked.</text>
      <biological_entity id="o4126" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_density" src="d0_s7" value="uncrowded" value_original="uncrowded" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="5-ranked" value_original="5-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch fascicles with 2 spreading and 1 pendent branch.</text>
      <biological_entity id="o4127" name="branch" name_original="branch" src="d0_s8" type="structure">
        <character constraint="with branch" constraintid="o4128" is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o4128" name="branch" name_original="branch" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves ovatelanceolate, 0.7 mm or more, straight, concave, loosely involute from apex to middle or near base;</text>
      <biological_entity constraint="branch" id="o4129" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.7" value_original="0.7" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s9" value="concave" value_original="concave" />
        <character constraint="from apex" constraintid="o4130" is_modifier="false" modifier="loosely" name="shape_or_vernation" src="d0_s9" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o4130" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o4131" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="true" name="position" src="d0_s9" value="middle" value_original="middle" />
      </biological_entity>
      <relation from="o4130" id="r1122" name="to" negation="false" src="d0_s9" to="o4131" />
    </statement>
    <statement id="d0_s10">
      <text>concave surface with few (2–4) small, rounded, or elliptic pores, especially in cell angles, concave surface aporose or with 1–2 pores at cell ends.</text>
      <biological_entity id="o4132" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o4133" name="pore" name_original="pores" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s10" to="4" />
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
        <character is_modifier="true" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity constraint="cell" id="o4134" name="angle" name_original="angles" src="d0_s10" type="structure" />
      <biological_entity id="o4136" name="pore" name_original="pores" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <biological_entity constraint="cell" id="o4137" name="end" name_original="ends" src="d0_s10" type="structure" />
      <relation from="o4132" id="r1123" name="with" negation="false" src="d0_s10" to="o4133" />
      <relation from="o4132" id="r1124" modifier="especially" name="in" negation="false" src="d0_s10" to="o4134" />
      <relation from="o4135" id="r1125" name="with" negation="false" src="d0_s10" to="o4136" />
      <relation from="o4136" id="r1126" name="at" negation="false" src="d0_s10" to="o4137" />
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition unknown.</text>
      <biological_entity id="o4135" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="concave" value_original="concave" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores unknown.</text>
      <biological_entity id="o4138" name="spore" name_original="spores" src="d0_s12" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Blanket mires, especially with Pinus contorta</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="blanket mires" />
        <character name="habitat" value="pinus contorta" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>89.</number>
  <discussion>The type locality of Sphagnum wilfii in the Queen Charlotte Islands of British Columbia is a site on a pygmy pine slope near the coast. This species has been collected only infrequently but is fairly common in southeastern Alaska. The combination of red pigment, the rather large and triangular to triangular-lingulate stem leaves and the quinquefarious, loosely spreading branch leaves should identify it where it occurs. See also discussion under 68. S. bartlettianum.</discussion>
  
</bio:treatment>