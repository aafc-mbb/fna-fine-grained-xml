<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="treatment_page">347</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">fissidentaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">fissidens</taxon_name>
    <taxon_name authority="Müller Hal." date="1848" rank="species">taylorii</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Frond.</publication_title>
      <place_in_publication>1: 65. 1848,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fissidentaceae;genus fissidens;species taylorii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075569</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1.4–1.6 × 0.5–1 mm.</text>
      <biological_entity id="o3940" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s0" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s0" to="1" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem unbranched;</text>
      <biological_entity id="o3941" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hyaline nodules present;</text>
      <biological_entity constraint="axillary" id="o3942" name="nodule" name_original="nodules" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand absent.</text>
      <biological_entity constraint="central" id="o3943" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves as many as 9 pairs, lanceolate, acute to obtuse-apiculate, to 0.5–1 × 0.1–0.2 mm;</text>
      <biological_entity id="o3944" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse-apiculate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s4" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3945" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="9" value_original="9" />
      </biological_entity>
      <relation from="o3944" id="r908" name="as many as" negation="false" src="d0_s4" to="o3945" />
    </statement>
    <statement id="d0_s5">
      <text>dorsal lamina narrowed proximally, ending at insertion or slightly before;</text>
      <biological_entity constraint="dorsal" id="o3946" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>vaginant laminae 1/2–2/3 leaf length, ± unequal, minor lamina ending near margin;</text>
      <biological_entity constraint="vaginant" id="o3947" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s6" to="2/3" />
      </biological_entity>
      <biological_entity id="o3948" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="length" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="minor" id="o3949" name="lamina" name_original="lamina" src="d0_s6" type="structure" />
      <biological_entity id="o3950" name="margin" name_original="margin" src="d0_s6" type="structure" />
      <relation from="o3949" id="r909" name="ending near" negation="false" src="d0_s6" to="o3950" />
    </statement>
    <statement id="d0_s7">
      <text>margin ± entire, elimbate or irregularly limbate, limbidium best developed on perichaetial and subtending leaves, limbidial cells 1-stratose, infrequently 2-stratose;</text>
      <biological_entity id="o3951" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="elimbate" value_original="elimbate" />
        <character is_modifier="false" modifier="irregularly" name="coloration" src="d0_s7" value="limbate" value_original="limbate" />
      </biological_entity>
      <biological_entity id="o3952" name="limbidium" name_original="limbidium" src="d0_s7" type="structure">
        <character constraint="on perichaetial" constraintid="o3953" is_modifier="false" name="development" src="d0_s7" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o3953" name="perichaetial" name_original="perichaetial" src="d0_s7" type="structure" />
      <biological_entity constraint="perichaetial and subtending" id="o3954" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o3955" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="limbidial" value_original="limbidial" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" modifier="infrequently" name="architecture" src="d0_s7" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa ending 2–3 cells before apex to percurrent, bryoides-type;</text>
      <biological_entity id="o3956" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" notes="" src="d0_s8" value="bryoides-type" value_original="bryoides-type" />
      </biological_entity>
      <biological_entity id="o3957" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o3958" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <relation from="o3956" id="r910" name="ending" negation="false" src="d0_s8" to="o3957" />
      <relation from="o3956" id="r911" name="before" negation="false" src="d0_s8" to="o3958" />
    </statement>
    <statement id="d0_s9">
      <text>laminal cells 1-stratose, smooth, slightly bulging, firm-walled, irregularly hexagonal, 9–11 µm, many slightly longer than wide.</text>
      <biological_entity constraint="laminal" id="o3959" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_shape" src="d0_s9" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s9" value="hexagonal" value_original="hexagonal" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s9" to="11" to_unit="um" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="many" value_original="many" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="slightly longer than wide" value_original="slightly longer than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition rhizautoicous and gonioautoicous;</text>
    </statement>
    <statement id="d0_s11">
      <text>perigonia gemmiform, proximal to infertile and fertile stems, and axillary.</text>
      <biological_entity id="o3960" name="perigonium" name_original="perigonia" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="gemmiform" value_original="gemmiform" />
        <character is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="infertile" value_original="infertile" />
        <character is_modifier="false" name="position" notes="" src="d0_s11" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o3961" name="stem" name_original="stems" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sporophytes 1 per perichaetium.</text>
      <biological_entity id="o3962" name="sporophyte" name_original="sporophytes" src="d0_s12" type="structure">
        <character constraint="per perichaetium" constraintid="o3963" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o3963" name="perichaetium" name_original="perichaetium" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seta 2–23.5 mm.</text>
      <biological_entity id="o3964" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="23.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule theca exserted, erect, radially symmetric to ± arcuate, bilaterally symmetric;</text>
      <biological_entity constraint="capsule" id="o3965" name="theca" name_original="theca" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character char_type="range_value" from="radially symmetric" name="shape" src="d0_s14" to="more or less arcuate" />
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s14" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome bryoides-type;</text>
      <biological_entity id="o3966" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s15" value="bryoides-type" value_original="bryoides-type" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum 0.2 mm.</text>
      <biological_entity id="o3967" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra and spores not seen.</text>
      <biological_entity id="o3968" name="calyptra" name_original="calyptra" src="d0_s17" type="structure" />
      <biological_entity id="o3969" name="spore" name_original="spores" src="d0_s17" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist soil on banks of drainage ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist soil" constraint="on banks of drainage ditches" />
        <character name="habitat" value="banks" constraint="of drainage ditches" />
        <character name="habitat" value="drainage ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Calif.; Mexico; West Indies; South America; Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion>Fissidens taylorii, named in honor of the collector of the type specimen, Thomas Taylor, an Irish botanist, has been collected in the United State only twice. It is similar to F. curvatus by virtue of its dimorphic stems and typical bryoides-type peristome, but differs in its shorter costa and weaker limbidium. The limbidium can be quite variable; smaller leaves can be elimbate while larger leaves are limbate on all laminae.</discussion>
  
</bio:treatment>