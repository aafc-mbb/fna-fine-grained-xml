<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gary L. Smith Merrill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="treatment_page">142</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1805" rank="genus">OLIGOTRICHUM</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Franç. ed.</publication_title>
      <place_in_publication>3, 2: 491. 1805  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus OLIGOTRICHUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oligo-, few, and trichos, hair, alluding to calyptra</other_info_on_name>
    <other_info_on_name type="fna_id">122815</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, not polytrichoid, gregarious, in loose tufts.</text>
      <biological_entity id="o6820" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="polytrichoid" value_original="polytrichoid" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6821" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o6820" id="r1905" name="in" negation="false" src="d0_s0" to="o6821" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or sparingly branched by innovation.</text>
      <biological_entity id="o6822" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="by innovation" constraintid="o6823" is_modifier="false" modifier="sparingly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o6823" name="innovation" name_original="innovation" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves various, weakly sheathing at base, ± plane to distinctly channeled or subtubulose, erect when dry and sometimes crisped, erect spreading when moist (in O. falcatum falcate-secund wet or dry), ovatelanceolate to lanceolate to oblongelliptic;</text>
      <biological_entity id="o6824" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="variability" src="d0_s2" value="various" value_original="various" />
        <character constraint="at base" constraintid="o6825" is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
        <character char_type="range_value" from="less plane" name="shape" notes="" src="d0_s2" to="distinctly channeled" />
        <character name="shape" src="d0_s2" value="subtubulose" value_original="subtubulose" />
        <character is_modifier="false" modifier="when dry and sometimes crisped" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o6825" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apex flat or cucullate, acute, obtuse, or apiculate, the margins subentire, denticulate or coarsely serrate, the margins not bordered by linear cells;</text>
      <biological_entity id="o6826" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o6827" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o6828" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="by cells" constraintid="o6829" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o6829" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa percurrent or short-excurrent, toothed towards apex or with low abaxial lamellae;</text>
      <biological_entity id="o6830" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-excurrent" value_original="short-excurrent" />
        <character constraint="towards apex or with abaxial lamellae" constraintid="o6831" is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6831" name="lamella" name_original="lamellae" src="d0_s4" type="structure">
        <character is_modifier="true" name="position" src="d0_s4" value="low" value_original="low" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lamina broad, 1-stratose, the abaxial surface with low projecting teeth, and often with abaxial lamellae;</text>
      <biological_entity id="o6832" name="lamina" name_original="lamina" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6833" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o6834" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="low" value_original="low" />
        <character is_modifier="true" name="orientation" src="d0_s5" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6835" name="lamella" name_original="lamellae" src="d0_s5" type="structure" />
      <relation from="o6833" id="r1906" name="with" negation="false" src="d0_s5" to="o6834" />
      <relation from="o6833" id="r1907" modifier="often" name="with" negation="false" src="d0_s5" to="o6835" />
    </statement>
    <statement id="d0_s6">
      <text>adaxial lamellae confined to the costa, straight or more often transversely undulate, margins entire to sharply serrate, the marginal cells in cross-section not differentiated, smooth.</text>
      <biological_entity constraint="adaxial" id="o6836" name="lamella" name_original="lamellae" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" notes="" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="often transversely" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o6837" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o6838" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s6" to="sharply serrate" />
      </biological_entity>
      <biological_entity id="o6840" name="cross-section" name_original="cross-section" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o6836" id="r1908" name="confined to" negation="false" src="d0_s6" to="o6837" />
      <relation from="o6839" id="r1909" name="in" negation="false" src="d0_s6" to="o6840" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="marginal" id="o6839" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perigonia often disproportionately large, the bracts broadly ovate, at times colored and petaloid, overlapping, forming a conspicuous rosette;</text>
      <biological_entity id="o6841" name="perigonium" name_original="perigonia" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often disproportionately" name="size" src="d0_s8" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o6842" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="colored" value_original="colored" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o6843" name="rosette" name_original="rosette" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o6842" id="r1910" name="forming a" negation="false" src="d0_s8" to="o6843" />
    </statement>
    <statement id="d0_s9">
      <text>perichaetial leaves longer and narrower than the vegetative leaves.</text>
      <biological_entity constraint="perichaetial" id="o6844" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character constraint="than the vegetative leaves" constraintid="o6845" is_modifier="false" name="width" src="d0_s9" value="longer and narrower" value_original="longer and narrower" />
      </biological_entity>
      <biological_entity id="o6845" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="vegetative" value_original="vegetative" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta typically solitary, smooth.</text>
      <biological_entity id="o6846" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="typically" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule cylindric, usually broadest near the base, terete, often rugose or sometimes with 4 or more indistinct angles or ridges, hypophysis not differentiated, tapering, with stomata at the base;</text>
      <biological_entity id="o6847" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
        <character constraint="near base" constraintid="o6848" is_modifier="false" modifier="usually" name="width" src="d0_s11" value="broadest" value_original="broadest" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="often" name="relief" src="d0_s11" value="rugose" value_original="rugose" />
        <character name="relief" src="d0_s11" value="sometimes" value_original="sometimes" />
      </biological_entity>
      <biological_entity id="o6848" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o6849" name="angle" name_original="angles" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o6850" name="ridge" name_original="ridges" src="d0_s11" type="structure" />
      <biological_entity id="o6851" name="hypophysis" name_original="hypophysis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6852" name="stoma" name_original="stomata" src="d0_s11" type="structure" />
      <biological_entity id="o6853" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o6847" id="r1911" name="with" negation="false" src="d0_s11" to="o6849" />
      <relation from="o6847" id="r1912" name="with" negation="false" src="d0_s11" to="o6850" />
      <relation from="o6851" id="r1913" name="with" negation="false" src="d0_s11" to="o6852" />
      <relation from="o6852" id="r1914" name="at" negation="false" src="d0_s11" to="o6853" />
    </statement>
    <statement id="d0_s12">
      <text>exothecial cells not papillate or pitted;</text>
      <biological_entity constraint="exothecial" id="o6854" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s12" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="relief" src="d0_s12" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>operculum rostrate;</text>
      <biological_entity id="o6855" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome teeth 32, pale, subacute to obtuse, compound or sporadically simple.</text>
      <biological_entity constraint="peristome" id="o6856" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
        <character char_type="range_value" from="subacute" name="shape" src="d0_s14" to="obtuse" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="compound" value_original="compound" />
        <character is_modifier="false" modifier="sporadically" name="architecture" src="d0_s14" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Calyptra sparsely hairy.</text>
      <biological_entity id="o6857" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores finely papillose.</text>
      <biological_entity id="o6858" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Almost worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Almost worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Species 24 (4 in the flora).</discussion>
  <discussion>The North American species of Oligotrichum show wide variation in habit, leaf form, and development of abaxial lamellae. As in Pogonatum, the peristome teeth are compound (double), but in Oligotrichum the teeth are pale and not intensely pigmented. The indistinctly angled or ridged capsules of some species also resemble those of Pogonatum, but in that genus the capsules lack stomata and the exothecial cells are papillate. Oligotrichum parallelum is a lax plant, similar to Atrichum in appearance, but without a differentiated leaf border. All four species produce abaxial as well as adaxial lamellae, most notably O. aligerum.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 3-6 mm, crisped when dry, laxly spreading when moist; leaf margins coarsely and often doubly serrate; adaxial lamellae straight, not or scarcely undulate; stems 2.5-6 cm.</description>
      <determination>1 Oligotrichum parallelum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 1.5-3 mm, not crisped when dry, not much different wet or dry; leaf margins various: regularly serrate, finely and distantly serrulate, or almost entire; adaxial lamellae tall and distinctly undulate; stems 1-2(-3) cm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves narrowly lanceolate, channeled above and ± erect-appressed when dry; lamina and costa with prominent abaxial lamellae; leaf margins distinctly serrate.</description>
      <determination>2 Oligotrichum aligerum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves ovate-lanceolate, subtubulose above, arcuate-incurved or distinctly falcate-secund; abaxial lamellae mostly restricted to the costa; leaf margins remotely denticulate or almost entire</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves ± straight, arcuate-incurved; costa with abaxial lamellae usually present, often extending to below mid-leaf</description>
      <determination>3 Oligotrichum hercynicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves strongly falcate-secund; costa smooth at abaxially or with low abaxial lamellae or teeth near apex.</description>
      <determination>4 Oligotrichum falcatum</determination>
    </key_statement>
  </key>
</bio:treatment>