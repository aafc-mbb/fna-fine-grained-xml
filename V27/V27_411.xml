<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="treatment_page">301</other_info_on_meta>
    <other_info_on_meta type="illustration_page">301</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="subfamily">racomitrioideae</taxon_name>
    <taxon_name authority="P. Beauvois" date="1822" rank="genus">codriophorus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">codriophorus</taxon_name>
    <taxon_name authority="(Bednarek-Ochyra) Bednarek-Ochyra &amp; Ochyra in R. Ochyra et al." date="2003" rank="species">ryszardii</taxon_name>
    <place_of_publication>
      <publication_title>in R. Ochyra et al., Cens. Cat. Polish Mosses,</publication_title>
      <place_in_publication>141. 2003,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily racomitrioideae;genus codriophorus;section codriophorus;species ryszardii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075519</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Racomitrium</taxon_name>
    <taxon_name authority="Bednarek-Ochyra" date="unknown" rank="species">ryszardii</taxon_name>
    <place_of_publication>
      <publication_title>Cryptog. Bryol. Lichénol.</publication_title>
      <place_in_publication>21: 276, figs. 1–3. 2000</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Racomitrium;species ryszardii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, in loose, extensive tufts or mats, dull, green, yellow-brownish or olive to green-brown.</text>
      <biological_entity id="o3650" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="olive" name="coloration" src="d0_s0" to="green-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3651" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="size" src="d0_s0" value="extensive" value_original="extensive" />
      </biological_entity>
      <biological_entity id="o3652" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o3650" id="r846" name="in" negation="false" src="d0_s0" to="o3651" />
      <relation from="o3650" id="r847" name="in" negation="false" src="d0_s0" to="o3652" />
    </statement>
    <statement id="d0_s1">
      <text>Stems (1–) 2–7 cm or occasionally up to 10 cm long, prostrate to ascending, irregularly sparsely branched, usually radiculose at base.</text>
      <biological_entity id="o3653" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character name="some_measurement" src="d0_s1" value="occasionally" value_original="occasionally" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="irregularly sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o3654" name="base" name_original="base" src="d0_s1" type="structure" />
      <relation from="o3653" id="r848" modifier="usually" name="at" negation="false" src="d0_s1" to="o3654" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves lanceolate to linear-lanceolate, (2.8–) 3.2–4.0 (–4.2) × 0.8–1 (–1.1) mm;</text>
      <biological_entity id="o3655" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear-lanceolate" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="atypical_length" src="d0_s2" to="3.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.0" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s2" to="4.0" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins 1-stratose throughout, recurved on both sides to about 3/4–7/8 of way up the leaf;</text>
      <biological_entity id="o3656" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
        <character constraint="on sides" constraintid="o3657" is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o3657" name="side" name_original="sides" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="of way" constraintid="o3658" from="3/4" name="quantity" src="d0_s3" to="7/8" />
      </biological_entity>
      <biological_entity id="o3658" name="way" name_original="way" src="d0_s3" type="structure" />
      <biological_entity id="o3659" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <relation from="o3658" id="r849" name="up" negation="false" src="d0_s3" to="o3659" />
    </statement>
    <statement id="d0_s4">
      <text>apices slenderly long acuminate, obtuse to subacute, irregularly bluntly erose-dentate, cristate to papillose-crenulate, rarely entire at the extreme apex;</text>
      <biological_entity id="o3660" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slenderly" name="shape" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="subacute irregularly bluntly erose-dentate cristate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="subacute irregularly bluntly erose-dentate cristate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="subacute irregularly bluntly erose-dentate cristate" />
        <character constraint="at extreme apex" constraintid="o3661" is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="extreme" id="o3661" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa subpercurrent, (80–) 90–120 (–135) µm wide;</text>
      <biological_entity id="o3662" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="subpercurrent" value_original="subpercurrent" />
        <character char_type="range_value" from="80" from_unit="um" name="width" src="d0_s5" to="90" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="um" name="width" src="d0_s5" to="135" to_unit="um" />
        <character char_type="range_value" from="90" from_unit="um" name="width" src="d0_s5" to="120" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 1-stratose throughout.</text>
      <biological_entity constraint="laminal" id="o3663" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta brown, (4.5–) 5.5–7.5 (–9) mm.</text>
      <biological_entity id="o3664" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule brown, cylindric, (2–) 2.3–3 (–3.2) mm, smooth;</text>
      <biological_entity id="o3665" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>peristome teeth 500–650 µm, dark yellow to orangebrown, regularly 2-fid to the base, densely papillose with tall, spiculose papillae.</text>
      <biological_entity constraint="peristome" id="o3666" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="500" from_unit="um" name="some_measurement" src="d0_s9" to="650" to_unit="um" />
        <character char_type="range_value" from="dark yellow" name="coloration" src="d0_s9" to="orangebrown" />
        <character constraint="to base" constraintid="o3667" is_modifier="false" modifier="regularly" name="shape" src="d0_s9" value="2-fid" value_original="2-fid" />
        <character constraint="with papillae" constraintid="o3668" is_modifier="false" modifier="densely" name="relief" notes="" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o3667" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o3668" name="papilla" name_original="papillae" src="d0_s9" type="structure">
        <character is_modifier="true" name="height" src="d0_s9" value="tall" value_original="tall" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="spiculose" value_original="spiculose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores 10–16 µm.</text>
      <biological_entity id="o3669" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s10" to="16" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist or wet, seldom dry, shaded or exposed granite, basalt and sandstone, rarely limestone rock outcrops, boulders and cliff faces in stream beds and on stream or lake banks throughout the coastal coniferous forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist or wet" />
        <character name="habitat" value="seldom dry" />
        <character name="habitat" value="granite" modifier="shaded or exposed" />
        <character name="habitat" value="shaded" />
        <character name="habitat" value="basalt" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="rock outcrops" modifier="rarely limestone" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="cliff" />
        <character name="habitat" value="stream beds" modifier="faces in" />
        <character name="habitat" value="stream" modifier="and on" />
        <character name="habitat" value="lake banks" />
        <character name="habitat" value="the coastal coniferous forest" modifier="throughout" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Codriophorus ryszardii ranges from Kodiak Island southwards to the Olympic Mountains and Cascade Range of Washington and northern Oregon. The species was recognized as a distinct taxon only recently by Bednarek-Ochyra, who showed that the western North American specimens determined as Racomitrium aquaticum (Schrader) Bridel (W. B. Schofield 1968, 1976; E. Lawton 1971) had nothing to do with that European endemic and represented a new species. Although there are some similarities in the structure of the costa, C. ryszardii is immediately distinct in having lanceolate to linear-lanceolate leaves, 3.2–4 mm. They are narrowly long acuminate and end with an obtuse or subacute apex that is most often bluntly erose-dentate, giving it a cristate appearance, and only seldom is the leaf apex entire. Moreover, its leaf margins are recurved to 3/4–7/8 of the leaf length, costae are often bluntly winged abaxially in the distal portion, laminal cells are entirely 1-stratose, and the supra-alar cells are not differentiated from the adjacent laminal cells and do not form basal marginal borders. Additionally, the peristome teeth are longer, 500–650 µm, and they are densely papillose with needle-like papillae. The leaves of C. aquaticus are shorter, 2.5–3 mm, broadly lanceolate and more shortly acuminate into an obtuse and always entire, never erose-dentate or cristate apex. Moreover, the leaf margins are recurved to 1/2–3/4 of the leaf length, costae are symmetric and never winged abaxially, laminal cells are sometimes 2-stratose distally, the supra-alar cells are quadrate to short-rectangular with relatively thick, smooth to slightly sinuose walls forming short but usually distinct, hyaline to yellowish-hyaline but otherwise pellucid marginal borders, and the peristome teeth are shorter, to 450 µm, and they are only finely papillose.</discussion>
  
</bio:treatment>