<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Patricia M. Eckel</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Steere" date="unknown" rank="family">ANDREAEOBRYACEAE</taxon_name>
    <taxon_hierarchy>family ANDREAEOBRYACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10041</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants clear green to olive green to dark redbrown to dull black, moderate in size, pulvinate to mat-forming.</text>
      <biological_entity id="o8778" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="clear green" name="coloration" src="d0_s0" to="olive green" />
        <character is_modifier="false" name="size" src="d0_s0" value="moderate" value_original="moderate" />
        <character is_modifier="false" name="shape" src="d0_s0" value="pulvinate" value_original="pulvinate" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, irregularly branched;</text>
      <biological_entity id="o8779" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids present;</text>
      <biological_entity id="o8780" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand absent.</text>
      <biological_entity constraint="central" id="o8781" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves dimorphic, falcate-secund and ovatelanceolate, occasionally minute, ecostate, imbricate, squamiform on flagelliform shoots or proximally on main-stem;</text>
      <biological_entity id="o8782" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s4" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="occasionally" name="size" src="d0_s4" value="minute" value_original="minute" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character constraint="on " constraintid="o8784" is_modifier="false" name="shape" src="d0_s4" value="squamiform" value_original="squamiform" />
      </biological_entity>
      <biological_entity id="o8783" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="flagelliform" value_original="flagelliform" />
      </biological_entity>
      <biological_entity id="o8784" name="main-stem" name_original="main-stem" src="d0_s4" type="structure" />
      <biological_entity id="o8785" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="flagelliform" value_original="flagelliform" />
      </biological_entity>
      <biological_entity id="o8786" name="main-stem" name_original="main-stem" src="d0_s4" type="structure" />
      <relation from="o8784" id="r2415" name="on" negation="false" src="d0_s4" to="o8785" />
      <relation from="o8784" id="r2416" name="on" negation="false" src="d0_s4" to="o8786" />
    </statement>
    <statement id="d0_s5">
      <text>costa single, broad, indistinct, excurrent, forming a broad, obtuse subula, lamellae absent, in section at midleaf with substereid cells centrally, adaxially and abaxially of more or less uniform thick-walled cells;</text>
      <biological_entity id="o8787" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="false" name="width" src="d0_s5" value="broad" value_original="broad" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o8788" name="subulum" name_original="subula" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="broad" value_original="broad" />
        <character is_modifier="true" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o8789" name="lamella" name_original="lamellae" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8790" name="section" name_original="section" src="d0_s5" type="structure" />
      <biological_entity id="o8791" name="midleaf" name_original="midleaf" src="d0_s5" type="structure" />
      <biological_entity id="o8792" name="bud" name_original="cells" src="d0_s5" type="structure" />
      <biological_entity id="o8793" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s5" value="uniform" value_original="uniform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <relation from="o8787" id="r2417" name="forming a" negation="false" src="d0_s5" to="o8788" />
      <relation from="o8789" id="r2418" name="in" negation="false" src="d0_s5" to="o8790" />
      <relation from="o8790" id="r2419" name="at" negation="false" src="d0_s5" to="o8791" />
      <relation from="o8791" id="r2420" name="with" negation="false" src="d0_s5" to="o8792" />
      <relation from="o8789" id="r2421" modifier="adaxially; abaxially" name="part_of" negation="false" src="d0_s5" to="o8793" />
    </statement>
    <statement id="d0_s6">
      <text>margins plane to weakly tubulose, never recurved;</text>
      <biological_entity id="o8794" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s6" to="weakly tubulose" />
        <character is_modifier="false" modifier="never" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminal cells roundedquadrate to short-rectangular, 1-stratose at base, becoming 2-stratose distally, multistratose in four layers when filling the fleshy subula.</text>
      <biological_entity id="o8796" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o8797" name="layer" name_original="layers" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction absent.</text>
      <biological_entity constraint="laminal" id="o8795" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="roundedquadrate" name="shape" src="d0_s7" to="short-rectangular" />
        <character constraint="at base" constraintid="o8796" is_modifier="false" name="architecture" src="d0_s7" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" modifier="becoming; distally" name="architecture" notes="" src="d0_s7" value="2-stratose" value_original="2-stratose" />
        <character constraint="in layers" constraintid="o8797" is_modifier="false" name="architecture" src="d0_s7" value="multistratose" value_original="multistratose" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Perichaetial leaves scarcely differentiated from stem-leaves, not convolute-sheathing.</text>
      <biological_entity constraint="perichaetial" id="o8798" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character constraint="from stem-leaves" constraintid="o8799" is_modifier="false" modifier="scarcely" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" notes="" src="d0_s9" value="convolute-sheathing" value_original="convolute-sheathing" />
      </biological_entity>
      <biological_entity id="o8799" name="stem-leaf" name_original="stem-leaves" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Pseudopodium as an elongate gametophytic stalk not developed.</text>
      <biological_entity id="o8800" name="pseudopodium" name_original="pseudopodium" src="d0_s10" type="structure" />
      <biological_entity id="o8801" name="gametophytic" name_original="gametophytic" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="not" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o8802" name="stalk" name_original="stalk" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="not" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
      <relation from="o8800" id="r2422" name="as" negation="false" src="d0_s10" to="o8801" />
      <relation from="o8800" id="r2423" name="as" negation="false" src="d0_s10" to="o8802" />
    </statement>
    <statement id="d0_s11">
      <text>Sporophytes terminal on a short and broad seta.</text>
      <biological_entity id="o8803" name="sporophyte" name_original="sporophytes" src="d0_s11" type="structure">
        <character constraint="on seta" constraintid="o8804" is_modifier="false" name="position_or_structure_subtype" src="d0_s11" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o8804" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s11" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule erect, rounded angular-ovate (trullate), broadest toward the base, opening irregularly during period of dehiscence by 4–8 lateral longitudinal valves connected at the apex;</text>
      <biological_entity id="o8805" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="angular-ovate" value_original="angular-ovate" />
        <character constraint="toward base" constraintid="o8806" is_modifier="false" name="width" src="d0_s12" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o8806" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o8807" name="period" name_original="period" src="d0_s12" type="structure" />
      <biological_entity constraint="lateral" id="o8808" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="true" name="character" src="d0_s12" value="dehiscence" value_original="dehiscence" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s12" to="8" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="longitudinal" value_original="longitudinal" />
        <character constraint="at apex" constraintid="o8809" is_modifier="false" name="fusion" src="d0_s12" value="connected" value_original="connected" />
      </biological_entity>
      <biological_entity id="o8809" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <relation from="o8805" id="r2424" name="opening" negation="false" src="d0_s12" to="o8807" />
      <relation from="o8805" id="r2425" name="consist_of" negation="false" src="d0_s12" to="o8808" />
    </statement>
    <statement id="d0_s13">
      <text>stomata, annulus, operculum, and peristome absent.</text>
      <biological_entity id="o8810" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8811" name="annulus" name_original="annulus" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8812" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8813" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Calyptra large, enveloping the whole capsule, mitrate, becoming cucullate on dehiscence, persistent.</text>
      <biological_entity id="o8814" name="calyptra" name_original="calyptra" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="large" value_original="large" />
        <character is_modifier="false" name="shape" src="d0_s14" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" modifier="becoming" name="dehiscence" src="d0_s14" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o8815" name="capsule" name_original="capsule" src="d0_s14" type="structure" />
      <relation from="o8814" id="r2426" name="enveloping the whole" negation="false" src="d0_s14" to="o8815" />
    </statement>
    <statement id="d0_s15">
      <text>Spores chlorophyllose or aborted, the former spheric to ovoid, relatively large, (50–) 90–100 (–120) µm, papillose or reticulate-papillose.</text>
      <biological_entity id="o8816" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character is_modifier="false" name="growth_order" src="d0_s15" value="former" value_original="former" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s15" to="ovoid" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s15" value="large" value_original="large" />
        <character char_type="range_value" from="50" from_unit="um" name="atypical_some_measurement" src="d0_s15" to="90" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s15" to="120" to_unit="um" />
        <character char_type="range_value" from="90" from_unit="um" name="some_measurement" src="d0_s15" to="100" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s15" value="reticulate-papillose" value_original="reticulate-papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America in arctic and subarctic areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America in arctic and subarctic areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Genus 1, species 1.</discussion>
  <discussion>Unlike the case with the similar Andreaeaceae, in Andreaeobryaceae the elevating stalk supporting the capsule is derived from sporophytic (diploid) rather than gametophytic (haploid) tissue. The foot of the seta is embedded in the vaginula at the stem apex, as generally occurs in mosses of the Bryales. Sporogenous tissue and the columella are derived from the endothecium, uniting the family with that of the Andreaeaceae in this respect, and not with the Sphagnaceae, where the spore sac derives from the amphithecium. The capsule is also unlike that of the Andreaeaceae, which is clearly elliptic, not angular-ovate. The family is also similar to Andreaea in the presence of thallose protonematal appendages. The axillary hairs at the base of the leaves have terminal cells that are beaked, as in the genus Takakia. The leaves are not particularly fragile, but fragmentation may prove a factor in asexual reproduction for the species. The following is a modification of B. M. Murray’s (1987) treatment.</discussion>
  
</bio:treatment>