<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">624</other_info_on_meta>
    <other_info_on_meta type="mention_page">625</other_info_on_meta>
    <other_info_on_meta type="mention_page">626</other_info_on_meta>
    <other_info_on_meta type="treatment_page">627</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="genus">syntrichia</taxon_name>
    <taxon_name authority="(Hedwig) F. Weber &amp; D. Mohr" date="1803" rank="species">ruralis</taxon_name>
    <place_of_publication>
      <publication_title>Index Mus. Pl. Crypt.,</publication_title>
      <place_in_publication>[2]. 1803,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus syntrichia;species ruralis</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002110</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">ruralis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>121. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Barbula;species ruralis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Syntrichia</taxon_name>
    <taxon_name authority="(Bescherelle) Dixon" date="unknown" rank="species">ruraliformis</taxon_name>
    <taxon_hierarchy>genus Syntrichia;species ruraliformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="(Bescherelle) W. Ingham" date="unknown" rank="species">ruraliformis</taxon_name>
    <taxon_hierarchy>genus Tortula;species ruraliformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="(Hedwig) P. Gaertner, B. Meyer &amp; Scherbius" date="unknown" rank="species">ruralis</taxon_name>
    <taxon_hierarchy>genus Tortula;species ruralis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–15 mm.</text>
      <biological_entity id="o745" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s0" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves clasping at base, infolded and twisted around the stem when dry, widespreading (in smaller forms) to squarrose-recurved when moist, lingulate-ovate, 1.5–3.5 × 0.75–1.25 mm, canaliculate to keeled;</text>
      <biological_entity id="o746" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o747" is_modifier="false" name="architecture_or_fixation" src="d0_s1" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" notes="" src="d0_s1" value="infolded" value_original="infolded" />
        <character constraint="around stem" constraintid="o748" is_modifier="false" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o747" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o748" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character char_type="range_value" from="widespreading" modifier="when moist" name="orientation" src="d0_s1" to="squarrose-recurved" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lingulate-ovate" value_original="lingulate-ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s1" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.75" from_unit="mm" name="width" src="d0_s1" to="1.25" to_unit="mm" />
        <character char_type="range_value" from="canaliculate" name="shape" src="d0_s1" to="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins tightly revolute in the proximal 7/8 or more, entire;</text>
      <biological_entity id="o749" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="in proximal 7/8" constraintid="o750" is_modifier="false" modifier="tightly" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o750" name="7/8" name_original="7/8" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apices emarginate to acute;</text>
      <biological_entity id="o751" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="emarginate" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa excurrent into a serrate (or occasionally only faintly serrulate), hyaline awn that often red or sometimes broadly hyaline at base, weakly to strongly papillose on the abaxial surface and often serrate near the apex because of projecting cell ends, redbrown;</text>
      <biological_entity id="o752" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="into awn" constraintid="o753" is_modifier="false" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
        <character constraint="at base" constraintid="o754" is_modifier="false" modifier="sometimes broadly; sometimes broadly" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character constraint="on abaxial surface" constraintid="o755" is_modifier="false" modifier="weakly to strongly" name="relief" notes="" src="d0_s4" value="papillose" value_original="papillose" />
        <character constraint="near apex" constraintid="o756" is_modifier="false" modifier="often" name="architecture_or_shape" notes="" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity id="o753" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o754" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o755" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o756" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity constraint="cell" id="o757" name="end" name_original="ends" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="projecting" value_original="projecting" />
      </biological_entity>
      <relation from="o756" id="r186" name="because of" negation="false" src="d0_s4" to="o757" />
    </statement>
    <statement id="d0_s5">
      <text>basal-cells abruptly differentiated, narrowly rectangular, 35–70 (–90) × 11–18 µm, quadrate to narrowly rectangular at the margins;</text>
      <biological_entity id="o758" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s5" to="90" to_unit="um" />
        <character char_type="range_value" from="35" from_unit="um" name="length" src="d0_s5" to="70" to_unit="um" />
        <character char_type="range_value" from="11" from_unit="um" name="width" src="d0_s5" to="18" to_unit="um" />
        <character char_type="range_value" constraint="at margins" constraintid="o759" from="quadrate" name="shape" src="d0_s5" to="narrowly rectangular" />
      </biological_entity>
      <biological_entity id="o759" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal cells quadrate to polygonal, 8–12 µm, with 3–6 papillae per cell, bulging, somewhat obscure.</text>
      <biological_entity id="o761" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o762" name="cell" name_original="cell" src="d0_s6" type="structure" />
      <relation from="o760" id="r187" name="with" negation="false" src="d0_s6" to="o761" />
      <relation from="o761" id="r188" name="per" negation="false" src="d0_s6" to="o762" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="distal" id="o760" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s6" to="polygonal" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s6" to="12" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_shape" notes="" src="d0_s6" value="bulging" value_original="bulging" />
        <character is_modifier="false" modifier="somewhat" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta red, 5–10 mm.</text>
      <biological_entity id="o763" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule redbrown, 2–3.5 mm, straight, with an abrupt neck;</text>
      <biological_entity id="o764" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o765" name="neck" name_original="neck" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="abrupt" value_original="abrupt" />
      </biological_entity>
      <relation from="o764" id="r189" name="with" negation="false" src="d0_s10" to="o765" />
    </statement>
    <statement id="d0_s11">
      <text>operculum 1.25–1.75 mm, brown;</text>
      <biological_entity id="o766" name="operculum" name_original="operculum" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.25" from_unit="mm" name="some_measurement" src="d0_s11" to="1.75" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome ca. 1.25 mm, the upper divisions twisted ca. 2 turns, red, the basal membrane white, about 1/3 the total length.</text>
      <biological_entity id="o767" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.25" value_original="1.25" />
      </biological_entity>
      <biological_entity constraint="upper" id="o768" name="division" name_original="divisions" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="twisted" value_original="twisted" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="basal" id="o769" name="membrane" name_original="membrane" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character name="length" src="d0_s12" value="1/3" value_original="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 11–15 µm, papillose.</text>
      <biological_entity id="o770" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s13" to="15" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to moist soil and rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to moist soil" />
        <character name="habitat" value="dry to moist rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr. (Nfld.), N.S., Ont.; Alaska, Ariz., Calif., Colo., Idaho, Maine, Mass., Mich., Mont., Mo., Nev., N.Y., Okla., Oreg., S.Dak., Tex., Utah, Wash., Wyo.; Mexico; s South America; Eurasia; s Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion>Syntrichia ruralis generally has conspicuously squarrose-recurved leaves when wet, with margins recurved nearly to the apex, distal portions of the costa toothed abaxially because of projecting cell ends, and relatively small laminal cells. The decurrent, hyaline base of the awn sometimes used to distinguish S. ruraliformis is not reliable and can, on occasion, be found in S. princeps, S. papillosissima, and S. norvegica. Specific distinctions in the S. ruralis complex are subtle, for the most part, requiring cross sections of leaves and stems, and careful measurements. The leaves of S. princeps and S. obtusissima are narrowed near the middle, whereas those of S. papillosissima, S. norvegica, and S. ruralis are widest about one-third the way up from the base and then taper to the apex. The stem of S. princeps and S. obtusissima has a strong central strand of thinner-walled cells, the costa has a group of hydroids just abaxial to the guide cells, and the basal cells in the leaf are relatively wide. In S. papillosissima, S. norvegica, and S. ruralis, the stem lacks a central strand, the costa lacks hydroids, and the basal cells in the leaf are relatively narrow.</discussion>
  
</bio:treatment>