<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">607</other_info_on_meta>
    <other_info_on_meta type="mention_page">609</other_info_on_meta>
    <other_info_on_meta type="treatment_page">608</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Juratzka" date="1882" rank="genus">pterygoneurum</taxon_name>
    <taxon_name authority="(Bridel) Juratzka" date="1882" rank="species">subsessile</taxon_name>
    <place_of_publication>
      <publication_title>Laubm.-Fl. Oesterr.-Ung.,</publication_title>
      <place_in_publication>96. 1882,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus pterygoneurum;species subsessile</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002102</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnostomum</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">subsessile</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Recent., suppl.</publication_title>
      <place_in_publication>1: 35. 1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gymnostomum;species subsessile;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves with distal lamina smooth;</text>
      <biological_entity id="o7527" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="distal" id="o7528" name="lamina" name_original="lamina" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s0" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o7527" id="r1792" name="with" negation="false" src="d0_s0" to="o7528" />
    </statement>
    <statement id="d0_s1">
      <text>awn smooth or sharply serrulate;</text>
      <biological_entity id="o7529" name="awn" name_original="awn" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sharply" name="architecture" src="d0_s1" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lamellae 10–12 cells in height, not lobed, sometimes bearing filaments.</text>
      <biological_entity id="o7530" name="lamella" name_original="lamellae" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="12" />
      </biological_entity>
      <biological_entity id="o7531" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity id="o7532" name="bearing" name_original="bearing" src="d0_s2" type="structure">
        <character is_modifier="true" name="character" src="d0_s2" value="height" value_original="height" />
        <character is_modifier="true" modifier="not" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o7533" name="filament" name_original="filaments" src="d0_s2" type="structure">
        <character is_modifier="true" name="character" src="d0_s2" value="height" value_original="height" />
        <character is_modifier="true" modifier="not" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <relation from="o7531" id="r1793" name="in" negation="false" src="d0_s2" to="o7532" />
      <relation from="o7531" id="r1794" name="in" negation="false" src="d0_s2" to="o7533" />
    </statement>
    <statement id="d0_s3">
      <text>Capsule stegocarpous (or stegocarpous but bursting irregularly), immersed to emergent, short-ovoid, annulus present, operculum cells in straight rows;</text>
      <biological_entity id="o7534" name="capsule" name_original="capsule" src="d0_s3" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s3" value="stegocarpous" value_original="stegocarpous" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="location" src="d0_s3" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="short-ovoid" value_original="short-ovoid" />
      </biological_entity>
      <biological_entity id="o7535" name="annulus" name_original="annulus" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7537" name="row" name_original="rows" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o7536" id="r1795" name="in" negation="false" src="d0_s3" to="o7537" />
    </statement>
    <statement id="d0_s4">
      <text>eperistomate.</text>
      <biological_entity constraint="operculum" id="o7536" name="cell" name_original="cells" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Calyptra mitrate.</text>
      <biological_entity id="o7538" name="calyptra" name_original="calyptra" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="mitrate" value_original="mitrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North Temperate Zone, s South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North Temperate Zone" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Pterygoneurum subsessile is an abundant moss in the arid West, often occurring with P. ovatum. In some specimens, the perigoniate plants appear separate, but this species and doubtless others are apparently occasionally rhizautoicous. Following the reasoning of R. T. Wareham (1939), var. henrici is placed with the typical variety. The characters associated with P. californicum (H. A. Crum 1967) are poor: the spores are finely papillose, the leaf cells do have weak collenchymatous thickenings, and the calyptra is long-mitrate.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsule dehiscent by the fallen operculum</description>
      <determination>3a Pterygoneurum subsessile var. subsessile</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsule bursting irregularly, annulus differentiated but not fissile</description>
      <determination>3b Pterygoneurum subsessile var. kieneri</determination>
    </key_statement>
  </key>
</bio:treatment>