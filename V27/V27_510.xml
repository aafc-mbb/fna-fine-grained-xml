<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">365</other_info_on_meta>
    <other_info_on_meta type="illustration_page">364</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Cardot" date="1908" rank="genus">campylopodiella</taxon_name>
    <taxon_name authority="(Müller Hal.) Frahm &amp; Isoviita" date="1988" rank="species">flagellacea</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>37: 968. 1988,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus campylopodiella;species flagellacea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075576</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">flagellaceum</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Frond.</publication_title>
      <place_in_publication>2: 597. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species flagellaceum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atractylocarpus</taxon_name>
    <taxon_name authority="(Müller Hal.) Williams" date="unknown" rank="species">flagellaceus</taxon_name>
    <taxon_hierarchy>genus Atractylocarpus;species flagellaceus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, yellowish green, in tufts.</text>
      <biological_entity id="o3349" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3350" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o3349" id="r762" name="in" negation="false" src="d0_s0" to="o3350" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–15 mm, radiculose.</text>
      <biological_entity id="o3351" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-patent when wet, appressed when dry, 1.8–2.3 mm, 5–8 times as long as wide, lanceolate, gradually contracted, margins entire, only slightly denticulate at the extreme apex;</text>
      <biological_entity id="o3352" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when wet" name="orientation" src="d0_s2" value="erect-patent" value_original="erect-patent" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s2" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="5-8" value_original="5-8" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="gradually" name="condition_or_size" src="d0_s2" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o3353" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="at extreme apex" constraintid="o3354" is_modifier="false" modifier="only slightly" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="extreme" id="o3354" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>costa filling 1/2–2/3 of the leaf base, 155 µm wide and indistinctly delimited at base, filling most parts of the leaf apex, excurrent, in transverse-section with large adaxial and abaxial hyalocysts, a median band of stereids and 2–4 stereids adaxially;</text>
      <biological_entity id="o3355" name="costa" name_original="costa" src="d0_s3" type="structure" constraint="apex" constraint_original="apex; apex">
        <character char_type="range_value" constraint="of leaf base" constraintid="o3356" from="1/2" name="quantity" src="d0_s3" to="2/3" />
        <character name="width" notes="" src="d0_s3" unit="um" value="155" value_original="155" />
        <character constraint="at base" constraintid="o3357" is_modifier="false" modifier="indistinctly" name="prominence" src="d0_s3" value="delimited" value_original="delimited" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o3356" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o3357" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o3358" name="part" name_original="parts" src="d0_s3" type="structure" />
      <biological_entity constraint="leaf" id="o3359" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o3360" name="transverse-section" name_original="transverse-section" src="d0_s3" type="structure" />
      <biological_entity constraint="and adaxial abaxial" id="o3361" name="hyalocyst" name_original="hyalocysts" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="large" value_original="large" />
      </biological_entity>
      <biological_entity constraint="median" id="o3362" name="band" name_original="band" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o3363" name="stereid" name_original="stereids" src="d0_s3" type="structure" />
      <relation from="o3355" id="r763" name="filling most" negation="false" src="d0_s3" to="o3358" />
      <relation from="o3355" id="r764" name="part_of" negation="false" src="d0_s3" to="o3359" />
      <relation from="o3355" id="r765" name="in" negation="false" src="d0_s3" to="o3360" />
      <relation from="o3360" id="r766" name="with" negation="false" src="d0_s3" to="o3361" />
      <relation from="o3362" id="r767" name="part_of" negation="false" src="d0_s3" to="o3363" />
    </statement>
    <statement id="d0_s4">
      <text>alar cells hyaline or brownish, often 2-stratose, slightly inflated;</text>
      <biological_entity id="o3364" name="stereid" name_original="stereids" src="d0_s3" type="structure" />
      <biological_entity id="o3365" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s4" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells in about 6 rows, rectangular, 15–40 × 10–16 µm, narrower at margins;</text>
      <biological_entity constraint="basal laminal" id="o3366" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s5" to="40" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s5" to="16" to_unit="um" />
        <character constraint="at margins" constraintid="o3368" is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o3367" name="row" name_original="rows" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o3368" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <relation from="o3366" id="r768" name="in" negation="false" src="d0_s5" to="o3367" />
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells rectangular, 13–30 × 4–12 µm.</text>
      <biological_entity constraint="distal laminal" id="o3369" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="13" from_unit="um" name="length" src="d0_s6" to="30" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="width" src="d0_s6" to="12" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, earth covered rocks, bases of trees, moderate elevations (300-500 m)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="earth" />
        <character name="habitat" value="rocks" modifier="covered" />
        <character name="habitat" value="bases" constraint="of trees , moderate elevations ( 300-500 m )" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="moderate elevations" />
        <character name="habitat" value="300-500 m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico, Central America; South America (Andes).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (Andes)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Sporophytes of Campylopodiella flagellacea have been found only once, in South America. The only North American record was collected on a seeping roadside rock in Trinity County (Shevock 17741, BONN). That specimen differs from Central American specimens in its more compact tufts and the lack of flagelliferous branches. The species was earlier regarded as a sterile flagelliferous expression of C. stenocarpa by M. Padberg and J.-P. Frahm (1985), but later was treated as separate by Frahm (1991). It differs from C. stenocarpa in its sterile condition, shorter leaves, narrower lamina, and more distinct alar cells.</discussion>
  
</bio:treatment>