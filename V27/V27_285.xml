<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="mention_page">221</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="Poelt" date="1953" rank="species">boreale</taxon_name>
    <place_of_publication>
      <publication_title>Svensk Bot. Tidskr.</publication_title>
      <place_in_publication>47: 256, fig. 1b, e. 1953,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species boreale</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075493</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in open tufts or mats, dull black, often yellowish green distally, usually purplish when wet, especially in older portions of stems.</text>
      <biological_entity id="o2752" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reflectance" notes="" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="when wet" name="coloration" src="d0_s0" value="purplish" value_original="purplish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2753" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o2754" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <biological_entity id="o2755" name="portion" name_original="portions" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="older" value_original="older" />
      </biological_entity>
      <biological_entity id="o2756" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <relation from="o2752" id="r750" name="in" negation="false" src="d0_s0" to="o2753" />
      <relation from="o2752" id="r751" name="in" negation="false" src="d0_s0" to="o2754" />
      <relation from="o2752" id="r752" modifier="especially" name="in" negation="false" src="d0_s0" to="o2755" />
      <relation from="o2755" id="r753" name="part_of" negation="false" src="d0_s0" to="o2756" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.5–10 cm, central strand absent or indistinct.</text>
      <biological_entity id="o2757" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o2758" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s1" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect proximally, usually slightly curved distally, rarely somewhat secund when dry, ovatelanceolate, sharply keeled distally, 1.6–2.5 mm, 1-stratose;</text>
      <biological_entity id="o2759" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually slightly; distally" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="sharply; distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins usually recurved throughout, denticulate distally, 1-stratose or 2-stratose;</text>
      <biological_entity id="o2760" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually; throughout" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apices acute;</text>
      <biological_entity id="o2761" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent or excurrent as a denticulate, occasionally weakly decurrent awn, abaxial surface papillose;</text>
      <biological_entity id="o2762" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character constraint="as awn" constraintid="o2763" is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o2763" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="true" modifier="occasionally weakly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2764" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate or oblate, often trigonous;</text>
      <biological_entity constraint="basal marginal" id="o2765" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblate" value_original="oblate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells mostly short-rectangular or quadrate, 8–10 µm wide, papillose, strongly sinuose with reddish or orange walls.</text>
      <biological_entity id="o2767" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o2766" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s7" to="10" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
        <character constraint="with walls" constraintid="o2767" is_modifier="false" modifier="strongly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule redbrown, sometimes orangebrown, ovoid-cylindric, narrowed to the mouth, 0.7–1.25 mm, rarely striate when old;</text>
      <biological_entity id="o2768" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="orangebrown" value_original="orangebrown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character constraint="to mouth" constraintid="o2769" is_modifier="false" name="shape" src="d0_s9" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.25" to_unit="mm" />
        <character is_modifier="false" modifier="when old" name="coloration_or_pubescence_or_relief" src="d0_s9" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o2769" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells mostly isodiametric, mainly more or less quadrate, usually mixed with a few short-elongate or oblate cells, thin-walled, with small trigones;</text>
      <biological_entity constraint="exothecial" id="o2770" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" modifier="mainly more or less" name="shape" src="d0_s10" value="quadrate" value_original="quadrate" />
        <character constraint="with cells" constraintid="o2771" is_modifier="false" modifier="usually" name="arrangement" src="d0_s10" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o2771" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="true" name="shape" src="d0_s10" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="true" name="shape" src="d0_s10" value="oblate" value_original="oblate" />
      </biological_entity>
      <biological_entity id="o2772" name="trigone" name_original="trigones" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
      </biological_entity>
      <relation from="o2770" id="r754" name="with" negation="false" src="d0_s10" to="o2772" />
    </statement>
    <statement id="d0_s11">
      <text>stomata present;</text>
      <biological_entity id="o2773" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome patent or erect, dark-red or red, 190–380 µm, densely papillose, entire or slightly perforated.</text>
      <biological_entity id="o2774" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="patent" value_original="patent" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-red" value_original="dark-red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character char_type="range_value" from="190" from_unit="um" name="some_measurement" src="d0_s12" to="380" to_unit="um" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s12" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 10–14 µm, granulose.</text>
      <biological_entity id="o2775" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s13" to="14" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="granulose" value_original="granulose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Somewhat shaded granite or limestone ledges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granite" modifier="somewhat shaded" />
        <character name="habitat" value="limestone ledges" />
        <character name="habitat" value="shaded" modifier="somewhat" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Ont., Que., Sask.; Alaska, Colo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Schistidium boreale is one of the five species of the genus in North America with papillose distal laminal cells. The combination of its dull black color, usually turning purplish when wet, reddish or orange laminal cell walls, and ovoid-cylindrical capsules that are narrowed at the mouth separates this species from the other papillose taxa. Schistidium papillosum is usually olivaceous and lacks colored cell walls; S. strictum is usually a dull red-brown, has cupulate capsules, and appears to be restricted to northwestern, coastal areas, and S. frisvollianum, an arctic species, is more strongly ornamented. None of these three taxa turns purple when wet. Schistidium maritimum occasionally has papillose distal laminal cells. That species, however, is restricted to coastal areas, its leaves are ovate-lanceolate to linear-lanceolate with 2-stratose distal laminae, and its costa has one or two well-developed stereid bands, which are absent in all other North American species of the genus.</discussion>
  
</bio:treatment>