<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">560</other_info_on_meta>
    <other_info_on_meta type="mention_page">578</other_info_on_meta>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
    <other_info_on_meta type="illustration_page">487</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Schimper) Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">trichostomoideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1846" rank="genus">eucladium</taxon_name>
    <taxon_name authority="(Bridel) Bruch &amp; Schimper" date="1846" rank="species">verticillatum</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>1: 93. 1846,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily trichostomoideae;genus eucladium;species verticillatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001161</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Weissia</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">verticillata</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Schrader)</publication_title>
      <place_in_publication>1800(1): 283. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Weissia;species verticillata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 0.7–6 cm, leaves often branched in whorls in successive innovations.</text>
      <biological_entity id="o5317" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5318" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="in whorls" constraintid="o5319" is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o5319" name="whorl" name_original="whorls" src="d0_s0" type="structure" />
      <biological_entity id="o5320" name="innovation" name_original="innovations" src="d0_s0" type="structure">
        <character is_modifier="true" name="development" src="d0_s0" value="successive" value_original="successive" />
      </biological_entity>
      <relation from="o5319" id="r1237" name="in" negation="false" src="d0_s0" to="o5320" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 1.2–2.5 mm, flat to channeled distally or at mid leaf, in transverse-section at mid leaf often tapering from costa to leaf margin because of decreasing size of cells, mucro occasionally ending in a sharp, clear cell;</text>
      <biological_entity id="o5321" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s1" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="flat" modifier="distally" name="shape" src="d0_s1" to="channeled" />
        <character is_modifier="false" name="shape" src="d0_s1" value="at mid leaf" value_original="at mid leaf" />
      </biological_entity>
      <biological_entity constraint="mid" id="o5322" name="leaf" name_original="leaf" src="d0_s1" type="structure" />
      <biological_entity id="o5323" name="transverse-section" name_original="transverse-section" src="d0_s1" type="structure" />
      <biological_entity constraint="mid" id="o5324" name="leaf" name_original="leaf" src="d0_s1" type="structure">
        <character constraint="from costa" constraintid="o5325" is_modifier="false" modifier="often" name="shape" src="d0_s1" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o5325" name="costa" name_original="costa" src="d0_s1" type="structure" />
      <biological_entity constraint="leaf" id="o5326" name="margin" name_original="margin" src="d0_s1" type="structure" />
      <biological_entity id="o5327" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="decreasing" value_original="decreasing" />
      </biological_entity>
      <biological_entity id="o5328" name="mucro" name_original="mucro" src="d0_s1" type="structure" />
      <biological_entity id="o5329" name="cell" name_original="cell" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="sharp" value_original="sharp" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="clear" value_original="clear" />
      </biological_entity>
      <relation from="o5321" id="r1238" name="at" negation="false" src="d0_s1" to="o5322" />
      <relation from="o5321" id="r1239" name="in" negation="false" src="d0_s1" to="o5323" />
      <relation from="o5323" id="r1240" name="at" negation="false" src="d0_s1" to="o5324" />
      <relation from="o5325" id="r1241" name="to" negation="false" src="d0_s1" to="o5326" />
      <relation from="o5326" id="r1242" name="because of" negation="false" src="d0_s1" to="o5327" />
      <relation from="o5328" id="r1243" name="ending in a" negation="false" src="d0_s1" to="o5329" />
    </statement>
    <statement id="d0_s2">
      <text>marginal serrulations projecting from the distal end of cells of the hyaline cells of the leaf base;</text>
      <biological_entity constraint="marginal" id="o5330" name="serrulation" name_original="serrulations" src="d0_s2" type="structure">
        <character constraint="from distal end" constraintid="o5331" is_modifier="false" name="orientation" src="d0_s2" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="distal" id="o5331" name="end" name_original="end" src="d0_s2" type="structure" />
      <biological_entity id="o5332" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity id="o5333" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o5334" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o5331" id="r1244" name="part_of" negation="false" src="d0_s2" to="o5332" />
      <relation from="o5331" id="r1245" name="part_of" negation="false" src="d0_s2" to="o5333" />
      <relation from="o5331" id="r1246" name="part_of" negation="false" src="d0_s2" to="o5334" />
    </statement>
    <statement id="d0_s3">
      <text>costa to 1/3 or more the width of the leaf near the base, basal-cells 12–15 µm wide, 2–5: 1;</text>
      <biological_entity id="o5335" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity id="o5336" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o5337" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o5338" name="basal-cell" name_original="basal-cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s3" to="15" to_unit="um" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="5" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <relation from="o5336" id="r1247" name="near" negation="false" src="d0_s3" to="o5337" />
    </statement>
    <statement id="d0_s4">
      <text>median and distal laminal cells 8–10 µm wide, 1 (–2):1, walls rather thick, often irregularly so, large juxtacostally and decreasing in size to the leaf margin, irregular in shape from quadrate to rectangular, 1–2 (–3):1, sometimes transversely elongate 2: 1, occasionally with transverse walls oblique, marginal cells narrower in places and 2–3: 1 occasionally approximating a border;</text>
      <biological_entity constraint="median and distal laminal" id="o5339" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s4" to="10" to_unit="um" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="2" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5340" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rather" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="often irregularly; irregularly; juxtacostally" name="size" src="d0_s4" value="large" value_original="large" />
        <character constraint="to leaf margin" constraintid="o5341" is_modifier="false" name="size" src="d0_s4" value="decreasing" value_original="decreasing" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="irregular" value_original="irregular" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="rectangular" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" modifier="sometimes transversely" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o5341" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o5342" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s4" value="transverse" value_original="transverse" />
        <character is_modifier="false" name="orientation_or_shape" src="d0_s4" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o5343" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character constraint="in places" constraintid="o5344" is_modifier="false" name="width" src="d0_s4" value="narrower" value_original="narrower" />
        <character modifier="occasionally" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5344" name="place" name_original="places" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o5345" name="border" name_original="border" src="d0_s4" type="structure" />
      <relation from="o5340" id="r1248" modifier="occasionally" name="with" negation="false" src="d0_s4" to="o5342" />
      <relation from="o5343" id="r1249" name="approximating a" negation="false" src="d0_s4" to="o5345" />
    </statement>
    <statement id="d0_s5">
      <text>papillae variously scattered or centered.</text>
      <biological_entity constraint="median and distal laminal" id="o5346" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s5" to="10" to_unit="um" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o5347" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="variously" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="position" src="d0_s5" value="centered" value_original="centered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seta yellow to redbrown, not or little twisted.</text>
      <biological_entity id="o5348" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s6" to="redbrown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule 0.8–1.8 mm, redbrown when old;</text>
      <biological_entity id="o5349" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>operculum 0.5–0.8 mm;</text>
      <biological_entity id="o5350" name="operculum" name_original="operculum" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>peristome rudimentary or to 300 µm, yellow to orange, with a low, papillose basal membrane.</text>
      <biological_entity id="o5351" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="rudimentary" value_original="rudimentary" />
        <character name="prominence" src="d0_s9" value="0-300 um" value_original="0-300 um" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="orange" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5352" name="membrane" name_original="membrane" src="d0_s9" type="structure">
        <character is_modifier="true" name="position" src="d0_s9" value="low" value_original="low" />
        <character is_modifier="true" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
      <relation from="o5351" id="r1250" name="with" negation="false" src="d0_s9" to="o5352" />
    </statement>
    <statement id="d0_s10">
      <text>Calyptra ca. 2.5 mm.</text>
      <biological_entity id="o5353" name="calyptra" name_original="calyptra" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores pale.</text>
      <biological_entity id="o5354" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporophytes infrequent, capsules mature Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dripping calcareous or sometimes granitic rock faces or mortar, around springs, dripping bluffs in calcareous regions</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dripping calcareous" />
        <character name="habitat" value="granitic rock" modifier="sometimes" />
        <character name="habitat" value="mortar" constraint="around springs" />
        <character name="habitat" value="springs" modifier="around" />
        <character name="habitat" value="bluffs" modifier="dripping" constraint="in calcareous regions" />
        <character name="habitat" value="calcareous regions" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-1900 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Nfld. and Labr. (Nfld.), Ont.; Ala., Ariz., Ark., Calif., Colo., Fla., Idaho, Ill., Ind., Kans., Ky., Mich., Miss., Mo., Nev., N.Mex., Ohio, Okla., Tenn., Tex., Utah, Va., Wash., Wis.; Mexico; Bermuda; Central America; Europe; Asia; n, s Africa; Atlantic Islands (Macaronesia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Bermuda" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Eucladium verticillatum is associated with yearround seepage, possibly the reason that, although the stem may be reduced, the leaves are seldom reduced as they frequently are in drought-tolerant species of the family. The plants are typically a yellow-green, and there is usually a vivid contrast between the pellucid green laminal cells and the clear basal ones. Stem hyalodermis cells are frequently attached to the costa base, and form decurrencies of long and thin-walled cells in the leaf angles when leaves are removed. Gymnostomum and Molendoa are similar plants but differ by blunt apices and subpercurrent costae. Both these genera possess a stem central strand, no hyalodermis, and occasional 2-stratose areas in the lamina. Eucladium may be separated from these taxa and others that may be mistaken for it, such as Hymenostylium and Anoectangium by its highly differentiated bulging or lax, thin-walled basal cells. Assurances in the literature to the contrary, many other taxa also possess serrulate or denticulate leaf-shoulder margins, especially Hymenostylium, while Eucladium may rarely lack such serrulations. Specimens of Eucladium in which the lamina is highly reduced so that the leaves seem entirely costate are included without special rank in the range of variation of the species in North America. The laminal papillae of Hymenostylium are clear, sharp, well-defined; those of Eucladium are low, amorphous or scablike on the lumen surface.</discussion>
  
</bio:treatment>