<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gary L. Smith Merrill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="treatment_page">145</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Bridel" date="1827" rank="genus">PSILOPILUM</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>2: 95. 1827  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus PSILOPILUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek psilos, naked, and pilon, hair, alluding to calyptra</other_info_on_name>
    <other_info_on_name type="fna_id">127351</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, not polytrichoid, gregarious, often in extensive colonies on soil.</text>
      <biological_entity id="o7478" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="polytrichoid" value_original="polytrichoid" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7479" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="extensive" value_original="extensive" />
      </biological_entity>
      <biological_entity id="o7480" name="soil" name_original="soil" src="d0_s0" type="structure" />
      <relation from="o7478" id="r2105" modifier="often" name="in" negation="false" src="d0_s0" to="o7479" />
      <relation from="o7479" id="r2106" name="on" negation="false" src="d0_s0" to="o7480" />
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched.</text>
      <biological_entity id="o7481" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves only slightly sheathing at base, distinctly concave to channeled, cucullate, the apex obtuse or apiculate;</text>
      <biological_entity id="o7482" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at base" constraintid="o7483" is_modifier="false" modifier="only slightly" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
        <character char_type="range_value" from="distinctly concave" name="shape" notes="" src="d0_s2" to="channeled" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cucullate" value_original="cucullate" />
      </biological_entity>
      <biological_entity id="o7483" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o7484" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>lamina broad and 1-stratose, margins entire or bluntly and irregularly toothed distally by projecting ends of rhomboidal marginal cells, forming an indistinct border;</text>
      <biological_entity id="o7485" name="lamina" name_original="lamina" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o7486" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s3" value="bluntly" value_original="bluntly" />
        <character constraint="by ends" constraintid="o7487" is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o7487" name="end" name_original="ends" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o7488" name="bud" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="rhomboidal" value_original="rhomboidal" />
      </biological_entity>
      <biological_entity id="o7489" name="border" name_original="border" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <relation from="o7487" id="r2107" name="part_of" negation="false" src="d0_s3" to="o7488" />
      <relation from="o7486" id="r2108" name="forming an" negation="false" src="d0_s3" to="o7489" />
    </statement>
    <statement id="d0_s4">
      <text>costa smooth at back or roughened near apex, rarely with a few low adaxial lamellae;</text>
      <biological_entity id="o7490" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="at " constraintid="o7492" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7491" name="back" name_original="back" src="d0_s4" type="structure" />
      <biological_entity id="o7492" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="relief_or_texture" src="d0_s4" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7493" name="lamella" name_original="lamellae" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="true" name="position" src="d0_s4" value="low" value_original="low" />
      </biological_entity>
      <relation from="o7490" id="r2109" modifier="rarely" name="with" negation="false" src="d0_s4" to="o7493" />
    </statement>
    <statement id="d0_s5">
      <text>lamellae restricted to costa, transversely undulate, margins entire or incised and irregularly dentate, marginal cells in section undifferentiated, smooth.</text>
      <biological_entity id="o7494" name="lamella" name_original="lamellae" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o7495" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o7496" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o7498" name="section" name_original="section" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
      <relation from="o7494" id="r2110" name="restricted to" negation="false" src="d0_s5" to="o7495" />
      <relation from="o7497" id="r2111" name="in" negation="false" src="d0_s5" to="o7498" />
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="marginal" id="o7497" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>male plants smaller, perigonia conspicuous, cupulate, bracts broadly ovate, flaring at apex;</text>
      <biological_entity id="o7499" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="male" value_original="male" />
        <character is_modifier="false" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7500" name="perigonium" name_original="perigonia" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cupulate" value_original="cupulate" />
      </biological_entity>
      <biological_entity id="o7501" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character constraint="at apex" constraintid="o7502" is_modifier="false" name="shape" src="d0_s7" value="flaring" value_original="flaring" />
      </biological_entity>
      <biological_entity id="o7502" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>perichaetial leaves erect, lanceolate, often much longer than stem-leaves.</text>
      <biological_entity constraint="perichaetial" id="o7503" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character constraint="than stem-leaves" constraintid="o7504" is_modifier="false" name="length_or_size" src="d0_s8" value="often much longer" value_original="often much longer" />
      </biological_entity>
      <biological_entity id="o7504" name="stem-leaf" name_original="stem-leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Capsule moderately to strongly curved and cylindric to gibbous and asymmetric, wrinkled when dry, not angled, tapering to base;</text>
      <biological_entity id="o7505" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="moderately to strongly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s9" to="gibbous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="when dry" name="relief" src="d0_s9" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character constraint="to base" constraintid="o7506" is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o7506" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>exothecium smooth, exothecial cells short to longitudinally elongate, often with thickened radial longitudinal walls, no thin spots or pits;</text>
      <biological_entity id="o7507" name="exothecium" name_original="exothecium" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="exothecial" id="o7508" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" modifier="longitudinally" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="quantity" notes="" src="d0_s10" value="no" value_original="no" />
        <character is_modifier="false" name="width" notes="" src="d0_s10" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o7509" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="radial" value_original="radial" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s10" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o7510" name="spot" name_original="spots" src="d0_s10" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="radial" value_original="radial" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s10" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o7511" name="pit" name_original="pits" src="d0_s10" type="structure" />
      <relation from="o7508" id="r2112" modifier="often" name="with" negation="false" src="d0_s10" to="o7509" />
      <relation from="o7508" id="r2113" modifier="often" name="with" negation="false" src="d0_s10" to="o7510" />
      <relation from="o7508" id="r2114" modifier="often" name="with" negation="false" src="d0_s10" to="o7511" />
    </statement>
    <statement id="d0_s11">
      <text>stomata superficial, at base of capsule;</text>
      <biological_entity id="o7512" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="superficial" value_original="superficial" />
      </biological_entity>
      <biological_entity id="o7513" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o7514" name="capsule" name_original="capsule" src="d0_s11" type="structure" />
      <relation from="o7512" id="r2115" name="at" negation="false" src="d0_s11" to="o7513" />
      <relation from="o7513" id="r2116" name="part_of" negation="false" src="d0_s11" to="o7514" />
    </statement>
    <statement id="d0_s12">
      <text>operculum rostrate;</text>
      <biological_entity id="o7515" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome deeply divided to below capsule rim, the teeth 32, linear, crowded, at times poorly formed and irregular in shape.</text>
      <biological_entity id="o7516" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character constraint="below capsule rim" constraintid="o7517" is_modifier="false" modifier="deeply" name="shape" src="d0_s13" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity constraint="capsule" id="o7517" name="rim" name_original="rim" src="d0_s13" type="structure" />
      <biological_entity id="o7518" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="32" value_original="32" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="poorly" name="shape" src="d0_s13" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Calyptra smooth or roughened at tip.</text>
      <biological_entity id="o7519" name="calyptra" name_original="calyptra" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character constraint="at tip" constraintid="o7520" is_modifier="false" name="relief" src="d0_s14" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o7520" name="tip" name_original="tip" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Spores large (20–25 µm), finely papillose.</text>
      <biological_entity id="o7521" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="large" value_original="large" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide in arctic and subarctic areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide in arctic and subarctic areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Psilopilum is a small genus of pioneer mosses on soil in disturbed or otherwise unstable, non-calcareous sites, in arctic and subarctic North America and Eurasia. The closest affinities of Psilopilum are with Atrichum, as indicated by the tendency towards bordered leaves (P. laevigatum), finely longitudinally striate exothecium, and a similar peristome of 32 linear, crowded teeth. Sporophytes are commonly produced in both species. The best recent treatment is that of D. G. Long (1985), who examined an extensive series of collections from arctic America.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves oblong-ovate, erect-appressed when dry, spreading when moist; leaf margins entire or minutely denticulate near apex; lamellae entire or crenulate in profile; capsule cylindric, weakly to moderately curved, not or weakly gibbous, suberect or inclined.</description>
      <determination>1 Psilopilum cavifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ovate to oblong-obovate, erect-incurved when wet or dry; leaf margins crenate-dentate distally, hyaline (decolorate); lamellae incised, coarsely and irregularly dentate in profile; capsule ovoid-gibbous, strongly curved, horizontal.</description>
      <determination>2 Psilopilum laevigatum</determination>
    </key_statement>
  </key>
</bio:treatment>