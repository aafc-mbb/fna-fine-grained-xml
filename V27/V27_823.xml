<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">573</other_info_on_meta>
    <other_info_on_meta type="illustration_page">574</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Herzog" date="1916" rank="genus">rhexophyllum</taxon_name>
    <taxon_name authority="(Mitten) Hilpert" date="1933" rank="species">subnigrum</taxon_name>
    <place_of_publication>
      <publication_title>Beih. Bot. Centralbl.</publication_title>
      <place_in_publication>50(2): 684. 1933,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus rhexophyllum;species subnigrum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075550</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">subnigra</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>12: 164. 1869</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tortula;species subnigra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose, wiry turfs.</text>
      <biological_entity id="o1490" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1491" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="wiry" value_original="wiry" />
      </biological_entity>
      <relation from="o1490" id="r352" name="in" negation="false" src="d0_s0" to="o1491" />
    </statement>
    <statement id="d0_s1">
      <text>Stems robust, often branching, without tomentum, few coarse, redbrown rhizoids at the stem base or occasionally higher and more abundant in robust plants.</text>
      <biological_entity id="o1492" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="robust" value_original="robust" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="quantity" notes="" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="relief" src="d0_s1" value="coarse" value_original="coarse" />
      </biological_entity>
      <biological_entity id="o1493" name="tomentum" name_original="tomentum" src="d0_s1" type="structure" />
      <biological_entity id="o1494" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity constraint="stem" id="o1495" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o1496" name="plant" name_original="plants" src="d0_s1" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s1" value="robust" value_original="robust" />
        <character is_modifier="true" name="quantity" src="d0_s1" value="abundant" value_original="abundant" />
      </biological_entity>
      <relation from="o1492" id="r353" name="without" negation="false" src="d0_s1" to="o1493" />
      <relation from="o1494" id="r354" name="at" negation="false" src="d0_s1" to="o1495" />
      <relation from="o1494" id="r355" modifier="at the stem base or occasionally higher and more abundant in robust plants" name="at" negation="false" src="d0_s1" to="o1496" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves 1.8–2.5 mm, at least some leaves with tattered laminae;</text>
      <biological_entity constraint="cauline" id="o1497" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1499" name="lamina" name_original="laminae" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="tattered" value_original="tattered" />
      </biological_entity>
      <relation from="o1498" id="r356" modifier="at-least" name="with" negation="false" src="d0_s2" to="o1499" />
    </statement>
    <statement id="d0_s3">
      <text>most apices present, tending to epapillose;</text>
      <biological_entity id="o1498" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1500" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal surface cells on costa roughened abaxially by distally-projecting papillae at distal ends of cells;</text>
      <biological_entity constraint="surface" id="o1501" name="cell" name_original="cells" src="d0_s4" type="structure" constraint_original="distal surface" />
      <biological_entity id="o1502" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="by papillae" constraintid="o1503" is_modifier="false" name="relief_or_texture" src="d0_s4" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o1503" name="papilla" name_original="papillae" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="distally-projecting" value_original="distally-projecting" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1504" name="end" name_original="ends" src="d0_s4" type="structure" />
      <biological_entity id="o1505" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <relation from="o1501" id="r357" name="on" negation="false" src="d0_s4" to="o1502" />
      <relation from="o1503" id="r358" name="at" negation="false" src="d0_s4" to="o1504" />
      <relation from="o1504" id="r359" name="part_of" negation="false" src="d0_s4" to="o1505" />
    </statement>
    <statement id="d0_s5">
      <text>basal-cells gradually differentiated from distal laminal cells, pale-yellow, smooth, 15–17 µm wide;</text>
      <biological_entity id="o1506" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character constraint="from distal laminal, cells" constraintid="o1507, o1508" is_modifier="false" modifier="gradually" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s5" to="17" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1507" name="laminal" name_original="laminal" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o1508" name="cell" name_original="cells" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells 7–11 µm wide.</text>
      <biological_entity constraint="distal laminal" id="o1509" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s6" to="11" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Not fertile in the flora area. Shaded rock faces or clefts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="the flora area" modifier="not fertile in" />
        <character name="habitat" value="rock faces" />
        <character name="habitat" value="clefts" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (2000-2500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="2000" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Tex.; Mexico; South America (Bolivia, Peru).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Rhexophyllum is characterized by leaves deeply keeled, lanceolate from an ovate base, squarrose-recurved above a sheathing leaf base when moist, with sharp, deep to shallow, irregular marginal teeth, 2-stratose distal lamina, occasionally splitting to the costa, and by the red color of the lamina both naturally and in 2% KOH. Material from the study area may have leaves more distant on the stem proximally, the stems appearing more thinly foliose. Sometimes the marginal teeth are pronounced, sometimes low or eroded. The lamina may be slightly to almost completely 2-stratose. The degree of fragility is also variable.</discussion>
  <discussion>Species of Leptodontium are similar in their squarrose leaves, but these are yellow, 1-stratose, not fragile, have larger distal laminal cells and fewer papillae; Leptodontium also lacks a stem central strand and has peristomate capsules, the abaxial surface of the costa is smooth, whereas it is distally roughened-papillose in Rhexophyllum. Characters of Rhexophyllum subnigrum growing in North America may depart from those expressed under optimal habitat conditions, as given in the generic description above. Plants from the American Southwest may strongly resemble species of Grimmia, which are common there and with which Rhexophyllum may grow. Rhexophyllum differs in its coppery, brick-red color, and strongly keeled leaves with irregularly thick-walled limb cells that become transversely flattened (1:</discussion>
  
</bio:treatment>