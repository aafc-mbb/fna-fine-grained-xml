<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">628</other_info_on_meta>
    <other_info_on_meta type="treatment_page">630</other_info_on_meta>
    <other_info_on_meta type="illustration_page">631</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Schimper" date="1860" rank="genus">microbryum</taxon_name>
    <taxon_name authority="(Weber &amp; D. Mohr) Schimper" date="1860" rank="species">floerkeanum</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Eur.,</publication_title>
      <place_in_publication>11. 1860,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus microbryum;species floerkeanum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075520</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phascum</taxon_name>
    <taxon_name authority="Weber &amp; D. Mohr" date="unknown" rank="species">floerkeanum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Taschenbuch,</publication_title>
      <place_in_publication>70, fig. 451. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Phascum;species floerkeanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Distal laminal cells weakly convex superficially, adaxial surface of costa not mammillose.</text>
      <biological_entity constraint="distal laminal" id="o562" name="cell" name_original="cells" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="weakly; superficially" name="shape" src="d0_s0" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o563" name="surface" name_original="surface" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s0" value="mammillose" value_original="mammillose" />
      </biological_entity>
      <biological_entity id="o564" name="costa" name_original="costa" src="d0_s0" type="structure" />
      <relation from="o563" id="r139" name="part_of" negation="false" src="d0_s0" to="o564" />
    </statement>
    <statement id="d0_s1">
      <text>Seta extremely short, nearly absent.</text>
      <biological_entity id="o565" name="seta" name_original="seta" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="extremely" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" modifier="nearly" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Capsule nearly spherical, cleistocarpous.</text>
      <biological_entity id="o566" name="capsule" name_original="capsule" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s2" value="spherical" value_original="spherical" />
        <character is_modifier="false" name="dehiscence" src="d0_s2" value="cleistocarpous" value_original="cleistocarpous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Spores papillose, 20–25 µm.</text>
      <biological_entity id="o567" name="spore" name_original="spores" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s3" to="25" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature in late fall (Oct–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="late fall" from="late fall" constraint="Oct-Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, fields, pastures</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Europe; sw Asia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>The leaves of Microbryum floerkeanum are ovate to ovate-lanceolate and sheath the immersed capsule. The papillae occur near the apex of the leaf, one per lumen, and are large, nearly covering the lumen. The calyptra is variably papillose, merely rough, or smooth. Variety badium (Bruch &amp; Schimper) Schimper is poorly distinguishable, but may be segregated by its long-acuminate leaf apices—the typical variety has short-acuminate apices.</discussion>
  
</bio:treatment>