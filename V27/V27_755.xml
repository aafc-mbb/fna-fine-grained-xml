<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">525</other_info_on_meta>
    <other_info_on_meta type="mention_page">551</other_info_on_meta>
    <other_info_on_meta type="treatment_page">526</other_info_on_meta>
    <other_info_on_meta type="illustration_page">526</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="P. C. Chen" date="1941" rank="genus">bellibarbula</taxon_name>
    <taxon_name authority="(Griffith) R. H. Zander" date="1993" rank="species">recurva</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Buffalo Soc. Nat. Sci.</publication_title>
      <place_in_publication>32: 142. 1993,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus bellibarbula;species recurva</taxon_hierarchy>
    <other_info_on_name type="fna_id">240002008</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnostomum</taxon_name>
    <taxon_name authority="Griffith" date="unknown" rank="species">recurvum</taxon_name>
    <place_of_publication>
      <publication_title>Calcutta J. Nat. Hist.</publication_title>
      <place_in_publication>2: 482. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gymnostomum;species recurvum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryoerythrophyllum</taxon_name>
    <taxon_name authority="(Griffith) K. Saito" date="unknown" rank="species">recurvum</taxon_name>
    <taxon_hierarchy>genus Bryoerythrophyllum;species recurvum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems occasionally branched in robust specimens, often exposed between subdistant leaf-bases;</text>
      <biological_entity id="o8883" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="in specimens" constraintid="o8884" is_modifier="false" modifier="occasionally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character constraint="between leaf-bases" constraintid="o8885" is_modifier="false" modifier="often" name="prominence" notes="" src="d0_s0" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity id="o8884" name="specimen" name_original="specimens" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s0" value="robust" value_original="robust" />
      </biological_entity>
      <biological_entity id="o8885" name="leaf-base" name_original="leaf-bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="subdistant" value_original="subdistant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizoids few at stem base.</text>
      <biological_entity id="o8886" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character constraint="at stem base" constraintid="o8887" is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="stem" id="o8887" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves somewhat remote on stem, 0.8–1.5 (–2) mm;</text>
      <biological_entity id="o8888" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="on stem" constraintid="o8889" is_modifier="false" modifier="somewhat" name="arrangement_or_density" src="d0_s2" value="remote" value_original="remote" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8889" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margin variously broadly channeled to strongly or weakly recurved in proximal 3/4;</text>
      <biological_entity id="o8890" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="variously broadly" name="shape" src="d0_s3" value="channeled" value_original="channeled" />
        <character constraint="in proximal 3/4" constraintid="o8891" is_modifier="false" modifier="weakly; weakly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8891" name="3/4" name_original="3/4" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apex flat or some leaves somewhat keeled, often ending in a single pellucid cell, some apices occasionally flexed sideways with the sinuose costa;</text>
      <biological_entity id="o8892" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character name="prominence_or_shape" src="d0_s4" value="some" value_original="some" />
      </biological_entity>
      <biological_entity id="o8893" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o8894" name="cell" name_original="cell" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <biological_entity id="o8895" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <biological_entity id="o8896" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="occasionally" name="shape" src="d0_s4" value="flexed" value_original="flexed" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sinuose" value_original="sinuose" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <relation from="o8893" id="r2111" modifier="often" name="ending in a" negation="false" src="d0_s4" to="o8894" />
      <relation from="o8893" id="r2112" modifier="often" name="ending in a" negation="false" src="d0_s4" to="o8895" />
      <relation from="o8893" id="r2113" modifier="often" name="ending in a" negation="false" src="d0_s4" to="o8896" />
    </statement>
    <statement id="d0_s5">
      <text>costa sinuose beyond leaf middle, rather thick, papillose on both surfaces;</text>
      <biological_entity id="o8897" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character constraint="beyond leaf" constraintid="o8898" is_modifier="false" name="architecture" src="d0_s5" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" modifier="rather" name="width" notes="" src="d0_s5" value="thick" value_original="thick" />
        <character constraint="on surfaces" constraintid="o8899" is_modifier="false" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o8898" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o8899" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 10–13 µm wide, sharply papillose.</text>
      <biological_entity constraint="laminal" id="o8900" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s6" to="13" to_unit="um" />
        <character is_modifier="false" modifier="sharply" name="relief" src="d0_s6" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perigonia gemmate, at the tips of stems or terminating innovations in series along a developing stem, leaves larger than stem-leaves, broadly ovatelanceolate and clasping the antheridia.</text>
      <biological_entity id="o8901" name="perigonium" name_original="perigonia" src="d0_s7" type="structure" />
      <biological_entity id="o8902" name="tip" name_original="tips" src="d0_s7" type="structure" />
      <biological_entity id="o8903" name="stem" name_original="stems" src="d0_s7" type="structure" />
      <biological_entity id="o8904" name="innovation" name_original="innovations" src="d0_s7" type="structure" />
      <biological_entity id="o8905" name="series" name_original="series" src="d0_s7" type="structure" />
      <biological_entity id="o8906" name="stem" name_original="stem" src="d0_s7" type="structure">
        <character is_modifier="true" name="development" src="d0_s7" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity id="o8907" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="than stem-leaves" constraintid="o8908" is_modifier="false" name="size" src="d0_s7" value="larger" value_original="larger" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity id="o8908" name="stem-leaf" name_original="stem-leaves" src="d0_s7" type="structure" />
      <biological_entity id="o8909" name="antheridium" name_original="antheridia" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s7" value="clasping" value_original="clasping" />
      </biological_entity>
      <relation from="o8901" id="r2114" name="at" negation="false" src="d0_s7" to="o8902" />
      <relation from="o8902" id="r2115" name="part_of" negation="false" src="d0_s7" to="o8903" />
      <relation from="o8902" id="r2116" name="part_of" negation="false" src="d0_s7" to="o8904" />
      <relation from="o8902" id="r2117" name="in" negation="false" src="d0_s7" to="o8905" />
      <relation from="o8905" id="r2118" name="along" negation="false" src="d0_s7" to="o8906" />
    </statement>
    <statement id="d0_s8">
      <text>Perichaetiate plants and sporophytes not known in area of the flora.</text>
      <biological_entity id="o8910" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="perichaetiate" value_original="perichaetiate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8911" name="sporophyte" name_original="sporophytes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="perichaetiate" value_original="perichaetiate" />
      </biological_entity>
      <biological_entity id="o8912" name="area" name_original="area" src="d0_s8" type="structure" />
      <relation from="o8910" id="r2119" name="known in" negation="false" src="d0_s8" to="o8912" />
      <relation from="o8911" id="r2120" name="known in" negation="false" src="d0_s8" to="o8912" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thin soil over moist rock in protected mountain stations, forested gorges, coves, cliffs, along streams and by waterfalls</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thin soil" constraint="over moist rock in protected mountain stations , forested gorges , coves ," />
        <character name="habitat" value="moist rock" constraint="in protected mountain stations , forested gorges , coves ," />
        <character name="habitat" value="protected mountain stations" />
        <character name="habitat" value="forested gorges" />
        <character name="habitat" value="coves" />
        <character name="habitat" value="streams" modifier="cliffs along" />
        <character name="habitat" value="waterfalls" modifier="and by" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (600-900 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="600" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., S.C.; Mexico; South America (Bolivia); Asia (the Himalayas, s India).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="Asia (the Himalayas)" establishment_means="native" />
        <character name="distribution" value="Asia (s India)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>In the flora area Bellibarbula recurva is known only from four counties in western North Carolina, and one station in western South Carolina. One specimen examined was perigoniate; all remaining material seen was sterile. The characters that distinguish this species are the sinuous distal costa, the nearly quadrate basal cells, and the red laminal coloration. Bryoerythrophyllum ferruginascens, another species rare in the flora area and known from the Appalachians, is very similar but differs by its more elongate basal cells, propagula on the rhizoids and costa straight throughout. Didymodon vinealis, a species or species group with an almost entirely western distribution, is separated by its straight costa and a deep apical groove to the apex within which lies an epapillose costal surface with elongate cells; Bellibarbula recurva has a flattened apex with quadrate, papillose cells on the costal ventral surface.</discussion>
  
</bio:treatment>