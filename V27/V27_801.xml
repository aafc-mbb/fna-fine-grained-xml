<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">541</other_info_on_meta>
    <other_info_on_meta type="treatment_page">558</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">didymodon</taxon_name>
    <taxon_name authority="(Renauld &amp; Cardot) Brotherus in H. G. A. Engler and K. Prantl" date="1909" rank="species">maschalogena</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>234/235[I,3]: 1192. 1909,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus didymodon;species maschalogena</taxon_hierarchy>
    <other_info_on_name type="fna_id">242444263</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="Renauld &amp; Cardot" date="unknown" rank="species">maschalogena</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Roy. Bot. Belgique</publication_title>
      <place_in_publication>41(1): 53. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Barbula;species maschalogena;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="Steere" date="unknown" rank="species">michiganensis</taxon_name>
    <taxon_hierarchy>genus Barbula;species michiganensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Didymodon</taxon_name>
    <taxon_name authority="(Steere) K. Saito" date="unknown" rank="species">michiganensis</taxon_name>
    <taxon_hierarchy>genus Didymodon;species michiganensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green to glossy green.</text>
      <biological_entity id="o4950" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="glossy green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 2 cm, central strand present.</text>
      <biological_entity id="o4951" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o4952" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves catenulate-incurved when dry, spreading and weakly keeled when moist, monomorphic, ovatelanceolate, adaxially grooved in distal third, 0.9–1.1 mm, base often but not always sharply differentiated in shape, ovate, margins narrowly to broadly recurved in proximal 1/3–1/2, entire, apex narrowly acute, abruptly acuminate, not fragile;</text>
      <biological_entity id="o4953" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="catenulate-incurved" value_original="catenulate-incurved" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character constraint="in distal third" constraintid="o4954" is_modifier="false" modifier="adaxially" name="architecture" src="d0_s2" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" notes="" src="d0_s2" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4954" name="third" name_original="third" src="d0_s2" type="structure" />
      <biological_entity id="o4955" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not always sharply" name="shape" src="d0_s2" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o4956" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="in proximal 1/3-1/2" constraintid="o4957" is_modifier="false" modifier="narrowly to broadly" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4957" name="1/3-1/2" name_original="1/3-1/2" src="d0_s2" type="structure" />
      <biological_entity id="o4958" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s2" value="fragile" value_original="fragile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent to very shortly excurrent, tapering, widened pad of cells absent, adaxial costal cells elongate, 4 cells wide at mid leaf, guide cells in 1 layer;</text>
      <biological_entity id="o4959" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent to very" value_original="percurrent to very" />
        <character is_modifier="false" modifier="very; shortly" name="architecture" src="d0_s3" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o4960" name="pad" name_original="pad" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="widened" value_original="widened" />
      </biological_entity>
      <biological_entity id="o4961" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial costal" id="o4962" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
        <character name="quantity" src="d0_s3" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o4963" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character constraint="at mid leaf" constraintid="o4964" is_modifier="false" name="width" src="d0_s3" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="mid" id="o4964" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity constraint="guide" id="o4965" name="cell" name_original="cells" src="d0_s3" type="structure" />
      <biological_entity id="o4966" name="layer" name_original="layer" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4960" id="r1167" name="consist_of" negation="false" src="d0_s3" to="o4961" />
      <relation from="o4965" id="r1168" name="in" negation="false" src="d0_s3" to="o4966" />
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells little differentiated, quadrate or very shortrectangular, walls thickened and lumens usually oval;</text>
      <biological_entity constraint="basal laminal" id="o4967" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s4" value="quadrate" value_original="quadrate" />
        <character name="shape" src="d0_s4" value="very" value_original="very" />
      </biological_entity>
      <biological_entity id="o4968" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o4969" name="lumen" name_original="lumens" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="oval" value_original="oval" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells 7–9 (–12) µm wide, 1: 1, papillae absent to low, simple, lumens subquadrate to oval, walls irregularly thickened, convex on both sides, 1-stratose.</text>
      <biological_entity constraint="distal laminal" id="o4970" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="um" name="width" src="d0_s5" to="12" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s5" to="9" to_unit="um" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4971" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s5" value="low" value_original="low" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o4972" name="lumen" name_original="lumens" src="d0_s5" type="structure">
        <character char_type="range_value" from="subquadrate" name="shape" src="d0_s5" to="oval" />
      </biological_entity>
      <biological_entity id="o4974" name="side" name_original="sides" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by multicellular spheric gemmae in leaf-axils.</text>
      <biological_entity id="o4975" name="gemma" name_original="gemmae" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
        <character is_modifier="true" name="shape" src="d0_s6" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o4976" name="leaf-axil" name_original="leaf-axils" src="d0_s6" type="structure" />
      <relation from="o4975" id="r1169" name="in" negation="false" src="d0_s6" to="o4976" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition sterile in range of flora.</text>
      <biological_entity id="o4973" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character constraint="on sides" constraintid="o4974" is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity id="o4977" name="flora" name_original="flora" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sporophytes not seen.</text>
    </statement>
    <statement id="d0_s9">
      <text>Distal laminal KOH reaction yellow or orangebrown.</text>
      <biological_entity id="o4978" name="sporophyte" name_original="sporophytes" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s9" value="laminal" value_original="laminal" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="orangebrown" value_original="orangebrown" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, rock, spray zone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="spray zone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.W.T.; Mich.; Mexico; Asia; Atlantic Islands (Cape Verde); Indian Ocean Islands (Reunion).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Cape Verde)" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Reunion)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion>The significant characters of Didymodon maschalogena are the small size of the leaves, which are catenulate, and the presence of propagula; the appearance of the areolation cannot distinguish this species from many congeners but is clearly in longitudinal rows. The species is apparently widespread but of spotty distribution. It was long known as D. michiganensis, but J.-P. Frahm et al. (1996) recently found an earlier name.</discussion>
  
</bio:treatment>