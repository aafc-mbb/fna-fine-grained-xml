<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="(C. E. O. Jensen) Horrell" date="1900" rank="section">Polyclada</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot.</publication_title>
      <place_in_publication>38: 119. 1900,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section Polyclada</taxon_hierarchy>
    <other_info_on_name type="fna_id">316309</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="C. E. O. Jensen" date="unknown" rank="unranked">Polyclada</taxon_name>
    <place_of_publication>
      <publication_title>in Botaniske Forening København, Festskrift,</publication_title>
      <place_in_publication>82. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sphagnum;unranked Polyclada;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="(Russow) H. Klinggräff" date="unknown" rank="unranked">Pycnoclada</taxon_name>
    <taxon_hierarchy>genus Sphagnum;unranked Pycnoclada;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to moderate-sized, with distinct capitulum;</text>
      <biological_entity id="o4924" name="capitulum" name_original="capitulum" src="d0_s0" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s0" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o4923" id="r1364" name="with" negation="false" src="d0_s0" to="o4924" />
    </statement>
    <statement id="d0_s1">
      <text>green, brown or variegated brown and red.</text>
      <biological_entity id="o4923" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="moderate-sized" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="variegated brown and red" value_original="variegated brown and red" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems green to redbrown, superficial cortex of 2–4 layers of efibrillose, nonornamented, enlarged, short-rectangular cells.</text>
      <biological_entity id="o4925" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="redbrown" />
      </biological_entity>
      <biological_entity constraint="superficial" id="o4926" name="cortex" name_original="cortex" src="d0_s2" type="structure" />
      <biological_entity id="o4927" name="layer" name_original="layers" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o4928" name="bud" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="nonornamented" value_original="nonornamented" />
        <character is_modifier="true" name="size" src="d0_s2" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" name="shape" src="d0_s2" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <relation from="o4926" id="r1365" name="consist_of" negation="false" src="d0_s2" to="o4927" />
      <relation from="o4927" id="r1366" name="part_of" negation="false" src="d0_s2" to="o4928" />
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves smaller than branch leaves, triangular-lingulate, with rounded apex, border entire;</text>
      <biological_entity id="o4929" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character constraint="than branch leaves" constraintid="o4930" is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular-lingulate" value_original="triangular-lingulate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o4930" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4931" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4932" name="border" name_original="border" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o4929" id="r1367" name="with" negation="false" src="d0_s3" to="o4931" />
    </statement>
    <statement id="d0_s4">
      <text>hyaline cells efibrillose, nonornamented, rhomboid, septate;</text>
    </statement>
    <statement id="d0_s5">
      <text>resorbed on convex surface;</text>
      <biological_entity id="o4933" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="efibrillose" value_original="efibrillose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="nonornamented" value_original="nonornamented" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rhomboid" value_original="rhomboid" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o4934" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="convex" value_original="convex" />
      </biological_entity>
      <relation from="o4933" id="r1368" name="resorbed on" negation="false" src="d0_s5" to="o4934" />
    </statement>
    <statement id="d0_s6">
      <text>concave surface intact except near apex.</text>
      <biological_entity id="o4935" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="concave" value_original="concave" />
        <character constraint="except apex" constraintid="o4936" is_modifier="false" name="condition" src="d0_s6" value="intact" value_original="intact" />
      </biological_entity>
      <biological_entity id="o4936" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Branches dimorphic, spreading branches stiff;</text>
      <biological_entity id="o4937" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s7" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o4938" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pendent branches slender and delicate.</text>
      <biological_entity id="o4939" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s8" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch fascicles with 3 (–8) spreading and 3 (–8) pendent branches.</text>
      <biological_entity id="o4940" name="branch" name_original="branch" src="d0_s9" type="structure">
        <character constraint="with " constraintid="o4941" is_modifier="false" name="arrangement" src="d0_s9" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o4941" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Branch stems with distinct solitary retort cells, necks not or slightly rostrate.</text>
      <biological_entity constraint="branch" id="o4942" name="stem" name_original="stems" src="d0_s10" type="structure" />
      <biological_entity constraint="retort" id="o4943" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o4944" name="neck" name_original="necks" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="rostrate" value_original="rostrate" />
      </biological_entity>
      <relation from="o4942" id="r1369" name="with" negation="false" src="d0_s10" to="o4943" />
    </statement>
    <statement id="d0_s11">
      <text>Branch leaves ovatelanceolate, apex involute, border entire;</text>
      <biological_entity constraint="branch" id="o4945" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity id="o4946" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o4947" name="border" name_original="border" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hyaline cells fibrillose and nonornamented, convex surface with 4–6 small ringed ovate pores (approximately 1/4 the diameter of the cell), concave surface with 0–4 unringed pores per cell;</text>
      <biological_entity id="o4948" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s12" value="fibrillose" value_original="fibrillose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="nonornamented" value_original="nonornamented" />
      </biological_entity>
      <biological_entity id="o4949" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o4950" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s12" to="6" />
        <character is_modifier="true" name="size" src="d0_s12" value="small" value_original="small" />
        <character is_modifier="true" name="relief" src="d0_s12" value="ringed" value_original="ringed" />
        <character is_modifier="true" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o4951" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o4952" name="pore" name_original="pores" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s12" to="4" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="unringed" value_original="unringed" />
      </biological_entity>
      <biological_entity id="o4953" name="cell" name_original="cell" src="d0_s12" type="structure" />
      <relation from="o4949" id="r1370" name="with" negation="false" src="d0_s12" to="o4950" />
      <relation from="o4951" id="r1371" name="with" negation="false" src="d0_s12" to="o4952" />
      <relation from="o4952" id="r1372" name="per" negation="false" src="d0_s12" to="o4953" />
    </statement>
    <statement id="d0_s13">
      <text>chlorophyllous cells elliptic to truncate-elliptic in transverse-section, qually exposed on both surfaces.</text>
      <biological_entity id="o4955" name="transverse-section" name_original="transverse-section" src="d0_s13" type="structure" />
      <biological_entity id="o4956" name="surface" name_original="surfaces" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition monoicous or dioicous.</text>
      <biological_entity id="o4954" name="bud" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="chlorophyllous" value_original="chlorophyllous" />
        <character char_type="range_value" constraint="in transverse-section" constraintid="o4955" from="elliptic" name="arrangement_or_shape" src="d0_s13" to="truncate-elliptic" />
        <character constraint="on surfaces" constraintid="o4956" is_modifier="false" modifier="qually" name="prominence" notes="" src="d0_s13" value="exposed" value_original="exposed" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="monoicous" value_original="monoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule with few pseudostomata.</text>
      <biological_entity id="o4957" name="capsule" name_original="capsule" src="d0_s15" type="structure" />
      <biological_entity id="o4958" name="pseudostomatum" name_original="pseudostomata" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="few" value_original="few" />
      </biological_entity>
      <relation from="o4957" id="r1373" name="with" negation="false" src="d0_s15" to="o4958" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 15–24 µm; finely papillose on both surfaces;</text>
      <biological_entity id="o4959" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s16" to="24" to_unit="um" />
        <character constraint="on surfaces" constraintid="o4960" is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o4960" name="surface" name_original="surfaces" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>proximal laesura less than 0.5 the length of the spore.</text>
      <biological_entity constraint="proximal" id="o4961" name="laesurum" name_original="laesura" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="0.5" />
      </biological_entity>
      <biological_entity id="o4962" name="spore" name_original="spore" src="d0_s17" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1h.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>