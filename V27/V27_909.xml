<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">618</other_info_on_meta>
    <other_info_on_meta type="mention_page">621</other_info_on_meta>
    <other_info_on_meta type="treatment_page">623</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="genus">syntrichia</taxon_name>
    <taxon_name authority="(H. A. Crum &amp; L. E. Anderson) Ochyra" date="1992" rank="species">ammonsiana</taxon_name>
    <place_of_publication>
      <publication_title>Fragm. Florist. Geobot.</publication_title>
      <place_in_publication>37: 212. 1992,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus syntrichia;species ammonsiana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075542</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="H. A. Crum &amp; L. E. Anderson" date="unknown" rank="species">ammonsiana</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>82: 469. figs. 3–5. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tortula;species ammonsiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–10 mm.</text>
      <biological_entity id="o8703" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves infolded and twisted when dry, recurved when moist, broadly spatulate, 1.5–2.5 × 0.6–0.8 mm;</text>
      <biological_entity id="o8704" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="infolded" value_original="infolded" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s1" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s1" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins plane, entire before but with a few teeth near the apex;</text>
      <biological_entity id="o8705" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character constraint="before teeth" constraintid="o8706" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8706" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o8707" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <relation from="o8706" id="r2070" name="near" negation="false" src="d0_s2" to="o8707" />
    </statement>
    <statement id="d0_s3">
      <text>apices acute;</text>
      <biological_entity id="o8708" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa percurrent, yellow or brown, smooth;</text>
      <biological_entity id="o8709" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal-cells abruptly differentiated, narrower toward the margins;</text>
      <biological_entity id="o8710" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character constraint="toward margins" constraintid="o8711" is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o8711" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal cells quadrate to hexagonal, 10–15 µm, bulging, with about 4 papillae per cell.</text>
      <biological_entity constraint="distal" id="o8712" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s6" to="hexagonal" />
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s6" to="15" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity id="o8713" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o8714" name="cell" name_original="cell" src="d0_s6" type="structure" />
      <relation from="o8712" id="r2071" name="with" negation="false" src="d0_s6" to="o8713" />
      <relation from="o8713" id="r2072" name="per" negation="false" src="d0_s6" to="o8714" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction propagula borne on stalks in axils of distal leaves, leaflike, 0.25–0.45 mm, green, papillose, costate.</text>
      <biological_entity id="o8716" name="stalk" name_original="stalks" src="d0_s7" type="structure" />
      <biological_entity id="o8717" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o8718" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o8715" id="r2073" name="borne on" negation="false" src="d0_s7" to="o8716" />
      <relation from="o8715" id="r2074" name="in" negation="false" src="d0_s7" to="o8717" />
      <relation from="o8717" id="r2075" name="part_of" negation="false" src="d0_s7" to="o8718" />
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous (perigonia and sporophytes unknown).</text>
      <biological_entity id="o8715" name="propagulum" name_original="propagula" src="d0_s7" type="structure">
        <character is_modifier="true" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s7" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s7" to="0.45" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="costate" value_original="costate" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone in deep shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep shade" modifier="sandstone in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., Tenn., W.Va.; South America (Peru); Africa (South Africa).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="Africa (South Africa)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Syntrichia ammonsiana can easily be distinguished from S. chisosa by its 1-stratose laminae.</discussion>
  
</bio:treatment>