<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="mention_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">600</other_info_on_meta>
    <other_info_on_meta type="treatment_page">599</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">tortula</taxon_name>
    <taxon_name authority="(Schimper) Lindberg" date="1879" rank="species">systylia</taxon_name>
    <place_of_publication>
      <publication_title>Musc. Scand.,</publication_title>
      <place_in_publication>20. 1879,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus tortula;species systylia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065195</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Desmatodon</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="species">systylius</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>28: 145. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Desmatodon;species systylius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves obovate to occasionally ovate, apex acute or broadly acute, awned, margins recurved or plane proximally, not bordered;</text>
      <biological_entity id="o7121" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s0" to="occasionally ovate" />
      </biological_entity>
      <biological_entity id="o7122" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s0" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o7123" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s0" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="bordered" value_original="bordered" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>costa excurrent into the awn, lacking an adaxial pad of cells, distally narrow, 2–3 cells across the convex adaxial surface;</text>
      <biological_entity id="o7124" name="costa" name_original="costa" src="d0_s1" type="structure">
        <character constraint="into awn" constraintid="o7125" is_modifier="false" name="architecture" src="d0_s1" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o7125" name="awn" name_original="awn" src="d0_s1" type="structure" />
      <biological_entity constraint="adaxial" id="o7126" name="pad" name_original="pad" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="distally" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
      <biological_entity id="o7127" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity id="o7128" name="cell" name_original="cells" src="d0_s1" type="structure" />
      <biological_entity constraint="adaxial" id="o7129" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="convex" value_original="convex" />
      </biological_entity>
      <relation from="o7126" id="r1702" name="part_of" negation="false" src="d0_s1" to="o7127" />
      <relation from="o7128" id="r1703" name="across" negation="false" src="d0_s1" to="o7129" />
    </statement>
    <statement id="d0_s2">
      <text>distal laminal cells hexagonal to rhomboidal, width 15–22 µm wide, 1: 1 near apex but elongate at mid leaf and basally, smooth.</text>
      <biological_entity id="o7131" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character constraint="at mid leaf" constraintid="o7132" is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="mid" id="o7132" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o7130" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="hexagonal" name="shape" src="d0_s2" to="rhomboidal" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s2" to="22" to_unit="um" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character constraint="near apex" constraintid="o7131" name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_relief" notes="" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporophytes exerted.</text>
      <biological_entity id="o7133" name="sporophyte" name_original="sporophytes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Seta 0.8–1.1 cm.</text>
      <biological_entity id="o7134" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s5" to="1.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule systylius, short-cylindric, erect and nearly straight, urn 1.5–2 mm;</text>
      <biological_entity id="o7135" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="systylius" value_original="systylius" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o7136" name="urn" name_original="urn" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peristome 200–250 µm, teeth 16, straight, divided to near base but variously fused or perforate, basal membrane low;</text>
      <biological_entity id="o7137" name="peristome" name_original="peristome" src="d0_s7" type="structure">
        <character char_type="range_value" from="200" from_unit="um" name="some_measurement" src="d0_s7" to="250" to_unit="um" />
      </biological_entity>
      <biological_entity id="o7138" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character constraint="to base" constraintid="o7139" is_modifier="false" name="shape" src="d0_s7" value="divided" value_original="divided" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o7139" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="variously" name="fusion" src="d0_s7" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7140" name="membrane" name_original="membrane" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="low" value_original="low" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>operculum 0.5–0.7 mm.</text>
      <biological_entity id="o7141" name="operculum" name_original="operculum" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 22–30 (–35) µm, spheric, warty papillose.</text>
      <biological_entity id="o7142" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s9" to="35" to_unit="um" />
        <character char_type="range_value" from="22" from_unit="um" name="some_measurement" src="d0_s9" to="30" to_unit="um" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="warty" value_original="warty" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil in rock crevices, tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" constraint="in rock crevices , tundra" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations (0–3700 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="0" from_unit="m" constraint="high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Que., Yukon; Alaska, Calif., Colo., Wyo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Tortula systylia may be confused with Stegonia but the latter has a distinct triangle of echlorophyllose cells at the apex of the much broader leaves. It is similar also to T. hoppeana, but the laminal cells are smooth. After dehiscence, the systylius capsules retain a raised operculum on the columellar stalk.</discussion>
  
</bio:treatment>