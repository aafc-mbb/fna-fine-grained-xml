<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">369</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="treatment_page">368</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bridel" date="1818" rank="genus">campylopus</taxon_name>
    <taxon_name authority="(Austin) Lesquereu×&amp; James" date="1884" rank="species">angustiretis</taxon_name>
    <place_of_publication>
      <publication_title>Man.,</publication_title>
      <place_in_publication>80. 1884,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus campylopus;species angustiretis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443559</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Austin" date="unknown" rank="species">angustirete</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>4: 150. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species angustirete;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Campylopus</taxon_name>
    <taxon_name authority="R. S. Williams" date="unknown" rank="species">delicatulus</taxon_name>
    <taxon_hierarchy>genus Campylopus;species delicatulus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Campylopus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gracilicaulis</taxon_name>
    <taxon_name authority="(Austin) Kindberg" date="unknown" rank="subspecies">angustiretis</taxon_name>
    <taxon_hierarchy>genus Campylopus;species gracilicaulis;subspecies angustiretis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Campylopus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">surinamensis</taxon_name>
    <taxon_name authority="(Austin) J.-P. Frahm" date="unknown" rank="variety">angustiretis</taxon_name>
    <taxon_hierarchy>genus Campylopus;species surinamensis;variety angustiretis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–20 mm, in loose light green to gray-green mats, evenly foliate with distant, spreading leaves, the distal ones sometimes forming a comal tuft, not tomentose.</text>
      <biological_entity id="o8223" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s0" to="20" to_unit="mm" />
        <character constraint="with leaves" constraintid="o8225" is_modifier="false" modifier="evenly" name="architecture" notes="" src="d0_s0" value="foliate" value_original="foliate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8224" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="loose" value_original="loose" />
        <character char_type="range_value" from="light green" is_modifier="true" name="coloration" src="d0_s0" to="gray-green" />
      </biological_entity>
      <biological_entity id="o8225" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="distant" value_original="distant" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8226" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes; not" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o8227" name="comal" name_original="comal" src="d0_s0" type="structure" />
      <biological_entity id="o8228" name="tuft" name_original="tuft" src="d0_s0" type="structure" />
      <relation from="o8223" id="r1958" name="in" negation="false" src="d0_s0" to="o8224" />
      <relation from="o8226" id="r1959" modifier="sometimes" name="forming a" negation="false" src="d0_s0" to="o8227" />
      <relation from="o8226" id="r1960" modifier="sometimes" name="forming a" negation="false" src="d0_s0" to="o8228" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 6 mm, lanceolate, keeled, long-decurrent, gradually narrowed into a fine channelled, concolorous, straight tip that denticulate at the outermost apex;</text>
      <biological_entity id="o8230" name="tip" name_original="tip" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="concolorous" value_original="concolorous" />
        <character is_modifier="true" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character constraint="at outermost apex" constraintid="o8231" is_modifier="false" name="shape" src="d0_s1" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o8231" name="apex" name_original="apex" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>alar cells large, inflated and auriculate, hyaline;</text>
      <biological_entity id="o8229" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="mm" value="6" value_original="6" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s1" value="long-decurrent" value_original="long-decurrent" />
        <character constraint="into tip" constraintid="o8230" is_modifier="false" modifier="gradually" name="shape" src="d0_s1" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o8232" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="large" value_original="large" />
        <character is_modifier="false" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal laminal cells rectangular, moderately thick-walled, narrower and thin-walled in several marginal rows;</text>
      <biological_entity constraint="basal laminal" id="o8233" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s3" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="width" src="d0_s3" value="narrower" value_original="narrower" />
        <character constraint="in marginal rows" constraintid="o8234" is_modifier="false" name="architecture" src="d0_s3" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o8234" name="row" name_original="rows" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="several" value_original="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal laminal cells elongate, 6–10: 1, not sharply delimited from the basal laminal cells;</text>
      <biological_entity constraint="distal laminal" id="o8235" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="10" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="from basal laminal, cells" constraintid="o8236, o8237" is_modifier="false" modifier="not sharply" name="prominence" src="d0_s4" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8236" name="laminal" name_original="laminal" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o8237" name="cell" name_original="cells" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa relatively narrow, filling 1/4–1/3 of leaf width, excurrent, in transverse-section with a adaxial band of hyalocysts that slightly smaller than the median deuters, abaxially with groups of stereids, smooth at back.</text>
      <biological_entity constraint="distal laminal" id="o8238" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o8240" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s5" to="1/3" />
      </biological_entity>
      <biological_entity id="o8241" name="transverse-section" name_original="transverse-section" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o8242" name="band" name_original="band" src="d0_s5" type="structure">
        <character constraint="than the median deuters" constraintid="o8244" is_modifier="false" name="size" src="d0_s5" value="slightly smaller" value_original="slightly smaller" />
      </biological_entity>
      <biological_entity id="o8243" name="hyalocyst" name_original="hyalocysts" src="d0_s5" type="structure" />
      <biological_entity constraint="median" id="o8244" name="deuter" name_original="deuters" src="d0_s5" type="structure" />
      <biological_entity id="o8245" name="group" name_original="groups" src="d0_s5" type="structure" />
      <biological_entity id="o8246" name="stereid" name_original="stereids" src="d0_s5" type="structure" />
      <biological_entity id="o8247" name="back" name_original="back" src="d0_s5" type="structure" />
      <relation from="o8239" id="r1961" name="filling" negation="false" src="d0_s5" to="o8240" />
      <relation from="o8239" id="r1962" name="in" negation="false" src="d0_s5" to="o8241" />
      <relation from="o8241" id="r1963" name="with" negation="false" src="d0_s5" to="o8242" />
      <relation from="o8242" id="r1964" name="part_of" negation="false" src="d0_s5" to="o8243" />
      <relation from="o8239" id="r1965" modifier="abaxially" name="with" negation="false" src="d0_s5" to="o8245" />
      <relation from="o8245" id="r1966" name="part_of" negation="false" src="d0_s5" to="o8246" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction not known.</text>
      <biological_entity id="o8239" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="relatively" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="width" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character constraint="than the median deuters" constraintid="o8244" is_modifier="false" name="size" src="d0_s5" value="slightly smaller" value_original="slightly smaller" />
        <character constraint="at back" constraintid="o8247" is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sporophytes not known.</text>
      <biological_entity id="o8248" name="sporophyte" name_original="sporophytes" src="d0_s7" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy soil in wet depressions in coastal lowlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" modifier="open" constraint="in wet depressions in coastal lowlands" />
        <character name="habitat" value="wet depressions" modifier="in" constraint="in coastal lowlands" />
        <character name="habitat" value="coastal lowlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Campylopus angustiretis differs from all other species of the genus by its elongate distal laminal cells. Thus it is not certain if it actually belongs to this genus. Because of the lack of sporophytes a decision cannot be made. It has been treated as a variety of the sympatric C. surinamensis and superficially resembles certain expressions of that species (described as C. donnellii). It differs, however, by a narrower costa, the awn not coarsely serrate, the transverse section of the costa showing larger hyalocysts, and distinctly keeled leaves.</discussion>
  
</bio:treatment>