<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
    <other_info_on_meta type="illustration_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="Lindberg" date="1862" rank="section">cuspidata</taxon_name>
    <taxon_name authority="Warnstorf" date="1877" rank="species">obtusum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Berlin)</publication_title>
      <place_in_publication>35: 478. 1877,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section cuspidata;species obtusum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200000817</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate to robust, weak-stemmed, yellow, yellowish-brown to golden brown;</text>
      <biological_entity id="o8481" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="moderate" value_original="moderate" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="weak-stemmed" value_original="weak-stemmed" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="yellowish-brown" name="coloration" src="d0_s0" to="golden brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>capitulum varying from rounded, not 5-radiate and twisted to flat 5-radiate and straight branched.</text>
      <biological_entity id="o8482" name="capitulum" name_original="capitulum" src="d0_s1" type="structure">
        <character constraint="from rounded" is_modifier="false" name="variability" src="d0_s1" value="varying" value_original="varying" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s1" value="5-radiate" value_original="5-radiate" />
        <character char_type="range_value" from="twisted" name="architecture" src="d0_s1" to="flat 5-radiate" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem pale green to pale-brown;</text>
      <biological_entity id="o8483" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s2" to="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>superficial cortex of weakly to moderately differentiated.</text>
      <biological_entity constraint="superficial" id="o8484" name="cortex" name_original="cortex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="weakly to moderately" name="variability" src="d0_s3" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves triangular-lingulate, 0.9–1.3 mm;</text>
    </statement>
    <statement id="d0_s5">
      <text>usually appressed;</text>
      <biological_entity id="o8485" name="leaf-stem" name_original="stem-leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular-lingulate" value_original="triangular-lingulate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="distance" src="d0_s4" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex obtuse and often erose;</text>
      <biological_entity id="o8486" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hyaline cells efibrillose and nonseptate.</text>
      <biological_entity id="o8487" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="efibrillose" value_original="efibrillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branches tapering or in more robust forms, frequently blunt, straight to arcuate, leaves slightly to moderately elongated at distal end.</text>
      <biological_entity id="o8488" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s8" value="in more robust forms" value_original="in more robust forms" />
        <character is_modifier="false" modifier="frequently" name="shape" notes="" src="d0_s8" value="blunt" value_original="blunt" />
        <character char_type="range_value" from="straight" name="course" src="d0_s8" to="arcuate" />
      </biological_entity>
      <biological_entity id="o8489" name="form" name_original="forms" src="d0_s8" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s8" value="robust" value_original="robust" />
      </biological_entity>
      <biological_entity id="o8490" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character constraint="at distal end" constraintid="o8491" is_modifier="false" modifier="slightly to moderately" name="length" src="d0_s8" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8491" name="end" name_original="end" src="d0_s8" type="structure" />
      <relation from="o8488" id="r2344" name="in" negation="false" src="d0_s8" to="o8489" />
    </statement>
    <statement id="d0_s9">
      <text>Branch fascicles with 2 spreading and 2 pendent branches.</text>
      <biological_entity id="o8492" name="branch" name_original="branch" src="d0_s9" type="structure">
        <character constraint="with branches" constraintid="o8493" is_modifier="false" name="arrangement" src="d0_s9" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o8493" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Branch stems green, with cortex enlarged with conspicuous retort cells.</text>
      <biological_entity constraint="branch" id="o8494" name="stem" name_original="stems" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o8495" name="cortex" name_original="cortex" src="d0_s10" type="structure">
        <character constraint="with retort cells" constraintid="o8496" is_modifier="false" name="size" src="d0_s10" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="retort" id="o8496" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o8494" id="r2345" name="with" negation="false" src="d0_s10" to="o8495" />
    </statement>
    <statement id="d0_s11">
      <text>Branch leaves ovate to ovatelanceolate;</text>
    </statement>
    <statement id="d0_s12">
      <text>more than 1.8 mm;</text>
    </statement>
    <statement id="d0_s13">
      <text>straight, stiff, not much undulate and reflexed to recurved;</text>
      <biological_entity constraint="branch" id="o8497" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="ovatelanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" upper_restricted="false" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="fragility" src="d0_s13" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="not much" name="shape" src="d0_s13" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="reflexed" name="orientation" src="d0_s13" to="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>margins entire;</text>
      <biological_entity id="o8498" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>hyaline cells on convex surface with a few end pores, but mostly numerous small to very small (often barely visible) pores or wall-thinnings free from the commissures, on concave surface similar, but with pores generally fewer and larger;</text>
      <biological_entity id="o8499" name="bud" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8500" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="end" id="o8501" name="pore" name_original="pores" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o8502" name="pore" name_original="pores" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="mostly" name="quantity" src="d0_s15" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="size" src="d0_s15" value="small to very" value_original="small to very" />
        <character is_modifier="true" modifier="very" name="size" src="d0_s15" value="small" value_original="small" />
        <character constraint="from commissures" constraintid="o8504" is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="size" src="d0_s15" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o8503" name="wall-thinning" name_original="wall-thinnings" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="mostly" name="quantity" src="d0_s15" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="size" src="d0_s15" value="small to very" value_original="small to very" />
        <character is_modifier="true" modifier="very" name="size" src="d0_s15" value="small" value_original="small" />
        <character constraint="from commissures" constraintid="o8504" is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="size" src="d0_s15" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o8504" name="commissure" name_original="commissures" src="d0_s15" type="structure" />
      <biological_entity id="o8505" name="surface" name_original="surface" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o8506" name="pore" name_original="pores" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="generally" name="quantity" src="d0_s15" value="fewer" value_original="fewer" />
      </biological_entity>
      <relation from="o8499" id="r2346" name="on" negation="false" src="d0_s15" to="o8500" />
      <relation from="o8500" id="r2347" name="with" negation="false" src="d0_s15" to="o8501" />
      <relation from="o8502" id="r2348" name="on" negation="false" src="d0_s15" to="o8505" />
      <relation from="o8503" id="r2349" name="on" negation="false" src="d0_s15" to="o8505" />
      <relation from="o8502" id="r2350" name="with" negation="false" src="d0_s15" to="o8506" />
      <relation from="o8503" id="r2351" name="with" negation="false" src="d0_s15" to="o8506" />
    </statement>
    <statement id="d0_s16">
      <text>chlorophyllous cells triangular in transverse-section, just reaching concave surface or slightly enclosed.</text>
      <biological_entity id="o8508" name="transverse-section" name_original="transverse-section" src="d0_s16" type="structure" />
      <biological_entity id="o8509" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="concave" value_original="concave" />
      </biological_entity>
      <relation from="o8507" id="r2352" modifier="just" name="reaching" negation="false" src="d0_s16" to="o8509" />
    </statement>
    <statement id="d0_s17">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o8507" name="bud" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="chlorophyllous" value_original="chlorophyllous" />
        <character constraint="in transverse-section" constraintid="o8508" is_modifier="false" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s16" value="enclosed" value_original="enclosed" />
        <character is_modifier="false" name="reproduction" src="d0_s17" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s17" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 18–27 µm; both surfaces covered with rough, irregular verrucate plates of papillae, bifurcated Y-mark sculpture on distal surface;</text>
      <biological_entity id="o8510" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s18" to="27" to_unit="um" />
      </biological_entity>
      <biological_entity id="o8511" name="surface" name_original="surfaces" src="d0_s18" type="structure" />
      <biological_entity id="o8512" name="plate" name_original="plates" src="d0_s18" type="structure">
        <character is_modifier="true" name="sculpture" src="d0_s18" value="rough" value_original="rough" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s18" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="relief" src="d0_s18" value="verrucate" value_original="verrucate" />
      </biological_entity>
      <biological_entity id="o8513" name="papilla" name_original="papillae" src="d0_s18" type="structure">
        <character is_modifier="true" name="sculpture" src="d0_s18" value="rough" value_original="rough" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s18" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="relief" src="d0_s18" value="verrucate" value_original="verrucate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8514" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s18" value="bifurcated" value_original="bifurcated" />
        <character is_modifier="true" name="relief" src="d0_s18" value="y-mark" value_original="y-mark" />
        <character is_modifier="true" name="sculpture" src="d0_s18" value="rough" value_original="rough" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s18" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="relief" src="d0_s18" value="verrucate" value_original="verrucate" />
      </biological_entity>
      <relation from="o8511" id="r2353" name="covered with" negation="false" src="d0_s18" to="o8512" />
      <relation from="o8511" id="r2354" name="covered with" negation="false" src="d0_s18" to="o8513" />
      <relation from="o8511" id="r2355" name="covered with" negation="false" src="d0_s18" to="o8514" />
    </statement>
    <statement id="d0_s19">
      <text>proximal laesura less than 0.5 spore radius.</text>
      <biological_entity constraint="proximal" id="o8515" name="laesurum" name_original="laesura" src="d0_s19" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s19" to="0.5" />
      </biological_entity>
      <biological_entity id="o8516" name="spore" name_original="spore" src="d0_s19" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forming carpets in minerotrophic peatlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="carpets" modifier="forming" constraint="in minerotrophic peatlands" />
        <character name="habitat" value="minerotrophic peatlands" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Minn.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <discussion>Sporophytes are uncommon in Sphagnum obtusum. This is a quite phenotypically variable species that warrants further investigation, which may result in taxonomic splitting. The strongly obtuse stem leaf should separate it from any similar species with which it occurs. Sphagnum mendocinum looks similar phenotypically but there appears to be no range overlap with S. obtusum. The tiny branch leaf pores, which may seem like no more than pinpricks in the cell surface, easily separate S. obtusum microscopically from other species of sect. Cuspidata.</discussion>
  
</bio:treatment>