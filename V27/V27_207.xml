<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy R. Brassard</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">624</other_info_on_meta>
    <other_info_on_meta type="treatment_page">165</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">TIMMIACEAE</taxon_name>
    <taxon_hierarchy>family TIMMIACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10900</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acrocarpous, large.</text>
      <biological_entity id="o429" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems stiff, erect, mostly unbranched, in cross-section with a cortical region of small, thick-walled cells, a parenchyma of larger, thin-walled cells, and a distinct central strand;</text>
      <biological_entity id="o430" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o431" name="cross-section" name_original="cross-section" src="d0_s1" type="structure" />
      <biological_entity id="o432" name="cortical" name_original="cortical" src="d0_s1" type="structure" />
      <biological_entity id="o433" name="region" name_original="region" src="d0_s1" type="structure" />
      <biological_entity id="o434" name="bud" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
      <biological_entity id="o435" name="parenchyma" name_original="parenchyma" src="d0_s1" type="structure" />
      <biological_entity id="o436" name="bud" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="larger" value_original="larger" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity constraint="central" id="o437" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o430" id="r128" name="in" negation="false" src="d0_s1" to="o431" />
      <relation from="o431" id="r129" name="with" negation="false" src="d0_s1" to="o432" />
      <relation from="o431" id="r130" name="with" negation="false" src="d0_s1" to="o433" />
      <relation from="o432" id="r131" name="part_of" negation="false" src="d0_s1" to="o434" />
      <relation from="o433" id="r132" name="part_of" negation="false" src="d0_s1" to="o434" />
      <relation from="o435" id="r133" name="part_of" negation="false" src="d0_s1" to="o436" />
    </statement>
    <statement id="d0_s2">
      <text>rhizoids abundant, especially on the proximal stems and in the leaf-axils, dark reddish black, coarsely papillose, ca. 15–35 µm wide.</text>
      <biological_entity id="o438" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s2" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="black" value_original="black" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s2" to="35" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o439" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o440" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure" />
      <relation from="o438" id="r134" modifier="especially" name="on" negation="false" src="d0_s2" to="o439" />
      <relation from="o438" id="r135" name="in" negation="false" src="d0_s2" to="o440" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves lanceolate or long-lanceolate, with a sheathing, entire base occupying the proximal 1/6–1/3 of leaf, and a nonsheathing, serrate limb;</text>
      <biological_entity id="o441" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="long-lanceolate" value_original="long-lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="nonsheathing" value_original="nonsheathing" />
      </biological_entity>
      <biological_entity id="o442" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o443" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="true" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/6" is_modifier="true" name="quantity" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity id="o444" name="limb" name_original="limb" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <relation from="o441" id="r136" name="with" negation="false" src="d0_s3" to="o442" />
      <relation from="o442" id="r137" name="occupying the" negation="false" src="d0_s3" to="o443" />
    </statement>
    <statement id="d0_s4">
      <text>costa strong, single, subpercurrent to excurrent, in cross-section (mid-limb) with large guide cells, two bands of stereids, and enlarged abaxial and adaxial cells;</text>
      <biological_entity id="o445" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s4" value="strong" value_original="strong" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="false" name="position" src="d0_s4" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o446" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity constraint="guide" id="o447" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="large" value_original="large" />
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o448" name="band" name_original="bands" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o449" name="stereid" name_original="stereids" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial and adaxial" id="o450" name="bud" name_original="cells" src="d0_s4" type="structure" />
      <relation from="o445" id="r138" name="in" negation="false" src="d0_s4" to="o446" />
      <relation from="o446" id="r139" name="with" negation="false" src="d0_s4" to="o447" />
      <relation from="o448" id="r140" name="part_of" negation="false" src="d0_s4" to="o449" />
    </statement>
    <statement id="d0_s5">
      <text>cells of limb lamina irregularly isodiametric or quadrate, smooth, papillose or mammillose on adaxial and abaxial surfaces;</text>
      <biological_entity id="o451" name="bud" name_original="cells" src="d0_s5" type="structure" constraint="lamina" constraint_original="lamina; lamina">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s5" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
        <character constraint="on adaxial abaxial surfaces" constraintid="o453" is_modifier="false" name="relief" src="d0_s5" value="mammillose" value_original="mammillose" />
      </biological_entity>
      <biological_entity constraint="limb" id="o452" name="lamina" name_original="lamina" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial and abaxial" id="o453" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <relation from="o451" id="r141" name="part_of" negation="false" src="d0_s5" to="o452" />
    </statement>
    <statement id="d0_s6">
      <text>cells of the sheath lamina rectangular.</text>
      <biological_entity id="o454" name="bud" name_original="cells" src="d0_s6" type="structure" constraint="lamina" constraint_original="lamina; lamina">
        <character is_modifier="false" name="shape" src="d0_s6" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="sheath" id="o455" name="lamina" name_original="lamina" src="d0_s6" type="structure" />
      <relation from="o454" id="r142" name="part_of" negation="false" src="d0_s6" to="o455" />
    </statement>
    <statement id="d0_s7">
      <text>Seta terminal, elongate, erect, single or double, 1.4–3.5 cm.</text>
      <biological_entity id="o456" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="single" value_original="single" />
        <character name="quantity" src="d0_s7" value="double" value_original="double" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="some_measurement" src="d0_s7" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule pendulous to nearly erect, ovate to oblong-cylindric, smooth or broadly plicate;</text>
      <biological_entity id="o457" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character char_type="range_value" from="pendulous" name="orientation" src="d0_s8" to="nearly erect" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="oblong-cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s8" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>annulus large, revoluble;</text>
      <biological_entity id="o458" name="annulus" name_original="annulus" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="large" value_original="large" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="revoluble" value_original="revoluble" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>operculum hemispheric or conic, with distinct mamilla at the tip;</text>
      <biological_entity id="o459" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity id="o460" name="mamilla" name_original="mamilla" src="d0_s10" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o461" name="tip" name_original="tip" src="d0_s10" type="structure" />
      <relation from="o459" id="r143" name="with" negation="false" src="d0_s10" to="o460" />
      <relation from="o460" id="r144" name="at" negation="false" src="d0_s10" to="o461" />
    </statement>
    <statement id="d0_s11">
      <text>stomata numerous on the neck or over the entire urn;</text>
      <biological_entity id="o462" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character constraint="on the neck or over urn" constraintid="o463" is_modifier="false" name="quantity" src="d0_s11" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o463" name="urn" name_original="urn" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome diplolepideous;</text>
      <biological_entity id="o464" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="diplolepideous" value_original="diplolepideous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exostome of 16 lanceolate teeth sometimes united near the base, smooth or papillose proximally, strongly vertically barred and papillose distally, with prominent projecting lamellae on the interior surface;</text>
      <biological_entity id="o465" name="exostome" name_original="exostome" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="strongly vertically" name="coloration_or_relief" src="d0_s13" value="barred" value_original="barred" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o466" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="16" value_original="16" />
        <character is_modifier="true" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character constraint="near base" constraintid="o467" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s13" value="united" value_original="united" />
      </biological_entity>
      <biological_entity id="o467" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity id="o468" name="lamella" name_original="lamellae" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="orientation" src="d0_s13" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="interior" id="o469" name="surface" name_original="surface" src="d0_s13" type="structure" />
      <relation from="o465" id="r145" name="consist_of" negation="false" src="d0_s13" to="o466" />
      <relation from="o465" id="r146" name="with" negation="false" src="d0_s13" to="o468" />
      <relation from="o468" id="r147" name="on" negation="false" src="d0_s13" to="o469" />
    </statement>
    <statement id="d0_s14">
      <text>endostome about the same height as the exostome, with a high basal membrane about half the height of the exostome, and 64 irregularly anastomosing, nodose cilia papillose externally and smooth or appendiculate on the interior surface.</text>
      <biological_entity id="o470" name="endostome" name_original="endostome" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="irregularly" name="height" src="d0_s14" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
      <biological_entity id="o471" name="exostome" name_original="exostome" src="d0_s14" type="structure" />
      <biological_entity constraint="basal" id="o472" name="membrane" name_original="membrane" src="d0_s14" type="structure">
        <character is_modifier="true" name="height" src="d0_s14" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o473" name="exostome" name_original="exostome" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="64" value_original="64" />
      </biological_entity>
      <biological_entity id="o474" name="cilium" name_original="cilia" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="nodose" value_original="nodose" />
        <character is_modifier="false" modifier="externally" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="smooth" value_original="smooth" />
        <character constraint="on interior surface" constraintid="o475" is_modifier="false" name="architecture" src="d0_s14" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
      <biological_entity constraint="interior" id="o475" name="surface" name_original="surface" src="d0_s14" type="structure" />
      <relation from="o470" id="r148" name="as" negation="false" src="d0_s14" to="o471" />
      <relation from="o470" id="r149" name="with" negation="false" src="d0_s14" to="o472" />
    </statement>
    <statement id="d0_s15">
      <text>Calyptra linear-cylindrical.</text>
      <biological_entity id="o476" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="linear-cylindrical" value_original="linear-cylindrical" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, n Africa, Pacific Islands (Hawaii, New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Genus 1, species 4 (4 in the flora).</discussion>
  <discussion>Timmiaceae is a small, taxonomically isolated family.</discussion>
  
</bio:treatment>