<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="treatment_page">369</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bridel" date="1818" rank="genus">campylopus</taxon_name>
    <taxon_name authority="Grout" date="1939" rank="species">carolinae</taxon_name>
    <place_of_publication>
      <publication_title>Moss Fl. N. Amer.</publication_title>
      <place_in_publication>1: 249, plate 122. 1939,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus campylopus;species carolinae</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443566</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually less than 1 cm, in loose mats, dark green to brownish green or blackish;</text>
      <biological_entity id="o6084" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1" to_unit="cm" />
        <character char_type="range_value" from="dark green" name="coloration" notes="" src="d0_s0" to="brownish green or blackish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6085" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o6084" id="r1452" name="in" negation="false" src="d0_s0" to="o6085" />
    </statement>
    <statement id="d0_s1">
      <text>leaves erect-patent;</text>
      <biological_entity id="o6086" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect-patent" value_original="erect-patent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stems sparsely tomentose.</text>
      <biological_entity id="o6087" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2.5–4 mm, small, lanceolate, ending in a concolorous straight tip, convolute in the distal part, with entire margins;</text>
      <biological_entity id="o6089" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="concolorous" value_original="concolorous" />
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6090" name="part" name_original="part" src="d0_s3" type="structure" />
      <biological_entity id="o6091" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o6088" id="r1453" name="ending in a" negation="false" src="d0_s3" to="o6089" />
      <relation from="o6088" id="r1454" name="with" negation="false" src="d0_s3" to="o6091" />
    </statement>
    <statement id="d0_s4">
      <text>alar cells not or only slightly differentiated;</text>
      <biological_entity id="o6088" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character constraint="in distal part" constraintid="o6090" is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity id="o6092" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="only slightly" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells rectangular, firm-walled, hyaline, 2.5–3.5:1, indistinctly bordered at margins;</text>
      <biological_entity constraint="basal laminal" id="o6093" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="2.5" name="quantity" src="d0_s5" to="3.5" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character constraint="at margins" constraintid="o6094" is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s5" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o6094" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells oblique to oval, incrassate, ca. 3–5: 1;</text>
      <biological_entity constraint="distal laminal" id="o6095" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblique" name="shape" src="d0_s6" to="oval" />
        <character is_modifier="false" name="size" src="d0_s6" value="incrassate" value_original="incrassate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa filling 1/3 of leaf width, excurrent in a straight, toothed, hyaline point, in transverse-section showing abaxial and adaxial stereids, ridged abaxially with prominent cells.</text>
      <biological_entity constraint="distal laminal" id="o6096" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblique" name="shape" src="d0_s7" to="oval" />
        <character is_modifier="false" name="size" src="d0_s7" value="incrassate" value_original="incrassate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o6098" name="leaf" name_original="leaf" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o6099" name="point" name_original="point" src="d0_s7" type="structure">
        <character is_modifier="true" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="true" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o6100" name="transverse-section" name_original="transverse-section" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial and adaxial" id="o6101" name="stereid" name_original="stereids" src="d0_s7" type="structure" />
      <biological_entity id="o6102" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o6097" id="r1455" name="filling" negation="false" src="d0_s7" to="o6098" />
      <relation from="o6097" id="r1456" name="in" negation="false" src="d0_s7" to="o6100" />
      <relation from="o6100" id="r1457" name="showing" negation="false" src="d0_s7" to="o6101" />
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction occasionally by means of deciduous stem tips.</text>
      <biological_entity id="o6097" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character constraint="in point" constraintid="o6099" is_modifier="false" name="width" src="d0_s7" value="excurrent" value_original="excurrent" />
        <character constraint="with cells" constraintid="o6102" is_modifier="false" name="shape" src="d0_s7" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity constraint="stem" id="o6103" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Sporophytes not known in area of the flora.</text>
      <biological_entity id="o6104" name="sporophyte" name_original="sporophytes" src="d0_s9" type="structure" />
      <biological_entity id="o6105" name="area" name_original="area" src="d0_s9" type="structure" />
      <relation from="o6104" id="r1458" name="known in" negation="false" src="d0_s9" to="o6105" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Typically buried in white sand in depressions, in open pine and pine-oak forests and open grassland, coastal lowlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="white sand" modifier="typically buried in" constraint="in depressions" />
        <character name="habitat" value="depressions" />
        <character name="habitat" value="open pine" modifier="in" />
        <character name="habitat" value="pine-oak forests" />
        <character name="habitat" value="open grassland" />
        <character name="habitat" value="coastal lowlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., N.C.; South America (Brazil).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>The disjunction for Campylopus carolinae of western South America–southeastern North America is also found for C. angustiretis, C. surinamensis and C. pyriformis, which grow in similar habitats in white sand. The type material from Brunswick, North Carolina is mixed with C. surinamensis, which caused confusion and recognition of this species as a variety of C. delicatulus R. S. Williams (= C. angustiretis).</discussion>
  
</bio:treatment>