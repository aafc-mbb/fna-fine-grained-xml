<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">368</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="treatment_page">375</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bridel" date="1818" rank="genus">campylopus</taxon_name>
    <taxon_name authority="Sullivant &amp; Lesquereux" date="1865" rank="species">tallulensis</taxon_name>
    <place_of_publication>
      <publication_title>Musci Bor.-Amer. ed.</publication_title>
      <place_in_publication>2, 17. 1865,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus campylopus;species tallulensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443587</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 5 cm, in tufts, yellowish green, rarely green.</text>
      <biological_entity id="o612" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o613" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o612" id="r153" name="in" negation="false" src="d0_s0" to="o613" />
    </statement>
    <statement id="d0_s1">
      <text>Stems slender, not or densely reddish tomentose, evenly foliate.</text>
      <biological_entity id="o614" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="not; densely" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves about 5 mm, erect-spreading, lanceolate, narrowed to a straight, serrate tip;</text>
      <biological_entity id="o616" name="tip" name_original="tip" src="d0_s2" type="structure">
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alar cells hardly differentiated, forming hyaline or reddish auricles;</text>
      <biological_entity id="o615" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character constraint="to tip" constraintid="o616" is_modifier="false" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o617" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="hardly" name="variability" src="d0_s3" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o618" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
      </biological_entity>
      <relation from="o617" id="r154" name="forming" negation="false" src="d0_s3" to="o618" />
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells hyaline, thin-walled, rectangular, often forming a V-shaped area;</text>
      <biological_entity constraint="basal laminal" id="o619" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o620" name="area" name_original="area" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
      <relation from="o619" id="r155" modifier="often" name="forming a" negation="false" src="d0_s4" to="o620" />
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells short-rectangular, incrassate;</text>
      <biological_entity constraint="distal laminal" id="o621" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="size" src="d0_s5" value="incrassate" value_original="incrassate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa filling half of the leaf width, shortly excurrent in a concolorous tip, in transverse-section showing large adaxial hyalocysts occupying 1/2 of the thickness of the leaf, and abaxial groups of stereids, abaxially ridged.</text>
      <biological_entity id="o622" name="costa" name_original="costa" src="d0_s6" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character constraint="in tip" constraintid="o624" is_modifier="false" modifier="shortly" name="width" src="d0_s6" value="excurrent" value_original="excurrent" />
        <character constraint="of thickness" constraintid="o627" name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o623" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o624" name="tip" name_original="tip" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="concolorous" value_original="concolorous" />
      </biological_entity>
      <biological_entity id="o625" name="transverse-section" name_original="transverse-section" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o626" name="hyalocyst" name_original="hyalocysts" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o627" name="thickness" name_original="thickness" src="d0_s6" type="structure" />
      <biological_entity id="o628" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o630" name="stereid" name_original="stereids" src="d0_s6" type="structure" />
      <relation from="o622" id="r156" name="part_of" negation="false" src="d0_s6" to="o623" />
      <relation from="o622" id="r157" name="in" negation="false" src="d0_s6" to="o625" />
      <relation from="o625" id="r158" name="showing" negation="false" src="d0_s6" to="o626" />
      <relation from="o627" id="r159" name="part_of" negation="false" src="d0_s6" to="o628" />
      <relation from="o629" id="r160" name="part_of" negation="false" src="d0_s6" to="o630" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction by deciduous leaves or stem tips.</text>
      <biological_entity constraint="abaxial" id="o629" name="hyalocyst" name_original="hyalocysts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s6" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o631" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="by stem tips" constraintid="o632" is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="stem" id="o632" name="tip" name_original="tips" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Sporophytes not known.</text>
      <biological_entity id="o633" name="sporophyte" name_original="sporophytes" src="d0_s8" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acidic rocks (granite, sandstone), exposed boulders, rarely on soil in open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic rocks" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="boulders" modifier=") exposed" />
        <character name="habitat" value="soil" modifier="rarely on" constraint="in open woods" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Del., Ga., Ill., La., Miss., N.C., Ohio, S.C., Tenn., Va., Wyo.; Mexico; Central America (Nicaragua); South America (Bolivia, Colombia, Peru, Venezuela).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Nicaragua)" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="South America (Colombia)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion>The disjunction of Campylopus tallulensis from southeastern North America to Mexico, which is also met in other bryophytes and flowering plants, is considered to be a result of a former continuous range in the Tertiary. Campylopus tallulensis was included in C. flexuosus by American authors. There is a superficial similarity regarding the habit and the shape of the distal laminal cells. Campylopus flexuosus is, however, easily distinguished by thick-walled basal laminal cells and the presence of microphyllous brood branches. Plants of C. tallulensis from Mexico and eastern North America are robust and yellowish to golden green. In contrast, the specimens collected in Illinois, Mississippi (in part), and Arkansas are more slender and dark green, resembling C. subulatus in appearance. It is not known whether these differences in color depend on a different geological substrate or are the expression of different populations. Both species are anatomically very similar with thin-walled hyaline basal laminal cells, almost quadrate distal laminal cells, a costa excurrent in a sometimes subhyaline point and being roughened at the abaxial side like a rat’s tail file and a channeled leaf apex. The only way to distinguish both species seems to be the transverse section of the costa, which shows very distinct groups of abaxial stereids in C. tallulensis but no abaxial stereids in C. subulatus. Furthermore, the adaxial hyalocysts of C. tallulensis are twice as wide as those of C. subulatus (J.-P. Frahm 1994). On the basis of this character, the only records of C. subulatus in North America from California belong to this species and are not extensions of the range of C. tallulensis from Mexico.</discussion>
  
</bio:treatment>