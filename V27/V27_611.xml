<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Wilfred B. Schofield</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="treatment_page">428</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1846" rank="genus">RHABDOWEISIA</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>1: 97. 1846  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus RHABDOWEISIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek rhabdos, rod, presumably alluding to ribbed capsule, and genus Weissia, alluding to resemblance</other_info_on_name>
    <other_info_on_name type="fna_id">128216</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in short loose tufts, perennial, dull green to brownish.</text>
      <biological_entity id="o3904" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" notes="" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dull" value_original="dull" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3905" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o3904" id="r899" name="in" negation="false" src="d0_s0" to="o3905" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.2–3 cm, erect, forked by innovations, redbrown, without central strand, rhizoids smooth, at base of stem.</text>
      <biological_entity id="o3906" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="by innovations" constraintid="o3907" is_modifier="false" name="shape" src="d0_s1" value="forked" value_original="forked" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s1" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity id="o3907" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
      <biological_entity constraint="central" id="o3908" name="strand" name_original="strand" src="d0_s1" type="structure" />
      <biological_entity id="o3909" name="rhizoid" name_original="rhizoids" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o3910" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o3911" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o3906" id="r900" name="without" negation="false" src="d0_s1" to="o3908" />
      <relation from="o3909" id="r901" name="at" negation="false" src="d0_s1" to="o3910" />
      <relation from="o3910" id="r902" name="part_of" negation="false" src="d0_s1" to="o3911" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves oblong-lanceolate to narrowly lanceolate, obtuse to acute, keeled, strongly divergent when moist, crisped to sinuose when dry, 2–4 mm;</text>
      <biological_entity id="o3912" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="narrowly lanceolate obtuse" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="narrowly lanceolate obtuse" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="when moist" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="sinuose" value_original="sinuose" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa subpercurrent, formed of rectangular cells;</text>
      <biological_entity id="o3913" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
      <biological_entity id="o3914" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <relation from="o3913" id="r903" name="formed of" negation="false" src="d0_s3" to="o3914" />
    </statement>
    <statement id="d0_s4">
      <text>margins usually recurved, mainly serrate to serrulate along distal margins, with 1-celled irregular teeth;</text>
      <biological_entity id="o3915" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="mainly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character constraint="along distal margins" constraintid="o3916" is_modifier="false" name="shape_or_architecture" src="d0_s4" value="serrate to serrulate" value_original="serrate to serrulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3916" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o3917" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="1-celled" value_original="1-celled" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s4" value="irregular" value_original="irregular" />
      </biological_entity>
      <relation from="o3915" id="r904" name="with" negation="false" src="d0_s4" to="o3917" />
    </statement>
    <statement id="d0_s5">
      <text>laminal cells smooth, proximal cells hyaline to brownish, rectangular, thin-walled, absent chlorophyll;</text>
      <biological_entity constraint="laminal" id="o3918" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3919" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="hyaline" name="coloration" src="d0_s5" to="brownish" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o3920" name="chlorophyll" name_original="chlorophyll" src="d0_s5" type="substance">
        <character is_modifier="true" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal cells, quadrate to irregular, usually broader than long, chlorophyllose except for the marginal teeth.</text>
      <biological_entity constraint="marginal" id="o3922" name="tooth" name_original="teeth" src="d0_s6" type="structure" />
      <relation from="o3921" id="r905" name="except for" negation="false" src="d0_s6" to="o3922" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="distal" id="o3921" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s6" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="width" src="d0_s6" value="usually broader than long" value_original="usually broader than long" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perigonial leaves short-elliptic;</text>
      <biological_entity constraint="perigonial" id="o3923" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="short-elliptic" value_original="short-elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves not differing from the vegetative.</text>
      <biological_entity constraint="perichaetial" id="o3924" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seta solitary, erect, yellow, 2–6 mm, smooth.</text>
      <biological_entity id="o3925" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s11" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule exserted, erect, brown to yellowbrown, 0.5–1 mm, symmetric, ovoid to oblong-cylindric, when dry and empty contracted below mouth and deeply 8-furrowed,;</text>
      <biological_entity id="o3926" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s12" to="yellowbrown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="oblong-cylindric" />
        <character is_modifier="false" modifier="when dry empty below mouth and deeply 8-furrowed" name="condition_or_size" src="d0_s12" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>annulus absent;</text>
      <biological_entity id="o3927" name="annulus" name_original="annulus" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stomata at base of urn, phaneropore;</text>
      <biological_entity id="o3928" name="stoma" name_original="stomata" src="d0_s14" type="structure" />
      <biological_entity id="o3929" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o3930" name="urn" name_original="urn" src="d0_s14" type="structure" />
      <biological_entity id="o3931" name="phaneropore" name_original="phaneropore" src="d0_s14" type="structure" />
      <relation from="o3928" id="r906" name="at" negation="false" src="d0_s14" to="o3929" />
      <relation from="o3929" id="r907" name="part_of" negation="false" src="d0_s14" to="o3930" />
    </statement>
    <statement id="d0_s15">
      <text>operculum obliquely subulate, 0.5–1 mm;</text>
      <biological_entity id="o3932" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome single, 16 teeth joined by a low basal membrane, inserted slightly below mouth, irregular and not forked, early deciduous.</text>
      <biological_entity id="o3933" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="single" value_original="single" />
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
      <biological_entity id="o3934" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character constraint="by basal membrane" constraintid="o3935" is_modifier="false" name="fusion" src="d0_s16" value="joined" value_original="joined" />
        <character constraint="slightly below mouth" constraintid="o3936" is_modifier="false" name="position" notes="" src="d0_s16" value="inserted" value_original="inserted" />
        <character is_modifier="false" name="architecture_or_course" notes="" src="d0_s16" value="irregular" value_original="irregular" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="forked" value_original="forked" />
        <character is_modifier="false" name="duration" src="d0_s16" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3935" name="membrane" name_original="membrane" src="d0_s16" type="structure">
        <character is_modifier="true" name="position" src="d0_s16" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o3936" name="mouth" name_original="mouth" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Calyptra cucullate, smooth, covering 1/2 of capsule.</text>
      <biological_entity id="o3937" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="position_relational" src="d0_s17" value="covering" value_original="covering" />
        <character constraint="of capsule" constraintid="o3938" name="quantity" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o3938" name="capsule" name_original="capsule" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Spores nearly spherical to slightly angled, smooth, 16–20 µm, papillose.</text>
      <biological_entity id="o3939" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="nearly spherical" name="shape" src="d0_s18" to="slightly angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="16" from_unit="um" name="some_measurement" src="d0_s18" to="20" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Europe, Asia, Africa, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Species 6 (2 in the flora).</discussion>
  <discussion>Rhabdoweisia is known predominately from the Northern Hemisphere. A strong case has been made to include this, as well as other genera, in the family Rhabdoweisiaceae Limpricht.</discussion>
  <references>
    <reference>Lawton, E. 1961. A revision of the genus Rhabdoweisia. Bryologist 64: 140–156.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves long-ligulate, 2-4 mm, broadly acute to obtuse, irregularly dentate in distal part, distal cells hexagonal, 14-20 µm wide, thin-walled relative to the lumina.</description>
      <determination>1 Rhabdoweisia crenulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves linear-lanceolate, 2-3.5 mm, gradually and narrowly acute, serrulate in distal part, distal cells mainly quadrate, often wider than long, 10-14 µm wide, somewhat incrassate.</description>
      <determination>2 Rhabdoweisia crispata</determination>
    </key_statement>
  </key>
</bio:treatment>