<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">621</other_info_on_meta>
    <other_info_on_meta type="treatment_page">620</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="genus">syntrichia</taxon_name>
    <taxon_name authority="(Wilson) Juratska" date="1882" rank="species">papillosa</taxon_name>
    <place_of_publication>
      <publication_title>Laubm.-Fl. Oestrr.-Ung.,</publication_title>
      <place_in_publication>141. 1882,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus syntrichia;species papillosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075540</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="Wilson" date="unknown" rank="species">papillosa</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>4: 193. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tortula;species papillosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–4 (–8) mm.</text>
      <biological_entity id="o8072" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s0" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves incurved and slightly twisted when dry, erect to widespreading when moist, spatulate, (1.5–) 2–3 × 0.75–1.25 mm;</text>
      <biological_entity id="o8073" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="erect" modifier="when moist" name="orientation" src="d0_s1" to="widespreading" />
        <character is_modifier="false" name="shape" src="d0_s1" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s1" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.75" from_unit="mm" name="width" src="d0_s1" to="1.25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins incurved when dry, plane to erect when moist, entire or occasionally serrulate near the apex;</text>
      <biological_entity id="o8074" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="near apex" constraintid="o8075" is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o8075" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apices acute;</text>
      <biological_entity id="o8076" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa excurrent into a short, smooth or serrulate, yellowish or hyaline awn 1/8–1/5 the leaf length, yellow or red, rounded and sharply papillose-serrate abaxially, smooth on the adaxial surface;</text>
      <biological_entity id="o8077" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="into awn" constraintid="o8078" is_modifier="false" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o8078" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="yellowish" value_original="yellowish" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="1/8" name="quantity" src="d0_s4" to="1/5" />
      </biological_entity>
      <biological_entity id="o8079" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="length" src="d0_s4" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sharply; abaxially" name="architecture_or_shape" src="d0_s4" value="papillose-serrate" value_original="papillose-serrate" />
        <character constraint="on adaxial surface" constraintid="o8080" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8080" name="surface" name_original="surface" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>basal-cells gradually differentiated;</text>
      <biological_entity id="o8081" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal cells isodiametric, rounded-hexagonal, 14–22 µm, papillae abaxial, single, simple, rarely forked near the costa, cells rather thick-walled, collenchymatous;</text>
      <biological_entity constraint="distal" id="o8082" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-hexagonal" value_original="rounded-hexagonal" />
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s6" to="22" to_unit="um" />
      </biological_entity>
      <biological_entity id="o8083" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character constraint="near costa" constraintid="o8084" is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o8084" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o8085" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s6" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cells elongate near leaf apex.</text>
      <biological_entity constraint="leaf" id="o8087" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction by gemmae borne on the adaxial surface of the costa, spherical or ovoid, 4–10-celled, brown when mature, smooth.</text>
      <biological_entity id="o8088" name="gemma" name_original="gemmae" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8089" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="spherical" value_original="spherical" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="4-10-celled" value_original="4-10-celled" />
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8090" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <relation from="o8088" id="r1939" name="borne on" negation="false" src="d0_s8" to="o8089" />
      <relation from="o8089" id="r1940" name="part_of" negation="false" src="d0_s8" to="o8090" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition reportedly dioicous.</text>
      <biological_entity id="o8086" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character constraint="near leaf apex" constraintid="o8087" is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" modifier="reportedly" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Sporophytes not known from the flora region.</text>
      <biological_entity id="o8091" name="sporophyte" name_original="sporophytes" src="d0_s10" type="structure" />
      <biological_entity id="o8092" name="flora" name_original="flora" src="d0_s10" type="structure" />
      <biological_entity id="o8093" name="region" name_original="region" src="d0_s10" type="structure" />
      <relation from="o8091" id="r1941" name="known from the" negation="false" src="d0_s10" to="o8092" />
      <relation from="o8091" id="r1942" name="known from the" negation="false" src="d0_s10" to="o8093" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bark of trees or in rock crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bark" constraint="of trees" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="rock crevices" modifier="or in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S., Ont.; Ariz., Calif., Colo., Conn., Ga., Ill., Maine, Mass., Mich., Mo., Nev., N.J., N.Mex., N.Y., N.C., Ohio, Pa., Tenn., Tex., Utah, Wash.; Mexico; South America (Brazil, Colombia, Ecuador); Europe; s Africa; Atlantic Islands (Falkland Islands); Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="South America (Colombia)" establishment_means="native" />
        <character name="distribution" value="South America (Ecuador)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Falkland Islands)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>The leaves of Syntrichia papillosa have unipapillose cells, with the papillae only on the abaxial surface, strongly papillose-serrate costae, and small, smooth, brown propagula borne on the adaxial surface of the costa. The plants superficially resemble S. laevipila in the field, but the position and nature of the propagula as well as the incurved leaf margins and roughened back of the costa distinguish S. papillosa, even with a hand lens. Sporophytes are known only from Australia, Tasmania, and New Zealand.</discussion>
  
</bio:treatment>