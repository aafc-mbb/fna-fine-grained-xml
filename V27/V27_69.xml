<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">79</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="treatment_page">80</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sphagnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sphagnum</taxon_name>
    <taxon_name authority="(Lindberg) Schimper" date="1876" rank="section">subsecunda</taxon_name>
    <taxon_name authority="Flatberg" date="2005" rank="species">inexspectatum</taxon_name>
    <place_of_publication>
      <publication_title>Lindbergia</publication_title>
      <place_in_publication>30: 59. 2005,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sphagnaceae;genus sphagnum;section subsecunda;species inexspectatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075482</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">subsecundum</taxon_name>
    <taxon_name authority="H. A. Crum" date="unknown" rank="variety">andrusii</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species subsecundum;variety andrusii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphagnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">subsecundum</taxon_name>
    <taxon_name authority="(Warnstorf) H. A. Crum" date="unknown" rank="variety">junsaiense</taxon_name>
    <taxon_hierarchy>genus Sphagnum;species subsecundum;variety junsaiense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderate-sized, normally erect;</text>
    </statement>
    <statement id="d0_s1">
      <text>yellowish to reddish-brown, greenish in shaded forms;</text>
      <biological_entity id="o5357" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="moderate-sized" />
        <character is_modifier="false" modifier="normally" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s1" to="reddish-brown" />
        <character constraint="in shaded forms" is_modifier="false" name="coloration" src="d0_s1" value="greenish" value_original="greenish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>capitulum moderately distinct and rounded.</text>
      <biological_entity id="o5358" name="capitulum" name_original="capitulum" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems green to brown;</text>
      <biological_entity id="o5359" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>superficial cortex of partly 1 and partly 2 layers of thin-walled enlarged cells.</text>
      <biological_entity constraint="superficial" id="o5360" name="cortex" name_original="cortex" src="d0_s4" type="structure">
        <character modifier="partly" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character modifier="partly" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o5361" name="layer" name_original="layers" src="d0_s4" type="structure" />
      <biological_entity id="o5362" name="bud" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="true" name="size" src="d0_s4" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <relation from="o5361" id="r1491" name="part_of" negation="false" src="d0_s4" to="o5362" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves triangular-lingulate to ovate-lingulate, 0.8–0.9 mm, apex rounded, straight;</text>
      <biological_entity id="o5363" name="leaf-stem" name_original="stem-leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="triangular-lingulate" name="shape" src="d0_s5" to="ovate-lingulate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s5" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5364" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hyaline cells mostly nonseptate, fibrillose in distal 1/3–2/3 of leaf, a few ringed pores at corners of cells and along commissures on convex surface, ringed pores along the commissures on the concave surface in greater numbers than on convex surface.</text>
      <biological_entity id="o5365" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character constraint="in distal 1/3-2/3" constraintid="o5366" is_modifier="false" name="texture" src="d0_s6" value="fibrillose" value_original="fibrillose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o5366" name="1/3-2/3" name_original="1/3-2/3" src="d0_s6" type="structure" />
      <biological_entity id="o5367" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o5368" name="pore" name_original="pores" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="true" name="relief" src="d0_s6" value="ringed" value_original="ringed" />
      </biological_entity>
      <biological_entity id="o5369" name="commissure" name_original="commissures" src="d0_s6" type="structure" />
      <biological_entity id="o5370" name="pore" name_original="pores" src="d0_s6" type="structure">
        <character is_modifier="true" name="relief" src="d0_s6" value="ringed" value_original="ringed" />
      </biological_entity>
      <biological_entity id="o5371" name="commissure" name_original="commissures" src="d0_s6" type="structure" />
      <biological_entity id="o5372" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity constraint="greater" id="o5373" name="number" name_original="numbers" src="d0_s6" type="structure" />
      <relation from="o5366" id="r1492" name="part_of" negation="false" src="d0_s6" to="o5367" />
      <relation from="o5368" id="r1493" name="at corners of cells and along" negation="false" src="d0_s6" to="o5369" />
      <relation from="o5370" id="r1494" name="along" negation="false" src="d0_s6" to="o5371" />
      <relation from="o5371" id="r1495" name="on" negation="false" src="d0_s6" to="o5372" />
      <relation from="o5372" id="r1496" name="in" negation="false" src="d0_s6" to="o5373" />
    </statement>
    <statement id="d0_s7">
      <text>Branches short, not turgid.</text>
      <biological_entity id="o5374" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="turgid" value_original="turgid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch fascicles with 2–3 spreading and 1–2 pendent branches.</text>
      <biological_entity id="o5375" name="branch" name_original="branch" src="d0_s8" type="structure">
        <character constraint="with branches" constraintid="o5376" is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o5376" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves broad-ovate to ovatelanceolate, 0.9–2.1 mm, straight;</text>
      <biological_entity constraint="branch" id="o5377" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="broad-ovate" name="shape" src="d0_s9" to="ovatelanceolate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s9" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hyaline cells with numerous ringed pores (10–20) along the commissures on the convex surface, a few pseudopores and ringed pores (less than 8 per cell) occur on the cell angles on the concave surface.</text>
      <biological_entity id="o5379" name="pore" name_original="pores" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="relief" src="d0_s10" value="ringed" value_original="ringed" />
        <character char_type="range_value" constraint="along commissures" constraintid="o5380" from="10" name="atypical_quantity" src="d0_s10" to="20" />
      </biological_entity>
      <biological_entity id="o5380" name="commissure" name_original="commissures" src="d0_s10" type="structure" />
      <biological_entity id="o5381" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="convex" value_original="convex" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o5382" name="pseudopore" name_original="pseudopores" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="convex" value_original="convex" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o5383" name="pore" name_original="pores" src="d0_s10" type="structure">
        <character is_modifier="true" name="relief" src="d0_s10" value="ringed" value_original="ringed" />
      </biological_entity>
      <biological_entity constraint="cell" id="o5384" name="angle" name_original="angles" src="d0_s10" type="structure" />
      <biological_entity id="o5385" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="concave" value_original="concave" />
      </biological_entity>
      <relation from="o5378" id="r1497" name="with" negation="false" src="d0_s10" to="o5379" />
      <relation from="o5380" id="r1498" name="on" negation="false" src="d0_s10" to="o5381" />
      <relation from="o5380" id="r1499" name="on" negation="false" src="d0_s10" to="o5382" />
      <relation from="o5380" id="r1500" name="on" negation="false" src="d0_s10" to="o5383" />
      <relation from="o5383" id="r1501" name="occur on" negation="false" src="d0_s10" to="o5384" />
      <relation from="o5384" id="r1502" name="on" negation="false" src="d0_s10" to="o5385" />
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o5378" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule exserted, with few pseudostomata.</text>
      <biological_entity id="o5386" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o5387" name="pseudostomatum" name_original="pseudostomata" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="few" value_original="few" />
      </biological_entity>
      <relation from="o5386" id="r1503" name="with" negation="false" src="d0_s12" to="o5387" />
    </statement>
    <statement id="d0_s13">
      <text>Spores 36–39 µm; coarsely papillose on both surfaces;</text>
      <biological_entity id="o5388" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="36" from_unit="um" name="some_measurement" src="d0_s13" to="39" to_unit="um" />
        <character constraint="on surfaces" constraintid="o5389" is_modifier="false" modifier="coarsely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o5389" name="surface" name_original="surfaces" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>proximal laesura less than or equal to 0.5 spore radius.</text>
      <biological_entity constraint="proximal" id="o5390" name="laesurum" name_original="laesura" src="d0_s14" type="structure" />
      <biological_entity id="o5391" name="spore" name_original="spore" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equal" value_original="equal" />
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s14" to="0.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ecology unclear, but growing in carpets in depressions, blanket mires</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="carpets" modifier="ecology unclear but growing in" constraint="in depressions , blanket mires" />
        <character name="habitat" value="depressions" />
        <character name="habitat" value="blanket mires" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <discussion>Sphagnum inexpectatum is frequently collected with S. tenellum, S. pacificum, S. andersonianum, and S. rubellum in weakly minerotrophic blanket mires. It is similar in size to S. subsecundum, with which its range completely overlaps. The latter species has many of the branch leaves subsecund while those of S. inexspectatum are straight. The stem leaves of S. inexspectatum are also conspicuously larger than those of S. subsecundum.</discussion>
  <discussion>Microscopically Sphagnum inexspectatum has a stem cortex that has enlarged thin-walled cells that form 1–2 layers, whereas S. subsecundum has only one. The stem leaves of S. inexspectatum also have numerous commissural pores in the hyaline cells in the distal half of the concave surface, whereas S. subsecundum has only a few if any in this region and these are more free than commissural.</discussion>
  
</bio:treatment>