<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="treatment_page">421</other_info_on_meta>
    <other_info_on_meta type="illustration_page">422</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">dicranaceae</taxon_name>
    <taxon_name authority="I. Hagen" date="1915" rank="genus">kiaeria</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper) Brotherus" date="1923" rank="species">blyttii</taxon_name>
    <place_of_publication>
      <publication_title>Laubm. Fennoskand.,</publication_title>
      <place_in_publication>87. 1923,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dicranaceae;genus kiaeria;species blyttii</taxon_hierarchy>
    <other_info_on_name type="fna_id">240000065</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dicranum</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="species">blyttii</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Vetensk. Acad. Handl.</publication_title>
      <place_in_publication>34: 164. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dicranum;species blyttii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arctoa</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper) Loeske" date="unknown" rank="species">blyttii</taxon_name>
    <taxon_hierarchy>genus Arctoa;species blyttii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arctoa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">blyttii</taxon_name>
    <taxon_name authority="(R. S. Williams) Grout" date="unknown" rank="variety">hispidula</taxon_name>
    <taxon_hierarchy>genus Arctoa;species blyttii;variety hispidula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in loose or brown tufts, dark green, dull.</text>
      <biological_entity id="o5656" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5657" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="coloration" src="d0_s0" value="brown" value_original="brown" />
      </biological_entity>
      <relation from="o5656" id="r1335" name="in" negation="false" src="d0_s0" to="o5657" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–2 cm.</text>
      <biological_entity id="o5658" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Branch leaves erect-spreading, flexuose and somewhat crisped when dry, lanceolate, gradually subulate, 2–4 mm, margins occasionally distally 2-stratose;</text>
      <biological_entity constraint="branch" id="o5659" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="course" src="d0_s2" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5660" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="occasionally distally" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa 35–50 µm at base;</text>
      <biological_entity id="o5661" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o5662" from="35" from_unit="um" name="some_measurement" src="d0_s3" to="50" to_unit="um" />
      </biological_entity>
      <biological_entity id="o5662" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>distal laminal cells mostly subquadrate (1–2:1), 8–11 µm wide, papillose on the dorsal surface;</text>
      <biological_entity constraint="distal laminal" id="o5663" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s4" to="11" to_unit="um" />
        <character constraint="on dorsal surface" constraintid="o5664" is_modifier="false" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o5664" name="surface" name_original="surface" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells elongate, smooth, sometimes porose, alar cells gradually enlarged.</text>
      <biological_entity constraint="distal laminal" id="o5665" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o5666" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity id="o5667" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Perichaetial leaves similar to the cauline.</text>
      <biological_entity constraint="perichaetial" id="o5668" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perigonia terminal on separate branches or located well below the perichaetia.</text>
      <biological_entity id="o5669" name="perigonium" name_original="perigonia" src="d0_s7" type="structure">
        <character constraint="on " constraintid="o5671" is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o5670" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="separate" value_original="separate" />
      </biological_entity>
      <biological_entity id="o5671" name="perichaetium" name_original="perichaetia" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Capsule not ribbed when dry, urn 1–1.2 mm.</text>
      <biological_entity id="o5672" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when dry" name="architecture_or_shape" src="d0_s8" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o5673" name="urn" name_original="urn" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 13–23 µm.</text>
      <biological_entity id="o5674" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s9" to="23" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil in rock crevices or acid rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" constraint="in rock crevices or acid rock" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="acid rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>alpine elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., Que., Yukon; Alaska, Calif., Idaho, Maine, N.H., N.Y., Oreg., Wash.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Kiaeria blyttii is distinguished by position of the male inflorescence at the ends of separate branches or well below the perichaetia, giving the appearance of separate stems (polyoicous). In the field, it can be distinguished from small sterile forms of K. starkei by its more terrestrial habit, spreading leaves, dull, dark green or brownish color, and lack of grooves on the dry capsules.</discussion>
  
</bio:treatment>