<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="treatment_page">220</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="(Turner ex Robt. Scott) Bruch &amp; Schimper" date="1845" rank="species">maritimum</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>3: 102. 1845,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species maritimum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001329</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grimmia</taxon_name>
    <taxon_name authority="Turner ex Robt. Scott" date="unknown" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Dublin Soc.</publication_title>
      <place_in_publication>3: 158, plate 1. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grimmia;species maritima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schistidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">maritimum</taxon_name>
    <taxon_name authority="(I. Hagen) B. Bremer" date="unknown" rank="subspecies">piliferum</taxon_name>
    <taxon_hierarchy>genus Schistidium;species maritimum;subspecies piliferum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in tufts, olivaceous, often brownish.</text>
      <biological_entity id="o952" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o953" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o952" id="r270" name="in" negation="false" src="d0_s0" to="o953" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.8–5 cm, central strand absent.</text>
      <biological_entity id="o954" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o955" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually curved, sometimes erect, often somewhat contorted when dry, ovatelanceolate to linear-lanceolate, keeled, (1.2–) 1.5–2.6 (–4) mm, usually 2-stratose distally;</text>
      <biological_entity id="o956" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2.6" to_unit="mm" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins usually plane distally, recurved proximally, smooth or papillose, 2-stratose or 3-stratose, rarely 4-stratose distally;</text>
      <biological_entity id="o957" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-stratose" value_original="3-stratose" />
        <character is_modifier="false" modifier="rarely; distally" name="architecture" src="d0_s3" value="4-stratose" value_original="4-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apices acute or blunt, sometimes ending in a short, fleshy, multistratose apiculus;</text>
      <biological_entity id="o958" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o959" name="apiculu" name_original="apiculus" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="multistratose" value_original="multistratose" />
      </biological_entity>
      <relation from="o958" id="r271" modifier="sometimes" name="ending in" negation="false" src="d0_s4" to="o959" />
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent, rarely excurrent as a short denticulate awn, with guide cells and one or two stereid bands, abaxial surface usually slightly papillose;</text>
      <biological_entity id="o960" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character constraint="as awn" constraintid="o961" is_modifier="false" modifier="rarely" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o961" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="guide" id="o962" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o963" name="band" name_original="bands" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o964" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually slightly" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
      <relation from="o960" id="r272" name="with" negation="false" src="d0_s5" to="o962" />
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate or short-rectangular, often trigonous;</text>
      <biological_entity constraint="basal marginal" id="o965" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells isodiametric, rounded or angular, sometimes short-rectangular, 7–11 µm wide, smooth or weakly papillose, sometimes weakly bulging-mammillose, straight or slightly sinuose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="distal laminal" id="o966" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="angular" value_original="angular" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s7" to="11" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="sometimes weakly" name="relief" src="d0_s7" value="bulging-mammillose" value_original="bulging-mammillose" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule dark reddish-brown or dark-brown, ovoid, cupulate, or campanulate, 0.6–1.3 mm;</text>
      <biological_entity id="o967" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells usually angular, isodiametric, occasionally elongate, thick-walled;</text>
      <biological_entity constraint="exothecial" id="o968" name="bud" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="thick-walled" value_original="thick-walled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stomata present;</text>
      <biological_entity id="o969" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome patent to revolute, 200–500 µm, red, papillose, often strongly perforated.</text>
      <biological_entity id="o970" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="patent" value_original="patent" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s12" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="200" from_unit="um" name="some_measurement" src="d0_s12" to="500" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="often strongly" name="architecture" src="d0_s12" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 15–30 µm, granulose.</text>
      <biological_entity id="o971" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s13" to="30" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="granulose" value_original="granulose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocks within or near the spray zone along coasts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocks" />
        <character name="habitat" value="the spray zone" />
        <character name="habitat" value="coasts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations (0-20 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" constraint="low elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.B., Nfld. and Labr. (Nfld.), N.S., P.E.I., Que.; Alaska, Calif., Maine, Mass., Oreg., Wash.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion>Schistidium maritimum is one of the easiest species of the genus to identify. Its usually 2-stratose distal laminae, well developed stereid bands, usually small and often campanulate capsules, and coastal habitat are distinctive. Subspecies piliferum, characterized by the presence of awns and a single stereid layer, is not recognized here; further study is needed.</discussion>
  
</bio:treatment>