<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="treatment_page">113</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">tetraphidaceae</taxon_name>
    <taxon_name authority="Schwägrichen" date="1824" rank="genus">TETRODONTIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>2(1,2): 102. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family tetraphidaceae;genus TETRODONTIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek tetra, four, and odontos, tooth, alluding to peristome</other_info_on_name>
    <other_info_on_name type="fna_id">132664</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants very small, budlike, dull dark-green to brownish green, in scattered, gregarious or sometimes very small clumps.</text>
      <biological_entity id="o5753" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="very" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="budlike" value_original="budlike" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dull" value_original="dull" />
        <character char_type="range_value" from="dark-green" name="coloration" src="d0_s0" to="brownish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5754" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character is_modifier="true" modifier="sometimes very" name="size" src="d0_s0" value="small" value_original="small" />
      </biological_entity>
      <relation from="o5753" id="r1617" name="in" negation="false" src="d0_s0" to="o5754" />
    </statement>
    <statement id="d0_s1">
      <text>Thallose protonematal flaps persistent and usually present.</text>
      <biological_entity constraint="protonematal" id="o5755" name="flap" name_original="flaps" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="thallose" value_original="thallose" />
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems few, very short, less than 0.05 cm;</text>
      <biological_entity id="o5756" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="0.05" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>flagelliform shoots may occur at the base of the stem, 0.2–0.5 cm, with 3-ranked, tightly appressed linear to lanceolate leaves.</text>
      <biological_entity id="o5757" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5758" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o5759" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity id="o5760" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="3-ranked" value_original="3-ranked" />
        <character is_modifier="true" modifier="tightly" name="shape" src="d0_s3" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s3" to="lanceolate" />
      </biological_entity>
      <relation from="o5757" id="r1618" name="occur at" negation="false" src="d0_s3" to="o5758" />
      <relation from="o5758" id="r1619" name="part_of" negation="false" src="d0_s3" to="o5759" />
      <relation from="o5757" id="r1620" name="with" negation="false" src="d0_s3" to="o5760" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves of main-stem few, appressed, ovate, acuminate, or obtuse, margins entire or dentate, to 1.2 mm;</text>
      <biological_entity id="o5761" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o5762" name="main-stem" name_original="main-stem" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o5763" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
      <relation from="o5761" id="r1621" name="consist_of" negation="false" src="d0_s4" to="o5762" />
    </statement>
    <statement id="d0_s5">
      <text>costa single, weak or absent;</text>
      <biological_entity id="o5764" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="weak" value_original="weak" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminal cells rhombic or rectangular.</text>
      <biological_entity constraint="laminal" id="o5765" name="bud" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual structures absent.</text>
      <biological_entity id="o5766" name="structure" name_original="structures" src="d0_s7" type="structure">
        <character is_modifier="true" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition: perichaetial and perigonial buds occurring on the same protonema;</text>
      <biological_entity id="o5767" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity constraint="perichaetial and perigonial" id="o5768" name="bud" name_original="buds" src="d0_s8" type="structure" />
      <biological_entity id="o5769" name="protonemum" name_original="protonema" src="d0_s8" type="structure" />
      <relation from="o5768" id="r1622" name="occurring on the same" negation="false" src="d0_s8" to="o5769" />
    </statement>
    <statement id="d0_s9">
      <text>perichaetial leaves ovate to ovatelanceolate, concave.</text>
      <biological_entity id="o5770" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
      </biological_entity>
      <biological_entity constraint="perichaetial" id="o5771" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 3–8 mm, straight, smooth, slightly twisted when dry.</text>
      <biological_entity id="o5772" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s10" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule 0.6–1 mm, ovate to shortly oblong-cylindric, straight;</text>
      <biological_entity id="o5773" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="shortly oblong-cylindric" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum conic, obliquely apiculate;</text>
      <biological_entity id="o5774" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="obliquely" name="architecture_or_shape" src="d0_s12" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome usually not splitting or appearing to more than four.</text>
      <biological_entity id="o5775" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually not" name="architecture_or_dehiscence" src="d0_s13" value="splitting" value_original="splitting" />
        <character name="architecture_or_dehiscence" src="d0_s13" value="appearing" value_original="appearing" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Calyptra smooth or somewhat plicate, yellowish.</text>
      <biological_entity id="o5776" name="calyptra" name_original="calyptra" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s14" value="plicate" value_original="plicate" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores smooth or finely papillose, 10–16 µm.</text>
      <biological_entity id="o5777" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s15" to="16" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America (Chile); Eurasia, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants with numerous flagelliform shoots, protonematal flaps less than 0.5 mm.</description>
      <determination>1 Tetrodontium repandum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants without flagelliform shoots, protonematal flaps 0.5-2.5 mm.</description>
      <determination>2 Tetrodontium brownianum</determination>
    </key_statement>
  </key>
</bio:treatment>