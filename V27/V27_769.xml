<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="mention_page">533</other_info_on_meta>
    <other_info_on_meta type="mention_page">560</other_info_on_meta>
    <other_info_on_meta type="treatment_page">534</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="(Herzog) Hilpert" date="1933" rank="subfamily">barbuloideae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">barbula</taxon_name>
    <taxon_name authority="(Müller Hal.) Brotherus in H. G. A. Engler et al." date="1924" rank="species">bolleana</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler et al., Nat. Pflanzenfam. ed.</publication_title>
      <place_in_publication>2, 10: 280. 1924,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily barbuloideae;genus barbula;species bolleana</taxon_hierarchy>
    <other_info_on_name type="fna_id">242443944</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Meesia</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">bolleana</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Berlin)</publication_title>
      <place_in_publication>20: 338. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Meesia;species bolleana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Barbula</taxon_name>
    <taxon_name authority="(Lorentz) M. Fleischer" date="unknown" rank="species">ehrenbergii</taxon_name>
    <taxon_hierarchy>genus Barbula;species ehrenbergii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 3.5 cm.</text>
      <biological_entity id="o4319" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves lax when wet, short-ligulate to long-elliptic, 2–2.7 mm, base scarcely differentiated to ovate, not sheathing, margins plane or weakly recurved in proximal 1/2 to 3/4, apex narrowly to broadly obtuse, occasionally broadly acute, entire or weakly apiculate;</text>
      <biological_entity id="o4320" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when wet" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="short-ligulate" value_original="short-ligulate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="long-elliptic" value_original="long-elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4321" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="scarcely" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o4322" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character constraint="in proximal 1/2-3/4" constraintid="o4323" is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4323" name="1/2-3/4" name_original="1/2-3/4" src="d0_s1" type="structure" />
      <biological_entity id="o4324" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="occasionally broadly" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s1" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa percurrent to ending 2–3 cells before the apex, abaxial costal surface smooth, hydroids absent;</text>
      <biological_entity id="o4325" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o4326" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <biological_entity id="o4327" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity constraint="abaxial costal" id="o4328" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o4329" name="hydroid" name_original="hydroids" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o4325" id="r1033" name="ending" negation="false" src="d0_s2" to="o4326" />
      <relation from="o4325" id="r1034" name="before" negation="false" src="d0_s2" to="o4327" />
    </statement>
    <statement id="d0_s3">
      <text>distal laminal cells lax-walled, shortrectangular, 11–15 µm wide, 1–2: 1, smooth or with low, simple papillae.</text>
      <biological_entity constraint="distal laminal" id="o4330" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="lax-walled" value_original="lax-walled" />
        <character char_type="range_value" from="11" from_unit="um" name="width" src="d0_s3" to="15" to_unit="um" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="with low" value_original="with low" />
      </biological_entity>
      <biological_entity id="o4331" name="low" name_original="low" src="d0_s3" type="structure" />
      <relation from="o4330" id="r1035" name="with" negation="false" src="d0_s3" to="o4331" />
    </statement>
    <statement id="d0_s4">
      <text>Specialized asexual reproduction by fusiform or armed gemmae borne on stalks in leaf-axils, to 185 µm. Sporophytes absent in the flora area.</text>
      <biological_entity id="o4332" name="papilla" name_original="papillae" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="development" src="d0_s4" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o4333" name="gemma" name_original="gemmae" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="shape" src="d0_s4" value="fusiform" value_original="fusiform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="armed" value_original="armed" />
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" notes="" src="d0_s4" to="185" to_unit="um" />
      </biological_entity>
      <biological_entity id="o4334" name="stalk" name_original="stalks" src="d0_s4" type="structure" />
      <biological_entity id="o4335" name="leaf-axil" name_original="leaf-axils" src="d0_s4" type="structure" />
      <biological_entity id="o4336" name="sporophyte" name_original="sporophytes" src="d0_s4" type="structure">
        <character constraint="in flora, area" constraintid="o4337, o4338" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4337" name="flora" name_original="flora" src="d0_s4" type="structure" />
      <biological_entity id="o4338" name="area" name_original="area" src="d0_s4" type="structure" />
      <relation from="o4333" id="r1036" name="borne on" negation="false" src="d0_s4" to="o4334" />
      <relation from="o4333" id="r1037" name="in" negation="false" src="d0_s4" to="o4335" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet limestone, moist areas, wet rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet limestone" />
        <character name="habitat" value="moist areas" />
        <character name="habitat" value="wet rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Ark., Mo., N.Mex., Okla., Tex., Utah; Mexico; West Indies; Eurasia; n Africa; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>The name Barbula bolleana replaces the long familiar B. ehrenbergii (J.-P. Frahm et al. 1996). The lax, largely smooth distal leaf cells, easily seen with the dissecting microscope, distinguish this hygrophilic species. Specimens with largely quadrate distal laminal cells may appear to have a weak intramarginal border along the leaf base of somewhat elongate cells. This species may be confused with Didymodon tophaceus, which generally has smaller leaf cells (9–12 µm wide) in fewer longitudinal rows (10–15 for D. tophaceus, 20–30 for B. bolleana), and often has long-decurrent leaf margins. Barbula indica is rarely also tufa forming. A specimen (Patterson 2185, NY) previously reported for Virginia (Frederick County) is Didymodon tophaceus.</discussion>
  
</bio:treatment>