<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">617</other_info_on_meta>
    <other_info_on_meta type="illustration_page">617</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="(Müller Hal.) Kindberg" date="1882" rank="genus">aloina</taxon_name>
    <taxon_name authority="(Hedwig) Limpricht" date="1888" rank="species">rigida</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">rigida</taxon_name>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus aloina;species rigida;variety rigida</taxon_hierarchy>
    <other_info_on_name type="fna_id">250061634</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 2.5 mm.</text>
      <biological_entity id="o6128" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="2.5" to_unit="mm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves short-lingulate to ligulate, 0.5–2.5 mm, margins entire to irregularly denticulate, differentiated at base, apex cucullate;</text>
      <biological_entity id="o6129" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="short-lingulate" value_original="short-lingulate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6130" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s1" to="irregularly denticulate" />
        <character constraint="at base" constraintid="o6131" is_modifier="false" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o6131" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o6132" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="cucullate" value_original="cucullate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>costa subpercurrent to percurrent, filaments of 3–9 cells, cells cylindric to spheric;</text>
      <biological_entity id="o6133" name="costa" name_original="costa" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o6134" name="filament" name_original="filaments" src="d0_s2" type="structure" />
      <biological_entity id="o6135" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="9" />
      </biological_entity>
      <biological_entity id="o6136" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s2" to="spheric" />
      </biological_entity>
      <relation from="o6134" id="r1448" name="consist_of" negation="false" src="d0_s2" to="o6135" />
    </statement>
    <statement id="d0_s3">
      <text>cells of leaf base 11–88 µm, medial and distal cells 9–40 µm, papillae none.</text>
      <biological_entity id="o6137" name="cell" name_original="cells" src="d0_s3" type="structure" constraint="base" constraint_original="base; base">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s3" to="88" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o6138" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="medial and distal" id="o6139" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s3" to="40" to_unit="um" />
      </biological_entity>
      <relation from="o6137" id="r1449" name="part_of" negation="false" src="d0_s3" to="o6138" />
    </statement>
    <statement id="d0_s4">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o6140" name="papilla" name_original="papillae" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="0" value_original="none" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s4" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta 4.5–17 mm.</text>
      <biological_entity id="o6141" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule urn ovoid-cylindric, 1.7–3.4 mm;</text>
      <biological_entity constraint="capsule" id="o6142" name="urn" name_original="urn" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid-cylindric" value_original="ovoid-cylindric" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s6" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>operculum conical to subulate, long-rostrate, 1.2–1.9 mm;</text>
      <biological_entity id="o6143" name="operculum" name_original="operculum" src="d0_s7" type="structure">
        <character char_type="range_value" from="conical" name="shape" src="d0_s7" to="subulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="long-rostrate" value_original="long-rostrate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s7" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peristome 1200–1750 µm, strongly twisted.</text>
      <biological_entity id="o6144" name="peristome" name_original="peristome" src="d0_s8" type="structure">
        <character char_type="range_value" from="1200" from_unit="um" name="some_measurement" src="d0_s8" to="1750" to_unit="um" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 11–22 µm.</text>
      <biological_entity id="o6145" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s9" to="22" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jun–Aug(-Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Jun" />
        <character name="capsules maturing time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocks, banks, clay, sandy or gravelly soil in deserts, plains or conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocks" />
        <character name="habitat" value="banks" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="sandy" constraint="in deserts , plains or conifer forests" />
        <character name="habitat" value="gravelly soil" constraint="in deserts , plains or conifer forests" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="plains" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1000-3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1000" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.B., N.S., Nunavut, Ont., Yukon; Colo., Ill., Iowa, Kans., Mont., Nebr., N.Mex., Tex.; Mexico (Nuevo León, Querétaro, Zacatecas); South America; Europe; Asia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Querétaro)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  <discussion>The ovoid-cylindrical capsule with a long peristome and long-rostrate operculum as well as the differentiated basal leaf margins are the distinguishing features of var. rigida. Despite recent reports from California, this species has not been confirmed from that state.</discussion>
  
</bio:treatment>