<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="treatment_page">644</other_info_on_meta>
    <other_info_on_meta type="illustration_page">645</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. Koponen" date="unknown" rank="family">splachnobryaceae</taxon_name>
    <taxon_name authority="Müller Hal." date="1869" rank="genus">splachnobryum</taxon_name>
    <taxon_name authority="(Bridel) Müller Hal." date="1869" rank="species">obtusum</taxon_name>
    <place_of_publication>
      <publication_title>Verh. K. K. Zool.-Bot. Ges. Wien</publication_title>
      <place_in_publication>19: 504. 1869,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family splachnobryaceae;genus splachnobryum;species obtusum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001362</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Weissia</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">obtusa</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Recent., suppl.</publication_title>
      <place_in_publication>1: 118. 1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Weissia;species obtusa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Splachnobryum</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">bernoullii</taxon_name>
    <taxon_hierarchy>genus Splachnobryum;species bernoullii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants gregarious, small, dull, often encrusted with soil.</text>
      <biological_entity id="o6113" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character constraint="with soil" constraintid="o6114" is_modifier="false" modifier="often" name="habitat" src="d0_s0" value="encrusted" value_original="encrusted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6114" name="soil" name_original="soil" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 1 cm but mostly much shorter, distal cell of axillary hairs swollen, somewhat asymmetric, colorless, often encrusted.</text>
      <biological_entity id="o6115" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="mostly much" name="height_or_length_or_size" src="d0_s1" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6116" name="cell" name_original="cell" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s1" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="colorless" value_original="colorless" />
        <character is_modifier="false" modifier="often" name="habitat" src="d0_s1" value="encrusted" value_original="encrusted" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o6117" name="hair" name_original="hairs" src="d0_s1" type="structure" />
      <relation from="o6116" id="r1445" name="part_of" negation="false" src="d0_s1" to="o6117" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves soft, commonly shriveled when dry, rarely uncontorted or nearly so, 0.6–0.8 mm, oblong to obovate or spatulate, sometimes decurrent from costa and margins, margins plane, or a little recurved proximally;</text>
      <biological_entity id="o6118" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s2" value="shriveled" value_original="shriveled" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="uncontorted" value_original="uncontorted" />
        <character name="shape" src="d0_s2" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s2" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="obovate or spatulate" />
        <character constraint="from margins" constraintid="o6120" is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o6119" name="costa" name_original="costa" src="d0_s2" type="structure" />
      <biological_entity id="o6120" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <biological_entity id="o6121" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>costa weak to strong, 1/2 leaf length to nearly percurrent, sometimes spurred or forked, or both, distally.</text>
      <biological_entity id="o6122" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character char_type="range_value" from="weak" name="fragility" src="d0_s3" to="strong" />
        <character name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Specialized asexual reproduction by gemmae on axillary rhizoids, rare;</text>
      <biological_entity id="o6123" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="length" src="d0_s3" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" name="shape" src="d0_s3" value="forked" value_original="forked" />
        <character is_modifier="false" name="development" src="d0_s4" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o6124" name="gemma" name_original="gemmae" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o6125" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="rare" value_original="rare" />
      </biological_entity>
      <relation from="o6124" id="r1446" name="on" negation="false" src="d0_s4" to="o6125" />
    </statement>
    <statement id="d0_s5">
      <text>rhizoid tubers inconspicuous, of several bulging cells in linear arrangement.</text>
      <biological_entity constraint="rhizoid" id="o6126" name="tuber" name_original="tubers" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o6127" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="several" value_original="several" />
        <character is_modifier="true" name="pubescence_or_shape" src="d0_s5" value="bulging" value_original="bulging" />
      </biological_entity>
      <relation from="o6126" id="r1447" name="consist_of" negation="false" src="d0_s5" to="o6127" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Not producing sporophytes in the flora area. Exposed sites on damp or periodically wet limestone, marl, calcareous soil, mortar-work</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sporophytes" modifier="not producing" constraint="in the flora area" />
        <character name="habitat" value="the flora area" />
        <character name="habitat" value="sites" constraint="on damp or periodically wet limestone , marl , calcareous soil , mortar-work" />
        <character name="habitat" value="damp" />
        <character name="habitat" value="wet limestone" modifier="periodically" />
        <character name="habitat" value="marl" />
        <character name="habitat" value="calcareous soil" />
        <character name="habitat" value="mortar-work" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Fla., La., Okla., Tex.; Mexico; West Indies; Central America; South America; Africa; Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Splachnobryum obtusum is almost entirely restricted to base-rich substrates in the flora area and probably elsewhere in its broad range. It is an obscure moss, difficult to find in the field because of its small size, drab aspect, and absence of field characters. Most specimens from the flora area are tiny, poorly developed plants with short stems and small leaves. In the flora area, underdeveloped specimens of various other mosses, mostly Bryaceae and Pottiaceae, are sometimes misidentified as Splachnobryum. Under the microscope the leaf shape, crenulate distal leaf margin, and distal leaf cells in ascending rows diverging from the costa, are helpful for identification. The oddly shaped axillary hairs also are helpful, but often difficult to demonstrate. The leaves of many specimens of this moss are difficult to rehydrate after drying. Plants with archegonia are common in at least some of the material seen. Rhizoid tubers, as reported by T. Arts (1996) for African specimens of S. obtusum, are present in at least some of the specimens from the flora area, including the New Orleans material.</discussion>
  
</bio:treatment>