<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/20 13:47:35</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="mention_page">156</other_info_on_meta>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="illustration_page">144</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwägrichen" date="unknown" rank="family">polytrichaceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1805" rank="genus">oligotrichum</taxon_name>
    <taxon_name authority="(Mitten) Kindberg" date="1896" rank="species">parallelum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not.</publication_title>
      <place_in_publication>1896: 191. 1896,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polytrichaceae;genus oligotrichum;species parallelum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250063208</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atrichum</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">parallelum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 48, plate 8. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Atrichum;species parallelum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atrichum</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">leiophyllum</taxon_name>
    <taxon_hierarchy>genus Atrichum;species leiophyllum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants pale to dark olive green, in loose tufts.</text>
      <biological_entity id="o3531" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s0" to="dark olive" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3532" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o3531" id="r955" name="in" negation="false" src="d0_s0" to="o3532" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–6 (–8) cm, simple or branching by innovations, bracteate proximally, gradually larger distally.</text>
      <biological_entity id="o3533" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="by innovations" constraintid="o3534" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="proximally" name="architecture" notes="" src="d0_s1" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s1" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o3534" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 5–6 mm, crisped and contorted when dry, when moist laxly spreading, plane, ovatelanceolate to elliptic-oblong, abruptly acuminate at the apex, ending in a short spine, the margins coarsely and sharply serrate to the middle or below;</text>
      <biological_entity id="o3535" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when moist laxly spreading" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s2" to="elliptic-oblong" />
        <character constraint="at apex" constraintid="o3536" is_modifier="false" modifier="abruptly" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o3536" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o3537" name="spine" name_original="spine" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o3538" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="to middle leaves" constraintid="o3539" is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="middle" id="o3539" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o3535" id="r956" name="ending in" negation="false" src="d0_s2" to="o3537" />
    </statement>
    <statement id="d0_s3">
      <text>costa percurrent or shortly excurrent in the apex, with a few low dentate abaxial lamellae or lamelliform teeth near the apex;</text>
      <biological_entity id="o3540" name="costa" name_original="costa" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="percurrent" value_original="percurrent" />
        <character constraint="in apex" constraintid="o3541" is_modifier="false" modifier="shortly" name="architecture" src="d0_s3" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o3541" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o3542" name="lamella" name_original="lamellae" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="true" name="position" src="d0_s3" value="low" value_original="low" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o3543" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="lamelliform" value_original="lamelliform" />
      </biological_entity>
      <biological_entity id="o3544" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <relation from="o3540" id="r957" name="with" negation="false" src="d0_s3" to="o3542" />
      <relation from="o3540" id="r958" name="with" negation="false" src="d0_s3" to="o3543" />
      <relation from="o3543" id="r959" name="near" negation="false" src="d0_s3" to="o3544" />
    </statement>
    <statement id="d0_s4">
      <text>abaxial surface of lamina smooth or with a few scattered teeth;</text>
      <biological_entity constraint="lamina" id="o3545" name="surface" name_original="surface" src="d0_s4" type="structure" constraint_original="lamina abaxial; lamina">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="with a few scattered teeth" />
      </biological_entity>
      <biological_entity id="o3546" name="lamina" name_original="lamina" src="d0_s4" type="structure" />
      <biological_entity id="o3547" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o3545" id="r960" name="part_of" negation="false" src="d0_s4" to="o3546" />
      <relation from="o3545" id="r961" name="with" negation="false" src="d0_s4" to="o3547" />
    </statement>
    <statement id="d0_s5">
      <text>adaxial lamellae 4–6, straight, not undulate, 3–5 (–8) cells high, entire in profile;</text>
      <biological_entity constraint="adaxial" id="o3548" name="lamella" name_original="lamellae" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="8" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o3549" name="bud" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="height" src="d0_s5" value="high" value_original="high" />
        <character is_modifier="false" modifier="in profile" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>median basal-cells short-rectangular, 3: 1, rather thin-walled, finely striate-papillose;</text>
      <biological_entity constraint="median" id="o3550" name="basal-bud" name_original="basal-cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s6" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s6" value="striate-papillose" value_original="striate-papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>median cells of lamina 20–30 µm, subquadrate to hexagonal, thin to firm-walled with minute trigones, smooth;</text>
      <biological_entity constraint="median" id="o3551" name="basal-bud" name_original="basal-cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="median" id="o3552" name="bud" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s7" to="30" to_unit="um" />
        <character char_type="range_value" from="subquadrate" name="shape" src="d0_s7" to="hexagonal" />
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character constraint="with trigones" constraintid="o3554" is_modifier="false" name="architecture" src="d0_s7" value="firm-walled" value_original="firm-walled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o3553" name="lamina" name_original="lamina" src="d0_s7" type="structure" />
      <biological_entity id="o3554" name="trigone" name_original="trigones" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o3552" id="r962" name="part_of" negation="false" src="d0_s7" to="o3553" />
    </statement>
    <statement id="d0_s8">
      <text>1–3 rows of cells along the margins evenly thicker-walled, often colored, the marginal serrations ending in a short tooth cell, with 2–4 supporting cells;</text>
      <biological_entity constraint="median" id="o3555" name="basal-bud" name_original="basal-cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="short-rectangular" value_original="short-rectangular" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o3556" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" notes="" src="d0_s8" value="colored" value_original="colored" />
      </biological_entity>
      <biological_entity id="o3557" name="bud" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o3558" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s8" value="thicker-walled" value_original="thicker-walled" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o3559" name="serration" name_original="serrations" src="d0_s8" type="structure" />
      <biological_entity constraint="tooth" id="o3560" name="cell" name_original="cell" src="d0_s8" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character char_type="range_value" from="2" modifier="with" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o3561" name="bud" name_original="cells" src="d0_s8" type="structure" />
      <relation from="o3556" id="r963" name="part_of" negation="false" src="d0_s8" to="o3557" />
      <relation from="o3556" id="r964" name="along" negation="false" src="d0_s8" to="o3558" />
      <relation from="o3559" id="r965" name="ending in" negation="false" src="d0_s8" to="o3560" />
      <relation from="o3559" id="r966" name="supporting" negation="false" src="d0_s8" to="o3561" />
    </statement>
    <statement id="d0_s9">
      <text>perichaetial leaves 6.5–7 mm, subsheathing at base, spreading.</text>
      <biological_entity constraint="median" id="o3562" name="basal-bud" name_original="basal-cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rectangular" value_original="short-rectangular" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="perichaetial" id="o3563" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character constraint="at base" constraintid="o3564" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="subsheathing" value_original="subsheathing" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o3564" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Seta to 5 cm, 1 (–2) per perichaetium, reddish, wiry.</text>
      <biological_entity id="o3565" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s10" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="2" />
        <character constraint="per perichaetium" constraintid="o3566" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="wiry" value_original="wiry" />
      </biological_entity>
      <biological_entity id="o3566" name="perichaetium" name_original="perichaetium" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Capsule 4–7 mm, light yellowish-brown, greenish near base, reddish at the mouth, cylindric, slightly curved, somewhat larger toward the base, strongly contracted below the mouth when dry, the surface rugose;</text>
      <biological_entity id="o3567" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="light yellowish-brown" value_original="light yellowish-brown" />
        <character constraint="near base" constraintid="o3568" is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character constraint="at mouth" constraintid="o3569" is_modifier="false" name="coloration" notes="" src="d0_s11" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character constraint="toward base" constraintid="o3570" is_modifier="false" modifier="somewhat" name="size" src="d0_s11" value="larger" value_original="larger" />
        <character constraint="below mouth" constraintid="o3571" is_modifier="false" modifier="strongly" name="condition_or_size" notes="" src="d0_s11" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o3568" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o3569" name="mouth" name_original="mouth" src="d0_s11" type="structure" />
      <biological_entity id="o3570" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o3571" name="mouth" name_original="mouth" src="d0_s11" type="structure" />
      <biological_entity id="o3572" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="when dry" name="relief" src="d0_s11" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exothecial cells irregularly short-rectangular to hexagonal, rather thin-walled, smooth, much smaller and thicker-walled below the mouth;</text>
      <biological_entity constraint="exothecial" id="o3573" name="bud" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="irregularly short-rectangular" name="shape" src="d0_s12" to="hexagonal" />
        <character is_modifier="false" modifier="rather" name="architecture" src="d0_s12" value="thin-walled" value_original="thin-walled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s12" value="smaller" value_original="smaller" />
        <character constraint="below mouth" constraintid="o3574" is_modifier="false" name="architecture" src="d0_s12" value="thicker-walled" value_original="thicker-walled" />
      </biological_entity>
      <biological_entity id="o3574" name="mouth" name_original="mouth" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stomata superficial, numerous in proximal 1/4 of capsule;</text>
      <biological_entity id="o3575" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="superficial" value_original="superficial" />
        <character constraint="in proximal 1/4" constraintid="o3576" is_modifier="false" name="quantity" src="d0_s13" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3576" name="1/4" name_original="1/4" src="d0_s13" type="structure" />
      <biological_entity id="o3577" name="capsule" name_original="capsule" src="d0_s13" type="structure" />
      <relation from="o3576" id="r967" name="part_of" negation="false" src="d0_s13" to="o3577" />
    </statement>
    <statement id="d0_s14">
      <text>peristome teeth 32, obtuse to truncate at the apex.</text>
      <biological_entity constraint="peristome" id="o3578" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
        <character char_type="range_value" constraint="at apex" constraintid="o3579" from="obtuse" name="shape" src="d0_s14" to="truncate" />
      </biological_entity>
      <biological_entity id="o3579" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Spores 12–16 µm.</text>
      <biological_entity id="o3580" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s15" to="16" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet rocks and soil, shaded banks, wet cliffs beside waterfalls, cutbanks in ravines, over boulders in creeks, at higher elevations in late snow areas, drainage channels on steep slopes, beside snowmelt streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet rocks" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="shaded banks" />
        <character name="habitat" value="wet cliffs" constraint="beside waterfalls" />
        <character name="habitat" value="waterfalls" />
        <character name="habitat" value="ravines" modifier="cutbanks in" />
        <character name="habitat" value="boulders" modifier="over" constraint="in creeks" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="higher elevations" modifier="at" constraint="in late snow areas , drainage channels on steep slopes" />
        <character name="habitat" value="late snow areas" />
        <character name="habitat" value="drainage channels" constraint="on steep slopes" />
        <character name="habitat" value="steep slopes" />
        <character name="habitat" value="snowmelt streams" modifier="beside" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (1000-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska, Wash.; Asia (Japan, Russia in Kamchatka).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Russia in Kamchatka)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Oligotrichum parallelum is an oceanic, arctic-montane species occurring northward in interior Alaska to 67°N. When optimally developed O. parallelum is a handsome plant, differing from other regional Oligotrichum species in its larger size, leaves crisped when dry, laxly spreading when moist, with coarsely, and often doubly serrate margins, and lower, straight lamellae. The leaf margins may occasionally show a hint of a border of somewhat elongated cells. Typically, however, the marginal cells are ± isodiametric, thicker-walled, and yellowish.</discussion>
  
</bio:treatment>