<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:09:16</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">210</other_info_on_meta>
    <other_info_on_meta type="mention_page">211</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
    <other_info_on_meta type="illustration_page">224</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Arnott" date="unknown" rank="family">grimmiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1902" rank="subfamily">grimmioideae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="1845" rank="genus">schistidium</taxon_name>
    <taxon_name authority="H. H. Blom" date="1996" rank="species">subjulaceum</taxon_name>
    <place_of_publication>
      <publication_title>Bryophyt. Biblioth.</publication_title>
      <place_in_publication>49: 161, fig. 61. 1996,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus schistidium;species subjulaceum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075421</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in open to somewhat compact tufts, olivaceous, brownish, or nearly black.</text>
      <biological_entity id="o5630" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5631" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open to somewhat" value_original="open to somewhat" />
        <character is_modifier="true" modifier="somewhat" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
      </biological_entity>
      <relation from="o5630" id="r1331" name="in" negation="false" src="d0_s0" to="o5631" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1.3–2 (–3) cm, central strand distinct.</text>
      <biological_entity id="o5632" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o5633" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect or curved, sometimes curved towards stem, imbricate, sometimes weakly spreading when dry, ovate-triangular to ovatelanceolate, keeled distally, 1.2–1.8 (–2.2) mm, 1-stratose, rarely with 2-stratose striae or patches distally;</text>
      <biological_entity id="o5634" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character constraint="towards stem" constraintid="o5635" is_modifier="false" modifier="sometimes" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="ovate-triangular" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s2" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o5635" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o5636" name="stria" name_original="striae" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o5637" name="patch" name_original="patches" src="d0_s2" type="structure" />
      <relation from="o5634" id="r1332" modifier="rarely" name="with" negation="false" src="d0_s2" to="o5636" />
      <relation from="o5634" id="r1333" modifier="rarely" name="with" negation="false" src="d0_s2" to="o5637" />
    </statement>
    <statement id="d0_s3">
      <text>margins usually recurved to near apex, smooth or rarely weakly denticulate distally, 2-stratose, occasionally 3-stratose, often in more than one row;</text>
      <biological_entity id="o5638" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="to apex" constraintid="o5639" is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="rarely weakly; distally" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s3" value="3-stratose" value_original="3-stratose" />
      </biological_entity>
      <biological_entity id="o5639" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o5641" name="row" name_original="row" src="d0_s3" type="structure" />
      <biological_entity id="o5640" name="row" name_original="row" src="d0_s3" type="structure" />
      <relation from="o5640" id="r1334" modifier="often; often; often; often" name="in" negation="false" src="d0_s3" to="o5641" />
    </statement>
    <statement id="d0_s4">
      <text>apices acute or somewhat rounded;</text>
      <biological_entity id="o5642" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa subpercurrent, percurrent, or excurrent as a short, denticulate, slightly decurrent awn, abaxial surface occasionally weakly papillose;</text>
      <biological_entity id="o5643" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character constraint="as awn" constraintid="o5644" is_modifier="false" name="architecture" src="d0_s5" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o5644" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="true" modifier="slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5645" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="occasionally weakly" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal marginal cells quadrate or short-rectangular;</text>
      <biological_entity constraint="basal marginal" id="o5646" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells mostly isodiametric or ovate, occasionally short-rectangular, 9–13 µm wide, smooth, weakly sinuose, walls usually pale orange or yellowish.</text>
      <biological_entity constraint="distal laminal" id="o5647" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="9" from_unit="um" name="width" src="d0_s7" to="13" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s7" value="sinuose" value_original="sinuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity id="o5648" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="pale orange" value_original="pale orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule light (yellow) brown, occasionally redbrown, short-cylindric or ovoid, usually narrowed towards the mouth, 0.8–1.4 (–1.7) mm;</text>
      <biological_entity id="o5649" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light brown" value_original="light brown" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s9" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-cylindric" value_original="short-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character constraint="towards mouth" constraintid="o5650" is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s9" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5650" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells mostly irregularly angular, short-elongate or isodiametric, rarely oblate, walls thin or moderately and unevenly thickened, often somewhat curved, trigonous;</text>
      <biological_entity constraint="exothecial" id="o5651" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="mostly irregularly" name="shape" src="d0_s10" value="angular" value_original="angular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="oblate" value_original="oblate" />
      </biological_entity>
      <biological_entity id="o5652" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character name="width" src="d0_s10" value="moderately" value_original="moderately" />
        <character is_modifier="false" modifier="unevenly" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="often somewhat" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s10" value="trigonous" value_original="trigonous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stomata present;</text>
      <biological_entity id="o5653" name="stoma" name_original="stomata" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peristome patent to squarrose-recurved, often twisted, 300–450 µm, dull dark red or brown, densely papillose, usually entire, sometimes weakly perforated.</text>
      <biological_entity id="o5654" name="peristome" name_original="peristome" src="d0_s12" type="structure">
        <character char_type="range_value" from="patent" name="orientation" src="d0_s12" to="squarrose-recurved" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s12" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="300" from_unit="um" name="some_measurement" src="d0_s12" to="450" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes weakly" name="architecture" src="d0_s12" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 11–18 µm, granulose or verruculose.</text>
      <biological_entity id="o5655" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s13" to="18" to_unit="um" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="granulose" value_original="granulose" />
        <character is_modifier="false" name="relief" src="d0_s13" value="verruculose" value_original="verruculose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring to early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet to dry rocks in or along water courses or in periodically wet sites such as in crevices or on ledges, rarely on rocks well away from wet areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet to dry rocks" />
        <character name="habitat" value="water courses" modifier="in or along" />
        <character name="habitat" value="wet sites" modifier="or in periodically" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="rocks" modifier="rarely on" />
        <character name="habitat" value="wet areas" modifier="well away from" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1000-1600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1000" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Yukon; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <discussion>Schistidium subjulaceum appears closely related to S. rivulare. Its smaller, more compact stature, and the consistently larger, often lighter colored capsules that are usually slightly narrowed towards the mouth separate it from S. rivulare.</discussion>
  
</bio:treatment>