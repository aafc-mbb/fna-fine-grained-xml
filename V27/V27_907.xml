<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/19 21:10:38</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="treatment_page">621</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pottiaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler et al." date="1924" rank="subfamily">pottioideae</taxon_name>
    <taxon_name authority="Bridel" date="1801" rank="genus">syntrichia</taxon_name>
    <taxon_name authority="(Steere) R. H. Zander" date="1993" rank="species">bartramii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Buffalo Soc. Nat. Sci.</publication_title>
      <place_in_publication>32: 267. 1993,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pottiaceae;subfamily pottioideae;genus syntrichia;species bartramii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075536</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tortula</taxon_name>
    <taxon_name authority="Steere" date="unknown" rank="species">bartramii</taxon_name>
    <place_of_publication>
      <publication_title>in A. J. Grout, Moss Fl. N. Amer.</publication_title>
      <place_in_publication>1: 241, plate 115, fig. B. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tortula;species bartramii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 2–10 mm.</text>
      <biological_entity id="o517" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves infolded and spirally twisted around the stem when dry, widespreading when moist, lingulate to spatulate, 1.25–2 × 0.5–0.75 mm;</text>
      <biological_entity id="o518" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="infolded" value_original="infolded" />
        <character constraint="around stem" constraintid="o519" is_modifier="false" modifier="spirally" name="architecture" src="d0_s1" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o519" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s1" value="widespreading" value_original="widespreading" />
        <character char_type="range_value" from="lingulate" name="shape" src="d0_s1" to="spatulate" />
        <character char_type="range_value" from="1.25" from_unit="mm" name="length" src="d0_s1" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="0.75" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins plane, entire, except for papillose crenulations, not bordered;</text>
      <biological_entity id="o520" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s2" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o521" name="crenulation" name_original="crenulations" src="d0_s2" type="structure">
        <character is_modifier="true" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>apices acute to truncate or occasionally emarginate;</text>
      <biological_entity id="o522" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s3" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>costa excurrent into a conspicuously tapered, serrate awn 0.1–0.6 mm, red or yellow, sparsely to densely spinulose abaxially;</text>
      <biological_entity id="o523" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character constraint="into awn" constraintid="o524" is_modifier="false" name="architecture" src="d0_s4" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sparsely to densely; abaxially" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o524" name="awn" name_original="awn" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="conspicuously" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal-cells abruptly differentiated, with somewhat thickened cross-walls, those at the margins narrower;</text>
      <biological_entity id="o525" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o526" name="cross-wall" name_original="cross-walls" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="somewhat" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o527" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <relation from="o525" id="r130" name="with" negation="false" src="d0_s5" to="o526" />
      <relation from="o525" id="r131" name="at" negation="false" src="d0_s5" to="o527" />
    </statement>
    <statement id="d0_s6">
      <text>distal cells irregularly polygonal, isodiametric, 9–13 µm, obscure, bulging and densely papillose, with 4–6 papillae per cell, moderately thick-walled and not collenchymatous.</text>
      <biological_entity id="o529" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o530" name="cell" name_original="cell" src="d0_s6" type="structure" />
      <relation from="o528" id="r132" name="with" negation="false" src="d0_s6" to="o529" />
      <relation from="o529" id="r133" name="per" negation="false" src="d0_s6" to="o530" />
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous (perigonia and sporophytes unknown).</text>
      <biological_entity constraint="distal" id="o528" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s6" value="polygonal" value_original="polygonal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s6" to="13" to_unit="um" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s6" value="bulging" value_original="bulging" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s6" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="moderately" name="architecture" notes="" src="d0_s6" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="collenchymatous" value_original="collenchymatous" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry soil and rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry soil" />
        <character name="habitat" value="rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, N.Mex., Tex.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Diagnostic features of the rare Syntrichia bartramii include plane leaf margins and the costa excurrent into a tapered, serrate awn. In contrast with those of S. fragilis, the leaves of S. bartramii are not fragile and have awns, somewhat smaller, more obscure distal cells, less well-defined groups of basal cells, and plane margins. Without propagula, plants of S. laevipila can be separated from S. bartramii by the abaxially smooth costae and smooth awns, and also by larger and less obscure leaf cells. It should be noted that leaves of S. bartramii sometimes show 2-stratose patches (one or a few cells in width). In contrast, S. chisosa has leaves that are almost completely 2-stratose distally, less twisted when dry, and merely mucronate at the apex.</discussion>
  
</bio:treatment>