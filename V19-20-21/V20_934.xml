<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="treatment_page">408</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">xylorhiza</taxon_name>
    <taxon_name authority="S. L. Welsh &amp; N. D. Atwood" date="1981" rank="species">cronquistii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>33: 302, fig. 8. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus xylorhiza;species cronquistii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067836</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(S. L. Welsh &amp; N. D. Atwood) Cronquist" date="unknown" rank="species">cronquistii</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species cronquistii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, ca. 30 cm.</text>
      <biological_entity id="o30466" name="whole_organism" name_original="" src="" type="structure">
        <character name="some_measurement" src="d0_s0" unit="cm" value="30" value_original="30" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched mostly in proximal 3/4, sparsely villous and stipitate-glandular to subglabrous.</text>
      <biological_entity id="o30467" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="in proximal 3/4" constraintid="o30468" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s1" value="villous" value_original="villous" />
        <character char_type="range_value" from="stipitate-glandular" name="pubescence" src="d0_s1" to="subglabrous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o30468" name="3/4" name_original="3/4" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear-lanceolate to narrowly oblanceolate, 2.5–6 mm wide, bases attenuate, not clasping, margins flat, usually shallowly spinulose-toothed, sometimes entire, faces sparsely villous.</text>
      <biological_entity id="o30469" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="narrowly oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30470" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o30471" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually shallowly" name="shape" src="d0_s2" value="spinulose-toothed" value_original="spinulose-toothed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30472" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 5–10 cm.</text>
      <biological_entity id="o30473" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 10–12 × 13–18 mm.</text>
      <biological_entity id="o30474" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 13–17;</text>
      <biological_entity id="o30475" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s5" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas white.</text>
      <biological_entity id="o30476" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Style-branch appendages ± equal or slightly shorter than stigmatic lines.</text>
      <biological_entity constraint="style-branch" id="o30477" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character constraint="than stigmatic lines" constraintid="o30478" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="more or less equal or slightly shorter" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o30478" name="line" name_original="lines" src="d0_s7" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gray sand of the Kaiparowits Formation</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gray sand" constraint="of the kaiparowits formation" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Cronquist’s woody-aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Xylorhiza cronquistii grows in Kane County. S. L. Welsh et al. (2003) noted that it is “more or less intermediate in morphologic features between X. confertifolia and X. tortifolia var. imberbis; it occurs on habitats intermediate between the two parental types and is at the approximate summit of the distribution of the latter.”</discussion>
  
</bio:treatment>