<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="treatment_page">377</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="S. F. Blake &amp; Sherff" date="1940" rank="genus">JAMESIANTHUS</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Mus. Nat. Hist., Bot. Ser.</publication_title>
      <place_in_publication>22: 399. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus JAMESIANTHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">For Robert Leslie James, 1897–1977, American botanist and historian, and Greek anthos, flower</other_info_on_name>
    <other_info_on_name type="fna_id">116740</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 60–150+ cm (fibrous-rooted).</text>
      <biological_entity id="o4978" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s2">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate or sessile;</text>
      <biological_entity id="o4979" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades ± lanceolate to narrowly trullate, (bases sometimes ± auriculate) margins entire or denticulate, faces: abaxial glabrous, adaxial minutely scabrellous to hirtellous.</text>
      <biological_entity id="o4980" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="less lanceolate" name="shape" src="d0_s4" to="narrowly trullate" />
      </biological_entity>
      <biological_entity id="o4981" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o4982" name="face" name_original="faces" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o4983" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4984" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s4" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiate, borne singly or in open, corymbiform arrays.</text>
      <biological_entity id="o4985" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in open , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o4986" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o4985" id="r379" name="in" negation="false" src="d0_s5" to="o4986" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± campanulate or broader, 9–12+ mm diam.</text>
      <biological_entity id="o4987" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="width" src="d0_s6" value="broader" value_original="broader" />
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s6" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries persistent, 14–18+ in ± 3 series (± oblong, ovate, ovate-oblong, or ovate-attenuate, unequal, outer shorter).</text>
      <biological_entity id="o4988" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o4989" from="14" name="quantity" src="d0_s7" to="18" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4989" name="series" name_original="series" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles flat to convex, epaleate.</text>
      <biological_entity id="o4990" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s8" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 6–8, pistillate, fertile;</text>
      <biological_entity id="o4991" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow.</text>
      <biological_entity id="o4992" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 20–30, all bisexual or inner functionally staminate;</text>
      <biological_entity id="o4993" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s11" to="30" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s11" value="inner" value_original="inner" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, tubes shorter than to equaling ± campanulate to funnelform throats, lobes 5, ovate-deltate to deltate (style-branches stout, appendages deltate).</text>
      <biological_entity id="o4994" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o4995" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter than to equaling more" />
        <character constraint="to funnelform, throats" constraintid="o4996, o4997" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="less campanulate" value_original="less campanulate" />
      </biological_entity>
      <biological_entity id="o4996" name="funnelform" name_original="funnelform" src="d0_s12" type="structure" />
      <biological_entity id="o4997" name="throat" name_original="throats" src="d0_s12" type="structure" />
      <biological_entity id="o4998" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" from="ovate-deltate" name="shape" src="d0_s12" to="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Ray cypselae ± obovoid, weakly obcompressed, 16–24-nerved, ± hispidulous;</text>
      <biological_entity constraint="ray" id="o4999" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s13" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="16-24-nerved" value_original="16-24-nerved" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s13" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 0, or fragile, of 6–8+ barbellate bristles (borne on crowns).</text>
      <biological_entity id="o5000" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s14" value="fragile" value_original="fragile" />
      </biological_entity>
      <biological_entity id="o5001" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s14" to="8" upper_restricted="false" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <relation from="o5000" id="r380" name="consist_of" negation="false" src="d0_s14" to="o5001" />
    </statement>
    <statement id="d0_s15">
      <text>Disc cypselae ± ellipsoid to clavate, ± hispidulous;</text>
      <biological_entity constraint="disc" id="o5002" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="less ellipsoid" name="shape" src="d0_s15" to="clavate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s15" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0, or fragile, of 6–8+ barbellulate bristles (borne on crowns).</text>
      <biological_entity id="o5003" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s16" value="fragile" value_original="fragile" />
      </biological_entity>
      <biological_entity id="o5004" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s16" to="8" upper_restricted="false" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <relation from="o5003" id="r381" name="consist_of" negation="false" src="d0_s16" to="o5004" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>362.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>