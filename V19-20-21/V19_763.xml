<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">psilocarphus</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">oregonus</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 341. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus psilocarphus;species oregonus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067407</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants silvery to whitish, densely sericeous to somewhat lanuginose.</text>
      <biological_entity id="o12412" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="silvery" name="coloration" src="d0_s0" to="whitish" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="sericeous to somewhat" value_original="sericeous to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s0" value="lanuginose" value_original="lanuginose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (1–) 2–10, ascending to ± prostrate;</text>
      <biological_entity id="o12413" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="10" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="more or less prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal internode lengths mostly 0.5–1.5 (–2) times leaf lengths.</text>
      <biological_entity constraint="proximal" id="o12414" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o12415" is_modifier="false" modifier="mostly" name="length" src="d0_s2" value="0.5-1.5(-2) times leaf lengths" value_original="0.5-1.5(-2) times leaf lengths" />
      </biological_entity>
      <biological_entity id="o12415" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Capitular leaves ± erect, appressed to heads, linear to narrowly oblanceolate, widest in distal 2/3, longest 12–20 mm, lengths mostly 6–12 times widths, (3–) 3.5–5 times head heights.</text>
      <biological_entity constraint="capitular" id="o12416" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="to heads" constraintid="o12417" is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s3" to="narrowly oblanceolate" />
        <character constraint="in distal 2/3" constraintid="o12418" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character is_modifier="false" name="length" notes="" src="d0_s3" value="longest" value_original="longest" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="l_w_ratio" src="d0_s3" value="6-12" value_original="6-12" />
        <character constraint="head" constraintid="o12419" is_modifier="false" name="height" src="d0_s3" value="(3-)3.5-5 times head heights" value_original="(3-)3.5-5 times head heights" />
      </biological_entity>
      <biological_entity id="o12417" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o12418" name="2/3" name_original="2/3" src="d0_s3" type="structure" />
      <biological_entity id="o12419" name="head" name_original="head" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads ± spheric, largest 4–6 mm.</text>
      <biological_entity id="o12420" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles unlobed.</text>
      <biological_entity id="o12421" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pistillate paleae individually visible through indument, longest mostly 1.5–2.7 mm.</text>
      <biological_entity id="o12422" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character constraint="through indument" constraintid="o12423" is_modifier="false" modifier="individually" name="prominence" src="d0_s6" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="mostly" name="length" notes="" src="d0_s6" value="longest" value_original="longest" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12423" name="indument" name_original="indument" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Staminate corollas 0.7–1.4 mm, lobes mostly 4.</text>
      <biological_entity id="o12424" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s7" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12425" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae narrowly ± cylindric, terete, 0.6–1.2 mm.</text>
      <biological_entity id="o12426" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly more or less" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late Mar–mid Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Aug" from="late Mar" />
        <character name="fruiting time" char_type="range_value" to="mid Aug" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally inundated or flooded clay soils (vernal pool margins, drainages, moist rocky slopes)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clay soils" modifier="seasonally inundated or flooded" />
        <character name="habitat" value="vernal pool margins" />
        <character name="habitat" value="drainages" />
        <character name="habitat" value="moist rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1800(–2400) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Oregon woollyheads or woolly marbles</other_name>
  <discussion>Psilocarphus oregonus occurs from west-central California through most of Oregon to southeastern Washington, western Idaho, and northern Nevada. Relatively narrow-leaved, montane forms of P. tenellus account for reports of P. oregonus from the southern Sierra Nevada to Baja California; further study may show these to be intermediates between the two taxa.</discussion>
  <discussion>A malformed plant collected in Merced County, California, appears to have been a sterile hybrid between P. oregonus and Hesperevax caulescens (J. D. Morefield 1992c).</discussion>
  
</bio:treatment>