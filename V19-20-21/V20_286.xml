<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="mention_page">145</other_info_on_meta>
    <other_info_on_meta type="treatment_page">136</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) G. L. Nesom" date="1993" rank="subsection">maritimae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">sempervirens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 878. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection maritimae;species sempervirens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067570</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="unknown" rank="species">sempervirens</taxon_name>
    <taxon_hierarchy>genus Aster;species sempervirens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–200 cm;</text>
      <biological_entity id="o11124" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices short, stout.</text>
      <biological_entity id="o11125" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10 (–20+), erect or ascending, glabrous throughout or hairy in arrays.</text>
      <biological_entity id="o11126" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="in arrays" constraintid="o11127" is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11127" name="array" name_original="arrays" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: rosettes present at flowering;</text>
      <biological_entity id="o11128" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11129" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal cauline tapering to long, winged petioles sheathing stems or nearly so, blades narrowly ovate to oblanceolate, 100–400 × 10–60 mm, thick or fleshy, entire, acute, glabrous;</text>
      <biological_entity id="o11130" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o11131" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o11132" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o11133" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly ovate" modifier="nearly" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s4" to="400" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="60" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid to distal cauline usually numerous, sessile, blades lanceolate, 40–60 × 5–10 mm, reduced distally, thick or fleshy, bases sometimes subclasping, margins entire.</text>
      <biological_entity id="o11134" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o11135" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o11136" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o11137" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
      </biological_entity>
      <biological_entity id="o11138" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 20–500, secund, in paniculiform arrays, secund-pyramidal to broadly club-shaped, sometimes leafy proximally, at least proximal branches spreading-recurved, branches and peduncles bracteolate, bracteoles reduced distally.</text>
      <biological_entity id="o11139" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="500" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="secund" value_original="secund" />
        <character char_type="range_value" from="secund-pyramidal" name="shape" notes="" src="d0_s6" to="broadly club-shaped" />
        <character is_modifier="false" modifier="sometimes; proximally" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o11140" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o11142" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="at-least" name="position" src="d0_s6" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading-recurved" value_original="spreading-recurved" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
      <biological_entity id="o11143" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="at-least" name="position" src="d0_s6" value="proximal" value_original="proximal" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading-recurved" value_original="spreading-recurved" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
      <biological_entity id="o11144" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o11139" id="r1020" name="in" negation="false" src="d0_s6" to="o11140" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 2–3 mm, glabrous or sparsely hairy.</text>
      <biological_entity id="o11145" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres 3–7 mm.</text>
      <biological_entity id="o11146" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–4 series, unequal, lanceolate, margins ciliate, apices acute.</text>
      <biological_entity id="o11147" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o11148" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o11149" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11150" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o11147" id="r1021" name="in" negation="false" src="d0_s9" to="o11148" />
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8–17;</text>
      <biological_entity id="o11151" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>laminae 5–6.2 × 0.4–0.6 mm.</text>
      <biological_entity id="o11152" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 10–22;</text>
      <biological_entity id="o11153" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 3–3.2 mm, lobes 0.5–1.2 mm.</text>
      <biological_entity id="o11154" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11155" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae (obconic) 1.1–1.5 mm, moderately strigose;</text>
      <biological_entity id="o11156" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s14" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 3.8–4 mm (slightly clavate).</text>
      <biological_entity id="o11157" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Nfld. and Labr. (Nfld.), Ont., P.E.I., Que.; Ala., Conn., Del., Fla., Ga., Ill., La., Maine, Mass., Md., Mich., Miss., N.C., N.H., N.J., N.Y., Ohio, Pa., R.I., S.C., Tex., Va.; Mexico, West Indies, Central America; introduced inland around Great Lakes, introduced to Atlantic Islands (Azores).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="inland around Great Lakes" establishment_means="introduced" />
        <character name="distribution" value="to Atlantic Islands (Azores)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <other_name type="common_name">Seaside goldenrod</other_name>
  <other_name type="common_name">verge d’or toujours verte</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Solidago sempervirens is common along the seacoast from the Gulf of St. Lawrence to central America and the northern West Indies. Introduced populations are sometimes very large near the Detroit River and Lake Erie in southwestern Ontario, eastern Michigan, and adjacent Ohio. A second disjunct group of populations occurs in Illinois and Indiana in the Chicago area at the southern end of Lake Michigan. Two mostly geographically separate subspecies can be recognized in the flora range. A race also occurs in the Azores and is undoubtedly introduced there [Solidago sempervirens var. azorica (Hochstetter ex Seubert) H. St. John]. Plants cultivated in European gardens have been labeled S. sempervirens var. viminea (Aiton) A. Gray.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 4–7 mm, rays 12–17, disc florets ca. 17–22; Newfoundland to New Jersey, locally to Virginia</description>
      <determination>38a Solidago sempervirens subsp. sempervirens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 3–4 mm, rays 7–11, disc florets ca. 10–16; Massachusetts to Texas and southward</description>
      <determination>38b Solidago sempervirens subsp. mexicana</determination>
    </key_statement>
  </key>
</bio:treatment>