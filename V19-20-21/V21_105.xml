<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="treatment_page">51</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="section">macrocline</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 355. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section macrocline;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067453</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, to 200 cm (rhizomatous, roots fibrous).</text>
      <biological_entity id="o24116" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: green, blades broadly ovate to lanceolate (rarely lobed), herbaceous, bases attenuate to cuneate or broadly rounded, ultimate margins entire or serrate, apices acute, faces sparsely to densely hairy (mostly adaxially), rarely glabrous;</text>
      <biological_entity id="o24117" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o24118" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s1" to="lanceolate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o24119" name="base" name_original="bases" src="d0_s1" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s1" to="cuneate or broadly rounded" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o24120" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o24121" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o24122" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal petiolate, 12–30 × 3–9 cm;</text>
      <biological_entity id="o24123" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o24124" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline petiolate or sessile, 5–25 × 2–10 cm.</text>
      <biological_entity id="o24125" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o24126" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in ± corymbiform arrays.</text>
      <biological_entity id="o24127" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o24128" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o24127" id="r1654" name="in" negation="false" src="d0_s4" to="o24128" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries to 3 cm (margins mostly ciliate, hairy, especially abaxially).</text>
      <biological_entity id="o24129" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacles ovoid to columnar;</text>
      <biological_entity id="o24130" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="columnar" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>paleae (proximally light-brown, distally green, becoming maroon with age) 5–7 mm, apices acute to acuminate, abaxial tips densely hairy.</text>
      <biological_entity id="o24131" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24132" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24133" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0.</text>
      <biological_entity id="o24134" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Discs 17–45 × 12–20 mm.</text>
      <biological_entity id="o24135" name="disc" name_original="discs" src="d0_s9" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s9" to="45" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 200–500+;</text>
      <biological_entity id="o24136" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="200" name="quantity" src="d0_s10" to="500" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellowish green proximally, blackish maroon distally, 4–6 mm;</text>
      <biological_entity id="o24137" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s11" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s11" value="blackish maroon" value_original="blackish maroon" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style-branches ca. 1.2 mm, apices acute to rounded.</text>
      <biological_entity id="o24138" name="branch-style" name_original="style-branches" src="d0_s12" type="structure">
        <character name="distance" src="d0_s12" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
      <biological_entity id="o24139" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 3.5–5 mm;</text>
      <biological_entity id="o24140" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi coroniform, to 1.2 mm. 2n = 36.</text>
      <biological_entity id="o24141" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24142" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open meadows, streamsides, seeps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open meadows" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="seeps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Western coneflower</other_name>
  <discussion>Rudbeckia occidentalis is sometimes grown as an ornamental.</discussion>
  
</bio:treatment>