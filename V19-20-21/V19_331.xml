<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="mention_page">252</other_info_on_meta>
    <other_info_on_meta type="treatment_page">251</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="F. H. Wiggers" date="1780" rank="genus">taraxacum</taxon_name>
    <taxon_name authority="Rydberg" date="1901" rank="species">alaskanum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>28: 512. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus taraxacum;species alaskanum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067700</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Taraxacum</taxon_name>
    <taxon_name authority="Dahlstedt" date="unknown" rank="species">kamtschaticum</taxon_name>
    <taxon_hierarchy>genus Taraxacum;species kamtschaticum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Taraxacum</taxon_name>
    <taxon_name authority="Jurtzev" date="unknown" rank="species">pseudokamtschaticum</taxon_name>
    <taxon_hierarchy>genus Taraxacum;species pseudokamtschaticum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (1.5–) 3–9 (–16, mostly in fruit) cm;</text>
      <biological_entity id="o9280" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="9" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots sometimes branched.</text>
      <biological_entity id="o9281" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–3+, ascending, proximally purplish, glabrous or glabrate.</text>
      <biological_entity id="o9282" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s2" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves fewer than 10, horizontal to patent (green);</text>
      <biological_entity id="o9283" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="10" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s3" to="patent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles slender or decurrent lines from bases;</text>
      <biological_entity id="o9284" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
      <biological_entity constraint="slender" id="o9285" name="line" name_original="lines" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o9286" name="base" name_original="bases" src="d0_s4" type="structure" />
      <relation from="o9285" id="r865" name="from" negation="false" src="d0_s4" to="o9286" />
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate or narrowly oblong (usually runcinate), (1.5–) 2.2–11.6 × 0.4–2.2 cm, bases attenuate, margins usually lobed regularly, ± deeply, in 3–5 (–6) pairs, occasionally (younger) only toothed or denticulate, lobes usually retrorse, occasionally straight or antrorse, triangular to narrowly triangular, terminals often largest, acuminate to acute-rounded, sometimes toothed, teeth 0 (–1) on lobes or sinuses, triangular, apices obtuse to acute, faces glabrous.</text>
      <biological_entity id="o9287" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s5" to="2.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s5" to="11.6" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="2.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9288" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o9289" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually; regularly" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="occasionally only" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o9290" name="pair" name_original="pairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o9291" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" modifier="occasionally" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="narrowly triangular" />
      </biological_entity>
      <biological_entity id="o9292" name="terminal" name_original="terminals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute-rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o9293" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="1" />
        <character constraint="on sinuses" constraintid="o9295" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o9294" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity id="o9295" name="sinuse" name_original="sinuses" src="d0_s5" type="structure" />
      <biological_entity id="o9296" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o9297" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o9289" id="r866" modifier="more or less deeply; deeply" name="in" negation="false" src="d0_s5" to="o9290" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 7–9, spreading, becoming reflexed to revolute, often purplish, particularly adaxially, ovate to lanceovate or elliptic bracklets in 2 series, 2.5–4.5 × 1.7–2.7 mm, margins not or narrowly scarious, hyaline, apices acuminate to caudate, tips sometimes flaring, scarious, erose, hornless.</text>
      <biological_entity id="o9298" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="of 7-9" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character char_type="range_value" constraint="in series" constraintid="o9299" from="ovate" modifier="particularly adaxially; adaxially" name="shape" src="d0_s6" to="lanceovate or elliptic" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" notes="" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" notes="" src="d0_s6" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9299" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9300" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o9301" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s6" to="caudate" />
      </biological_entity>
      <biological_entity id="o9302" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="flaring" value_original="flaring" />
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hornless" value_original="hornless" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres dark green, often glaucous, often purplish, particularly adaxially, cylindro to narrowly campanulate, 9–14 mm.</text>
      <biological_entity id="o9303" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="cylindro" modifier="particularly adaxially; adaxially" name="shape" src="d0_s7" to="narrowly campanulate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 7–8 in 3 series, lanceolate, 1.4–2.6 (–4) mm wide, margins not scarious (some outer) to narrowly scarious, apices long-acuminate, tips purplish or grayish, often flared, scarious, hornless.</text>
      <biological_entity id="o9304" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o9305" from="7" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.6" from_inclusive="false" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s8" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9305" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o9306" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="not scarious" name="texture" src="d0_s8" to="narrowly scarious" />
      </biological_entity>
      <biological_entity id="o9307" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o9308" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s8" value="flared" value_original="flared" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s8" value="hornless" value_original="hornless" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 30–55+;</text>
      <biological_entity id="o9309" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s9" to="55" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow (gray-striped abaxially, sometimes becoming orange purplish with age on drying), outer 11–14 × 1.5–2.4 mm.</text>
      <biological_entity id="o9310" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s10" to="14" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae maroon to brown or reddish-brown, sometimes grayish, bodies oblanceoloid (sometimes narrowly), 3–3.8 mm, cones conic, 0.8–1 mm, beaks stout, 3–6 mm, ribs 15 narrow (6 prominent), faces proximally ± tuberculate, ± muricate in distal 1/3–1/2 (or less);</text>
      <biological_entity id="o9311" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="maroon" name="coloration" src="d0_s11" to="brown or reddish-brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="grayish" value_original="grayish" />
      </biological_entity>
      <biological_entity id="o9312" name="body" name_original="bodies" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceoloid" value_original="oblanceoloid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9313" name="cone" name_original="cones" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9314" name="beak" name_original="beaks" src="d0_s11" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9315" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="15" value_original="15" />
        <character is_modifier="false" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o9316" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="proximally more or less" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
        <character constraint="in distal 1/3-1/2" constraintid="o9317" is_modifier="false" modifier="more or less" name="relief" src="d0_s11" value="muricate" value_original="muricate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9317" name="1/3-1/2" name_original="1/3-1/2" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pappi white to yellowish, 4–6.5 mm. 2n = 24, 32 (as T. kamtschaticum).</text>
      <biological_entity id="o9318" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="yellowish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9319" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine slopes and tundra, arctic tundra, rich arctic seaside bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine slopes" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="arctic tundra" />
        <character name="habitat" value="rich arctic seaside bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska; Russian Far East.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Russian Far East" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Alaska dandelion</other_name>
  <discussion>In all the specimens examined from Alaska, it is not possible to find consistent differences between Taraxacum alaskanum and T. kamtschaticum (as applied in Alaska; or T. pseudokamtschaticum). Assignment to one or the other species appears random. The leaf character used in keys to separate the entities does not work, or at least is not matched by specimens as determined; cypsela color varies within species, and the slight difference noted is not sufficient to warrant separation. Neither is there a significant size difference between coastal and inland material. The name T. sibiricum Dahlstedt has been applied mistakenly by American authors to this entity.</discussion>
  
</bio:treatment>