<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">317</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1866" rank="species">breweri</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="1992" rank="variety">covillei</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>72: 172. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species breweri;variety covillei</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068319</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">covillei</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 20. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species covillei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">foliosus</taxon_name>
    <taxon_name authority="(Greene) G. R. Compton" date="unknown" rank="variety">covillei</taxon_name>
    <taxon_hierarchy>genus Erigeron;species foliosus;variety covillei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (30–) 40–75 cm;</text>
      <biological_entity id="o8284" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>woody roots or caudices with relatively long and slender branches.</text>
      <biological_entity id="o8285" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o8286" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity constraint="slender" id="o8287" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="relatively" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
      <relation from="o8285" id="r753" name="with" negation="false" src="d0_s1" to="o8287" />
      <relation from="o8286" id="r754" name="with" negation="false" src="d0_s1" to="o8287" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending-erect, not wiry or brittle.</text>
      <biological_entity id="o8288" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending-erect" value_original="ascending-erect" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="brittle" value_original="brittle" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries hispido-hirsute (hairs white, ascending, relatively thick-based, dense on outermost phyllaries, much less so on mid and inner), eglandular.</text>
      <biological_entity id="o8289" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hispido-hirsute" value_original="hispido-hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky sites in sagebrush, chaparral, juniper</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky sites" modifier="open" constraint="in sagebrush , chaparral , juniper" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>106b.</number>
  
</bio:treatment>