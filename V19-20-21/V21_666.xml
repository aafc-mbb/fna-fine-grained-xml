<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="mention_page">273</other_info_on_meta>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">calycadenia</taxon_name>
    <taxon_name authority="R. L. Carr &amp; G. D. Carr" date="2004" rank="species">micrantha</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 261, figs. 1, 2B. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus calycadenia;species micrantha</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066277</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–50 cm (relatively slender);</text>
    </statement>
    <statement id="d0_s1">
      <text>self-compatible.</text>
      <biological_entity id="o740" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="self-compatible" value_original="self-compatible" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (often purplish, especially distally, slender, usually less than 3 mm diam. at bases) branched (branches often relatively many, usually from near midstems, arcuate to ascending), glabrous.</text>
      <biological_entity id="o741" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly alternate, 2–5 cm (proximal), hispidulous and ± long-hairy (especially margins and adaxial faces).</text>
      <biological_entity id="o742" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="long-hairy" value_original="long-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in ± spiciform arrays (1–3 per node, ± sessile).</text>
      <biological_entity id="o743" name="head" name_original="heads" src="d0_s4" type="structure">
        <character constraint="in more or less spiciform arrays" is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in more or less spiciform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncular bracts lance-oblong to linear, 2–4 mm (nearly terete to strongly flattened, glabrous or hispid overall, sometimes pectinate-fimbriate), apices ± rounded, tack-glands usually 1 (terminal).</text>
      <biological_entity constraint="peduncular" id="o744" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="lance-oblong" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o745" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o746" name="tack-gland" name_original="tack-glands" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 4–5 mm, abaxial faces glabrous or ± hispid distally or sparsely bristly, shaggy long-hairy on margins distally, tack-glands 0 (–1) (terminal).</text>
      <biological_entity id="o747" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o748" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s6" value="or" value_original="or" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="shaggy" value_original="shaggy" />
        <character constraint="on margins" constraintid="o749" is_modifier="false" name="pubescence" src="d0_s6" value="long-hairy" value_original="long-hairy" />
      </biological_entity>
      <biological_entity id="o749" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o750" name="tack-gland" name_original="tack-glands" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="1" />
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae 5–6 mm.</text>
      <biological_entity id="o751" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 1–3 (–6);</text>
      <biological_entity id="o752" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="6" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas bright-yellow, tubes 1–1.5 mm, laminae 2–2.5 (–3) mm (central lobes smaller than laterals, oblong to narrowly triangular, symmetric, widest at bases, laterals asymmetric, sinuses 1/4 laminae).</text>
      <biological_entity id="o753" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
      <biological_entity id="o754" name="tube" name_original="tubes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o755" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 1–3;</text>
      <biological_entity id="o756" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellowish, 3–4 mm.</text>
      <biological_entity id="o757" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae ca. 3 mm, rough-wrinkled, glabrous.</text>
      <biological_entity constraint="ray" id="o758" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="relief" src="d0_s12" value="rough-wrinkled" value_original="rough-wrinkled" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc cypselae ca. 3 mm (seldom formed, terete and tapered toward bases, smooth to ± ridged, glabrous);</text>
      <biological_entity constraint="disc" id="o759" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 0.2n = 14.</text>
      <biological_entity id="o760" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o761" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering usually Aug–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="usually" to="fall" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open, rocky ridges, hillsides and talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" modifier="dry" />
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Calycadenia micrantha is most closely related to variants of C. truncata and is found in the North Coast Range from Trinity County to Lake and Colusa counties. Calycadenia truncata subsp. microcephala H. M. Hall ex D. D. Keck may be a synonym of C. micrantha. D. D. Keck (1960b) reported this to be on the east slope of the Santa Lucia Mountains of Monterey County.</discussion>
  
</bio:treatment>