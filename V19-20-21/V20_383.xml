<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">176</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">stenotus</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">armerioides</taxon_name>
    <taxon_name authority="(S. L. Welsh &amp; F. J. Smith) Kartesz &amp; Gandhi" date="1991" rank="variety">gramineus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>71: 61. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus stenotus;species armerioides;variety gramineus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068811</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray)" date="unknown" rank="species">armerioides</taxon_name>
    <taxon_name authority="S. L. Welsh &amp; F. J. Smith" date="unknown" rank="variety">gramineus</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>43: 371. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species armerioides;variety gramineus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 3.5–8 cm.</text>
      <biological_entity id="o22097" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles stout;</text>
      <biological_entity id="o22098" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o22099" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades usually 1-nerved, linear, 40 (–70) × 1–1.5 mm, bases about as wides as blades, margins ciliate, faces usually at least ± scabrous.</text>
      <biological_entity id="o22100" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22101" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="70" to_unit="mm" />
        <character name="length" src="d0_s2" unit="mm" value="40" value_original="40" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22102" name="base" name_original="bases" src="d0_s2" type="structure" />
      <biological_entity id="o22103" name="blade" name_original="blades" src="d0_s2" type="structure" />
      <biological_entity id="o22104" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o22105" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually at-least more or less" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1 (–2).</text>
      <biological_entity id="o22106" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="2" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting mid–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="mid" />
        <character name="fruiting time" char_type="range_value" to="late spring" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pinyon-juniper woodlands on shale</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinyon-juniper woodlands" constraint="on shale" />
        <character name="habitat" value="shale" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Stenotus armerioides var. gramineus is known only from Green River Formation shales of the Uintah Basin of Duschesne and Uintah counties.</discussion>
  
</bio:treatment>