<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
    <other_info_on_meta type="illustration_page">236</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="(E. James) Torrey &amp; A. Gray" date="1843" rank="species">runcinata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">runcinata</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species runcinata;subspecies runcinata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068235</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–60 cm.</text>
      <biological_entity id="o13298" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles narrowly winged;</text>
      <biological_entity id="o13299" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13300" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s1" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades narrowly obovate, elliptic, lanceolate, or spatulate, sometimes runcinate, 0.5–3.5 cm wide, margins entire or toothed to pinnately lobed (lobes or teeth remote, not prominently white-tipped), faces glabrous or hispidulous.</text>
      <biological_entity id="o13301" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13302" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="runcinate" value_original="runcinate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13303" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="toothed" name="shape" src="d0_s2" to="pinnately lobed" />
      </biological_entity>
      <biological_entity id="o13304" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1–12.</text>
      <biological_entity id="o13305" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 8–10 mm.</text>
      <biological_entity id="o13306" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries lanceolate, strongly stipitate-glandular, apices acute.</text>
      <biological_entity id="o13307" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="strongly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o13308" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae dark-brown, 3.5–7.5 mm, tapered, not beaked;</text>
      <biological_entity id="o13309" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s6" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi 4–6 mm. 2n = 22.</text>
      <biological_entity id="o13310" name="pappus" name_original="pappi" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13311" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows, low wet areas, swales, bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="low wet areas" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Sask.; Colo., Idaho, Minn., Mont., Nebr., Nev., N.Mex., N.Dak., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20a.</number>
  <other_name type="common_name">Fiddleleaf or naked-stem or scapose or dandelion hawksbeard</other_name>
  
</bio:treatment>