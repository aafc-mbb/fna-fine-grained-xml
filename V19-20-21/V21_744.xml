<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="treatment_page">306</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Molina" date="1782" rank="genus">madia</taxon_name>
    <taxon_name authority="D. D. Keck" date="1945" rank="species">subspicata</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>564: 45. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus madia;species subspicata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067144</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–60 cm, self-compatible (heads not showy).</text>
      <biological_entity id="o10144" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="self-compatible" value_original="self-compatible" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally ± villous, distally glandular-pubescent, glands yellowish, lateral branches not surpassing main-stems.</text>
      <biological_entity id="o10145" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally more or less" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o10146" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10147" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o10148" name="main-stem" name_original="main-stems" src="d0_s1" type="structure" />
      <relation from="o10147" id="r699" name="surpassing" negation="true" src="d0_s1" to="o10148" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear to lance-linear, 2–7 cm × 1–5 mm.</text>
      <biological_entity id="o10149" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="arrangement_or_course_or_shape" src="d0_s2" to="lance-linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in spiciform or spiciform-racemiform arrays (peduncles 0 or lengths usually less than 2 times heads).</text>
      <biological_entity id="o10150" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o10151" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="spiciform-racemiform" value_original="spiciform-racemiform" />
      </biological_entity>
      <relation from="o10150" id="r700" name="in" negation="false" src="d0_s3" to="o10151" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres globose or ovoid, 6–8 mm.</text>
      <biological_entity id="o10152" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± hirsute and thick-stalked-glandular as well, glands golden yellow, apices ± erect, sulcate or flat.</text>
      <biological_entity id="o10153" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thick-stalked-glandular" value_original="thick-stalked-glandular" />
      </biological_entity>
      <biological_entity id="o10154" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="coloration" src="d0_s5" value="golden yellow" value_original="golden yellow" />
      </biological_entity>
      <biological_entity id="o10155" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae mostly persistent, distinct or connate less than 1/2 their lengths.</text>
      <biological_entity id="o10156" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s6" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 5–8;</text>
      <biological_entity id="o10157" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas pale-yellow, laminae 1–2.5 mm.</text>
      <biological_entity id="o10158" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o10159" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 5–15, bisexual, fertile;</text>
      <biological_entity id="o10160" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="15" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3–3.5 mm, pubescent;</text>
      <biological_entity id="o10161" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow to brownish.</text>
      <biological_entity id="o10162" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae black or brown, sometimes purple-mottled, dull, compressed, ± clavate, beakless.</text>
      <biological_entity constraint="ray" id="o10163" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="purple-mottled" value_original="purple-mottled" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="dull" value_original="dull" />
        <character is_modifier="false" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="beakless" value_original="beakless" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc cypselae similar.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity constraint="disc" id="o10164" name="cypsela" name_original="cypselae" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o10165" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands and open woodlands, often in shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="shade" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Madia subspicata occurs locally in the central and northern Sierra Nevada foothills, sometimes with the morphologically similar M. gracilis.</discussion>
  
</bio:treatment>