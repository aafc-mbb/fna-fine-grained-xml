<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">21</other_info_on_meta>
    <other_info_on_meta type="illustration_page">21</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">aster</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">alpinus</taxon_name>
    <taxon_name authority="Onno" date="1932" rank="subspecies">vierhapperi</taxon_name>
    <place_of_publication>
      <publication_title>Biblioth. Bot.</publication_title>
      <place_in_publication>26(106): 25. 1932</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus aster;species alpinus;subspecies vierhapperi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250068081</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">alpinus</taxon_name>
    <taxon_name authority="(Onno) Cronquist" date="unknown" rank="variety">vierhapperi</taxon_name>
    <taxon_hierarchy>genus Aster;species alpinus;variety vierhapperi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Linnaeus) Semple" date="unknown" rank="species">culminis</taxon_name>
    <taxon_hierarchy>genus Aster;species culminis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diplactis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alpinus</taxon_name>
    <taxon_name authority="(Onno) Semple" date="unknown" rank="subspecies">vierhapperi</taxon_name>
    <taxon_hierarchy>genus Diplactis;species alpinus;subspecies vierhapperi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2.5–32 (–40) cm, cespitose;</text>
      <biological_entity id="o23612" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="32" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s0" to="32" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes woody, caudices thick, woody, branched, ± covered with marcescent leaf-bases.</text>
      <biological_entity id="o23613" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o23614" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o23615" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
      </biological_entity>
      <relation from="o23614" id="r2185" modifier="more or less" name="covered with" negation="false" src="d0_s1" to="o23615" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (per rosette), erect to ascending, simple, ± densely white-villous, becoming nearly woolly near heads, sometimes short-stipitate-glandular proximally, ± densely short or long-stipitate-glandular distally.</text>
      <biological_entity id="o23616" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s2" value="white-villous" value_original="white-villous" />
        <character constraint="near heads" constraintid="o23617" is_modifier="false" modifier="becoming nearly" name="pubescence" src="d0_s2" value="woolly" value_original="woolly" />
        <character is_modifier="false" modifier="sometimes; proximally" name="pubescence" notes="" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="more or less densely" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o23617" name="head" name_original="heads" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, margins entire, densely villous-ciliate, faces ± densely villous, ± densely short-stipitate-glandular or (distal mainly) eglandular;</text>
      <biological_entity id="o23618" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o23619" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s3" value="villous-ciliate" value_original="villous-ciliate" />
      </biological_entity>
      <biological_entity id="o23620" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal persistent, usually petiolate to subpetiolate, sometimes sessile, blades 3-nerved, oblanceolate or obovate to spatulate, 10–112 × 2.5–14 mm, bases cuneate or attenuate, apices ± acute to rounded;</text>
      <biological_entity constraint="basal" id="o23621" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="usually petiolate" name="architecture" src="d0_s4" to="subpetiolate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o23622" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="112" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23623" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o23624" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="less acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline usually sessile, lanceolate or lance-oblong to linear-lanceolate or linear, 7–43 (–50) × 1–6 (–8) mm.</text>
      <biological_entity constraint="cauline" id="o23625" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="lance-oblong" name="shape" src="d0_s5" to="linear-lanceolate or linear" />
        <character char_type="range_value" from="43" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="43" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly.</text>
      <biological_entity id="o23626" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres broadly hemispheric, 6–11 mm.</text>
      <biological_entity id="o23627" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2–3 series (usually green, sometimes ± purplish), lanceolate to lance-oblong, subequal, herbaceous, apices acute (outer) to acuminate or ± cuspidate (inner), faces ± densely villous, sometimes ± sparsely short-stipitate-glandular.</text>
      <biological_entity id="o23628" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s8" to="lance-oblong" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o23629" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o23630" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate or more or less cuspidate" />
      </biological_entity>
      <biological_entity id="o23631" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes more or less sparsely" name="pubescence" src="d0_s8" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <relation from="o23628" id="r2186" name="in" negation="false" src="d0_s8" to="o23629" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets (20–) 25–55 (–95);</text>
      <biological_entity id="o23632" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s9" to="25" to_inclusive="false" />
        <character char_type="range_value" from="55" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="95" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="55" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae pink, white, or lavender [purple], (9–) 11–17+ × (1–) 1.6–2.4 mm.</text>
      <biological_entity id="o23633" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s10" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s10" to="17" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s10" to="1.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s10" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 50–100+;</text>
      <biological_entity id="o23634" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s11" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow turning brown, 5–6 mm, tubes much shorter than funnelform throats.</text>
      <biological_entity id="o23635" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow turning brown" value_original="yellow turning brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23636" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character constraint="than funnelform throats" constraintid="o23637" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o23637" name="throat" name_original="throats" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Cypselae tan to brown or dark reddish-brown, obovoid, compressed, (2–) 2.5–3.2 mm, 2 marginal ribs plus 0–2 thin nerves on each face, faces ± densely strigillose, sometimes gland-dotted;</text>
      <biological_entity id="o23638" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s13" to="brown or dark reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.2" to_unit="mm" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o23639" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s13" to="2" />
      </biological_entity>
      <biological_entity id="o23640" name="nerve" name_original="nerves" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o23641" name="face" name_original="face" src="d0_s13" type="structure" />
      <biological_entity id="o23642" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s13" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <relation from="o23640" id="r2187" name="on" negation="false" src="d0_s13" to="o23641" />
    </statement>
    <statement id="d0_s14">
      <text>pappi tawny to white, 5–6 mm, about equaling disc corollas.</text>
      <biological_entity constraint="disc" id="o23644" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o23643" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="tawny" name="coloration" src="d0_s14" to="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23645" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open to semishaded, mesic to dry, rocky, gravelly or silty, often calcareous areas, alpine tundra, montane to alpine meadows, cold prairies, Artemisia steppes, gravelly or eroding stream banks and flats, edges of open riparian boreal forests, bluffs, talus and cliffs, sandy ridges in muskeg</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open to semishaded" />
        <character name="habitat" value="mesic" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="silty" />
        <character name="habitat" value="areas" modifier="often calcareous" />
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="alpine tundra" />
        <character name="habitat" value="montane to alpine meadows" />
        <character name="habitat" value="cold prairies" />
        <character name="habitat" value="artemisia steppes" />
        <character name="habitat" value="stream banks" modifier="gravelly or eroding" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="edges" constraint="of open riparian" />
        <character name="habitat" value="open riparian" />
        <character name="habitat" value="boreal forests" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="sandy ridges" constraint="in muskeg" />
        <character name="habitat" value="muskeg" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Ont., Yukon; Alaska, Colo., Idaho, Wyo.; e Asia (Russian Far East, e Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="e Asia (e Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Aster alpinus is the only species of the genus in the strict sense that is native to North America. The report of the species for Quebec on the NatureServe website is erroneous. The species is thought to be rare in Ontario, Colorado, Idaho, and Wyoming. The reports for Idaho and Wyoming are new. Confusion in identification often occurs with Erigeron caespitosus Nuttall, E. glacialis Nuttall, and E. hyperboreus, among others. J. C. Semple et al. (2002) used the name A. culminis for the North American entity; my study of Russian and North American material showed that it is not possible to distinguish between Siberian and North American materials, which indeed share a pubescence type different from that of the typical subspecies. It is not possible to divide the species easily into distinct species or varieties. The only difference found in material from the two areas is that rays in the former tend to be purple, while in the latter they are mostly pinkish white to pink, sometimes purple, a difference deemed insufficient at the present time to warrant species recognition. A thorough biosystematic and molecular study of the complex may resolve this taxonomic problem.</discussion>
  <discussion>Aster americanus Onno, referable here, is an invalid name.</discussion>
  
</bio:treatment>