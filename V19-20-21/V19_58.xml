<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">112</other_info_on_meta>
    <other_info_on_meta type="illustration_page">90</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Muhlenberg ex Willdenow) Sprengel" date="1826" rank="species">discolor</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>3: 373. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species discolor</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416293</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cnicus</taxon_name>
    <taxon_name authority="Muhlenberg ex Willdenow" date="unknown" rank="species">discolor</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>3: 1670. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cnicus;species discolor;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="(Muhlenberg ex Willdenow) Nuttall" date="unknown" rank="species">discolor</taxon_name>
    <taxon_hierarchy>genus Carduus;species discolor;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or sometimes perennials, 80–200 cm;</text>
      <biological_entity id="o16117" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots and often cluster of coarse fibrous-roots, roots without tuberlike enlargements.</text>
      <biological_entity id="o16119" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character constraint="of fibrous-roots" constraintid="o16120" is_modifier="false" modifier="often" name="arrangement" src="d0_s1" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o16120" name="fibrou-root" name_original="fibrous-roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="relief" src="d0_s1" value="coarse" value_original="coarse" />
      </biological_entity>
      <biological_entity id="o16121" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o16122" name="enlargement" name_original="enlargements" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="tuberlike" value_original="tuberlike" />
      </biological_entity>
      <relation from="o16121" id="r1470" name="without" negation="false" src="d0_s1" to="o16122" />
    </statement>
    <statement id="d0_s2">
      <text>Stems single, erect, villous with septate trichomes, sometimes ± glabrate, distally ± tomentose;</text>
      <biological_entity id="o16123" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="with trichomes" constraintid="o16124" is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" notes="" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally more or less" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o16124" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches few–many, ascending.</text>
      <biological_entity id="o16125" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" is_modifier="false" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades oblanceolate to elliptic or ovate, 10–25 (–50) × 1–13 (–25) cm, usually deeply divided more than halfway to midveins, proximal sometimes undivided, lobes linear-lanceolate, margins revolute, ascending, entire or spinulose to remotely few toothed or sharply lobed, main spines 1–5 mm, abaxial faces white-tomentose, adaxial faces green, villous with septate trichomes or glabrate;</text>
      <biological_entity id="o16126" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16127" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="elliptic or ovate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="13" to_unit="cm" />
        <character is_modifier="false" modifier="usually deeply" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o16128" name="midvein" name_original="midveins" src="d0_s4" type="structure">
        <character is_modifier="true" name="position" src="d0_s4" value="halfway" value_original="halfway" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16129" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity id="o16130" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o16131" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="spinulose to remotely few toothed or sharply lobed" />
      </biological_entity>
      <biological_entity constraint="main" id="o16132" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16133" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16134" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character constraint="with trichomes" constraintid="o16135" is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o16135" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal usually absent at flowering, winged-petiolate, bases tapered;</text>
      <biological_entity id="o16136" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o16137" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
      <biological_entity id="o16138" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline well distributed, gradually reduced, bases narrowed, sometimes weakly clasping, not decurrent;</text>
      <biological_entity id="o16139" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o16140" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o16141" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="sometimes weakly" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cauline well developed.</text>
      <biological_entity id="o16142" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal cauline" id="o16143" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s7" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–many in corymbiform or paniculiform arrays.</text>
      <biological_entity id="o16144" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o16145" from="1" is_modifier="false" name="quantity" src="d0_s8" to="many" />
      </biological_entity>
      <biological_entity id="o16145" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–5 cm (not overtopped by crowded distal cauline leaves).</text>
      <biological_entity id="o16146" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres ovoid to broadly cylindric or campanulate, 2–3.5 (–4) × 1.5–3 cm, thinly arachnoid.</text>
      <biological_entity id="o16147" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="broadly cylindric or campanulate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s10" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s10" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 10–12 series, strongly imbricate, greenish with subapical darker central zone, ovate (outer) to lanceolate (inner), abaxial faces with narrow glutinous ridge, outer and middle bodies appressed, margins entire, spines abruptly spreading to deflexed, slender, 3–9 mm;</text>
      <biological_entity id="o16148" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character constraint="with subapical central zone" constraintid="o16150" is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s11" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o16149" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
      <biological_entity constraint="subapical central" id="o16150" name="zone" name_original="zone" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16151" name="face" name_original="faces" src="d0_s11" type="structure" />
      <biological_entity id="o16152" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <biological_entity constraint="outer and middle" id="o16153" name="body" name_original="bodies" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o16154" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16155" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" from="abruptly spreading" name="orientation" src="d0_s11" to="deflexed" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <relation from="o16148" id="r1471" name="in" negation="false" src="d0_s11" to="o16149" />
      <relation from="o16151" id="r1472" name="with" negation="false" src="d0_s11" to="o16152" />
    </statement>
    <statement id="d0_s12">
      <text>spines slender, 3–9 mm;</text>
      <biological_entity id="o16156" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner phyllaries spreading, narrow, flattened, finely serrulate.</text>
      <biological_entity id="o16157" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="phyllary" constraint_original="phyllary; phyllary">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s13" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16158" name="phyllary" name_original="phyllaries" src="d0_s13" type="structure" />
      <relation from="o16157" id="r1473" name="part_of" negation="false" src="d0_s13" to="o16158" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas pink to purple (white), 25–32 mm, tubes 12–16 mm, throats 7–10 mm, (noticeably wider than tubes), lobes 6–9 mm;</text>
      <biological_entity id="o16159" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="purple" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s14" to="32" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16160" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16161" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16162" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 4–6 mm.</text>
      <biological_entity constraint="style" id="o16163" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae tan to brownish, 4–5 mm, apical collars straw-colored, 0.5–75 mm;</text>
      <biological_entity id="o16164" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s16" to="brownish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o16165" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="75" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 18–25 mm. 2n = 20, 21, 22.</text>
      <biological_entity id="o16166" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s17" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16167" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
        <character name="quantity" src="d0_s17" value="21" value_original="21" />
        <character name="quantity" src="d0_s17" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Jun–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tallgrass prairies, deciduous woodlands, forest openings, disturbed sites, often in damp soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tallgrass prairies" />
        <character name="habitat" value="deciduous woodlands" />
        <character name="habitat" value="forest openings" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="damp soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>5–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="5" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont., Que., Sask.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.Y., N.C., Ohio, Pa., R.I., S.C., S.Dak., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Field thistle</other_name>
  <other_name type="common_name">chardon discolore</other_name>
  <discussion>Cirsium discolor is widespread in eastern North America from the prairies of southeastern Saskatchewan, western Minnesota, and Iowa south to northern Louisiana and east across southern Canada to the New England states and the southern Appalachians. It hybridizes with both C. altissimum (discussed thereunder) and C. muticum (G. B. Ownbey 1951b, 1964; W. L. Bloom 1977). Meiosis in first-generation hybrids between C. discolor and C. muticum is usually irregular (Bloom) and most pollen grains are infertile (Ownbey 1951b; Bloom). The presence of a small number of viable cypselae in heads of putative F1 hybrids (Ownbey 1951b) indicates that some F2 hybrids or backcrosses are formed.</discussion>
  
</bio:treatment>