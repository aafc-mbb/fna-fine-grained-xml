<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ronald L. Hartman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="mention_page">402</other_info_on_meta>
    <other_info_on_meta type="treatment_page">383</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">XANTHISMA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 94. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus XANTHISMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek xanthos, yellow, and -i smos, condition or quality, alluding to bright yellow florets</other_info_on_name>
    <other_info_on_name type="fna_id">135016</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, perennials, or subshrubs, 3–100 cm (taprooted, caudices woody, much branched [rhizomes]).</text>
      <biological_entity id="o29634" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o29637" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, spreading, or sprawling, often much branched, glabrous or hispid to hispidulous, villous, or stipitate-glandular (especially distally).</text>
      <biological_entity id="o29638" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" modifier="often much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s1" to="hispidulous villous or stipitate-glandular" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s1" to="hispidulous villous or stipitate-glandular" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s1" to="hispidulous villous or stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal (sometimes persistent) and cauline;</text>
      <biological_entity id="o29639" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>short-petiolate or sessile;</text>
      <biological_entity id="o29640" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1-nerved, lanceolate to oblanceolate or spatulate (bases tapering to clasping), margins entire, serrate, dentate, pinnatifid, or 2-pinnatifid (apices of blades, lobes, and teeth apiculate to bristle-tipped, bristles 1–4 mm), faces usually glabrous, hispid, hispidulous, or villous, sometimes also stipitate-glandular.</text>
      <biological_entity id="o29641" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o29642" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate or spatulate" />
      </biological_entity>
      <biological_entity id="o29643" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-pinnatifid" value_original="2-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o29644" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiate or discoid, borne singly or in corymbiform arrays.</text>
      <biological_entity id="o29645" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in corymbiform arrays" value_original="in corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o29646" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o29645" id="r2737" name="in" negation="false" src="d0_s5" to="o29646" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate, campanulate, or hemispheric, (4–10 ×) 6–25 mm.</text>
      <biological_entity id="o29647" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="[4" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="]6" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 26–80+ in 2–8 series, appressed, spreading, or reflexed, 1-nerved (flat to rounded), linear to narrowly oblong or lanceolate, or enlarged distally into ovate to orbiculate or depressed-elliptic laminae, unequal, stiff, leathery, bases indurate, margins sometimes scarious, (apices herbaceous or achlorophyllous) faces hispid to hispidulous, villous, and/or stipitate-glandular.</text>
      <biological_entity id="o29648" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o29649" from="26" name="quantity" src="d0_s7" to="80" upper_restricted="false" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="narrowly oblong or lanceolate" />
        <character constraint="into laminae" constraintid="o29650" is_modifier="false" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="texture" src="d0_s7" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o29649" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <biological_entity id="o29650" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s7" to="orbiculate or depressed-elliptic" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o29651" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o29652" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o29653" name="face" name_original="faces" src="d0_s7" type="structure">
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s7" to="hispidulous villous and/or stipitate-glandular" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s7" to="hispidulous villous and/or stipitate-glandular" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s7" to="hispidulous villous and/or stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles flat to convex, pitted (pit borders usually laciniate or irregularly bristly, the teeth or setae 0.1–2+ mm), epaleate.</text>
      <biological_entity id="o29654" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s8" to="convex" />
        <character is_modifier="false" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0 (in X. grindelioides and 2 Mexican taxa) or 12–60+, pistillate, fertile;</text>
      <biological_entity id="o29655" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="60" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white, pink, red-purple, purple, or yellow.</text>
      <biological_entity id="o29656" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 15–200+, bisexual, fertile;</text>
      <biological_entity id="o29657" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="200" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, tubes length 1/4–1/3 ± funnelform throats (usually glabrous), lobes 5, spreading, triangular (glabrous or sparsely puberulent, hairs fine, antrorse);</text>
      <biological_entity id="o29658" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o29659" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1/4" modifier="more or less" name="quantity" src="d0_s12" to="1/3" />
      </biological_entity>
      <biological_entity id="o29660" name="throat" name_original="throats" src="d0_s12" type="structure" />
      <biological_entity id="o29661" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="length" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style-branch appendages lanceolate.</text>
      <biological_entity constraint="style-branch" id="o29662" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae distinctly dimorphic (tan to redbrown or purple), ellipsoid to obovoid, oblong, or obscurely cordate, those of rays (if present) ± 3-sided, rounded abaxially, of disc compressed (thin or thick walled), 6–18-ribbed, faces silky (hairs antrorsely ascending to appressed);</text>
      <biological_entity id="o29663" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="distinctly" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s14" to="obovoid oblong or obscurely cordate" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s14" to="obovoid oblong or obscurely cordate" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s14" to="obovoid oblong or obscurely cordate" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="6-18-ribbed" value_original="6-18-ribbed" />
      </biological_entity>
      <biological_entity id="o29664" name="ray" name_original="rays" src="d0_s14" type="structure" />
      <biological_entity id="o29665" name="disc" name_original="disc" src="d0_s14" type="structure" />
      <biological_entity id="o29666" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="silky" value_original="silky" />
      </biological_entity>
      <relation from="o29663" id="r2738" name="part_of" negation="false" src="d0_s14" to="o29664" />
      <relation from="o29663" id="r2739" name="part_of" negation="false" src="d0_s14" to="o29665" />
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent, of 30–90+ usually whitish to brown or reddish-brown, basally flattened (wider at overlapping bases), coarsely barbellate, apically attenuate bristles in 2–4 series.</text>
      <biological_entity id="o29667" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="30" modifier="of" name="quantity" src="d0_s15" to="90" upper_restricted="false" />
        <character char_type="range_value" from="usually whitish" name="coloration" src="d0_s15" to="brown or reddish-brown" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="coarsely" name="architecture" src="d0_s15" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o29669" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="4" />
      </biological_entity>
      <relation from="o29668" id="r2740" name="in" negation="false" src="d0_s15" to="o29669" />
    </statement>
    <statement id="d0_s16">
      <text>x = (2, 3, 4) 5.</text>
      <biological_entity id="o29668" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s15" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity constraint="x" id="o29670" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="[2" value_original="[2" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character name="quantity" src="d0_s16" value="4]" value_original="4]" />
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>195.</number>
  <other_name type="common_name">Sleepy-daisy</other_name>
  <discussion>Species 17 (9 in the flora).</discussion>
  <discussion>As here circumscribed, Xanthisma includes four sections of Machaeranthera in the sense of R. L. Hartman 1990: Blepharodon, Sideranthus, Havardii, and Stenoloba. The last section, consisting of a single Mexican species, is treated here as a synonym of sect. Sideranthus. This grouping is well supported by molecular data (D. R. Morgan 1993, 1997, 2003; Morgan and Hartman 2003; Morgan and B. B. Simpson 1992). Although Xanthisma includes species with both cyanic (blue, purple, pink, or white) and yellow rays, several morphologic characteristics are shared by its members, including short, turbinate, thick-walled fruits that are moderately to densely silky, receptacular scales, leaves with marginal spines, and chromosome numbers based on x = 4 or 5, with a descending dysploid series in one taxon. Many of these species have, in fact, been grouped together at the generic or sectional level by earlier authors such as E. L. Greene (1894b), H. M. Hall (1928), L. H. Shinners (1950b), A. Cronquist and D. D. Keck (1957), J. C. Semple (1974), and Hartman (1976, 1990). The following key is based largely on data from Hartman (1976, 1990) and Semple.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inner phyllaries (at least) with proximal portion stalklike, abruptly enlarged into ovate to orbiculate or elliptic blade, 2–5 mm wide, apices acuminate to obtuse or broadly rounded, not bristle-tipped (Texas, s Oklahoma, e New Mexico) (sect. Xanthisma)</description>
      <determination>1 Xanthisma texanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries not markedly expanded distally, linear to broadly oblong or lanceolate, 1–2 mm wide, apices narrowly obtuse to long-attenuate, usually bristle-tipped</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rays white, pink, purple, or lavender, or ray florets 0 (sect. Blepharodon)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rays yellow</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets 0 (n New Mexico and n Arizona to sw Canada)</description>
      <determination>4 Xanthisma grindelioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets present</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants 2.5–14 cm; stems simple; basal leaves (rosettes) persistent; heads 1; (montane to alpine) Colorado, s Wyoming</description>
      <determination>3 Xanthisma coloradoense</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants 15–40 cm; stems often branched distally; basal leaves usually withering by flowering; heads 1–10+; s New Mexico, w Texas, n Mexico</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves usually finely or obscurely serrate or serrulate, usually with 12–25 teeth per side; peduncles hispid or hispidulous; s New Mexico, w Texas</description>
      <determination>2 Xanthisma blephariphyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves serrate, often coarsely, with 5–14 teeth per side; peduncles stipitate-glandular; se New Mexico and w Texas; n Mexico</description>
      <determination>5 Xanthisma gypsophilum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Annuals, taprooted; leaves serrate to dentate, teeth blunt or terminating in a stiff callosity, not bristle-tipped; phyllary apices obtuse to broadly acute, not bristle-tipped; cypselae oblong or narrowly ellipsoid, 18–22-nerved (barely discernible); se New Mexico, w Texas (sect. Havardii)</description>
      <determination>9 Xanthisma viscidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perennials or subshrubs with branched caudices (if taprooted annuals then phyllary apices narrowly acute to acuminate, prominently bristle-tipped); leaves entire, pinnatifid, or deeply 2-pinnatifid, if serrate or dentate, teeth bristle-tipped; cypselae ellipsoid to broadly obovoid, ribs weak yet readily discernible; w North America, Mexico (sect. Sideranthus)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Annuals, taprooted</description>
      <determination>6 Xanthisma gracile</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Perennials or subshrubs, caudices branched, often woody</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Cauline leaves scalelike throughout; peduncle bracts imbricate, grading into phyllaries</description>
      <determination>7 Xanthisma junceum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Cauline leaves not scalelike at least proximal to midstems; peduncle bracts 0–3, leaflike, not grading into phyllaries</description>
      <determination>8 Xanthisma spinulosum</determination>
    </key_statement>
  </key>
</bio:treatment>