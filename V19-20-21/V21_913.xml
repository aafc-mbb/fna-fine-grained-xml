<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="treatment_page">363</other_info_on_meta>
    <other_info_on_meta type="illustration_page">363</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="B. G. Baldwin" date="2000" rank="genus">constancea</taxon_name>
    <taxon_name authority="(A. Gray) B. G. Baldwin" date="2000" rank="species">nevinii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>46: 160. 2000</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus constancea;species nevinii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066413</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriophyllum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">nevinii</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 1(2): 452. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriophyllum;species nevinii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 8–25 cm.</text>
      <biological_entity id="o25043" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 1–5 mm.</text>
      <biological_entity id="o25044" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries 4–6 mm.</text>
      <biological_entity id="o25045" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray laminae 2–3 × 1–2 mm.</text>
      <biological_entity constraint="ray" id="o25046" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc corollas 2–4 mm.</text>
      <biological_entity constraint="disc" id="o25047" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o25048" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappus-scales 0.5–2.5 mm. 2n = 38.</text>
      <biological_entity id="o25049" name="pappus-scale" name_original="pappus-scales" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25050" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal bluffs, cliff faces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="cliff" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Constancea nevinii is known from San Clemente, Santa Barbara, and Santa Catalina islands.</discussion>
  
</bio:treatment>