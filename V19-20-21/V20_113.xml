<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">nauseosa</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety nauseosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068285</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly 20–60 cm.</text>
      <biological_entity id="o19315" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems grayish to white, leafy, loosely tomentose.</text>
      <biological_entity id="o19316" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="grayish" name="coloration" src="d0_s1" to="white" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves grayish green to white;</text>
      <biological_entity id="o19317" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="grayish green" name="coloration" src="d0_s2" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved, narrowly linear, 10–50 × 1–1.5 mm, faces loosely tomentose.</text>
      <biological_entity id="o19318" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19319" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 7–10.5 mm.</text>
      <biological_entity id="o19320" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="10.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 10–18, apices erect, mostly obtuse, abaxial faces tomentose, sometimes sparingly.</text>
      <biological_entity id="o19321" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="18" />
      </biological_entity>
      <biological_entity id="o19322" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19323" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 6.5–9 mm, tubes usually puberulent, rarely arachnose, lobes 1–2 mm, glabrous;</text>
      <biological_entity id="o19324" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19325" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s6" value="arachnose" value_original="arachnose" />
      </biological_entity>
      <biological_entity id="o19326" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages longer than stigmatic portions.</text>
      <biological_entity constraint="style" id="o19327" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic portions" constraintid="o19328" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o19328" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae densely hairy;</text>
      <biological_entity id="o19329" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 5.2–7.2 mm. 2n = 18.</text>
      <biological_entity id="o19330" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s9" to="7.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19331" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Plains and hills, often in barren alkaline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="plains" />
        <character name="habitat" value="hills" />
        <character name="habitat" value="barren alkaline soils" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Colo., Idaho, Mont., Nebr., N.Dak., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21m.</number>
  <other_name type="common_name">Rubber rabbitbrush</other_name>
  
</bio:treatment>