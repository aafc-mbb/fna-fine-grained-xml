<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="genus">carphephorus</taxon_name>
    <taxon_name authority="(Michaux) Torrey &amp; A. Gray" date="1841" rank="species">bellidifolius</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 66. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus carphephorus;species bellidifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066288</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">bellidifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 93. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Liatris;species bellidifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–60 cm.</text>
      <biological_entity id="o6011" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous or glabrate (except peduncles), eglandular.</text>
      <biological_entity id="o6012" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline usually oblanceolate, sometimes nearly spatulate, mostly 4–20 cm;</text>
      <biological_entity id="o6013" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="sometimes nearly" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="4" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline gradually reduced, faces glanddotted.</text>
      <biological_entity id="o6014" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o6015" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6016" name="face" name_original="faces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in open, loose, usually corymbiform, sometimes paniculiform, arrays.</text>
      <biological_entity id="o6017" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o6018" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o6017" id="r445" name="in" negation="false" src="d0_s4" to="o6018" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles minutely puberulent (viscid, hairs not glandular).</text>
      <biological_entity id="o6019" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 7–12 (–15) mm.</text>
      <biological_entity id="o6020" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 15–40+ in 3–5+ series, broadly elliptic to elliptic-obovate, glabrous, eglandular (except margins ciliate), apices rounded.</text>
      <biological_entity id="o6021" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o6022" from="15" name="quantity" src="d0_s7" to="40" upper_restricted="false" />
        <character char_type="range_value" from="broadly elliptic" name="shape" notes="" src="d0_s7" to="elliptic-obovate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o6022" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6023" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles paleate (at least peripherally).</text>
      <biological_entity id="o6024" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas eglandular, lobes 1.5–2.5 mm.</text>
      <biological_entity id="o6025" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o6026" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae minutely sessile-glandular;</text>
      <biological_entity id="o6027" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s10" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappus bristles in 1 (–2) series.</text>
      <biological_entity id="o6029" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s11" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <relation from="o6028" id="r446" name="in" negation="false" src="d0_s11" to="o6029" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity constraint="pappus" id="o6028" name="bristle" name_original="bristles" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o6030" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dunes, sandhills, sandy rises in flatwoods, sandy fields, roadsides, weedy banks, open pine, scrub oak, and turkey oak-pine woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dunes" />
        <character name="habitat" value="sandhills" />
        <character name="habitat" value="sandy rises" constraint="in flatwoods , sandy fields , roadsides , weedy banks , open pine , scrub oak , and turkey oak-pine woods" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="sandy fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="weedy banks" />
        <character name="habitat" value="open pine" />
        <character name="habitat" value="scrub oak" />
        <character name="habitat" value="turkey oak-pine woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–40 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="40" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Sandy-woods chaffhead</other_name>
  
</bio:treatment>