<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="treatment_page">483</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="(Cassini ex Dumortier) Anderberg" date="1989" rank="tribe">plucheeae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">pluchea</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1989" rank="species">yucatanensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>67: 160. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe plucheeae;genus pluchea;species yucatanensis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250067359</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–60 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>probably fibrous-rooted.</text>
      <biological_entity id="o2613" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="probably" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ± stipitate or sessile-glandular, otherwise glabrous.</text>
      <biological_entity id="o2614" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s2" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile;</text>
      <biological_entity id="o2615" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (leathery, slightly succulent, shiny) oblong-obovate to oblong-oblanceolate or broadly lanceolate, 3–5 × (0.6–) 1.5–2 cm (bases subclasping and subauriculate), margins serrulate, faces ± stipitate or sessile-glandular, otherwise glabrous or distalmost minutely puberulent.</text>
      <biological_entity id="o2616" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong-obovate" name="shape" src="d0_s4" to="oblong-oblanceolate or broadly lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_width" src="d0_s4" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2617" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o2618" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o2619" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o2620" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o2621" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o2620" id="r257" name="in" negation="false" src="d0_s5" to="o2621" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate to campanulate, 5–6 × 4–5 mm.</text>
      <biological_entity id="o2622" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s6" to="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries pink to lavender or cream, proximally stipitate or sessile-glandular, distally densely stipitate-glandular (outermost ovatelanceolate, lengths usually 1 times inner, rarely only 0.5 times inner).</text>
      <biological_entity id="o2623" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s7" to="lavender or cream" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s7" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="distally densely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas pink to lavender or cream or pinkish to rosy.</text>
      <biological_entity id="o2624" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s8" to="lavender or cream or pinkish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi persistent, bristles distinct.</text>
      <biological_entity id="o2625" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o2626" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="late May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Low woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="low woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Miss.; Mexico; Central America (Belize).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Belize)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Yucatan camphorweed</other_name>
  <discussion>Pluchea yucatanensis apparently is native along the Gulf and Caribbean coasts of Mexico and Central America, most commonly on the Yucatan Peninsula and in Belize. In the United States, it is known from collections made from 1896 to 1969 in coastal Alabama and Mississippi; it appears to be naturalized in the flora.</discussion>
  <discussion>Pluchea yucatanensis is similar in habit and general appearance to P. foetida and P. baccharis and has been identified as both; the rosy tinted phyllaries and florets are more similar to those of P. baccharis. The glabrous, slightly thickened, shiny leaves and glabrous phyllaries are recognition traits for the species.</discussion>
  
</bio:treatment>