<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="treatment_page">285</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1897" rank="species">lobbii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.</publication_title>
      <place_in_publication>4: 425. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species lobbii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066471</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">lobbii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>9: 109. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species lobbii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–70 cm.</text>
      <biological_entity id="o505" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid.</text>
      <biological_entity id="o506" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades pinnatifid to toothed, faces hispid-hirsute, sometimes sessile or short-stipitate-glandular.</text>
      <biological_entity id="o507" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o508" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s2" to="toothed" />
      </biological_entity>
      <biological_entity id="o509" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid-hirsute" value_original="hispid-hirsute" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o510" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o511" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o510" id="r38" name="in" negation="false" src="d0_s3" to="o511" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads overlapping proximal 0–1/2 of each involucre or not.</text>
      <biological_entity id="o512" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" constraint="of involucre" constraintid="o514" from="0" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o513" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o514" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o512" id="r39" name="subtending" negation="false" src="d0_s4" to="o513" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± evenly sessile or short-stipitate-glandular, including margins and apices, and with pustule-based hairs, at least on midribs.</text>
      <biological_entity id="o515" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o516" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o517" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o518" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pustule-based" value_original="pustule-based" />
      </biological_entity>
      <biological_entity id="o519" name="midrib" name_original="midribs" src="d0_s5" type="structure" />
      <relation from="o515" id="r40" name="including" negation="false" src="d0_s5" to="o516" />
      <relation from="o515" id="r41" name="including" negation="false" src="d0_s5" to="o517" />
      <relation from="o515" id="r42" name="with" negation="false" src="d0_s5" to="o518" />
      <relation from="o515" id="r43" modifier="at-least" name="on" negation="false" src="d0_s5" to="o519" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o520" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o521" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o520" id="r44" name="in" negation="false" src="d0_s6" to="o521" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 3 (–4);</text>
      <biological_entity id="o522" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="4" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae deep yellow, 3–5 mm.</text>
      <biological_entity id="o523" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 3 (–4), all or mostly functionally staminate;</text>
      <biological_entity id="o524" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="4" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" modifier="mostly functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers reddish to dark purple.</text>
      <biological_entity id="o525" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s10" to="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi of (4–) 6–8 (–12) quadrate or oblong to lance-linear, fimbriate to laciniate scales 0.5–1 mm. 2n = 22.</text>
      <biological_entity id="o526" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" modifier="of" name="atypical_quantity" src="d0_s11" to="6" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" modifier="of" name="atypical_quantity" src="d0_s11" to="12" />
        <character char_type="range_value" from="6" modifier="of" name="quantity" src="d0_s11" to="8" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="lance-linear fimbriate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="lance-linear fimbriate" />
      </biological_entity>
      <biological_entity id="o527" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o528" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, open woodlands, sagebrush scrub, disturbed sites, often in sandy or clayey soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="sagebrush scrub" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700(–1800) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Deinandra lobbii occurs on hills and flats of the northern South Coast Ranges and San Francisco Bay area and is disjunct in northeastern California, where putatively introduced. Populations in northeastern California occur at higher elevations than elsewhere (1000–1800 m).</discussion>
  
</bio:treatment>