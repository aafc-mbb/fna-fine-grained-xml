<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">190</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
    <other_info_on_meta type="illustration_page">183</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="section">gyrophyllum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">tripteris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 908. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section gyrophyllum;species tripteris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416309</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tripteris</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="variety">deamii</taxon_name>
    <taxon_hierarchy>genus Coreopsis;species tripteris;variety deamii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tripteris</taxon_name>
    <taxon_name authority="Sherff" date="unknown" rank="variety">smithii</taxon_name>
    <taxon_hierarchy>genus Coreopsis;species tripteris;variety smithii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 90–180+ cm.</text>
      <biological_entity id="o9206" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s0" to="180" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Internodes (± mid-stem) 3–7 (–10+) cm.</text>
      <biological_entity id="o9207" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 5–45 mm;</text>
      <biological_entity id="o9208" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9209" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades usually 3-foliolate, leaflets usually simple, sometimes ± pinnately lobed, ultimate blades ± lanceolate, 4–9 (–12+) cm × (9–) 12–35+ mm.</text>
      <biological_entity id="o9210" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9211" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
      <biological_entity id="o9212" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes more or less pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o9213" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_width" src="d0_s3" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s3" to="35" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 2–5+ cm.</text>
      <biological_entity id="o9214" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of 5–6 oblong bractlets 1.5–5 mm.</text>
      <biological_entity id="o9215" name="calyculus" name_original="calyculi" src="d0_s5" type="structure" />
      <biological_entity id="o9216" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="true" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o9215" id="r634" name="consist_of" negation="false" src="d0_s5" to="o9216" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8, ± oblong to lance-oblong, 6–8 mm.</text>
      <biological_entity id="o9217" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="8" value_original="8" />
        <character char_type="range_value" from="less oblong" name="shape" src="d0_s6" to="lance-oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray laminae 12–22+ mm.</text>
      <biological_entity constraint="ray" id="o9218" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="22" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 40–80+;</text>
      <biological_entity id="o9219" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s8" to="80" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas redbrown to purplish, 5–6 mm.</text>
      <biological_entity id="o9220" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="redbrown" name="coloration" src="d0_s9" to="purplish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae obovate to oblong, 4–5 (–6) mm.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 26.</text>
      <biological_entity id="o9221" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="oblong" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9222" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Heavy loams, moist sands, along streams, boggy meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="heavy loams" />
        <character name="habitat" value="moist sands" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="boggy meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Miss., Mo., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  
</bio:treatment>