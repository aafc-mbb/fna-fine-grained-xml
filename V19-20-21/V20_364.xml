<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="treatment_page">166</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Rydberg" date="1906" rank="genus">OREOCHRYSUM</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>33: 152. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus OREOCHRYSUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oreios, of mountains, and chrysos, gold</other_info_on_name>
    <other_info_on_name type="fna_id">123107</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Cassini" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Rydberg) H. M. Hall" date="unknown" rank="section">Oreochrysum</taxon_name>
    <taxon_hierarchy>genus Haplopappus;section Oreochrysum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="(Rydberg) Semple" date="unknown" rank="subgenus">Oreochrysum</taxon_name>
    <taxon_hierarchy>genus Solidago;subgenus Oreochrysum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 15–60 (–100) cm;</text>
      <biological_entity id="o21132" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes long, slender, scale-leaved, thickening, becoming woody.</text>
      <biological_entity id="o21133" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="scale-leaved" value_original="scale-leaved" />
        <character is_modifier="false" name="width" src="d0_s1" value="thickening" value_original="thickening" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, usually simple, minutely puberulous or hirtellous, stipitate-glandular.</text>
      <biological_entity id="o21134" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="puberulous" value_original="puberulous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline (basal and proximal cauline persistent);</text>
    </statement>
    <statement id="d0_s4">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate to subpetiolate;</text>
      <biological_entity id="o21135" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="petiolate" name="architecture" src="d0_s5" to="subpetiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal and proximal cauline blades 1-nerved, spatulate-oblanceolate, mid and distal elliptic to broadly ovatelanceolate or oblanceolate, margins entire (apices acute to obtuse or rounded), minutely, short-stipitate-glandular or gland-dotted, viscid.</text>
      <biological_entity constraint="basal and proximal cauline" id="o21136" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate-oblanceolate" value_original="spatulate-oblanceolate" />
      </biological_entity>
      <biological_entity constraint="mid distal and" id="o21137" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="broadly ovatelanceolate or oblanceolate" />
      </biological_entity>
      <biological_entity id="o21138" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s6" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads radiate, in distinctly flat-topped, tightly corymbiform arrays.</text>
      <biological_entity id="o21139" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="radiate" value_original="radiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres campanulate to hemispheric, 10–11 × 6–8 mm.</text>
      <biological_entity id="o21140" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s8" to="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 15–24 in 3–4 series, mostly appressed, 1-nerved (rarely with lateral pair; convex proximally, flat beyond), outer lanceolate to ovate, inner broadly lanceolate-oblong, strongly unequal to subequal, herbaceous, slender (apices green-tipped and erect to reflexing), glabrous or hirtellous, minutely stipitate-glandular, non-resinous.</text>
      <biological_entity id="o21141" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o21142" from="15" name="quantity" src="d0_s9" to="24" />
        <character is_modifier="false" modifier="mostly" name="fixation_or_orientation" notes="" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-nerved" value_original="1-nerved" />
      </biological_entity>
      <biological_entity id="o21142" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21143" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="ovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o21144" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character char_type="range_value" from="strongly unequal" name="size" src="d0_s9" to="subequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s9" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="coating" src="d0_s9" value="non-resinous" value_original="non-resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat, pitted, epaleate.</text>
      <biological_entity id="o21145" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 12–20, pistillate, fertile;</text>
      <biological_entity id="o21146" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="20" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow.</text>
      <biological_entity id="o21147" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 25–37, bisexual, fertile;</text>
      <biological_entity id="o21148" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="37" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, tubes shorter than narrowly tubular-funnelform throats, lobes 5, erect to spreading, triangular;</text>
      <biological_entity id="o21149" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21150" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than narrowly tubular-funnelform throats" constraintid="o21151" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21151" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s14" value="tubular-funnelform" value_original="tubular-funnelform" />
      </biological_entity>
      <biological_entity id="o21152" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s14" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branch appendages linear.</text>
      <biological_entity constraint="style-branch" id="o21153" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae fusiform, plump but distinctly compressed, nerves 12–16 (whitish, raised), glabrous;</text>
      <biological_entity id="o21154" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="size" src="d0_s16" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o21155" name="nerve" name_original="nerves" src="d0_s16" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s16" to="16" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi persistent, of 40–60, equal, barbellate, apically attenuate bristles in 2 (–3) series.</text>
      <biological_entity id="o21157" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" name="quantity" src="d0_s17" to="60" />
        <character is_modifier="true" name="variability" src="d0_s17" value="equal" value_original="equal" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s17" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o21158" name="series" name_original="series" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s17" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
      <relation from="o21156" id="r1956" name="consist_of" negation="false" src="d0_s17" to="o21157" />
      <relation from="o21157" id="r1957" name="in" negation="false" src="d0_s17" to="o21158" />
    </statement>
    <statement id="d0_s18">
      <text>x = 9.</text>
      <biological_entity id="o21156" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o21159" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>164.</number>
  <discussion>Species 1.</discussion>
  <discussion>Oreochrysum often has been treated as Solidago parryi; J. C. Semple et al. (1999) placed Oreochrysum at subgeneric rank within Solidago. Oreochrysum is separated from Solidago on the basis of its stipitate-glandular vestiture, large heads in strongly corymbiform arrays, herbaceous phyllaries, prominent rays, narrow disc corollas with relatively short lobes, linear-lanceolate style-branch appendages, and large cypselae. It has no apparent close relatives within Solidago.</discussion>
  
</bio:treatment>