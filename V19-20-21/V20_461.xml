<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="treatment_page">208</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaetopappa</taxon_name>
    <taxon_name authority="(A. Gray) Shinners" date="1946" rank="species">effusa</taxon_name>
    <place_of_publication>
      <publication_title>Wrightia</publication_title>
      <place_in_publication>1: 68. 1946</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chaetopappa;species effusa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066323</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Keerlia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">effusa</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 222. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Keerlia;species effusa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–80 cm, eglandular;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted, with caudexlike, lignescent rhizomes.</text>
      <biological_entity id="o29227" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o29228" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="caudex-like" value_original="caudexlike" />
        <character is_modifier="true" name="texture" src="d0_s1" value="lignescent" value_original="lignescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not densely overlapping, proximal and mid cauline blades oblong to lanceolate-oblong, 10–20 × 6–12 mm, abruptly reduced distally, herbaceous, bases truncate to subcordate, subclasping, flat, faces sparsely hispidulous to hispido-pilose.</text>
      <biological_entity id="o29229" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not densely" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="proximal and mid cauline" id="o29230" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="lanceolate-oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly; distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o29231" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="subcordate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o29232" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparsely hispidulous" name="pubescence" src="d0_s2" to="hispido-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres conic to broadly cylindric, 3–4.5 × 2–2.5 mm.</text>
      <biological_entity id="o29233" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s3" to="broadly cylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 6–9;</text>
      <biological_entity id="o29234" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas white.</text>
      <biological_entity id="o29235" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 4–7, functionally staminate.</text>
      <biological_entity id="o29236" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 1.6–2.2 mm, 2 (–3) -nerved (ray) or 3–5-nerved (disc), strigose;</text>
      <biological_entity id="o29237" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s7" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="2(-3)-nerved" value_original="2(-3)-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-5-nerved" value_original="3-5-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi in 1 series, thickened rings or minute, erose crowns less than 0.1 mm. 2n = 16.</text>
      <biological_entity id="o29238" name="pappus" name_original="pappi" src="d0_s8" type="structure" />
      <biological_entity id="o29239" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o29240" name="ring" name_original="rings" src="d0_s8" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="size" src="d0_s8" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o29241" name="crown" name_original="crowns" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29242" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="16" value_original="16" />
      </biological_entity>
      <relation from="o29238" id="r2698" name="in" negation="false" src="d0_s8" to="o29239" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone cliffs, ledges, bluffs, steep hillsides, sometimes in seepy areas, oak-juniper, oak, or mixed deciduous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="steep hillsides" />
        <character name="habitat" value="seepy areas" />
        <character name="habitat" value="oak-juniper" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="mixed deciduous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Spreading lazy daisy</other_name>
  
</bio:treatment>