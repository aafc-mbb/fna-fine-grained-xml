<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="treatment_page">432</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helenium</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">puberulum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 667. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus helenium;species puberulum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066857</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 50–160 cm.</text>
      <biological_entity id="o16664" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="160" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="160" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, branched distally, strongly winged, glabrous proximally, sparsely hairy distally.</text>
      <biological_entity id="o16666" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s1" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves glabrous or sparsely hairy;</text>
      <biological_entity id="o16667" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades oblanceolate to oblongelliptic, entire;</text>
      <biological_entity constraint="basal" id="o16668" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="oblongelliptic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal and mid-blades lanceolate to oblongelliptic, entire;</text>
      <biological_entity constraint="proximal" id="o16669" name="mid-blade" name_original="mid-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblongelliptic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal blades lance-linear, entire.</text>
      <biological_entity constraint="distal" id="o16670" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 4–20 (–30) per plant, in paniculiform arrays.</text>
      <biological_entity id="o16671" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="30" />
        <character char_type="range_value" constraint="per plant" constraintid="o16672" from="4" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o16672" name="plant" name_original="plant" src="d0_s6" type="structure" />
      <biological_entity id="o16673" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o16671" id="r1150" name="in" negation="false" src="d0_s6" to="o16673" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles (6–) 9–17 (–23) cm, sparsely to moderately hairy.</text>
      <biological_entity id="o16674" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="9" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="23" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s7" to="17" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres globose to depressed-globose, 9–15 × 9–17 (–19) mm.</text>
      <biological_entity id="o16675" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s8" to="depressed-globose" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="19" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (distinct or connate proximally) moderately hairy.</text>
      <biological_entity id="o16676" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0, or 13–15, pistillate, fertile;</text>
      <biological_entity id="o16677" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s10" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 3.8–10 × 2–4 mm.</text>
      <biological_entity id="o16678" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 300–500 (–1000+);</text>
      <biological_entity id="o16679" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="500" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="1000" upper_restricted="false" />
        <character char_type="range_value" from="300" name="quantity" src="d0_s12" to="500" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow proximally, yellow to reddish-brown to purple distally, (1.6–) 1.9–2.7 mm, lobes 4 (–5).</text>
      <biological_entity id="o16680" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="yellow" modifier="distally" name="coloration" src="d0_s13" to="reddish-brown" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s13" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16681" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="5" />
        <character name="quantity" src="d0_s13" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1.2–1.9 mm, moderately hairy;</text>
      <biological_entity id="o16682" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s14" to="1.9" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of 5–6 entire, aristate scales 0.4–1 mm. 2n = 58.</text>
      <biological_entity id="o16683" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity id="o16684" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s15" to="6" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="true" name="shape" src="d0_s15" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16685" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="58" value_original="58" />
      </biological_entity>
      <relation from="o16683" id="r1151" name="consist_of" negation="false" src="d0_s15" to="o16684" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Jun–Aug(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams, ditches, seepage areas, around ponds and lakes, forests, woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="seepage areas" constraint="around ponds and lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Rosilla</other_name>
  <discussion>Helenium puberulum may be of amphidiploid origin from hybridization between H. bigelovii (perennial, radiate, 2n = 32) and H. thurberi (annual, rayless, 2n = 26) (M. W. Bierner 1972).</discussion>
  
</bio:treatment>