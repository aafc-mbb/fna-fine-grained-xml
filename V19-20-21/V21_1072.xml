<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="treatment_page">429</other_info_on_meta>
    <other_info_on_meta type="illustration_page">430</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helenium</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">vernale</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>210. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus helenium;species vernale</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066860</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–80 cm.</text>
      <biological_entity id="o7023" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, usually unbranched distally, weakly winged, glabrous proximally, glabrous or sparsely hairy distally.</text>
      <biological_entity id="o7024" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s1" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually glabrous, rarely sparsely hairy;</text>
      <biological_entity id="o7025" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades obovate to narrowly oblanceolate, usually entire or undulate to undulate-serrate;</text>
      <biological_entity constraint="basal" id="o7026" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s3" to="narrowly oblanceolate usually entire or undulate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s3" to="narrowly oblanceolate usually entire or undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal and mid-blades usually narrowly lanceolate to narrowly oblanceolate, usually entire, sometimes toothed;</text>
      <biological_entity constraint="proximal" id="o7027" name="mid-blade" name_original="mid-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually narrowly lanceolate" name="shape" src="d0_s4" to="narrowly oblanceolate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal blades lance-linear, entire.</text>
      <biological_entity constraint="distal" id="o7028" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1 (–3) per plant, usually borne singly.</text>
      <biological_entity id="o7029" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character constraint="per plant" constraintid="o7030" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually" name="arrangement" notes="" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o7030" name="plant" name_original="plant" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 2–23 cm, usually glabrous, sometimes sparsely hairy.</text>
      <biological_entity id="o7031" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="23" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric, 10–17 × 15–27 mm.</text>
      <biological_entity id="o7032" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s8" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries glabrous or sparsely hairy.</text>
      <biological_entity id="o7033" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 13–30, neuter;</text>
      <biological_entity id="o7034" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s10" to="30" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 15–21 × 5–10 mm.</text>
      <biological_entity id="o7035" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s11" to="21" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 150–650 (–800+);</text>
      <biological_entity id="o7036" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="650" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="800" upper_restricted="false" />
        <character char_type="range_value" from="150" name="quantity" src="d0_s12" to="650" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow proximally, yellow to yellowbrown distally, 4.6–6 mm, lobes 5.</text>
      <biological_entity id="o7037" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="yellow" modifier="distally" name="coloration" src="d0_s13" to="yellowbrown" />
        <character char_type="range_value" from="4.6" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7038" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1–1.5 mm, glabrous;</text>
      <biological_entity id="o7039" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of usually 8 entire or lacerate, non-aristate scales 1.5–2 mm. 2n = 34.</text>
      <biological_entity id="o7040" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character modifier="usually" name="quantity" src="d0_s15" value="8" value_original="8" />
        <character is_modifier="false" name="shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o7041" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="non-aristate" value_original="non-aristate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7042" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ditches, other moist areas such as wet woods, bogs, and swamp edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ditches" />
        <character name="habitat" value="other moist areas" />
        <character name="habitat" value="wet woods" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="swamp edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Savanna or spring sneezeweed</other_name>
  <discussion>The name Helenium nuttallii A. Gray is illegitimate; it has been applied to plants treated here as H. vernale.</discussion>
  
</bio:treatment>