<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">546</other_info_on_meta>
    <other_info_on_meta type="treatment_page">547</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Willdenow" date="1803" rank="genus">mikania</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">batatifolia</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 197. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus mikania;species batatifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067190</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems obscurely 6-angled, sometimes winged on angles, often densely glandular, glabrous or puberulent;</text>
      <biological_entity id="o1113" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s0" value="6-angled" value_original="6-angled" />
        <character constraint="on angles" constraintid="o1114" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="often densely" name="pubescence" notes="" src="d0_s0" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o1114" name="angle" name_original="angles" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>internodes 3.5–13 cm.</text>
      <biological_entity id="o1115" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s1" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petioles 10–40 mm, glabrous, glandular.</text>
      <biological_entity id="o1116" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades deltate-ovate, 1.5–6 × 1–5 cm, (subcoriaceous to somewhat fleshy) bases cordate to subcordate, margins usually hastately dentate to ± lobed (lobes divergent), apices acute to acuminate, faces glabrous (and glanddotted).</text>
      <biological_entity id="o1117" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate-ovate" value_original="deltate-ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1118" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s3" to="subcordate" />
      </biological_entity>
      <biological_entity id="o1119" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually hastately dentate" name="shape" src="d0_s3" to="more or less lobed" />
      </biological_entity>
      <biological_entity id="o1120" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Arrays of heads corymbiform, 2–4 × 2–6 cm.</text>
      <biological_entity id="o1121" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1122" name="head" name_original="heads" src="d0_s4" type="structure" />
      <relation from="o1121" id="r73" name="part_of" negation="false" src="d0_s4" to="o1122" />
    </statement>
    <statement id="d0_s5">
      <text>Heads 4–6 mm.</text>
      <biological_entity id="o1123" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries lanceolate to narrowly ovate, ca. 3.5 mm, apices acuminate (abaxial faces puberulent).</text>
      <biological_entity id="o1124" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="narrowly ovate" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="3.5" value_original="3.5" />
      </biological_entity>
      <biological_entity id="o1125" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Corollas white, ca. 3 mm, glanddotted, lobes deltate.</text>
      <biological_entity id="o1126" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1127" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae brown, 1.5–2 mm, densely glanddotted;</text>
      <biological_entity id="o1128" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of ca. 40 white, barbellate bristles ca. 3 mm.</text>
      <biological_entity id="o1129" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
      <biological_entity id="o1130" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="40" value_original="40" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="barbellate" value_original="barbellate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <relation from="o1129" id="r74" name="consist_of" negation="false" src="d0_s9" to="o1130" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands, savannas, salt marshes, swamps, usually in oölite or coral soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="oölite" modifier="usually" />
        <character name="habitat" value="coral soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Bahamas, Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="past_name">batataefolia</other_name>
  <discussion>J. K. Small (1933), B. L. Robinson (1934), R. W. Long and O. Lakela (1971), and W. C. Holmes (1981, 1993) recognized Mikania batatifolia. Hn. Alain (1962) referred M. batatifolia to M. micrantha Kunth, a common, polymorphic taxon of humid American tropics. A. Cronquist (1980) merged M. batatifolia with M. scandens. In reexamining the members of the M. scandens complex, which includes M. batatifolia (Robinson), it is apparent that M. batatifolia is distinct. Differences with M. scandens, including chemical evidence, were cited by Holmes (1981). For additional information, see Holmes (1981, 1993).</discussion>
  
</bio:treatment>