<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">21</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">parthenium</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1842" rank="species">alpinum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 285. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus parthenium;species alpinum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067286</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bolophyta</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">alpina</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 348. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bolophyta;species alpina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parthenium</taxon_name>
    <taxon_name authority="Barneby" date="unknown" rank="species">tetraneuris</taxon_name>
    <taxon_hierarchy>genus Parthenium;species tetraneuris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parthenium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alpinum</taxon_name>
    <taxon_name authority="(Barneby) Rollins" date="unknown" rank="variety">tetraneuris</taxon_name>
    <taxon_hierarchy>genus Parthenium;species alpinum;variety tetraneuris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 1–2 cm (underground caudices 2–5+ cm, branched; plants cespitose or forming mats).</text>
      <biological_entity id="o21085" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="2" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades oblanceolate to spatulate, 6–18 (–35) × 1–3 (–4+) mm, margins entire, faces strigilloso-sericeous (gray) and obscurely glanddotted.</text>
      <biological_entity id="o21086" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="spatulate" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="35" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s1" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="4" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21087" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21088" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s1" value="strigilloso-sericeous" value_original="strigilloso-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads ± disciform, borne singly.</text>
      <biological_entity id="o21089" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="disciform" value_original="disciform" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 0–5 (–30) mm.</text>
      <biological_entity id="o21090" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries: outer 5–8 ± linear, 4 mm, inner 5–8 ± orbiculate, 4–5 mm.</text>
      <biological_entity id="o21091" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o21092" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="8" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character name="some_measurement" src="d0_s4" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="inner" id="o21093" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="8" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate florets 5–8;</text>
      <biological_entity id="o21094" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla laminae 0 (tubes ± compressed, obscurely 2–4-lobed).</text>
      <biological_entity constraint="corolla" id="o21095" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 18–28+.</text>
      <biological_entity id="o21096" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s7" to="28" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae oblanceoloid, 4 mm (narrowly winged);</text>
      <biological_entity id="o21097" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceoloid" value_original="oblanceoloid" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappuslike enations 2, erect to spreading, ± subulate, 0.5–1 mm (a third, ± subulate element sometimes at apex of adaxial face).</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 36.</text>
      <biological_entity id="o21098" name="enation" name_original="enations" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="pappuslike" value_original="pappuslike" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="spreading" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21099" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shale and calcareous outcrops, red clays</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shale" />
        <character name="habitat" value="calcareous outcrops" />
        <character name="habitat" value="red clays" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Parthenium alpinum is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>