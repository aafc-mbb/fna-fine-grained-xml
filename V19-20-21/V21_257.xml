<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="(Linnaeus) Britton ex Kearney" date="1893" rank="species">alternifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>20: 485. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species alternifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417423</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alternifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 909. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Coreopsis;species alternifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (30–) 100–200+ cm (perennating bases ± erect or horizontal rhizomes, internodes winged, at least proximal).</text>
      <biological_entity id="o8324" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly alternate (proximal sometimes opposite);</text>
      <biological_entity id="o8325" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades lance-elliptic or lanceolate to lance-linear, 10–25+ × 2–8+ cm, bases narrowly cuneate, margins coarsely toothed to subentire, apices attenuate, faces scabrellous.</text>
      <biological_entity id="o8326" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="lance-linear" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="8" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o8327" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o8328" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="coarsely toothed" name="shape" src="d0_s2" to="subentire" />
      </biological_entity>
      <biological_entity id="o8329" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o8330" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads (3–) 8–25 (–50+) in corymbiform to paniculiform arrays.</text>
      <biological_entity id="o8331" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s3" to="8" to_inclusive="false" />
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="50" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o8332" from="8" name="quantity" src="d0_s3" to="25" />
      </biological_entity>
      <biological_entity id="o8332" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s3" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± saucerlike, 10–12+ mm diam.</text>
      <biological_entity id="o8333" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="saucerlike" value_original="saucerlike" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s4" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 8–12+ in 1 (–2) series, ± spreading to reflexed, spatulate or lance-linear to linear, 3–8+ mm.</text>
      <biological_entity id="o8334" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o8335" from="8" name="quantity" src="d0_s5" to="12" upper_restricted="false" />
        <character char_type="range_value" from="less spreading" name="orientation" notes="" src="d0_s5" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character name="shape" src="d0_s5" value="lance-linear to linear" value_original="lance-linear to linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o8335" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets (2–) 6–8+;</text>
      <biological_entity id="o8336" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s6" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 15–25+ mm.</text>
      <biological_entity id="o8337" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 40–60+;</text>
      <biological_entity id="o8338" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s8" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow.</text>
      <biological_entity id="o8339" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae dark-brown to black, oblanceolate to ± orbiculate, 4.5–5 mm, faces sparsely hirtellous to glabrate;</text>
      <biological_entity id="o8340" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s10" to="black" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s10" to="more or less orbiculate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8341" name="face" name_original="faces" src="d0_s10" type="structure">
        <character char_type="range_value" from="sparsely hirtellous" name="pubescence" src="d0_s10" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 1.5–2 mm. 2n = 68.</text>
      <biological_entity id="o8342" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8343" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alluvial flats, along streams, woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alluvial flats" constraint="along streams" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mich., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Verbesina alternifolia may be no longer present in Delaware.</discussion>
  
</bio:treatment>