<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="treatment_page">341</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="(Nuttall) R. Chan" date="2002" rank="section">amphiachaenia</taxon_name>
    <taxon_name authority="R. Chan" date="2002" rank="species">ornduffii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>48: 209. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section amphiachaenia;species ornduffii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067057</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lasthenia</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">macrantha</taxon_name>
    <taxon_name authority="Ornduff" date="unknown" rank="subspecies">prisca</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>21: 96. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lasthenia;species macrantha;subspecies prisca;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 4–28 cm.</text>
      <biological_entity id="o12955" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="28" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent, branched proximally, ± hairy throughout, more so distally.</text>
      <biological_entity id="o12956" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less; throughout" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear to oblong, 6–40 × 1.8–5 (–18) mm, (± fleshy) margins entire or with 3–5+ teeth, faces glabrous or ± hairy.</text>
      <biological_entity id="o12957" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="18" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12958" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="with 3-5+ teeth" value_original="with 3-5+ teeth" />
      </biological_entity>
      <biological_entity id="o12959" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12960" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o12958" id="r887" name="with" negation="false" src="d0_s2" to="o12959" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres campanulate to depressed-hemispheric, 5–14 mm.</text>
      <biological_entity id="o12961" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s3" to="depressed-hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries (± persistent) 8–14 (in 2 series), elliptic to ovate, ± hairy.</text>
      <biological_entity id="o12962" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" to="14" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles conic, muricate, glabrous.</text>
      <biological_entity id="o12963" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="relief" src="d0_s5" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 8–15;</text>
      <biological_entity id="o12964" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae elliptic to oblong, 5–9 (–13.4) mm.</text>
      <biological_entity id="o12965" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="oblong" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="13.4" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anther appendages deltate to sublanceolate.</text>
      <biological_entity constraint="anther" id="o12966" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s8" to="sublanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae silver-gray, linear to narrowly clavate, to 4 mm, glabrous;</text>
      <biological_entity id="o12967" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="silver-gray" value_original="silver-gray" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="narrowly clavate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 0, or of 1–4 translucent, brown, subulate, aristate scales (often variable within heads).</text>
      <biological_entity id="o12969" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="4" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s10" value="translucent" value_original="translucent" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="true" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
      <relation from="o12968" id="r888" name="consist_of" negation="false" src="d0_s10" to="o12969" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 32.</text>
      <biological_entity id="o12968" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12970" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round (mostly May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Ornduff’s goldfields</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Lasthenia ornduffii is known only from six or so populations in grasslands along the immediate coast in Curry County. The plants are usually scapiform.</discussion>
  
</bio:treatment>