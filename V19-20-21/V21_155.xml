<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan R. Smith</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="treatment_page">71</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">ZINNIA</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1189, 1221, 1377. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus ZINNIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Johann Gottfried Zinn, 1727–1759, professor of botany, Göttingen, known for botanical studies in Mexico</other_info_on_name>
    <other_info_on_name type="fna_id">135326</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or subshrubs [perennials], 10–100 (–200+) cm.</text>
      <biological_entity id="o22382" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o22383" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate or erect.</text>
      <biological_entity id="o22384" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite or subopposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile [petiolate];</text>
      <biological_entity id="o22385" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (1-nerved, 3-nerved, or 5-nerved from bases) acerose, elliptic, lance-linear, lanceolate, linear, oblong, or ovate, bases rounded to cuneate, margins entire, faces hairy (often scabrous or scabrellous), usually glanddotted.</text>
      <biological_entity id="o22386" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o22387" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o22388" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o22389" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads usually radiate (rarely ± discoid in Z. anomala), borne singly.</text>
      <biological_entity id="o22390" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, cylindric, to hemispheric or broader, 5–25 mm diam.</text>
      <biological_entity id="o22391" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="hemispheric" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 12–30+ in 3–4+ series (orbiculate to obovate or oblong, unequal, often colored or dark-banded distally, outer shorter).</text>
      <biological_entity id="o22392" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o22393" from="12" name="quantity" src="d0_s8" to="30" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o22393" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic, paleate (paleae yellowish, often reddish to purplish distally, chartaceous to scarious, conduplicate, apices rounded to acute, sometimes fimbriate).</text>
      <biological_entity id="o22394" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets usually 5–21 (more in “double” cultivars, sometimes 0 in Z. anomala), pistillate, fertile;</text>
      <biological_entity id="o22395" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="21" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, orange, red, maroon, purple, or white (laminae persistent, sessile or nearly so, becoming papery, sometimes much reduced).</text>
      <biological_entity id="o22396" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 20–150+, bisexual, fertile;</text>
      <biological_entity id="o22397" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="150" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas usually yellow to reddish, sometimes purple-tinged, tubes much shorter than cylindric throats, lobes 5, lanceovate (usually unequal, usually villous or velutinous adaxially).</text>
      <biological_entity id="o22398" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="usually yellow" name="coloration" src="d0_s13" to="reddish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o22399" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than cylindric throats" constraintid="o22400" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o22400" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o22401" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceovate" value_original="lanceovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 3-angled (ray) or flattened (disc; not winged);</text>
      <biological_entity id="o22402" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, or persistent, of 1–3 (–4) awns or toothlike scales.</text>
      <biological_entity id="o22404" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s15" to="4" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="3" />
      </biological_entity>
      <biological_entity id="o22405" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s15" to="4" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="3" />
      </biological_entity>
      <relation from="o22403" id="r1540" name="consists_of" negation="false" src="d0_s15" to="o22404" />
      <relation from="o22403" id="r1541" name="consists_of" negation="false" src="d0_s15" to="o22405" />
    </statement>
    <statement id="d0_s16">
      <text>x = 12 (11, 10).</text>
      <biological_entity id="o22403" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="toothlike" value_original="toothlike" />
      </biological_entity>
      <biological_entity constraint="x" id="o22406" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
        <character name="quantity" src="d0_s16" value="[11" value_original="[11" />
        <character name="quantity" src="d0_s16" value="10]" value_original="10]" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, Central America, South America (one species to Argentina, Bolivia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (one species to Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>266.</number>
  <other_name type="common_name">Zinnia</other_name>
  <discussion>Species ca. 17 (5 in the flora).</discussion>
  <discussion>A. M. Torres (1963) recognized subg. Diplothrix, comprising six species, including the three perennial species treated here, and subg. Zinnia, comprising 11 species, mostly annuals. This division is reflected in the first couplet of the key.</discussion>
  <discussion>Zinnia angustifolia Kunth (= Z. linearis Bentham), native to northern and western Mexico, is commonly grown as an ornamental in the United States and has been reported from Utah (S. L. Welsh et al. 1993); the record was likely from a cultivated source. The species also persists in gardens in California; it is not known outside of cultivation. It can be distinguished from other zinnias by the combination of annual habit, plants to 50 cm, leaf blades linear to narrowly elliptic (mostly 2–7 cm × 4–8 mm), involucres mostly hemispheric, usually much less than 1 cm high or wide, bright orange ray corollas (white-rayed and other color variants known in cultivation), and lobes of disc flowers glabrous or nearly so. Hybrids between Z. angustifolia and Z. violacea are known in the horticultural trade.</discussion>
  <discussion>The lack of articulation of the corolla tubes in the ray florets of Zinnia verticillata Andrews (= Z. peruviana) and the bilateral disposition of vascular bundles (continuous with vasculature of the ovary walls) in the ray florets led D. Don (1830) to conclude that true ray “corollas” in Zinnia are lacking, being replaced instead by de novo petaloid structures that mimic ray corollas of other Compositae.</discussion>
  <references>
    <reference>Don, D. 1830. On the origin and nature of the ligulate rays in Zinnia; and on a remarkable multiplication observed in the parts of fructification of that genus. Trans. Linn. Soc. London 16: 155–158.</reference>
    <reference>Torres, A. M. 1963. Taxonomy of Zinnia. Brittonia 15: 1–25.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals, 30–50(–200) cm; leaf blades elliptic, lanceolate, oblong, or ovate; involucres campanulate, or hemispheric or broader, 10–25 mm diam</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Subshrubs, to 22 cm; leaf blades linear to acerose; involucres narrowly campanulate, cylindric, or subhemispheric, 5–10mm diam</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres narrowly to broadly campanulate, 9–18 × 10–20 mm; paleae apically obtuse, erose or subentire; pappi usually of 1 awn (disc cypselae)</description>
      <determination>4 Zinnia peruviana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres hemispheric or broader, 10–15 × 15–25 mm; paleae apically fimbriate; pappi 0</description>
      <determination>5 Zinnia violacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 1-nerved, linear to acerose; ray florets 4–7, corollas usually white, sometimes pale yellow</description>
      <determination>1 Zinnia acerosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 1- or 3-nerved (some larger leaves), linear; ray florets usually 5–8, corollas usually yellow (laminae sometimes 0)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucres 8–10 mm; ray laminae 0–6(–9) mm</description>
      <determination>2 Zinnia anomala</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucres 5–8 mm; ray laminae mostly 10–18 mm</description>
      <determination>3 Zinnia grandiflora</determination>
    </key_statement>
  </key>
</bio:treatment>