<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">504</other_info_on_meta>
    <other_info_on_meta type="treatment_page">518</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="(Miller) Lessing" date="1832" rank="subgenus">ABSINTHIUM</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Gen. Compos.,</publication_title>
      <place_in_publication>264. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus ABSINTHIUM</taxon_hierarchy>
    <other_info_on_name type="fna_id">316928</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Miller" date="unknown" rank="subgenus">Absinthium</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 1. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>subgenus Absinthium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="(Miller) Darijma" date="unknown" rank="subsection">Absinthium</taxon_name>
    <taxon_hierarchy>genus Artemisia;subsection Absinthium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted, caudices woody, rhizomes absent.</text>
      <biological_entity id="o21241" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o21242" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o21243" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not wandlike.</text>
      <biological_entity id="o21244" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="wand-like" value_original="wandlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous or persistent, basal and/or cauline (petiolate or sessile, not in fascicles).</text>
      <biological_entity id="o21245" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21246" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o21247" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads disciform.</text>
      <biological_entity id="o21248" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="disciform" value_original="disciform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles epaleate, villous (hairs relatively long).</text>
      <biological_entity id="o21249" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="epaleate" value_original="epaleate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Florets: peripheral 6–27 pistillate and fertile;</text>
      <biological_entity id="o21250" name="floret" name_original="florets" src="d0_s6" type="structure" />
      <biological_entity constraint="peripheral" id="o21251" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="27" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>central 15–100 bisexual and fertile;</text>
      <biological_entity id="o21252" name="floret" name_original="florets" src="d0_s7" type="structure" />
      <biological_entity constraint="central" id="o21253" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="100" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas (pale-yellow) funnelform.</text>
      <biological_entity id="o21254" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity id="o21255" name="corolla" name_original="corollas" src="d0_s8" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate regions, North America, South America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>119c.</number>
  <discussion>Species ca. 40 (5 in the flora).</discussion>
  <references>
    <reference>Besser, W. S. J. G. von. 1829. Lettre [on Artemisia including Absinthium]...au Directeur. Bull. Soc. Imp. Naturalistes Moscou 1: 219–265.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 10–40 or 40–60(–100) cm; leaves pinnately lobed (basal 2–3-pinnatifid) or 1–2-ternately lobed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 5–50 cm; leaves entire or 1–3-pinnately lobed</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves pinnately lobed (basal 2–3-pinnatifid, lobes obovate)</description>
      <determination>19 Artemisia absinthium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves 1–2-ternately lobed (lobes filiform, to 0.5 mm wide)</description>
      <determination>20 Artemisia frigida</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucres 3–5 mm diam</description>
      <determination>20 Artemisia frigida</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucres 4–8 mm diam</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves bright green, faces glabrous or sparsely hairy; phyllary margins light green.</description>
      <determination>22 Artemisia rupestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves gray-green, faces canescent to villous; phyllary margins black to brown</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads borne singly or (2–5) in paniculiform to racemiform arrays; corolla lobes glabrous</description>
      <determination>21 Artemisia pattersonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads (5–22) in spiciform arrays; corolla lobes hairy</description>
      <determination>23 Artemisia scopulorum</determination>
    </key_statement>
  </key>
</bio:treatment>