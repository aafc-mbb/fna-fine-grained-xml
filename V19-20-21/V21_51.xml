<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">29</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">OXYTENIA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 20. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus OXYTENIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oxytenes, acuminate, “in allusion to the rigid narrow foliage”</other_info_on_name>
    <other_info_on_name type="fna_id">123533</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 50–200 cm.</text>
      <biological_entity id="o16549" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, virgately branched.</text>
      <biological_entity id="o16551" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="virgately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or ± sessile;</text>
      <biological_entity id="o16552" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly pinnately lobed (lobes 3–7+, linear to filiform), distal (not lobed) linear to filiform, faces usually sericeous to strigillose, sometimes glabrate or glabrous, usually glanddotted.</text>
      <biological_entity id="o16553" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16554" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="filiform" />
      </biological_entity>
      <biological_entity id="o16555" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually sericeous" name="pubescence" src="d0_s5" to="strigillose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads disciform, in (ebracteate or nearly so) paniculiform arrays, or borne singly or in glomerules of 1–5+.</text>
      <biological_entity id="o16556" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o16557" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o16558" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o16556" id="r1140" name="in" negation="false" src="d0_s6" to="o16557" />
      <relation from="o16556" id="r1141" name="borne" negation="false" src="d0_s6" to="o16558" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± hemispheric, 4–5+ mm diam.</text>
      <biological_entity id="o16559" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 10–15+ in 2–3 series, distinct, outer 5–7 herbaceous, inner scarious to membranous (± villous).</text>
      <biological_entity id="o16560" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o16561" from="10" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o16561" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o16562" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="7" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16563" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="scarious" name="texture" src="d0_s8" to="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex;</text>
      <biological_entity id="o16564" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleae spatulate to cuneiform, membranous, distally ± villous.</text>
      <biological_entity id="o16565" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s10" to="cuneiform" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="distally more or less" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate florets 5;</text>
      <biological_entity id="o16566" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 0.</text>
      <biological_entity id="o16567" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Functionally staminate florets 10–25+;</text>
      <biological_entity id="o16568" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="25" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas whitish, funnelform, lobes 5, erect (filaments connate, anthers distinct or weakly coherent).</text>
      <biological_entity id="o16569" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o16570" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae plumply obovoid, obcompressed or weakly 3–4-angled, smooth, ± villous, little, if at all, glanddotted;</text>
      <biological_entity id="o16571" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="plumply" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s15" value="3-4-angled" value_original="3-4-angled" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s15" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 18.</text>
      <biological_entity id="o16572" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o16573" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>251.</number>
  <other_name type="common_name">Copper-weed</other_name>
  <discussion>Species 1.</discussion>
  
</bio:treatment>