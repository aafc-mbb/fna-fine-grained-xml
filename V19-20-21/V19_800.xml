<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="treatment_page">477</other_info_on_meta>
    <other_info_on_meta type="illustration_page">477</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="(Cassini ex Dumortier) Anderberg" date="1989" rank="tribe">plucheeae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">pterocaulon</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">virgatum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 454. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe plucheeae;genus pterocaulon;species virgatum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242428642</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">virgatum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1211. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species virgatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–15 dm.</text>
      <biological_entity id="o7181" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades linear to narrowly elliptic or linear-lanceolate, 5–10 (–15) cm × (2–) 5–10 (–14) mm, lengths mostly 6–8 times widths, margins entire or minutely denticulate (revolute).</text>
      <biological_entity id="o7182" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s1" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s1" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="l_w_ratio" src="d0_s1" value="6-8" value_original="6-8" />
      </biological_entity>
      <biological_entity id="o7183" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s1" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads in open, interrupted, ± cylindric arrays (5–) 8–20 cm (main axes visible between glomerules of heads).</text>
      <biological_entity id="o7184" name="head" name_original="heads" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres campanulate to cylindric, 4–5 mm.</text>
      <biological_entity id="o7185" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s3" to="cylindric" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pistillate florets 25–50.</text>
      <biological_entity id="o7186" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s4" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Functionally staminate florets 2–4 (–5).</text>
      <biological_entity id="o7187" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 1–1.4 mm.</text>
      <biological_entity id="o7188" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy areas, ditches, moist places in woods, in sand, sandy loam, and sandy clay</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy areas" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="places" modifier="moist" constraint="in woods" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="sand" modifier="in" />
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="sandy clay" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Tex.; Mexico (Tamaulipas); West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Wand blackroot</other_name>
  
</bio:treatment>