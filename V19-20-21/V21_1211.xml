<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">481</other_info_on_meta>
    <other_info_on_meta type="treatment_page">482</other_info_on_meta>
    <other_info_on_meta type="illustration_page">480</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ageratum</taxon_name>
    <taxon_name authority="Zuccagni" date="1806" rank="species">corymbosum</taxon_name>
    <place_of_publication>
      <publication_title>Cent. Observ. Bot., no.</publication_title>
      <place_in_publication>85. 1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus ageratum;species corymbosum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066025</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ageratum</taxon_name>
    <taxon_name authority="Hemsley" date="unknown" rank="species">corymbosum</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">jaliscense</taxon_name>
    <taxon_hierarchy>genus Ageratum;species corymbosum;variety jaliscense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ageratum</taxon_name>
    <taxon_name authority="Hemsley" date="unknown" rank="species">salicifolium</taxon_name>
    <taxon_hierarchy>genus Ageratum;species salicifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ageratum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">strictum</taxon_name>
    <taxon_hierarchy>genus Ageratum;species strictum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 30–100 cm (fibrous-rooted).</text>
      <biological_entity id="o450" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o451" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to basally decumbent, puberulent to minutely strigoso-hispid.</text>
      <biological_entity id="o452" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="basally decumbent" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s1" to="minutely strigoso-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ovate to rhombic-lanceolate, 3–8 × 1–3.5 cm, margins toothed, abaxial faces usually puberulent, sometimes minutely strigoso-hispid, densely glanddotted.</text>
      <biological_entity id="o453" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="rhombic-lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o454" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o455" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s2" value="strigoso-hispid" value_original="strigoso-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles finely puberulent (not pilose), eglandular.</text>
      <biological_entity id="o456" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 5–6 mm.</text>
      <biological_entity id="o457" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries narrowly lanceolate (0.4–0.7 mm wide, innermost often 1–1.5 mm longer than outer), finely puberulent, eglandular, tips green or purplish, filiform.</text>
      <biological_entity id="o458" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o459" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas usually blue to lavender, sometimes white.</text>
      <biological_entity id="o460" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually blue" name="coloration" src="d0_s6" to="lavender" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae glabrous;</text>
      <biological_entity id="o461" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi usually crowns of connate scales with erose margins or tubular portions longer than divisions, rarely with 1 or more awnlike lobes.</text>
      <biological_entity id="o462" name="pappus" name_original="pappi" src="d0_s8" type="structure" />
      <biological_entity id="o464" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o465" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o466" name="portion" name_original="portions" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="tubular" value_original="tubular" />
        <character constraint="than divisions" constraintid="o467" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o467" name="division" name_original="divisions" src="d0_s8" type="structure" />
      <biological_entity id="o468" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <relation from="o463" id="r28" name="part_of" negation="false" src="d0_s8" to="o464" />
      <relation from="o463" id="r29" name="with" negation="false" src="d0_s8" to="o465" />
      <relation from="o463" id="r30" name="with" negation="false" src="d0_s8" to="o466" />
      <relation from="o463" id="r31" modifier="rarely" name="with" negation="false" src="d0_s8" to="o468" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 20, 30, 40.</text>
      <biological_entity id="o463" name="crown" name_original="crowns" src="d0_s8" type="structure" constraint="scale" constraint_original="scale; scale">
        <character constraint="than divisions" constraintid="o467" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o469" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
        <character name="quantity" src="d0_s9" value="30" value_original="30" />
        <character name="quantity" src="d0_s9" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices, ledges, cliffs, other rocky sites in canyons, along streams, in desert grasslands, oak-agave, oak, oak-juniper, and pine-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="other rocky sites" constraint="in canyons" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="oak" modifier="oak-agave" />
        <character name="habitat" value="oak-juniper" />
        <character name="habitat" value="pine-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(900–)1200–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1900" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Flat-top whiteweed</other_name>
  <discussion>Habitat information came mostly from collections from Sonora and Chihuahua, Mexico. Ageratum corymbosum grows in all Mexican states except for the extreme southeast. It has been included in various summaries as occurring in Texas; as noted by D. S. Correll and M. C. Johnston (1970), those records apparently were based on a collection by Charles Wright from southwestern New Mexico.</discussion>
  <discussion>Varieties and forms of Ageratum corymbosum have been recognized (e.g., M. F. Johnson 1971; R. McVaugh 1984). McVaugh wryly noted that extremes of these intergrading infraspecific entities “can be recognized with a little imagination.” The form that reaches the United States (with ovate-lanceolate leaves) is var. jaliscense.</discussion>
  
</bio:treatment>