<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">130</other_info_on_meta>
    <other_info_on_meta type="illustration_page">131</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">borrichia</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">frutescens</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 489. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus borrichia;species frutescens</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220001813</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Buphthalmum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">frutescens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 903. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Buphthalmum;species frutescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually erect, sometimes decumbent or arching.</text>
      <biological_entity id="o18564" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves (at least mid cauline) obovate or elliptic to oblanceolate, 4.5–8 (–11) × 1–3 cm, (petioles or blade bases usually with 1–2+ spine-tipped teeth) margins dentate to serrate (usually only proximal 1/3–1/2, rarely most of each margin), faces usually villous (longer hairs) and/or sericeous (shorter hairs), rarely glabrous.</text>
      <biological_entity id="o18565" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s1" to="oblanceolate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="11" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18566" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="dentate" name="architecture_or_shape" src="d0_s1" to="serrate" />
      </biological_entity>
      <biological_entity id="o18567" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads borne singly (peduncles 2–6 cm).</text>
      <biological_entity id="o18568" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres globose, 5–8 mm.</text>
      <biological_entity id="o18569" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="globose" value_original="globose" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 20–40 in 3–4 series (reflexed in fruit, outer shorter, apices spine-tipped, inner similar, spines spreading to erect, faces sericeous).</text>
      <biological_entity id="o18570" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o18571" from="20" name="quantity" src="d0_s4" to="40" />
      </biological_entity>
      <biological_entity id="o18571" name="series" name_original="series" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Paleae: apices spine-tipped.</text>
      <biological_entity id="o18572" name="palea" name_original="paleae" src="d0_s5" type="structure" />
      <biological_entity id="o18573" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 15–30;</text>
      <biological_entity id="o18574" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s6" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 6–8 × 2.5–6 mm.</text>
      <biological_entity id="o18575" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 20–75;</text>
      <biological_entity id="o18576" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="75" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 5–6.5 mm, lobes 1.75–2.5 mm.</text>
      <biological_entity id="o18577" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18578" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.75" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2.5–4 (–5) mm, 3–4-angled.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = usually 28, rarely 42 (2n = 3x).</text>
      <biological_entity id="o18579" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-4-angled" value_original="3-4-angled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18580" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" unit="usually ,rarely" value="28" value_original="28" />
        <character name="quantity" src="d0_s11" unit="usually ,rarely" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Salt marsh communities, brackish backwaters, limestone rocky shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="salt marsh communities" />
        <character name="habitat" value="brackish backwaters" />
        <character name="habitat" value="rocky shores" modifier="limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex., Va.; Mexico; introduced in West Indies, Bermuda.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="in West Indies" establishment_means="introduced" />
        <character name="distribution" value="Bermuda" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Borrichia frutescens grows in salt marsh and brackish backwaters along the Atlantic and Gulf coasts from Virginia to the Yucatan Peninsula in Mexico, and inland along the Rio Grande Valley in Texas. It is found on limestone rocky shores in the Florida Keys. It is introduced in Bermuda, the Bahamas, and Cuba.</discussion>
  
</bio:treatment>