<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">339</other_info_on_meta>
    <other_info_on_meta type="illustration_page">340</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">bellidiastrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">bellidiastrum</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species bellidiastrum;variety bellidiastrum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068313</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems intricately branched;</text>
      <biological_entity id="o96" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="intricately" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal mostly 1–2 (–2.5) mm wide.</text>
      <biological_entity constraint="proximal" id="o97" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="width" src="d0_s1" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline blades linear to linear-oblanceolate, 10–15 (–30) × 1–2.5 (–3) mm, margins entire or rarely with pair of shallow teeth.</text>
      <biological_entity id="o98" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal and proximal cauline" id="o99" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="linear-oblanceolate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o101" name="pair" name_original="pair" src="d0_s2" type="structure" />
      <biological_entity id="o102" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="depth" src="d0_s2" value="shallow" value_original="shallow" />
      </biological_entity>
      <relation from="o100" id="r8" name="with" negation="false" src="d0_s2" to="o101" />
      <relation from="o101" id="r9" name="part_of" negation="false" src="d0_s2" to="o102" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 18, 36.</text>
      <biological_entity id="o100" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s2" value="rarely" value_original="rarely" />
      </biological_entity>
      <biological_entity constraint="2n" id="o103" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="18" value_original="18" />
        <character name="quantity" src="d0_s3" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open habitats in deep, loose sand, areas of desert shrub, sagebrush, juniper, or pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="habitats" modifier="open" constraint="in deep , loose sand , areas of desert shrub , sagebrush , juniper , or pinyon-juniper woodlands" />
        <character name="habitat" value="deep" modifier="in" />
        <character name="habitat" value="loose sand" />
        <character name="habitat" value="areas" constraint="of desert shrub , sagebrush , juniper , or pinyon-juniper woodlands" />
        <character name="habitat" value="desert shrub" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., Mont., Nebr., N.Mex., Okla., S.Dak., Tex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>158b.</number>
  
</bio:treatment>