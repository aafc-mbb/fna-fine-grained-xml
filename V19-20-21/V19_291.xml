<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="treatment_page">230</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="Greene" date="1895" rank="species">modocensis</taxon_name>
    <taxon_name authority="(Coville) Babcock &amp; Stebbins" date="1938" rank="subspecies">rostrata</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>504: 152. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species modocensis;subspecies rostrata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068223</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Coville" date="unknown" rank="species">rostrata</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>3: 564, plate 25. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crepis;species rostrata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–30 cm;</text>
      <biological_entity id="o3472" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>setae whitish, conspicuously curled or crisped (1–2 mm).</text>
      <biological_entity id="o3473" name="seta" name_original="setae" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s1" value="curled" value_original="curled" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crisped" value_original="crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems densely setose or tomentose.</text>
      <biological_entity id="o3474" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petioles setose;</text>
      <biological_entity id="o3475" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3476" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="setose" value_original="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 10–15 × 3–4 cm (tomentulose or glabrate, midribs setose).</text>
      <biological_entity id="o3477" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3478" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 12–17 mm.</text>
      <biological_entity id="o3479" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries densely setose or tomentose.</text>
      <biological_entity id="o3480" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae greenish black, 7–10 mm, beaked (beaks 1–2 mm, ribs alternating strong and weak);</text>
      <biological_entity id="o3481" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish black" value_original="greenish black" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi 7–10 mm. 2n = 22, 33, 44.</text>
      <biological_entity id="o3482" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3483" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="22" value_original="22" />
        <character name="quantity" src="d0_s8" value="33" value_original="33" />
        <character name="quantity" src="d0_s8" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry open places, rocky ridges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry open places" />
        <character name="habitat" value="rocky ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11c.</number>
  <discussion>Subspecies rostrata is distinguished mainly by the dense curly setae of the involucres and cypselae with relatively short, coarse beaks.</discussion>
  
</bio:treatment>