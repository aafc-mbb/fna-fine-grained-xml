<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1832" rank="subtribe">flaveriinae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">flaveria</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="species">linearis</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Sp. Pl.,</publication_title>
      <place_in_publication>33. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe flaveriinae;genus flaveria;species linearis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066775</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Flaveria</taxon_name>
    <taxon_name authority="(J. R. Johnston) R. W. Long &amp; E. L. Rhamstine" date="unknown" rank="species">×latifolia</taxon_name>
    <taxon_hierarchy>genus Flaveria;species ×latifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–80 cm (glabrous or pubescent, mostly on distal peduncles).</text>
      <biological_entity id="o2211" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o2212" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o2213" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear, 50–100 (–130) × 1–4 (–15) mm, ± connate, margins entire or spinulose-serrulate.</text>
      <biological_entity id="o2214" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="130" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o2215" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinulose-serrulate" value_original="spinulose-serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 10–150+, in clusters in corymbiform-paniculiform arrays.</text>
      <biological_entity id="o2216" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s4" to="150" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2217" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform-paniculiform" value_original="corymbiform-paniculiform" />
      </biological_entity>
      <relation from="o2216" id="r163" modifier="in clusters" name="in" negation="false" src="d0_s4" to="o2217" />
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of 1–3 linear bractlets 1–2.5 mm.</text>
      <biological_entity id="o2218" name="calyculus" name_original="calyculi" src="d0_s5" type="structure" />
      <biological_entity id="o2219" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o2218" id="r164" name="consist_of" negation="false" src="d0_s5" to="o2219" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres oblong-angular, 3.3–4.5 mm.</text>
      <biological_entity id="o2220" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="oblong-angular" value_original="oblong-angular" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 5 (–6), linear or oblong.</text>
      <biological_entity id="o2221" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="6" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0 or 1;</text>
      <biological_entity id="o2222" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s8" unit="or" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae yellow, oval to obovate-spatulate, 2–3 mm.</text>
      <biological_entity id="o2223" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s9" to="obovate-spatulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets (2–) 5–7 (–8);</text>
      <biological_entity id="o2224" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="8" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla-tubes 0.8–1.2 mm, throats basally tubular, becoming funnelform-campanulate apically, 1–1.5 mm.</text>
      <biological_entity id="o2225" name="corolla-tube" name_original="corolla-tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2226" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character is_modifier="false" modifier="becoming; apically" name="shape" src="d0_s11" value="funnelform-campanulate" value_original="funnelform-campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae linear, 1.2–1.8 mm;</text>
      <biological_entity id="o2227" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0.2n = 36.</text>
      <biological_entity id="o2228" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2229" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, beaches, hammocks, pinelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="pinelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10(–20+) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="20+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico (Quintana Roo, Yucatán); West Indies (Bahamas, Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico (Quintana Roo)" establishment_means="native" />
        <character name="distribution" value="Mexico (Yucatán)" establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Narrowleaf yellowtops</other_name>
  <discussion>Flaveria linearis is variable; it typically has linear leaves, calyculi of relatively short, linear bractlets, and oblong-angular involucres. The heads are relatively small with 5–8 florets, and throats of the disc corollas are tubular at the base, abruptly expanding distally to become funnelform-campanulate. Plants of this species, the most common Flaveria in Florida, occur throughout most of the Florida peninsula, often near the coast.</discussion>
  
</bio:treatment>