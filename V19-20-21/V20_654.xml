<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="treatment_page">304</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Canby" date="1888" rank="species">tweedyi</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>13: 17. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species tweedyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066690</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 4–20 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices multicipital crowns or branches relatively short and thick.</text>
      <biological_entity id="o19445" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o19446" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o19447" name="crown" name_original="crowns" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="multicipital" value_original="multicipital" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o19448" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="multicipital" value_original="multicipital" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending or decumbent-prostrate, densely strigose (hairs relatively thin), eglandular.</text>
      <biological_entity id="o19449" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending or decumbent-prostrate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o19450" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal (silvery) blades spatulate, broadly elliptic, 50–250 × 5–13 mm, cauline abruptly reduced distally, margins entire, faces densely strigoso-sericeous (hairs gray-white), eglandular.</text>
      <biological_entity constraint="basal" id="o19451" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="250" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o19452" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o19453" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19454" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="strigoso-sericeous" value_original="strigoso-sericeous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–4.</text>
      <biological_entity id="o19455" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 4–6 × 9–14 mm.</text>
      <biological_entity id="o19456" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series, finely hirsuto-strigose, sparsely minutely glandular.</text>
      <biological_entity id="o19457" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" notes="" src="d0_s7" value="hirsuto-strigose" value_original="hirsuto-strigose" />
        <character is_modifier="false" modifier="sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o19458" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o19457" id="r1806" name="in" negation="false" src="d0_s7" to="o19458" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 20–50;</text>
      <biological_entity id="o19459" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas blue to purple, sometimes white, 5–9 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o19460" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19461" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.1–4.3 mm.</text>
      <biological_entity constraint="disc" id="o19462" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s10" to="4.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2–2.5 mm, 2-nerved, faces strigoso-sericeous;</text>
      <biological_entity id="o19463" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o19464" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="strigoso-sericeous" value_original="strigoso-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 20–30 bristles.</text>
      <biological_entity id="o19465" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o19466" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o19467" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
      <relation from="o19465" id="r1807" name="outer of" negation="false" src="d0_s12" to="o19466" />
      <relation from="o19465" id="r1808" name="inner of" negation="false" src="d0_s12" to="o19467" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clay hills, rocky slopes, limestone taluses, shale outcrops, sagebrush-grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clay hills" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="limestone taluses" />
        <character name="habitat" value="shale outcrops" />
        <character name="habitat" value="sagebrush-grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1300–)1600–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>71.</number>
  <other_name type="common_name">Tweedy’s fleabane</other_name>
  
</bio:treatment>