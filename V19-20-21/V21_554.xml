<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">pectis</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">angustifolia</taxon_name>
    <taxon_name authority="(de Candolle) D. J. Keil" date="1977" rank="variety">tenella</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>79: 58. 1977</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus pectis;species angustifolia;variety tenella</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068624</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pectis</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">tenella</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 99. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pectis;species tenella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 2–15 cm;</text>
      <biological_entity id="o20163" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>herbage spicy-scented (lemon-scented in some Mexican populations).</text>
      <biological_entity id="o20164" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" name="odor" src="d0_s1" value="spicy-scented" value_original="spicy-scented" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 10–40 mm, bases of distal blades not notably expanded.</text>
      <biological_entity id="o20165" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20166" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not notably" name="size" src="d0_s2" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20167" name="blade" name_original="blades" src="d0_s2" type="structure" />
      <relation from="o20166" id="r1376" name="part_of" negation="false" src="d0_s2" to="o20167" />
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 5–15 mm.</text>
      <biological_entity id="o20168" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries linear, 3–5 mm, widest near middles, each with 1 subterminal oil-gland 0.2–0.5 mm plus smaller, submarginal oil-glands.</text>
      <biological_entity id="o20169" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character constraint="near middles" constraintid="o20170" is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character is_modifier="false" name="size" src="d0_s4" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o20170" name="middle" name_original="middles" src="d0_s4" type="structure" />
      <biological_entity constraint="subterminal" id="o20171" name="gland-oil" name_original="oil-gland" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="distance" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="submarginal" id="o20172" name="oil-gland" name_original="oil-glands" src="d0_s4" type="structure" />
      <relation from="o20169" id="r1377" name="with" negation="false" src="d0_s4" to="o20171" />
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2.5–4 mm;</text>
      <biological_entity id="o20173" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi of 0–7 awns or bristles 1–3 mm and/or coroniform.</text>
      <biological_entity id="o20175" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <biological_entity id="o20176" name="bristle" name_original="bristles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o20174" id="r1378" name="consist_of" negation="false" src="d0_s6" to="o20175" />
      <relation from="o20174" id="r1379" name="consist_of" negation="false" src="d0_s6" to="o20176" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 24.</text>
      <biological_entity id="o20174" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20177" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deserts, grasslands, arid woodlands, shrublands, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deserts" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="arid woodlands" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1100(–2500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Aguascalientes, Chihuahua, Coahuila, Durango, Nuevo León, San Luis Potosí, Tamaulipas, Veracruz, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Aguascalientes)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Veracruz)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10c.</number>
  <other_name type="common_name">Mexican chinchweed</other_name>
  <other_name type="common_name">low pectis</other_name>
  <discussion>Variety tenella is widely distributed in the Chihuahuan Desert and adjacent regions. The isolated occurrence in northern New Mexico perhaps represents an introduction.</discussion>
  
</bio:treatment>