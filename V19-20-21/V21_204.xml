<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
    <other_info_on_meta type="illustration_page">83</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="A. Gray ex Nuttall" date="1840" rank="genus">engelmannia</taxon_name>
    <taxon_name authority="(Rafinesque) Goodman &amp; C. A. Lawson" date="1992" rank="species">peristenia</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>94: 381. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus engelmannia;species peristenia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416482</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silphium</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">peristenium</taxon_name>
    <place_of_publication>
      <publication_title>Atlantic J.</publication_title>
      <place_in_publication>1: 146. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Silphium;species peristenium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Engelmannia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">pinnatifida</taxon_name>
    <taxon_hierarchy>genus Engelmannia;species pinnatifida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves (5–) 10–30 cm.</text>
      <biological_entity constraint="basal" id="o25868" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves gradually smaller distally, primary lobes ovate to narrowly linear, their margins sometimes again lobed or toothed.</text>
      <biological_entity constraint="cauline" id="o25869" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s1" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="primary" id="o25870" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="narrowly linear" />
      </biological_entity>
      <biological_entity id="o25871" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 6–10 mm.</text>
      <biological_entity id="o25872" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary tips hirsute-ciliate.</text>
      <biological_entity constraint="phyllary" id="o25873" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="hirsute-ciliate" value_original="hirsute-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray corollas 10–16 mm.</text>
      <biological_entity constraint="ray" id="o25874" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas 3.5–5 mm.</text>
      <biological_entity constraint="disc" id="o25875" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 3–4 mm;</text>
      <biological_entity id="o25876" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi 1–2 mm. 2n = 18.</text>
      <biological_entity id="o25877" name="pappus" name_original="pappi" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25878" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, pinyon-juniper woodlands, desert scrub, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Ark., Colo., Kans., Mo., Nebr., N.Mex., Okla., S.Dak., Tex.; Mexico (Chihuahua, Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Engelmann daisy</other_name>
  <other_name type="common_name">cutleaf daisy</other_name>
  
</bio:treatment>