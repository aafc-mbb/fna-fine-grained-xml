<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Willdenow" date="1807" rank="genus">grindelia</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="species">scabra</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 120, plate 332, figs. 1, 2. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus grindelia;species scabra</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066819</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, 20–70 cm.</text>
      <biological_entity id="o2053" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually reddish, ± villous to hirtellous.</text>
      <biological_entity id="o2056" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="less villous" name="pubescence" src="d0_s1" to="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaf-blades ovoid-oblong or spatulate to oblanceolate or lanceolate, 20–35 (–85) mm, lengths 1.5–2.5 times widths, bases ± clasping, margins serrate to denticulate (teeth apiculate to setose), apices obtuse to acute, faces usually puberulous to scabridulous and little, or not at all, gland-dotted, sometimes glabrous.</text>
      <biological_entity constraint="cauline" id="o2057" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="oblanceolate or lanceolate" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s2" to="85" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="distance" src="d0_s2" to="35" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="1.5-2.5" value_original="1.5-2.5" />
      </biological_entity>
      <biological_entity id="o2058" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o2059" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s2" to="denticulate" />
      </biological_entity>
      <biological_entity id="o2060" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o2061" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="puberulous" value_original="puberulous" />
        <character is_modifier="false" name="relief" src="d0_s2" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="at all" name="coloration" src="d0_s2" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in open, corymbiform arrays or borne singly.</text>
      <biological_entity id="o2062" name="head" name_original="heads" src="d0_s3" type="structure">
        <character constraint="in open , corymbiform arrays or borne" is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± hemispheric, 7–11 × (10–) 15–20 mm (often subtended by leaflike bracts).</text>
      <biological_entity id="o2063" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s4" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries in 4–6 series, spreading to appressed, filiform or linear to lanceolate or oblanceolate, apices filiform to subulate, slightly recurved to nearly straight or incurved (usually scabridulous), scarcely to moderately resinous.</text>
      <biological_entity id="o2064" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s5" to="appressed" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate or oblanceolate" />
      </biological_entity>
      <biological_entity id="o2065" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o2066" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s5" to="subulate" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="scarcely to moderately" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o2064" id="r195" name="in" negation="false" src="d0_s5" to="o2065" />
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 17–30;</text>
      <biological_entity id="o2067" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="17" name="quantity" src="d0_s6" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae (usually more orange than yellow) 12–16 mm.</text>
      <biological_entity id="o2068" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae brownish, 2.5–3 mm, apices smooth or minutely coronate, faces smooth or striate;</text>
      <biological_entity id="o2069" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2070" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s8" value="coronate" value_original="coronate" />
      </biological_entity>
      <biological_entity id="o2071" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of 2–4 straight, usually barbellulate to barbellate, sometimes smooth bristles 3–6 mm, slightly shorter than to ± equaling disc corollas.</text>
      <biological_entity id="o2072" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character constraint="than to more or less equaling disc corollas" constraintid="o2074" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s9" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o2073" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="true" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="usually barbellulate" is_modifier="true" name="architecture" src="d0_s9" to="barbellate" />
        <character is_modifier="true" modifier="sometimes" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="disc" id="o2074" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o2072" id="r196" name="consist_of" negation="false" src="d0_s9" to="o2073" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky slopes, mesas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="mesas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  
</bio:treatment>