<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">parryi</taxon_name>
    <taxon_name authority="(H. M. Hall &amp; Clements) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">latior</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 89. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species parryi;variety latior</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068302</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">parryi</taxon_name>
    <taxon_name authority="H. M. Hall &amp; Clements" date="unknown" rank="subspecies">latior</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>326: 199. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species parryi;subspecies latior;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–100 cm.</text>
      <biological_entity id="o29278" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves crowded, green;</text>
      <biological_entity id="o29279" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 1–3-nerved, oblanceolate, 45–70 × 5–14 mm, faces glabrous or puberulent, gland-dotted;</text>
      <biological_entity id="o29280" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="1-3-nerved" value_original="1-3-nerved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29281" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distalmost shorter than arrays.</text>
      <biological_entity constraint="distalmost" id="o29282" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character constraint="than arrays" constraintid="" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 10+ in cymiform arrays.</text>
      <biological_entity id="o29283" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o29284" from="10" name="quantity" src="d0_s4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o29284" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 12–14 mm.</text>
      <biological_entity id="o29285" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 11–15, chartaceous, apices erect or ± recurved, attenuate (mostly glabrous).</text>
      <biological_entity id="o29286" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s6" to="15" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o29287" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 5–7;</text>
      <biological_entity id="o29288" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas ca. 10 mm, tubes mostly glabrous, throats ± abruptly dilated, lobes about 2.5 mm.</text>
      <biological_entity id="o29289" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o29290" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29291" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less abruptly" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o29292" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open pine and fir forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open pine and fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27f.</number>
  <other_name type="common_name">Broadleaf rabbitbrush</other_name>
  
</bio:treatment>