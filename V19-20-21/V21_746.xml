<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="treatment_page">306</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Molina" date="1782" rank="genus">madia</taxon_name>
    <taxon_name authority="Greene" date="1882" rank="species">citriodora</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>9: 63. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus madia;species citriodora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067138</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–70 cm, self-compatible (heads not showy).</text>
      <biological_entity id="o18982" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="self-compatible" value_original="self-compatible" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally villous to hirsute, distally glandular-pubescent, glands purple, lateral branches often surpassing main-stems.</text>
      <biological_entity id="o18983" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="proximally villous" name="pubescence" src="d0_s1" to="hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o18984" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s1" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18985" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o18986" name="main-stem" name_original="main-stems" src="d0_s1" type="structure" />
      <relation from="o18985" id="r1292" modifier="often" name="surpassing" negation="false" src="d0_s1" to="o18986" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear, 2–9 cm × 1–10 mm.</text>
      <biological_entity id="o18987" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in open, corymbiform arrays.</text>
      <biological_entity id="o18988" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o18989" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o18988" id="r1293" name="in" negation="false" src="d0_s3" to="o18989" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± ovoid to hemispheric, 6–8 mm.</text>
      <biological_entity id="o18990" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="less ovoid" name="shape" src="d0_s4" to="hemispheric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± villous or hirsute, glandular-pubescent as well (often sparsely), glands purple, apices usually ± erect, flat.</text>
      <biological_entity id="o18991" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o18992" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="coloration_or_density" src="d0_s5" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o18993" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae mostly persistent, mostly connate 1/2+ their lengths.</text>
      <biological_entity id="o18994" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="mostly" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/2" name="lengths" src="d0_s6" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 5–12;</text>
      <biological_entity id="o18995" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas greenish yellow, laminae 4–11 mm.</text>
      <biological_entity id="o18996" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity id="o18997" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 8–50+, functionally staminate;</text>
      <biological_entity id="o18998" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="50" upper_restricted="false" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 2–3 mm, pubescent;</text>
      <biological_entity id="o18999" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers ± dark purple.</text>
      <biological_entity id="o19000" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae black or brown, sometimes mottled, glossy, ± 3-angled (abaxial sides rounded, adaxial sides 2-faced, angles between those faces ca. 70°), beakless (or nearly so).</text>
      <biological_entity constraint="ray" id="o19001" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="mottled" value_original="mottled" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="beakless" value_original="beakless" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc cypselae 0.2n = 16.</text>
      <biological_entity constraint="disc" id="o19002" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19003" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in woodlands, forests, and shrublands, disturbed sites, stream banks, often in dry, stony or clayey soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in woodlands , forests , and shrublands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="stony" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Madia citriodora occurs in northern California, northwestern Nevada, Oregon, and Washington, sometimes with (and often confused with) M. gracilis.</discussion>
  
</bio:treatment>