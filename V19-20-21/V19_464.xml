<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
    <other_info_on_meta type="illustration_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 192. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species californica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220008056</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–45 cm.</text>
      <biological_entity id="o2302" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 (scapiform), erect or arcuate-ascending, mostly glabrous (shaggily piloso-hirsute proximally and at bases of heads).</text>
      <biological_entity id="o2303" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arcuate-ascending" value_original="arcuate-ascending" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal oblanceolate to linear, pinnately lobed (lobes usually linear to filiform, sometimes broader), not fleshy, ultimate margins entire or dentate, faces usually shaggily piloso-hirsute (at least proximally), glabrescent;</text>
      <biological_entity constraint="cauline" id="o2304" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o2305" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="linear" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o2306" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o2307" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually shaggily" name="pubescence" src="d0_s2" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal usually 0.</text>
      <biological_entity constraint="cauline" id="o2308" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o2309" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 12–20, lanceolate to linear bractlets, hyaline margins 0.05–0.2 mm, abaxial faces shaggily piloso-hirsute to arachnose.</text>
      <biological_entity id="o2310" name="calyculus" name_original="calyculi" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" modifier="of 12-20" name="shape" src="d0_s4" to="linear" />
      </biological_entity>
      <biological_entity id="o2311" name="bractlet" name_original="bractlets" src="d0_s4" type="structure" />
      <biological_entity id="o2312" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2313" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="shaggily piloso-hirsute" name="pubescence" src="d0_s4" to="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres (8–) 10–15 × 5–6 mm.</text>
      <biological_entity id="o2314" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries usually 20–26+ in 2-3+ series, (midstripes often reddish) lanceolate to lance-linear or subulate, unequal, hyaline margins 0.1–0.5 mm wide, abaxial faces (of outermost, at least) shaggily piloso-hirsute to arachnose (at least proximally).</text>
      <biological_entity id="o2315" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o2316" from="20" name="quantity" src="d0_s6" to="26" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s6" to="lance-linear or subulate" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o2316" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2317" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2318" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="shaggily piloso-hirsute" name="pubescence" src="d0_s6" to="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles sparsely bristly or glabrous.</text>
      <biological_entity id="o2319" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 40–250;</text>
      <biological_entity id="o2320" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s8" to="250" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas usually yellow to pale-yellow (often with abaxial reddish stripes), sometimes white, 16–20 mm;</text>
      <biological_entity id="o2321" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="usually yellow" name="coloration" src="d0_s9" to="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 11–13 mm.</text>
      <biological_entity constraint="outer" id="o2322" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± prismatic, 2–3.4 mm, ribs extending to apices, 5 more prominent than others;</text>
      <biological_entity id="o2323" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="prismatic" value_original="prismatic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2324" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character constraint="than others" constraintid="o2326" is_modifier="false" name="prominence" src="d0_s11" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o2325" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <biological_entity id="o2326" name="other" name_original="others" src="d0_s11" type="structure" />
      <relation from="o2324" id="r222" name="extending to" negation="false" src="d0_s11" to="o2325" />
    </statement>
    <statement id="d0_s12">
      <text>persistent pappi of 12–15+, irregular, lance-deltate teeth plus (1–) 2 bristles.</text>
      <biological_entity id="o2327" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o2328" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s12" to="15" upper_restricted="false" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s12" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="shape" src="d0_s12" value="lance-deltate" value_original="lance-deltate" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s12" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2329" name="bristle" name_original="bristles" src="d0_s12" type="structure" />
      <relation from="o2327" id="r223" name="consist_of" negation="false" src="d0_s12" to="o2328" />
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 3-porate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o2330" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="3-porate" value_original="3-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2331" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy soil in grasslands, oak woodlands, chaparral, or desert margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" modifier="open" constraint="in grasslands , oak woodlands , chaparral , or desert margins" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="desert margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">California desertdandelion</other_name>
  <discussion>Malacothrix californica grows in the San Joaquin Valley, central western California, southwestern California, and the Mojave Desert.</discussion>
  
</bio:treatment>