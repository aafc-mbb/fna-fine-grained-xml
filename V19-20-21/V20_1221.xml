<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="treatment_page">553</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Greene" date="1888" rank="species">astephanus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 174. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species astephanus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067474</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–100 cm (caudices unknown).</text>
      <biological_entity id="o24030" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage unevenly floccose-lanate or tomentose, glabrescent.</text>
      <biological_entity id="o24031" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="unevenly" name="pubescence" src="d0_s1" value="floccose-lanate" value_original="floccose-lanate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems single.</text>
      <biological_entity id="o24032" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves progressively reduced distally;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o24033" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades elliptic-ovate to lanceolate, 10–20 (–30?) × 3–7+ cm, bases tapered, margins dentate to denticulate (teeth cartilaginous; mid leaves similar, sessile, smaller; distal leaves bractlike).</text>
      <biological_entity id="o24034" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s5" to="7" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o24035" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o24036" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s5" to="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 10–20+ in loose or congested, cymiform arrays.</text>
      <biological_entity id="o24037" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o24038" from="10" name="quantity" src="d0_s6" to="20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o24038" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 5–8+ lance-linear to filiform bractlets (lengths 1/3–7/8 phyllaries).</text>
      <biological_entity id="o24039" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o24040" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="8" upper_restricted="false" />
        <character char_type="range_value" from="lance-linear" is_modifier="true" name="shape" src="d0_s7" to="filiform" />
      </biological_entity>
      <relation from="o24039" id="r2217" name="consist_of" negation="false" src="d0_s7" to="o24040" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries ± 21, 7–8 mm, tips greenish.</text>
      <biological_entity id="o24041" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="21" value_original="21" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24042" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0.</text>
      <biological_entity id="o24043" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae glabrous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 40.</text>
      <biological_entity id="o24044" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24045" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep, rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="steep" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Senecio astephanus is collected infrequently.</discussion>
  
</bio:treatment>