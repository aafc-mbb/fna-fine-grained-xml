<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">172</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">heliomeris</taxon_name>
    <taxon_name authority="Nuttall" date="1847" rank="species">multiflora</taxon_name>
    <taxon_name authority="(Greene ex Wooton &amp; Standley) W. F. Yates" date="1979" rank="variety">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Indiana Acad. Sci.</publication_title>
      <place_in_publication>88: 368. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus heliomeris;species multiflora;variety brevifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068453</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnolomia</taxon_name>
    <taxon_name authority="Greene ex Wooton &amp; Standley" date="unknown" rank="species">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 190. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gymnolomia;species brevifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viguiera</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="species">ovalis</taxon_name>
    <taxon_hierarchy>genus Viguiera;species ovalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves mostly alternate;</text>
      <biological_entity id="o20153" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s0" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blades elliptic to ovate, 8–28 mm wide, margins usually flat or obscurely revolute, apices obtuse (often mucronate).</text>
      <biological_entity id="o20154" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s1" to="ovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s1" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20155" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>2n = 16.</text>
      <biological_entity id="o20156" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20157" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Canyons, woods, often in shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="canyons" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="shade" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2400–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="2400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4c.</number>
  
</bio:treatment>