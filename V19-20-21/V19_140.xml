<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="illustration_page">145</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(A. Gray) B. L. Robinson" date="1911" rank="species">eatonii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>13: 240. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species eatonii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066368</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cnicus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">eatonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 56. 1883 (as eatoni),</place_in_publication>
      <other_info_on_pub>based on Cirsium eriocephalum A. Gray [not Wallroth] var. leiocephalum D. C. Eaton in S. Watson, Botany (Fortieth Parallel), 196. 1871</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Cnicus;species eatonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–150 cm;</text>
      <biological_entity id="o10093" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taprooted caudices.</text>
      <biological_entity id="o10094" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, (fleshy), erect or ascending, simple to sparingly branched in distal 1/2, sometimes openly branched, glabrous to villous or tomentose with septate trichomes, sometimes ± glabrate;</text>
      <biological_entity id="o10095" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" constraint="in distal 1/2" constraintid="o10096" from="simple" name="architecture" src="d0_s2" to="sparingly branched" />
        <character is_modifier="false" modifier="sometimes openly" name="architecture" notes="" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" constraint="with trichomes" constraintid="o10097" from="glabrous" name="pubescence" src="d0_s2" to="villous or tomentose" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" notes="" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10096" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o10097" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches on distal stems 0–many, ascending.</text>
      <biological_entity id="o10098" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10099" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="many" />
      </biological_entity>
      <relation from="o10098" id="r932" name="on" negation="false" src="d0_s3" to="o10099" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades oblong, 10–30 × 1–5 cm, margins usually strongly undulate, unlobed and spiny-dentate or shallowly to deeply pinnatifid with 10–20 pairs of lobes, teeth or lobes closely spaced, often overlapping, lance-oblong to broadly triangular, deeply 3-lobed, ± spiny-dentate, main spines 2–12 mm, abaxial faces glabrous or villous with septate trichomes along midveins to densely arachnoid-tomentose, adaxial glabrous or villous with septate trichomes along midveins;</text>
      <biological_entity id="o10100" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10101" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10102" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually strongly" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" constraint="with pairs" constraintid="o10103" from="spiny-dentate or" name="shape" src="d0_s4" to="shallowly deeply pinnatifid" />
        <character is_modifier="false" modifier="often" name="arrangement" notes="" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="lance-oblong" name="shape" src="d0_s4" to="broadly triangular" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="spiny-dentate" value_original="spiny-dentate" />
      </biological_entity>
      <biological_entity id="o10103" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s4" to="20" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s4" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o10104" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o10105" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity id="o10106" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity constraint="main" id="o10107" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10108" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="with trichomes" constraintid="o10109" is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o10109" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o10110" name="midvein" name_original="midveins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o10111" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="densely" name="pubescence" src="d0_s4" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="with trichomes" constraintid="o10112" is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o10112" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o10113" name="midvein" name_original="midveins" src="d0_s4" type="structure" />
      <relation from="o10103" id="r933" name="part_of" negation="false" src="d0_s4" to="o10104" />
      <relation from="o10103" id="r934" name="part_of" negation="false" src="d0_s4" to="o10105" />
      <relation from="o10103" id="r935" name="part_of" negation="false" src="d0_s4" to="o10106" />
      <relation from="o10109" id="r936" name="along" negation="false" src="d0_s4" to="o10110" />
      <relation from="o10110" id="r937" name="to" negation="false" src="d0_s4" to="o10111" />
      <relation from="o10112" id="r938" name="along" negation="false" src="d0_s4" to="o10113" />
    </statement>
    <statement id="d0_s5">
      <text>basal often present at flowering, spiny winged-petiolate or sessile;</text>
      <biological_entity id="o10114" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o10115" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline many, well distributed, proximally ± winged-petiolate, distally sessile, gradually reduced;</text>
      <biological_entity id="o10116" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o10117" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="many" value_original="many" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="proximally more or less" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal not much reduced, often closely subtending heads.</text>
      <biological_entity id="o10118" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o10119" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not much" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10120" name="head" name_original="heads" src="d0_s7" type="structure" />
      <relation from="o10119" id="r939" name="often closely subtending" negation="false" src="d0_s7" to="o10120" />
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–many, erect or nodding, closely subtended by spiny-fringed bracts, usually sessile or short-pedunculate and crowded in subcapitate, spiciform, or racemiform (less commonly in openly branched) arrays.</text>
      <biological_entity id="o10121" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s8" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
        <character is_modifier="false" modifier="closely; usually" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="short-pedunculate" value_original="short-pedunculate" />
        <character constraint="in arrays" constraintid="o10123" is_modifier="false" name="arrangement" src="d0_s8" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o10122" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="spiny-fringed" value_original="spiny-fringed" />
      </biological_entity>
      <biological_entity id="o10123" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="subcapitate" value_original="subcapitate" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o10121" id="r940" modifier="closely" name="subtended by" negation="false" src="d0_s8" to="o10122" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–14+ cm.</text>
      <biological_entity id="o10124" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="14" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres green or suffused with dark purple, broadly ovoid to campanulate, 2–5 × 1.5–5 cm (appearing wider when pressed), loosely to densely villous or tomentose with septate trichomes and/or arachnoid-tomentose with finer, non-septate trichomes.</text>
      <biological_entity id="o10125" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="suffused with dark purple" value_original="suffused with dark purple" />
        <character char_type="range_value" from="broadly ovoid" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="loosely to densely; densely" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
        <character constraint="with trichomes" constraintid="o10126" is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o10126" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="septate" value_original="septate" />
        <character constraint="with trichomes" constraintid="o10127" is_modifier="false" name="pubescence" src="d0_s10" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
      </biological_entity>
      <biological_entity id="o10127" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="finer" value_original="finer" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="non-septate" value_original="non-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 4–5 series, subequal, bases short-appressed, abaxial faces without or with very narrow glutinous ridge, apices usually stiffly ascending to spreading, linear-acicular, tapering to spines 7–35 mm;</text>
      <biological_entity id="o10128" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o10129" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity id="o10130" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="short-appressed" value_original="short-appressed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10131" name="face" name_original="faces" src="d0_s11" type="structure" />
      <biological_entity id="o10132" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="very" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <biological_entity id="o10133" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="usually stiffly ascending" name="orientation" src="d0_s11" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s11" value="linear-acicular" value_original="linear-acicular" />
        <character constraint="to spines" constraintid="o10134" is_modifier="false" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o10134" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="35" to_unit="mm" />
      </biological_entity>
      <relation from="o10128" id="r941" name="in" negation="false" src="d0_s11" to="o10129" />
      <relation from="o10131" id="r942" name="without or with" negation="false" src="d0_s11" to="o10132" />
    </statement>
    <statement id="d0_s12">
      <text>outer usually pinnately spiny, sometimes entire;</text>
      <biological_entity constraint="outer" id="o10135" name="phyllary" name_original="phyllaries" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually pinnately" name="architecture_or_shape" src="d0_s12" value="spiny" value_original="spiny" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner straight, plane or spine-tipped.</text>
      <biological_entity id="o10136" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="phyllary" constraint_original="phyllary; phyllary">
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s13" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s13" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
      <biological_entity constraint="inner" id="o10137" name="phyllary" name_original="phyllaries" src="d0_s13" type="structure" />
      <relation from="o10136" id="r943" name="part_of" negation="false" src="d0_s13" to="o10137" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas ochroleucous or yellow to lavender, pink, or purple, 15–35 mm, tubes 3.5–10 mm, throats 5–14 mm, lobes (linear), 4–12.5 mm;</text>
      <biological_entity id="o10138" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="lavender pink or purple" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="lavender pink or purple" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="lavender pink or purple" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10139" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10140" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10141" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="12.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 3–6 mm, conspicuously exserted beyond corolla lobes.</text>
      <biological_entity constraint="style" id="o10142" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o10143" name="lobe" name_original="lobes" src="d0_s15" type="structure" />
      <relation from="o10142" id="r944" modifier="conspicuously" name="exserted beyond" negation="false" src="d0_s15" to="o10143" />
    </statement>
    <statement id="d0_s16">
      <text>Cypselae dark-brown, 5.5–7 mm, apical collars stramineous or not differentiated;</text>
      <biological_entity id="o10144" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o10145" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="not" name="variability" src="d0_s16" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 12–25 mm.</text>
      <biological_entity id="o10146" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s17" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wyo.; Rocky Mountains and high peaks of Great Basin desert region.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Rocky Mountains and high peaks of Great Basin desert region" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51.</number>
  <other_name type="past_name">eatoni</other_name>
  <other_name type="common_name">Mountaintop or Eaton’s thistle</other_name>
  <discussion>Varieties 7 (7 in the flora).</discussion>
  <discussion>Cirsium eatonii is a polymorphic species widely distributed in a high elevation archipelago across the central Rocky Mountains and the Intermountain Region. During Pleistocene glacial episodes, the progenitors of this species complex undoubtedly occupied lower elevation sites and likely had more contiguous populations. Post-glacial isolation of these populations in allopatric high elevation sites has allowed them to differentiate to a greater or lesser extent. Prehistoric or recent introgressive hybridization with other thistle species probably has contributed to the diversification of the complex (R. J. Moore and C. Frankton 1965). Several of the races recognized here as varieties have been treated in the past as species (e.g., C. clokeyi, C. peckii). Their current geographic isolation and more or less distinctive features might support such recognition, but application of this approach across the complex would result in a proliferation of microspecies.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres densely tomentose, individual phyllaries ± obscured by pubescence</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres glabrous or thinly tomentose, individual phyllaries evident</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Arrays nodding; corollas yellow or pink to pale purple</description>
      <determination>51d Cirsium eatonii var. eriocephalum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Arrays erect; corollas white or pink to deep purple</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla throats 8–11.5 mm; se Oregon, nw Nevada</description>
      <determination>51g Cirsium eatonii var. peckii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla throats 3.5–8 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants 50–150 cm, strictly erect; corollas pink or pale to deep purple; s Colorado</description>
      <determination>51e Cirsium eatonii var. hesperium</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants 10–65 cm, ascending to erect; corollas white to pink or lavender; Idaho and Montana to Utah and n Colorado</description>
      <determination>51f Cirsium eatonii var. murdockii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas usually 25–35 mm</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas usually 17–25 mm</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Longest pappus bristles 16–18 mm</description>
      <determination>51b Cirsium eatonii var. clokeyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Longest pappus bristles 20–25 mm</description>
      <determination>51c Cirsium eatonii var. viperinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Most outer phyllaries with lateral spines; Nevada and Utah</description>
      <determination>51a Cirsium eatonii var. eatonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Most outer phyllaries without lateral spines; nw Nevada and se Oregon</description>
      <determination>51g Cirsium eatonii var. peckii</determination>
    </key_statement>
  </key>
</bio:treatment>