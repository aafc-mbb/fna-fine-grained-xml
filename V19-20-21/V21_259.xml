<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="illustration_page">109</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="(Linnaeus) Walter" date="1788" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>213. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417426</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Siegesbeckia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 900. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Siegesbeckia;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 80–200+ cm (perennating bases ± erect, internodes winged).</text>
      <biological_entity id="o16794" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly opposite (distal sometimes alternate);</text>
      <biological_entity id="o16795" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ± ovate or lance-deltate to lanceolate, 6–12 (–16+) × 3–6 (–10+) cm, bases ± cuneate, margins coarsely toothed to subentire, apices usually acute to attenuate, sometimes rounded, faces scabrellous.</text>
      <biological_entity id="o16796" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="less ovate or lance-deltate" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="16" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="10" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16797" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o16798" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="coarsely toothed" name="shape" src="d0_s2" to="subentire" />
      </biological_entity>
      <biological_entity id="o16799" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually acute" name="shape" src="d0_s2" to="attenuate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o16800" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 20–100+ in corymbiform-paniculiform arrays.</text>
      <biological_entity id="o16801" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o16802" from="20" name="quantity" src="d0_s3" to="100" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16802" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform-paniculiform" value_original="corymbiform-paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres campanulate to turbinate, 3–5+ mm diam.</text>
      <biological_entity id="o16803" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s4" to="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s4" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 8–12+ in 2 series, ± erect, spatulate to oblanceolate, 3–7+ mm.</text>
      <biological_entity id="o16804" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o16805" from="8" name="quantity" src="d0_s5" to="12" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16805" name="series" name_original="series" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets (0–) 1–3 (–5);</text>
      <biological_entity id="o16806" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="1" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 10–15 (–20+) mm.</text>
      <biological_entity id="o16807" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="20" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 8–15+;</text>
      <biological_entity id="o16808" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow.</text>
      <biological_entity id="o16809" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae dark-brown to black, oblanceolate, 5 mm, faces strigose;</text>
      <biological_entity id="o16810" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s10" to="black" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o16811" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 3–4 mm. 2n = 34.</text>
      <biological_entity id="o16812" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16813" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bottomlands, margins of thickets, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bottomlands" />
        <character name="habitat" value="margins" constraint="of thickets , waste places" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., D.C., Fla., Ga., Ill., Ky., Md., Miss., Mo., N.C., Ohio, Okla., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  
</bio:treatment>