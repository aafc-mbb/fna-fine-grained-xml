<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="treatment_page">19</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">XANTHIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 987. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 424. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus XANTHIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek xanthos, yellow, evidently alluding to an ancient name for a plant that produced a yellow dye</other_info_on_name>
    <other_info_on_name type="fna_id">135017</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (coarse), 10–200+ cm.</text>
      <biological_entity id="o2839" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched.</text>
      <biological_entity id="o2840" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate (proximal 2–6 sometimes opposite);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o2841" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate, linear, ovate, rounded-deltate, or suborbiculate, often ± palmately or pinnately lobed, ultimate margins entire or ± toothed, faces hirtellous or ± strigose, usually glanddotted as well.</text>
      <biological_entity id="o2842" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-deltate" value_original="rounded-deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-deltate" value_original="rounded-deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="often more or less; more or less palmately; palmately; pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o2843" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o2844" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, either pistillate (proximal) or functionally staminate (distal), in racemiform to spiciform arrays or borne singly (in axils).</text>
      <biological_entity id="o2845" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="either" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="racemiform" name="architecture" src="d0_s6" to="spiciform" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate heads: involucres ± ellipsoid, 2–5+ mm diam. at anthesis (6–20+ mm diam. at maturity);</text>
      <biological_entity id="o2846" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2847" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" constraint="at anthesis" from="2" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>phyllaries 30–75+ in 6–12+ series, outer 5–8 distinct, the rest (sometimes interpreted as paleae) proximally connate, their distinct tips mostly ± hooked (the distal 1–3 usually longer, stouter, and not hooked), the whole becoming a hard, prickly perigynium (a bur);</text>
      <biological_entity id="o2848" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2849" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o2850" from="30" name="quantity" src="d0_s8" to="75" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2850" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="12" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2851" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o2852" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="mostly more or less" name="shape" src="d0_s8" value="hooked" value_original="hooked" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s8" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o2853" name="perigynium" name_original="perigynium" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="prickly" value_original="prickly" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>florets 2, corollas 0.</text>
      <biological_entity id="o2854" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2855" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2856" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate heads: involucres ± saucer-shaped, 3–5 mm diam.;</text>
      <biological_entity id="o2857" name="head" name_original="heads" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2858" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="saucer--shaped" value_original="saucer--shaped" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>phyllaries 6–16+ in 1–2+ series, distinct to bases;</text>
      <biological_entity id="o2859" name="head" name_original="heads" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2860" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o2861" from="6" name="quantity" src="d0_s11" to="16" upper_restricted="false" />
        <character constraint="to bases" constraintid="o2862" is_modifier="false" name="fusion" notes="" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o2861" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="2" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2862" name="base" name_original="bases" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>receptacles conic to columnar;</text>
      <biological_entity id="o2863" name="head" name_original="heads" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2864" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s12" to="columnar" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleae spatulate to cuneiform or linear, membranous, distally ± villous or hirtellous;</text>
      <biological_entity id="o2865" name="head" name_original="heads" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2866" name="palea" name_original="paleae" src="d0_s13" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s13" to="cuneiform or linear" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="distally more or less" name="pubescence" src="d0_s13" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>florets 20–50+, corollas whitish, ± funnelform, lobes 5, erect or reflexed (filaments connate, anthers distinct or weakly coherent).</text>
      <biological_entity id="o2867" name="head" name_original="heads" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2868" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="50" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2869" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o2870" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (black) ± fusiform, enclosed in obovoid to ellipsoid, hard, prickly, 2-chambered burs;</text>
      <biological_entity id="o2871" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity id="o2872" name="bur" name_original="burs" src="d0_s15" type="structure">
        <character char_type="range_value" from="obovoid" is_modifier="true" name="shape" src="d0_s15" to="ellipsoid" />
        <character is_modifier="true" name="texture" src="d0_s15" value="hard" value_original="hard" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="prickly" value_original="prickly" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="2-chambered" value_original="2-chambered" />
      </biological_entity>
      <relation from="o2871" id="r215" name="enclosed in" negation="false" src="d0_s15" to="o2872" />
    </statement>
    <statement id="d0_s16">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 18.</text>
      <biological_entity id="o2873" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o2874" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>New World, introduced nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="New World" establishment_means="introduced" />
        <character name="distribution" value="nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>245.</number>
  <other_name type="common_name">Lampourde</other_name>
  <discussion>Species 2–3 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Nodal spines 0; leaf blades suborbiculate to ± pentagonal or deltate</description>
      <determination>1 Xanthium strumarium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Nodal spines (1–)3-lobed, 15–30+ mm; leaf blades ± lanceolate to ovate or lance-linear</description>
      <determination>2 Xanthium spinosum</determination>
    </key_statement>
  </key>
</bio:treatment>