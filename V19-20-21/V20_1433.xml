<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">635</other_info_on_meta>
    <other_info_on_meta type="illustration_page">633</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">tussilago</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">farfara</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 865. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus tussilago;species farfara</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200024610</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves: blades palmately 5–12-lobed or angled, mostly 5–20+ × 5–20+ cm, margins irregularly denticulate.</text>
      <biological_entity constraint="basal" id="o29213" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o29214" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s0" value="5-12-lobed" value_original="5-12-lobed" />
        <character is_modifier="false" name="shape" src="d0_s0" value="angled" value_original="angled" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o29215" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly; irregularly" name="shape" src="d0_s0" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves mostly 5–25 mm.</text>
      <biological_entity constraint="cauline" id="o29216" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Calyculi: bractlets 5–15 mm.</text>
      <biological_entity id="o29217" name="calyculus" name_original="calyculi" src="d0_s2" type="structure" />
      <biological_entity id="o29218" name="bractlet" name_original="bractlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries mostly 7–15 mm.</text>
      <biological_entity id="o29219" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray corollas: laminae (2–) 4–10 mm.</text>
      <biological_entity constraint="ray" id="o29220" name="corolla" name_original="corollas" src="d0_s4" type="structure" />
      <biological_entity id="o29221" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas 10–12 mm.</text>
      <biological_entity constraint="disc" id="o29222" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 3–4 mm;</text>
      <biological_entity id="o29223" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi 8–12 mm, ± surpassing involucres.</text>
      <biological_entity id="o29225" name="involucre" name_original="involucres" src="d0_s7" type="structure" />
      <relation from="o29224" id="r2697" modifier="more or less" name="surpassing" negation="false" src="d0_s7" to="o29225" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 60.</text>
      <biological_entity id="o29224" name="pappus" name_original="pappi" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29226" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, sandy or rocky soils, calcareous sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="calcareous sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Saint-Pierre and Miquelon; B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Conn., Ill., Ind., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Tenn., Vt., Va., Wash., W.Va., Wis.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Saint-Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Coltsfoot</other_name>
  <discussion>Flowering heads of Tussilago farfara close at night (laminae of ray corollas arch and roll inward). The species is becoming an invasive weed in some areas.</discussion>
  
</bio:treatment>