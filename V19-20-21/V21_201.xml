<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">berlandiera</taxon_name>
    <taxon_name authority="(Hooker) Small" date="1903" rank="species">×betonicifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1246, 1340. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus berlandiera;species ×betonicifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068087</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silphium</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">betonicifolium</taxon_name>
    <place_of_publication>
      <publication_title>Compan. Bot. Mag.</publication_title>
      <place_in_publication>1: 99. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Silphium;species betonicifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Berlandiera</taxon_name>
    <taxon_name authority="(Michaux) Nuttall" date="unknown" rank="species">pumila</taxon_name>
    <taxon_name authority="G. L. Nesom &amp; B. L. Turner" date="unknown" rank="variety">scabrella</taxon_name>
    <taxon_hierarchy>genus Berlandiera;species pumila;variety scabrella;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Berlandiera</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">texana</taxon_name>
    <taxon_name authority="(Hooker) Torrey &amp; A. Gray" date="unknown" rank="variety">betonicifolia</taxon_name>
    <taxon_hierarchy>genus Berlandiera;species texana;variety betonicifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 110 cm.</text>
      <biological_entity id="o24745" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (erect, flexible, sometimes suffrutescent) usually branched.</text>
      <biological_entity id="o24746" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves evenly distributed along stems;</text>
      <biological_entity id="o24748" name="stem" name_original="stems" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o24747" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="along stems" constraintid="o24748" is_modifier="false" modifier="evenly" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (at least at mid-stem) narrowly to broadly ovate (widths 1/2–11/4 lengths), membranous, margins serrate to crenate or doubly crenate, faces finely hirsute to hispid (at least adaxial).</text>
      <biological_entity id="o24749" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o24750" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s4" to="crenate or doubly crenate" />
      </biological_entity>
      <biological_entity id="o24751" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="finely hirsute" name="pubescence" src="d0_s4" to="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in paniculiform to corymbiform arrays.</text>
      <biological_entity id="o24752" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o24753" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s5" to="corymbiform" />
      </biological_entity>
      <relation from="o24752" id="r1686" name="in" negation="false" src="d0_s5" to="o24753" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles hairy (hairs spreading to erect, relatively fine, curled).</text>
      <biological_entity id="o24754" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 12–18 mm diam.</text>
      <biological_entity id="o24755" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray corollas deep yellow to orange-yellow, abaxial veins green, laminae 9–18 × 5.4–9 mm.</text>
      <biological_entity constraint="ray" id="o24756" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="deep" value_original="deep" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="orange-yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24757" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o24758" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="18" to_unit="mm" />
        <character char_type="range_value" from="5.4" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas red to maroon.</text>
      <biological_entity constraint="disc" id="o24759" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s9" to="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (obovate) 2.5–4 × 4–5.5 mm. 2n = 30.</text>
      <biological_entity id="o24760" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24761" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy-loamy soils, edges of or in woodlands of oak, pine, and/or hickory</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy-loamy soils" />
        <character name="habitat" value="edges" constraint="of or in woodlands of oak" />
        <character name="habitat" value="woodlands" constraint="of oak" />
        <character name="habitat" value="oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>In most characters, Berlandiera ×betonicifolia is intermediate between its parents (B. pumila and B. texana) in varying degrees and combinations. Drummond’s type (K) of Silphium betonicifolium resembles B. texana, to which it was assigned as a variety by J. Torrey and A. Gray (1838–1843), who commented that other specimens fitted neither varietal description completely. Artificially produced F1 offspring between extreme forms of the parents resemble closely Drummond’s type collection. Character states of S. betonicifolium that Hooker described in contradistinction to those of B. texana include longer petioles, less woody stems, and more ovate, hirsute leaves of membranous texture; and from B. pumila by the coarser and more deeply crenate margins, and peduncles with “beautiful jointed purplish hairs.” Artificially produced crosses between F1 hybrids and B. pumila resemble most of the field-collected B. ×betonicifolia specimens [called B. pumila (green form) by G. L. Nesom and B. L. Turner (1998)] because gene flow of B. ×betonicifolia is over a much greater geographic region of overlap with B. pumila than with B. texana.</discussion>
  
</bio:treatment>