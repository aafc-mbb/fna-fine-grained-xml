<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">fendleri</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 104. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species fendleri</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067146</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 3–15 (–25+) cm.</text>
      <biological_entity id="o16047" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (1–) 3–8, ± decumbent or spreading-ascending, branched proximally and distally, glaucous or glabrous.</text>
      <biological_entity id="o16048" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="8" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal elliptic to oblong-oblanceolate, sometimes pinnately lobed (lobes 2–4+ pairs, oblong to triangular, unequal, apices acute), not fleshy, ultimate margins usually dentate, faces glabrous;</text>
      <biological_entity constraint="cauline" id="o16049" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o16050" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="oblong-oblanceolate" />
        <character is_modifier="false" modifier="sometimes pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16051" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o16052" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal reduced (narrowly triangular to linear or filiform, margins dentate or entire).</text>
      <biological_entity constraint="cauline" id="o16053" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o16054" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 5–12, ovate to lanceolate bractlets, hyaline margins 0.05–0.2 mm wide.</text>
      <biological_entity id="o16055" name="calyculus" name_original="calyculi" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" modifier="of 5-12" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o16056" name="bractlet" name_original="bractlets" src="d0_s4" type="structure" />
      <biological_entity id="o16057" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate, 7–10 × 5–6+ mm.</text>
      <biological_entity id="o16058" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 13–25+ in 2–3 series, lance-oblong or lanceolate to lance-linear, subequal, hyaline margins 0.05–0.3 mm wide, faces glabrous.</text>
      <biological_entity id="o16059" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o16060" from="13" name="quantity" src="d0_s6" to="25" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s6" to="lance-linear" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o16060" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o16061" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s6" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16062" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles not bristly.</text>
      <biological_entity id="o16063" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 16–88;</text>
      <biological_entity id="o16064" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s8" to="88" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow (usually with red or purplish abaxial stripes), 6–14 mm;</text>
      <biological_entity id="o16065" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 5–8 mm.</text>
      <biological_entity constraint="outer" id="o16066" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± cylindric, 1.8–2.4 mm (distal 0.3 mm slightly expanded, cupped, smooth), ribs not extending to apices, ± equal;</text>
      <biological_entity id="o16067" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16068" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o16069" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <relation from="o16068" id="r1465" name="extending to" negation="false" src="d0_s11" to="o16069" />
    </statement>
    <statement id="d0_s12">
      <text>persistent pappi of 12–15, ± deltate teeth (often hidden within cups at apices of cypselae) plus 1–2 bristles.</text>
      <biological_entity id="o16070" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o16071" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s12" to="15" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="2" />
      </biological_entity>
      <biological_entity id="o16072" name="bristle" name_original="bristles" src="d0_s12" type="structure" />
      <relation from="o16070" id="r1466" name="consist_of" negation="false" src="d0_s12" to="o16071" />
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 3-porate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o16073" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="3-porate" value_original="3-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16074" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, pinyon-juniper woodlands, creosote bush associations</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="bush associations" modifier="creosote" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>80–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="80" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Fendler’s desertdandelion</other_name>
  <discussion>Malacothrix fendleri grows in the Sonoran Desert. “San Bernardino Co.” as locality for a specimen from the herbarium of J. G. Lemmon in UC (336493) is evidently an error.</discussion>
  
</bio:treatment>