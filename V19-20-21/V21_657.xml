<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="treatment_page">268</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">layia</taxon_name>
    <taxon_name authority="D. D. Keck" date="1958" rank="species">septentrionalis</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>4: 106. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus layia;species septentrionalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067071</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 6–35 cm (self-incompatible);</text>
    </statement>
    <statement id="d0_s1">
      <text>glandular, not strongly scented.</text>
      <biological_entity id="o25142" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="not strongly" name="odor" src="d0_s1" value="scented" value_original="scented" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems purple-streaked or not.</text>
      <biological_entity id="o25143" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple-streaked" value_original="purple-streaked" />
        <character name="coloration" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades oblanceolate or lanceolate to linear, 4–70 mm, margins (basal leaves) toothed to pinnatifid.</text>
      <biological_entity id="o25144" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s3" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25145" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="toothed" name="shape" src="d0_s3" to="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± ellipsoid to campanulate, 5–12 × 3–12+ mm.</text>
      <biological_entity id="o25146" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="less ellipsoid" name="shape" src="d0_s4" to="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 5–9, apices usually shorter than folded bases.</text>
      <biological_entity id="o25147" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o25148" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character constraint="than folded bases" constraintid="o25149" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o25149" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series between ray and disc-florets.</text>
      <biological_entity id="o25150" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity constraint="between ray and disc-floret" constraintid="o25152-o25153" id="o25151" name="series" name_original="series" src="d0_s6" type="structure" constraint_original="between  ray and  disc-floret, ">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25152" name="ray" name_original="ray" src="d0_s6" type="structure" />
      <biological_entity id="o25153" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure" />
      <relation from="o25150" id="r1708" name="in" negation="false" src="d0_s6" to="o25151" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 5–9;</text>
      <biological_entity id="o25154" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae yellow, 4–15 mm.</text>
      <biological_entity id="o25155" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 10–65+, corollas 5–8 mm;</text>
      <biological_entity id="o25156" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="65" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o25157" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow to brownish.</text>
      <biological_entity id="o25158" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray cypselae glabrous or sparsely hairy.</text>
      <biological_entity constraint="ray" id="o25159" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc pappi of 16–22 white, ± equal bristles or setiform scales 4–7 mm, each proximally plumose and adaxially woolly.</text>
      <biological_entity id="o25161" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s12" to="22" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s12" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o25162" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <relation from="o25160" id="r1709" name="consist_of" negation="false" src="d0_s12" to="o25161" />
      <relation from="o25160" id="r1710" name="consist_of" negation="false" src="d0_s12" to="o25162" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity constraint="disc" id="o25160" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" notes="" src="d0_s12" value="plumose" value_original="plumose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s12" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25163" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, openings in chaparral, woodlands, on serpentine or sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="openings" constraint="in chaparral" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="serpentine" modifier="woodlands on" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Layia septentrionalis occurs in the central and southern Inner North Coast Ranges and the Sutter Buttes (southern Sacramento Valley).</discussion>
  
</bio:treatment>