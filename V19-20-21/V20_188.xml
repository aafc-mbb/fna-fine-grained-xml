<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">96</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">bigelowia</taxon_name>
    <taxon_name authority="(Michaux) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">nudata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">nudata</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus bigelowia;species nudata;variety nudata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068091</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle" date="unknown" rank="species">virgata</taxon_name>
    <taxon_hierarchy>genus Bigelowia;species virgata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves narrowly oblanceolate, (2–) 3–8 (–10) × (4–) 5–14 mm.</text>
      <biological_entity constraint="basal" id="o9979" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s0" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s0" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s0" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s0" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres 4.5–6 mm.</text>
      <biological_entity id="o9980" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Florets 3–4 mm. 2n = 18.</text>
      <biological_entity id="o9981" name="floret" name_original="florets" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9982" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sandy loam or sandy peat, savannas, pine flatwoods, swamp margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist sandy loam" />
        <character name="habitat" value="sandy peat" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="margins" modifier="swamp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  
</bio:treatment>