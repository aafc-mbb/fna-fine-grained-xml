<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="treatment_page">553</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">megacephalus</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 410. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species megacephalus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067494</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (15–) 30–50 cm (rhizomes woody, suberect or creeping).</text>
      <biological_entity id="o21051" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage loosely arachno-tomentose, unevenly glabrescent.</text>
      <biological_entity id="o21052" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="arachno-tomentose" value_original="arachno-tomentose" />
        <character is_modifier="false" modifier="unevenly" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems single or clustered.</text>
      <biological_entity id="o21053" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves progressively reduced distally;</text>
    </statement>
    <statement id="d0_s4">
      <text>weakly petiolate;</text>
      <biological_entity id="o21054" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate to narrowly oblanceolate, 10–18+ × 1–2 (–3+) cm, bases tapered, margins entire or wavy (often with dark, cartilaginous denticles; mid leaves similar, smaller, sessile; distal leaves bractlike).</text>
      <biological_entity id="o21055" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="narrowly oblanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="3" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21056" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o21057" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads usually 1 (sometimes subtended by 1–2 smaller heads).</text>
      <biological_entity id="o21058" name="head" name_original="heads" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 5–8+ linear to filiform bractlets (lengths 1/2–7/8+ phyllaries).</text>
      <biological_entity id="o21059" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o21060" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="8" upper_restricted="false" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s7" to="filiform" />
      </biological_entity>
      <relation from="o21059" id="r1950" name="consist_of" negation="false" src="d0_s7" to="o21060" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries ± 21, 9–12 (–14) mm, tips not notably blackened (short-hairy).</text>
      <biological_entity id="o21061" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="21" value_original="21" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21062" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not notably" name="coloration" src="d0_s8" value="blackened" value_original="blackened" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets ± 13;</text>
      <biological_entity id="o21063" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae (5–) 15–20 mm.</text>
      <biological_entity constraint="corolla" id="o21064" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 40.</text>
      <biological_entity id="o21065" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21066" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, moist or drying sites, especially on mountain slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="rocky moist or drying" />
        <character name="habitat" value="mountain slopes" modifier="especially" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  
</bio:treatment>