<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">473</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="treatment_page">492</other_info_on_meta>
    <other_info_on_meta type="illustration_page">491</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rafinesque) G. L. Nesom" date="1995" rank="subgenus">virgulus</taxon_name>
    <taxon_name authority="(Linnaeus) G. L. Nesom" date="1995" rank="species">concolor</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 278. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus virgulus;species concolor</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067633</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">concolor</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 874. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species concolor;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lasallea</taxon_name>
    <taxon_name authority="(Linnaeus) Semple &amp; Brouillet" date="unknown" rank="species">concolor</taxon_name>
    <taxon_hierarchy>genus Lasallea;species concolor;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Virgulus</taxon_name>
    <taxon_name authority="(Linnaeus) Reveal &amp; Keener" date="unknown" rank="species">concolor</taxon_name>
    <taxon_hierarchy>genus Virgulus;species concolor;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–80 cm, cespitose;</text>
    </statement>
    <statement id="d0_s1">
      <text>with cormoid, woody caudices.</text>
      <biological_entity id="o27348" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o27349" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="cormoid" value_original="cormoid" />
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o27348" id="r2528" name="with" negation="false" src="d0_s1" to="o27349" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, ascending to erect (light to dark-brown), glabrous or densely canescent distally.</text>
      <biological_entity id="o27350" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (green to graysih) soft to firm;</text>
      <biological_entity id="o27351" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="soft" name="texture" src="d0_s3" to="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering, sessile, blades (1–3-nerved) elliptic-lanceolate, 10–43 × 5–15 mm, bases attenuate, margins usually entire, rarely remotely serrate, piloso-ciliate, apices acute to obtuse, faces silvery silky-pilose to sparsely pilose;</text>
      <biological_entity constraint="basal" id="o27352" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, margins, apices, faces" constraintid="o27353, o27354, o27355, o27356, o27357" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
        <character char_type="range_value" from="silky-pilose" name="pubescence" src="d0_s4" to="sparsely pilose" />
      </biological_entity>
      <biological_entity id="o27353" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="43" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o27354" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="43" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o27355" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="43" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o27356" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="43" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o27357" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="43" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline withering by flowering, sessile, blades oblanceolate, 20–35 × 5–15 mm, bases rounded, subclasping, margins entire, scabrous to silky-pilose, apices acute to obtuse, cuspidate mucronate, faces ± densely silky;</text>
      <biological_entity constraint="proximal cauline" id="o27358" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades, bases, margins, apices, faces" constraintid="o27359, o27360, o27361, o27362, o27363" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o27359" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-pilose" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s5" value="silky" value_original="silky" />
      </biological_entity>
      <biological_entity id="o27360" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-pilose" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s5" value="silky" value_original="silky" />
      </biological_entity>
      <biological_entity id="o27361" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-pilose" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s5" value="silky" value_original="silky" />
      </biological_entity>
      <biological_entity id="o27362" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-pilose" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s5" value="silky" value_original="silky" />
      </biological_entity>
      <biological_entity id="o27363" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-pilose" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s5" value="silky" value_original="silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades usually oblong to lanceolate, rarely ovate, 9–15 × 1.8–5 mm, reduced distally, bases cuneate, margins entire, apices acute, mucronate, faces ± densely silky or sparsely strigose, sometimes glabrate (var. devestitum).</text>
      <biological_entity constraint="distal" id="o27364" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o27365" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually oblong" name="shape" src="d0_s6" to="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o27366" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o27367" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o27368" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o27369" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s6" value="silky" value_original="silky" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads in narrow, paniculiform (virgate) arrays (1–3 (–5) per branch).</text>
      <biological_entity id="o27370" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per branch" constraintid="o27371" from="1" modifier="in narrow , paniculiform arrays" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o27371" name="branch" name_original="branch" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles densely hairy, bracts becoming linear, grading into phyllaries.</text>
      <biological_entity id="o27372" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o27373" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="becoming" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o27374" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o27373" id="r2529" name="into" negation="false" src="d0_s8" to="o27374" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate to narrowly campanulate, 5–7 mm.</text>
      <biological_entity id="o27375" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s9" to="narrowly campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–5 series, appressed, lanceolate-oblong, strongly unequal, bases (tan) ± indurate, margins scarious proximally, green distally, green zones restricted to apex, obscured by hair, apices usually acute, sometimes acuminate, usually mucronate, sometimes subspinulose, faces ± densely silky.</text>
      <biological_entity id="o27376" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o27377" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o27378" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o27379" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="proximally" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s10" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o27380" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character constraint="by hair" constraintid="o27382" is_modifier="false" name="prominence" src="d0_s10" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o27381" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o27382" name="hair" name_original="hair" src="d0_s10" type="structure" />
      <biological_entity id="o27383" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s10" value="subspinulose" value_original="subspinulose" />
      </biological_entity>
      <biological_entity id="o27384" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s10" value="silky" value_original="silky" />
      </biological_entity>
      <relation from="o27376" id="r2530" name="in" negation="false" src="d0_s10" to="o27377" />
      <relation from="o27380" id="r2531" name="restricted to" negation="false" src="d0_s10" to="o27381" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 8–12;</text>
      <biological_entity id="o27385" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas rose-purple, rarely white, laminae 4–9 × 1–1.5 mm.</text>
      <biological_entity id="o27386" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="rose-purple" value_original="rose-purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o27387" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (9–) 11–17 (–21);</text>
      <biological_entity id="o27388" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s13" to="11" to_inclusive="false" />
        <character char_type="range_value" from="17" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="21" />
        <character char_type="range_value" from="11" name="quantity" src="d0_s13" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas pink turning purple, 4.5–6 mm, tubes shorter than narrowly funnelform throats (thinly puberulent), lobes triangular, 0.4–0.7 mm.</text>
      <biological_entity id="o27389" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink turning purple" value_original="pink turning purple" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27390" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than narrowly funnelform throats" constraintid="o27391" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o27391" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o27392" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae obovoid, not compressed, 2.5–3.5 mm, 7–10-nerved, faces densely strigose;</text>
      <biological_entity id="o27393" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="7-10-nerved" value_original="7-10-nerved" />
      </biological_entity>
      <biological_entity id="o27394" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi tan, (3.5–) 4–6 mm.</text>
      <biological_entity id="o27395" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="tan" value_original="tan" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., Fla., Ga., Ky., La., Mass., Md., Miss., N.C., N.J., N.Y., R.I., S.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Eastern silvery aster</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal cauline leaves and phyllaries moderately to densely silky; e United States</description>
      <determination>18a Symphyotrichum concolor var. concolor</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal cauline leaves and phyllaries glabrous or sparsely hispido-pilose; panhandle Florida</description>
      <determination>18b Symphyotrichum concolor var. devestitum</determination>
    </key_statement>
  </key>
</bio:treatment>