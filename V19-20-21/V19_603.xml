<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
    <other_info_on_meta type="illustration_page">375</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="genus">phalacroseris</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 364. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus phalacroseris;species bolanderi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220010255</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phalacroseris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bolanderi</taxon_name>
    <taxon_name authority="H. M. Hall" date="unknown" rank="variety">coronata</taxon_name>
    <taxon_hierarchy>genus Phalacroseris;species bolanderi;variety coronata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 6–20 cm, faces glabrous.</text>
      <biological_entity id="o19120" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19121" name="face" name_original="faces" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres 7–13 mm after flowering.</text>
      <biological_entity id="o19122" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="after flowering" from="7" from_unit="mm" name="some_measurement" src="d0_s1" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries not reflexed in fruit, margins often red distally, glabrous or minutely puberulent apically.</text>
      <biological_entity id="o19123" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character constraint="in fruit" constraintid="o19124" is_modifier="false" modifier="not" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o19124" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
      <biological_entity id="o19125" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often; distally" name="coloration" src="d0_s2" value="red" value_original="red" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely; apically" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Florets 10–18 mm, much surpassing phyllaries at flowering, glabrous.</text>
      <biological_entity id="o19126" name="floret" name_original="florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="18" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19127" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure" />
      <relation from="o19126" id="r1724" modifier="much" name="surpassing" negation="false" src="d0_s3" to="o19127" />
    </statement>
    <statement id="d0_s4">
      <text>Cypselae straight or slightly curved on one side, 3–4 mm, obtusely 3-angled at 1 adaxial and 2 lateral nerves.</text>
      <biological_entity id="o19129" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o19130" name="adaxial" name_original="adaxial" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19131" name="nerve" name_original="nerves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>2n = 18.</text>
      <biological_entity id="o19128" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character constraint="on side" constraintid="o19129" is_modifier="false" modifier="slightly" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="4" to_unit="mm" />
        <character constraint="at adaxial" constraintid="o19130" is_modifier="false" modifier="obtusely" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19132" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows and sphagnum bogs, coniferous upper montane forests and mixed subalpine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="sphagnum bogs" />
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="upper montane forests" />
        <character name="habitat" value="mixed subalpine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Bolander dandelion</other_name>
  <discussion>A vestigial coroniform pappus found in some individuals is too minor a feature to merit varietal designation.</discussion>
  
</bio:treatment>