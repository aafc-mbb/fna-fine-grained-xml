<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">278</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="Fries" date="1862" rank="species">horridum</taxon_name>
    <place_of_publication>
      <publication_title>Uppsala Univ. Årsskr.</publication_title>
      <place_in_publication>1862: 154. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species horridum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066944</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–25 (–40+) cm.</text>
      <biological_entity id="o14974" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally piloso-hirsute (hairs 2–4+ mm), distally piloso-hirsute (hairs 1–3+ mm) and stellate-pubescent.</text>
      <biological_entity id="o14975" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 3–6+, cauline 3–6+;</text>
      <biological_entity id="o14976" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o14977" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="6" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o14978" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="6" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblong to spatulate or oblanceolate, (15–) 25–75 (–150+) × 6–12 (–30+) mm, lengths 3–6+ times widths, bases cuneate, margins usually entire, sometimes denticulate, apices obtuse to acute, faces piloso-hirsute (to lanate, hairs 2–4 mm).</text>
      <biological_entity id="o14979" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14980" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="spatulate or oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s3" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="150" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="75" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3-6+" value_original="3-6+" />
      </biological_entity>
      <biological_entity id="o14981" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o14982" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o14983" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o14984" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (3–) 8–25 (–100+) in corymbiform to paniculiform arrays.</text>
      <biological_entity id="o14985" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="8" to_inclusive="false" />
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="100" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o14986" from="8" name="quantity" src="d0_s4" to="25" />
      </biological_entity>
      <biological_entity id="o14986" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s4" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles usually stellate-pubescent, sometimes piloso-hirsute as well (hairs 1–2 mm).</text>
      <biological_entity id="o14987" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="sometimes; well" name="pubescence" src="d0_s5" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 5–8+.</text>
      <biological_entity id="o14988" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o14989" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± cylindric to campanulate, 6–9 (× 3–4) mm.</text>
      <biological_entity id="o14990" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="less cylindric" name="shape" src="d0_s7" to="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="9[" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="4]" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–15+, apices acute to acuminate, abaxial faces usually densely piloso-hirsute (hairs 1–2+ mm), sometimes stellate-pubescent or glabrous.</text>
      <biological_entity id="o14991" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14992" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14993" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s8" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 6–12 (–15+);</text>
      <biological_entity id="o14994" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="15" upper_restricted="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 8–9 mm.</text>
      <biological_entity id="o14995" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae (tan to redbrown) columnar, 3–3.5 mm;</text>
      <biological_entity id="o14996" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of ca. 60+, stramineous bristles in ± 2 series, 4–6 mm.</text>
      <biological_entity id="o14997" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14998" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" is_modifier="true" name="quantity" src="d0_s12" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="stramineous" value_original="stramineous" />
        <character modifier="more or less" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14999" name="series" name_original="series" src="d0_s12" type="structure" />
      <relation from="o14997" id="r1362" name="consist_of" negation="false" src="d0_s12" to="o14998" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Boulders, gravels, meadows, pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="boulders" />
        <character name="habitat" value="gravels" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  
</bio:treatment>