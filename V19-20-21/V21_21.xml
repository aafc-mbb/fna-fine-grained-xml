<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="treatment_page">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ambrosia</taxon_name>
    <taxon_name authority="(A. Nelson) Shinners" date="1949" rank="species">grayi</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>17: 174. 1949</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus ambrosia;species grayi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066052</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaertneria</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">grayi</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>34: 35. 1902,</place_in_publication>
      <other_info_on_pub>based on Franseria tomentosa A. Gray, Mem. Amer. Acad. Arts, n. s. 4: 80. 1849, not Ambrosia tomentosa Nuttall 1818</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Gaertneria;species grayi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–30+ cm.</text>
      <biological_entity id="o20473" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o20474" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly alternate;</text>
      <biological_entity id="o20475" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 10–45+ mm;</text>
      <biological_entity id="o20476" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="45" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades elliptic to ovate, 45–60 (–100+) × 30–45 (–75+) mm, usually 1–2-pinnately lobed (lobes ± lanceolate), bases cuneate, ultimate margins serrate, abaxial faces densely sericeous or strigillose (silver or grayish), adaxial faces strigillose.</text>
      <biological_entity id="o20477" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="100" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="75" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="45" to_unit="mm" />
        <character is_modifier="false" modifier="usually 1-2-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o20478" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o20479" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20480" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20481" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate heads clustered, proximal to staminates;</text>
      <biological_entity id="o20482" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
        <character constraint="to staminates" constraintid="o20483" is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o20483" name="staminate" name_original="staminates" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>florets (1–) 2.</text>
      <biological_entity id="o20484" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate heads: peduncles 1–2 mm;</text>
      <biological_entity id="o20485" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o20486" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucres saucer-shaped, 3.5–5+ mm diam., strigillose;</text>
      <biological_entity id="o20487" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o20488" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="saucer--shaped" value_original="saucer--shaped" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s8" to="5" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>florets 8–25 (–40+).</text>
      <biological_entity id="o20489" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o20490" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="40" upper_restricted="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Burs: bodies ± pyriform to globose, 3–5+ mm, stipitate-glandular, spines 8–12+, scattered, stoutly conic, 0.5–1+ mm, tips straight.</text>
      <biological_entity id="o20492" name="body" name_original="bodies" src="d0_s10" type="structure">
        <character char_type="range_value" from="less pyriform" name="shape" src="d0_s10" to="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20493" name="spine" name_original="spines" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="12" upper_restricted="false" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="scattered" value_original="scattered" />
        <character is_modifier="false" modifier="stoutly" name="shape" src="d0_s10" value="conic" value_original="conic" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 36.</text>
      <biological_entity id="o20494" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20495" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or seasonally wet flats, depressions, alkaline, clay soils, prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet flats" modifier="wet or seasonally" />
        <character name="habitat" value="depressions" />
        <character name="habitat" value="alkaline" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Nebr., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  
</bio:treatment>