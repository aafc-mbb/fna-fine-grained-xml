<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="treatment_page">147</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(G. Don) G. L. Nesom" date="1993" rank="subsection">Venosae</taxon_name>
    <taxon_name authority="unknown" date="1830" rank="series">venosae</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">fistulosa</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8., Solidago no. 19. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection venosae;series venosae;species fistulosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067544</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Miller) Kuntze" date="unknown" rank="species">fistulosus</taxon_name>
    <taxon_hierarchy>genus Aster;species fistulosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="A. H. Moore" date="unknown" rank="species">aspericaulis</taxon_name>
    <taxon_hierarchy>genus Solidago;species aspericaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–150 cm;</text>
      <biological_entity id="o27665" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes creeping, elongated, sparsely scaly.</text>
      <biological_entity id="o27666" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="length" src="d0_s1" value="elongated" value_original="elongated" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20+, erect (stout), conspicuously spreading-hirsute, at least distally.</text>
      <biological_entity id="o27667" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s2" value="spreading-hirsute" value_original="spreading-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline usually withering by flowering except on new shoots, tapering to broadly winged petioles, blades oblanceolate, 30–50 × 8–15 mm, rapidly increasing in size distally, margins shallowly serrate, scabroso-strigose, faces often more densely hairy than distal;</text>
      <biological_entity id="o27668" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character constraint="except " constraintid="o27669" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o27669" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o27670" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="broadly" name="architecture" src="d0_s3" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o27671" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="size" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="size" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27672" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rapidly; distally; shallowly" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabroso-strigose" value_original="scabroso-strigose" />
      </biological_entity>
      <biological_entity id="o27673" name="face" name_original="faces" src="d0_s3" type="structure">
        <character constraint="than distal blades" constraintid="o27674" is_modifier="false" name="pubescence" src="d0_s3" value="more densely hairy" value_original="more densely hairy" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27674" name="blade" name_original="blades" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>mid to distal cauline numerous, crowded, sessile, blades lanceolate-ovate to elliptic-oblong, larger ones 35–120 × 8–35 mm, much reduced distally, bases broad and ± clasping, margins obscurely serrulate or entire, faces usually moderately hirsuto-villous on midnerves, often less so abaxially, adaxial sparsely strigose or glabrous.</text>
      <biological_entity id="o27675" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o27676" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o27677" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate-ovate" name="shape" src="d0_s4" to="elliptic-oblong" />
        <character is_modifier="false" name="size" src="d0_s4" value="larger" value_original="larger" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="much; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o27678" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o27679" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o27680" name="face" name_original="faces" src="d0_s4" type="structure">
        <character constraint="on midnerves" constraintid="o27681" is_modifier="false" modifier="usually moderately" name="pubescence" src="d0_s4" value="hirsuto-villous" value_original="hirsuto-villous" />
      </biological_entity>
      <biological_entity id="o27681" name="midnerve" name_original="midnerves" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o27682" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often less; less; abaxially; sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 35–500, in paniculiform arrays, usually dense, branches recurved-secund.</text>
      <biological_entity id="o27683" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s5" to="500" />
        <character is_modifier="false" modifier="usually" name="density" notes="" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o27684" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o27685" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="recurved-secund" value_original="recurved-secund" />
      </biological_entity>
      <relation from="o27683" id="r2560" name="in" negation="false" src="d0_s5" to="o27684" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 2–8 mm, sparsely to moderately strigillose;</text>
      <biological_entity id="o27686" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s6" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles 1–3, linear to linear-lanceolate, tending to group proximal to involucres, sometimes grading into phyllaries.</text>
      <biological_entity id="o27687" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="linear-lanceolate" />
        <character constraint="to involucres" constraintid="o27689" is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o27688" name="group" name_original="group" src="d0_s7" type="structure" />
      <biological_entity id="o27689" name="involucre" name_original="involucres" src="d0_s7" type="structure" />
      <biological_entity id="o27690" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure" />
      <relation from="o27687" id="r2561" name="tending to" negation="false" src="d0_s7" to="o27688" />
      <relation from="o27687" id="r2562" modifier="sometimes" name="into" negation="false" src="d0_s7" to="o27690" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly campanulate, 3.5–5.5 mm.</text>
      <biological_entity id="o27691" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 4–5 series, unequal, glabrous;</text>
      <biological_entity id="o27692" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27693" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o27692" id="r2563" name="in" negation="false" src="d0_s9" to="o27693" />
    </statement>
    <statement id="d0_s10">
      <text>outer narrowly ovatelanceolate, mid and inner linear-lanceolate.</text>
      <biological_entity constraint="outer" id="o27694" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity constraint="mid and inner" id="o27695" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (2–) 4–10;</text>
      <biological_entity id="o27696" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 1.2–2.5 × 0.2–0.5 mm.</text>
      <biological_entity id="o27697" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (2–) 4–7;</text>
      <biological_entity id="o27698" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s13" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 4–5 mm, lobes 0.5–1 mm.</text>
      <biological_entity id="o27699" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27700" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (narrowly obconic) 1.5–1.8 mm, sparsely strigillose, sometimes only apically;</text>
      <biological_entity id="o27701" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 3–4 mm. 2n = 18.</text>
      <biological_entity id="o27702" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27703" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct(–Nov; year-round s).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" constraint=" (-Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly wetter sandy soils, seepage areas, boggy grounds, edges of marshes and thickets, open pine woodlands, roadside ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" modifier="mostly wetter" />
        <character name="habitat" value="seepage areas" />
        <character name="habitat" value="boggy grounds" />
        <character name="habitat" value="edges" constraint="of marshes and thickets , open pine woodlands , roadside ditches" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="open pine woodlands" />
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="wetter" modifier="mostly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S.; Ala., Del., Fla., Ga., La., Md., Miss., N.J., N.C., Pa., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <other_name type="common_name">Pine-barren goldenrod</other_name>
  <discussion>Solidago fistulosa grows mainly on the coastal plains. It was introduced at Stone Mountain, Georgia. Solidago pyramidata Pursh may be a synonym of S. fistulosa.</discussion>
  
</bio:treatment>