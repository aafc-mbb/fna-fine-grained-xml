<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">360</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Greene" date="1900" rank="genus">oreostemma</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Greene" date="1900" rank="species">alpigenum</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="1993" rank="variety">andersonii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>74: 312. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus oreostemma;species alpigenum;variety andersonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068605</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">andersonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 540. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species andersonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="unknown" rank="species">alpigenus</taxon_name>
    <taxon_name authority="(A. Gray) M. Peck" date="unknown" rank="variety">andersonii</taxon_name>
    <taxon_hierarchy>genus Aster;species alpigenus;variety andersonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oreostemma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alpigenum</taxon_name>
    <taxon_name authority="(A. Gray) Semple" date="unknown" rank="subspecies">andersonii</taxon_name>
    <taxon_hierarchy>genus Oreostemma;species alpigenum;subspecies andersonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (4–) 8–40 (–70) cm.</text>
      <biological_entity id="o19212" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaf-blades linear-oblanceolate to narrowly oblanceolate, (30–) 80–120 (–350) × (1–) 2–6 (–9) mm, apices acute.</text>
      <biological_entity constraint="basal" id="o19213" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s1" to="narrowly oblanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_length" src="d0_s1" to="80" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="350" to_unit="mm" />
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s1" to="120" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s1" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19214" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cypselae strigillose.</text>
    </statement>
    <statement id="d0_s3">
      <text>2n = 18, 36.</text>
      <biological_entity id="o19215" name="cypsela" name_original="cypselae" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19216" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="18" value_original="18" />
        <character name="quantity" src="d0_s3" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs (sometimes with Darlingtonia), marshes, moist to wet meadows, lake edges, pine woods, alpine tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="moist to wet meadows" />
        <character name="habitat" value="lake edges" />
        <character name="habitat" value="pine woods" />
        <character name="habitat" value="alpine tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1200–)1500–3300(–3700) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3700" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Anderson’s mountaincrown</other_name>
  
</bio:treatment>