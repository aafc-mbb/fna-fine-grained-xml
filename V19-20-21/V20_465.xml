<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">209</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaetopappa</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="1988" rank="species">imberbis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>64: 449. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chaetopappa;species imberbis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066326</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chaetopappa</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle" date="unknown" rank="species">asteroides</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">imberbis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>16: 82. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chaetopappa;species asteroides;variety imberbis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–15 cm, eglandular;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o17006" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not densely overlapping, basal and proximal cauline blades oblanceolate-spatulate to oblanceolate-obovate, 8–26 × 1–5 mm, reduced in size distally, herbaceous, bases not clasping, flat, faces hispido-pilose.</text>
      <biological_entity id="o17007" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not densely" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="basal and proximal cauline" id="o17008" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate-spatulate" name="shape" src="d0_s2" to="oblanceolate-obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="26" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o17009" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o17010" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispido-pilose" value_original="hispido-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres cylindro-turbinate, 3.5–4.5 × 2.5–5 mm.</text>
      <biological_entity id="o17011" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindro-turbinate" value_original="cylindro-turbinate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s3" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 8–19;</text>
      <biological_entity id="o17012" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" to="19" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas white.</text>
      <biological_entity id="o17013" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 11–18, bisexual.</text>
      <biological_entity id="o17014" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s6" to="18" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 1.5–1.8 mm, 8-nerved, faces long-strigose in 8 lines;</text>
      <biological_entity id="o17015" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="8-nerved" value_original="8-nerved" />
      </biological_entity>
      <biological_entity id="o17016" name="face" name_original="faces" src="d0_s7" type="structure">
        <character constraint="in lines" constraintid="o17017" is_modifier="false" name="pubescence" src="d0_s7" value="long-strigose" value_original="long-strigose" />
      </biological_entity>
      <biological_entity id="o17017" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi white-hyaline, erose-margined, asymmetric, cuplike crowns 0.2–0.5 mm, in 1 series.</text>
      <biological_entity id="o17018" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white-hyaline" value_original="white-hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="erose-margined" value_original="erose-margined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity id="o17020" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17019" id="r1578" name="in" negation="false" src="d0_s8" to="o17020" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 16.</text>
      <biological_entity id="o17019" name="crown" name_original="crowns" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="cuplike" value_original="cuplike" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17021" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas in deep, loose sand, usually in oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="open" />
        <character name="habitat" value="deep" modifier="in" />
        <character name="habitat" value="loose sand" />
        <character name="habitat" value="oak woodlands" modifier="usually in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Awnless lazy daisy</other_name>
  
</bio:treatment>