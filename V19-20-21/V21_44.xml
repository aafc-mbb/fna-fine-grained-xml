<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iva</taxon_name>
    <taxon_name authority="A. Gray" date="1876" rank="species">hayesiana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 78. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus iva;species hayesiana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067020</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 10–100 cm.</text>
      <biological_entity id="o8882" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o8883" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sprawling to erect (usually woody at bases).</text>
      <biological_entity id="o8884" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="sprawling" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 0–3 (–10) mm;</text>
      <biological_entity id="o8885" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8886" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades lance-elliptic to oblanceolate, 2–5 (–10) cm × 5–10 (–18) mm, margins usually entire, rarely toothed, faces sparsely strigose to scabrellous, glanddotted.</text>
      <biological_entity id="o8887" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o8888" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lance-elliptic" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8889" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o8890" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in ± racemiform arrays.</text>
      <biological_entity id="o8891" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o8892" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o8891" id="r615" name="in" negation="false" src="d0_s4" to="o8892" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 2–3+ mm.</text>
      <biological_entity id="o8893" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± hemispheric, 2.5–3.5 mm.</text>
      <biological_entity id="o8894" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries: outer 5 distinct, ± herbaceous.</text>
      <biological_entity id="o8895" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity constraint="outer" id="o8896" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Paleae linear to cuneiform, 1.5–2.5 mm.</text>
      <biological_entity id="o8897" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="cuneiform" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate florets 5;</text>
      <biological_entity id="o8898" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 0.5–1 mm.</text>
      <biological_entity id="o8899" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Functionally staminate florets 5–12 (–20);</text>
      <biological_entity id="o8900" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="20" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 1.5–2.5 mm.</text>
      <biological_entity id="o8901" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 2–2.5 mm.</text>
      <biological_entity id="o8902" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline flats, depressions</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline flats" />
        <character name="habitat" value="depressions" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>