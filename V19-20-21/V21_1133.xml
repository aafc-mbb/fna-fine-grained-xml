<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="treatment_page">451</other_info_on_meta>
    <other_info_on_meta type="illustration_page">452</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="genus">tetraneuris</taxon_name>
    <taxon_name authority="(Pursh) Greene" date="1898" rank="species">acaulis</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 265. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus tetraneuris;species acaulis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220013384</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">acaulis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 743. 1813 (as Galardia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gaillardia;species acaulis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenoxys</taxon_name>
    <taxon_name authority="(Pursh) K. F. Parker" date="unknown" rank="species">acaulis</taxon_name>
    <taxon_hierarchy>genus Hymenoxys;species acaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2–30+ cm.</text>
      <biological_entity id="o16147" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudices ± branched, branches notably thickened distally.</text>
      <biological_entity id="o16148" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o16149" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="notably; distally" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–35 (–60), erect, unbranched.</text>
      <biological_entity id="o16150" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="60" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="35" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves all basal, new leaves tightly clustered;</text>
      <biological_entity id="o16151" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o16152" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16153" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="new" value_original="new" />
        <character is_modifier="false" modifier="tightly" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades spatulate or oblanceolate to linear-oblanceolate, entire, glabrous or ± hairy, often lanuginose, sericeous, or strigoso-canescent, eglandular or ± glanddotted.</text>
      <biological_entity id="o16154" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="linear-oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="lanuginose" value_original="lanuginose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigoso-canescent" value_original="strigoso-canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigoso-canescent" value_original="strigoso-canescent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character name="architecture" src="d0_s4" value="more or less" value_original="more or less" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–35 (–60) per plant, borne singly.</text>
      <biological_entity id="o16155" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="60" />
        <character char_type="range_value" constraint="per plant" constraintid="o16156" from="1" name="quantity" src="d0_s5" to="35" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o16156" name="plant" name_original="plant" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 0.5–30 cm, ± hairy.</text>
      <biological_entity id="o16157" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 7–12 × 8–16 mm.</text>
      <biological_entity id="o16158" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Outer phyllaries 6–12, 3.9–9 (–11.5) mm, margins 0–0.4 mm wide, sometimes slightly scarious, abaxial faces ± hairy.</text>
      <biological_entity constraint="outer" id="o16159" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="12" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="11.5" to_unit="mm" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16160" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s8" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes slightly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16161" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 8–15 (–21);</text>
      <biological_entity id="o16162" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="21" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 9–19 mm.</text>
      <biological_entity id="o16163" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 25–200+;</text>
      <biological_entity id="o16164" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s11" to="200" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, 2.7–4.3 mm.</text>
      <biological_entity id="o16165" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s12" to="4.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 2–4 mm;</text>
      <biological_entity id="o16166" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 5–8 obovate to oblanceolate, aristate scales 2.2–3.7 mm.</text>
      <biological_entity id="o16167" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o16168" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s14" to="8" />
        <character char_type="range_value" from="obovate" is_modifier="true" name="shape" src="d0_s14" to="oblanceolate" />
        <character is_modifier="true" name="shape" src="d0_s14" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.7" to_unit="mm" />
      </biological_entity>
      <relation from="o16167" id="r1118" name="consist_of" negation="false" src="d0_s14" to="o16168" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Ariz., Calif., Colo., Idaho, Kans., Mont., N.Dak., N.Mex., Nebr., Nev., Okla., S.Dak., Tex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves sparsely to densely hairy, often lanuginose, sometimes sericeous, not strigoso-canescent; peduncles 0.5–8(–12) cm; outer phyllaries 6.8–9(–11.5) mm; growing at (2100–)3000–3900 m</description>
      <determination>9a Tetraneuris acaulis var. caespitosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves sparsely to densely hairy, often strigoso-canescent; peduncles (1–)5–20(–30) cm; outer phyllaries 4–7.5 mm; growing at 700–2900(–3500) m</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades usually densely, sometimes sparsely, strigoso-canescent; e of continental divide (except for 2 reports from nw Idaho)</description>
      <determination>9b Tetraneuris acaulis var. acaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades glabrous or sparsely to moderately to sometimes densely hairy, not strigoso-canescent; w of continental divide</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades glabrous or usually sparsely to moderately or sometimes densely hairy, densely gland-dotted; n Arizona, se California, w Colorado, s Idaho, e Nevada, Utah</description>
      <determination>9c Tetraneuris acaulis var. arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades glabrous or sparsely to moderately hairy, at least some (usually most or all) eglandular or sparsely gland-dotted; w Colorado, c, ne Utah, and sw Wyoming</description>
      <determination>9d Tetraneuris acaulis var. epunctata</determination>
    </key_statement>
  </key>
</bio:treatment>