<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">484</other_info_on_meta>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
    <other_info_on_meta type="illustration_page">486</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cavanilles" date="1797" rank="genus">stevia</taxon_name>
    <taxon_name authority="Cavanilles" date="1797" rank="species">serrata</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>4: 33, plate 355. 1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus stevia;species serrata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250006457</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stevia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">serrata</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">haplopappa</taxon_name>
    <taxon_hierarchy>genus Stevia;species serrata;variety haplopappa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stevia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">serrata</taxon_name>
    <taxon_name authority="(Willdenow) B. L. Robinson" date="unknown" rank="variety">ivifolia</taxon_name>
    <taxon_hierarchy>genus Stevia;species serrata;variety ivifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–100 cm.</text>
      <biological_entity id="o12142" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly alternate (at least not regularly opposite, crowded, with axillary clusters of smaller leaves);</text>
      <biological_entity id="o12143" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles 0;</text>
      <biological_entity id="o12144" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (3-nerved) narrowly lanceolate to lance-linear, 1.5–4 cm, margins serrulate.</text>
      <biological_entity id="o12145" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s3" to="lance-linear" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12146" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne in ± congested, compact clusters.</text>
      <biological_entity id="o12147" name="head" name_original="heads" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0 or 1–4 mm, sessile-glandular, villous-puberulent.</text>
      <biological_entity id="o12148" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="1-4 mm" value_original="1-4 mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous-puberulent" value_original="villous-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–6 (–7) mm.</text>
      <biological_entity id="o12149" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries sessile-glandular, sparsely villosulous, apices acute to acuminate.</text>
      <biological_entity id="o12150" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="villosulous" value_original="villosulous" />
      </biological_entity>
      <biological_entity id="o12151" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas white or pink, lobes sparsely sessile-glandular, finely villous-hirsute.</text>
      <biological_entity id="o12152" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o12153" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s8" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s8" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi usually ± equaling corollas, sometimes coroniform or 0.2n = (22–) 34 (–54) univalents, less often 17 pairs.</text>
      <biological_entity id="o12154" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" notes="" src="d0_s9" value="coroniform" value_original="coroniform" />
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12155" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="usually more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12156" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character char_type="range_value" from="22" name="atypical_quantity" src="d0_s9" to="34" to_inclusive="false" unit=",less often  pairs" />
        <character char_type="range_value" from="34" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="54" unit=",less often  pairs" />
        <character name="quantity" src="d0_s9" unit=",less often  pairs" value="34" value_original="34" />
        <character name="quantity" src="d0_s9" unit=",less often  pairs" value="17" value_original="17" />
      </biological_entity>
      <biological_entity id="o12157" name="univalent" name_original="univalents" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o12158" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character char_type="range_value" from="22" name="atypical_quantity" src="d0_s9" to="34" to_inclusive="false" unit=",less often  pairs" />
        <character char_type="range_value" from="34" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="54" unit=",less often  pairs" />
        <character name="quantity" src="d0_s9" unit=",less often  pairs" value="34" value_original="34" />
        <character name="quantity" src="d0_s9" unit=",less often  pairs" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jul–)Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, disturbed sites, oak-grasslands, oak-pine grasslands, and oak, mixed conifer-oak, mixed pine, ponderosa pine-Douglas fir, pine-fir-aspen, spruce-Douglas fir, and fir-hemlock woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="oak-grasslands" />
        <character name="habitat" value="oak-pine grasslands" />
        <character name="habitat" value="oak" modifier="and" />
        <character name="habitat" value="mixed conifer-oak" />
        <character name="habitat" value="mixed pine" />
        <character name="habitat" value="ponderosa pine-douglas fir" />
        <character name="habitat" value="pine-fir-aspen" />
        <character name="habitat" value="spruce-douglas fir" />
        <character name="habitat" value="fir-hemlock woodlands" modifier="and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Saw-tooth candyleaf</other_name>
  <discussion>Some collections of Stevia serrata from Cochise and Graham counties, Arizona, were annotated by J. L. Grashoff as “S. serrata &gt; plummerae”; in leaf arrangement and morphology (venation, margin, and shape), they appear to be similar to typical S. serrata from the same area.</discussion>
  
</bio:treatment>