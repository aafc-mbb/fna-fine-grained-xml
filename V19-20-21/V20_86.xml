<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="treatment_page">57</other_info_on_meta>
    <other_info_on_meta type="illustration_page">49</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) McClatchie" date="1894" rank="species">cuneata</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>2: 124. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species cuneata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066516</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">cuneatus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 635. 1873 (as Aplopappus)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species cuneatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–100 cm.</text>
      <biological_entity id="o8626" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to ascending or erect, green when young, highly branched, glabrous, gland-dotted (sometimes in pits), resinous.</text>
      <biological_entity id="o8627" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="ascending or erect" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="highly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending-spreading;</text>
      <biological_entity id="o8628" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending-spreading" value_original="ascending-spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades cuneate to spatulate, 2–25 × 2–16 mm, midnerves evident or faint, (margins usually flat) apices rounded, obtuse, or retuse, sometimes mucronate, faces glabrous, gland-dotted (in pits), thickly resinous;</text>
      <biological_entity id="o8629" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="spatulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8630" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="evident" value_original="evident" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity id="o8631" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="retuse" value_original="retuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o8632" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="thickly" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary leaf fascicles rarely present.</text>
      <biological_entity constraint="axillary" id="o8633" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually borne singly, sometimes in rounded, cymiform arrays (to 5 × 8 cm).</text>
      <biological_entity id="o8634" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 2–10 mm (bracts 0–10+, scalelike).</text>
      <biological_entity id="o8635" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres turbinate to narrowly campanulate, 6–12.5 × 4–14 mm.</text>
      <biological_entity id="o8636" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s7" to="narrowly campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="12.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 20–60 in 3–7 series, tan, ovate to lanceolate, 2–6 × 0.5–2.5 mm, unequal, mostly chartaceous, sometimes each with herbaceous subapical patch, midnerves evident on proximal 1/2 or throughout, subapical resin ducts 0 or slightly darker, thickened and expanded, (margins membranous, weakly lacerate) apices acute to acuminate or obtuse, abaxial faces glabrous.</text>
      <biological_entity id="o8637" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o8638" from="20" name="quantity" src="d0_s8" to="60" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o8638" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o8639" name="patch" name_original="patch" src="d0_s8" type="structure">
        <character is_modifier="true" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o8640" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character constraint="on " constraintid="o8642" is_modifier="false" name="prominence" src="d0_s8" value="evident" value_original="evident" />
        <character is_modifier="false" modifier="slightly" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="size" src="d0_s8" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8641" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <biological_entity constraint="resin" id="o8642" name="duct" name_original="ducts" src="d0_s8" type="structure" constraint_original="subapical resin" />
      <biological_entity constraint="proximal" id="o8643" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <biological_entity constraint="resin" id="o8644" name="duct" name_original="ducts" src="d0_s8" type="structure" constraint_original="subapical resin">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8645" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate or obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8646" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o8637" id="r788" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o8639" />
      <relation from="o8642" id="r789" name="on" negation="false" src="d0_s8" to="o8643" />
      <relation from="o8642" id="r790" name="on" negation="false" src="d0_s8" to="o8644" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0 (–7);</text>
      <biological_entity id="o8647" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="7" />
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae 3–4.3 × 1–1.5 mm.</text>
      <biological_entity id="o8648" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 7–70;</text>
      <biological_entity id="o8649" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas ca. 5.5 mm.</text>
      <biological_entity id="o8650" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="5.5" value_original="5.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae tan to brown, turbinate, 2.5–3 mm, sericeous to villous;</text>
      <biological_entity id="o8651" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s13" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s13" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi off-white to brown, 6.5–8 mm. 2n = 18.</text>
      <biological_entity id="o8652" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s14" to="brown" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8653" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Nev.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Variety spathulata extends into Mexico. It is reported to hybridize with Ericameria nauseosa.</discussion>
  <references>
    <reference>Urbatsch, L. E. 1976. Systematics of the Ericameria cuneata complex (Compositae, Astereae). Madroño 23: 338–345.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Disc florets 36–70 (San Diego County, California)</description>
      <determination>10b Ericameria cuneata var. macrocephala</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Disc florets 7–33</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves sessile, blades cuneate, largest 3–14(–18) × 2–9(–12)</description>
      <determination>10a Ericameria cuneata var. cuneata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves petiolate, blades spatulate, largest (9–)12–25 × 4–16</description>
      <determination>10c Ericameria cuneata var. spathulata</determination>
    </key_statement>
  </key>
</bio:treatment>