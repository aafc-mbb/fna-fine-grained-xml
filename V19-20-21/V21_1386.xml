<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">546</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Willdenow" date="1803" rank="genus">mikania</taxon_name>
    <taxon_name authority="(Linnaeus f.) Willdenow" date="1803" rank="species">cordifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>3: 1746. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus mikania;species cordifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">242428429</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cacalia</taxon_name>
    <taxon_name authority="Linnaeus f." date="unknown" rank="species">cordifolia</taxon_name>
    <place_of_publication>
      <publication_title>Suppl. Pl.,</publication_title>
      <place_in_publication>351. 1782</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cacalia;species cordifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 6-angled, gray-tomentulose or tomentose;</text>
      <biological_entity id="o6658" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="6-angled" value_original="6-angled" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="gray-tomentulose" value_original="gray-tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>internodes 5–20 cm.</text>
      <biological_entity id="o6659" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petioles 25–55 mm, densely pilose to tomentose.</text>
      <biological_entity id="o6660" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="densely pilose" name="pubescence" src="d0_s2" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades ovate to deltate, 5–10 × 3–8 cm, bases cordate, margins subentire to undulate-dentate, apices acute to acuminate, faces densely pilose to tomentose (abaxial paler than adaxial).</text>
      <biological_entity id="o6661" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="deltate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6662" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o6663" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s3" to="undulate-dentate" />
      </biological_entity>
      <biological_entity id="o6664" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Arrays of heads compound-corymbiform (terminal and lateral), 6 × 7+ cm.</text>
      <biological_entity id="o6665" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="densely pilose" name="pubescence" src="d0_s3" to="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound-corymbiform" value_original="compound-corymbiform" />
        <character name="length" src="d0_s4" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6666" name="head" name_original="heads" src="d0_s4" type="structure" />
      <relation from="o6665" id="r484" name="part_of" negation="false" src="d0_s4" to="o6666" />
    </statement>
    <statement id="d0_s5">
      <text>Heads 7–10 mm.</text>
      <biological_entity id="o6667" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries substramineous, elliptic to narrowly ovate, 6–8 mm, apices acute to slightly rounded.</text>
      <biological_entity id="o6668" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="substramineous" value_original="substramineous" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="narrowly ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6669" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="slightly rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Corollas white, 3.5–5 mm, lobes linear.</text>
      <biological_entity id="o6670" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6671" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae brown, 3–4 mm, glabrous or pubescent, sparsely glanddotted;</text>
      <biological_entity id="o6672" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of ca. 60 white, barbellate bristles 4–5 mm. 2n = 38.</text>
      <biological_entity id="o6673" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
      <biological_entity id="o6674" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="60" value_original="60" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="barbellate" value_original="barbellate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6675" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="38" value_original="38" />
      </biological_entity>
      <relation from="o6673" id="r485" name="consist_of" negation="false" src="d0_s9" to="o6674" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet areas, woodlands, calcareous soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet areas" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="calcareous soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., Tex.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Mikania cordifolia grows in all wet-tropical and subtropical America from northern Argentina to the lower Gulf Coastal Plain of the United States. It has the largest natural distribution of any species in the genus. In the tropics, M. cordifolia tends to be weedy, frequently occupying disturbed sites, usually in the lowlands. It is not weedy in the United States. In Louisiana, Mississippi, and Texas, M. cordifolia occurs in relatively open seeps and stream sides in beech (Fagus grandiflora Ehrhart) woods. It was collected in 1875 from the Navy Ballast Yard in Kargins Point, New Jersey (W. C. Holmes 1981); no further records for New Jersey are known.</discussion>
  
</bio:treatment>