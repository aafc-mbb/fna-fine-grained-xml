<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(Rydberg) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">salicifolia</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 87. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety salicifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068289</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">salicifolius</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>37: 130. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species salicifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) Britton" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="(Rydberg) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">salicifolius</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;subspecies salicifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–120 cm.</text>
      <biological_entity id="o7717" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems grayish to nearly green, leafy, loosely tomentose.</text>
      <biological_entity id="o7718" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="grayish" name="coloration" src="d0_s1" to="nearly green" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves grayish green;</text>
      <biological_entity id="o7719" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish green" value_original="grayish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades mostly 3–5-nerved, broadly linear, 40–90 × 3–6 (–10) mm, faces tomentulose.</text>
      <biological_entity id="o7720" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s3" value="3-5-nerved" value_original="3-5-nerved" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7721" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 7.5–9.5 (–10.5) mm.</text>
      <biological_entity id="o7722" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s4" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 11–17, apices erect, obtuse, outer abaxial faces glabrous or with apical tufts of hairs, inner frequently glabrous.</text>
      <biological_entity id="o7723" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s5" to="17" />
      </biological_entity>
      <biological_entity id="o7724" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="outer abaxial" id="o7725" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with apical tufts" value_original="with apical tufts" />
        <character is_modifier="false" name="position" notes="" src="d0_s5" value="inner" value_original="inner" />
        <character is_modifier="false" modifier="frequently" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="apical" id="o7726" name="tuft" name_original="tufts" src="d0_s5" type="structure" />
      <biological_entity id="o7727" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o7725" id="r703" name="with" negation="false" src="d0_s5" to="o7726" />
      <relation from="o7726" id="r704" name="part_of" negation="false" src="d0_s5" to="o7727" />
    </statement>
    <statement id="d0_s6">
      <text>Corollas 7.5–9.5 mm, tubes minutely puberulent, lobes 1–1.5 mm, glabrous;</text>
      <biological_entity id="o7728" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s6" to="9.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7729" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o7730" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages longer than stigmatic portions.</text>
      <biological_entity constraint="style" id="o7731" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic portions" constraintid="o7732" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o7732" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae densely hairy;</text>
      <biological_entity id="o7733" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 7.5–7.6 mm.</text>
      <biological_entity id="o7734" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering later summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush, pinyon-juniper, mountain brush, and aspen communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="mountain brush" />
        <character name="habitat" value="aspen communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21q.</number>
  <other_name type="common_name">Willowlike rabbitbrush</other_name>
  <discussion>Variety salicifolia is known from Cache to Sevier counties. Intermediates between var. salicifolia and var. graveolens have been observed.</discussion>
  
</bio:treatment>