<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
    <other_info_on_meta type="illustration_page">352</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">eriophyllum</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1883" rank="species">wallacei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 25. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus eriophyllum;species wallacei</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066717</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bahia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">wallacei</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 105. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bahia;species wallacei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antheropeas</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg" date="unknown" rank="species">wallacei</taxon_name>
    <taxon_hierarchy>genus Antheropeas;species wallacei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriophyllum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wallacei</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="variety">rubellum</taxon_name>
    <taxon_hierarchy>genus Eriophyllum;species wallacei;variety rubellum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–15 cm.</text>
      <biological_entity id="o8671" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading or ascending.</text>
      <biological_entity id="o8672" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading or ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades obovate to spatulate, 7–20 mm, sometimes 3-lobed, ultimate margins entire, plane (apices ± rounded), faces ± woolly.</text>
      <biological_entity id="o8673" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="spatulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="distance" src="d0_s2" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o8674" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o8675" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly.</text>
      <biological_entity id="o8676" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–3 cm.</text>
      <biological_entity id="o8677" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres broadly campanulate, 4–6 mm diam.</text>
      <biological_entity id="o8678" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 5–10, distinct.</text>
      <biological_entity id="o8679" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 5–10;</text>
      <biological_entity id="o8680" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae usually cream or yellow, sometimes white with red veins, 3–4 mm.</text>
      <biological_entity id="o8681" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character constraint="with veins" constraintid="o8682" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8682" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 20–30;</text>
      <biological_entity id="o8683" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 2–3 mm (tubes cylindric, throats funnelform, gradually dilated, lobes glandular; anther appendages subulate, not glandular).</text>
      <biological_entity id="o8684" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± 2 mm;</text>
      <biological_entity id="o8685" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi usually of 6–10 ± oblong scales 0.4–0.8 mm, rarely 0.2n = 10 + 0–1 I or 0–3 B.</text>
      <biological_entity id="o8686" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o8687" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s12" to="10" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
        <character modifier="rarely" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8688" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" unit="i,b" upper_restricted="false" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="1" unit="i,b" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="3" unit="i,b" />
      </biological_entity>
      <relation from="o8686" id="r603" name="consist_of" negation="false" src="d0_s12" to="o8687" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Dec–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly openings, creosote-bush or sagebrush scrublands, Joshua Tree or pinyon-juniper woodlands, or chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly openings" />
        <character name="habitat" value="creosote-bush" />
        <character name="habitat" value="sagebrush scrublands" />
        <character name="habitat" value="joshua tree" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Woolly easterbonnets</other_name>
  <other_name type="common_name">Wallace’s woolly daisy</other_name>
  
</bio:treatment>