<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">36</other_info_on_meta>
    <other_info_on_meta type="treatment_page">37</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1822" rank="genus">laënnecia</taxon_name>
    <taxon_name authority="filaginoides de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">filaginoides</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 376. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus laënnecia;species filaginoides</taxon_hierarchy>
    <other_info_on_name type="fna_id">250007280</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Conyza</taxon_name>
    <taxon_name authority="(de Candolle) Hieronymus" date="unknown" rank="species">filaginoides</taxon_name>
    <taxon_hierarchy>genus Conyza;species filaginoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–30+ cm.</text>
      <biological_entity id="o26340" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades: bases obscurely, if at all, clasping, faces densely to loosely tomentose (at least abaxially, sometimes glandular beneath tomentum);</text>
      <biological_entity id="o26341" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure" />
      <biological_entity id="o26342" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="at all" name="architecture_or_fixation" src="d0_s1" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o26343" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely to loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal narrowly obovate or oblanceolate to linear, 10–30 × 1–5 mm, often obscurely pinnately lobed or toothed;</text>
      <biological_entity id="o26344" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="often obscurely pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal similar, smaller, entire or distally toothed.</text>
      <biological_entity id="o26345" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in ± racemiform arrays (on main and lateral stems).</text>
      <biological_entity id="o26346" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o26347" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o26346" id="r2431" name="in" negation="false" src="d0_s4" to="o26347" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres 3.5–4.5 mm.</text>
      <biological_entity id="o26348" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacles 1.5–2.5 mm diam.</text>
      <biological_entity id="o26349" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate florets 20–40+;</text>
      <biological_entity id="o26350" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="40" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas: lengths ± 1/2 styles, laminae 0.</text>
      <biological_entity id="o26351" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o26352" name="style" name_original="styles" src="d0_s8" type="structure" />
      <biological_entity id="o26353" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 3–10.</text>
      <biological_entity id="o26354" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae purplish, 1.3–1.5 mm, densely sericeous (hairs 0.3–0.5 mm), sometimes glandular near apices;</text>
      <biological_entity id="o26355" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="sericeous" value_original="sericeous" />
        <character constraint="near apices" constraintid="o26356" is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o26356" name="apex" name_original="apices" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pappi of 30–40+ white bristles in 2 series, outer ± persistent, 0.8–1 mm, inner persistent or ± fragile, 2–3 mm. 2n = 18.</text>
      <biological_entity id="o26357" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o26358" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s11" to="40" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o26359" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o26360" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o26361" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s11" value="fragile" value_original="fragile" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26362" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
      <relation from="o26357" id="r2432" name="consist_of" negation="false" src="d0_s11" to="o26358" />
      <relation from="o26358" id="r2433" name="in" negation="false" src="d0_s11" to="o26359" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, openings in pine and oak forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="openings" constraint="in pine and oak forests" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="oak forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>