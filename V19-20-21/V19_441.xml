<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">301</other_info_on_meta>
    <other_info_on_meta type="illustration_page">301</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">rhagadiolus</taxon_name>
    <taxon_name authority="(Linnaeus) Gaertner" date="1791" rank="species">stellatus</taxon_name>
    <place_of_publication>
      <publication_title>Fruct. Sem. Pl.</publication_title>
      <place_in_publication>2: 354. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus rhagadiolus;species stellatus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067439</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lapsana</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">stellata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 811. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lapsana;species stellata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 25–100 (–150+) × 10–30 (–60+) mm.</text>
      <biological_entity id="o20934" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="150" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s0" to="100" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s0" to="60" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Phyllaries linear (ultimately each convolute about a subtended cypsela), glabrous or hispid to setose.</text>
      <biological_entity id="o20935" name="phyllary" name_original="phyllaries" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s1" value="linear" value_original="linear" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s1" to="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cypselae: outer 10–15 (–25) mm, inner 5–10 mm. 2n = 10.</text>
      <biological_entity id="o20936" name="cypsela" name_original="cypselae" src="d0_s2" type="structure" />
      <biological_entity constraint="outer" id="o20937" name="cypsela" name_original="cypselae" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20938" name="cypsela" name_original="cypselae" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20939" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>