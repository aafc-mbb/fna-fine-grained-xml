<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">eriophyllum</taxon_name>
    <taxon_name authority="Greene ex A. Gray" date="1883" rank="species">nubigenum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 25. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus eriophyllum;species nubigenum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066715</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–15 cm.</text>
      <biological_entity id="o4798" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending.</text>
      <biological_entity id="o4799" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades oblanceolate, 1–2 cm, margins entire, plane (apices acute), faces woolly.</text>
      <biological_entity id="o4800" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4801" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4802" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o4803" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly or in loose arrays.</text>
      <biological_entity id="o4804" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="in loose arrays" value_original="in loose arrays" />
      </biological_entity>
      <biological_entity id="o4805" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o4804" id="r368" name="in" negation="false" src="d0_s3" to="o4805" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles ± 1 cm.</text>
      <biological_entity id="o4806" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres cylindric, 3–4+ mm diam.</text>
      <biological_entity id="o4807" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s5" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 4–6, distinct.</text>
      <biological_entity id="o4808" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="6" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 4–6;</text>
      <biological_entity id="o4809" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae yellow, ± 1 mm (inconspicuous).</text>
      <biological_entity id="o4810" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character modifier="more or less" name="some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 10–20;</text>
      <biological_entity id="o4811" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas ± 2 mm (tubes cylindric, throats broadly funnelform, abruptly dilated, lobes not glandular; anther appendages deltate, glandular).</text>
      <biological_entity id="o4812" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2.5–3 mm;</text>
      <biological_entity id="o4813" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 8–10 lanceolate to spatulate (± unequal) scales 0.5–1.5 mm. 2n = 14.</text>
      <biological_entity id="o4814" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" modifier="of" name="quantity" src="d0_s12" to="10" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="spatulate" />
      </biological_entity>
      <biological_entity id="o4815" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4816" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly or rocky openings, yellow pine or red fir forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="rocky openings" />
        <character name="habitat" value="yellow pine" />
        <character name="habitat" value="red fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Yosemite woolly sunflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>