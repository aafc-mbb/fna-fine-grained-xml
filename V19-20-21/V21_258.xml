<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">helianthoides</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 135. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species helianthoides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417425</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–120+ cm (perennating bases ± erect or horizontal rhizomes, internodes winged).</text>
      <biological_entity id="o20792" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly alternate (proximal sometimes opposite);</text>
      <biological_entity id="o20793" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ± lanceovate to lanceolate, 5–12+ × 2–5+ cm, bases ± cuneate, margins ± toothed, apices acute to attenuate, faces strigose to sericeous.</text>
      <biological_entity id="o20794" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="less lanceovate" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o20795" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o20796" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o20797" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="attenuate" />
      </biological_entity>
      <biological_entity id="o20798" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s2" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 2–5 (–10+) in ± corymbiform arrays.</text>
      <biological_entity id="o20799" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="10" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o20800" from="2" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o20800" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± hemispheric, 10–15+ mm diam.</text>
      <biological_entity id="o20801" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s4" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 16–21+ in 2–3 series, ± erect, lanceolate, 6–9+ mm.</text>
      <biological_entity id="o20802" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o20803" from="16" name="quantity" src="d0_s5" to="21" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o20803" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 8–13+;</text>
      <biological_entity id="o20804" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 20–25 (–30+) mm.</text>
      <biological_entity id="o20805" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 40–80+;</text>
      <biological_entity id="o20806" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s8" to="80" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow.</text>
      <biological_entity id="o20807" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae dark-brown to black, oblanceolate to elliptic, 5 mm, faces strigillose to glabrate;</text>
      <biological_entity id="o20808" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s10" to="black" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s10" to="elliptic" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o20809" name="face" name_original="faces" src="d0_s10" type="structure">
        <character char_type="range_value" from="strigillose" name="pubescence" src="d0_s10" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0.5–1.5 mm. 2n = 34.</text>
      <biological_entity id="o20810" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20811" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist places in sandy, pine woodlands, post-oak woodlands, disturbed places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist places" constraint="in sandy , pine woodlands , post-oak woodlands , disturbed places" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="pine woodlands" />
        <character name="habitat" value="post-oak woodlands" />
        <character name="habitat" value="disturbed places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>70–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="70" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Ill., Ind., Iowa, Kans., Ky., La., Miss., Mo., N.C., Ohio, Okla., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Verbesina helianthoides may be no longer present in Georgia.</discussion>
  
</bio:treatment>