<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="illustration_page">278</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(H. M. Hall ex D. D. Keck) B. G. Baldwin" date="1999" rank="species">increscens</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 468. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species increscens</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066469</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">paniculata</taxon_name>
    <taxon_name authority="H. M. Hall ex D. D. Keck" date="unknown" rank="subspecies">increscens</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>3: 11. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species paniculata;subspecies increscens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="(H. M. Hall ex D. D. Keck) Tanowitz" date="unknown" rank="species">increscens</taxon_name>
    <taxon_hierarchy>genus Hemizonia;species increscens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 6–100 cm.</text>
      <biological_entity id="o17172" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid.</text>
      <biological_entity id="o17173" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades pinnatifid, faces ± hirsute or villous and sometimes stipitate-glandular.</text>
      <biological_entity id="o17174" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o17175" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o17176" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually in paniculiform arrays sometimes congested.</text>
      <biological_entity id="o17177" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o17178" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" src="d0_s3" value="congested" value_original="congested" />
      </biological_entity>
      <relation from="o17177" id="r1181" name="in" negation="false" src="d0_s3" to="o17178" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads usually overlapping proximal 0–1/2 of each involucre.</text>
      <biological_entity id="o17179" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" constraint="of involucre" constraintid="o17181" from="0" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o17180" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o17181" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o17179" id="r1182" name="subtending" negation="false" src="d0_s4" to="o17180" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries evenly stipitate-glandular, including margins and apices, usually with nonglandular, non-pustule-based hairs as well.</text>
      <biological_entity id="o17182" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o17183" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o17184" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o17185" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <relation from="o17182" id="r1183" name="including" negation="false" src="d0_s5" to="o17183" />
      <relation from="o17182" id="r1184" name="including" negation="false" src="d0_s5" to="o17184" />
      <relation from="o17182" id="r1185" modifier="usually; well" name="with" negation="false" src="d0_s5" to="o17185" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o17186" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o17187" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17186" id="r1186" name="in" negation="false" src="d0_s6" to="o17187" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 8–13 (–15);</text>
      <biological_entity id="o17188" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae pale to deep yellow, 4–8 mm.</text>
      <biological_entity id="o17189" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="deep yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 11–32, all or mostly functionally staminate;</text>
      <biological_entity id="o17190" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s9" to="32" />
        <character is_modifier="false" modifier="mostly functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers reddish to dark purple.</text>
      <biological_entity id="o17191" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s10" to="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi of 5–14 linear, lance-linear, or oblong, fimbriate to erose scales 1–2 mm.</text>
      <biological_entity id="o17192" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="of 5-14 linear" name="shape" src="d0_s11" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity id="o17193" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Extensive morphologic and ecologic variation within Deinandra increscens has been interpreted variously. Subspecies recognized here follow the treatment of B. D. Tanowitz (1982) except for return of Hoover’s subsp. foliosa to D. paniculata A. Gray and recognition of D. bacigalupii B. G. Baldwin for members of subsp. increscens in the sense of Tanowitz from the eastern San Francisco Bay Area.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually erect, sparsely to densely leafy; heads in open, paniculiform arrays</description>
      <determination>9a Deinandra increscens subsp. increscens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually decumbent, densely leafy; heads ± congested</description>
      <determination>9b Deinandra increscens subsp. villosa</determination>
    </key_statement>
  </key>
</bio:treatment>