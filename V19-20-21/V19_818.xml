<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">489</other_info_on_meta>
    <other_info_on_meta type="treatment_page">490</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">tanacetum</taxon_name>
    <taxon_name authority="(Linnaeus) Schultz-Bipontinus" date="1844" rank="species">parthenium</taxon_name>
    <place_of_publication>
      <publication_title>Tanaceteen,</publication_title>
      <place_in_publication>55. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus tanacetum;species parthenium</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242101204</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Matricaria</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">parthenium</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 890. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Matricaria;species parthenium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysanthemum</taxon_name>
    <taxon_name authority="(Linnaeus) Bernhardi" date="unknown" rank="species">parthenium</taxon_name>
    <taxon_hierarchy>genus Chrysanthemum;species parthenium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (20–) 30–60 (–80) cm.</text>
      <biological_entity id="o6912" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3+ (ridged), erect, branched (usually glabrous proximally, puberulent distally).</text>
      <biological_entity id="o6913" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mainly cauline;</text>
      <biological_entity id="o6915" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mainly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o6914" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades ovate to rounded-deltate, 4–10+ × 1.5–4 cm, usually 1–2-pinnately lobed (primary lobes 3–5+ pairs, ± ovate), ultimate margins pinnatifid to dentate, faces (at least abaxial) usually puberulent, gland-dotted.</text>
      <biological_entity id="o6916" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="rounded-deltate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="usually 1-2-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o6917" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s4" to="dentate" />
      </biological_entity>
      <biological_entity id="o6918" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 5–20 (–30) in corymbiform arrays.</text>
      <biological_entity id="o6919" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="30" />
        <character char_type="range_value" constraint="in arrays" constraintid="o6920" from="5" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o6920" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–7 mm diam.</text>
      <biological_entity id="o6921" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 10–21+ (more in “doubles”), pistillate, fertile;</text>
      <biological_entity id="o6922" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="21" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white, laminae 2–8 (–12) mm.</text>
      <biological_entity id="o6923" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o6924" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas ca. 2 mm.</text>
      <biological_entity constraint="disc" id="o6925" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae ± columnar, 1–2 mm, 5–10-ribbed;</text>
      <biological_entity id="o6926" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="5-10-ribbed" value_original="5-10-ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0 or coroniform, 0.1–0.2+ mm.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 18.</text>
      <biological_entity id="o6927" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6928" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, urban areas, roadsides, fields, abandoned plantings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="urban areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="plantings" modifier="abandoned" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Ont.; Ala., Colo., Conn., Del., Idaho, Ill., Ind., Ky., Maine, Md., Mass., Mich., Miss., Mo., Mont., Nev., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., R.I., S.C., Utah, Vt., Wash., W.Va., Wis., Wyo.; Eurasia; n Africa; widely naturalized in New World and Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="widely naturalized in New World and Old World" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Feverfew</other_name>
  <discussion>Tanacetum parthenium is widely cultivated throughout North America.</discussion>
  
</bio:treatment>