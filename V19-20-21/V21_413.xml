<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Edward E. Schilling</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">136</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="treatment_page">169</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">HELIOMERIS</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 19. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus HELIOMERIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek helios, sun, and - merus, part</other_info_on_name>
    <other_info_on_name type="fna_id">114912</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Kunth" date="unknown" rank="genus">Viguiera</taxon_name>
    <taxon_name authority="(Nuttall) S. F. Blake" date="unknown" rank="section">Heliomeris</taxon_name>
    <taxon_hierarchy>genus Viguiera;section Heliomeris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, (2–) 10–90 (–120+) cm.</text>
      <biological_entity id="o14194" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched distally or ± throughout.</text>
      <biological_entity id="o14196" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally; distally; more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o14198" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite or alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or subsessile;</text>
      <biological_entity id="o14197" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually 1 (–3) -nerved, elliptic, lance-linear, lanceolate, lanceovate, linear, ovate, rhombic, or rhombic-ovate, margins entire (often revolute), faces hispid or strigose to stigillose, sometimes glanddotted.</text>
      <biological_entity id="o14199" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="1(-3)-nerved" value_original="1(-3)-nerved" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic-ovate" value_original="rhombic-ovate" />
      </biological_entity>
      <biological_entity id="o14200" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14201" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in cymiform to paniculiform arrays.</text>
      <biological_entity id="o14202" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in arrays" constraintid="o14203" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in cymiform to paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o14203" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="cymiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric or broader, 6–14 mm diam.</text>
      <biological_entity id="o14204" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 14–25 in ± 2–3 series (mostly lance-linear, herbaceous).</text>
      <biological_entity id="o14205" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o14206" from="14" name="quantity" src="d0_s8" to="25" />
      </biological_entity>
      <biological_entity id="o14206" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic;</text>
    </statement>
    <statement id="d0_s10">
      <text>paleate (paleae tan to brown, ovate to oblong-rectangular, conduplicate).</text>
      <biological_entity id="o14207" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 5–15, neuter;</text>
      <biological_entity id="o14208" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow (laminae elliptic, oblong, obovate, oval, or ovate).</text>
      <biological_entity id="o14209" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 25–50+, bisexual, fertile;</text>
      <biological_entity id="o14210" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="50" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, tubes shorter than campanulate throats, lobes 5, triangular (style-branches relatively slender, apices acute).</text>
      <biological_entity id="o14211" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14212" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than campanulate throats" constraintid="o14213" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14213" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o14214" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (black, mottled, or gray-striate) weakly 4-angled, ± obpyramidal (glabrous);</text>
      <biological_entity id="o14215" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s15" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="obpyramidal" value_original="obpyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 8.</text>
      <biological_entity id="o14216" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o14217" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>300.</number>
  <other_name type="common_name">Golden-eye</other_name>
  <discussion>Species 5 (4 in the flora).</discussion>
  <discussion>Heliomeris has often been submerged within Viguiera; herbaceous phyllaries, epappose cypselae, and distinctive chromosome base number provide morphologic and genetic features that correlate with molecular phylogenetic studies to suggest that it be recognized as distinct. A species from granite outcrops of the southeastern United States, Helianthus porteri, shows remarkable morphologic similarity to Heliomeris, but cytologic, crossing, and molecular phylogenetic studies have provided abundant evidence that it is properly placed in Helianthus. W. F. Yates and C. B. Heiser (1979) provided a useful summary of Heliomeris, and their treatment is followed here.</discussion>
  <references>
    <reference>Yates, W. F. and C. B. Heiser. 1979. Synopsis of Heliomeris. Proc. Indiana Acad. Sci. 88: 364–372.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; leaf margins conspicuously ciliate at least 3/4+ their lengths (hairs often 0.5+ mm)</description>
      <determination>1 Heliomeris hispida</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals or perennials; leaf margins ciliate to 1/4 their lengths (hairs to 0.5 mm)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Annuals; leaf blades (at least proximal) ovate to rhombic-ovate, abaxial faces not gland-dotted; peduncle lengths 2–5 times leafy portions of stems</description>
      <determination>2 Heliomeris soliceps</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Annuals or perennials; leaf blades (at least proximal) elliptic, lanceolate, lance-linear, lance-ovate, linear, or ovate, abaxial faces often gland-dotted; peduncle lengths 1/20–1/2 leafy portions of stems</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perennials (caudices woody), proximal and midstem leaves relatively broad (lengths 2–8 times widths)</description>
      <determination>4 Heliomeris multiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Annuals (taprooted) or perennials (caudices woody), proximal and midstem leaves relatively narrow (lengths 6–30+ times widths)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Annuals (taprooted), proximal and midstem leaves (40–)80–160 × 4–8(–12)mm</description>
      <determination>3 Heliomeris longifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Annuals (taprooted), proximal and midstem leaves 10–70(–85) × 1.5–5 mm, or perennials (caudices woody), proximal and midstem leaves 10–90 × 2–20 mm</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Annuals (taprooted, stem branching symmetric), proximal and midstem leaves 10–70(–85) × 1.5–5 mm, heads relatively small, involucres 6–9 diam</description>
      <determination>3 Heliomeris longifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Perennials (caudices woody, stem branching often asymmetric), proximal and midstem leaves 10–90 × 2–20 mm, heads relatively large, involucres 6–14 mm diam</description>
      <determination>4 Heliomeris multiflora</determination>
    </key_statement>
  </key>
</bio:treatment>