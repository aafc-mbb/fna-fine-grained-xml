<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">473</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="treatment_page">493</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rafinesque) G. L. Nesom" date="1995" rank="subgenus">virgulus</taxon_name>
    <taxon_name authority="(Small) Semple in J. C. Semple et al." date="2002" rank="species">plumosum</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Semple et al., Cult. Native Asters Ontario,</publication_title>
      <place_in_publication>134. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus virgulus;species plumosum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067671</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">plumosus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>51: 387. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species plumosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Symphyotrichum</taxon_name>
    <taxon_name authority="(Linnaeus) G. L. Nesom" date="unknown" rank="species">concolor</taxon_name>
    <taxon_name authority="(Small) Wunderlin &amp; B. F. Hansen" date="unknown" rank="variety">plumosum</taxon_name>
    <taxon_hierarchy>genus Symphyotrichum;species concolor;variety plumosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–100 cm, cespitose, eglandular;</text>
    </statement>
    <statement id="d0_s1">
      <text>with cormoid, woody caudices.</text>
      <biological_entity id="o5580" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o5581" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="cormoid" value_original="cormoid" />
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o5580" id="r505" name="with" negation="false" src="d0_s1" to="o5581" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 5–10+, erect or arching (light to dark-brown), proximally sparsely to moderately finely woolly-pilose (hairs spreading to ascending), more densely so distally.</text>
      <biological_entity id="o5582" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" modifier="proximally sparsely; sparsely to moderately finely" name="pubescence" src="d0_s2" value="woolly-pilose" value_original="woolly-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (grayish) soft;</text>
      <biological_entity id="o5583" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering, sessile, blades (1–3-nerved) elliptic-lanceolate, 20–40 × 10–20 mm, bases attenuate, margins usually entire, rarely remotely serrate, piloso-ciliate, apices acute to obtuse, faces silvery piloso-silky;</text>
      <biological_entity constraint="basal" id="o5584" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, margins, apices, faces" constraintid="o5585, o5586, o5587, o5588, o5589" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="piloso-silky" value_original="piloso-silky" />
      </biological_entity>
      <biological_entity id="o5585" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o5586" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o5587" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o5588" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
      <biological_entity id="o5589" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="piloso-ciliate" value_original="piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="silvery" value_original="silvery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline withering by flowering, sessile, blades oblanceolate, 20–30 × 4–8 mm, bases rounded, subclasping, margins entire, scabrous to silky-piloso-ciliate, apices acute to obtuse, cuspidate-mucronate, faces moderately strigillose;</text>
      <biological_entity constraint="proximal cauline" id="o5590" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades, bases, margins, apices, faces" constraintid="o5591, o5592, o5593, o5594, o5595" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o5591" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuspidate-mucronate" value_original="cuspidate-mucronate" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o5592" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuspidate-mucronate" value_original="cuspidate-mucronate" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o5593" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuspidate-mucronate" value_original="cuspidate-mucronate" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o5594" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuspidate-mucronate" value_original="cuspidate-mucronate" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o5595" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="silky-piloso-ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuspidate-mucronate" value_original="cuspidate-mucronate" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades lanceolate to narrowly ovate, 8–15 × 2.5–5 mm, reduced distally, bases cuneate, margins entire, apices acute, mucronate, faces moderately, finely lanoso-strigose.</text>
      <biological_entity constraint="distal" id="o5596" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o5597" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="narrowly ovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o5598" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o5599" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5600" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o5601" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="lanoso-strigose" value_original="lanoso-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads radiate, in narrow, paniculiform (wand-shaped) to sometimes compact, racemiform arrays (1 (–3) per branch).</text>
      <biological_entity id="o5602" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o5603" name="branch" name_original="branch" src="d0_s7" type="structure" />
      <relation from="o5602" id="r506" modifier="in narrow , paniculiform to sometimes sometimes compact , racemiform arrays" name="per" negation="false" src="d0_s7" to="o5603" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles densely hairy, bracts linear, grading into phyllaries.</text>
      <biological_entity id="o5604" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5605" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o5606" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o5605" id="r507" name="into" negation="false" src="d0_s8" to="o5606" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 7–9 mm.</text>
      <biological_entity id="o5607" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, spreading to reflexed, linear, unequal, bases (tan) ± indurate, margins scarious proximally, green distally, green zones foliaceous, apices acute, faces moderately woolly-strigose.</text>
      <biological_entity id="o5608" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o5609" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o5610" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o5611" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="proximally" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s10" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o5612" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o5613" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o5614" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s10" value="woolly-strigose" value_original="woolly-strigose" />
      </biological_entity>
      <relation from="o5608" id="r508" name="in" negation="false" src="d0_s10" to="o5609" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 7–12;</text>
      <biological_entity id="o5615" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas rose-purple, laminae 6–9 × 1–2 mm.</text>
      <biological_entity id="o5616" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="rose-purple" value_original="rose-purple" />
      </biological_entity>
      <biological_entity id="o5617" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 14–20;</text>
      <biological_entity id="o5618" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas pink turning purple, 5–6 mm, tubes shorter than narrowly funnelform throats (thinly puberulent), lobes triangular, 0.3–0.5 mm.</text>
      <biological_entity id="o5619" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink turning purple" value_original="pink turning purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5620" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than narrowly funnelform throats" constraintid="o5621" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5621" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o5622" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae fusiform-obovoid, not compressed, 2–3 mm, 6–8-nerved, faces densely strigose;</text>
      <biological_entity id="o5623" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform-obovoid" value_original="fusiform-obovoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="6-8-nerved" value_original="6-8-nerved" />
      </biological_entity>
      <biological_entity id="o5624" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi tan to yellowish tan, (5.5–) 6.5–8 mm. 2n = 8.</text>
      <biological_entity id="o5625" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s16" to="yellowish tan" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="6.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5626" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep, dry to moist, sandy soils, pine flatwoods, pine-scrub oak woods, favored by fires and clearcuts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="deep" />
        <character name="habitat" value="dry to moist" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="pine-scrub oak woods" />
        <character name="habitat" value="fires" modifier="favored by" />
        <character name="habitat" value="clearcuts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–40 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="40" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion>Symphyotrichum plumosum is known only from Franklin County. It differs from S. concolor var. concolor in its long-acuminate, recurved to reflexed phyllaries. No intermediates with S. concolor were seen in the field.</discussion>
  
</bio:treatment>