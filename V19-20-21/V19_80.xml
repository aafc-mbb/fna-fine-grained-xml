<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="treatment_page">123</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="species">ochrocentrum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 110. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species ochrocentrum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066387</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–90 cm;</text>
      <biological_entity id="o13245" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>crown sprouts or runner roots producing adventitious-buds.</text>
      <biological_entity constraint="crown" id="o13246" name="sprout" name_original="sprouts" src="d0_s1" type="structure" />
      <biological_entity constraint="runner" id="o13247" name="root" name_original="roots" src="d0_s1" type="structure" constraint_original="crown runner" />
      <biological_entity id="o13248" name="adventitiou-bud" name_original="adventitious-buds" src="d0_s1" type="structure" />
      <relation from="o13246" id="r1207" name="producing" negation="false" src="d0_s1" to="o13248" />
      <relation from="o13247" id="r1208" name="producing" negation="false" src="d0_s1" to="o13248" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20+, erect or ascending, densely gray-tomentose with non-septate trichomes;</text>
      <biological_entity id="o13249" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character constraint="with trichomes" constraintid="o13250" is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <biological_entity id="o13250" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="non-septate" value_original="non-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches 0 or few, usually in distal 1/2, ascending.</text>
      <biological_entity id="o13251" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13252" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <relation from="o13251" id="r1209" modifier="usually" name="in" negation="false" src="d0_s3" to="o13252" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades oblong to narrowly elliptic, 10–30 × 2–8 cm, strongly undulate, margins coarsely dentate or shallowly to deeply pinnatifid with 8–15 pairs of lobes 0.5–2 cm, often revolute, lobes ± triangular, closely spaced, spreading, spinose-dentate and cleft into 2–5 spine-tipped divisions, main spines 5–20 mm, yellowish, abaxial faces densely white-tomentose, adaxial thinly gray-tomentose;</text>
      <biological_entity id="o13253" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13254" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="narrowly elliptic" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o13255" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="with pairs" constraintid="o13256" from="coarsely dentate or" name="shape" src="d0_s4" to="shallowly deeply pinnatifid" />
        <character is_modifier="false" modifier="often" name="shape_or_vernation" notes="" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o13256" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s4" to="15" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13257" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o13258" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s4" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinose-dentate" value_original="spinose-dentate" />
        <character constraint="into divisions" constraintid="o13259" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o13259" name="division" name_original="divisions" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
      <biological_entity constraint="main" id="o13260" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13261" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13262" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <relation from="o13256" id="r1210" name="part_of" negation="false" src="d0_s4" to="o13257" />
    </statement>
    <statement id="d0_s5">
      <text>basal usually present at flowering, winged-petiolate;</text>
      <biological_entity id="o13263" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o13264" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline sessile, progressively reduced distally, bases ± auriculate to long-decurrent as spiny wings;</text>
      <biological_entity id="o13265" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o13266" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o13267" name="base" name_original="bases" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="as wings" constraintid="o13268" from="less auriculate" name="shape" src="d0_s6" to="long-decurrent" />
      </biological_entity>
      <biological_entity id="o13268" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cauline usually much reduced, less lobed.</text>
      <biological_entity id="o13269" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal cauline" id="o13270" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually much" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="less" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–few, in leafy, ± corymbiform arrays.</text>
      <biological_entity id="o13271" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s8" to="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–4 cm.</text>
      <biological_entity id="o13272" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres ovoid to hemispheric or broadly campanulate, 2.5–4.5 × 2.5–4.5 cm in first-formed heads, often smaller in later ones, loosely arachnoid on phyllary margins or glabrate.</text>
      <biological_entity id="o13273" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="hemispheric or broadly campanulate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s10" to="4.5" to_unit="cm" />
        <character char_type="range_value" constraint="in first-formed heads" constraintid="o13274" from="2.5" from_unit="cm" name="width" src="d0_s10" to="4.5" to_unit="cm" />
        <character constraint="in ones" constraintid="o13275" is_modifier="false" modifier="often" name="size" notes="" src="d0_s10" value="smaller" value_original="smaller" />
        <character constraint="on phyllary margins" constraintid="o13276" is_modifier="false" modifier="loosely" name="pubescence" notes="" src="d0_s10" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="first-formed" id="o13274" name="head" name_original="heads" src="d0_s10" type="structure" />
      <biological_entity id="o13275" name="one" name_original="ones" src="d0_s10" type="structure">
        <character is_modifier="true" name="condition" src="d0_s10" value="later" value_original="later" />
      </biological_entity>
      <biological_entity constraint="phyllary" id="o13276" name="margin" name_original="margins" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 5–10 series, imbricate, ovate (outer) to linear-lanceolate (inner), margins entire, abaxial faces with narrow glutinous ridge;</text>
      <biological_entity id="o13277" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o13278" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <biological_entity id="o13279" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13281" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o13277" id="r1211" name="in" negation="false" src="d0_s11" to="o13278" />
      <relation from="o13280" id="r1212" name="with" negation="false" src="d0_s11" to="o13281" />
    </statement>
    <statement id="d0_s12">
      <text>outer and middle appressed, spines spreading, 3–12 mm;</text>
      <biological_entity constraint="abaxial" id="o13280" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o13282" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="middle" value_original="middle" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o13283" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner often flexuous, expanded and flat, scabrid-margined, sometimes erose, spineless.</text>
      <biological_entity id="o13284" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="flexuou; flexuou">
        <character is_modifier="false" name="size" src="d0_s13" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="scabrid-margined" value_original="scabrid-margined" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_relief" src="d0_s13" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="spineless" value_original="spineless" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13285" name="flexuou" name_original="flexuous" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="often" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <relation from="o13284" id="r1213" name="part_of" negation="false" src="d0_s13" to="o13285" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas white or pale lavender to purple, pink, or red, 25–45 mm, tubes 8–25 mm, throats 6–17 mm, lobes 6–15 mm;</text>
      <biological_entity id="o13286" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s14" to="purple pink or red" />
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s14" to="purple pink or red" />
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s14" to="purple pink or red" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s14" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13287" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13288" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13289" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 2–8 mm.</text>
      <biological_entity constraint="style" id="o13290" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae light-brown, sometimes with lighter or darker streaks, 6–9 mm, apical collars colored like the body, narrow;</text>
      <biological_entity id="o13291" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s16" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13292" name="streak" name_original="streaks" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="lighter" value_original="lighter" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity constraint="apical" id="o13293" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character constraint="like body" constraintid="o13294" is_modifier="false" name="coloration" src="d0_s16" value="colored" value_original="colored" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s16" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o13294" name="body" name_original="body" src="d0_s16" type="structure" />
      <relation from="o13291" id="r1214" modifier="sometimes" name="with" negation="false" src="d0_s16" to="o13292" />
    </statement>
    <statement id="d0_s17">
      <text>pappi (white or tawny), 20–40 mm, usually noticeably shorter than corolla.</text>
      <biological_entity id="o13296" name="corolla" name_original="corolla" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 30, 31, 32, 34.</text>
      <biological_entity id="o13295" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s17" to="40" to_unit="mm" />
        <character constraint="than corolla" constraintid="o13296" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="usually noticeably shorter" value_original="usually noticeably shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13297" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="30" value_original="30" />
        <character name="quantity" src="d0_s18" value="31" value_original="31" />
        <character name="quantity" src="d0_s18" value="32" value_original="32" />
        <character name="quantity" src="d0_s18" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Kans., N.Mex., Nebr., Okla., S.Dak., Tex., Utah, Wyo.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems densely leafy, nodes crowded; leaves often long- decurrent; corollas white or pale lavender to purple</description>
      <determination>24a Cirsium ochrocentrum var. ochrocentrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems leafy, nodes usually well separated; distal cauline leaves clasping, or if decurrent spiny wing usually less than 1 cm; corollas red, pink, or reddish purple (rarely white)</description>
      <determination>24b Cirsium ochrocentrum var. martinii</determination>
    </key_statement>
  </key>
</bio:treatment>