<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">342</other_info_on_meta>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="(Bartling) Nuttall" date="1841" rank="section">hologymne</taxon_name>
    <taxon_name authority="Lindley" date="1835" rank="species">glabrata</taxon_name>
    <place_of_publication>
      <publication_title>Edwards’s Bot. Reg.</publication_title>
      <place_in_publication>21: plate 1780. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section hologymne;species glabrata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067051</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 60 cm.</text>
      <biological_entity id="o997" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched distally, glabrous or slightly hairy.</text>
      <biological_entity id="o998" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear or subulate, 40–150 × 2–3+ mm, margins entire, faces glabrous.</text>
      <biological_entity id="o999" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s2" to="150" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1000" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1001" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres hemispheric, 5–10 mm.</text>
      <biological_entity id="o1002" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 10–14, ± lanceolate (distinct tips ± deltate), glabrous but for apices.</text>
      <biological_entity id="o1003" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s4" to="14" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character constraint="but-for apices" constraintid="o1004" is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1004" name="apex" name_original="apices" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Receptacles ± conic, papillate, glabrous or sparsely hairy.</text>
      <biological_entity id="o1005" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="relief" src="d0_s5" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 7–15;</text>
      <biological_entity id="o1006" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>(corollas yellow) laminae oblong, 4–14 mm.</text>
      <biological_entity id="o1007" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anther appendages deltate or broadly ovate.</text>
      <biological_entity constraint="anther" id="o1008" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae gray, clavate or obovoid, 2–3.5 mm, margins not ciliate, faces glabrous, or hairy and papillate (papillae rusty or yellowish, wartlike);</text>
      <biological_entity id="o1009" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="gray" value_original="gray" />
        <character is_modifier="false" name="shape" src="d0_s9" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1010" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o1011" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 0.</text>
      <biological_entity id="o1012" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Historically, aboriginal Californians used fruits and leaves of Lasthenia glabrata for food. The subspecies are allopatric and almost identical except for their cypselae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cypselae glabrous, not papillate</description>
      <determination>9a Lasthenia glabrata subsp. glabrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cypselae ± hairy and papillate</description>
      <determination>9b Lasthenia glabrata subsp. coulteri</determination>
    </key_statement>
  </key>
</bio:treatment>