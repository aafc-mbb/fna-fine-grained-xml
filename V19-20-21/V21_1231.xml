<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">488</other_info_on_meta>
    <other_info_on_meta type="illustration_page">489</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="R. M. King &amp; H. Robinson" date="1970" rank="genus">shinnersia</taxon_name>
    <taxon_name authority="(A. Gray) R. M. King &amp; H. Robinson" date="1970" rank="species">rivularis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>19: 297. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus shinnersia;species rivularis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220012488</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trichocoronis</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">rivularis</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 66. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Trichocoronis;species rivularis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rooted in muck;</text>
      <biological_entity id="o15991" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="in muck" constraintid="o15992" is_modifier="false" name="architecture" src="d0_s0" value="rooted" value_original="rooted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15992" name="muck" name_original="muck" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes relatively long.</text>
      <biological_entity id="o15993" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems commonly submerged except for terminal 10 cm or less.</text>
      <biological_entity id="o15994" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="except-for terminal" constraintid="o15995" is_modifier="false" modifier="commonly" name="location" src="d0_s2" value="submerged" value_original="submerged" />
      </biological_entity>
      <biological_entity id="o15995" name="terminal" name_original="terminal" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="cm" value="10" value_original="10" />
        <character name="some_measurement" src="d0_s2" value="less" value_original="less" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly 2–4 cm.</text>
      <biological_entity id="o15996" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne out of water.</text>
      <biological_entity id="o15997" name="head" name_original="heads" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Corollas ca. 2 mm.</text>
      <biological_entity id="o15998" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae ca. 2 mm. 2n = 60.</text>
      <biological_entity id="o15999" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16000" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Dec–)Mar–May(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Water along streams, in slow-moving water (to 1 m deep) arising from calcareous outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="water" constraint="along streams" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="slow-moving water" />
        <character name="habitat" value="1 m" />
        <character name="habitat" value="calcareous outcrops" modifier="deep" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Rio Grande bugheal</other_name>
  <discussion>Shinnersia rivularis has been treated within Trichocoronis. The genera differ in features noted in the couplet below. Sclerolepis, of the southeastern United States, apparently also is closely related, and its recognition as separate reinforces recognition of Shinnersia.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads borne singly, from axils of primary leaves; involucres 5–6 mm; corollas infundibular, tubes relatively long; cypselae sparsely gland-dotted and with forked hairs; carpopodia oblique; pappi 0.</description>
      <determination>400 Shinnersia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads usually in loose or open, corymbiform, bracteate arrays; involucres 2–3 mm; corollas tubular-funnelform, tubes relatively short; cypselae eglandular, scabrellous on angles, without forked hairs; carpopodia at right angles; pappi persistent, of 2–6 setiform scales.</description>
      <determination>399 Trichocoronis</determination>
    </key_statement>
  </key>
</bio:treatment>