<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">472</other_info_on_meta>
    <other_info_on_meta type="mention_page">474</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="treatment_page">492</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rafinesque) G. L. Nesom" date="1995" rank="subgenus">virgulus</taxon_name>
    <taxon_name authority="(Alexander) G. L. Nesom" date="1995" rank="species">walteri</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 294. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus virgulus;species walteri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067694</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Alexander" date="unknown" rank="species">walteri</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Man. S.E. Fl.,</publication_title>
      <place_in_publication>1382, 1509. 1933,</place_in_publication>
      <other_info_on_pub>based on A. squarrosus Walter, Fl. Carol., 209. 1788, not Allioni 1785</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species walteri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lasallea</taxon_name>
    <taxon_name authority="(Alexander) Semple &amp; Brouillet" date="unknown" rank="species">walteri</taxon_name>
    <taxon_hierarchy>genus Lasallea;species walteri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Virgulus</taxon_name>
    <taxon_name authority="(Alexander) Reveal &amp; Keener" date="unknown" rank="species">walteri</taxon_name>
    <taxon_hierarchy>genus Virgulus;species walteri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–100 cm, colonial or cespitose, eglandular, sometimes sparsely, minutely stipitate-glandular;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-rhizomatous, with erect, thick, ± cormoid, woody caudices.</text>
      <biological_entity id="o22356" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes sparsely; sparsely; minutely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o22357" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="true" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s1" value="cormoid" value_original="cormoid" />
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, erect to scandent-sprawling (mid to dark-brown, branched from middle), glabrous or very sparsely fine strigose.</text>
      <biological_entity id="o22358" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="scandent-sprawling" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="very sparsely" name="width" src="d0_s2" value="fine" value_original="fine" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (bright green) thick, firm, ± succulent, margins entire;</text>
      <biological_entity id="o22359" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o22360" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal early deciduous, sessile, blades (3-nerved) oblanceolate to obovate, 10–43 × 7–15 mm, bases cuneate, margins scabrous, apices obtuse, mucronate, faces sparsely finely scabrous;</text>
      <biological_entity constraint="basal" id="o22361" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o22362" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="43" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22363" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o22364" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o22365" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o22366" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely finely" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline sessile (patent to reflexed), blades ovate to lanceolate, 8–30 × 2–10 mm, bases cordate-clasping, apices acute, sometimes spinulose, faces finely scabrous, shiny;</text>
      <biological_entity constraint="proximal cauline" id="o22367" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o22368" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22369" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s5" value="cordate-clasping" value_original="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o22370" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s5" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity id="o22371" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades lanceolate to triangular, 5–15 × 2.5–3.5 mm, reduced to bracts distally, bases clasping, margins finely scabrous, apices acute, white-spinulose, faces finely scabrous.</text>
      <biological_entity constraint="distal" id="o22372" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o22373" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="triangular" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
        <character constraint="to bracts" constraintid="o22374" is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o22374" name="bract" name_original="bracts" src="d0_s6" type="structure" />
      <biological_entity id="o22375" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o22376" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o22377" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="white-spinulose" value_original="white-spinulose" />
      </biological_entity>
      <biological_entity id="o22378" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads borne singly in diffusely paniculiform arrays, branches initially patent to divaricate, elongate.</text>
      <biological_entity id="o22379" name="head" name_original="heads" src="d0_s7" type="structure">
        <character constraint="in arrays" constraintid="o22380" is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o22380" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="diffusely" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o22381" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="initially" name="orientation" src="d0_s7" value="patent" value_original="patent" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles glabrous, bracts relatively numerous, tiny, foliaceous, becoming minute, linear, glabrous.</text>
      <biological_entity id="o22382" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22383" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="relatively" name="quantity" src="d0_s8" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="size" src="d0_s8" value="tiny" value_original="tiny" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="becoming" name="size" src="d0_s8" value="minute" value_original="minute" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 5–7 mm.</text>
      <biological_entity id="o22384" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–5 series, oblanceolate, strongly unequal, bases (tan) ± indurate, margins hyaline, finely scabrous, green zones diamond-shaped, in distal 1/4–1/2, apices obtuse, mucronate to finely subspinulose, innermost acuminate, faces sparsely strigillose (often obscured by thick, shiny cuticle).</text>
      <biological_entity id="o22385" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o22386" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o22387" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o22388" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o22389" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="diamond--shaped" value_original="diamond--shaped" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="finely" name="architecture" notes="" src="d0_s10" value="subspinulose" value_original="subspinulose" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o22391" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s10" to="1/2" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o22392" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s10" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s10" to="1/2" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity constraint="innermost" id="o22393" name="apex" name_original="apices" src="d0_s10" type="structure" />
      <relation from="o22385" id="r2065" name="in" negation="false" src="d0_s10" to="o22386" />
      <relation from="o22389" id="r2066" name="in" negation="false" src="d0_s10" to="o22391" />
      <relation from="o22389" id="r2067" name="in" negation="false" src="d0_s10" to="o22392" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 11–26;</text>
      <biological_entity id="o22394" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s11" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas bluish purple, laminae 5–9 (–11) × 0.5–1 mm.</text>
      <biological_entity id="o22395" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="bluish purple" value_original="bluish purple" />
      </biological_entity>
      <biological_entity id="o22396" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (6–) 15–25 (–30);</text>
      <biological_entity id="o22397" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s13" to="15" to_inclusive="false" />
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="30" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, 4–6.5 mm, throats narrowly funnelform, lobes triangular, 0.4–0.8 mm.</text>
      <biological_entity id="o22398" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22399" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o22400" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae tan to brown, obovoid, not compressed, 2–2.5 mm, nerves very faint, faces sparsely to moderately strigose;</text>
      <biological_entity id="o22401" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s15" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22402" name="nerve" name_original="nerves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="very" name="prominence" src="d0_s15" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity id="o22403" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi tan, 4–6 mm. 2n = 20.</text>
      <biological_entity id="o22404" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="tan" value_original="tan" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22405" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, clayey soils, margins of woods, open oak-pine scrub, pine flatwoods, fields, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clayey soils" />
        <character name="habitat" value="margins" constraint="of woods" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="oak-pine scrub" modifier="open" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Walter’s aster</other_name>
  
</bio:treatment>