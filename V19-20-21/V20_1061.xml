<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Luc Brouillet</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">18</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="treatment_page">458</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1995" rank="genus">CANADANTHUS</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 250. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus CANADANTHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Canada and Greek anthos, flower, alluding to mainly Canadian distribution</other_info_on_name>
    <other_info_on_name type="fna_id">105493</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(G. L. Nesom) Semple" date="unknown" rank="subgenus">Canadanthus</taxon_name>
    <taxon_hierarchy>genus Aster;subgenus Canadanthus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–100 (–130) cm (rhizomatous).</text>
      <biological_entity id="o28858" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="130" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, simple, ± densely villous, stipitate-glandular distally.</text>
      <biological_entity id="o28859" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o28860" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-nerved, elliptic-lanceolate or oblanceolate to (sometimes narrowly) oblanceolate or lanceolate, margins entire to serrate, abaxial faces glabrate to ± strigose, adaxial sparsely villous, (distal) stipitate-glandular.</text>
      <biological_entity id="o28861" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="oblanceolate or lanceolate" />
      </biological_entity>
      <biological_entity id="o28862" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s5" to="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28863" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="more or less strigose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28864" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, usually in leafy, irregularly flat-topped, paniculiform to corymbiform arrays, sometimes borne singly.</text>
      <biological_entity id="o28865" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" notes="" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o28866" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="true" modifier="irregularly" name="shape" src="d0_s6" value="flat-topped" value_original="flat-topped" />
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s6" to="corymbiform" />
      </biological_entity>
      <relation from="o28865" id="r2669" modifier="usually" name="in" negation="false" src="d0_s6" to="o28866" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindro-campanulate, {(5–) 7–10 ×} 12–16 mm.</text>
      <biological_entity id="o28867" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s7" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 26–60+ in 3–5 series, 1-nerved (flat), lanceolate, subequal, outer ± foliaceous, inner herbaceous, bases not indurate, abaxial faces stipitate-glandular.</text>
      <biological_entity id="o28868" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o28869" from="26" name="quantity" src="d0_s8" to="60" upper_restricted="false" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o28869" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o28870" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o28871" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o28872" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s8" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28873" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, pitted, epaleate.</text>
      <biological_entity id="o28874" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 20–65 in 1 series, pistillate, fertile;</text>
      <biological_entity id="o28875" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o28876" from="20" name="quantity" src="d0_s10" to="65" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o28876" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas pale to dark purple or rose (coiling).</text>
      <biological_entity id="o28877" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s11" to="dark purple or rose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 40–65, bisexual, fertile;</text>
      <biological_entity id="o28878" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="65" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas whitish to pale-yellow, turning purple, lobes sometimes purplish-tinged, ± ampliate, tubes shorter than tubular throats, lobes 5, erect, triangular;</text>
      <biological_entity id="o28879" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s13" to="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="purplish-tinged" value_original="purplish-tinged" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s13" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o28880" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s13" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o28881" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than tubular throats" constraintid="o28882" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o28882" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o28883" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o28879" id="r2670" name="turning" negation="false" src="d0_s13" to="o28880" />
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages narrowly triangular (hairy).</text>
      <biological_entity constraint="style-branch" id="o28884" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae obconic-fusiform, compressed, 4–9-nerved, faces sparsely strigose, eglandular;</text>
      <biological_entity id="o28885" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obconic-fusiform" value_original="obconic-fusiform" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="4-9-nerved" value_original="4-9-nerved" />
      </biological_entity>
      <biological_entity id="o28886" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi of 20–35+ cinnamon, equal, barbellate, attenuate bristles in 1 series.</text>
      <biological_entity id="o28888" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s16" to="35" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="cinnamon" value_original="cinnamon" />
        <character is_modifier="true" name="variability" src="d0_s16" value="equal" value_original="equal" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o28889" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o28887" id="r2671" name="consist_of" negation="false" src="d0_s16" to="o28888" />
      <relation from="o28888" id="r2672" name="in" negation="false" src="d0_s16" to="o28889" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o28887" name="pappus" name_original="pappi" src="d0_s16" type="structure" />
      <biological_entity constraint="x" id="o28890" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>210.</number>
  <other_name type="common_name">Aster</other_name>
  <discussion>Species 1.</discussion>
  <discussion>G. L. Nesom (1994b, 2000) established Canadanthus as a monotypic genus and provided justification at the morphologic and cytologic levels. According to Nesom, Canadanthus (x = 9) belongs in Symphyotrichinae, in an isolated position similar to that of Ampelaster (x = 9), Almutaster (x = 9), and Psilactis (x = 9, 5, 4); most of the species of the subtribe are in genus Symphyotrichum (x = 8, 7, 5, 4). J. C. Semple and L. Brouillet (1980) had placed Canadanthus in Aster subg. Modesti. A molecular phylogenetic study of North American asters based on restriction site analysis of chloroplast DNA by Xiang C. and Semple (1996) showed Canadanthus to be basal to Ampelaster and Symphyotrichum. The basal position of Canadanthus in subtribe Symphyotrichinae was confirmed in a phylogeny of North American and Eurasian asters using nuclear ribosomal DNA (ITS) (Semple et al. 2002). Canadanthus modestus has been associated also with Eurybia sibirica, notably by Á. Löve and D. Löve (1982b) when they created the genus Weberaster, or with E. radulina (e.g., A. G. Jones 1980; Jones and D. A. Young 1983). This may be due to similarities in the habit, foliage, and involucres of these species.</discussion>
  
</bio:treatment>