<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="treatment_page">97</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Hooker ex Nuttall" date="1840" rank="genus">balsamorhiza</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">balsamorhiza</taxon_name>
    <taxon_name authority="(W. M. Sharp) W. A. Weber" date="1999" rank="species">lanata</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>85: 20. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus balsamorhiza;subgenus balsamorhiza;species lanata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066211</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Balsamorhiza</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">hookeri</taxon_name>
    <taxon_name authority="W. M. Sharp" date="unknown" rank="variety">lanata</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>22: 130. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Balsamorhiza;species hookeri;variety lanata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–20 (–30) cm.</text>
      <biological_entity id="o2826" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blades white to grayish, lanceolate to linear-oblong, 10–20 × 3–6 (–8) cm (1–2-pinnatifid, primary lobes lance-linear to oblong, mostly 5–40 × 1–10 mm, secondary lobes or teeth antrorse, divergent), bases cuneate to truncate, ultimate margins mostly entire (± revolute), apices rounded to acute, faces densely lanate-tomentose to villous.</text>
      <biological_entity constraint="basal" id="o2827" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o2828" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s1" to="grayish" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="linear-oblong" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2829" name="base" name_original="bases" src="d0_s1" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s1" to="truncate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o2830" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2831" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="acute" />
      </biological_entity>
      <biological_entity id="o2832" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="densely lanate-tomentose" name="pubescence" src="d0_s1" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads borne singly.</text>
      <biological_entity id="o2833" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres ± hemispheric, 12–20 mm diam.</text>
      <biological_entity id="o2834" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Outer phyllaries lanceolate to linear, 10–20 mm, sometimes surpassing inner, apices acute to attenuate.</text>
      <biological_entity constraint="outer" id="o2835" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o2836" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity id="o2837" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="attenuate" />
      </biological_entity>
      <relation from="o2835" id="r214" modifier="sometimes" name="surpassing" negation="false" src="d0_s4" to="o2836" />
    </statement>
    <statement id="d0_s5">
      <text>Ray laminae (10–) 15–20 mm (abaxially puberulent or glabrous).</text>
      <biological_entity constraint="ray" id="o2838" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, grassy slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="grassy slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Balsamorhiza lanata is known from a relatively restricted area in northern California and southern Oregon. No hybrids between it and other species have been noted.</discussion>
  
</bio:treatment>