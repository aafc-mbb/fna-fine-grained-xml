<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="treatment_page">320</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Perityle</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section perityle</taxon_hierarchy>
    <other_info_on_name type="fna_id">316957</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, or subshrubs (delicate or robust, sometimes weedy, in rock crevices or soil).</text>
      <biological_entity id="o4220" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o4222" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres campanulate to hemispheric, 3–11 × 4–14 mm.</text>
      <biological_entity id="o4223" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s1" to="hemispheric" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ray-florets 0 or (1–) 8–18;</text>
      <biological_entity id="o4224" name="ray-floret" name_original="ray-florets" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s2" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corollas yellow or white.</text>
      <biological_entity id="o4225" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc-florets 20–100+;</text>
      <biological_entity id="o4226" name="disc-floret" name_original="disc-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s4" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas yellow (sometimes pink or purple-tinged).</text>
      <biological_entity id="o4227" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae linear, linear-elliptic, linear-oblong, oblanceolate, oblong, ovate, or subcuneate, usually flattened (outer often 3-angled), margins usually prominently calloused, sometimes thin (not calloused), usually ± densely ciliate;</text>
      <biological_entity id="o4228" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcuneate" value_original="subcuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcuneate" value_original="subcuneate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o4229" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually prominently" name="texture" src="d0_s6" value="calloused" value_original="calloused" />
        <character is_modifier="false" modifier="sometimes" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually more or less densely" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi 0 or of 1–2 (–3+), unequal to subequal, ± barbellate bristles 0.5–4 (–6) mm, sometimes plus crowns of hyaline, laciniate scales.</text>
      <biological_entity id="o4230" name="pappus" name_original="pappi" src="d0_s7" type="structure">
        <character constraint="of bristles" constraintid="o4231" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="of 1-2(-3+) , unequal to subequal , more or less barbellate bristles" />
      </biological_entity>
      <biological_entity id="o4231" name="bristle" name_original="bristles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" upper_restricted="false" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
        <character char_type="range_value" from="unequal" is_modifier="true" name="size" src="d0_s7" to="subequal" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s7" value="barbellate" value_original="barbellate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4233" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s7" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o4232" id="r337" modifier="sometimes" name="part_of" negation="false" src="d0_s7" to="o4233" />
    </statement>
    <statement id="d0_s8">
      <text>x = 17, 19.</text>
      <biological_entity id="o4232" name="crown" name_original="crowns" src="d0_s7" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity constraint="x" id="o4234" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="17" value_original="17" />
        <character name="quantity" src="d0_s8" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>352b.</number>
  <discussion>Species 30 (7 in the flora).</discussion>
  <references>
    <reference>Powell, A. M. 1974. Taxonomy of Perityle, section Perityle (Compositae–Peritylanae). Rhodora 76: 229–306.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray florets 0 (Brewster and Terrell counties, Texas)</description>
      <determination>13 Perityle aglossa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray florets usually 6–18 (sometimes 0 or 1–6 in P. parryi from w Presidio County, Texas)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray and disc corollas yellow; Texas</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray corollas white, disc corollas yellow (sometimes purple tinged); Arizona, California, Nevada, New Mexico, Utah</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves usually cordate to subreniform, margins usually irregularly dentate, laciniate, or ± 3-lobed, seldom more divided (perennials or subshrubs in rock crevices or in soil)</description>
      <determination>11 Perityle parryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves usually ternately lobed or ± cruciform, lobes usually again ternately lobed (perennials in soil)</description>
      <determination>12 Perityle vaseyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Annuals (sometimes persisting)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Subshrubs</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Cypselae (1.5–)2–3 mm, margins thin (not calloused); pappi 0, or of single, antrorsely to retrorsely barbellate bristles 1–3 mm plus crowns of hyaline, laciniate scales</description>
      <determination>7 Perityle emoryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Cypselae 1.5–2 mm, margins prominently calloused; pappi of 2 unequal, antrorsely barbellate bristles 0.8–1.2 mm (the longest) plus crowns of hyaline, laciniate scales</description>
      <determination>8 Perityle microglossa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades deltate-ovate to ovate-rhombic, margins usually entire or serrate to serrate-crenate, sometimes shallow-lobed; c Arizona</description>
      <determination>9 Perityle ciliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades pedately lobed (lobes spatulate or linear) or 2–3-pinnatifid (lobes linear-filiform); s Arizona, New Mexico</description>
      <determination>10 Perityle coronopifolia</determination>
    </key_statement>
  </key>
</bio:treatment>