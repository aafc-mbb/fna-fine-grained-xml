<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">417</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">psathyrotes</taxon_name>
    <taxon_name authority="(Torrey) A. Gray" date="1868" rank="species">ramosissima</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 363. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus psathyrotes;species ramosissima</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067376</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tetradymia</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">ramosissima</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Not. Milit. Reconn.,</publication_title>
      <place_in_publication>145. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tetradymia;species ramosissima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 3–30 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>lanate, furfuraceous.</text>
      <biological_entity id="o14917" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="furfuraceous" value_original="furfuraceous" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect and spreading, much branched.</text>
      <biological_entity id="o14919" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades rounded-deltate to suborbiculate, 8–25 × 8–30 mm, margins toothed.</text>
      <biological_entity id="o14920" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded-deltate" name="shape" src="d0_s3" to="suborbiculate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14921" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 3–50 mm.</text>
      <biological_entity id="o14922" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres broadly turbinate to campanulate, 6–10 mm.</text>
      <biological_entity id="o14923" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly turbinate" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 15–24, outer persistent, 5–6, apically spatulate, tips reflexed to squarrose, inner falling, 10–18, apically lanceolate, tips erect.</text>
      <biological_entity id="o14924" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s6" to="24" />
      </biological_entity>
      <biological_entity constraint="outer" id="o14925" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="6" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s6" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o14926" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character char_type="range_value" from="reflexed" name="orientation" src="d0_s6" to="squarrose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o14927" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="falling" value_original="falling" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="18" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o14928" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 16–32;</text>
      <biological_entity id="o14929" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s7" to="32" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas pale-yellow, 4.5–5 mm.</text>
      <biological_entity id="o14930" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 1.5–3.5 mm;</text>
      <biological_entity id="o14931" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 120–140 subequal bristles in 2–4 series, 3–4 mm. 2n = 34.</text>
      <biological_entity id="o14932" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14933" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="120" is_modifier="true" name="quantity" src="d0_s10" to="140" />
        <character is_modifier="true" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o14934" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14935" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
      </biological_entity>
      <relation from="o14932" id="r1024" name="consist_of" negation="false" src="d0_s10" to="o14933" />
      <relation from="o14933" id="r1025" name="in" negation="false" src="d0_s10" to="o14934" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly in spring, otherwise sporadically, following rains.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly; , otherwise sporadically, following rains" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils and desert pavements</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="desert pavements" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-30–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="30" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Turtleback</other_name>
  
</bio:treatment>