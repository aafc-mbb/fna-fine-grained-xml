<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="treatment_page">255</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Greene) Shinners" date="1951" rank="species">camporum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">camporum</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species camporum;variety camporum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068473</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 35–70 cm.</text>
      <biological_entity id="o19563" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Distal stems and peduncles eglandular or sparsely to moderately stipitate-glandular.</text>
      <biological_entity constraint="distal" id="o19564" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19565" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Distal leaf faces moderately hispido-strigose, usually eglandular, rarely sparsely stipitate-glandular.</text>
    </statement>
    <statement id="d0_s3">
      <text>2n = 36.</text>
      <biological_entity constraint="leaf" id="o19566" name="face" name_original="faces" src="d0_s2" type="structure" constraint_original="distal leaf">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19567" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, sandy plains, limestone bluffs, ledges and glades, sandy banks, slopes of sandhills, dry rocky or sandy barrens, railroad rights-of-way, sandy disturbed areas, and in open oak woods of the central oak-hickory vegetation zone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="sandy plains" />
        <character name="habitat" value="limestone bluffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="sandy banks" />
        <character name="habitat" value="slopes" constraint="of sandhills , dry rocky or sandy barrens , railroad rights-of-way , sandy disturbed areas , and in open oak woods of the central oak-hickory vegetation zone" />
        <character name="habitat" value="sandhills" />
        <character name="habitat" value="dry rocky" />
        <character name="habitat" value="sandy barrens" />
        <character name="habitat" value="railroad rights-of-way" />
        <character name="habitat" value="sandy disturbed areas" />
        <character name="habitat" value="open oak woods" />
        <character name="habitat" value="the central oak-hickory vegetation zone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ill., Ind., Mo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17a.</number>
  <discussion>Variety camporum grows on bluffs along the Mississippi River, sandy areas near rivers, and roadsides in eastern Missouri, northern Arkansas, Illinois, and western Indiana. It may be similar in stature and general appearance to Heterotheca villosa var. minor and var. villosa, whose ranges just reach the northwestern limit of distribution of var. camporum in northern Illinois. Possibly it was introduced in Nebraska in the late 1800s but did not survive. Sparse serration of the mid cauline leaf margins distinguishes var. camporum from varieties of H. villosa.</discussion>
  
</bio:treatment>