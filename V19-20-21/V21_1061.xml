<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
    <other_info_on_meta type="illustration_page">421</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Fougeroux" date="1788" rank="genus">gaillardia</taxon_name>
    <taxon_name authority="Fougeroux" date="1788" rank="species">pulchella</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Acad. Roy. Sci. Mém. Math. Phys. (Paris,</publication_title>
      <place_in_publication>4to) 1786: 5, fig. 1. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus gaillardia;species pulchella</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200023953</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="(Hooker) de Candolle" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_hierarchy>genus Gaillardia;species drummondii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">neomexicana</taxon_name>
    <taxon_hierarchy>genus Gaillardia;species neomexicana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="D. Don" date="unknown" rank="species">picta</taxon_name>
    <taxon_hierarchy>genus Gaillardia;species picta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pulchella</taxon_name>
    <taxon_name authority="B. L. Turner &amp; M. Whalen" date="unknown" rank="variety">australis</taxon_name>
    <taxon_hierarchy>genus Gaillardia;species pulchella;variety australis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pulchella</taxon_name>
    <taxon_name authority="(D. Don) A. Gray" date="unknown" rank="variety">picta</taxon_name>
    <taxon_hierarchy>genus Gaillardia;species pulchella;variety picta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (sometimes persisting), 5–35 (–60+) cm.</text>
      <biological_entity id="o5372" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
      <biological_entity id="o5373" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiolar bases 0–3+ cm;</text>
      <biological_entity constraint="petiolar" id="o5374" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear, oblong, or spatulate, 1–5 (–12) cm × 4–12 (–35) mm, (bases of distal ± clasping) margins usually entire, sometimes toothed or lobed, faces closely strigillose or hirtellous to ± villous (hairs jointed).</text>
      <biological_entity id="o5375" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5376" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o5377" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s3" to="more or less villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 3–10 (–20) cm.</text>
      <biological_entity id="o5378" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 18–28+, narrowly triangular to linear-attenuate, 6–14+ mm, usually ciliate with jointed hairs.</text>
      <biological_entity id="o5379" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s5" to="28" upper_restricted="false" />
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s5" to="linear-attenuate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="14" to_unit="mm" upper_restricted="false" />
        <character constraint="with hairs" constraintid="o5380" is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o5380" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacular setae 1.5–3 mm.</text>
      <biological_entity constraint="receptacular" id="o5381" name="seta" name_original="setae" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets usually 8–14, rarely 0;</text>
      <biological_entity id="o5382" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="14" />
        <character modifier="rarely" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas usually reddish to purplish proximally, yellow to orange distally, rarely yellow, reddish, or purplish throughout, 13–30+ mm.</text>
      <biological_entity id="o5383" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually reddish" name="coloration" src="d0_s8" to="purplish proximally" />
        <character char_type="range_value" from="usually reddish" name="coloration" src="d0_s8" to="purplish proximally" />
        <character char_type="range_value" from="usually reddish" modifier="throughout" name="coloration" src="d0_s8" to="purplish proximally" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 40–100+;</text>
      <biological_entity id="o5384" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellowish to purple or brown, often bicolored, tubes 0.8–1.2 mm, throats campanulate to urceolate, 3–4 mm, lobes deltate to ovate, often attenuate, 1–3+ mm, jointed hairs 0.3+ mm.</text>
      <biological_entity id="o5385" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s10" to="purple or brown" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="bicolored" value_original="bicolored" />
      </biological_entity>
      <biological_entity id="o5386" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5387" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s10" to="urceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5388" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s10" to="ovate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s10" value="attenuate" value_original="attenuate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o5389" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae obpyramidal, 2–2.5 mm, hairs 1.5–2 mm, inserted at bases and on angles;</text>
      <biological_entity id="o5390" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5391" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5392" name="angle" name_original="angles" src="d0_s11" type="structure" />
      <relation from="o5391" id="r407" name="inserted at bases and on" negation="false" src="d0_s11" to="o5392" />
    </statement>
    <statement id="d0_s12">
      <text>pappi of 7–8 deltate to lanceolate, aristate scales 4–7 mm (scarious bases 1–2.5 × 0.7–1.3 mm).</text>
      <biological_entity id="o5394" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s12" to="8" />
        <character char_type="range_value" from="deltate" is_modifier="true" name="shape" src="d0_s12" to="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s12" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <relation from="o5393" id="r408" name="consist_of" negation="false" src="d0_s12" to="o5394" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 34.</text>
      <biological_entity id="o5393" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o5395" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Dec, mostly May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Jan" />
        <character name="flowering time" char_type="range_value" modifier="mostly" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or calcareous soils, often disturbed places, mostly in grasslands or open places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="calcareous soils" />
        <character name="habitat" value="disturbed places" modifier="often" />
        <character name="habitat" value="grasslands" modifier="mostly" />
        <character name="habitat" value="open places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Colo., Conn., Fla., Ga., Kans., Maine, Miss., Mo., Nebr., N.H., N.Mex., N.C., Okla., S.C., S.Dak., Tex., Vt.; Mexico (Chihuahua, Coahuila, Nuevo León, Sonora, and Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (and Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Firewheel</other_name>
  <other_name type="common_name">Indian blanket</other_name>
  <discussion>Spring-flowering plants with most of their proximal leaves pinnately lobed or coarsely toothed, mainly found in south-central Texas, have been treated as var. australis (B. L. Turner and M. Whalen 1975). Plants from near or on beaches of the Atlantic and the Gulf of Mexico, usually with somewhat fleshy leaves and often persisting for more than one year, have been distinguished as var. picta.</discussion>
  <discussion>Cultivars of Gaillardia pulchella (or of hybrids between G. pulchella and G. aristata) are used horticulturally.</discussion>
  <references>
    <reference>Turner, B. L. and M. Whalen. 1975. Taxonomic study of Gaillardia pulchella (Asteraceae–Heliantheae). Wrightia 5: 189–192.</reference>
  </references>
  
</bio:treatment>