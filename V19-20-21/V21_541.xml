<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">pectis</taxon_name>
    <taxon_name authority="(Fernald) Rydberg in N. L. Britton et al." date="1916" rank="species">cylindrica</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 198. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus pectis;species cylindrica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067291</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pectis</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">prostrata</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">cylindrica</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>33: 68. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pectis;species prostrata;variety cylindrica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–20 cm (across or high);</text>
      <biological_entity id="o12193" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>herbage not scented.</text>
      <biological_entity id="o12194" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s1" value="scented" value_original="scented" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to ascending (often mat-forming, densely leafy distally), puberulent (in decurrent lines or throughout) or glabrate.</text>
      <biological_entity id="o12195" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (bluish green) linear to linear-oblanceolate or narrowly oblong, 10–30 × 1.5–4 mm, margins with 2–5 pairs of setae 1–2 mm, faces glabrous (abaxial densely dotted with scattered, circular oil-glands 0.05–0.2 mm).</text>
      <biological_entity id="o12196" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-oblanceolate or narrowly oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12197" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o12198" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12199" name="seta" name_original="setae" src="d0_s3" type="structure" />
      <biological_entity id="o12200" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12197" id="r836" name="with" negation="false" src="d0_s3" to="o12198" />
      <relation from="o12198" id="r837" name="part_of" negation="false" src="d0_s3" to="o12199" />
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in congested, (leafy) cymiform arrays.</text>
      <biological_entity id="o12201" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in congested , cymiform arrays" />
      </biological_entity>
      <biological_entity id="o12202" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s4" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o12201" id="r838" name="in" negation="false" src="d0_s4" to="o12202" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–5 mm.</text>
      <biological_entity id="o12203" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres cylindric.</text>
      <biological_entity id="o12204" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries coherent (falling together), oblong to oblong-obovate, 6–10 × 2–3 mm (dotted with scattered, elliptic oil-glands 0.05–0.15 mm).</text>
      <biological_entity id="o12205" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="coherent" value_original="coherent" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="oblong-obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 3 (–4);</text>
      <biological_entity id="o12206" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="4" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 3–4 mm (scarcely surpassing phyllaries).</text>
      <biological_entity id="o12207" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets (3–) 7–14;</text>
      <biological_entity id="o12208" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 2.2–2.6 mm (2-lipped).</text>
      <biological_entity id="o12209" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 4–5.5 mm, puberulent (distally glandular-puberulent);</text>
      <biological_entity id="o12210" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 2 (ray) and 5 (disc) lanceolate scales 1.5–3.5 mm. 2n = 48.</text>
      <biological_entity id="o12211" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o12212" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="true" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12213" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="48" value_original="48" />
      </biological_entity>
      <relation from="o12211" id="r839" name="consist_of" negation="false" src="d0_s13" to="o12212" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deserts, oak-juniper woodlands, grasslands, wash channels, mud flats, lawns, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deserts" />
        <character name="habitat" value="oak-juniper woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="channels" modifier="wash" />
        <character name="habitat" value="mud flats" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Baja California Sur, Chihuahua, Coahuila, Durango, Nuevo León, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Three-rayed or Sonoran chinchweed</other_name>
  <discussion>Pectis cylindrica (2n = 48) is similar to P. prostrata (2n = 24); the two occasionally grow together (D. J. Keil 1975b). Some herbaria contain mixed collections of the two. No evidence is available of hybrids between the two.</discussion>
  
</bio:treatment>