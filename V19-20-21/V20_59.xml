<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">44</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">doellingeria</taxon_name>
    <taxon_name authority="(Miller) Nees" date="1832" rank="species">umbellata</taxon_name>
    <taxon_name authority="(A. Gray) Britton in N. L. Britton and A. Brown" date="1898" rank="variety">pubens</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and A. Brown, Ill. Fl. N. U.S.</publication_title>
      <place_in_publication>3: 392. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus doellingeria;species umbellata;variety pubens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068263</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">umbellatus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">pubens</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 197. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species umbellatus;variety pubens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Cronquist" date="unknown" rank="species">pubentior</taxon_name>
    <taxon_hierarchy>genus Aster;species pubentior;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Doellingeria</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg" date="unknown" rank="species">pubens</taxon_name>
    <taxon_hierarchy>genus Doellingeria;species pubens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Doellingeria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">umbellata</taxon_name>
    <taxon_name authority="(A. Gray) Á. Löve &amp; D. Löve" date="unknown" rank="subspecies">pubens</taxon_name>
    <taxon_hierarchy>genus Doellingeria;species umbellata;subspecies pubens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–120 cm.</text>
      <biological_entity id="o27725" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves densely puberulent (more than 12 hairs/mm2), abaxially moderately to densely canescent (indument often dense over entire plant).</text>
      <biological_entity id="o27726" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="abaxially moderately; moderately to densely" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries sparsely piloso-villous, sometimes more densely apically.</text>
    </statement>
    <statement id="d0_s3">
      <text>2n = 18.</text>
      <biological_entity id="o27727" name="phyllarie" name_original="phyllaries" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="piloso-villous" value_original="piloso-villous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27728" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist soils, clearings, thickets, margins of forests and near streams, prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist soils" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="margins" constraint="of forests and near streams" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="near streams" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500(–700) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Ont., Que., Sask.; Ill., Iowa, Mich., Minn., Nebr., N.Dak., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Aster pubescent</other_name>
  <discussion>Disjunct populations of var. pubens are found in Alberta. The indument is often dense over the entire plant. The leaves are moderately to densely canescent abaxially.</discussion>
  
</bio:treatment>