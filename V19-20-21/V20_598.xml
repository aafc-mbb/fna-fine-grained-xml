<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Cronquist" date="1947" rank="species">piperianus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>6: 197. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species piperianus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066654</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (3–) 6–12 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices usually branched.</text>
      <biological_entity id="o9613" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o9614" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect (distinctly white in proximal portions), usually strigose at least on distal 1/2, sometimes strigoso-hirsute or mixed-hispid, minutely glandular.</text>
      <biological_entity id="o9615" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="on distal 1/2" constraintid="o9616" is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="strigoso-hirsute" value_original="strigoso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="mixed-hispid" value_original="mixed-hispid" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9616" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline (proximal internodes elongate; petioles prominently ciliate, hairs spreading, thick-based);</text>
      <biological_entity id="o9617" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9618" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to linear-oblanceolate, 20–40 × 1–1.5 mm (on proximal 1/2–2/3 of stems), (bases relatively thin, not sheathing) margins entire, hispido-ciliate, faces strigose.</text>
      <biological_entity id="o9619" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9620" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="hispido-ciliate" value_original="hispido-ciliate" />
      </biological_entity>
      <biological_entity id="o9621" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o9622" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3.5–4 × 5–10 mm.</text>
      <biological_entity id="o9623" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, hirsuto-villous, sometimes minutely glandular.</text>
      <biological_entity id="o9624" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="hirsuto-villous" value_original="hirsuto-villous" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o9625" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o9624" id="r888" name="in" negation="false" src="d0_s7" to="o9625" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 25–40;</text>
      <biological_entity id="o9626" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s8" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, drying dark yellow (sometimes fading on drying and appearing whitish or creamy), 4–9 mm, laminae coiling.</text>
      <biological_entity id="o9627" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="condition" src="d0_s9" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark yellow" value_original="dark yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9628" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2.5–3 mm.</text>
      <biological_entity constraint="disc" id="o9629" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.6–1.8 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o9630" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o9631" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 15–25 bristles.</text>
      <biological_entity id="o9632" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o9633" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o9634" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s12" to="25" />
      </biological_entity>
      <relation from="o9632" id="r889" name="outer of" negation="false" src="d0_s12" to="o9633" />
      <relation from="o9632" id="r890" name="inner of" negation="false" src="d0_s12" to="o9634" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open places, level to moderate slope, commonly with sagebrush, usually undisturbed, sometimes grazed and/or burned, sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="open places" />
        <character name="habitat" value="level" />
        <character name="habitat" value="moderate slope" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="sites" modifier="undisturbed sometimes grazed and\/or burned" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(100–)200–400(–700) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Piper’s fleabane</other_name>
  
</bio:treatment>