<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">547</other_info_on_meta>
    <other_info_on_meta type="illustration_page">545</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="J. Gay ex Gussone" date="1845" rank="genus">cota</taxon_name>
    <taxon_name authority="(Linnaeus) J. Gay ex Gussone" date="1845" rank="species">tinctoria</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Sicul. Syn.</publication_title>
      <place_in_publication>2: 867. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus cota;species tinctoria</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066437</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anthemis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">tinctoria</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 896. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anthemis;species tinctoria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 1–3 (–5) cm, ultimate lobes narrowly oblong to spatulate or linear, ultimate margins entire or serrate (teeth apiculate).</text>
      <biological_entity id="o17244" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o17245" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s0" to="spatulate or linear" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o17246" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Phyllaries: abaxial faces sericeous to arachnose, margins often ciliolate.</text>
      <biological_entity id="o17247" name="phyllary" name_original="phyllaries" src="d0_s1" type="structure" />
      <biological_entity constraint="abaxial" id="o17248" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s1" to="arachnose" />
      </biological_entity>
      <biological_entity id="o17249" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s1" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Paleae 4–5 mm (including spinose tips).</text>
      <biological_entity id="o17250" name="palea" name_original="paleae" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray laminae yellow, 6–12+ mm.</text>
      <biological_entity constraint="ray" id="o17251" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc corollas 3.5–4 mm.</text>
      <biological_entity constraint="disc" id="o17252" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 1.8–2.2 mm;</text>
      <biological_entity id="o17253" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s5" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi usually 2–2.5 mm. 2n = 18.</text>
      <biological_entity id="o17254" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17255" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, fields, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–600+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., Que., Sask.; Alaska, Ark., Calif., Conn., Idaho, Ill., Iowa, Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Golden marguerite</other_name>
  <other_name type="common_name">yellow chamomile</other_name>
  
</bio:treatment>