<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">130</other_info_on_meta>
    <other_info_on_meta type="illustration_page">131</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">borrichia</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">arborescens</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 489. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus borrichia;species arborescens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066243</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Buphthalmum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">arborescens</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1227. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Buphthalmum;species arborescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems decumbent to ascending.</text>
      <biological_entity id="o22862" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s0" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves (at least mid cauline) oblanceolate to linear, 5–10 (–15) × 0.4–2 cm, margins entire or dentate to serrate (teeth remote mostly toward apices), faces glabrous or sericeous (sometimes on same leaf).</text>
      <biological_entity id="o22863" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="linear" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22864" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o22865" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads borne singly or (3–6) in cymiform arrays (peduncles 0.5–5 cm).</text>
      <biological_entity id="o22866" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o22867" from="3" name="atypical_quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o22867" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres ovoid to hemispheric, (8–) 10–14 (–17) mm.</text>
      <biological_entity id="o22868" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s3" to="hemispheric" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="17" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 10–16 in 3–4 series (chartaceous in fruiting heads, outer ovate-elliptic, apices obtuse or acute, faces glabrate or sericeous; inner oblanceolate, apices rounded, obtuse, or acute, faces glabrous).</text>
      <biological_entity id="o22869" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o22870" from="10" name="quantity" src="d0_s4" to="16" />
      </biological_entity>
      <biological_entity id="o22870" name="series" name_original="series" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Paleae: apices obtuse to acute.</text>
      <biological_entity id="o22871" name="palea" name_original="paleae" src="d0_s5" type="structure" />
      <biological_entity id="o22872" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 7–15;</text>
      <biological_entity id="o22873" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 5–8 × 2.5–4 mm.</text>
      <biological_entity id="o22874" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 20–50;</text>
      <biological_entity id="o22875" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 5–6 mm, lobes 1–1.5 mm.</text>
      <biological_entity id="o22876" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22877" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 3.5–5 mm, 4-angled.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 28.</text>
      <biological_entity id="o22878" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="4-angled" value_original="4-angled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22879" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky coasts, sandy marshes, beaches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky coasts" />
        <character name="habitat" value="sandy marshes" />
        <character name="habitat" value="beaches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies; Bermuda.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Bermuda" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Borrichia arborescens grows in the Florida Keys, Bermuda, and the West Indies, including Jamaica and Guadeloupe. Plants of B. arborescens are usually glabrous; both glabrous and hairy plants occur in the northern West Indies. Common names that include “tree” are misleading because the plants are shrubs.</discussion>
  
</bio:treatment>