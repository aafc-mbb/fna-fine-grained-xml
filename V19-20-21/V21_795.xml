<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="treatment_page">321</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">perityle</taxon_name>
    <taxon_name authority="Torrey in W. H. Emory" date="1848" rank="species">emoryi</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Not. Milit. Reconn.,</publication_title>
      <place_in_publication>142. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section perityle;species emoryi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067319</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (sometimes persisting), 2–60 cm (delicate or robust, stems relatively few-to-many, erect or spreading);</text>
    </statement>
    <statement id="d0_s1">
      <text>puberulent to hirsute, glandular-pubescent.</text>
      <biological_entity id="o1086" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s1" to="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 3–45 mm;</text>
      <biological_entity id="o1087" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1088" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ovate, cordate, suborbiculate, or triangular, 17–60 × 10–50 mm, margins deeply toothed, lobed, cleft, or divided, lobes indented to irregularly dissected.</text>
      <biological_entity id="o1089" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1090" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1091" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o1092" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="indented" name="shape" src="d0_s3" to="irregularly dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in corymbiform arrays, 4–10 × 4–10 mm.</text>
      <biological_entity id="o1093" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in corymbiform arrays" value_original="in corymbiform arrays" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1094" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o1093" id="r71" name="in" negation="false" src="d0_s4" to="o1094" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–70 mm.</text>
      <biological_entity id="o1095" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate to hemispheric.</text>
      <biological_entity id="o1096" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s6" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 10–20, lanceolate or oblanceolate to ovatelanceolate, 4–6 × 1–2 mm.</text>
      <biological_entity id="o1097" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="20" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="ovatelanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets usually 8–14, rarely rudimentary or 0;</text>
      <biological_entity id="o1098" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="14" />
        <character is_modifier="false" modifier="rarely" name="prominence" src="d0_s8" value="rudimentary" value_original="rudimentary" />
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, laminae oblong, 1–4 (–6) × 1–3 mm.</text>
      <biological_entity id="o1099" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1100" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 40–100+;</text>
      <biological_entity id="o1101" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s10" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, tubes 0.7–1.3 mm, throats tubular to tubular-funnelform, 0.8–1.3 mm, lobes 0.1–0.2 mm.</text>
      <biological_entity id="o1102" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o1103" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1104" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character char_type="range_value" from="tubular" name="shape" src="d0_s11" to="tubular-funnelform" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1105" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae suboblong, oblanceolate, or subcuneate, (1.5–) 2–3 mm, margins thin (not calloused), long or short-ciliate;</text>
      <biological_entity id="o1106" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="suboblong" value_original="suboblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subcuneate" value_original="subcuneate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subcuneate" value_original="subcuneate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1107" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="short-ciliate" value_original="short-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0 or of 1 antrorsely to retrorsely barbellate bristles 1–3 mm plus crowns of hyaline, laciniate scales.</text>
      <biological_entity id="o1108" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1109" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="antrorsely to retrorsely" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1111" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o1110" id="r72" name="part_of" negation="false" src="d0_s13" to="o1111" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 65–72 or 100–116.</text>
      <biological_entity id="o1110" name="crown" name_original="crowns" src="d0_s13" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity constraint="2n" id="o1112" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character char_type="range_value" from="65" name="quantity" src="d0_s14" to="72" />
        <character char_type="range_value" from="100" name="quantity" src="d0_s14" to="116" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round (depending on latitude).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal bluffs, desert plains, slopes, washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="desert plains" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico; South America (Chile, Peru).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Emory’s rock daisy</other_name>
  <discussion>Perityle emoryi is a widespread polyploid of diverse habitats and is often weedy. It is variable; none of the variation appears to have population significance and does not require taxonomic recognition. The range of P. emoryi appears to be gradually expanding.</discussion>
  
</bio:treatment>