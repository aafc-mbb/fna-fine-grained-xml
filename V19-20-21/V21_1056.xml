<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">423</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Fougeroux" date="1788" rank="genus">gaillardia</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="species">multiceps</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>24: 512. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus gaillardia;species multiceps</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066786</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaillardia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">multiceps</taxon_name>
    <taxon_name authority="B. L. Turner" date="unknown" rank="variety">microcephala</taxon_name>
    <taxon_hierarchy>genus Gaillardia;species multiceps;variety microcephala;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 30–45+ cm.</text>
      <biological_entity id="o19157" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o19158" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
      <biological_entity id="o19159" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiolar bases essentially none;</text>
      <biological_entity constraint="petiolar" id="o19160" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="essentially" name="quantity" src="d0_s2" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades narrowly spatulate or linear, 2–6 cm × 3–5 (–8+) mm, margins entire, faces sparsely and minutely hispidulous or glabrate.</text>
      <biological_entity id="o19161" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="8" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19162" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19163" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s3" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 2–5+ cm.</text>
      <biological_entity id="o19164" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 20–26, lanceolate to narrowly triangular-attenuate, 6–10 mm, ± sericeous to villous, not ciliate with jointed hairs.</text>
      <biological_entity id="o19165" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s5" to="26" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="narrowly triangular-attenuate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="less sericeous" name="pubescence" src="d0_s5" to="villous" />
        <character constraint="with hairs" constraintid="o19166" is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19166" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacular setae 0.5–2.5 mm.</text>
      <biological_entity constraint="receptacular" id="o19167" name="seta" name_original="setae" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 8;</text>
      <biological_entity id="o19168" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 13–20 mm.</text>
      <biological_entity id="o19169" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 50–80+;</text>
      <biological_entity id="o19170" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s9" to="80" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas proximally yellow, distally purplish, tubes 1–1.5 mm, throats campanulate, 4–4.5 mm, lobes ovate-deltate, 0.8 mm, jointed hairs mostly 0.3+ mm.</text>
      <biological_entity id="o19171" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o19172" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19173" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19174" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate-deltate" value_original="ovate-deltate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
      <biological_entity id="o19175" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae obpyramidal, 2.5–3 mm, hairs 2.5–3 mm, inserted at bases and on angles and faces;</text>
      <biological_entity id="o19176" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19177" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19178" name="angle" name_original="angles" src="d0_s11" type="structure" />
      <biological_entity id="o19179" name="face" name_original="faces" src="d0_s11" type="structure" />
      <relation from="o19177" id="r1305" name="inserted at bases and on" negation="false" src="d0_s11" to="o19178" />
      <relation from="o19177" id="r1306" name="inserted at bases and on" negation="false" src="d0_s11" to="o19179" />
    </statement>
    <statement id="d0_s12">
      <text>pappi of 10 lanceolate, aristate scales 6–9 mm (scarious bases 4–5 × 0.8–1.5 mm).</text>
      <biological_entity id="o19181" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="10" value_original="10" />
        <character is_modifier="true" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s12" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <relation from="o19180" id="r1307" name="consist_of" negation="false" src="d0_s12" to="o19181" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 68, ca. 102.</text>
      <biological_entity id="o19180" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o19182" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="68" value_original="68" />
        <character name="quantity" src="d0_s13" value="102" value_original="102" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gypseous soils, including dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gypseous soils" />
        <character name="habitat" value="dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>See comments under 1. Gaillardia pinnatifida.</discussion>
  
</bio:treatment>