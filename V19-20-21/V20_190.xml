<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Arthur Haines</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="treatment_page">97</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Nuttall) Cassini in F. Cuvier" date="1825" rank="genus">EUTHAMIA</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 37: 471. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus EUTHAMIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek eu- , good, well, and thama, crowded, evidently alluding to branching pattern</other_info_on_name>
    <other_info_on_name type="fna_id">112460</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Euthamia</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 162. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Solidago;subgenus Euthamia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 40–200 cm (rhizomes creeping, fibrous-rooted).</text>
      <biological_entity id="o14760" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o14761" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (nearly terete), simple or branched, glabrous or hairy.</text>
      <biological_entity id="o14762" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o14763" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (± uniform along stems) linear to lanceolate (40–130 mm), margins entire, faces glabrous or hairy, sparsely to densely gland-dotted (dots obscure or evident, 0.1–0.25 mm diam., 0–86 per mm²).</text>
      <biological_entity id="o14764" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o14765" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14766" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely to densely" name="coloration" src="d0_s5" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or (glomerulate) in corymbiform or paniculiform arrays.</text>
      <biological_entity id="o14767" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in arrays" constraintid="o14768" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in corymbiform or paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o14768" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres obconic to hemispheric, (2.5–6.3 ×) 2.1–8.1 mm.</text>
      <biological_entity id="o14769" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s7" to="hemispheric" />
        <character char_type="range_value" from="[2.5" from_unit="mm" name="length" src="d0_s7" to="6.3" to_unit="mm" />
        <character char_type="range_value" from="]2.1" from_unit="mm" name="width" src="d0_s7" to="8.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 11–29 in 3–5 series, 1-nerved (flat), linear to ovate, bases often stramineous or pale, margins chartaceous or weakly cartilaginous, not scarious (apices with green zones, erose to ciliate), abaxial faces glabrous, little to very resinous.</text>
      <biological_entity id="o14770" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o14771" from="11" name="quantity" src="d0_s8" to="29" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="ovate" />
      </biological_entity>
      <biological_entity id="o14771" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o14772" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o14773" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="weakly" name="pubescence_or_texture" src="d0_s8" value="cartilaginous" value_original="cartilaginous" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14774" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="very" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, pitted (pit borders ± fimbrillate), epaleate.</text>
      <biological_entity id="o14775" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 7–22 (–35) (usually more numerous than disc-florets), pistillate, fertile;</text>
      <biological_entity id="o14776" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="22" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="35" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="22" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o14777" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 3–22, bisexual, fertile;</text>
      <biological_entity id="o14778" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="22" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than tubular to slender-funnelform throats, lobes 5, erect to ascending, oblong-lanceolate;</text>
      <biological_entity id="o14779" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14780" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="to throats" constraintid="o14781" is_modifier="false" name="shape" src="d0_s13" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o14781" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o14782" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-lanceolate" value_original="oblong-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages lanceolate.</text>
      <biological_entity constraint="style-branch" id="o14783" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae oblong to narrowly ellipsoid, ± terete, 2–4-nerved, strigose;</text>
      <biological_entity id="o14784" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="narrowly ellipsoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-4-nerved" value_original="2-4-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 20–30, white, ± equal, antrorsely barbellate, apically attenuate bristles in 1 series.</text>
      <biological_entity id="o14786" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s16" to="30" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s16" value="equal" value_original="equal" />
        <character is_modifier="true" modifier="antrorsely" name="architecture" src="d0_s16" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o14787" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o14785" id="r1352" name="consist_of" negation="false" src="d0_s16" to="o14786" />
      <relation from="o14786" id="r1353" name="in" negation="false" src="d0_s16" to="o14787" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o14785" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o14788" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico; introduced in Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>158.</number>
  <discussion>Species 5 (5 in the flora).</discussion>
  <discussion>Euthamia was formerly included in Solidago. Arrangements of heads, gland-dotted leaves, and DNA sequence data demonstrate that Euthamia should be treated as distinct from Solidago (L. C. Anderson and J. B. Creech 1975; R. D. Noyes and L. H. Rieseberg 1999).</discussion>
  <discussion>Ambiguous and contradictory information has led to much debate about who named Euthamia (D. J. Sieren 1981; K. N. Gandhi 1999; G. L. Nesom 1999; J. L. Strother 2000). I consider correct authorship for this genus to be as given in Index Nominum Genericorum (http://ravenel.si.edu/botany/ing).</discussion>
  <discussion>Euthamia is capable of tremendous phenotypic variation. Transitional aspects of a given plant are much more likely to be related to environmental factors than to introgression. Heights of arrays are determined by measuring from the summit of the plant to the bases of proximalmost head-bearing branches.</discussion>
  <references>
    <reference>Sieren, D. J. 1981. The taxonomy of the genus Euthamia. Rhodora 83: 551–579.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous and glaucous; arrays of heads not flat-topped, lengths of proximal branches 0.18–0.56 times array heights (creating multistoried aspects to arrays); apices of inner phyllaries acute to acuminate</description>
      <determination>5 Euthamia occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous or hairy, not glaucous; arrays of heads usually flat-topped or rounded, lengths of proximal branches 0.5–1 times array heights; apices of inner phyllaries obtuse to acute</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Disc corollas 2.5–3.3(–3.4) mm; involucres 3–4.7(–5.3) mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Disc corollas 3.3–4.8 mm; involucres (4–)4.5–6.3 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems glabrous or glabrate; leaf blades usually 1–3 mm wide (to 6 mm wide in some Maine and Nova Scotia populations), faces glabrous or glabrate, abundantly and prominently gland-dotted</description>
      <determination>1 Euthamia caroliniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems glabrous or densely spreading-hirtellous; leaf blades usually 3–12 mm wide, faces glabrous or densely spreading-hirtellous, usually sparsely and obscurely gland-dotted</description>
      <determination>2 Euthamia graminifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades linear to lanceolate, lengths 12–49 times widths, gradually reduced distally, faces abundantly and prominently gland-dotted, not pustulate; arrays (25–)35–60% of plant heights</description>
      <determination>3 Euthamia gymnospermoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades lanceolate to narrowly lanceolate, lengths 8–18 times widths, abruptly reduced distally, faces little and obscurely gland-dotted, sometimes pustulate;arrays 6–35% of plant heights</description>
      <determination>4 Euthamia leptocephala</determination>
    </key_statement>
  </key>
</bio:treatment>