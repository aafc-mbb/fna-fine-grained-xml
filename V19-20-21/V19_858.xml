<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">505</other_info_on_meta>
    <other_info_on_meta type="treatment_page">509</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="Besser" date="1829" rank="subgenus">drancunculus</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">pedatifida</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 399. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus drancunculus;species pedatifida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066159</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 5–15 cm (cespitose), aromatic.</text>
      <biological_entity id="o2627" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o2628" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (5–20), gray-green, glabrescent.</text>
      <biological_entity id="o2629" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s1" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, gray-green, mostly basal;</text>
      <biological_entity id="o2630" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o2631" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal blades reduced, mostly less than 1 cm, lobed or entire;</text>
      <biological_entity constraint="proximal" id="o2632" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal blades 1–2 × 0.5–0.8 cm, 1–2-ternately lobed, lobes 1–2 mm wide, apices acute, faces densely tomentose.</text>
      <biological_entity constraint="distal" id="o2633" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="0.8" to_unit="cm" />
        <character is_modifier="false" modifier="1-2-ternately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o2634" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2635" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o2636" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (mostly 6–15, 1 or 3–4 on lateral branches; mostly erect, sessile or pedunculate) in racemiform-paniculiform arrays, 5–8 × 0.5–0.8 cm.</text>
      <biological_entity id="o2637" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" notes="" src="d0_s5" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2638" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="racemiform-paniculiform" value_original="racemiform-paniculiform" />
      </biological_entity>
      <relation from="o2637" id="r258" name="in" negation="false" src="d0_s5" to="o2638" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres globose, 3–4 × 3–4 mm.</text>
      <biological_entity id="o2639" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries (margins scarious, obscured) white-tomentose.</text>
      <biological_entity id="o2640" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets: pistillate 4–7;</text>
      <biological_entity id="o2641" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>functionally staminate 5–9;</text>
      <biological_entity id="o2642" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, usually red-tinged, 2–3 mm, glabrous.</text>
      <biological_entity id="o2643" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <biological_entity id="o2644" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="red-tinged" value_original="red-tinged" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae (brown) ellipsoid (angled), 0.8–1 mm, (sometimes with white ribs) glabrous.</text>
      <biological_entity id="o2645" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>High plains, grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="high plains" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Matted sagewort</other_name>
  
</bio:treatment>