<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="treatment_page">98</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Hooker ex Nuttall" date="1840" rank="genus">balsamorhiza</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">balsamorhiza</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">macrophylla</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 350. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus balsamorhiza;subgenus balsamorhiza;species macrophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066213</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Balsamorhiza</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">hookeri</taxon_name>
    <taxon_name authority="(W. M. Sharp) Cronquist" date="unknown" rank="variety">idahoensis</taxon_name>
    <taxon_hierarchy>genus Balsamorhiza;species hookeri;variety idahoensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Balsamorhiza</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">macrophylla</taxon_name>
    <taxon_name authority="W. M. Sharp" date="unknown" rank="variety">idahoensis</taxon_name>
    <taxon_hierarchy>genus Balsamorhiza;species macrophylla;variety idahoensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–45 (–100) cm.</text>
      <biological_entity id="o18531" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves green, ovate to lanceolate, 20–50+ × 8–15 cm (pinnatifid, lobes lanceolate, 20–80+ × 10–40 mm, entire or ± dentate), bases ± cuneate, ultimate margins usually entire (plane or weakly revolute, ciliate), apices obtuse to acute, faces scabrous or piloso-hirtellous to pilose (at least abaxial usually glanddotted as well).</text>
      <biological_entity constraint="basal" id="o18532" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="lanceolate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s1" to="50" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18533" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o18534" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18535" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="acute" />
      </biological_entity>
      <biological_entity id="o18536" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="piloso-hirtellous" name="pubescence" src="d0_s1" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads usually borne singly.</text>
      <biological_entity id="o18537" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres ± hemispheric, 20–30 mm diam.</text>
      <biological_entity id="o18538" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Outer phyllaries lanceovate or lanceolate to lance-linear, 12–30 (–40) mm, equaling or surpassing inner (margins ciliate), apices acute to attenuate.</text>
      <biological_entity constraint="outer" id="o18539" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="lance-linear" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s4" to="30" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s4" value="surpassing inner phyllaries" value_original="surpassing inner phyllaries" />
      </biological_entity>
      <biological_entity id="o18540" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray laminae 35–50+ mm.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 100 ± 2.</text>
      <biological_entity constraint="ray" id="o18541" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18542" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="more,less" value="100" value_original="100" />
        <character name="quantity" src="d0_s6" unit="more,less" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep soils, rocky meadows, sagebrush scrublands, conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep soils" />
        <character name="habitat" value="rocky meadows" />
        <character name="habitat" value="sagebrush scrublands" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Balsamorhiza macrophylla is a high polyploid; it occurs sympatrically with B. sagittata. It evidently arose from hybridization between B. sagittata and B. hispidula. Balsamorhiza macrophylla has the multi-branched caudices and massive taproots of the former, and the leaf dissection of the latter. No hybrids with other species are known. Presumably, the high-polyploid chromosome complement precludes interbreeding. Plants of var. idahoensis are smaller, are known only from southwestern Idaho and northeastern Utah, and differ from var. macrophylla by being pilose, with strongly shaggy-pilose involucres. More study may determine that var. idahoensis merits specific rank. The Utah populations are not well understood and deserve attention.</discussion>
  <references>
    <reference>Helton, N., D. Wiens, and B. A. Barlow. 1972. High polyploidy and the origin of Balsamorhiza macrophylla. Madroño 21: 526–535.</reference>
  </references>
  
</bio:treatment>