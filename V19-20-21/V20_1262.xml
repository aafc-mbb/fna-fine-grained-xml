<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">564</other_info_on_meta>
    <other_info_on_meta type="treatment_page">565</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1843" rank="species">fremontii</taxon_name>
    <taxon_name authority="Cronquist in A. Cronquist et al." date="1994" rank="variety">inexpectatus</taxon_name>
    <place_of_publication>
      <publication_title>in A. Cronquist et al., Intermount. Fl.</publication_title>
      <place_in_publication>5: 172, unnumb. fig. [upper left] 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species fremontii;variety inexpectatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068724</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–2+ dm.</text>
      <biological_entity id="o9708" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly arching upward.</text>
      <biological_entity id="o9709" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually smaller than 4 × 2 cm, bases not clasping, margins notably laciniate to laciniate-toothed or lobed (the lobes sometimes also toothed).</text>
      <biological_entity id="o9710" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character modifier="usually smaller than" name="length" src="d0_s2" unit="cm" value="4" value_original="4" />
        <character modifier="usually smaller than" name="width" src="d0_s2" unit="cm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9711" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o9712" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="notably laciniate" name="shape" src="d0_s2" to="laciniate-toothed or lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1–2 (–4).</text>
      <biological_entity id="o9713" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries (± 8) ± 13, 7–10 mm.</text>
      <biological_entity id="o9714" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s4" value="8" value_original="8" />
        <character modifier="more or less" name="quantity" src="d0_s4" value="13" value_original="13" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering s­ummer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3000+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="" from="3000" from_unit="" constraint=" + m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42d.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>