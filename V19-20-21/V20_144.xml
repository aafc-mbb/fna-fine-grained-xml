<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">76</other_info_on_meta>
    <other_info_on_meta type="illustration_page">58</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">resinosa</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 319. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species resinosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066535</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">resinosus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species resinosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–50 cm.</text>
      <biological_entity id="o18824" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, green when young, fastigiately branched, glabrous or sparsely hairy, resinous.</text>
      <biological_entity id="o18825" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="fastigiately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly ascending to spreading, often recurved distally;</text>
      <biological_entity id="o18826" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly ascending" name="orientation" src="d0_s2" to="spreading" />
        <character is_modifier="false" modifier="often; distally" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades filiform to narrowly oblanceolate (adaxially sulcate), 10–25 × 0.5–1.5 mm, midnerves obscure, apices apiculate, faces glabrous, resinous (axillary fascicles usually present).</text>
      <biological_entity id="o18827" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18828" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o18829" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o18830" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in loose, cymiform arrays (1–4 cm wide).</text>
      <biological_entity id="o18831" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o18832" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o18831" id="r1742" name="in" negation="false" src="d0_s4" to="o18832" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–10 mm (bracts 0–15+).</text>
      <biological_entity id="o18833" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres obconic, 5.5–8 × 3–5 mm.</text>
      <biological_entity id="o18834" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 22–34 in 4–5 series, green to tan, lanceolate to oblong, 1–6 × 0.5–1.5 mm, strongly unequal (sometimes outer subequal), mostly chartaceous, outer herbaceous or apically so (mid bodies often apically obtuse to notched, with subulate appendages), midnerves evident, (margins narrowly membranous, entire or serrulate, often ciliolate) apices acute to attenuate (outer often squarrose), abaxial faces glabrous, resinous.</text>
      <biological_entity id="o18835" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o18836" from="22" name="quantity" src="d0_s7" to="34" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s7" to="tan" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s7" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o18836" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18837" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character name="growth_form_or_texture" src="d0_s7" value="apically" value_original="apically" />
      </biological_entity>
      <biological_entity id="o18838" name="midnerve" name_original="midnerves" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o18839" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="attenuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18840" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s7" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 3–7;</text>
      <biological_entity id="o18841" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae (white) 5.5–7 × 1.5–2 mm.</text>
      <biological_entity id="o18842" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 10–15;</text>
      <biological_entity id="o18843" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas (white) 6–8 mm.</text>
      <biological_entity id="o18844" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae tan to brown, narrowly turbinate, 4.5–5 mm, moderately to densely sericeous;</text>
      <biological_entity id="o18845" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s12" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi off-white to brown, 5.8–7.2 mm. 2n = 18.</text>
      <biological_entity id="o18846" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s13" to="brown" />
        <character char_type="range_value" from="5.8" from_unit="mm" name="some_measurement" src="d0_s13" to="7.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18847" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky plains, steep hillsides, and cliff faces, often on basalt</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky plains" />
        <character name="habitat" value="steep hillsides" />
        <character name="habitat" value="cliff" />
        <character name="habitat" value="basalt" modifier="faces often on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="common_name">Columbian goldenbush</other_name>
  <other_name type="common_name">Columbia goldenweed</other_name>
  
</bio:treatment>