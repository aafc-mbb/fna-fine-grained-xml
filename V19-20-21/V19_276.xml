<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="A. Heller" date="1899" rank="species">atribarba</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 314. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species atribarba</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066439</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Osterhout" date="unknown" rank="species">exilis</taxon_name>
    <taxon_hierarchy>genus Crepis;species exilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">exilis</taxon_name>
    <taxon_name authority="Babcock &amp; Stebbins" date="unknown" rank="subspecies">originalis</taxon_name>
    <taxon_hierarchy>genus Crepis;species exilis;subspecies originalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_name authority="D. C. Eaton" date="unknown" rank="variety">gracilis</taxon_name>
    <taxon_hierarchy>genus Crepis;species occidentalis;variety gracilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–70 cm (taproots slender, caudices swollen, often covered by old leaf-bases).</text>
      <biological_entity id="o7679" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–2, erect, slender, usually branched distal to middles, glabrous or tomentulose.</text>
      <biological_entity id="o7680" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="2" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="to middles" constraintid="o7681" is_modifier="false" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity id="o7681" name="middle" name_original="middles" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o7682" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades lanceolate to linear, 10–35 × 0.5–6 cm, margins deeply pinnately lobed (lobes narrowly lanceolate or linear, usually entire or toothed), apices acuminate, faces tomentulose to glabrate.</text>
      <biological_entity id="o7683" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="35" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7684" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o7685" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o7686" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="tomentulose" name="pubescence" src="d0_s4" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 3–30, in corymbiform arrays.</text>
      <biological_entity id="o7687" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="30" />
      </biological_entity>
      <biological_entity id="o7688" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o7687" id="r714" name="in" negation="false" src="d0_s5" to="o7688" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 5–10, narrowly triangular to lanceolate, tomentose bractlets 1–3 mm.</text>
      <biological_entity id="o7689" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly triangular" modifier="of 5-10" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o7690" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindro-campanulate, 10–12 × 4–7 mm.</text>
      <biological_entity id="o7691" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 8–13, lanceolate, 10–12 mm (margins yellow, scarious, eciliate), apices acute, abaxial faces usually tomentulose, sometimes glabrous, often with coarse, green or blackish setae, adaxial glabrous or with fine, appressed hairs.</text>
      <biological_entity id="o7692" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="13" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7693" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7694" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7695" name="seta" name_original="setae" src="d0_s8" type="structure">
        <character is_modifier="true" name="relief" src="d0_s8" value="coarse" value_original="coarse" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="blackish" value_original="blackish" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7696" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with fine , appressed hairs" />
      </biological_entity>
      <biological_entity id="o7697" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="fine" value_original="fine" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o7694" id="r715" modifier="often" name="with" negation="false" src="d0_s8" to="o7695" />
      <relation from="o7696" id="r716" name="with" negation="false" src="d0_s8" to="o7697" />
    </statement>
    <statement id="d0_s9">
      <text>Florets 6–35;</text>
      <biological_entity id="o7698" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 10–18 mm.</text>
      <biological_entity id="o7699" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae dark or blackish green, subcylindric, 3–10 mm, apices tapered, not beaked, ribs 12–15 (distinct);</text>
      <biological_entity id="o7700" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark" value_original="dark" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="blackish green" value_original="blackish green" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7701" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o7702" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi whitish, 5–9 mm. 2n = 22, 33, 44, 55, 88.</text>
      <biological_entity id="o7703" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7704" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="22" value_original="22" />
        <character name="quantity" src="d0_s12" value="33" value_original="33" />
        <character name="quantity" src="d0_s12" value="44" value_original="44" />
        <character name="quantity" src="d0_s12" value="55" value_original="55" />
        <character name="quantity" src="d0_s12" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open, grassy places, sagebrush slopes, pine forests, gravelly stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" modifier="dry" />
        <character name="habitat" value="grassy places" />
        <character name="habitat" value="sagebrush slopes" />
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="gravelly stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Colo., Idaho, Mont., Nebr., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">atrabarba</other_name>
  <other_name type="common_name">Slender or dark hawksbeard</other_name>
  <discussion>Crepis atribarba is generally recognized by the deeply pinnately lobed leaves with linear lobes, fine tomentulose indument on stems and leaves, setose phyllaries, and dark green, strongly ribbed cypselae. It is a variable mixture that includes polyploid, apomictic forms and hybrids with C. acuminata and other species. The typical form is recognized by its short stature, narrow pinnately lobed, tomentulose leaves, stems with 3–10 heads, and phyllaries with scattered, black, eglandular setae. Larger, more robust forms with stems 30–70 cm, 10–30+ heads, narrower involucres, and few or no black setae have been recognized as subsp. originalis. The latter was considered by E. B. Babcock (1947) to represent the original diploid form of the species; it is difficult to distinguish in practice.</discussion>
  
</bio:treatment>