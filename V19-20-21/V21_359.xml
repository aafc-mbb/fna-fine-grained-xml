<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">154</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="treatment_page">152</other_info_on_meta>
    <other_info_on_meta type="illustration_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Nuttall" date="1821" rank="species">petiolaris</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>2: 115. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species petiolaris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416637</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 40–200 cm.</text>
      <biological_entity id="o4948" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually densely canescent, hispid, or strigillose, rarely ± hirsute or glabrate.</text>
      <biological_entity id="o4949" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="rarely more or less" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="rarely more or less" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="rarely more or less" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o4951" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate;</text>
      <biological_entity id="o4950" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles 2–4 cm;</text>
      <biological_entity id="o4952" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (often bluish green) lanceolate to deltate-ovate or ovate, 4–15 × 1–8 cm, bases subcordate or truncate to cuneate, margins entire or ± serrate, abaxial faces strigose, sparsely to densely, or not at all, glanddotted.</text>
      <biological_entity id="o4953" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="deltate-ovate or ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4954" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o4955" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4956" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–5.</text>
      <biological_entity id="o4957" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 4–15 (–40) cm.</text>
      <biological_entity id="o4958" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="40" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± hemispheric, 10–24 mm diam.</text>
      <biological_entity id="o4959" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 14–25, lance-linear to lanceolate to lanceovate, 10–14 × 1–4 (–5) mm, (margins sometimes ciliate) apices short-attenuate, abaxial faces usually hispidulous, rarely sparsely hirsute to glabrate.</text>
      <biological_entity id="o4960" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s9" to="25" />
        <character char_type="range_value" from="lance-linear" name="shape" src="d0_s9" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4961" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-attenuate" value_original="short-attenuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4962" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="hispidulous" value_original="hispidulous" />
        <character char_type="range_value" from="rarely sparsely hirsute" name="pubescence" src="d0_s9" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 4.5–7.5 mm, 3-toothed, middle teeth ± ciliate or bearded, hairs whitish, 0.5–0.7 mm.</text>
      <biological_entity id="o4963" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o4964" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s10" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o4965" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 10–30;</text>
      <biological_entity id="o4966" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 15–20 mm.</text>
      <biological_entity id="o4967" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 50–100+;</text>
      <biological_entity id="o4968" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s13" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 4.5–6 mm, lobes usually reddish, rarely yellow;</text>
      <biological_entity id="o4969" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4970" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers reddish to purplish, appendages purplish (style-branches reddish).</text>
      <biological_entity id="o4971" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s15" to="purplish" />
      </biological_entity>
      <biological_entity id="o4972" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 3–4.5 mm, ± villous;</text>
      <biological_entity id="o4973" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 aristate scales 1.5–3 mm plus 0–2 erose scales 0.3–0.5 mm. 2n = 34.</text>
      <biological_entity id="o4974" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="2" />
      </biological_entity>
      <biological_entity id="o4975" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4976" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture_or_relief" src="d0_s17" value="erose" value_original="erose" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4977" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
      </biological_entity>
      <relation from="o4974" id="r378" name="consist_of" negation="false" src="d0_s17" to="o4975" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Ont., Sask.; Ariz., Ark., Calif., Colo., Conn., D.C., Del., Idaho, Ill., Ind., Iowa, Kans., La., Maine, Mass., Mich., Minn., Mo., Mont., N.C., N.Dak., N.J., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Oreg., Pa., S.C., S.Dak., Tenn., Tex., Utah, Va., W.Va., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Prairie sunflower</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Helianthus petiolaris is adventive beyond western North America.</discussion>
  <discussion>A third subspecies has yet to be named at that rank in Helianthus petiolaris; it has been called H. petiolaris var. canescens A. Gray. It differs in having stems, leaves, and phyllaries densely canescent and abaxial faces of leaves densely gland-dotted. It is additionally characterized by peduncles usually ebracteate, phyllaries 1–2 mm wide, disc corolla throats gradually narrowed distal to slight, not densely hairy basal bulges, and 2n = 34. It flowers late spring through late summer and grows on sandy soils in open areas at (10–)1000–2300 m in Arizona, California, Nevada, New Mexico, and Texas and in Mexico. It was treated as H. niveus (Bentham) Brandegee subsp. canescens (A. Gray) Heiser by C. B. Heiser et al. (1969); molecular and morphologic data appear to favor a placement within H. petiolaris.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually hispidulous to strigillose; peduncles usually bractless; phyllaries 3–5 mm wide; disc corollas: throats abruptly narrowed distal to densely hairy basal bulbs</description>
      <determination>9a Helianthus petiolaris subsp. petiolaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually ± hispid; peduncles usually each with leafy bract subtending head; phyllaries 2–3.5 mm wide; disc corollas: throats gradually narrowed distal to slight, not densely hairy, basal bulges</description>
      <determination>9b Helianthus petiolaris subsp. fallax</determination>
    </key_statement>
  </key>
</bio:treatment>