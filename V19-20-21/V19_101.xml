<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="treatment_page">132</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="1901" rank="species">hydrophilum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. W. Calif.,</publication_title>
      <place_in_publication>507. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species hydrophilum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066377</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">hydrophilus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>44: 358. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carduus;species hydrophilus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="species">vaseyi</taxon_name>
    <taxon_name authority="(Greene) Petrak" date="unknown" rank="variety">hydrophilum</taxon_name>
    <taxon_hierarchy>genus Cirsium;species vaseyi;variety hydrophilum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or monocarpic perennials, 100–220 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o13863" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="220" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="220" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, erect, (hollow), openly branched distally or throughout, thinly arachnoid with fine, non-septate trichomes, glabrate.</text>
      <biological_entity id="o13865" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="openly; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character constraint="with trichomes" constraintid="o13866" is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o13866" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="fine" value_original="fine" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="non-septate" value_original="non-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blades elliptic to broadly oblanceolate, 10–40+ cm, pinnatifid 1/2–2/3 distance to midveins, larger usually with broad sinuses, lobes broad, few lobed or dentate, main spines 2–9 mm, abaxial faces ± gray-tomentose, sometimes ± glabrate, adaxial thinly arachnoid-tomentose, soon glabrescent;</text>
      <biological_entity id="o13867" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13868" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="broadly oblanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="40" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" constraint="to midveins" constraintid="o13869" from="1/2" name="quantity" src="d0_s3" to="2/3" />
        <character constraint="with sinuses" constraintid="o13870" is_modifier="false" name="size" notes="" src="d0_s3" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o13869" name="midvein" name_original="midveins" src="d0_s3" type="structure" />
      <biological_entity id="o13870" name="sinuse" name_original="sinuses" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o13871" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="main" id="o13872" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13873" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="gray-tomentose" value_original="gray-tomentose" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13874" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal present or withered at flowering, winged-petiolate;</text>
      <biological_entity id="o13875" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o13876" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character constraint="at flowering" is_modifier="false" name="life_cycle" src="d0_s4" value="withered" value_original="withered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>principal cauline sessile, progressively reduced distally, bases auriculate-clasping or shortly decurrent;</text>
      <biological_entity id="o13877" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="principal cauline" id="o13878" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o13879" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s5" value="auriculate-clasping" value_original="auriculate-clasping" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal cauline reduced, bractlike, often spinier than proximal.</text>
      <biological_entity id="o13880" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal cauline" id="o13881" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s6" value="bractlike" value_original="bractlike" />
        <character constraint="than proximal leaves" constraintid="o13882" is_modifier="false" name="architecture" src="d0_s6" value="often spinier" value_original="often spinier" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o13882" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Heads borne singly or few at branch tips, sometimes subtended by clustered, ± leafy bracts, collectively forming ± open, many-headed paniculiform arrays.</text>
      <biological_entity id="o13883" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character constraint="at branch tips" constraintid="o13884" is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" modifier="sometimes; collectively; more or less" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="many-headed" value_original="many-headed" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity constraint="branch" id="o13884" name="tip" name_original="tips" src="d0_s7" type="structure" />
      <biological_entity id="o13885" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s7" value="clustered" value_original="clustered" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
      </biological_entity>
      <relation from="o13883" id="r1272" modifier="sometimes" name="subtended by" negation="false" src="d0_s7" to="o13885" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0–10+ cm.</text>
      <biological_entity id="o13886" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres ovoid to campanulate, 1.5–2.5 × 1.5–3 cm, thinly arachnoid, glabrate.</text>
      <biological_entity id="o13887" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s9" to="campanulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s9" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s9" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s9" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 6–9 series, imbricate, dark green to brownish, lanceolate (outer) to linear (inner), abaxial faces with narrow glutinous ridge;</text>
      <biological_entity id="o13888" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s10" to="brownish" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="linear" />
      </biological_entity>
      <biological_entity id="o13889" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s10" to="9" />
      </biological_entity>
      <biological_entity id="o13891" name="ridge" name_original="ridge" src="d0_s10" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s10" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o13888" id="r1273" name="in" negation="false" src="d0_s10" to="o13889" />
      <relation from="o13890" id="r1274" name="with" negation="false" src="d0_s10" to="o13891" />
    </statement>
    <statement id="d0_s11">
      <text>outer and middle appressed, apices spreading, finely serrulate, spines slender, 1–2 mm;</text>
      <biological_entity constraint="abaxial" id="o13890" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o13892" name="whole-organism" name_original="" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="middle" value_original="middle" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o13893" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s11" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o13894" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>apices of inner erect, ± flexuous.</text>
      <biological_entity id="o13895" name="apex" name_original="apices" src="d0_s12" type="structure" constraint="erect; erect">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13896" name="erect" name_original="erect" src="d0_s12" type="structure" />
      <relation from="o13895" id="r1275" name="part_of" negation="false" src="d0_s12" to="o13896" />
    </statement>
    <statement id="d0_s13">
      <text>Corollas pale rose-purple, 18–23 mm, tubes 8–10 mm, throats 5–6 mm, lobes 5–7 mm;</text>
      <biological_entity id="o13897" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale rose-purple" value_original="pale rose-purple" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s13" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13898" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13899" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13900" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style tips 3.5–4.5 mm.</text>
      <biological_entity constraint="style" id="o13901" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae dark-brown to black, 4–5 mm, collars very narrow, stramineous;</text>
      <biological_entity id="o13902" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s15" to="black" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13903" name="collar" name_original="collars" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="very" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="stramineous" value_original="stramineous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi ca. 15 mm. 2n = 32.</text>
      <biological_entity id="o13904" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="15" value_original="15" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13905" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads usually 2.5–3 cm; cypselae ca. 5 mm, oblong; tidal marshes</description>
      <determination>35a Cirsium hydrophilum var. hydrophilum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads usually 3–3.5 cm; cypselae 4–5 mm, oblong or elliptic; serpentine springs</description>
      <determination>35b Cirsium hydrophilum var. vaseyi</determination>
    </key_statement>
  </key>
</bio:treatment>