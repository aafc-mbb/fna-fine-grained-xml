<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="S. F. Blake" date="1937" rank="species">allocotus</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>27: 379. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species allocotus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066547</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–18 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices multicipital or branched.</text>
      <biological_entity id="o10659" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o10660" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="multicipital" value_original="multicipital" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending (branched, brittle), hispido-hirsute (hairs brittle), minutely glandular.</text>
      <biological_entity id="o10661" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispido-hirsute" value_original="hispido-hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (often not persistent) and cauline;</text>
      <biological_entity id="o10662" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades spatulate 15–30 × 1.5–4 mm, cauline gradually reduced distally (reduced to bracts on peduncles), margins usually 3-lobed (lobes linear to oblong-oblanceolate, about as wide as central portion of blades), sometimes 2-ternate or (cauline) entire, faces sparsely hispido-hirsute, minutely glandular.</text>
      <biological_entity id="o10663" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o10664" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10665" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="2-ternate" value_original="2-ternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10666" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hispido-hirsute" value_original="hispido-hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually 2–4.</text>
      <biological_entity id="o10667" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 4–5 × 6–9 mm.</text>
      <biological_entity id="o10668" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, glabrous or sometimes sparsely hispid, densely minutely glandular.</text>
      <biological_entity id="o10669" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o10670" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o10669" id="r973" name="in" negation="false" src="d0_s7" to="o10670" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 20–40;</text>
      <biological_entity id="o10671" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white to bluish, sometimes drying pink, 3–6 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o10672" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="bluish" />
        <character is_modifier="false" modifier="sometimes" name="condition" src="d0_s9" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10673" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2.5–3.5 mm.</text>
      <biological_entity constraint="disc" id="o10674" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2–2.3 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o10675" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o10676" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 12–20 bristles.</text>
      <biological_entity id="o10677" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o10678" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o10679" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
      <relation from="o10677" id="r974" name="outer of" negation="false" src="d0_s12" to="o10678" />
      <relation from="o10677" id="r975" name="inner of" negation="false" src="d0_s12" to="o10679" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, calcareous sites on cliff faces, ledges, talus slopes, ridgetops, rock outcrops, barren redbeds, sometimes with Utah juniper, mountain mahogany, or sagebrush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="calcareous sites" constraint="on cliff" />
        <character name="habitat" value="cliff" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="ridgetops" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="barren redbeds" />
        <character name="habitat" value="utah juniper" modifier="sometimes with" />
        <character name="habitat" value="mountain mahogany" />
        <character name="habitat" value="sagebrush" modifier="or" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>58.</number>
  <other_name type="common_name">Bighorn fleabane</other_name>
  <discussion>The brittle, hispid vestiture, and multiple small heads (more than one per stem) with short rays of Erigeron allocotus are unusual among its putative relatives with 3-parted leaves.</discussion>
  
</bio:treatment>