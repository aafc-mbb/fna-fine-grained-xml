<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="treatment_page">107</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="Shinners" date="1964" rank="species">walteri</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>1: 253. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species walteri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067800</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Athanasia</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">paniculata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>201. 1788,</place_in_publication>
      <other_info_on_pub>not Verbesina paniculata Poiret 1808</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Athanasia;species paniculata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 100–400 cm (perennating bases ± erect, internodes winged, at least proximal).</text>
      <biological_entity id="o4495" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="400" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly alternate (proximal usually opposite);</text>
      <biological_entity id="o4496" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades lance-elliptic to lanceolate or lance-linear, 10–20 × 2–7 cm, bases ± cuneate, margins coarsely toothed to subentire, apices acute, faces ± hirtellous.</text>
      <biological_entity id="o4497" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lance-elliptic" name="shape" src="d0_s2" to="lanceolate or lance-linear" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4498" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4499" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="coarsely toothed" name="shape" src="d0_s2" to="subentire" />
      </biological_entity>
      <biological_entity id="o4500" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o4501" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 8–35+ in corymbiform to paniculiform arrays.</text>
      <biological_entity id="o4502" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o4503" from="8" name="quantity" src="d0_s3" to="35" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4503" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s3" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± saucerlike, 8–10 mm diam.</text>
      <biological_entity id="o4504" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="saucerlike" value_original="saucerlike" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 12–15+ in 1–2 series, ± spreading, oblanceolate to linear, 2–5+ mm.</text>
      <biological_entity id="o4505" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4506" from="12" name="quantity" src="d0_s5" to="15" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s5" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4506" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 0.</text>
      <biological_entity id="o4507" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 40–60+;</text>
      <biological_entity id="o4508" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s7" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas ochroleucous to white.</text>
      <biological_entity id="o4509" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="ochroleucous" name="coloration" src="d0_s8" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae dark-brown to blackish, obovate, 3–4.5+ mm, faces ± strigillose;</text>
      <biological_entity id="o4510" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s9" to="blackish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4511" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 1–2 mm (sometimes shorter scales between awns).</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 34.</text>
      <biological_entity id="o4512" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4513" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bottomlands, flood plains, borders of woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bottomlands" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="borders" constraint="of woodlands" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., La., N.C., Okla., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Verbesina virginica may be no longer present in Georgia and North Carolina.</discussion>
  
</bio:treatment>