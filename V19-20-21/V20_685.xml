<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">foliosus</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="1925" rank="variety">hartwegii</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>1056. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species foliosus;variety hartwegii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068339</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">hartwegii</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 21. 1895 (as hartwegi)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species hartwegii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves often mostly with 1-directional orientation to one side of stems;</text>
      <biological_entity id="o14407" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o14408" name="side" name_original="side" src="d0_s0" type="structure">
        <character is_modifier="true" name="character" src="d0_s0" value="orientation" value_original="orientation" />
      </biological_entity>
      <biological_entity id="o14409" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <relation from="o14407" id="r1320" name="with" negation="false" src="d0_s0" to="o14408" />
      <relation from="o14408" id="r1321" name="part_of" negation="false" src="d0_s0" to="o14409" />
    </statement>
    <statement id="d0_s1">
      <text>blades (30–) 40–60 × 1–4 mm, apices usually acute, faces glabrate to sparsely strigose.</text>
      <biological_entity id="o14410" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_length" src="d0_s1" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s1" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14411" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o14412" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s1" to="sparsely strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries 0.5–0.8 mm wide, margins usually thick or only with narrow scarious rims, midnerves usually not distinct and orange-resinous, abaxial faces glabrate to sparsely strigose, sometimes sparsely, obscurely glandular;</text>
      <biological_entity id="o14413" name="phyllarie" name_original="phyllaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14414" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="width" src="d0_s2" value="thick" value_original="thick" />
        <character name="width" src="d0_s2" value="only" value_original="only" />
      </biological_entity>
      <biological_entity id="o14415" name="rim" name_original="rims" src="d0_s2" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o14416" name="midnerve" name_original="midnerves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coating" src="d0_s2" value="orange-resinous" value_original="orange-resinous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14417" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s2" to="sparsely strigose" />
        <character is_modifier="false" modifier="sometimes sparsely; sparsely; obscurely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <relation from="o14414" id="r1322" name="with" negation="false" src="d0_s2" to="o14415" />
    </statement>
    <statement id="d0_s3">
      <text>inner phyllaries (4–) 5–6 mm.</text>
      <biological_entity constraint="inner" id="o14418" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 26–50;</text>
      <biological_entity id="o14419" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="26" name="quantity" src="d0_s4" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas 8–13 mm.</text>
      <biological_entity id="o14420" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc corollas 4–5.5 mm.</text>
      <biological_entity constraint="disc" id="o14421" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky sites, commonly along rivers, hills in oak or pine-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="hills" constraint="in oak" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>96d.</number>
  <discussion>Among the varieties of Erigeron foliosus, var. hartwegii is the most distinct, morphologically and geographically, and might justifiably be treated at specific rank.</discussion>
  
</bio:treatment>