<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="treatment_page">286</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1884" rank="species">poliospermus</taxon_name>
    <taxon_name authority="(Cronquist) G. L. Nesom" date="unknown" rank="variety">disciformis</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 24. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species poliospermus;variety disciformis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068361</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="forma disciformis Cronquist" date="unknown" rank="species">poliospermus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>6: 194. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species poliospermus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems simple, densely hirsute, sparsely minutely glandular;</text>
      <biological_entity id="o27927" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>basal leaves and stems originating from compressed nodes at caudex apices.</text>
      <biological_entity constraint="basal" id="o27928" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o27929" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o27930" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o27931" name="apex" name_original="apices" src="d0_s1" type="structure" />
      <relation from="o27928" id="r2592" name="originating from" negation="false" src="d0_s1" to="o27930" />
      <relation from="o27929" id="r2593" name="originating from" negation="false" src="d0_s1" to="o27930" />
      <relation from="o27930" id="r2594" name="at" negation="false" src="d0_s1" to="o27931" />
    </statement>
    <statement id="d0_s2">
      <text>Heads 1.</text>
      <biological_entity id="o27932" name="head" name_original="heads" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres densely hirsute, sparsely minutely glandular.</text>
      <biological_entity id="o27933" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 0.</text>
      <biological_entity id="o27934" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23a.</number>
  
</bio:treatment>