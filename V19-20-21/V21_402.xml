<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">146</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="treatment_page">166</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 324. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species nuttallii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066892</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 100–400 cm (rhizomatous).</text>
      <biological_entity id="o19130" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="400" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (usually yellowbrown or greenish, sometimes glaucous) erect, glabrous, hispid, ± hirsute, or scabrous.</text>
      <biological_entity id="o19131" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>all or mostly opposite to mostly alternate;</text>
      <biological_entity id="o19132" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly opposite-to-mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles 0.5–1.5 cm;</text>
      <biological_entity id="o19133" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades light to dark green, 3-nerved (distal to bases) ± lanceolate to ± ovate, 4–20 × 0.8–4 cm, bases cuneate, margins entire or ± serrate (flat), abaxial faces hispid to hispidulous or tomentulose to villous-tomentose, glanddotted.</text>
      <biological_entity id="o19134" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s5" to="dark green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="less lanceolate" name="shape" src="d0_s5" to="more or less ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19135" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19136" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19137" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s5" to="hispidulous or tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–6.</text>
      <biological_entity id="o19138" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 1–18 cm.</text>
      <biological_entity id="o19139" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric, 10–20 mm diam.</text>
      <biological_entity id="o19140" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 30–38 (loose, spreading), lanceolate to lanceovate, 8–16 × 1.5–3 mm, (margins ciliate) apices acute to acuminate, abaxial faces usually ± strigose, rarely glabrate, not glanddotted.</text>
      <biological_entity id="o19141" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s9" to="38" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="lanceovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19142" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19143" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 8–12 mm, 3-toothed.</text>
      <biological_entity id="o19144" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 10–21;</text>
      <biological_entity id="o19145" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 20–25 mm (abaxial faces not glanddotted).</text>
      <biological_entity id="o19146" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s12" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 60+;</text>
      <biological_entity id="o19147" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 5–7 mm, lobes yellow;</text>
      <biological_entity id="o19148" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19149" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dark-brown to black, appendages yellow.</text>
      <biological_entity id="o19150" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s15" to="black" />
      </biological_entity>
      <biological_entity id="o19151" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 3–5 mm, glabrate;</text>
      <biological_entity id="o19152" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 aristate scales 2.2–4.5 mm plus 0–2 lanceolate scales 0.5–1 mm. 2n = 34 (subspecies unknown).</text>
      <biological_entity id="o19153" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="2" />
      </biological_entity>
      <biological_entity id="o19154" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19155" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19156" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
      </biological_entity>
      <relation from="o19153" id="r1304" name="consist_of" negation="false" src="d0_s17" to="o19154" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Ont., Que., Sask.; Ariz., Calif., Colo., Idaho, Iowa, Kans., Minn., Mo., Mont., N.Dak., N.Mex., Nebr., Nev., Okla., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <other_name type="common_name">Nuttall’s sunflower</other_name>
  <other_name type="common_name">hélianthe de Nuttall</other_name>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: abaxial faces tomentulose to villous-tomentose; phyllaries densely hairy (longer hairs 1+ mm)</description>
      <determination>45c Helianthus nuttallii subsp. parishii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: abaxial faces hispid to hispidulous; phyllaries ± glabrate (longer hairs 0.5–1 mm).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants usually 200–300 cm; leaves mostly alternate, blades narrowly to broadly lanceolate,apices acute to acuminate</description>
      <determination>45a Helianthus nuttallii subsp. nuttallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants usually 100–250 cm; leaves all or mostly opposite, blades lanceolate to nearly ovate, apices acute to obtuse</description>
      <determination>45b Helianthus nuttallii subsp. rydbergii</determination>
    </key_statement>
  </key>
</bio:treatment>