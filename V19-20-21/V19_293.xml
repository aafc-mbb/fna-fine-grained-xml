<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="treatment_page">230</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="Coville" date="1896" rank="species">monticola</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>3: 562, plate 22. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species monticola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066447</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">occidentalis</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">crinita</taxon_name>
    <taxon_hierarchy>genus Crepis;species occidentalis;variety crinita;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–35 cm (taproots vertical, caudices simple or branched).</text>
      <biological_entity id="o2798" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3, erect (reddish-brown), stout, branched proximally, densely setose and stipitate-glandular (setae 1–3 mm).</text>
      <biological_entity id="o2799" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o2800" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades elliptic or oblanceolate, 10–25 × 2–4 cm, margins pinnately lobed or sharply serrate (lobes lanceolate, acuminate), apices acute, faces villous or coarsely setose, stipitate-glandular (cauline 2–3, bases clasping, margins dentate or serrate).</text>
      <biological_entity id="o2801" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2802" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o2803" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o2804" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s4" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 2–20, in loose cymiform arrays.</text>
      <biological_entity id="o2805" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o2806" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s5" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o2805" id="r276" name="in" negation="false" src="d0_s5" to="o2806" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 3–10, narrowly lanceolate to linear, densely setose and stipitate-glandular bractlets 3–5 mm.</text>
      <biological_entity id="o2807" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" modifier="of 3-10" name="shape" src="d0_s6" to="linear" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o2808" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindro-campanulate, 14–24 × 5–15 mm.</text>
      <biological_entity id="o2809" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s7" to="24" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 7–12, lanceolate, 14–20 mm (margins yellowish, not scarious), apices long-acuminate, abaxial faces densely and coarsely setose, adaxial glabrous or with fine, appressed, yellowish hairs.</text>
      <biological_entity id="o2810" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="12" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2811" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2812" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s8" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2813" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with fine , appressed , yellowish hairs" />
      </biological_entity>
      <biological_entity id="o2814" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="fine" value_original="fine" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <relation from="o2813" id="r277" name="with" negation="false" src="d0_s8" to="o2814" />
    </statement>
    <statement id="d0_s9">
      <text>Florets 16–20;</text>
      <biological_entity id="o2815" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 16–21 mm.</text>
      <biological_entity id="o2816" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s10" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae reddish-brown, fusiform, 5.5–9 mm, apices narrowed (not beaked), ribs 13;</text>
      <biological_entity id="o2817" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2818" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o2819" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi creamy white, 9–13 mm (outer bristles shorter and finer).</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 22, 33, 44, 55, 77, 88.</text>
      <biological_entity id="o2820" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="creamy white" value_original="creamy white" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2821" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="22" value_original="22" />
        <character name="quantity" src="d0_s13" value="33" value_original="33" />
        <character name="quantity" src="d0_s13" value="44" value_original="44" />
        <character name="quantity" src="d0_s13" value="55" value_original="55" />
        <character name="quantity" src="d0_s13" value="77" value_original="77" />
        <character name="quantity" src="d0_s13" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous forests, thickets, open woods, valleys and foothills, dry gravelly open areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="valleys" />
        <character name="habitat" value="foothills" />
        <character name="habitat" value="dry open areas" modifier="gravelly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Mountain hawksbeard</other_name>
  <discussion>Crepis monticola is recognized by the densely stipitate-glandular stems and leaves, and long-acuminate phyllaries.</discussion>
  
</bio:treatment>