<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="genus">thelesperma</taxon_name>
    <taxon_name authority="(Regel) S. F. Blake" date="1928" rank="species">burridgeanum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>41: 146. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus thelesperma;species burridgeanum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067738</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cosmidium</taxon_name>
    <taxon_name authority="Regel" date="unknown" rank="species">burridgeanum</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (St. Petersburg)</publication_title>
      <place_in_publication>1857: 40. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cosmidium;species burridgeanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 30–40 (–70+) cm.</text>
      <biological_entity id="o20862" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves crowded to ± scattered over proximal 1/2–3/4 of plant heights, internodes mostly 5–40 (–60+) mm;</text>
      <biological_entity constraint="cauline" id="o20863" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="over plant" constraintid="o20864" from="crowded" name="arrangement" src="d0_s1" to="more or less scattered" />
      </biological_entity>
      <biological_entity id="o20864" name="plant" name_original="plant" src="d0_s1" type="structure">
        <character is_modifier="true" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/2" is_modifier="true" name="quantity" src="d0_s1" to="3/4" />
      </biological_entity>
      <biological_entity id="o20865" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_heights" src="d0_s1" to="60" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="heights" src="d0_s1" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lobes mostly linear to filiform, sometimes oblanceolate, 5–25 (–45) × 0.5–1 (–2+) mm.</text>
      <biological_entity id="o20866" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s2" to="filiform" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="45" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="2" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Calyculi of 5–6 (–9) linear to narrowly triangular bractlets 2–4 mm (margins hispido-ciliate).</text>
      <biological_entity id="o20867" name="calyculus" name_original="calyculi" src="d0_s3" type="structure" />
      <biological_entity id="o20868" name="bractlet" name_original="bractlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s3" to="9" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s3" to="6" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s3" to="narrowly triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o20867" id="r1435" name="consist_of" negation="false" src="d0_s3" to="o20868" />
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 8;</text>
      <biological_entity id="o20869" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>laminae wholly redbrown to purplish or mostly redbrown with distal margins yellow to orange, 7–9 (–12+) mm.</text>
      <biological_entity id="o20870" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="with distal margins" constraintid="o20871" from="wholly redbrown" name="coloration" src="d0_s5" to="purplish or mostly redbrown" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s5" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20871" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s5" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc corollas redbrown to purplish, throats shorter than lobes.</text>
      <biological_entity constraint="disc" id="o20872" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="redbrown" name="coloration" src="d0_s6" to="purplish" />
      </biological_entity>
      <biological_entity id="o20873" name="throat" name_original="throats" src="d0_s6" type="structure">
        <character constraint="than lobes" constraintid="o20874" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o20874" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 3.5–4 mm;</text>
      <biological_entity id="o20875" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi 0.5–1+ mm.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 18.</text>
      <biological_entity id="o20876" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20877" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites on sands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" constraint="on sands" />
        <character name="habitat" value="sands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Thelesperma burridgeanum may be a color-form of T. filifolium.</discussion>
  
</bio:treatment>