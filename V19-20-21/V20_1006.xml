<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">440</other_info_on_meta>
    <other_info_on_meta type="illustration_page">441</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">isocoma</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1894" rank="species">acradenia</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>2: 111. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus isocoma;species acradenia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067007</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">acradenia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>10: 126. 1883 (as Bigelovia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bigelowia;species acradenia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Greene) S. F. Blake" date="unknown" rank="species">acradenius</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species acradenius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage usually glabrous, sometimes minutely hispidulous, often stipitate-glandular, often resinous.</text>
      <biological_entity id="o13497" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s0" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="often" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades oblanceolate to narrowly obovate, 20–150 mm, margins entire or toothed or lobed (teeth or lobes apically rounded-obtuse).</text>
      <biological_entity id="o13498" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="narrowly obovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="distance" src="d0_s1" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13499" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s1" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 5–8 × 5–7 mm.</text>
      <biological_entity id="o13500" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary apices yellowish to greenish yellow, sometimes spinulose-aristate, usually not gland-dotted, usually with single, strongly developed, subepidermal resin pocket nearly as wide as bracts, sometimes a central pocket and 2+ smaller, lateral ones (resin pockets sometimes formed from numerous, coalesced, sessile glands).</text>
      <biological_entity constraint="phyllary" id="o13501" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s3" to="greenish yellow" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="spinulose-aristate" value_original="spinulose-aristate" />
        <character is_modifier="false" modifier="usually not" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity constraint="resin" id="o13502" name="pocket" name_original="pocket" src="d0_s3" type="structure" constraint_original="subepidermal resin">
        <character is_modifier="true" name="quantity" src="d0_s3" value="single" value_original="single" />
        <character is_modifier="true" modifier="strongly" name="development" src="d0_s3" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o13503" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity constraint="central" id="o13504" name="pocket" name_original="pocket" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" upper_restricted="false" />
        <character is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
      </biological_entity>
      <relation from="o13501" id="r1227" modifier="usually" name="with" negation="false" src="d0_s3" to="o13502" />
      <relation from="o13502" id="r1228" name="as wide as" negation="false" src="d0_s3" to="o13503" />
    </statement>
    <statement id="d0_s4">
      <text>Florets 12–27;</text>
      <biological_entity id="o13505" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s4" to="27" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas 5–7 (–8) mm.</text>
      <biological_entity id="o13506" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypsela ribs not forming apical horns.</text>
      <biological_entity constraint="cypsela" id="o13507" name="rib" name_original="ribs" src="d0_s6" type="structure" />
      <biological_entity constraint="apical" id="o13508" name="horn" name_original="horns" src="d0_s6" type="structure" />
      <relation from="o13507" id="r1229" name="forming" negation="false" src="d0_s6" to="o13508" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Alkali jimmyweed</other_name>
  <discussion>isocoma acradenia is distinctive in its whitish stems and narrow, whitish-indurate phyllaries with an apical resin pocket (or pockets). the leaves often occur in axillary fascicles</discussion>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 50–150 mm; florets 20–27; phyllaryapices spinulose-aristate</description>
      <determination>1b Isocoma acradenia var. bracteosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 20–50 mm; florets 12–24; phyllary apices usually rounded, not spinulose, sometimes with weakly developed aristae</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves usually entire, proximal rarely toothed or lobed</description>
      <determination>1a Isocoma acradenia var. acradenia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves all toothed to pinnatifid</description>
      <determination>1c Isocoma acradenia var. eremophila</determination>
    </key_statement>
  </key>
</bio:treatment>