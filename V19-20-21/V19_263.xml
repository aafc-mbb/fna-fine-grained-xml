<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="treatment_page">212</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="Schreber" date="1791" rank="genus">vernonia</taxon_name>
    <taxon_name authority="Small" date="1903" rank="species">blodgettii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1160, 1338. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus vernonia;species blodgettii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067803</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–3 (–5+) dm.</text>
      <biological_entity id="o3342" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous or glabrate.</text>
      <biological_entity id="o3343" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o3344" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3345" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades lance-linear to linear, 3–7+ cm × 2–8+ mm, l/w = 7–15+, abaxially scabrellous (hairs appressed, awl-shaped), resin-gland-dotted, adaxially scabrellous, glabrescent, resin-gland-dotted.</text>
      <biological_entity id="o3346" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lance-linear" name="arrangement_or_course_or_shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="7" name="l/w" src="d0_s3" to="15" upper_restricted="false" />
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="resin-gland-dotted" value_original="resin-gland-dotted" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="resin-gland-dotted" value_original="resin-gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in open, corymbiform to paniculiform arrays.</text>
      <biological_entity id="o3347" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o3348" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s4" to="paniculiform" />
      </biological_entity>
      <relation from="o3347" id="r332" name="in" negation="false" src="d0_s4" to="o3348" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 12–35 mm.</text>
      <biological_entity id="o3349" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate to obconic, 6–8 × 6–10 mm.</text>
      <biological_entity id="o3350" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s6" to="obconic" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 30–40+ in 4–5 series, puberulent, margins arachno-ciliate, the outer lanceovate, 2–3 mm, inner lance-oblong to linear-oblong, 6–7 mm, tips acute or rounded-apiculate.</text>
      <biological_entity id="o3351" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3352" from="30" name="quantity" src="d0_s7" to="40" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3352" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o3353" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="arachno-ciliate" value_original="arachno-ciliate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3354" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceovate" value_original="lanceovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3355" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="lance-oblong" name="shape" src="d0_s7" to="linear-oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3356" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded-apiculate" value_original="rounded-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 18–25+.</text>
      <biological_entity id="o3357" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s8" to="25" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 2.5–3+ mm;</text>
      <biological_entity id="o3358" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi stramineous to whitish, outer scales 25–30, 0.5–1.1 mm, contrasting with 30–40+, 5–7+ mm inner bristles.</text>
      <biological_entity id="o3359" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s10" to="whitish" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3361" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s10" to="40" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s10" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <relation from="o3361" id="r333" name="contrasting with" negation="false" src="d0_s10" to="o3361" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 34.</text>
      <biological_entity constraint="outer" id="o3360" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s10" to="30" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3362" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, peaty or sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="peaty" modifier="damp" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Bahamas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  
</bio:treatment>