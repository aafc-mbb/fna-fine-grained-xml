<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">209</other_info_on_meta>
    <other_info_on_meta type="illustration_page">208</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaetopappa</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">asteroides</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">asteroides</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chaetopappa;species asteroides;variety asteroides</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068144</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Involucres 3–4 × 3–5 mm.</text>
      <biological_entity id="o9567" name="involucre" name_original="involucres" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s0" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s0" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ray-florets 5–13.</text>
      <biological_entity id="o9568" name="ray-floret" name_original="ray-florets" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Disc-florets 6–15.</text>
      <biological_entity id="o9569" name="disc-floret" name_original="disc-florets" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pappus-scales 0.1–0.8 mm. 2n = 16.</text>
      <biological_entity id="o9570" name="pappus-scale" name_original="pappus-scales" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="distance" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9571" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites in sandy, gravelly, or rocky soils over limestone, granite, or sandstone, less commonly on clay, with juniper, oak, or pine, glades, tallgrass prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="open" constraint="in sandy" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="rocky soils" modifier="or" constraint="over limestone" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="sandstone" modifier="or" />
        <character name="habitat" value="clay" modifier="less commonly on" />
        <character name="habitat" value="juniper" modifier="with" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine" modifier="or" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="tallgrass prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Kans., Mo., Okla., Tex.; Mexico (Tamaulipas, disjunct southward to Hidalgo).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (disjunct southward to Hidalgo)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  
</bio:treatment>