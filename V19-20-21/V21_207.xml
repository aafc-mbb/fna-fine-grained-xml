<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Moench" date="1794" rank="genus">echinacea</taxon_name>
    <taxon_name authority="(Nuttall) Nuttall" date="1840" rank="species">atrorubens</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 354. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus echinacea;species atrorubens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066488</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rudbeckia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">atrorubens</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 80. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rudbeckia;species atrorubens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 90 cm (roots elongate-turbinate, ± branched).</text>
      <biological_entity id="o17104" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage usually hairy (hairs appressed to ascending, spreading on adaxial leaf faces, to 1.2 mm), rarely glabrous.</text>
      <biological_entity id="o17105" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems light green to tan.</text>
      <biological_entity id="o17106" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s2" to="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petioles 0–12 (–20) cm;</text>
      <biological_entity constraint="basal" id="o17107" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17108" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (1-), 3-nerved, or 5-nerved, usually linear or lanceolate, rarely ovate, 5–30 × 0.5–3 cm, bases attenuate, margins usually entire.</text>
      <biological_entity constraint="basal" id="o17109" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17110" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="[1" value_original="[1" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-nerved" value_original="5-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-nerved" value_original="5-nerved" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17111" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o17112" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 20–50 cm.</text>
      <biological_entity id="o17113" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s5" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries linear to lanceolate, 6–15 × 1–3 mm.</text>
      <biological_entity id="o17114" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles: paleae 9–15 mm, tips red to orange-tipped, usually straight, sharp-pointed.</text>
      <biological_entity id="o17115" name="receptacle" name_original="receptacles" src="d0_s7" type="structure" />
      <biological_entity id="o17116" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17117" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="orange-tipped" value_original="orange-tipped" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sharp-pointed" value_original="sharp-pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray corollas purple, rarely pink or white, laminae reflexed, 19–35 × 2–7 mm, glabrous or sparsely hairy abaxially.</text>
      <biological_entity constraint="ray" id="o17118" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o17119" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s8" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Discs ovoid to conic, 25–35 × 20–40 mm.</text>
      <biological_entity id="o17120" name="disc" name_original="discs" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s9" to="conic" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s9" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 4.5–5.5 mm, lobes greenish to pink or purple.</text>
      <biological_entity constraint="disc" id="o17121" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17122" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s10" to="pink or purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae tan, 4–5 mm, faces finely tuberculate, glabrous;</text>
      <biological_entity id="o17123" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="tan" value_original="tan" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17124" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi to 1.2 mm (major teeth 3–4).</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 11.</text>
      <biological_entity id="o17125" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17126" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, limestone or sandstone outcrops, prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandstone outcrops" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Topeka purple coneflower</other_name>
  
</bio:treatment>