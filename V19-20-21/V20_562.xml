<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="treatment_page">278</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Greenman" date="1905" rank="species">oreophilus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>41: 257. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species oreophilus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066645</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Achaetogeron</taxon_name>
    <taxon_name authority="Larsen" date="unknown" rank="species">pringlei</taxon_name>
    <taxon_hierarchy>genus Achaetogeron;species pringlei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">delphiniifolius</taxon_name>
    <taxon_name authority="(Greenman) Cronquist" date="unknown" rank="variety">oreophilus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species delphiniifolius;variety oreophilus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (8–) 25–90 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices woody.</text>
      <biological_entity id="o12044" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o12045" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sparsely hirsuto-villous (hairs 0.6–2 mm) or glabrous, densely stipitate-glandular (at least on distal 1/2, glands relatively large, capitate).</text>
      <biological_entity id="o12046" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hirsuto-villous" value_original="hirsuto-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (sometimes withering by flowering) and cauline;</text>
      <biological_entity id="o12047" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades 15–60 × 8–37 mm, cauline gradually reduced distally, margins deeply pinnatifid, lobes 2–4 (–5) pairs, faces usually glabrous, stipitate-glandular.</text>
      <biological_entity constraint="basal" id="o12048" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="37" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o12049" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o12050" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o12051" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o12052" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (1–) 5–25 (–50) in loosely corymbiform arrays.</text>
      <biological_entity id="o12053" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="50" />
        <character char_type="range_value" constraint="in arrays" constraintid="o12054" from="5" name="quantity" src="d0_s5" to="25" />
      </biological_entity>
      <biological_entity id="o12054" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="loosely" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 4.5–5.5 × 8–12 mm.</text>
      <biological_entity id="o12055" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s6" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series, glabrous or sparsely hirsute, densely stipitate-glandular.</text>
      <biological_entity id="o12056" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o12057" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o12056" id="r1099" name="in" negation="false" src="d0_s7" to="o12057" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 75–130;</text>
      <biological_entity id="o12058" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="75" name="quantity" src="d0_s8" to="130" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, 8–14 mm, laminae reflexing.</text>
      <biological_entity id="o12059" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12060" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2.8–3.5 mm.</text>
      <biological_entity constraint="disc" id="o12061" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1–1.2 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o12062" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o12063" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner (readily falling) of (8–) 10–12 bristles.</text>
      <biological_entity id="o12065" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o12066" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="atypical_quantity" src="d0_s12" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
      <relation from="o12064" id="r1100" name="outer of" negation="false" src="d0_s12" to="o12065" />
      <relation from="o12064" id="r1101" name="inner of" negation="false" src="d0_s12" to="o12066" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o12064" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o12067" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky habitats, cliff ledges or crevices, grassland, chaparral, oak, pine, pine-fir</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="rocky habitats" />
        <character name="habitat" value="cliff ledges" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2800(–3100) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3100" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Durango, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Chaparral fleabane</other_name>
  <discussion>Erigeron oreophilus is similar to E. neomexicanus in its perennial habit, typically pinnatifid leaves, white, reflexing rays, and 10–12 readily falling pappus bristles; it has a vestiture of dense, viscid, stipitate glands and sparse, spreading, nonglandular hairs.</discussion>
  
</bio:treatment>