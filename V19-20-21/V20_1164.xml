<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="treatment_page">523</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Cronquist) G. L. Nesom" date="1995" rank="species">welshii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 294. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species welshii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067695</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Cronquist" date="unknown" rank="species">welshii</taxon_name>
    <place_of_publication>
      <publication_title>in A. Cronquist et al., Intermount. Fl.</publication_title>
      <place_in_publication>5: 291, fig. p. 293. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species welshii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–100 cm colonial;</text>
      <biological_entity id="o28812" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes shallow and long, often relatively thick (those of the season each producing a distal rosette near the parent plant).</text>
      <biological_entity id="o28813" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="depth" src="d0_s1" value="shallow" value_original="shallow" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" modifier="often relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–2, ascending to erect (sometimes lax, sometimes reddish), glabrous or sparsely strigoso-villosulous distally (arrays).</text>
      <biological_entity id="o28814" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="2" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s2" value="strigoso-villosulous" value_original="strigoso-villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (adaxially green, abaxially pale green, sometimes glaucous) thin (proximal) to stiff (distal), margins serrulate (proximal) or entire or nearly so, ± revolute, scabrous, apices mucronulate or sometimes callous-pointed, faces glabrous;</text>
      <biological_entity id="o28815" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o28816" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s3" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o28817" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronulate" value_original="mucronulate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="callous-pointed" value_original="callous-pointed" />
      </biological_entity>
      <biological_entity id="o28818" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering, petiolate (petioles narrowly winged, sheathing), blades oblanceolate to spatulate, 10–40+ × 4–9+ mm, bases attenuate, margins entire, apices obtuse;</text>
      <biological_entity constraint="basal" id="o28819" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, margins, apices" constraintid="o28820, o28821, o28822, o28823" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o28820" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o28821" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o28822" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o28823" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline withering by flowering, sessile or winged-petiolate, blades lanceolate to oblanceolate, 50–150 × 8–11 mm, bases slightly clasping, margins sparsely serrulate, apices acute;</text>
      <biological_entity constraint="proximal cauline" id="o28824" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades, bases, margins, apices" constraintid="o28825, o28826, o28827, o28828" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o28825" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28826" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28827" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28828" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades linear to linear-lanceolate, 50–130 × 4–6 mm, gradually reduced distally, bases ± clasping, apices acute.</text>
      <biological_entity constraint="distal" id="o28829" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o28830" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s6" to="130" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o28831" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o28832" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads in open, slender, leafy, paniculiform or racemiform arrays, branches ascending.</text>
      <biological_entity id="o28833" name="head" name_original="heads" src="d0_s7" type="structure" />
      <biological_entity id="o28834" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="true" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <biological_entity id="o28835" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o28833" id="r2667" name="in" negation="false" src="d0_s7" to="o28834" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0.5–2 cm, scabro-villosulous, bracts linear, scabro-ciliolate.</text>
      <biological_entity id="o28836" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="scabro-villosulous" value_original="scabro-villosulous" />
      </biological_entity>
      <biological_entity id="o28837" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="scabro-ciliolate" value_original="scabro-ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindro-campanulate, 4–7 mm.</text>
      <biological_entity id="o28838" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, lanceolate (outer) to linear (inner), ± unequal, bases indurate, margins hyaline, erose, distally scabro-ciliolate to ciliolate, green zones ± foliaceous (outer) or lanceolate, apices acute to acuminate, sometimes purplish, faces glabrous.</text>
      <biological_entity id="o28839" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s10" to="linear" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o28840" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o28841" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o28842" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
        <character char_type="range_value" from="distally scabro-ciliolate" name="pubescence" src="d0_s10" to="ciliolate" />
      </biological_entity>
      <biological_entity id="o28843" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s10" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o28844" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o28845" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o28839" id="r2668" name="in" negation="false" src="d0_s10" to="o28840" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 18–25;</text>
      <biological_entity id="o28846" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s11" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas pink to white, laminae 9–12 × 0.8–1.5 mm.</text>
      <biological_entity id="o28847" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s12" to="white" />
      </biological_entity>
      <biological_entity id="o28848" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 23–60;</text>
      <biological_entity id="o28849" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="23" name="quantity" src="d0_s13" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas pale-yellow, 3.8–6.4 mm, tubes shorter than funnelform throats, lobes narrowly triangular, 0.4–0.8 mm.</text>
      <biological_entity id="o28850" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s14" to="6.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28851" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o28852" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o28852" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o28853" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae pinkish tan (nerves stramineous), obovoid, ± compressed, 0.8–1.6 mm, 3–5-nerved, faces sparsely strigillose;</text>
      <biological_entity id="o28854" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="pinkish tan" value_original="pinkish tan" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-5-nerved" value_original="3-5-nerved" />
      </biological_entity>
      <biological_entity id="o28855" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi white, 2.8–5.3 mm. 2n = 16.</text>
      <biological_entity id="o28856" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s16" to="5.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28857" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soils in dry areas, hanging gardens, seeps, wet ledges, stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soils" constraint="in dry areas" />
        <character name="habitat" value="dry areas" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="wet ledges" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Idaho, Mont., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="common_name">Welsh’s aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>