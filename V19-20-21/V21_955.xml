<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">380</other_info_on_meta>
    <other_info_on_meta type="illustration_page">379</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="A. Gray in War Department [U.S.]" date="1857" rank="genus">syntrichopappus</taxon_name>
    <taxon_name authority="A. Gray in War Department [U.S.]" date="1857" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 106, plate 15. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus syntrichopappus;species fremontii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220013207</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems decumbent or erect.</text>
      <biological_entity id="o22237" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blades narrowly cuneate to spatulate, 5–20 mm, margins usually distally 3-lobed, sometimes entire (revolute).</text>
      <biological_entity id="o22238" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o22239" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="narrowly cuneate" name="shape" src="d0_s1" to="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22240" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually distally" name="shape" src="d0_s1" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 3–25 mm.</text>
      <biological_entity id="o22241" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 5–7 mm.</text>
      <biological_entity id="o22242" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries ± 5.</text>
      <biological_entity id="o22243" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Rays yellow, laminae 3–5 mm.</text>
      <biological_entity id="o22244" name="ray" name_original="rays" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o22245" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 2–3.5 mm, sparsely to densely hispidulous;</text>
      <biological_entity id="o22246" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s6" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi of 30–40 bristles ± 2 mm. 2n = 12.</text>
      <biological_entity id="o22247" name="pappus" name_original="pappi" src="d0_s7" type="structure" />
      <biological_entity id="o22248" name="bristle" name_original="bristles" src="d0_s7" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s7" to="40" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22249" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="12" value_original="12" />
      </biological_entity>
      <relation from="o22247" id="r1528" name="consist_of" negation="false" src="d0_s7" to="o22248" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly openings in deserts, creosote-bush or sagebrush scrublands, or Joshua Tree or pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" constraint="in deserts , creosote-bush or sagebrush scrublands , or joshua tree or pinyon-juniper woodlands" />
        <character name="habitat" value="gravelly openings" constraint="in deserts , creosote-bush or sagebrush scrublands , or joshua tree or pinyon-juniper woodlands" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="creosote-bush" />
        <character name="habitat" value="sagebrush scrublands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Yellowray Fremont’s-gold</other_name>
  <other_name type="common_name">yellow syntrichopappus</other_name>
  
</bio:treatment>