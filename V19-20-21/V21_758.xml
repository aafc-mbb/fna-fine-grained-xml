<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">hymenopappinae</taxon_name>
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="species">filifolius</taxon_name>
    <taxon_name authority="(Greene) B. L. Turner" date="1956" rank="variety">parvulus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>58: 216. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe hymenopappinae;genus hymenopappus;species filifolius;variety parvulus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068518</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="Greene" date="1901" rank="species">parvulus</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Baker</publication_title>
      <place_in_publication>3: 30. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenopappus;species parvulus</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 10–25 cm.</text>
      <biological_entity id="o13231" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal axils glabrous or sparsely tomentose, terminal lobes 3–20 mm;</text>
      <biological_entity id="o13232" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o13233" name="axil" name_original="axils" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o13234" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 0–1 (–2).</text>
      <biological_entity id="o13235" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o13236" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="2" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 3–10 (–15).</text>
      <biological_entity id="o13237" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="15" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–2 cm.</text>
      <biological_entity id="o13238" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 4–8 mm.</text>
      <biological_entity id="o13239" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Florets 10–20;</text>
      <biological_entity id="o13240" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas yellowish, 2–3 mm, throats 1.5–1.8 mm, lengths 2–3 times lobes;</text>
      <biological_entity id="o13241" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13242" name="throat" name_original="throats" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.8" to_unit="mm" />
        <character constraint="lobe" constraintid="o13243" is_modifier="false" name="length" src="d0_s7" value="2-3 times lobes" value_original="2-3 times lobes" />
      </biological_entity>
      <biological_entity id="o13243" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>anthers 2 mm.</text>
      <biological_entity id="o13244" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 3.5–4 mm, hairs 1 mm;</text>
      <biological_entity id="o13245" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13246" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 0.6–1.8 mm. 2n = 34.</text>
      <biological_entity id="o13247" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13248" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sands, gravels</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sands" />
        <character name="habitat" value="gravels" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  
</bio:treatment>