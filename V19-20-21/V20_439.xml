<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">200</other_info_on_meta>
    <other_info_on_meta type="illustration_page">198</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="genus">townsendia</taxon_name>
    <taxon_name authority="(Richardson) Porter" date="1894" rank="species">exscapa</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Torrey Bot. Club</publication_title>
      <place_in_publication>5: 321. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus townsendia;species exscapa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067761</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Richardson" date="unknown" rank="species">exscapus</taxon_name>
    <place_of_publication>
      <publication_title>in J. Franklin et al., Narr. Journey Polar Sea,</publication_title>
      <place_in_publication>748. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species exscapus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Townsendia</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">sericea</taxon_name>
    <taxon_hierarchy>genus Townsendia;species sericea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 1–3 cm (± pulvinate).</text>
      <biological_entity id="o4261" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect;</text>
      <biological_entity id="o4262" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 0.1–1 mm, ± hirsute to strigose or glabrate.</text>
      <biological_entity id="o4263" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character char_type="range_value" from="less hirsute" name="pubescence" src="d0_s2" to="strigose or glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, blades ± spatulate to narrowly oblanceolate or linear, 12–40 (–75) × 2–6+ mm, not fleshy, faces ± strigillose.</text>
      <biological_entity id="o4264" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o4265" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="less spatulate" name="shape" src="d0_s3" to="narrowly oblanceolate or linear" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="75" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o4266" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads ± sessile (usually nestled among rosette leaves).</text>
      <biological_entity id="o4267" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric, (10–) 12–25+ mm diam.</text>
      <biological_entity id="o4268" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s5" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s5" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 30–45 (–60) in 4–6+ series, the longer narrowly lanceolate or linear to subulate, (10–) 12–14 (–17) mm (l/w = 6–9), apices obtuse to acute, abaxial faces sparsely strigillose or glabrate.</text>
      <biological_entity id="o4269" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="60" />
        <character char_type="range_value" constraint="in series" constraintid="o4270" from="30" name="quantity" src="d0_s6" to="45" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="subulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4270" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4271" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4272" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 11–40;</text>
      <biological_entity id="o4273" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white adaxially, laminae (9–) 12–18+ mm, glabrous abaxially.</text>
      <biological_entity id="o4274" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o4275" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 100–150+;</text>
      <biological_entity id="o4276" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s9" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 6.5–9 (–12+) mm.</text>
      <biological_entity id="o4277" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 4 (–6+) mm, faces hairy, hair tips glochidiform;</text>
      <biological_entity id="o4278" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="6" to_unit="mm" upper_restricted="false" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o4279" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="hair" id="o4280" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="glochidiform" value_original="glochidiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent;</text>
    </statement>
    <statement id="d0_s13">
      <text>on ray cypselae 20–30 setiform scales 6–8+ mm;</text>
      <biological_entity id="o4281" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>on disc cypselae 20–30+ setiform scales 6.5–11+ mm.</text>
      <biological_entity id="o4282" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o4283" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4284" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in pine forests, meadows, gravelly hills, sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in pine forests" />
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="gravelly hills" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1300–)1600–2300(–3300) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Ariz., Colo., Idaho, Kans., Mont., Nebr., Nev., N.Mex., N.Dak., Okla., S.Dak., Tex., Utah, Wyo.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  
</bio:treatment>