<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">402</other_info_on_meta>
    <other_info_on_meta type="treatment_page">404</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(R. L. Hartman) D. R. Morgan &amp; R. L. Hartman" date="2003" rank="genus">arida</taxon_name>
    <taxon_name authority="(A. Gray) D. R. Morgan &amp; R. L. Hartman" date="2003" rank="species">carnosa</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 1413. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus arida;species carnosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066102</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linosyris</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">carnosa</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 80. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Linosyris;species carnosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucosyris</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">carnosa</taxon_name>
    <taxon_hierarchy>genus Leucosyris;species carnosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucosyris</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="unknown" rank="species">carnosa</taxon_name>
    <taxon_name authority="(A. Gray) Cronquist" date="unknown" rank="variety">intricata</taxon_name>
    <taxon_hierarchy>genus Leucosyris;species carnosa;variety intricata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">carnosa</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species carnosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">carnosa</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="unknown" rank="variety">intricata</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species carnosa;variety intricata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 10–90 cm (bushy);</text>
      <biological_entity id="o16857" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes elongate.</text>
      <biological_entity id="o16858" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, suberect to reclining or sprawling, slender, repeatedly branched throughout, divaricately so, glabrous, glaucous.</text>
      <biological_entity id="o16859" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character char_type="range_value" from="suberect" name="orientation" src="d0_s2" to="reclining or sprawling" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="repeatedly; throughout" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="divaricately" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o16860" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal blades linear, 10–20 × 2–3 mm, distal greatly reduced, scalelike, (succulent) bases tapered, margins entire, apices apiculate to spinose, faces glabrous.</text>
      <biological_entity constraint="proximal" id="o16861" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16862" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o16863" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o16864" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16865" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="apiculate" name="architecture_or_shape" src="d0_s5" to="spinose" />
      </biological_entity>
      <biological_entity id="o16866" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly (terminal), in diffuse, bracteate, cymiform arrays.</text>
      <biological_entity id="o16867" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o16868" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o16867" id="r1565" name="in" negation="false" src="d0_s6" to="o16868" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres 5–7 × 4–7 mm in flower.</text>
      <biological_entity id="o16869" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" constraint="in flower" constraintid="o16870" from="4" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16870" name="flower" name_original="flower" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 4–5 series, appressed or spreading, lanceolate, 2–8 mm, bases whitish to tan or tan throughout, margins entire to laciniate, apices often green, obtuse to acuminate, faces glabrous.</text>
      <biological_entity id="o16871" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16872" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o16873" name="base" name_original="bases" src="d0_s8" type="structure">
        <character char_type="range_value" from="whitish" modifier="throughout" name="coloration" src="d0_s8" to="tan or tan" />
      </biological_entity>
      <biological_entity id="o16874" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s8" to="laciniate" />
      </biological_entity>
      <biological_entity id="o16875" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity id="o16876" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o16871" id="r1566" name="in" negation="false" src="d0_s8" to="o16872" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0.</text>
      <biological_entity id="o16877" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 12–25+;</text>
      <biological_entity id="o16878" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s10" to="25" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 5–6 mm.</text>
      <biological_entity id="o16879" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae subcylindric, 2.5–3 mm, 7–9-nerved per face, faces sparsely sericeous;</text>
      <biological_entity id="o16880" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character constraint="per face" constraintid="o16881" is_modifier="false" name="architecture" src="d0_s12" value="7-9-nerved" value_original="7-9-nerved" />
      </biological_entity>
      <biological_entity id="o16881" name="face" name_original="face" src="d0_s12" type="structure" />
      <biological_entity id="o16882" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi whitish, setose, 4–6 mm. 2n = 10.</text>
      <biological_entity id="o16883" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="setose" value_original="setose" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16884" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline flats, canyons, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline flats" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Shrubby alkali tansy-aster</other_name>
  <discussion>Arida carnosa is recognized by its subshrubby habit, glabrous, glaucous, and succulent stems, reduced scalelike leaves, and absence of ray florets. It occurs in or around alkaline salt flats, where it is associated with other salt-tolerant genera such as Sarcobatus, Distichlis, and Atriplex.</discussion>
  
</bio:treatment>