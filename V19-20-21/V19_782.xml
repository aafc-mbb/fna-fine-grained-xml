<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">469</other_info_on_meta>
    <other_info_on_meta type="treatment_page">468</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1868" rank="genus">hesperevax</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1897" rank="species">sparsiflora</taxon_name>
    <taxon_name authority="(A. Gray) Morefield" date="1992" rank="variety">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>17: 302. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus hesperevax;species sparsiflora;variety brevifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068471</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Evax</taxon_name>
    <taxon_name authority="(Bentham) A. Gray" date="unknown" rank="species">caulescens</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 229. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Evax;species caulescens;variety brevifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Evax</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="species">sparsiflora</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="variety">brevifolia</taxon_name>
    <taxon_hierarchy>genus Evax;species sparsiflora;variety brevifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± greenish, mostly 3–9 cm.</text>
      <biological_entity id="o5906" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="9" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline, largest 6–12 (–14) × 3–5 (–6) mm;</text>
      <biological_entity id="o5907" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="size" src="d0_s1" value="largest" value_original="largest" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="14" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s1" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ± round, ± densely lanuginose.</text>
      <biological_entity id="o5908" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="round" value_original="round" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s2" value="lanuginose" value_original="lanuginose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads: longest 3–3.7 mm.</text>
      <biological_entity id="o5909" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="longest" value_original="longest" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="3.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pistillate paleae 2.5–3.7 mm.</text>
      <biological_entity id="o5910" name="palea" name_original="paleae" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="3.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae mostly 1–1.7 mm.</text>
      <biological_entity id="o5911" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late Mar–early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="late Mar" />
        <character name="fruiting time" char_type="range_value" to="early Jul" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, grassy or wooded coastal bluffs, terraces, dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="grassy" />
        <character name="habitat" value="wooded coastal bluffs" />
        <character name="habitat" value="terraces" />
        <character name="habitat" value="dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Seaside or short-leaved evax</other_name>
  <discussion>Variety brevifolia occurs in a coastal band from the San Francisco Bay area to southwestern Oregon. It is of potential conservation concern in both states. See discussion under Hesperevax acaulis var. robustior.</discussion>
  
</bio:treatment>