<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Eric E. Lamont</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="treatment_page">488</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="genus">SCLEROLEPIS</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Sci. Soc. Philom. Paris</publication_title>
      <place_in_publication>1816: 198. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus SCLEROLEPIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek scleros, hard, and lepis, scale, alluding to pappus</other_info_on_name>
    <other_info_on_name type="fna_id">129790</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–30 (–60) cm (rhizomatous; aquatic to subaquatic).</text>
      <biological_entity id="o3546" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent or erect (sometimes floating), simple or branched distal to bases (terete, glabrous; internodes usually shorter than leaves).</text>
      <biological_entity id="o3547" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="to bases" constraintid="o3548" is_modifier="false" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o3548" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>whorled (4–6 per node);</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o3549" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-nerved, linear, margins entire (apices callus-tipped), faces glabrous or sparsely glanddotted.</text>
      <biological_entity id="o3550" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o3551" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3552" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s5" value="sparsely" value_original="sparsely" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, borne singly (peduncles ebracteate).</text>
      <biological_entity id="o3553" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate to subhemispheric, ca. 10 mm diam.</text>
      <biological_entity id="o3554" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="subhemispheric" />
        <character name="diameter" src="d0_s7" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 22–30+ in 2–3+ series, not notably nerved, linear to broadly lanceolate, unequal to ± equal (glabrous).</text>
      <biological_entity id="o3555" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o3556" from="22" name="quantity" src="d0_s8" to="30" upper_restricted="false" />
        <character is_modifier="false" modifier="not notably" name="architecture" notes="" src="d0_s8" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="broadly lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o3556" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic or hemispheric, epaleate.</text>
      <biological_entity id="o3557" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets ca. 50;</text>
      <biological_entity id="o3558" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="50" value_original="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas pink-lavender to flesh-colored, throats funnelform (sparsely stipitate-glandular), lobes 5, deltate;</text>
      <biological_entity id="o3559" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink-lavender" name="coloration" src="d0_s11" to="flesh-colored" />
      </biological_entity>
      <biological_entity id="o3560" name="throat" name_original="throats" src="d0_s11" type="structure" />
      <biological_entity id="o3561" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s11" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles: bases not enlarged, glabrous, branches filiform to linear-clavate.</text>
      <biological_entity id="o3562" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o3563" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3564" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s12" to="linear-clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae prismatic, 5-ribbed, glanddotted;</text>
      <biological_entity id="o3565" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="5-ribbed" value_original="5-ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi persistent, of 5 muticous scales.</text>
      <biological_entity id="o3567" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="true" name="shape" src="d0_s14" value="muticous" value_original="muticous" />
      </biological_entity>
      <relation from="o3566" id="r275" name="consist_of" negation="false" src="d0_s14" to="o3567" />
    </statement>
    <statement id="d0_s15">
      <text>x = 15.</text>
      <biological_entity id="o3566" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3568" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>401.</number>
  <other_name type="common_name">Bogbutton</other_name>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Crow, G. E. and C. B. Hellquist. 2000. Aquatic and Wetland Plants of Northeastern North America.... 2 vols. Madison. Vol. 1, pp. 414–415.</reference>
    <reference>Mehrhoff, L. J. 1983. Sclerolepis uniflora (Compositae) in New England. Rhodora 85: 433–438.</reference>
  </references>
  
</bio:treatment>