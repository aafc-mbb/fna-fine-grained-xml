<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="treatment_page">523</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Cody) Semple in J. C. Semple et al." date="2002" rank="species">nahanniense</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Semple et al., Cult. Native Asters Ontario,</publication_title>
      <place_in_publication>134. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species nahanniense</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067661</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Cody" date="unknown" rank="species">nahanniensis</taxon_name>
    <place_of_publication>
      <publication_title>Naturaliste Canad.</publication_title>
      <place_in_publication>101: 888. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species nahanniensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 7–39 cm, cespitose;</text>
    </statement>
    <statement id="d0_s1">
      <text>slender, woody, short-rhizomatous (rhizomes of season shallow or deep-seated, not producing rosettes near parent stems).</text>
      <biological_entity id="o12068" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="39" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1, ascending to decumbent (slender, green to reddish-brown), glabrate or sparsely short woolly-pilose in lines or zones, more densely so distally.</text>
      <biological_entity id="o12069" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="short" value_original="short" />
        <character constraint="in zones" constraintid="o12071" is_modifier="false" name="pubescence" src="d0_s2" value="woolly-pilose" value_original="woolly-pilose" />
      </biological_entity>
      <biological_entity id="o12070" name="line" name_original="lines" src="d0_s2" type="structure" />
      <biological_entity id="o12071" name="zone" name_original="zones" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves (light green, abaxial sometimes becoming much darker) thin to firm, margins entire, scabrous, apices mucronulate, faces glabrous;</text>
      <biological_entity id="o12072" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o12073" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12074" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o12075" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal persistent or withering by flowering, petiolate, blades oblanceolate to short-spatulate, 10–130 × 2–8 mm, bases cuneate to attenuate, margins rarely sparsely serrulate, apices obtuse, faces glabrous;</text>
      <biological_entity constraint="basal" id="o12076" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character constraint="by blades, bases, margins, apices, faces" constraintid="o12077, o12078, o12079, o12080, o12081" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o12077" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="short-spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="rarely sparsely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12078" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="short-spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="rarely sparsely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12079" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="short-spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="rarely sparsely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12080" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="short-spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="rarely sparsely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12081" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="short-spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="rarely sparsely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline persistent or withering by flowering, sessile, blades oblanceolate to linear-lanceolate, 13–60 × 2–6 mm, bases ± clasping, apices acute;</text>
      <biological_entity constraint="proximal cauline" id="o12082" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character constraint="by blades, bases, apices" constraintid="o12083, o12084, o12085" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o12083" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o12084" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o12085" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades linear to linear-lanceolate, 10–36 × 1–4 mm, reduced distally, bases subauriculate, subclasping to ± clasping.</text>
      <biological_entity constraint="distal" id="o12086" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o12087" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="36" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o12088" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subauriculate" value_original="subauriculate" />
        <character char_type="range_value" from="subclasping" name="architecture" src="d0_s6" to="more or less clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads usually borne singly or in open, paniculiform arrays (sometimes terminating lateral branches arising from decumbent stems or from proximal stem branches), branches few.</text>
      <biological_entity id="o12089" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="in open , paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o12090" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o12091" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
      </biological_entity>
      <relation from="o12089" id="r1102" name="in" negation="false" src="d0_s7" to="o12090" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 10–70 mm, sparsely to moderately short-strigose, bracts 0–10, lanceolate, often foliaceous, reduced distally.</text>
      <biological_entity id="o12092" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="70" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s8" value="short-strigose" value_original="short-strigose" />
      </biological_entity>
      <biological_entity id="o12093" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s8" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindro-campanulate, 4.7–7.4 mm.</text>
      <biological_entity id="o12094" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="4.7" from_unit="mm" name="some_measurement" src="d0_s9" to="7.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, outer spatulate, inner lanceolate, ± unequal to subequal, bases indurate, margins reddish, papery, erose-ciliate, green zones lanceolate, apices acute, faces glabrous.</text>
      <biological_entity id="o12095" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity id="o12096" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o12097" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o12098" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="less unequal" name="size" src="d0_s10" to="subequal" />
      </biological_entity>
      <biological_entity id="o12099" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o12100" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="texture" src="d0_s10" value="papery" value_original="papery" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="erose-ciliate" value_original="erose-ciliate" />
      </biological_entity>
      <biological_entity id="o12101" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o12102" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o12103" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12095" id="r1103" name="in" negation="false" src="d0_s10" to="o12096" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 15–41;</text>
      <biological_entity id="o12104" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="41" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas white to pale rose, often becoming rose-violet, laminae 5–13 × 0.7–1.8 mm.</text>
      <biological_entity id="o12105" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="pale rose" />
        <character is_modifier="false" modifier="often becoming" name="coloration" src="d0_s12" value="rose-violet" value_original="rose-violet" />
      </biological_entity>
      <biological_entity id="o12106" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 20–60;</text>
      <biological_entity id="o12107" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow turning reddish with age, 3.7–6.6 mm, tubes shorter than funnelform throats, lobes erect to spreading, lanceolate, 0.4–0.8 mm.</text>
      <biological_entity id="o12108" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character constraint="with age" constraintid="o12109" is_modifier="false" name="coloration" src="d0_s14" value="yellow turning reddish" value_original="yellow turning reddish" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="6.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12109" name="age" name_original="age" src="d0_s14" type="structure" />
      <biological_entity id="o12110" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o12111" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o12111" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o12112" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s14" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae tan (nerves stramineous), obovoid, compressed, 2–3 mm, 3–5-nerved, faces strigillose;</text>
      <biological_entity id="o12113" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-5-nerved" value_original="3-5-nerved" />
      </biological_entity>
      <biological_entity id="o12114" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi white, 4–6.5 mm. 2n = 16.</text>
      <biological_entity id="o12115" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12116" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks near hot mineral springs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" constraint="near hot mineral springs" />
        <character name="habitat" value="hot mineral springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>± 1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="1000" from_unit="m" constraint="± " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <other_name type="common_name">Nahanni aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Symphyotrichum nahanniense is known only from the South Nahanni River (Mackenzie Mountains), Nahanni National Park.</discussion>
  
</bio:treatment>