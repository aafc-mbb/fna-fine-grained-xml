<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="treatment_page">1</other_info_on_meta>
    <other_info_on_meta type="illustration_page">145</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">scariosum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 420. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species scariosum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066401</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">hookerianum</taxon_name>
    <taxon_name authority="(Nuttall) B. Boivin" date="unknown" rank="variety">scariosum</taxon_name>
    <taxon_hierarchy>genus Cirsium;species hookerianum;variety scariosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or monocarpic perennials, acaulescent, short caulescent and forming low rounded mounds, or caulescent and erect, 0–200 cm;</text>
      <biological_entity id="o7526" name="mound" name_original="mounds" src="d0_s0" type="structure">
        <character is_modifier="true" name="position" src="d0_s0" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o7524" id="r691" name="forming" negation="false" src="d0_s0" to="o7526" />
      <relation from="o7524" id="r692" name="forming" negation="false" src="d0_s0" to="o7526" />
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o7524" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems absent, or with crowded branches from near base, or simple and erect, often fleshy and thickened, glabrous to thinly gray tomentose, often villous with septate trichomes.</text>
      <biological_entity id="o7527" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="thinly gray tomentose" />
        <character constraint="with trichomes" constraintid="o7530" is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o7528" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o7529" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o7530" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="septate" value_original="septate" />
      </biological_entity>
      <relation from="o7527" id="r693" name="with" negation="false" src="d0_s2" to="o7528" />
      <relation from="o7528" id="r694" name="from" negation="false" src="d0_s2" to="o7529" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blades linear to elliptic, 5–20 × 3–7 cm, plane to strongly undulate, unlobed or shallowly to deeply pinnatifid, lobes linear-lanceolate to broadly triangular, closely spaced, spreading, spinose-dentate or lobed, main spines slender to stout, 2–15+ mm, abaxial faces glabrous or thinly to densely tomentose, ± villous with septate trichomes along the veins, glabrate or trichomes persistent, adaxial thinly arachnoid tomentose and soon glabrescent;</text>
      <biological_entity id="o7531" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7532" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="linear to elliptic" value_original="linear to elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="plane" name="shape" src="d0_s3" to="strongly undulate unlobed or" />
        <character char_type="range_value" from="plane" name="shape" src="d0_s3" to="strongly undulate unlobed or" />
      </biological_entity>
      <biological_entity id="o7533" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="broadly triangular" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spinose-dentate" value_original="spinose-dentate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="main" id="o7534" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s3" to="stout" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7535" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s3" to="thinly densely tomentose" />
        <character constraint="with trichomes" constraintid="o7536" is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o7536" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o7537" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity id="o7538" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7539" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="linear to elliptic" value_original="linear to elliptic" />
      </biological_entity>
      <relation from="o7536" id="r695" name="along" negation="false" src="d0_s3" to="o7537" />
    </statement>
    <statement id="d0_s4">
      <text>basal often present at flowering, sessile or winged-petiolate;</text>
      <biological_entity id="o7540" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o7541" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="often" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline many in caulescent forms, reduced distally or not, winged-petiolate or distal sessile;</text>
      <biological_entity id="o7542" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o7543" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="in forms" constraintid="o7544" is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
        <character is_modifier="false" modifier="distally; distally; not" name="size" notes="" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
      <biological_entity id="o7544" name="form" name_original="forms" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="caulescent" value_original="caulescent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7545" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal often well developed, similar to proximal, sometimes much narrower and bractlike.</text>
      <biological_entity id="o7546" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o7547" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often well" name="development" src="d0_s6" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="sometimes much" name="width" notes="" src="d0_s6" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="shape" src="d0_s6" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7548" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o7547" id="r696" name="to" negation="false" src="d0_s6" to="o7548" />
    </statement>
    <statement id="d0_s7">
      <text>Heads 1–many, erect, borne singly or often densely crowded in spiciform, racemiform, or subcapitate arrays, especially in acaulescent or short-caulescent plants, often closely subtended by distalmost leaves.</text>
      <biological_entity id="o7549" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s7" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character constraint="in arrays" constraintid="o7550" is_modifier="false" modifier="often densely" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o7550" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="subcapitate" value_original="subcapitate" />
      </biological_entity>
      <biological_entity id="o7551" name="plant" name_original="plants" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="short-caulescent" value_original="short-caulescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o7552" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o7549" id="r697" modifier="especially" name="in" negation="false" src="d0_s7" to="o7551" />
      <relation from="o7549" id="r698" modifier="often closely; closely" name="subtended by" negation="false" src="d0_s7" to="o7552" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0–10 cm, leafy-bracted.</text>
      <biological_entity id="o7553" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="leafy-bracted" value_original="leafy-bracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres ovoid to hemispheric, 2–4 × 1.5–6 cm, loosely arachnoid on phyllary margins or glabrate.</text>
      <biological_entity id="o7554" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s9" to="hemispheric" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s9" to="6" to_unit="cm" />
        <character constraint="on phyllary margins" constraintid="o7555" is_modifier="false" modifier="loosely" name="pubescence" src="d0_s9" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="phyllary" id="o7555" name="margin" name_original="margins" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 5–10 series, imbricate, ovate or lanceolate (outer) to linear or linear-lanceolate (inner), margins (outer) entire or scarious-fringed, abaxial faces without glutinous ridge;</text>
      <biological_entity id="o7556" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="linear or linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o7557" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
      <biological_entity id="o7558" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s10" value="scarious-fringed" value_original="scarious-fringed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7559" name="face" name_original="faces" src="d0_s10" type="structure" />
      <biological_entity id="o7560" name="ridge" name_original="ridge" src="d0_s10" type="structure">
        <character is_modifier="true" name="coating" src="d0_s10" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o7556" id="r699" name="in" negation="false" src="d0_s10" to="o7557" />
      <relation from="o7559" id="r700" name="without" negation="false" src="d0_s10" to="o7560" />
    </statement>
    <statement id="d0_s11">
      <text>outer and mid appressed, spines erect to spreading 0.5–13 mm;</text>
      <biological_entity constraint="outer and mid" id="o7561" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o7562" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s11" to="spreading" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>apices of mid and inner narrowed and scabro-denticulate or with expanded, erose-dentate tips, spineless or tipped with flattened spines.</text>
      <biological_entity id="o7563" name="apex" name_original="apices" src="d0_s12" type="structure" />
      <biological_entity id="o7564" name="tip" name_original="tips" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="scabro-denticulate" value_original="scabro-denticulate" />
        <character is_modifier="true" name="size" src="d0_s12" value="expanded" value_original="expanded" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="erose-dentate" value_original="erose-dentate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="spineless" value_original="spineless" />
        <character constraint="with spines" constraintid="o7565" is_modifier="false" name="architecture" src="d0_s12" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o7565" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Corollas white or pale lavender to purple, 20–40 mm, tubes 7–24 mm, throats 4–12 mm (noticeably larger than tubes), lobes 4–10 mm;</text>
      <biological_entity id="o7566" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="pale lavender" name="coloration" src="d0_s13" to="purple" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7567" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7568" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7569" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style tips 3.5–8 mm.</text>
      <biological_entity constraint="style" id="o7570" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae light to dark-brown, 4–6.5 mm, apical collars usually colored like body;</text>
      <biological_entity id="o7571" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s15" to="dark-brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o7572" name="collar" name_original="collars" src="d0_s15" type="structure">
        <character constraint="like body" constraintid="o7573" is_modifier="false" modifier="usually" name="coloration" src="d0_s15" value="colored" value_original="colored" />
      </biological_entity>
      <biological_entity id="o7573" name="body" name_original="body" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>pappi 17–35 mm, white to tan.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 34, 36.</text>
      <biological_entity id="o7574" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s16" to="35" to_unit="mm" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s16" to="tan" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7575" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
        <character name="quantity" src="d0_s17" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Que.; Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nev., Oreg., Utah, Wash., Wyo.; disjunct to e Que. (Mingan Archipelago).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="disjunct to e Que. (Mingan Archipelago)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="past_name">Circium</other_name>
  <other_name type="common_name">Meadow or elk thistle</other_name>
  <other_name type="common_name">chardon écailleux</other_name>
  <discussion>Varieties 8 (8 in the flora).</discussion>
  <discussion>Cirsium scariosum is a widely distributed complex of intergrading races distributed from southwestern Canada to northwestern Mexico. These plants range from acaulescent rosettes with a tight cluster of sessile heads to tall, erect, unbranched plants, or moundlike, more or less openly branched herbs. Acaulescent and caulescent plants sometimes occur in the same population.</discussion>
  <discussion>Members of this complex have been variously treated in the past. F. Petrak (1917) recognized ten species plus several subspecies for the taxa I am treating here as C. scariosum (in the broad sense). In floras, the names C. drummondii and C. foliosum have been widely misapplied to these plants (R. J. Moore and C. Frankton 1964). The latter two species, while clearly related to C. scariosum, have a range restricted mostly to Canada. Moore and Frankton (1967) attempted to bring order to the complex and recognized four species for plants that I include here in C. scariosum: C. acaulescens, C. congdonii, C. coloradense, and C. scariosum in the restricted sense. Moore and Frankton substituted the prior name C. tioganum for C. acaulescens. Unfortunately they did not extend their study widely enough and did not include some members of the complex in their investigations. S. L. Welsh (1982) proposed C. scariosum var. thorneae from Utah and lumped the various species recognized by Moore and Frankton within a highly polymorphic var. scariosum. After consulting with A. Cronquist and studying his manuscript treatment of Cirsium for the Intermountain Flora, D. J. Keil and C. E. Turner (1993) also accepted a broadly construed C. scariosum. Cronquist (1994) treated C. scariosum as an extremely variable species that included the four species recognized by Moore and Frankton plus the variety proposed by Welsh. Cronquist chose to not recognize infraspecific taxa.</discussion>
  <discussion>In the present treatment I have examined these plants from a biogeographic perspective with the goal of discerning regional patterns of variation. The large number of specimens available has allowed me to examine distributional patterns in relation to the topography and biogeographic history of the regions where this species occurs. My field studies also have provided me with observations that help to explain some of the anomalous specimens represented in herbaria. Although the variation within and between populations is sometimes amazing, more-or-less differentiated geographic races can be discerned. Because of the extraordinary and overlapping patterns of variation across the range of Cirsium scariosum, the following key to varieties should be regarded as at best an approximation.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants acaulescent (occasional short-caulescent individuals sometimes present in a population)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants caulescent (occasional acaulescent individuals sometimes present in a population)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas pink to purple; Sierra Nevada of w Nevada and e California to San Bernardino Mountains of s California</description>
      <determination>54e Cirsium scariosum var. congdonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas white to faintly pink-or lilac-tinged; widespread</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Abaxial leaf faces usually gray-tomentose; widespread, Colorado to s Oregon, n California</description>
      <determination>54c Cirsium scariosum var. americanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Abaxial leaf faces usually green, glabrous or glabrate; s. California</description>
      <determination>54g Cirsium scariosum var. citrinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Larger leaf spines 1–3 cm</description>
      <determination>54d Cirsium scariosum var. thorneae</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Larger leaf spines usually shorter</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas purple</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas white to faintly pink- or lilac-tinged</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Corolla lobes 5.5–8 mm; sw. Idaho, n Nevada, se Oregon</description>
      <determination>54f Cirsium scariosum var. toiyabense</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Corolla lobes 3.5–6 mm; e Oregon to sw Montana</description>
      <determination>54b Cirsium scariosum var. scariosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems usually proximally branched, plants often forming low, rounded mound; heads usually borne on short to ± elongate lateral branches; corollas 26–36 mm</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems usually erect, proximally unbranched; heads usually sessile or short-pedunculate in subcapitate to spiciform or racemiform arrays, usually closely subtended by numerous distal leaves; corollas 20–29 mm</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Apices of inner phyllaries acuminate and entire or rarely toothed; s California</description>
      <determination>54g Cirsium scariosum var. citrinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Apices of inner phyllaries usually expanded as a scarious, erose-toothed appendage; ne California, se Oregon</description>
      <determination>54h Cirsium scariosum var. robustum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Heads usually ± tightly clustered at stem tips, closely subtended and often overtopped by crowded distal leaves; distal leaves ± thin, usually fringed with numerous weak spines, often ± unpigmented proximally or tinged pink to purplish</description>
      <determination>54a Cirsium scariosum var. scariosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Heads usually in ± leafy, racemiform arrays, usually subtended by ± reduced, bractlike distal leaves; distal leaves firm, strongly spiny, usually green throughout</description>
      <determination>54b Cirsium scariosum var. coloradense</determination>
    </key_statement>
  </key>
</bio:treatment>