<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="treatment_page">286</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="B. G. Baldwin" date="1999" rank="species">bacigalupii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>46: 55, fig. 1. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species bacigalupii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066462</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–40 cm.</text>
      <biological_entity id="o15361" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid.</text>
      <biological_entity id="o15362" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades entire or irregularly lobed, faces ± hirsute and stipitate-glandular.</text>
      <biological_entity id="o15363" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o15364" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o15365" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in corymbiform or paniculiform arrays.</text>
      <biological_entity id="o15366" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o15367" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o15366" id="r1051" name="in" negation="false" src="d0_s3" to="o15367" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads usually overlapping proximal 0–1/2 of each involucre.</text>
      <biological_entity id="o15368" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" constraint="of involucre" constraintid="o15370" from="0" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o15369" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o15370" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o15368" id="r1052" name="subtending" negation="false" src="d0_s4" to="o15369" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± evenly stipitate-glandular, including margins and apices, with nonglandular, non-pustule-based hairs as well.</text>
      <biological_entity id="o15371" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o15372" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o15373" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o15374" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <relation from="o15371" id="r1053" name="including" negation="false" src="d0_s5" to="o15372" />
      <relation from="o15371" id="r1054" name="including" negation="false" src="d0_s5" to="o15373" />
      <relation from="o15371" id="r1055" modifier="well" name="with" negation="false" src="d0_s5" to="o15374" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o15375" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o15376" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o15375" id="r1056" name="in" negation="false" src="d0_s6" to="o15376" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (6–) 8 (–9);</text>
      <biological_entity id="o15377" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s7" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae deep yellow, 2–4 mm.</text>
      <biological_entity id="o15378" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets (10–) 15–18 (–21), all or mostly functionally staminate;</text>
      <biological_entity id="o15379" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s9" to="15" to_inclusive="false" />
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="21" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="18" />
        <character is_modifier="false" modifier="mostly functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow or brownish.</text>
      <biological_entity id="o15380" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi usually of 8–13, subulate to quadrate, fringed to deeply erose scales 0.1–0.8 mm, sometimes fimbriate crowns.</text>
      <biological_entity id="o15381" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="subulate" modifier="of 8-13" name="shape" src="d0_s11" to="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o15382" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="deeply" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 24.</text>
      <biological_entity id="o15383" name="crown" name_original="crowns" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="sometimes" name="shape" src="d0_s11" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15384" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline meadows, edges of alkali barrens or sinks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline meadows" />
        <character name="habitat" value="edges" constraint="of alkali barrens or sinks" />
        <character name="habitat" value="alkali barrens" />
        <character name="habitat" value="sinks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Deinandra bacigalupii occurs in the eastern San Francisco Bay area (Livermore Valley). Prior to recognition of D. bacigalupii, plants that constitute the species were regarded as northern outliers of D. increscens subsp. increscens.</discussion>
  
</bio:treatment>