<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="treatment_page">342</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1990" rank="species">vicinus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>69: 256. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species vicinus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066701</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–30 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudices with rhizomelike, relatively slender, lignescent, basal offsets 1–7 cm.</text>
      <biological_entity id="o16718" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o16719" name="caudex" name_original="caudices" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o16720" name="offset" name_original="offsets" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="rhizomelike" value_original="rhizomelike" />
        <character is_modifier="true" modifier="relatively" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="true" name="texture" src="d0_s1" value="lignescent" value_original="lignescent" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
      <relation from="o16719" id="r1547" name="with" negation="false" src="d0_s1" to="o16720" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending (branched at or below midstems), pilose on proximal 1/3 (hairs spreading-deflexed), loosely strigose distally, eglandular.</text>
      <biological_entity id="o16721" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character constraint="on proximal 1/3" constraintid="o16722" is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="loosely; distally" name="pubescence" notes="" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16722" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (usually persistent) and cauline;</text>
      <biological_entity id="o16723" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades lanceolate to oblanceolate or narrowly obovate, 10–30 × 3–10 mm, cauline little reduced distally, margins entire or with 1–2 pairs of teeth, loosely strigose (hairs ascending), eglandular.</text>
      <biological_entity constraint="basal" id="o16724" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate or narrowly obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o16725" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o16726" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with 1-2 pairs" value_original="with 1-2 pairs" />
        <character is_modifier="false" modifier="loosely" name="pubescence" notes="" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o16727" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity id="o16728" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <relation from="o16726" id="r1548" name="with" negation="false" src="d0_s4" to="o16727" />
      <relation from="o16727" id="r1549" name="part_of" negation="false" src="d0_s4" to="o16728" />
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–2) (branches from or proximal to midstem).</text>
      <biological_entity id="o16729" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 4–5 × 7–10 mm.</text>
      <biological_entity id="o16730" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, sparsely hirsute, sparsely minutely glandular.</text>
      <biological_entity id="o16731" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o16732" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o16731" id="r1550" name="in" negation="false" src="d0_s7" to="o16732" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 60–95;</text>
      <biological_entity id="o16733" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s8" to="95" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, with lilac abaxial midstripe, 7–10 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o16734" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16735" name="midstripe" name_original="midstripe" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="lilac" value_original="lilac" />
      </biological_entity>
      <biological_entity id="o16736" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
      <relation from="o16734" id="r1551" name="with" negation="false" src="d0_s9" to="o16735" />
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 1.8–2.2 mm (throats slightly indurate or inflated).</text>
      <biological_entity constraint="disc" id="o16737" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 0.6–0.8 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o16738" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o16739" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of scales, inner of 8–11 bristles.</text>
      <biological_entity id="o16740" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o16741" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o16742" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s12" to="11" />
      </biological_entity>
      <relation from="o16740" id="r1552" name="outer of" negation="false" src="d0_s12" to="o16741" />
      <relation from="o16740" id="r1553" name="inner of" negation="false" src="d0_s12" to="o16742" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and canyons, crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>164.</number>
  <other_name type="common_name">Neighbor fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>In Texas, Erigeron vicinus grows in the Davis Mountains, Jeff Davis County. In its fibrous-rooted habit and perennial duration, finely strigose distal stems, and simple or branched stems with heads on relatively long peduncles, E. vicinus is similar to E. pubescens Kunth, a species occurring throughout much of the northern half of Mexico but apparently not reaching the United States, and to forms of E. modestus (especially with respect to the stems reddish and pilose at bases). Erigeron vicinus was originally described from the Sierra Madera del Carmen of Coahuila, where it was thought to be endemic.</discussion>
  
</bio:treatment>