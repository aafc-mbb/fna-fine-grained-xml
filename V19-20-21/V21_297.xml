<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="treatment_page">121</other_info_on_meta>
    <other_info_on_meta type="illustration_page">120</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">encelia</taxon_name>
    <taxon_name authority="A. Gray ex Torrey in W. H. Emory" date="1848" rank="species">farinosa</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Not. Milit. Reconn.,</publication_title>
      <place_in_publication>143. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus encelia;species farinosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066497</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Encelia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">farinosa</taxon_name>
    <taxon_name authority="S. F. Blake) I. M. Johnston" date="unknown" rank="variety">phenicodonta</taxon_name>
    <taxon_hierarchy>genus Encelia;species farinosa;variety phenicodonta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 30–150 cm (sap fragrant).</text>
      <biological_entity id="o13309" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched distally, tomentose, developing smooth barks.</text>
      <biological_entity id="o13310" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o13311" name="bark" name_original="barks" src="d0_s1" type="structure">
        <character is_modifier="true" name="development" src="d0_s1" value="developing" value_original="developing" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline (clustered near stem tips);</text>
      <biological_entity id="o13312" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 10–20 mm;</text>
      <biological_entity id="o13313" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades silver or gray, ovate to lanceolate, 20–70 mm, apices obtuse or acute, faces tomentose.</text>
      <biological_entity id="o13314" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="silver" value_original="silver" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray" value_original="gray" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13315" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13316" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in paniculiform arrays (branching among heads mainly distal).</text>
      <biological_entity id="o13317" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o13318" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o13317" id="r912" name="in" negation="false" src="d0_s5" to="o13318" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles glabrous except near heads (± yellow).</text>
      <biological_entity id="o13319" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character constraint="except heads" constraintid="o13320" is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13320" name="head" name_original="heads" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres 4–10 mm.</text>
      <biological_entity id="o13321" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries lanceolate.</text>
      <biological_entity id="o13322" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 11–21;</text>
      <biological_entity id="o13323" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s9" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae 8–12 mm.</text>
      <biological_entity constraint="corolla" id="o13324" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas yellow or brown-purple, 5–6 mm.</text>
      <biological_entity constraint="disc" id="o13325" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown-purple" value_original="brown-purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 3–6 mm;</text>
      <biological_entity id="o13326" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0.2n = 36.</text>
      <biological_entity id="o13327" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13328" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May, Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal scrub, stony desert hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="stony desert hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Baja California, Baja California Sur, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Brittlebush</other_name>
  <other_name type="common_name">incienso</other_name>
  <discussion>Plants of Encelia farinosa with brown-purple disc corollas, found along the Colorado and Salt rivers, and common in Baja California, are var. phenicodonta. Plants with substrigose leaves, capitulescences branched toward bases rather than distally, and ray florets reduced in both size and number are most often hybrids and backcrosses between E. farinosa and E. frutescens. P. A. Munz (1959) indicated that I. L. Wiggins had reported var. radians Brandegee ex S. F. Blake as occurring in southeastern California; that variety is known only from Baja California.</discussion>
  
</bio:treatment>