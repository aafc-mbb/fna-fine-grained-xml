<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">mohavensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 87. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety mohavensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068283</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">mohavensis</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 138. 1884 (as Bigelovia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bigelowia;species mohavensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) Britton" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall" date="unknown" rank="variety">mohavensis</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;variety mohavensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–80 cm.</text>
      <biological_entity id="o11881" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems white to yellowish green, nearly leafless at flowering, densely tomentose.</text>
      <biological_entity id="o11882" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s1" to="yellowish green" />
        <character constraint="at flowering" is_modifier="false" modifier="nearly" name="architecture" src="d0_s1" value="leafless" value_original="leafless" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves yellowish green;</text>
      <biological_entity id="o11883" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved, filiform, 10–30 × 0.5–1.5 mm, faces glabrate.</text>
      <biological_entity id="o11884" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11885" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 8.5–12.</text>
      <biological_entity id="o11886" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="8.5" name="quantity" src="d0_s4" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 20–26, apices erect, acute, abaxial faces tomentose to glabrate.</text>
      <biological_entity id="o11887" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s5" to="26" />
      </biological_entity>
      <biological_entity id="o11888" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11889" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 7–10.9 mm, tubes puberulent, lobes 0.9–2 mm, glabrous;</text>
      <biological_entity id="o11890" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="10.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11891" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o11892" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages longer than stigmatic portions.</text>
      <biological_entity constraint="style" id="o11893" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic portions" constraintid="o11894" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o11894" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae densely hairy;</text>
      <biological_entity id="o11895" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 6.3–9.8 mm. 2n = 18.</text>
      <biological_entity id="o11896" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="6.3" from_unit="mm" name="some_measurement" src="d0_s9" to="9.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11897" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry scrub Artemisia, pinyon, Atriplex, Joshua tree communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinyon" />
        <character name="habitat" value="joshua tree communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21k.</number>
  <other_name type="common_name">Mojave rabbitbrush</other_name>
  <discussion>Variety mohavensis may intergrade with vars. hololeuca and oreophila. It grows in southern Nevada and southern California.</discussion>
  
</bio:treatment>