<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John C. Semple</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">214</other_info_on_meta>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="genus">BRADBURIA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 250. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus BRADBURIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For John Bradbury, 1768–1823, English naturalist, collector for the Liverpool Botanic Garden in the Missouri Territory, 1810–1811</other_info_on_name>
    <other_info_on_name type="fna_id">104508</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Nuttall) Elliott" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) G. L. Nesom" date="unknown" rank="section">Bradburia</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;section Bradburia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, rarely perennials, 15–80 cm;</text>
      <biological_entity id="o3036" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices taprooted, woody.</text>
      <biological_entity id="o3038" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, usually simple (annuals), sometimes proximally branched (perennials), sparsely pilose.</text>
      <biological_entity id="o3039" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes proximally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>alternate;</text>
      <biological_entity id="o3040" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal petiolate, cauline sesssile;</text>
      <biological_entity constraint="basal" id="o3041" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o3042" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blades 1-nerved, oblanceolate (bases attenuate), margins entire or apically dentate, sometimes coarsely hispido-pilose (apices acute), faces hispido-pilose, sometimes coarsely so;</text>
      <biological_entity id="o3043" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o3044" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes coarsely" name="pubescence" src="d0_s6" value="hispido-pilose" value_original="hispido-pilose" />
      </biological_entity>
      <biological_entity id="o3045" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hispido-pilose" value_original="hispido-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline blades linear-lanceolate to elliptic-oblong, reduced distally, obscurely apically dentate or entire, faces pilose.</text>
      <biological_entity constraint="cauline" id="o3046" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="elliptic-oblong" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="obscurely apically" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3047" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads radiate, borne singly or in lax paniculiform arrays (on long branches from distal nodes, much exceeding primary and higher level branches from which they arise).</text>
      <biological_entity id="o3048" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="in lax paniculiform arrays" value_original="in lax paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o3049" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o3048" id="r285" name="in" negation="false" src="d0_s8" to="o3049" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 1.5–10 cm, short-hispido-pilose, stipitate-glandular distally.</text>
      <biological_entity id="o3050" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="short-hispido-pilose" value_original="short-hispido-pilose" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres campanulate, (6–9 ×) 6–17 mm.</text>
      <biological_entity id="o3051" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="[6" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="]6" from_unit="mm" name="width" src="d0_s10" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries 25–60 in 3–5 series, 1-nerved (midnerves yellowbrown to brown, somewhat translucent, faint to obvious, raised; outer somewhat keeled proximally), linear to linear-lanceolate, strongly unequal, scarious to herbaceous distally, margins broadly scarious, faces sparsely to densely short to long-pilose, sparsely glandular.</text>
      <biological_entity id="o3052" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3053" from="25" name="quantity" src="d0_s11" to="60" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="linear-lanceolate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="scarious" modifier="distally" name="texture" src="d0_s11" to="herbaceous" />
      </biological_entity>
      <biological_entity id="o3053" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity id="o3054" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o3055" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="long-pilose" value_original="long-pilose" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Receptacles flat to slightly convex, pitted, epaleate.</text>
      <biological_entity id="o3056" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s12" to="slightly convex" />
        <character is_modifier="false" name="relief" src="d0_s12" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Ray-florets 7–25, pistillate, fertile;</text>
      <biological_entity id="o3057" name="ray-floret" name_original="ray-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s13" to="25" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow.</text>
      <biological_entity id="o3058" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Disc-florets 11–60, bisexual and fertile, or functionally staminate and sterile;</text>
      <biological_entity id="o3059" name="disc-floret" name_original="disc-florets" src="d0_s15" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s15" to="60" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>corollas yellow, tubes shorter than cylindric, distally narrowly expanded throats, lobes 5, erect, triangular;</text>
      <biological_entity id="o3060" name="corolla" name_original="corollas" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o3061" name="tube" name_original="tubes" src="d0_s16" type="structure">
        <character constraint="than cylindric , distally narrowly expanded throats" constraintid="o3062" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o3062" name="throat" name_original="throats" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character is_modifier="true" modifier="distally narrowly" name="size" src="d0_s16" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o3063" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>style-branch appendages linear-triangular.</text>
      <biological_entity constraint="style-branch" id="o3064" name="appendage" name_original="appendages" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="linear-triangular" value_original="linear-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Cypselae (straw to brown) obconic-obovoid, compressed or triangular, smooth or slightly ribbed, faces short-strigose;</text>
      <biological_entity id="o3065" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="obconic-obovoid" value_original="obconic-obovoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s18" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s18" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o3066" name="face" name_original="faces" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="short-strigose" value_original="short-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pappi persistent, of 20–35 stramineous to rusty brown, apically attenuate bristles in 2–3 series, outer either bristles grading into inner series or scales.</text>
      <biological_entity id="o3067" name="pappus" name_original="pappi" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o3068" name="bristle" name_original="bristles" src="d0_s19" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s19" to="35" />
        <character char_type="range_value" from="stramineous" is_modifier="true" name="coloration" src="d0_s19" to="rusty brown" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s19" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o3069" name="series" name_original="series" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s19" to="3" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3071" name="series" name_original="series" src="d0_s19" type="structure" />
      <biological_entity id="o3072" name="scale" name_original="scales" src="d0_s19" type="structure" />
      <relation from="o3067" id="r286" name="consist_of" negation="false" src="d0_s19" to="o3068" />
      <relation from="o3068" id="r287" name="in" negation="false" src="d0_s19" to="o3069" />
      <relation from="o3070" id="r288" name="into" negation="false" src="d0_s19" to="o3071" />
      <relation from="o3070" id="r289" name="into" negation="false" src="d0_s19" to="o3072" />
    </statement>
    <statement id="d0_s20">
      <text>x = 4, 3.</text>
      <biological_entity id="o3070" name="bristle" name_original="bristles" src="d0_s19" type="structure">
        <character is_modifier="true" name="position" src="d0_s19" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity constraint="x" id="o3073" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="4" value_original="4" />
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>181.</number>
  <other_name type="common_name">Goldenaster</other_name>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Bradburia was considered monotypic until recently. J. C. Semple and C. C. Chinnappa (1984) thought that one species, B. hirtella, was closely related to Chrysopsis pilosa but not to species of Heterotheca. G. L. Nesom (1991) included B. hirtella within Chrysopsis sect. Bradburia with C. pilosa. Semple (1996) maintained the genus as separate and transferred C. pilosa to Bradburia. In a cladistic study of subtribe Chrysopsidinae, Semple and L. Tebby (1999) found that the two species of Bradburia form a strongly supported group in a clade with Heterotheca and Croptilon, while Chrysopsis is in a clade with Pityopsis, the Mexican monotypic genus Tomentaurum G. L. Nesom, and the South American genus Noticastrum de Candolle.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Disc florets staminate; ray pappi of narrow, broad-based bristles 1–3 mm, outer series shorter than and grading into inner bristles</description>
      <determination>1 Bradburia hirtella</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Disc florets bisexual; ray and disc pappi in 2 distinct series, outer of flat scales 0.5–1.1 mm, inner of barbellate bristles 5–6 mm</description>
      <determination>2 Bradburia pilosa</determination>
    </key_statement>
  </key>
</bio:treatment>