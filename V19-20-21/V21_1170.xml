<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">464</other_info_on_meta>
    <other_info_on_meta type="mention_page">469</other_info_on_meta>
    <other_info_on_meta type="mention_page">473</other_info_on_meta>
    <other_info_on_meta type="treatment_page">468</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">eupatorium</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Small" date="1903" rank="species">lancifolium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1167. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus eupatorium;species lancifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066735</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Elliott [not Aublet]" date="unknown" rank="species">parviflorum</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="variety">lancifolium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 85. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species parviflorum;variety lancifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">semiserratum</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="unknown" rank="variety">lancifolium</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species semiserratum;variety lancifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–100+ cm.</text>
      <biological_entity id="o3758" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (from short rhizomes) single, branched distally, usually glabrous or glabrate, sometimes pilose.</text>
      <biological_entity id="o3759" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually opposite (distal sometimes alternate);</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile or subsessile (petioles to 5 mm);</text>
      <biological_entity id="o3760" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 3-nerved from bases, lanceolate to lance-linear, 30–50 × 5–13 mm, bases narrowly cuneate, margins serrate, apices acute, faces glabrous adaxially, sparsely pilose abaxially, densely glanddotted.</text>
      <biological_entity id="o3761" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="from bases" constraintid="o3762" is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s4" to="lance-linear" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3762" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o3763" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o3764" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o3765" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o3766" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o3767" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o3768" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o3767" id="r295" name="in" negation="false" src="d0_s5" to="o3768" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 7–10 in 2–3 series, elliptic, 1–4.5 × 0.2–0.8 mm, apices rounded, abaxial faces pilose, glanddotted.</text>
      <biological_entity id="o3769" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3770" from="7" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3770" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o3771" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3772" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 5;</text>
      <biological_entity id="o3773" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 2.5–3 mm.</text>
      <biological_entity id="o3774" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 2.5–2.8 mm;</text>
      <biological_entity id="o3775" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 30–40 bristles 3.5–4 mm. 2n = 20.</text>
      <biological_entity id="o3776" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity id="o3777" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s10" to="40" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3778" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
      <relation from="o3776" id="r296" name="consist_of" negation="false" src="d0_s10" to="o3777" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rolling terrain, clay soils, shade to sun, shortleaf pine and oak woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rolling terrain" modifier="dry" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="shade" />
        <character name="habitat" value="sun" />
        <character name="habitat" value="shortleaf pine" />
        <character name="habitat" value="oak woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–200+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200+" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., La., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Lanceleaf thoroughwort</other_name>
  <discussion>Eupatorium lancifolium has been combined with E. semiserratum or listed within it as a subspecies; it differs in its habitat as well as in having leaves that are somewhat smaller, typically a dull blue-green (in contrast to yellow-green in E. semiserratum), and 3-nerved from bases of blades.</discussion>
  
</bio:treatment>