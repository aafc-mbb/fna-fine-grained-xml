<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">146</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Cronquist" date="1953" rank="species">brevistylum</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>7: 26. 1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species brevistylum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066361</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials, 20–350 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o11748" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="350" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="350" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, erect, simple or branched in distal 1/2, loosely to densely villous or viscid-pilose with jointed trichomes, often arachnoid below heads;</text>
      <biological_entity id="o11750" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="in distal 1/2" constraintid="o11751" is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="loosely to densely; densely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character constraint="with trichomes" constraintid="o11752" is_modifier="false" name="pubescence" src="d0_s2" value="viscid-pilose" value_original="viscid-pilose" />
        <character constraint="below heads" constraintid="o11753" is_modifier="false" modifier="often" name="pubescence" notes="" src="d0_s2" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11751" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o11752" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="jointed" value_original="jointed" />
      </biological_entity>
      <biological_entity id="o11753" name="head" name_original="heads" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>branches 0–many, ascending.</text>
      <biological_entity id="o11754" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades oblong to elliptic or oblanceolate, 15–35 × 2–10 cm, flat to ± undulate, coarsely dentate to shallowly pinnatifid, lobes broadly triangular, spinulose to spiny-dentate or shallowly lobed, main spines slender, 3–7 mm, abaxial faces thinly gray-tomentose, villous along major veins, sometimes glabrescent, adaxial sparsely villous or viscid-pilose along midveins with jointed trichomes;</text>
      <biological_entity id="o11755" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o11756" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic or oblanceolate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s4" to="35" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s4" to="more or less undulate coarsely dentate" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s4" to="more or less undulate coarsely dentate" />
      </biological_entity>
      <biological_entity id="o11757" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character constraint="to main spines" constraintid="o11758" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="main" id="o11758" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spiny-dentate" value_original="spiny-dentate" />
        <character is_modifier="true" modifier="shallowly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11759" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="gray-tomentose" value_original="gray-tomentose" />
        <character constraint="along veins" constraintid="o11760" is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o11760" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11761" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character constraint="along midveins" constraintid="o11762" is_modifier="false" name="pubescence" src="d0_s4" value="viscid-pilose" value_original="viscid-pilose" />
      </biological_entity>
      <biological_entity id="o11762" name="midvein" name_original="midveins" src="d0_s4" type="structure" />
      <biological_entity id="o11763" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="jointed" value_original="jointed" />
      </biological_entity>
      <relation from="o11762" id="r1088" name="with" negation="false" src="d0_s4" to="o11763" />
    </statement>
    <statement id="d0_s5">
      <text>basal often absent at flowering, spiny winged-petiolate;</text>
      <biological_entity id="o11764" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o11765" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline well distributed, gradually reduced, proximal winged-petiolate, mid and distal sessile, bases clasping or short-decurrent;</text>
      <biological_entity id="o11766" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o11767" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11768" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
      <biological_entity constraint="mid and distal" id="o11769" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o11770" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal moderately to strongly reduced, often spinier than the proximal.</text>
      <biological_entity id="o11771" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o11772" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately to strongly" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character constraint="than the proximal leaves" constraintid="o11773" is_modifier="false" name="architecture" src="d0_s7" value="often spinier" value_original="often spinier" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11773" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–many, ± erect, usually crowded in subcapitate to tight corymbiform arrays, closely subtended by clustered ± leafy bracts.</text>
      <biological_entity id="o11774" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s8" to="many" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="in arrays" constraintid="o11775" is_modifier="false" modifier="usually" name="arrangement" src="d0_s8" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o11775" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character char_type="range_value" from="subcapitate" is_modifier="true" name="architecture" src="d0_s8" to="tight corymbiform" />
      </biological_entity>
      <biological_entity id="o11776" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s8" value="clustered" value_original="clustered" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s8" value="leafy" value_original="leafy" />
      </biological_entity>
      <relation from="o11774" id="r1089" modifier="closely" name="subtended by" negation="false" src="d0_s8" to="o11776" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–1 (–30) cm.</text>
      <biological_entity id="o11777" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres hemispheric to campanulate, 2.5–3.5 cm, 2.5–4 cm diam., loosely to densely arachnoid, phyllaries connected by long septate or non-septate trichomes.</text>
      <biological_entity id="o11778" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s10" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s10" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="loosely to densely" name="pubescence" src="d0_s10" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
      <biological_entity id="o11779" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character constraint="by trichomes" constraintid="o11780" is_modifier="false" name="fusion" src="d0_s10" value="connected" value_original="connected" />
      </biological_entity>
      <biological_entity id="o11780" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="septate" value_original="septate" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="non-septate" value_original="non-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries radiating in 5–10 series, subequal, green, linear-acicular, outermost margins sometimes spiny-fringed, otherwise all entire or minutely serrulate, abaxial faces without glutinous ridge;</text>
      <biological_entity id="o11781" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character constraint="in series" constraintid="o11782" is_modifier="false" name="arrangement" src="d0_s11" value="radiating" value_original="radiating" />
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s11" value="linear-acicular" value_original="linear-acicular" />
      </biological_entity>
      <biological_entity id="o11782" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o11783" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s11" value="spiny-fringed" value_original="spiny-fringed" />
        <character is_modifier="false" modifier="otherwise" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s11" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11784" name="face" name_original="faces" src="d0_s11" type="structure" />
      <biological_entity id="o11785" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o11784" id="r1090" name="without" negation="false" src="d0_s11" to="o11785" />
    </statement>
    <statement id="d0_s12">
      <text>outer and mid-bases short-appressed, apices stiffly radiating to ascending, long, very narrow, spines straight, slender, 3–5 m;</text>
      <biological_entity constraint="outer" id="o11786" name="mid-base" name_original="mid-bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="short-appressed" value_original="short-appressed" />
      </biological_entity>
      <biological_entity id="o11787" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="stiffly" name="arrangement" src="d0_s12" value="radiating" value_original="radiating" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="false" modifier="very" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o11788" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s12" to="5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner straight, flat.</text>
      <biological_entity id="o11789" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="straight; straight">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11790" name="straight" name_original="straight" src="d0_s13" type="structure" />
      <relation from="o11789" id="r1091" name="part_of" negation="false" src="d0_s13" to="o11790" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas white to pink or purple, very slender, 20–25 mm, tubes 10–17 mm, throats 4–5 mm, lobes filiform with knoblike tips, 3–5 mm;</text>
      <biological_entity id="o11791" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="pink or purple" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s14" value="slender" value_original="slender" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11792" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11793" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11794" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character constraint="with tips" constraintid="o11795" is_modifier="false" name="shape" src="d0_s14" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11795" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="knoblike" value_original="knoblike" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 2–4 mm, included or exserted (only 1–2 mm beyond corolla lobes).</text>
      <biological_entity constraint="style" id="o11796" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae brown, 3–4.5 mm, apical collars stramineous, 0.2 mm;</text>
      <biological_entity id="o11797" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o11798" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="stramineous" value_original="stramineous" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 10–22 mm. 2n = 34.</text>
      <biological_entity id="o11799" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s17" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11800" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Apr–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal meadows, marshes, swamps, riparian woodlands, moist sites in coastal scrub, chaparral, coastal woodlands, mixed conifer-hardwood forests, or coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal meadows" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="riparian woodlands" />
        <character name="habitat" value="moist sites" constraint="in coastal scrub" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="coastal woodlands" />
        <character name="habitat" value="mixed conifer-hardwood forests" />
        <character name="habitat" value="coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <other_name type="common_name">Indian or clustered or short-style thistle</other_name>
  <discussion>Cirsium brevistylum occurs in the coast ranges and adjacent coastal slope from southwestern British Columbia to southern California. In the Pacific Northwest its range extends inland to the northern Rocky Mountains of southern British Columbia, Idaho, and northwestern Montana, and the Blue and Wallowa ranges of eastern Oregon. It is absent from the central and southern Cascade Range.</discussion>
  <discussion>In older literature the name Cirsium edule was widely misapplied to this species. A. Cronquist (1953) pointed out that the type of C. edule has corolla and style features quite different from those of the plants that had been called by that name and established the name C. brevistylum, based upon the notably short styles of this species. Hybrids of C. brevistylum with C. edule have been named C. ×vancouveriense R. J. Moore &amp; C. Frankton.</discussion>
  
</bio:treatment>