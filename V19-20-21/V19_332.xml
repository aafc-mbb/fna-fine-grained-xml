<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="treatment_page">252</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="F. H. Wiggers" date="1780" rank="genus">taraxacum</taxon_name>
    <taxon_name authority="A. Nelson" date="1945" rank="species">carneocoloratum</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>32: 290. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus taraxacum;species carneocoloratum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067702</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3.5–9 cm;</text>
      <biological_entity id="o731" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s0" to="9" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots branched.</text>
      <biological_entity id="o732" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–3, ascending to erect, purplish proximally to completely (barely exceeding foliage), glabrous.</text>
      <biological_entity id="o733" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="proximally to completely" name="coloration" src="d0_s2" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="completely" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves fewer than 10 (–15), horizontal to patent or ± erect;</text>
      <biological_entity id="o734" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="15" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="10" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s3" to="patent or more or less erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles narrowly winged (mostly distally);</text>
      <biological_entity id="o735" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate (sometimes runcinate), 2.8–8 × 0.5–1.4 cm, bases attenuate, margins lobed shallowly (about 1/2 width of blades or less) or toothed, lobes straight or retrorse, sometimes antrorse, triangular or deltate to lanceolate, obtuse to acute or acuminate, teeth 0 on lobes, apices obtuse, faces glabrous.</text>
      <biological_entity id="o736" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o737" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o738" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o739" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="lanceolate obtuse" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="lanceolate obtuse" />
      </biological_entity>
      <biological_entity id="o740" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character constraint="on lobes" constraintid="o741" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o741" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity id="o742" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o743" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 10–12, appressed (to spreading), very widely ovate (outer) to ovate, dark green, often purple-tipped bractlets in 2 (–3) series, 4–5.5 × 2.5–4.5 mm, white-scarious to not scarious, apices acuminate to caudate, hornless, scarious-erose.</text>
      <biological_entity id="o744" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="of 10-12" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="very widely ovate" name="shape" src="d0_s6" to="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
      </biological_entity>
      <biological_entity id="o745" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="often" name="architecture" src="d0_s6" value="purple-tipped" value_original="purple-tipped" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" notes="" src="d0_s6" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" notes="" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="white-scarious" name="texture" src="d0_s6" to="not scarious" />
      </biological_entity>
      <biological_entity id="o746" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o747" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s6" to="caudate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hornless" value_original="hornless" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="scarious-erose" value_original="scarious-erose" />
      </biological_entity>
      <relation from="o745" id="r73" name="in" negation="false" src="d0_s6" to="o746" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres dark green, broadly campanulate, (10–) 12–16 mm.</text>
      <biological_entity id="o748" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–14 in 2 series, ovate to lanceovate, 2.8–5.5 mm wide, margins not scarious (some outer) to broadly so (at least proximally, inner), apices acuminate, inner scarious and erose, hornless.</text>
      <biological_entity id="o749" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o750" from="12" name="quantity" src="d0_s8" to="14" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s8" to="lanceovate" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o750" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o751" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o752" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o753" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s8" value="hornless" value_original="hornless" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 20–30+;</text>
      <biological_entity id="o754" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas pink purplish to pinkish cream (± bronze when fresh), outer 13–14 × 2.4–2.6 mm.</text>
      <biological_entity id="o755" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink purplish" name="coloration" src="d0_s10" to="pinkish cream" />
        <character is_modifier="false" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s10" to="14" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s10" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae grayish, greenish, or yellowish, bodies oblanceoloid, ca. 3–4+ mm, cones [mature not seen], beaks stout, ribs [mature not seen], faces proximally smooth, muricate in distal 1/4;</text>
      <biological_entity id="o756" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o757" name="body" name_original="bodies" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceoloid" value_original="oblanceoloid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o758" name="cone" name_original="cones" src="d0_s11" type="structure" />
      <biological_entity id="o759" name="beak" name_original="beaks" src="d0_s11" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o760" name="rib" name_original="ribs" src="d0_s11" type="structure" />
      <biological_entity id="o761" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character constraint="in distal 1/4" constraintid="o762" is_modifier="false" name="relief" src="d0_s11" value="muricate" value_original="muricate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o762" name="1/4" name_original="1/4" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pappi yellowish, ca. 7 mm.</text>
      <biological_entity id="o763" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>High alpine, gravelly areas and scree slopes, ridge crests, dry substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="high alpine" />
        <character name="habitat" value="gravelly areas" />
        <character name="habitat" value="scree slopes" />
        <character name="habitat" value="crests" modifier="ridge" />
        <character name="habitat" value="dry substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Pink dandelion</other_name>
  <discussion>Taraxacum carneocoloratum may be associated with unglaciated areas of Alaska and Yukon, where it is infrequent. It is easily distinguished in bloom by its pink ligules (± bronze when fresh).</discussion>
  
</bio:treatment>