<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="treatment_page">337</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1990" rank="species">arisolius</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>69: 243. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species arisolius</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066553</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or short-lived perennials, 30–70 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o10933" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, coarsely hirsute to hispid (hairs only along ribs, bases thickened), minutely glandular.</text>
      <biological_entity id="o10935" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="coarsely hirsute" name="pubescence" src="d0_s2" to="hispid" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o10936" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10937" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to linear-oblong, lanceolate, or oblanceolate (slightly narrowed distally), 25–50 × 2–5 mm, margins entire or sometimes lobed (lobes 1–2 pairs, coarse, rounded), faces hispido-strigose.</text>
      <biological_entity id="o10938" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblong lanceolate or oblanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblong lanceolate or oblanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblong lanceolate or oblanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10939" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o10940" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispido-strigose" value_original="hispido-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 20–50+ in loose, corymbiform arrays (buds erect).</text>
      <biological_entity id="o10941" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o10942" from="20" name="quantity" src="d0_s5" to="50" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10942" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s5" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 2.5–3.5 × 5–8 mm.</text>
      <biological_entity id="o10943" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series, minutely hispid, minutely glandular.</text>
      <biological_entity id="o10944" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" notes="" src="d0_s7" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o10945" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o10944" id="r1002" name="in" negation="false" src="d0_s7" to="o10945" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 125–180;</text>
      <biological_entity id="o10946" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="125" name="quantity" src="d0_s8" to="180" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas usually white, sometimes lavender or pinkish, 6–7 mm, laminae reflexing.</text>
      <biological_entity id="o10947" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10948" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2–2.5 mm (throats indurate and inflated).</text>
      <biological_entity constraint="disc" id="o10949" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 0.7–1 mm, 2-nerved (nerves orange), faces sparsely strigose to glabrate;</text>
      <biological_entity id="o10950" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o10951" name="face" name_original="faces" src="d0_s11" type="structure">
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s11" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae or lanceolate scales, inner of (10–) 12–17 bristles.</text>
      <biological_entity id="o10952" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o10953" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o10954" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o10956" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="atypical_quantity" src="d0_s12" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s12" to="17" />
      </biological_entity>
      <relation from="o10952" id="r1003" name="outer of" negation="false" src="d0_s12" to="o10953" />
      <relation from="o10952" id="r1004" name="outer of" negation="false" src="d0_s12" to="o10954" />
      <relation from="o10955" id="r1005" name="consist_of" negation="false" src="d0_s12" to="o10956" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity constraint="inner" id="o10955" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o10957" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, often in moist areas, sometimes with mesquite, openings, roadsides, oak, juniper, mesquite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="moist areas" modifier="often in" />
        <character name="habitat" value="mesquite" modifier="sometimes with" />
        <character name="habitat" value="openings" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(800–)1300–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1700" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>152.</number>
  <other_name type="common_name">Dry-sun fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erigeron arisolius is similar in general appearance to E. divergens; the erect buds and reflexing rays of E. arisolius contrast with the nodding buds and non-reflexing rays of E. divergens. Additionally, the latter has evenly distributed stem pubescence, cypselae with whitish nerves (versus orange), and fewer pappus bristles. Erigeron sceptrifer also may be difficult to distinguish from these: it is more similar to E. arisolius in vestiture and has shorter rays that usually become purple upon drying and that are downward-curved, compared to those of E. arisolius, where the rays dry a lighter color and reflex at the tube-lamina junction.</discussion>
  
</bio:treatment>