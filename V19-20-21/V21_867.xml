<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">346</other_info_on_meta>
    <other_info_on_meta type="treatment_page">347</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="(Nuttall) Ornduff" date="1966" rank="section">ptilomeris</taxon_name>
    <taxon_name authority="(Nuttall) Ornduff" date="1966" rank="species">coronaria</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>40: 76. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section ptilomeris;species coronaria</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067046</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ptilomeris</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">coronaria</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 382. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ptilomeris;species coronaria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baeria</taxon_name>
    <taxon_name authority="(Hooker) K. L. Chambers" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Baeria;species californica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baeria</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">coronaria</taxon_name>
    <taxon_hierarchy>genus Baeria;species coronaria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 40 cm (herbage sweetly scented).</text>
      <biological_entity id="o26139" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched distally, usually glandular-puberulent (often with longer nonglandular hairs as well).</text>
      <biological_entity id="o26140" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear, 15–60 × 0.5–5 mm, (not fleshy) margins entire or 1–2-pinnately lobed, faces hairy.</text>
      <biological_entity id="o26141" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="60" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26142" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="1-2-pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o26143" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres hemispheric to obconic, 4–7 mm.</text>
      <biological_entity id="o26144" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s3" to="obconic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 6–14, lanceolate to ovate, hairy.</text>
      <biological_entity id="o26145" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="14" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles conic, smooth, muricate, or pitted, hairy.</text>
      <biological_entity id="o26146" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s5" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="relief" src="d0_s5" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="relief" src="d0_s5" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="relief" src="d0_s5" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 6–15;</text>
      <biological_entity id="o26147" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>(corollas yellow) laminae linear-oblong or oblong, 3–10 mm.</text>
      <biological_entity id="o26148" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anther appendages elliptic, acute (style apices ± deltate with apical tufts of hairs and subapical fringes of shorter hairs).</text>
      <biological_entity constraint="anther" id="o26149" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae black, linear to narrowly clavate, to 2.5 mm, hairy;</text>
      <biological_entity id="o26150" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="narrowly clavate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi usually of 5–6+ lanceolate to ovate scales (1–5 uniaristate), sometimes of 4–5 subulate, aristate scales, or 0.2n = 8, 10.</text>
      <biological_entity id="o26151" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity id="o26152" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="6" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s10" to="ovate" />
      </biological_entity>
      <biological_entity id="o26153" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="true" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26154" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
      </biological_entity>
      <relation from="o26151" id="r1771" name="consist_of" negation="false" src="d0_s10" to="o26152" />
      <relation from="o26151" id="r1772" modifier="sometimes" name="consist_of" negation="false" src="d0_s10" to="o26153" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sunny, open grassy areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open grassy areas" modifier="sunny" />
        <character name="habitat" value="sunny" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Crowned or royal goldfields</other_name>
  <discussion>Pappus, head size, and branching pattern vary in Lasthenia coronaria. Two types of pappi are often found within a head and sometimes in different individuals of a population. The most distinctive feature of this species is its glandular herbage, which produces a characteristic sweet scent not present in any other lasthenia.</discussion>
  
</bio:treatment>