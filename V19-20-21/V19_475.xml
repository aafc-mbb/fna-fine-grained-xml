<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1843" rank="species">incana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 486. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species incana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067150</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacomeris</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">incanus</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 435. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Malacomeris;species incanus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacothrix</taxon_name>
    <taxon_name authority="Elmer" date="unknown" rank="species">incana</taxon_name>
    <taxon_name authority="(Elmer) E. W. Williams" date="unknown" rank="variety">succulenta</taxon_name>
    <taxon_hierarchy>genus Malacothrix;species incana;variety succulenta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacothrix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">succulenta</taxon_name>
    <taxon_hierarchy>genus Malacothrix;species succulenta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 12–70 cm (often mounded).</text>
      <biological_entity id="o16997" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, branched proximally and distally, usually tomentose, sometimes glabrous.</text>
      <biological_entity id="o16998" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally; usually" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal obovate to narrowly spatulate, sometimes pinnately lobed (lobes 1–2+ pairs, subequal, apices obtuse), sometimes ± fleshy, margins usually obtuse-lobed, sometimes entire;</text>
      <biological_entity constraint="cauline" id="o16999" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o17000" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="narrowly spatulate" />
        <character is_modifier="false" modifier="sometimes pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes more or less" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o17001" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="obtuse-lobed" value_original="obtuse-lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal not notably reduced (similar to others).</text>
      <biological_entity constraint="cauline" id="o17002" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o17003" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not notably" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 5–16+, ovate to lanceolate bractlets, hyaline margins 0.05–0.2 mm.</text>
      <biological_entity id="o17004" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o17005" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="16" upper_restricted="false" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o17006" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o17004" id="r1530" name="consist_of" negation="false" src="d0_s4" to="o17005" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres 10–14 × 4–8+ mm.</text>
      <biological_entity id="o17007" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 16–30 in 2–3 series, (red-tinged) lanceolate or oblong to linear, hyaline margins 0.05–0.1 mm wide, faces glabrous.</text>
      <biological_entity id="o17008" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o17009" from="16" name="quantity" src="d0_s6" to="30" />
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s6" to="linear" />
      </biological_entity>
      <biological_entity id="o17009" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o17010" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s6" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17011" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles not bristly.</text>
      <biological_entity id="o17012" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 47–99;</text>
      <biological_entity id="o17013" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="47" name="quantity" src="d0_s8" to="99" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas medium yellow, 11–20 mm;</text>
      <biological_entity id="o17014" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 5–10 mm.</text>
      <biological_entity constraint="outer" id="o17015" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae usually cylindro-fusiform, sometimes weakly prismatic, 1.5–2.2 mm, ribs extending to apices, ± equal;</text>
      <biological_entity id="o17016" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="cylindro-fusiform" value_original="cylindro-fusiform" />
        <character is_modifier="false" modifier="sometimes weakly" name="shape" src="d0_s11" value="prismatic" value_original="prismatic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17017" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o17018" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <relation from="o17017" id="r1531" name="extending to" negation="false" src="d0_s11" to="o17018" />
    </statement>
    <statement id="d0_s12">
      <text>persistent pappi 0.</text>
      <biological_entity id="o17019" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 3-porate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o17020" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="3-porate" value_original="3-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17021" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10(–100) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Dunedelion</other_name>
  <other_name type="common_name">dune malacothrix</other_name>
  <discussion>Malacothrix incana, a dune endemic, grows currently on the coastal mainland in Santa Barbara and San Luis Obispo counties, and on San Miguel, San Nicolas, Santa Cruz, and Santa Rosa islands. Its nomenclatural type specimen was collected at San Diego, probably from the Silver Strand dune areas on Coronado Island.</discussion>
  <discussion>Populations of a glabrous form, var. succulenta, occur in Santa Barbara County (e.g., Casmalia Beach) and San Luis Obispo County (e.g., west of Oso Flaco Lake). Glabrous forms and tomentose forms grow together on San Miguel and San Nicolas islands.</discussion>
  <discussion>Extensive hybridization between Malacothrix incana and M. foliosa subsp. polycephala occurs on San Nicolas Island where dunes have extended into areas of normal soil, particularly along the western and southwestern portions of the island.</discussion>
  <discussion>Hybridization between Malacothrix incana and M. saxatilis var. implicata occurs on San Miguel Island on east-facing slopes above Cuyler Harbor.</discussion>
  
</bio:treatment>