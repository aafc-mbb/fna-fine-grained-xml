<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">57</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">rudbeckia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">hirta</taxon_name>
    <taxon_name authority="(T. V. Moore) Perdue" date="1958" rank="variety">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>59: 296. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section rudbeckia;species hirta;variety angustifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068693</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rudbeckia</taxon_name>
    <taxon_name authority="T. V. Moore" date="unknown" rank="species">floridana</taxon_name>
    <taxon_name authority="T. V. Moore" date="unknown" rank="variety">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 176. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rudbeckia;species floridana;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rudbeckia</taxon_name>
    <taxon_name authority="T. V. Moore" date="unknown" rank="species">divergens</taxon_name>
    <taxon_hierarchy>genus Rudbeckia;species divergens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials.</text>
      <biological_entity id="o7401" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched mostly at or near mid heights, leafy toward bases.</text>
      <biological_entity id="o7404" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="at or near bases" constraintid="o7405" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o7405" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="position" src="d0_s1" value="mid" value_original="mid" />
        <character is_modifier="true" name="character" src="d0_s1" value="height" value_original="height" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal blades oblanceolate, 0.5–2.5 cm wide;</text>
      <biological_entity id="o7406" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o7407" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline narrowly elliptic (smaller distally);</text>
      <biological_entity id="o7408" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o7409" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire or serrate;</text>
      <biological_entity id="o7410" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7411" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>faces hispid to ± sericeous.</text>
      <biological_entity id="o7412" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o7413" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s5" to="more or less sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles at least 1/2 plant heights.</text>
      <biological_entity id="o7414" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>2n = 38.</text>
      <biological_entity id="o7415" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7416" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, pastures, old fields, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–80 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="80" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19a.</number>
  <discussion>Variety angustifolia grows along the Gulf coastal plain.</discussion>
  
</bio:treatment>