<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="treatment_page">352</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">aphanostephus</taxon_name>
    <taxon_name authority="(de Candolle) Trelease" date="1891" rank="species">skirrhobasis</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Arkansas Geol. Surv.</publication_title>
      <place_in_publication>4: 191. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus aphanostephus;species skirrhobasis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242416076</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Keerlia</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">skirrhobasis</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 310. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Keerlia;species skirrhobasis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–45 cm;</text>
      <biological_entity id="o29092" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>vestiture hirsuto-pilose and/or puberulent, stem hairs spreading to deflexed, 0.1–0.3 mm.</text>
      <biological_entity id="o29093" name="vestiture" name_original="vestiture" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsuto-pilose" value_original="hirsuto-pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="stem" id="o29094" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="deflexed" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s1" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllary apices acute to short-acuminate.</text>
      <biological_entity constraint="phyllary" id="o29095" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray-florets (10–) 25–45.</text>
      <biological_entity id="o29096" name="ray-floret" name_original="ray-florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s3" to="25" to_inclusive="false" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s3" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc-floret corollas: bases (mature) bulbous-swollen, indurate.</text>
      <biological_entity constraint="disc-floret" id="o29097" name="corolla" name_original="corollas" src="d0_s4" type="structure" />
      <biological_entity id="o29098" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="bulbous-swollen" value_original="bulbous-swollen" />
        <character is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae: hairs apically 1-directionally coiled;</text>
      <biological_entity id="o29099" name="cypsela" name_original="cypselae" src="d0_s5" type="structure" />
      <biological_entity id="o29100" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="apically 1-directionally" name="architecture" src="d0_s5" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi usually coroniform, uneven, rarely of acute to awn-tipped scales 0.2–2 mm.</text>
      <biological_entity id="o29101" name="cypsela" name_original="cypselae" src="d0_s6" type="structure" />
      <biological_entity id="o29102" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="coroniform" value_original="coroniform" />
        <character is_modifier="false" name="variability" src="d0_s6" value="uneven" value_original="uneven" />
      </biological_entity>
      <biological_entity id="o29103" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="awn-tipped" value_original="awn-tipped" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o29102" id="r2690" modifier="rarely" name="consists_of" negation="false" src="d0_s6" to="o29103" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Fla., Kans., La., N.Mex., Okla., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Arkansas lazydaisy</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves thickened, densely, almost felty gray-hairy; sand dunes along Gulf Coast</description>
      <determination>1c Aphanostephus skirrhobasis var. thalassius</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves not strongly thickened or felty gray-hairy; sandy soils, interior localities</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappi of acute or awn-tipped scales or setiform scales 0.4–2 mm</description>
      <determination>1a Aphanostephus skirrhobasis var. kidderi</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappi coroniform, uneven, ciliate, 0.1–0.3 mm, rarely of awn-tipped scales</description>
      <determination>1b Aphanostephus skirrhobasis var. skirrhobasis</determination>
    </key_statement>
  </key>
</bio:treatment>