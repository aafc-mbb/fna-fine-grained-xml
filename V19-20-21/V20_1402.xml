<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="treatment_page">625</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Rafinesque" date="1817" rank="genus">arnoglossum</taxon_name>
    <taxon_name authority="(Hooker) H. Robinson" date="1980" rank="species">reniforme</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>46: 441. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus arnoglossum;species reniforme</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066137</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="(Linnaeus) Hooker" date="unknown" rank="species">atriplicifolius</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">reniformis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 332. 1834,</place_in_publication>
      <other_info_on_pub>based on Cacalia reniformis Muhlenberg ex Willdenow, Sp. Pl. 3: 1735. 1803, not Lamarck 1778</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species atriplicifolius;variety reniformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Conophora</taxon_name>
    <taxon_name authority="(Hooker) Nieuwland" date="unknown" rank="species">reniformis</taxon_name>
    <taxon_hierarchy>genus Conophora;species reniformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mesadenia</taxon_name>
    <taxon_name authority="(Hooker) Rafinesque" date="unknown" rank="species">reniformis</taxon_name>
    <taxon_hierarchy>genus Mesadenia;species reniformis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="(Hooker) MacMillan" date="unknown" rank="species">reniformis</taxon_name>
    <taxon_hierarchy>genus S.;species reniformis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 100–300 cm.</text>
      <biological_entity id="o1594" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ridged to angulate (usually glabrous, rarely sparsely puberulent distally, not glaucous).</text>
      <biological_entity id="o1595" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ridged" name="shape" src="d0_s1" to="angulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: (petioles to 30 cm) blades (with 3–5 main veins) reniform or ovate-cordate, to 40+ cm, margins lobed or dentate.</text>
      <biological_entity constraint="basal" id="o1596" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1597" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-cordate" value_original="ovate-cordate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1598" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves: proximal petiolate, blades ovate, margins entire, serrate, or dentate;</text>
      <biological_entity constraint="cauline" id="o1599" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o1600" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o1601" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o1602" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal petiolate or sessile, smaller (rarely hairy).</text>
      <biological_entity constraint="cauline" id="o1603" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o1604" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="size" src="d0_s4" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres (8–) 10–13 (–14) mm.</text>
      <biological_entity id="o1605" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries pale green, narrowly ovate, midveins not winged (tips obtuse).</text>
      <biological_entity id="o1606" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o1607" name="midvein" name_original="midveins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Corollas white or greenish, 8–9 (–10.5) mm.</text>
      <biological_entity id="o1608" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae fusiform or clavate, 4–5 mm (dark-brown or purple, 4–5-ribbed);</text>
      <biological_entity id="o1609" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s8" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 6–8 mm. 2n = 50.</text>
      <biological_entity id="o1610" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1611" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="50" value_original="50" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid May–early Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Sep" from="mid May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Ga., Ill., Ind., Iowa, Ky., Md., Minn., Miss., Mo., N.J., N.C., Ohio, Okla., Pa., Tenn., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Arnoglossum muehlenbergii of H. Robinson, Cacalia muhlenbergii of Fernald, and Mesadenia muhlenbergii of Rydberg were based on Senecio muehlenbergii Schultz-Bipontinus, a superfluous name based on Cacalia reniformis Muhlenberg ex Willdenow.</discussion>
  
</bio:treatment>