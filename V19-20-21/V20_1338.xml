<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">597</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Rydberg) W. A. Weber &amp; Á. Löve" date="1981" rank="species">pseudaurea</taxon_name>
    <taxon_name authority="(Mackenzie &amp; Bush) Trock &amp; T. M. Barkley" date="1998" rank="variety">semicordata</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>18: 386. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species pseudaurea;variety semicordata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068619</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Mackenzie &amp; Bush" date="unknown" rank="species">semicordatus</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>16: 107. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species semicordatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">pseudaureus</taxon_name>
    <taxon_name authority="(Mackenzie &amp; Bush) T. M. Barkley" date="unknown" rank="variety">semicordatus</taxon_name>
    <taxon_hierarchy>genus Senecio;species pseudaureus;variety semicordatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–40+ cm (slender).</text>
      <biological_entity id="o29311" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: petiole lengths 1.5–2 times blades;</text>
      <biological_entity constraint="basal" id="o29312" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o29313" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character constraint="blade" constraintid="o29314" is_modifier="false" name="length" src="d0_s1" value="1.5-2 times blades" value_original="1.5-2 times blades" />
      </biological_entity>
      <biological_entity id="o29314" name="blade" name_original="blades" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blades broadly lanceolate to ovate, 20–40 × 10–20 mm, margins crenate.</text>
      <biological_entity constraint="basal" id="o29315" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o29316" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29317" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 6–12 in open or slightly congested, corymbiform or cymiform arrays.</text>
      <biological_entity id="o29318" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o29319" from="6" name="quantity" src="d0_s3" to="12" />
      </biological_entity>
      <biological_entity id="o29319" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="true" modifier="slightly" name="architecture" src="d0_s3" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 4–6 mm. 2n = 88, 126, 138.</text>
      <biological_entity id="o29320" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29321" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="88" value_original="88" />
        <character name="quantity" src="d0_s4" value="126" value_original="126" />
        <character name="quantity" src="d0_s4" value="138" value_original="138" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early Apr–mid Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Jun" from="early Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp areas and open prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="damp" />
        <character name="habitat" value="open prairies" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Iowa, Kans., Minn., Mo., Nebr., N.Dak., S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>43c.</number>
  <discussion>Variety semicordata resembles var. flavula; it grows in open prairies.</discussion>
  
</bio:treatment>