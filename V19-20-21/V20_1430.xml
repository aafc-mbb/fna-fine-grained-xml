<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert D. Dorn</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
    <other_info_on_meta type="treatment_page">634</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Dorn" date="1991" rank="genus">YERMO</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>38: 199, fig. 1. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus YERMO</taxon_hierarchy>
    <other_info_on_name type="etymology">Spanish, an uninhabited and utterly inhospitable place</other_info_on_name>
    <other_info_on_name type="fna_id">135211</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–30+ cm (taprooted).</text>
      <biological_entity id="o21737" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems single or clustered, erect.</text>
      <biological_entity id="o21738" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline (smaller distally);</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o21739" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (leathery) palmately 3-nerved (nerves ± parallel), lanceolate to ovate or obovate, margins entire or ± toothed, faces glabrous.</text>
      <biological_entity id="o21740" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovate or obovate" />
      </biological_entity>
      <biological_entity id="o21741" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o21742" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, (25–180) in corymbiform to paniculiform arrays (terminal and in distal leaf-axils).</text>
      <biological_entity id="o21743" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character char_type="range_value" constraint="in arrays" constraintid="o21744" from="25" name="atypical_quantity" src="d0_s6" to="180" />
      </biological_entity>
      <biological_entity id="o21744" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0 or bractlets 1–3+.</text>
      <biological_entity id="o21745" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21746" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindric, 3–5 mm diam.</text>
      <biological_entity id="o21747" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries persistent, (4–) 5 (–6) in 1–2 series (bright-yellow, drying pale, with greenish yellow midribs ± keeled in life), erect, distinct, oblanceolate or lanceolate to lance-linear, equal, margins scarious (apices cucullate, faces glabrous).</text>
      <biological_entity id="o21748" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="6" />
        <character constraint="in series" constraintid="o21749" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="lance-linear" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o21749" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o21750" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat (sometimes with central cusp), smooth, epaleate.</text>
      <biological_entity id="o21751" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 0.</text>
      <biological_entity id="o21752" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets (4–) 5 (–6), bisexual, fertile;</text>
      <biological_entity id="o21753" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes longer than throats, lobes 5, spreading, linear;</text>
      <biological_entity id="o21754" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21755" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than throats" constraintid="o21756" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o21756" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o21757" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branches: stigmatic areas continuous, apices rounded-truncate (micro-characters tussilaginoid).</text>
      <biological_entity id="o21758" name="style-branch" name_original="style-branches" src="d0_s14" type="structure" />
      <biological_entity constraint="stigmatic" id="o21759" name="area" name_original="areas" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o21760" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded-truncate" value_original="rounded-truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (brown) ellipsoid to oblanceoloid, slightly flattened, usually 10-ribbed, often short-hairy;</text>
      <biological_entity id="o21761" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s15" to="oblanceoloid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s15" value="10-ribbed" value_original="10-ribbed" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s15" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi tardily falling, of 70+, whitish, barbellulate bristles.</text>
      <biological_entity id="o21762" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s16" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o21763" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="70" is_modifier="true" name="quantity" src="d0_s16" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <relation from="o21762" id="r2007" name="consist_of" negation="false" src="d0_s16" to="o21763" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>239.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>