<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="treatment_page">477</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">eutrochium</taxon_name>
    <taxon_name authority="(E. E. Lamont) E. E. Lamont" date="2004" rank="species">steelei</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 902. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus eutrochium;species steelei</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066770</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="E. E. Lamont" date="unknown" rank="species">steelei</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>42: 279, fig. 1. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species steelei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatoriadelphus</taxon_name>
    <taxon_name authority="(E. E. Lamont) G. J. Schmidt &amp; E. E. Schilling" date="unknown" rank="species">steelei</taxon_name>
    <taxon_hierarchy>genus Eupatoriadelphus;species steelei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–200 cm.</text>
      <biological_entity id="o24575" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually greenish purple, sometimes evenly purplish, solid, usually glandular-pubescent throughout, sometimes densely puberulent and sparingly glandular.</text>
      <biological_entity id="o24576" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="greenish purple" value_original="greenish purple" />
        <character is_modifier="false" modifier="sometimes evenly" name="coloration" src="d0_s1" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sometimes densely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sparingly" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly in 3s–4s;</text>
      <biological_entity id="o24577" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petioles (0.7–) 1.3–2.8 (–3.6) mm, glabrate to densely ciliate;</text>
      <biological_entity id="o24578" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s3" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s3" to="densely ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades pinnately veined, lanceovate or ovate to deltate-ovate, mostly 7–30 × 2.5–18 cm, relatively firm, bases abruptly or gradually tapered, margins sharply serrate, abaxial faces ± glandular-pubescent and sparsely hirsute (at least midribs and main veins), adaxial faces scabrous-hirsute, glabrescent.</text>
      <biological_entity id="o24579" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="deltate-ovate" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="18" to_unit="cm" />
        <character is_modifier="false" modifier="mostly; relatively" name="texture" src="d0_s4" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o24580" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o24581" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24582" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24583" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous-hirsute" value_original="scabrous-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in loose, convex, compound corymbiform arrays.</text>
      <biological_entity id="o24584" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o24585" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s5" value="loose" value_original="loose" />
        <character is_modifier="true" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o24584" id="r1674" name="in" negation="false" src="d0_s5" to="o24585" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres often purplish, 6.5–9 × 3.5–5 mm.</text>
      <biological_entity id="o24586" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries mostly glabrous, outer sometimes hairy on midveins.</text>
      <biological_entity id="o24587" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o24588" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character constraint="on midveins" constraintid="o24589" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o24589" name="midvein" name_original="midveins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Florets (5–) 6–9 (–10);</text>
      <biological_entity id="o24590" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s8" to="6" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="10" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas usually pale pinkish or purplish, 4.5–7 mm.</text>
      <biological_entity id="o24591" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="pale pinkish" value_original="pale pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 3–4.5 mm. 2n = 20.</text>
      <biological_entity id="o24592" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24593" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly embankments, open woods, thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly embankments" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ky., N.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Eutrochium steelei is known from the Blue Ridge Province of western North Carolina and eastern Tennessee, the Ridge and Valley Province of southwestern Virginia, and the Appalachian Plateaus Province of eastern Kentucky.</discussion>
  
</bio:treatment>