<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">504</other_info_on_meta>
    <other_info_on_meta type="mention_page">522</other_info_on_meta>
    <other_info_on_meta type="treatment_page">531</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">artemisia</taxon_name>
    <taxon_name authority="J. W. Grimes &amp; Ertter" date="1979" rank="species">packardiae</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>31: 454, fig. 1. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus artemisia;species packardiae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066155</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–50 (–60) cm, strongly aromatic (rhizomatous, fibrous-rooted).</text>
      <biological_entity id="o5496" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="strongly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–20, erect, light-brown, simple or branched, glabrous.</text>
      <biological_entity id="o5497" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="20" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, dark green;</text>
      <biological_entity id="o5498" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark green" value_original="dark green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades lanceolate, 1.5–5 × 1–2.5 cm, 2-pinnatifid (primary lobes 5–9, 0.4–1.5 cm; cauline smaller, pinnatifid to entire), faces tomentose (abaxial) or glabrous (adaxial).</text>
      <biological_entity id="o5499" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="2-pinnatifid" value_original="2-pinnatifid" />
      </biological_entity>
      <biological_entity id="o5500" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (peduncles 0 or to 3 mm) in usually paniculiform, sometimes racemiform, arrays 5–20 × 1.5–4 cm.</text>
      <biological_entity id="o5501" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" modifier="in usually paniculiform , sometimes racemiform , arrays" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" modifier="in usually paniculiform , sometimes racemiform , arrays" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate to hemispheric, 2.5–3.5 × 2–4.5 mm.</text>
      <biological_entity id="o5502" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s5" to="hemispheric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries broadly ovate, glandular (at least at bases).</text>
      <biological_entity id="o5503" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 3–8;</text>
      <biological_entity id="o5504" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual, sometimes functionally staminate, (15–) 20–35;</text>
      <biological_entity id="o5505" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="sometimes functionally" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="15" name="atypical_quantity" src="d0_s8" to="20" to_inclusive="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas bright-yellow, 1.3–2.2 mm, glandular.</text>
      <biological_entity id="o5506" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o5507" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (light-brown) ellipsoid (± arcuate, ribs 4, prominent), ca. 1 mm, glandular.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o5508" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5509" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coarse taluses, alkaline soils, erosion gullies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coarse taluses" />
        <character name="habitat" value="alkaline soils" />
        <character name="habitat" value="erosion gullies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>41.</number>
  <other_name type="common_name">Succor Creek mugwort</other_name>
  <discussion>Artemisia packardiae is known only from southeastern Oregon, western Idaho, and northeastern Nevada. It is closely related to A. michauxiana and could be considered an ecologic variant.</discussion>
  
</bio:treatment>