<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="A. Gray in J. C. Frémont" date="1845" rank="genus">nicolletia</taxon_name>
    <taxon_name authority="A. Gray in J. C. Frémont" date="1845" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Frémont, Rep. Exped. Rocky Mts.,</publication_title>
      <place_in_publication>316. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus nicolletia;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="fna_id">220009239</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (5–) 10–29 cm (deeply taprooted).</text>
      <biological_entity id="o20923" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="29" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o20924" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 20–70 mm overall, lobes 5–11, ± quadrate to linear, rachis widths mostly 2–3 times lobe widths.</text>
      <biological_entity id="o20925" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="distance" src="d0_s2" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20926" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="11" />
        <character char_type="range_value" from="less quadrate" name="shape" src="d0_s2" to="linear" />
      </biological_entity>
      <biological_entity id="o20927" name="rachis" name_original="rachis" src="d0_s2" type="structure">
        <character constraint="lobe" constraintid="o20928" is_modifier="false" modifier="mostly" name="width" src="d0_s2" value="2-3 times lobe widths" value_original="2-3 times lobe widths" />
      </biological_entity>
      <biological_entity id="o20928" name="lobe" name_original="lobe" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 2–10 mm.</text>
      <biological_entity id="o20929" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 2–4 bractlets 4–8 mm.</text>
      <biological_entity id="o20930" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o20931" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="4" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o20930" id="r1439" name="consist_of" negation="false" src="d0_s4" to="o20931" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate to cylindric, 14–18 mm.</text>
      <biological_entity id="o20932" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s5" to="cylindric" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8–12, linear to ovate.</text>
      <biological_entity id="o20933" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="12" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 8–12, laminae 4–9 × 2.5–4 mm.</text>
      <biological_entity id="o20934" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
      <biological_entity id="o20935" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets (30–) 60–100+;</text>
      <biological_entity id="o20936" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" name="atypical_quantity" src="d0_s8" to="60" to_inclusive="false" />
        <character char_type="range_value" from="60" name="quantity" src="d0_s8" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, purple-tipped, 8–9.5 mm.</text>
      <biological_entity id="o20937" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="purple-tipped" value_original="purple-tipped" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 7–9 mm;</text>
      <biological_entity id="o20938" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: bristles 3–7 mm, scales 6–8 mm. 2n = 20.</text>
      <biological_entity id="o20939" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o20940" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20941" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20942" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep sands of fans and floors of desert washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep sands" constraint="of fans and floors" />
        <character name="habitat" value="fans" />
        <character name="habitat" value="floors" />
        <character name="habitat" value="desert washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Hole-in-the-sand plant</other_name>
  
</bio:treatment>