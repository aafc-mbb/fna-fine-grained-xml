<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="treatment_page">498</other_info_on_meta>
    <other_info_on_meta type="illustration_page">496</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="(Linnaeus) Shinners" date="1971" rank="species">eupatorioides</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>4: 274. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species eupatorioides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416187</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kuhnia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">eupatorioides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 2: 1662. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Kuhnia;species eupatorioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–200 cm (bases woody).</text>
      <biological_entity id="o19688" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, pubescent.</text>
      <biological_entity id="o19689" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly opposite (alternate in vars. gracillima and texana);</text>
      <biological_entity id="o19690" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 0–10 mm;</text>
      <biological_entity id="o19691" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1-nerved or 3-nerved from bases, lanceolate, lance-linear, lanceovate, lance-rhombic, linear, or oblong, 25–100 × 0.5–40 mm, bases acute, margins entire or ± dentate (often revolute), apices obtuse to acuminate, faces glandular-pubescent.</text>
      <biological_entity id="o19692" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-nerved" value_original="1-nerved" />
        <character constraint="from bases" constraintid="o19693" is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lance-rhombic" value_original="lance-rhombic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19693" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o19694" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o19695" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19696" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity id="o19697" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in paniculiform or corymbiform arrays.</text>
      <biological_entity id="o19698" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o19699" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o19698" id="r1340" name="in" negation="false" src="d0_s5" to="o19699" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–100 mm, glandular-pubescent.</text>
      <biological_entity id="o19700" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="100" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to narrowly campanulate, 7–15 mm.</text>
      <biological_entity id="o19701" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="narrowly campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 22–26 in 4–6 series, green to stramineous, sometimes purple-tinged, 3–7-striate, unequal, margins scarious (often ciliate);</text>
      <biological_entity id="o19702" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o19703" from="22" name="quantity" src="d0_s8" to="26" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s8" to="stramineous" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s8" value="3-7-striate" value_original="3-7-striate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o19703" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o19704" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer ovate to lanceovate (puberulent, often densely glanddotted, apices acute to acuminate), inner lanceolate (± glanddotted, apices obtuse to aristate).</text>
      <biological_entity constraint="outer" id="o19705" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o19706" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 6–35;</text>
      <biological_entity id="o19707" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas pale-yellow, yellow-green, pinkish lavender, or maroon, 4.5–6 mm.</text>
      <biological_entity id="o19708" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pinkish lavender" value_original="pinkish lavender" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pinkish lavender" value_original="pinkish lavender" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2.7–5.5 mm, glabrous or strigose, sometimes hispidulous or velutinous and/or glanddotted;</text>
      <biological_entity id="o19709" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s12" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="velutinous" value_original="velutinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 20–28 white or tawny, usually plumose or subplumose, sometimes barbellate, bristles.</text>
      <biological_entity id="o19710" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o19711" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s13" to="28" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="tawny" value_original="tawny" />
        <character is_modifier="true" modifier="usually" name="shape" src="d0_s13" value="plumose" value_original="plumose" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="subplumose" value_original="subplumose" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <relation from="o19710" id="r1341" name="consist_of" negation="false" src="d0_s13" to="o19711" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Colo., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mich., Minn., Miss., Mo., Mont., N.C., N.Dak., N.J., N.Mex., Nebr., Ohio, Okla., Pa., S.C., S.Dak., Tenn., Tex., Utah, Va., W.Va., Wis.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>Varieties 6 (6 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades lance-linear or linear, 0.5–3 mm wide</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades lanceolate, lance-linear, lance-ovate, or lance-rhombic, 1–40 mm wide</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Florets 9–13</description>
      <determination>13d Brickellia eupatorioides var. floridana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Florets 16–30</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 1–3 mm wide (lengths of proximal leaves greater than distals); involucres 8–13 mm</description>
      <determination>13b Brickellia eupatorioides var. chlorolepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 0.5–1 mm wide (lengths of proximal leaves equaling distals); involucres 5–10 mm</description>
      <determination>13e Brickellia eupatorioides var. gracillima</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries: lengths of outers and mids 1/2–1 times inners, apices long-acuminate, usuallycontorted</description>
      <determination>13f Brickellia eupatorioides var. texana</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries: lengths of outers and mids to 1/2 times inners, apices obtuse, acute, or acuminate, not contorted</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves 1-nerved from bases; petioles 0–1 mm; heads borne singly or 2–3 in loose, paniculiform arrays</description>
      <determination>13b Brickellia eupatorioides var. chlorolepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves 1- or 3-nerved from bases; petioles 0–10 mm; heads mostly 3–8 in dense, corymbiform arrays.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Involucres 7–11 mm; florets 6–15</description>
      <determination>13a Brickellia eupatorioides var. eupatorioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Involucres 8–15 mm; florets 15–35</description>
      <determination>13c Brickellia eupatorioides var. corymbulosa</determination>
    </key_statement>
  </key>
</bio:treatment>