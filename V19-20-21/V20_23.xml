<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="treatment_page">31</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">baccharis</taxon_name>
    <taxon_name authority="A. Gray" date="1879" rank="species">plummerae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">plummerae</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus baccharis;species plummerae;subspecies plummerae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068085</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–200 cm.</text>
      <biological_entity id="o2918" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems villous, eglandular.</text>
      <biological_entity id="o2919" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves oblong to oblanceolate (principal 3-nerved), (15–) 20–45 (–55) × (3–) 5–13 mm, sharply serrate.</text>
    </statement>
    <statement id="d0_s3">
      <text>2n = 18.</text>
      <biological_entity id="o2920" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s2" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="45" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2921" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes near beach, sea bluffs, brushy canyons, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" constraint="near beach" />
        <character name="habitat" value="beach" />
        <character name="habitat" value="sea bluffs" />
        <character name="habitat" value="brushy canyons" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12a.</number>
  <other_name type="common_name">Plummer’s baccharis</other_name>
  <discussion>Subspecies plummerae occurs in the Santa Monica Mountains in Los Angeles County, Matilija Canyon in Ventura County, the western slopes of the Santa Ynez Mountains and Santa Cruz Island in Ventura County (R. M. Beauchamp and J. Henrickson 1996).</discussion>
  
</bio:treatment>