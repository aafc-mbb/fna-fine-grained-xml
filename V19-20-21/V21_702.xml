<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="treatment_page">286</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(de Candolle) B. G. Baldwin" date="1999" rank="species">corymbosa</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 468. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species corymbosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066465</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hartmannia</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">corymbosa</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. L. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 694. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hartmannia;species corymbosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Deinandra</taxon_name>
    <taxon_name authority="(de Candolle) Torrey &amp; A. Gray" date="unknown" rank="species">corymbosa</taxon_name>
    <taxon_name authority="(Nuttall) B. G. Baldwin" date="unknown" rank="subspecies">macrocephala</taxon_name>
    <taxon_hierarchy>genus Deinandra;species corymbosa;subspecies macrocephala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">corymbosa</taxon_name>
    <taxon_hierarchy>genus Hemizonia;species corymbosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">corymbosa</taxon_name>
    <taxon_name authority="(Nuttall) D. D. Keck" date="unknown" rank="subspecies">macrocephala</taxon_name>
    <taxon_hierarchy>genus Hemizonia;species corymbosa;subspecies macrocephala;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 6–100 cm.</text>
      <biological_entity id="o20649" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid.</text>
      <biological_entity id="o20650" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades pinnatifid, faces ± hirsute to villous and sometimes stipitate-glandular.</text>
      <biological_entity id="o20651" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o20652" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o20653" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="less hirsute" name="pubescence" src="d0_s2" to="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in corymbiform, racemiform, or paniculiform arrays or in glomerules.</text>
      <biological_entity id="o20654" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o20655" name="glomerule" name_original="glomerules" src="d0_s3" type="structure" />
      <relation from="o20654" id="r1419" name="in corymbiform , racemiform , or paniculiform arrays or in" negation="false" src="d0_s3" to="o20655" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads often overlapping proximal 0–1/2+ of each involucre.</text>
      <biological_entity id="o20656" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" constraint="of involucre" constraintid="o20658" from="0" name="quantity" src="d0_s4" to="1/2" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o20657" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o20658" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o20656" id="r1420" name="subtending" negation="false" src="d0_s4" to="o20657" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries evenly stipitate-glandular, including margins and apices, with nonglandular, non-pustule-based hairs as well.</text>
      <biological_entity id="o20659" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20660" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o20661" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o20662" name="nonglandular" name_original="nonglandular" src="d0_s5" type="structure" />
      <biological_entity id="o20663" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <relation from="o20659" id="r1421" name="including" negation="false" src="d0_s5" to="o20660" />
      <relation from="o20659" id="r1422" name="including" negation="false" src="d0_s5" to="o20661" />
      <relation from="o20659" id="r1423" name="with" negation="false" src="d0_s5" to="o20662" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o20664" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o20665" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o20664" id="r1424" name="in" negation="false" src="d0_s6" to="o20665" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 15–35;</text>
      <biological_entity id="o20666" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae deep yellow, 4–8 mm.</text>
      <biological_entity id="o20667" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 24–70, all functionally staminate;</text>
      <biological_entity id="o20668" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="24" name="quantity" src="d0_s9" to="70" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers reddish to dark purple.</text>
      <biological_entity id="o20669" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s10" to="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi 0, or coroniform (irregular crowns of entire, erose, or laciniate scales 0.1–0.9) mm.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity id="o20670" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20671" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal grasslands, openings in coastal scrub or woods, dunes, disturbed sites (e.g., fallow fields), sandy or clayey soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal grasslands" />
        <character name="habitat" value="openings" constraint="in coastal scrub or woods" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="fallow fields" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Deinandra corymbosa occurs on the Central Coast and the Northern Coast and in the Outer South Coast Ranges and San Francisco Bay area. Plants with relatively large heads in glomerules from the Central Coast south of Big Sur have been treated as subsp. macrocephala; no subspecies are recognized here because of wide variation in head size and arrangement along the Central Coast north and south of Big Sur.</discussion>
  
</bio:treatment>