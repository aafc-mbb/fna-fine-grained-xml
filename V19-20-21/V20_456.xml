<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">205</other_info_on_meta>
    <other_info_on_meta type="illustration_page">200</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">dichaetophora</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="species">campestris</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 73. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus dichaetophora;species campestris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220004026</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 1–4 cm.</text>
      <biological_entity id="o25" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Heads terminal.</text>
      <biological_entity id="o26" name="head" name_original="heads" src="d0_s1" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s1" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 2–9 cm.</text>
      <biological_entity id="o27" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary margins prominently ciliate.</text>
      <biological_entity constraint="phyllary" id="o28" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc-florets corollas 1.5–2 mm.</text>
      <biological_entity constraint="disc-floret" id="o29" name="corolla" name_original="corollas" src="d0_s4" type="structure" constraint_original="disc-florets">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2–2.5 mm, bodies thickened, narrowly elliptic-lanceolate to elliptic.</text>
      <biological_entity id="o30" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>2n = 6.</text>
      <biological_entity id="o31" name="body" name_original="bodies" src="d0_s5" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="narrowly elliptic-lanceolate" name="shape" src="d0_s5" to="elliptic" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites, sandy soils, roadsides, mesquite savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="savannas" modifier="mesquite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Plainsdaisy</other_name>
  
</bio:treatment>