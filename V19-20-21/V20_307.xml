<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="treatment_page">145</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(G. Don) G. L. Nesom" date="1993" rank="subsection">Venosae</taxon_name>
    <taxon_name authority="unknown" date="1830" rank="series">venosae</taxon_name>
    <taxon_name authority="Small" date="1898" rank="species">delicatula</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 474. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection venosae;series venosae;species delicatula;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067540</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Engelmann ex Small" date="unknown" rank="species">microphylla</taxon_name>
    <taxon_hierarchy>genus Solidago;species microphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Muhlenberg ex Willdenow" date="unknown" rank="species">ulmifolia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">microphylla</taxon_name>
    <taxon_hierarchy>genus Solidago;species ulmifolia;variety microphylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–120 cm;</text>
      <biological_entity id="o6859" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices compact, branching, woody.</text>
      <biological_entity id="o6860" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, essentially glabrous.</text>
      <biological_entity id="o6861" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline tapering to short petioles, blades oblanceolate, 50–70 × 10–20 mm, margins serrate, scabroso-strigose, apices acute to acuminate, faces glabrous;</text>
      <biological_entity id="o6862" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6863" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o6864" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6865" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabroso-strigose" value_original="scabroso-strigose" />
      </biological_entity>
      <biological_entity id="o6866" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o6867" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid and distal cauline subpetiolate or sessile, blades elliptic-lanceolate, 30–70 × 7–15 mm, gradually reduced distally, tapering to bases, margins serrate, scabroso-strigose;</text>
      <biological_entity id="o6868" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6869" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character constraint="to bases" constraintid="o6870" is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6870" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o6871" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabroso-strigose" value_original="scabroso-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branch leaves similar, reduced to bracts distally.</text>
      <biological_entity id="o6872" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="branch" id="o6873" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6874" name="bract" name_original="bracts" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 160–480 in paniculiform arrays, with a strongly secund, primary, arching axis and nearly always 4–5 (–8) leafy, elongate, arching, secund, proximal branches.</text>
      <biological_entity id="o6875" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o6876" from="160" name="quantity" src="d0_s6" to="480" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="arching" value_original="arching" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o6876" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o6877" name="axis" name_original="axis" src="d0_s6" type="structure" constraint="primary">
        <character is_modifier="true" modifier="strongly" name="architecture" src="d0_s6" value="secund" value_original="secund" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="arching" value_original="arching" />
        <character char_type="range_value" from="5" from_inclusive="false" modifier="nearly always; always" name="atypical_quantity" src="d0_s6" to="8" />
        <character char_type="range_value" from="4" modifier="nearly always; always" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6878" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <relation from="o6875" id="r624" name="with" negation="false" src="d0_s6" to="o6877" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 2–5 mm, bracteolate, glabrous;</text>
      <biological_entity id="o6879" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="bracteolate" value_original="bracteolate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles grading into phyllaries.</text>
      <biological_entity id="o6880" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure" />
      <biological_entity id="o6881" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o6880" id="r625" name="into" negation="false" src="d0_s8" to="o6881" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres narrowly campanulate, 3–5 mm.</text>
      <biological_entity id="o6882" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, linear-lanceolate, strongly unequal, acute to ± attenuate, glabrous.</text>
      <biological_entity id="o6883" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s10" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="more or less attenuate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6884" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <relation from="o6883" id="r626" name="in" negation="false" src="d0_s10" to="o6884" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 1–4;</text>
      <biological_entity id="o6885" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 1–2 × 0.5–0.8 mm.</text>
      <biological_entity id="o6886" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 4–6;</text>
      <biological_entity id="o6887" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2.5 mm, lobes 1 mm.</text>
      <biological_entity id="o6888" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity id="o6889" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 1.5–2 mm (6–9 ribs), sparsely strigose, more so apically;</text>
      <biological_entity id="o6890" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 1.5–2 mm. 2n = 18.</text>
      <biological_entity id="o6891" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6892" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy and alluvial soils, dry open woods, banks of shaded creeks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="alluvial soils" />
        <character name="habitat" value="dry open woods" />
        <character name="habitat" value="banks" constraint="of shaded creeks" />
        <character name="habitat" value="shaded creeks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Kans., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>50.</number>
  <other_name type="common_name">Smooth elm-leaf goldenrod</other_name>
  <discussion>Solidago delicatula is similar to S. ulmifolia, but is essentially glabrous (except for leaf margins) with smaller, more numerous, less conspicuously veiny leaves. It is sufficiently distinct from S. ulmifolia that inclusion in that species as var. microphylla does not appear warranted. Reports from Alabama, western Florida, Louisiana, and Mississippi are likely just smaller-leaved S. ulmifolia. Solidago helleri Small may be a hybrid between S. delicatula and S. ulmifolia.</discussion>
  
</bio:treatment>