<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="treatment_page">133</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Mackenzie) G. L. Nesom" date="1993" rank="subsection">Argutae</taxon_name>
    <taxon_name authority="unknown" date="1993" rank="series">argutae</taxon_name>
    <taxon_name authority="Mackenzie ex Small" date="1933" rank="species">tarda</taxon_name>
    <place_of_publication>
      <publication_title>Man. S.E. Fl.,</publication_title>
      <place_in_publication>1355, 1509. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection argutae;series argutae;species tarda;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067579</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0–180 cm, caudex or slender rhizomes.</text>
      <biological_entity id="o9395" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9396" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity constraint="slender" id="o9397" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5+, glabrous, sparsely strigose in arrays.</text>
      <biological_entity id="o9398" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="in arrays" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal petiolate;</text>
      <biological_entity id="o9399" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o9400" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades broadly elliptic to ovate, 100–350 × 60–120 mm (including petioles), bases truncate to obtuse, apices acute to acuminate, margins sharply serrate, glabrous;</text>
      <biological_entity id="o9401" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9402" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s3" to="350" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="width" src="d0_s3" to="120" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9403" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity id="o9404" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o9405" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal cauline blades spreading to ascending, linear-elliptic, 30–50 × 6–15 mm.</text>
      <biological_entity id="o9406" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal cauline" id="o9407" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="ascending" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="linear-elliptic" value_original="linear-elliptic" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 25–50+, in elongate, paniculiform arrays, proximal branches recurved-secund, sometimes elongate.</text>
      <biological_entity id="o9408" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s5" to="50" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9409" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9410" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="recurved-secund" value_original="recurved-secund" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o9408" id="r873" name="in" negation="false" src="d0_s5" to="o9409" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1.5–3 mm, glabrate to moderately short-hispido-strigose, bracteoles 1–5, linear-lanceolate grading into phyllaries.</text>
      <biological_entity id="o9411" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s6" to="moderately short-hispido-strigose" />
      </biological_entity>
      <biological_entity id="o9412" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
        <character constraint="into phyllaries" constraintid="o9413" is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o9413" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, (4.5–) 5–7 mm (much exceeded by pappi).</text>
      <biological_entity id="o9414" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3–4 series, linear-lanceolate, strongly unequal.</text>
      <biological_entity id="o9415" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o9416" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o9415" id="r874" name="in" negation="false" src="d0_s8" to="o9416" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 4–9;</text>
      <biological_entity id="o9417" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae 4–5 × 1.5–2.5 mm.</text>
      <biological_entity id="o9418" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 9–11;</text>
      <biological_entity id="o9419" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s11" to="11" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 4–5 mm, lobes ca. 1.5 mm.</text>
      <biological_entity id="o9420" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9421" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae (brown, ribs dark-brown) 3 mm, sparsely short-strigose;</text>
      <biological_entity id="o9422" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="short-strigose" value_original="short-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 3–4 mm. 2n = 54.</text>
      <biological_entity id="o9423" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9424" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils in xeric places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" constraint="in xeric places" />
        <character name="habitat" value="xeric places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., Fla., Ga., Md., N.J., N.C., Pa., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <discussion>Solidago tarda requires a more xeric environment than S. arguta; it is found mostly on coastal plains. A. Cronquist (1980, citing G. H. Morton, pers. comm.) noted that some plants from northern Florida and southern Georgia had narrower, basally more tapering proximal leaves; some of those plants were tetraploid. The proper taxonomic status of those plants is uncertain.</discussion>
  
</bio:treatment>