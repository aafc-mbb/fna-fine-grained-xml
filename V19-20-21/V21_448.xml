<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="treatment_page">182</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">dimeresiinae</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1886" rank="genus">DIMERESIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 1(2): 448. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe dimeresiinae;genus DIMERESIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek dimeres, in 2 parts or with 2 members, allusion unclear</other_info_on_name>
    <other_info_on_name type="fna_id">110203</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–3 (–5) cm.</text>
      <biological_entity id="o10437" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect.</text>
      <biological_entity id="o10438" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and/or cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite (crowded, seemingly whorled);</text>
    </statement>
    <statement id="d0_s4">
      <text>subsessile or ± petiolate;</text>
      <biological_entity id="o10439" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades spatulate to oblanceolate, entire.</text>
      <biological_entity id="o10440" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, in clusters (of 2–5, congested, total heads to 100+).</text>
      <biological_entity id="o10441" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to obconic, 2 (–4) mm diam.</text>
      <biological_entity id="o10442" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="obconic" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="4" to_unit="mm" />
        <character name="diameter" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 2 (–3) in 1 series (basally distinct or connate, oblong, herbaceous).</text>
      <biological_entity id="o10443" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="3" />
        <character constraint="in series" constraintid="o10444" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o10444" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0.</text>
      <biological_entity id="o10445" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 2 (–3), bisexual, fertile;</text>
      <biological_entity id="o10446" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="3" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white, pink, or purple, tubes slightly shorter than funnelform throats, lobes 5, deltate-ovate.</text>
      <biological_entity id="o10447" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o10448" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character constraint="than funnelform throats" constraintid="o10449" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o10449" name="throat" name_original="throats" src="d0_s11" type="structure" />
      <biological_entity id="o10450" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s11" value="deltate-ovate" value_original="deltate-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Receptacles minute, epaleate.</text>
      <biological_entity id="o10451" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="minute" value_original="minute" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae cylindric to clavate, striate, glabrous;</text>
      <biological_entity id="o10452" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s13" to="clavate" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s13" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi falling, of ca. 20 basally connate, setiform to subulate, ± plumose scales (or flattened bristles).</text>
      <biological_entity id="o10454" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="20" value_original="20" />
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character char_type="range_value" from="setiform" is_modifier="true" name="shape" src="d0_s14" to="subulate" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s14" value="plumose" value_original="plumose" />
      </biological_entity>
      <relation from="o10453" id="r714" name="consist_of" negation="false" src="d0_s14" to="o10454" />
    </statement>
    <statement id="d0_s15">
      <text>x = 7.</text>
      <biological_entity id="o10453" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s14" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o10455" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>307.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>