<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
    <other_info_on_meta type="illustration_page">72</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">mutisieae</taxon_name>
    <taxon_name authority="P. Browne" date="1756" rank="genus">trixis</taxon_name>
    <taxon_name authority="Kellogg" date="1862" rank="species">californica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe mutisieae;genus trixis;species californica;variety californica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250068892</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–200 cm.</text>
      <biological_entity id="o2979" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually ascending, almost parallel with stems;</text>
      <biological_entity id="o2980" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="with stems" constraintid="o2981" is_modifier="false" modifier="almost" name="arrangement" src="d0_s1" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o2981" name="stem" name_original="stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>petioles winged, 1.5–5 mm;</text>
      <biological_entity id="o2982" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear-lanceolate to lanceolate, 2–11 cm, bases acute, margins entire or denticulate (often revolute), apices acute, induments: abaxial faces glandular-puberulent and sparsely strigose (sometimes only on veins), adaxial faces glandular and strigose to pilose;</text>
      <biological_entity id="o2983" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2984" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o2985" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o2986" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o2987" name="indument" name_original="induments" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o2988" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2989" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s3" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stomates on both faces.</text>
      <biological_entity id="o2990" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2991" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o2992" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o2993" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o2994" name="indument" name_original="induments" src="d0_s4" type="structure" />
      <biological_entity id="o2995" name="stomate" name_original="stomates" src="d0_s4" type="structure" />
      <biological_entity id="o2996" name="face" name_original="faces" src="d0_s4" type="structure" />
      <relation from="o2995" id="r292" name="on" negation="false" src="d0_s4" to="o2996" />
    </statement>
    <statement id="d0_s5">
      <text>Heads usually in corymbiform or paniculiform arrays, rarely borne singly.</text>
      <biological_entity id="o2997" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o2998" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o2997" id="r293" name="in" negation="false" src="d0_s5" to="o2998" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–45 mm, often ± inflated distally.</text>
      <biological_entity id="o2999" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="45" to_unit="mm" />
        <character is_modifier="false" modifier="often more or less; distally" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 5–7 linear to lanceolate or ovate bractlets 5–16 mm.</text>
      <biological_entity id="o3000" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o3001" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s7" to="lanceolate or ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
      <relation from="o3000" id="r294" name="consist_of" negation="false" src="d0_s7" to="o3001" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries usually 8, linear, 8–14 mm, apices acute.</text>
      <biological_entity id="o3002" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3003" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 11–25;</text>
      <biological_entity id="o3004" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s9" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla-tubes 6–9 mm, outer lips 5–8 mm, inner 4–5.5 mm.</text>
      <biological_entity id="o3005" name="corolla-tube" name_original="corolla-tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="distance" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3006" name="lip" name_original="lips" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3007" name="lip" name_original="lips" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 6–10.5 mm, papillalike double hairs producing mucilage when wetted;</text>
      <biological_entity id="o3008" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="papilla-like" value_original="papillalike" />
      </biological_entity>
      <biological_entity id="o3009" name="hair" name_original="hairs" src="d0_s11" type="structure" />
      <biological_entity id="o3010" name="mucilage" name_original="mucilage" src="d0_s11" type="structure" />
      <relation from="o3008" id="r295" name="double" negation="false" src="d0_s11" to="o3009" />
      <relation from="o3008" id="r296" name="producing" negation="false" src="d0_s11" to="o3010" />
    </statement>
    <statement id="d0_s12">
      <text>pappi 7.5–12 mm. 2n = 54.</text>
      <biological_entity id="o3011" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3012" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky slopes, washes, desert flats, thorn scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="dry" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="desert flats" />
        <character name="habitat" value="thorn scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1600(–2200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Tex.; Mexico (Baja California, Baja California Sur, Chihuahua, Coahuila, Durango, Nuevo León, San Luis Potosí, Sinaloa, Sonora, Tamaulipas, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>