<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="mention_page">156</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="treatment_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) G. L. Nesom" date="1993" rank="subsection">triplinerviae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">altissima</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 878. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection triplinerviae;species altissima</taxon_hierarchy>
    <other_info_on_name type="fna_id">242414377</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–200 cm;</text>
      <biological_entity id="o25856" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes short to long-creeping.</text>
      <biological_entity id="o25857" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="long-creeping" value_original="long-creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–40+, usually short-hairy throughout, sometimes proximally glabrescent.</text>
      <biological_entity id="o25858" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="40" upper_restricted="false" />
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s2" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" modifier="sometimes proximally" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal 0;</text>
      <biological_entity id="o25859" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o25860" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal cauline usually withering by flowering;</text>
      <biological_entity id="o25861" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal cauline" id="o25862" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by flowering" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sessile or subpetiolate, tapering to bases;</text>
      <biological_entity id="o25863" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subpetiolate" value_original="subpetiolate" />
        <character constraint="to bases" constraintid="o25864" is_modifier="false" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o25864" name="base" name_original="bases" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blades oblanceolate, 95–150 × 16–20 mm, relatively thick and firm, entire to serrate along distal 1/2, strongly 3-nerved, apices acute to acuminate, abaxial faces finely strigose, more so along nerves, adaxial ± scabrous;</text>
      <biological_entity id="o25865" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o25866" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="95" from_unit="mm" name="length" src="d0_s6" to="150" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s6" value="firm" value_original="firm" />
        <character constraint="along distal 1/2" constraintid="o25867" is_modifier="false" name="shape_or_architecture" src="d0_s6" value="entire to serrate" value_original="entire to serrate" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s6" value="3-nerved" value_original="3-nerved" />
      </biological_entity>
      <biological_entity constraint="distal" id="o25867" name="1/2" name_original="1/2" src="d0_s6" type="structure" />
      <biological_entity id="o25868" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25869" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o25870" name="nerve" name_original="nerves" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o25871" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o25869" id="r2397" name="along" negation="false" src="d0_s6" to="o25870" />
    </statement>
    <statement id="d0_s7">
      <text>mid to distal cauline blades oblanceolate (proximally) to lanceolate (distally), mid (30–) 45–100 (–170) × (5–) 7–16 (–25) mm, much reduced distally [(15–) 25–55 × (3–) 4.5–10 (–17) mm], margins finely serrate (teeth 0–6 (–14) per side on mid), distally usually becoming entire or remotely serrulate, adaxial faces ± scabrous, abaxial moderately strigillose, densely villoso-strigillose along nerves, distal sometimes minutely stipitate-glandular.</text>
      <biological_entity id="o25872" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="cauline" id="o25873" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="lanceolate" />
        <character is_modifier="false" name="position" src="d0_s7" value="mid" value_original="mid" />
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_length" src="d0_s7" to="45" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="170" to_unit="mm" />
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s7" to="100" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s7" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="much; distally" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o25874" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o25875" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="14" />
        <character char_type="range_value" constraint="per side" constraintid="o25876" from="0" name="quantity" src="d0_s7" to="6" />
        <character is_modifier="false" modifier="distally usually; usually becoming; becoming" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o25876" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity constraint="mid" id="o25877" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o25878" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25879" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s7" value="strigillose" value_original="strigillose" />
        <character constraint="along nerves" constraintid="o25880" is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="villoso-strigillose" value_original="villoso-strigillose" />
      </biological_entity>
      <biological_entity id="o25880" name="nerve" name_original="nerves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o25881" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o25876" id="r2398" name="on" negation="false" src="d0_s7" to="o25877" />
    </statement>
    <statement id="d0_s8">
      <text>Heads (15–) 100–1200+, secund, in secund, pyramidal, paniculiform arrays, branches divergent and recurved, sometimes ascending-divergent, sometimes merely club-shaped thyrsiform in small plants, 5–30 × 2–25 cm (often 1.5–2 times as long as wide in southern plants).</text>
      <biological_entity id="o25882" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="atypical_quantity" src="d0_s8" to="100" to_inclusive="false" />
        <character char_type="range_value" from="100" name="quantity" src="d0_s8" to="1200" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o25883" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="in secund , pyramidal , paniculiform arrays" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s8" value="ascending-divergent" value_original="ascending-divergent" />
        <character is_modifier="false" modifier="sometimes merely" name="shape" src="d0_s8" value="club--shaped" value_original="club--shaped" />
        <character constraint="in plants" constraintid="o25884" is_modifier="false" name="architecture" src="d0_s8" value="thyrsiform" value_original="thyrsiform" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" notes="" src="d0_s8" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25884" name="plant" name_original="plants" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 1–3.5 mm, moderately densely short hispiduloso-strigillose, sometimes minutely stipitate-glandular;</text>
      <biological_entity id="o25885" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="moderately densely" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispiduloso-strigillose" value_original="hispiduloso-strigillose" />
        <character is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles linear, sometimes minutely stipitate-glandular.</text>
      <biological_entity id="o25886" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres narrowly campanulate, 2.5–4.5 mm.</text>
      <biological_entity id="o25887" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Phyllaries in ca. 3 series, strongly unequal;</text>
      <biological_entity id="o25888" name="phyllarie" name_original="phyllaries" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" notes="" src="d0_s12" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o25889" name="series" name_original="series" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <relation from="o25888" id="r2399" name="in" negation="false" src="d0_s12" to="o25889" />
    </statement>
    <statement id="d0_s13">
      <text>outer lanceolate, acute, inner linear-lanceolate, margins rarely minutely stipitate-glandular, apices acute to obtuse.</text>
      <biological_entity constraint="outer" id="o25890" name="phyllarie" name_original="phyllaries" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="inner" id="o25891" name="phyllarie" name_original="phyllaries" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o25892" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="rarely minutely" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o25893" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Ray-florets (5–) 8–13 (–17);</text>
      <biological_entity id="o25894" name="ray-floret" name_original="ray-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s14" to="8" to_inclusive="false" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="17" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s14" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>laminae 0.7–1.5 (–2) × 0.1–0.4 (–0.5) mm.</text>
      <biological_entity id="o25895" name="lamina" name_original="laminae" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s15" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s15" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Disc-florets (2–) 3–6 (–9);</text>
      <biological_entity id="o25896" name="disc-floret" name_original="disc-florets" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s16" to="3" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="9" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s16" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>corollas usually 2.3–3.6 mm, lobes 0.5–0.9 (–1.2) mm.</text>
      <biological_entity id="o25897" name="corolla" name_original="corollas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s17" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25898" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Cypselae (narrowly obconic) 0.5–1.5 mm, sparsely to moderately strigillose;</text>
      <biological_entity id="o25899" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s18" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pappi 2.5–3.5 mm.</text>
      <biological_entity id="o25900" name="pappus" name_original="pappi" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s19" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., Ont., P.E.I., Que., Sask.; Ala., Ariz., Ark., Calif., Colo., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., Mont., N.Dak., N.H., N.J., N.Mex., N.Y., Nebr., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Va., Vt., W.Va., Wyo.; Mexico; introduced worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>57.</number>
  <other_name type="common_name">Late goldenrod</other_name>
  <other_name type="common_name">verge d’or haute</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres usually 3–4 mm; Arizona, California, and Utah, edge of Great plains eastward through e United States, northward in Canada to Nova Scotia and w to Saskatchewan, n of prairies</description>
      <determination>57a Solidago altissima subsp. altissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres usually 2–3 mm; Great plains e toIllinois</description>
      <determination>57b Solidago altissima subsp. gilvocanescens</determination>
    </key_statement>
  </key>
</bio:treatment>