<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">261</other_info_on_meta>
    <other_info_on_meta type="illustration_page">262</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">lagophylla</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">ramosissima</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 391. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus lagophylla;species ramosissima</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220007251</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lagophylla</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">congesta</taxon_name>
    <taxon_hierarchy>genus Lagophylla;species congesta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lagophylla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ramosissima</taxon_name>
    <taxon_name authority="(Greene) D. D. Keck" date="unknown" rank="subspecies">congesta</taxon_name>
    <taxon_hierarchy>genus Lagophylla;species ramosissima;subspecies congesta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–100 (–150) cm (plants self-compatible);</text>
    </statement>
    <statement id="d0_s1">
      <text>branching excurrent or ± pseudo-dichotomous, distal stems eglandular.</text>
      <biological_entity id="o3099" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o3100" name="whole-organism" name_original="" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="pseudo-dichotomous" value_original="pseudo-dichotomous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3101" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades grayish, mostly eglandular (distal stipitate-glandular abaxially, glands whitish or yellowish; plants otherwise eglandular).</text>
      <biological_entity id="o3102" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3103" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in paniculiform arrays or in glomerules.</text>
      <biological_entity id="o3104" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o3105" name="glomerule" name_original="glomerules" src="d0_s3" type="structure" />
      <relation from="o3104" id="r237" name="in paniculiform arrays or in" negation="false" src="d0_s3" to="o3105" />
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 2–5 bractlets.</text>
      <biological_entity id="o3106" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o3107" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <relation from="o3106" id="r238" name="consist_of" negation="false" src="d0_s4" to="o3107" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres obconic to obovoid.</text>
      <biological_entity id="o3108" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s5" to="obovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 4–7 mm, piloso-hirsute on angles, hairs ± patent to antrorsely curved, 0.5–1+ mm.</text>
      <biological_entity id="o3109" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character constraint="on angles" constraintid="o3110" is_modifier="false" name="pubescence" src="d0_s6" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
      <biological_entity id="o3110" name="angle" name_original="angles" src="d0_s6" type="structure" />
      <biological_entity id="o3111" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s6" value="patent" value_original="patent" />
        <character is_modifier="false" modifier="antrorsely" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray laminae 3–6 mm.</text>
      <biological_entity constraint="ray" id="o3112" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae dull to ± glossy (weakly striate).</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 14.</text>
      <biological_entity id="o3113" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="dull" name="reflectance" src="d0_s8" to="more or less glossy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3114" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, openings in chaparral, scrub, woodlands, and forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="openings" constraint="in chaparral , scrub , woodlands , and forests" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Lagophylla ramosissima occurs widely in dry, often disturbed or poor soils of the California Floristic Province, Great Basin, and Pacific Northwest. Plants with heads in glomerate arrays have been treated as L. congesta or L. ramosissima subsp. congesta; W. C. Thompson (1983, p. 21) concluded that L. congesta represents an “extreme morphological variant of L. ramosissima” unworthy of taxonomic recognition.</discussion>
  
</bio:treatment>