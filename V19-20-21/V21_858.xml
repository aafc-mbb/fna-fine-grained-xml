<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Lasthenia</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section Lasthenia</taxon_hierarchy>
    <other_info_on_name type="fna_id">316952</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o21155" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves entire.</text>
      <biological_entity id="o21156" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres hemispheric to obconic.</text>
      <biological_entity id="o21157" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s2" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries persistent, connate 2/3+ their lengths.</text>
      <biological_entity id="o21158" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character char_type="range_value" from="2/3" name="lengths" src="d0_s3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Receptacles conic, papillate, glabrous.</text>
      <biological_entity id="o21159" name="receptacle" name_original="receptacles" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="conic" value_original="conic" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray corollas yellow, laminae (0–) 0.5–2 mm, floral pigments remaining yellow in dilute aqueous alkali.</text>
      <biological_entity constraint="ray" id="o21160" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21161" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="floral" id="o21162" name="pigment" name_original="pigments" src="d0_s5" type="structure">
        <character constraint="in aqueous" constraintid="o21163" is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21163" name="aqueou" name_original="aqueous" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="dilute" value_original="dilute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc corolla lobes 4 (–5);</text>
      <biological_entity constraint="corolla" id="o21164" name="lobe" name_original="lobes" src="d0_s6" type="structure" constraint_original="disc corolla">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anther appendages oblong or obovate;</text>
      <biological_entity constraint="anther" id="o21165" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style apices ± deltate, glabrous or glabrate.</text>
      <biological_entity constraint="style" id="o21166" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae to 4 mm;</text>
    </statement>
    <statement id="d0_s10">
      <text>pappose.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 10.</text>
      <biological_entity id="o21167" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pappose" value_original="pappose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21168" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, South America (Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>354d.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Section Lasthenia has an amphitropical distribution: L. glaberrima ranges from southwestern Washington to central California; L. kunthii Hooker &amp; Arnott is known only from central Chile (P. H. Raven 1963c). The species are very similar morphologically, and conclusive identification of specimens sometimes is difficult without knowing where the plant was collected. Both L. glaberrima and L. kunthii are self-pollinating and are found in vernal pools or other seasonally wet habitats.</discussion>
  
</bio:treatment>