<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="treatment_page">160</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Mackenzie) G. L. Nesom" date="1993" rank="subsection">nemorales</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">velutina</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 332. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection nemorales;species velutina</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067582</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(de Candolle) Kuntze" date="unknown" rank="species">velutinus</taxon_name>
    <taxon_hierarchy>genus Aster;species velutinus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (forming diffuse clones) 15–80 (–150) cm;</text>
      <biological_entity id="o8090" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes creeping, slender.</text>
      <biological_entity id="o8091" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (at ends of rhizomes), ascending to erect, glabrate proximally to sparsely to densely strigoso-puberulent distally.</text>
      <biological_entity id="o8092" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="glabrate" modifier="distally" name="pubescence" src="d0_s2" to="to proximally sparsely densely strigoso-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline often persisting to flowering, gradually tapering to winged petioles, blades linear-oblanceolate to oblanceolate, rarely spatulate, 50–120 × 8–30 mm, proximalmost much smaller, margins entire to sharply serrate, faces glabrate to moderately scabroso-strigose;</text>
      <biological_entity id="o8093" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="often" name="duration" src="d0_s3" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o8094" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o8095" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s3" to="oblanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o8096" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s3" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o8097" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s3" to="sharply serrate" />
      </biological_entity>
      <biological_entity id="o8098" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s3" to="moderately scabroso-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid and distal cauline sessile or subsessille, blades elliptic to oblanceolate or obovate, 10–50 × 3–12 mm, mid tapering to bases, somewhat to strongly 3-nerved, largest, usually much reduced distally, margins entire or sometimes distally serrate, apices acute, faces sparsely to densely strigoso-puberulent, sometimes softly so.</text>
      <biological_entity id="o8099" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s4" value="subsessille" value_original="subsessille" />
      </biological_entity>
      <biological_entity id="o8100" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="oblanceolate or obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character constraint="to bases" constraintid="o8101" is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="sometimes distally" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o8101" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o8102" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="strongly" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="true" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character is_modifier="true" modifier="usually much" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8103" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8104" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s4" value="strigoso-puberulent" value_original="strigoso-puberulent" />
      </biological_entity>
      <relation from="o8100" id="r734" modifier="somewhat" name="to" negation="false" src="d0_s4" to="o8102" />
    </statement>
    <statement id="d0_s5">
      <text>Heads (2–) 30–500, in narrow to broad, thyrsiform to secund-pyramidal paniculiform arrays, branches recurved, secund, congested to lax.</text>
      <biological_entity id="o8105" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="30" to_inclusive="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s5" to="500" />
      </biological_entity>
      <biological_entity id="o8106" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrow" is_modifier="true" name="width" src="d0_s5" to="broad" />
        <character char_type="range_value" from="thyrsiform" is_modifier="true" name="architecture" src="d0_s5" to="secund-pyramidal paniculiform" />
      </biological_entity>
      <biological_entity id="o8107" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="congested" name="architecture_or_arrangement" src="d0_s5" to="lax" />
      </biological_entity>
      <relation from="o8105" id="r735" name="in" negation="false" src="d0_s5" to="o8106" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–6 mm, sparsely to densely strigillose;</text>
      <biological_entity id="o8108" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s6" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles 0–5, sometimes clustered near to and grading into phyllaries, linear-lanceolate.</text>
      <biological_entity id="o8109" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="5" />
        <character constraint="into phyllaries" constraintid="o8110" is_modifier="false" modifier="sometimes" name="arrangement_or_growth_form" src="d0_s7" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o8110" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres campanulate, 3.5–6 mm.</text>
      <biological_entity id="o8111" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–5 series, lanceolate to oblong, strongly unequal, acute or sometimes obtuse, glabrous or sparsely strigillose.</text>
      <biological_entity id="o8112" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s9" to="oblong" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o8113" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o8112" id="r736" name="in" negation="false" src="d0_s9" to="o8113" />
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 6–12;</text>
      <biological_entity id="o8114" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>laminae 2.9–6.3 × 0.3–0.7 (–1) mm.</text>
      <biological_entity id="o8115" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="length" src="d0_s11" to="6.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 5–17;</text>
      <biological_entity id="o8116" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 3.5–6 mm, lobes 0.8–1.7 mm.</text>
      <biological_entity id="o8117" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8118" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 0.7–2.7 mm, sparsely to densely strigillose;</text>
      <biological_entity id="o8119" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="2.7" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s14" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 2.5–4.7 mm.</text>
      <biological_entity id="o8120" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, N.Mex., Nev., Oreg., Tex., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>68.</number>
  <other_name type="common_name">Velvety or three-nerved goldenrod</other_name>
  <discussion>Subspecies 3 (2 in the flora).</discussion>
  <discussion>G. L. Nesom (1993b) merged Solidago californica, S. sparsiflora, and S. velutina without recognizing any infraspecific taxa, as did A. Cronquist (1994). J. C. Semple et al. (1990) compared S. californica and S. sparsiflora to S. nemoralis and found that all three are significantly different in a multivariate analysis. Evidence for separating the two subspecies of S. nemoralis was greater than the support for separating S. californica and S. sparsiflora.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and leaves sparsely strigillose; outer phyllaries glabrate; Wyoming to e California, s to Arizona and New Mexico, Mexico</description>
      <determination>68a Solidago velutina subsp. sparsiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and leaves moderately to densely strigillose; outer phyllaries sparsely strigillose; California and s Oregon</description>
      <determination>68b Solidago velutina subsp. californica</determination>
    </key_statement>
  </key>
</bio:treatment>