<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">55</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall" date="1907" rank="species">brachylepis</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>3: 56. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species brachylepis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066511</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">brachylepis</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>1: 614. 1876 (as Bigelovia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bigelowia;species brachylepis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="species">propinquus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species propinquus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–200 cm.</text>
      <biological_entity id="o3366" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, green when young, fastigiately branched, glabrous, gland-dotted (in pits), resinous.</text>
      <biological_entity id="o3367" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="fastigiately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending to spreading;</text>
      <biological_entity id="o3368" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear to narrowly elliptic (usually terete to sulcate adaxially), 10–25 × 0.8–1.5 mm, midnerves obscure to evident, apices usually acute, rarely rounded, sometimes mucronate, faces glabrous or sparsely hairy, gland-dotted (in circular, deep pits), resinous;</text>
      <biological_entity id="o3369" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s3" to="narrowly elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3370" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character char_type="range_value" from="obscure" name="prominence" src="d0_s3" to="evident" />
      </biological_entity>
      <biological_entity id="o3371" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles of 2–10 leaves, shorter than subtending leaves.</text>
      <biological_entity id="o3372" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o3373" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="10" />
        <character constraint="than subtending leaves" constraintid="o3374" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o3374" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Heads in racemiform to paniculiform arrays (1–15 × ca. 8 cm).</text>
      <biological_entity id="o3375" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o3376" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s5" to="paniculiform" />
      </biological_entity>
      <relation from="o3375" id="r306" name="in" negation="false" src="d0_s5" to="o3376" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 3–20 mm (leafy or bracteate).</text>
      <biological_entity id="o3377" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres turbinate, 4–6 × 2–5 mm.</text>
      <biological_entity id="o3378" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 15–22 in 3–4 series, tan, ovate to elliptic, 2.5–7 × 0.6–1.5 mm, unequal, outer ± herbaceous, mid and inner mostly chartaceous, midnerves darker, strongly raised, uniform in widths, resinous, (margins membranous, ciliate especially distally) apices erect, acute, abaxial faces glabrous.</text>
      <biological_entity id="o3379" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3380" from="15" name="quantity" src="d0_s8" to="22" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o3380" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3381" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="mid and inner" id="o3382" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o3383" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="darker" value_original="darker" />
        <character is_modifier="false" modifier="strongly" name="prominence" src="d0_s8" value="raised" value_original="raised" />
        <character constraint="in apices" constraintid="o3384" is_modifier="false" name="variability" src="d0_s8" value="uniform" value_original="uniform" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o3384" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="true" name="character" src="d0_s8" value="width" value_original="width" />
        <character is_modifier="true" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3385" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets usually 0, rarely 1–2;</text>
      <biological_entity id="o3386" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" modifier="rarely" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae elliptic, 4–6 × 1–1.5 mm.</text>
      <biological_entity id="o3387" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 6–16 (–22);</text>
      <biological_entity id="o3388" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="22" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 5–6.5 mm.</text>
      <biological_entity id="o3389" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae tan to purple, 3.5–4 mm (7–8 ribbed), densely hairy;</text>
      <biological_entity id="o3390" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s13" to="purple" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi off-white to brown, sometimes reddish, 5–6.5 mm. 2n = 36.</text>
      <biological_entity id="o3391" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s14" to="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3392" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open rocky slopes, chaparral and desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="open" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Boundary goldenbush</other_name>
  <other_name type="common_name">chaparral goldenweed</other_name>
  
</bio:treatment>