<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">616</other_info_on_meta>
    <other_info_on_meta type="treatment_page">617</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="(Reichenbach) Reichenbach" date="1841" rank="genus">tephroseris</taxon_name>
    <taxon_name authority="(A. E. Porsild) Holub" date="1973" rank="species">yukonensis</taxon_name>
    <place_of_publication>
      <publication_title>Folia Geobot. Phytotax.</publication_title>
      <place_in_publication>8: 174. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus tephroseris;species yukonensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067716</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="A. E. Porsild" date="unknown" rank="species">yukonensis</taxon_name>
    <place_of_publication>
      <publication_title>Canad. Field-Naturalist</publication_title>
      <place_in_publication>64: 44. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species yukonensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="species">alaskanus</taxon_name>
    <taxon_hierarchy>genus Senecio;species alaskanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–30 (–40) cm (proximal stems and leaves floccose-tomentose, becoming glabrate, especially on the adaxial leaf faces; distal stems and phyllaries floccose-tomentose, hairs yellow; rhizomes creeping).</text>
      <biological_entity id="o12429" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems single or loosely clustered.</text>
      <biological_entity id="o12430" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline (basal often withering before flowering);</text>
      <biological_entity id="o12431" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles unwinged or weakly so, 2–5 cm;</text>
      <biological_entity id="o12432" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unwinged" value_original="unwinged" />
        <character name="architecture" src="d0_s3" value="weakly" value_original="weakly" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades ovate to broadly lanceolate, 2–5 × 1–3 cm, margins subentire to dentate or wavy (proximal cauline petioles usually winged, 0.5–1.5 cm, blades narrowly ovate to lanceolate, 2–4+ × 0.5–2 cm, margins subentire to wavy; mid and distal cauline smaller, clasping, bractlike).</text>
      <biological_entity id="o12433" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="broadly lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12434" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="dentate or wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–4 (–6+).</text>
      <biological_entity id="o12435" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" upper_restricted="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± abruptly contracted to peduncles.</text>
      <biological_entity id="o12436" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character constraint="to peduncles" constraintid="o12437" is_modifier="false" modifier="more or less abruptly" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o12437" name="peduncle" name_original="peduncles" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries mostly (13–) 21, purplish (at least distally), 8–10 (–12) mm.</text>
      <biological_entity id="o12438" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="atypical_quantity" src="d0_s7" to="21" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="21" value_original="21" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets (0) or 13–21;</text>
      <biological_entity id="o12439" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="atypical_quantity" src="d0_s8" value="0" value_original="0" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s8" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla laminae to 10 mm.</text>
      <biological_entity constraint="corolla" id="o12440" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 40–60;</text>
      <biological_entity id="o12441" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s10" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas ochroleucous or white.</text>
      <biological_entity id="o12442" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae glabrous;</text>
      <biological_entity id="o12443" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi white.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = ca. 46 (48?).</text>
      <biological_entity id="o12444" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12445" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="46" value_original="46" />
        <character name="quantity" src="d0_s14" value="[48" value_original="[48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Localized in moist tundra meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist tundra meadows" modifier="localized in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>