<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="treatment_page">447</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Cassini" date="1822" rank="genus">logfia</taxon_name>
    <taxon_name authority="(Linnaeus) Cosson &amp; Germain" date="1843" rank="species">gallica</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>2, 20: 291. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus logfia;species gallica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220007752</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Filago</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">gallica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: add. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Filago;species gallica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oglifa</taxon_name>
    <taxon_name authority="(Linnaeus) Chrtek &amp; Holub" date="unknown" rank="species">gallica</taxon_name>
    <taxon_hierarchy>genus Oglifa;species gallica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–50 [–30] cm.</text>
      <biological_entity id="o11622" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="average_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5, ± erect;</text>
      <biological_entity id="o11623" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches ± leafy between proximal forks, remaining grayish to greenish, arachnoid-sericeous.</text>
      <biological_entity id="o11624" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character constraint="between proximal forks" constraintid="o11625" is_modifier="false" modifier="more or less" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="grayish" name="coloration" notes="" src="d0_s2" to="greenish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="arachnoid-sericeous" value_original="arachnoid-sericeous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11625" name="fork" name_original="forks" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly subulate, largest 20–30 (–40) × 1–1.5 (–2) mm, ± stiff;</text>
      <biological_entity id="o11626" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>longest capitular leaves 2–5 times head heights, acute or subspinose.</text>
      <biological_entity constraint="longest capitular" id="o11627" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="head" constraintid="o11628" is_modifier="false" name="height" src="d0_s4" value="2-5 times head heights" value_original="2-5 times head heights" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subspinose" value_original="subspinose" />
      </biological_entity>
      <biological_entity id="o11628" name="head" name_original="head" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Heads in glomerules of (2–) 3–10 (–14) in strictly dichasiiform arrays, narrowly ampulliform, largest (3–) 3.5–4.5 × 2–3 mm.</text>
      <biological_entity id="o11629" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s5" value="ampulliform" value_original="ampulliform" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s5" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s5" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11630" name="glomerule" name_original="glomerules" src="d0_s5" type="structure" />
      <biological_entity id="o11631" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="14" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="10" />
        <character is_modifier="true" modifier="strictly" name="architecture" src="d0_s5" value="dichasiiform" value_original="dichasiiform" />
      </biological_entity>
      <relation from="o11629" id="r1076" name="in" negation="false" src="d0_s5" to="o11630" />
      <relation from="o11630" id="r1077" name="part_of" negation="false" src="d0_s5" to="o11631" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries usually 5, equal, unlike paleae (hyaline, obovate).</text>
      <biological_entity id="o11632" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o11633" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="unlike" value_original="unlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles fungiform to obovoid, 0.7–0.9 mm, heights 0.8–1.1 times diams.</text>
      <biological_entity id="o11634" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character char_type="range_value" from="fungiform" name="shape" src="d0_s7" to="obovoid" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s7" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="height" src="d0_s7" value="0.8-1.1 times diams" value_original="0.8-1.1 times diams" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pistillate paleae (except innermost) 9–12 in 2 series, ± vertically ranked, tightly saccate, inflexed 70–90° proximally, gibbous, ± galeate, longest 3.3–4.1 mm, distal 15–30% of lengths glabrous abaxially;</text>
      <biological_entity id="o11635" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="in series" constraintid="o11636" from="9" name="quantity" src="d0_s8" to="12" />
        <character is_modifier="false" modifier="more or less vertically" name="arrangement" notes="" src="d0_s8" value="ranked" value_original="ranked" />
        <character is_modifier="false" modifier="tightly" name="architecture_or_shape" src="d0_s8" value="saccate" value_original="saccate" />
        <character is_modifier="false" modifier="70-90°; proximally" name="orientation" src="d0_s8" value="inflexed" value_original="inflexed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="galeate" value_original="galeate" />
        <character is_modifier="false" name="length" src="d0_s8" value="longest" value_original="longest" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.1" to_unit="mm" />
        <character is_modifier="false" modifier="15-30%" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character constraint="of lengths" is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11636" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bodies ± bony, ± terete;</text>
      <biological_entity id="o11637" name="body" name_original="bodies" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s9" value="osseous" value_original="bony" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>wings prominent.</text>
      <biological_entity id="o11638" name="wing" name_original="wings" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Innermost paleae ± 5, spreading in 1 series, pistillate.</text>
      <biological_entity constraint="innermost" id="o11639" name="palea" name_original="paleae" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character constraint="in series" constraintid="o11640" is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11640" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate florets: outer 9–12 epappose, inner 8–14 (–30) pappose.</text>
      <biological_entity id="o11641" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o11642" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s12" to="12" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epappose" value_original="epappose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11643" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="30" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="14" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pappose" value_original="pappose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Bisexual florets [2–] 3–5;</text>
      <biological_entity id="o11644" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s13" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2.2–3 mm, lobes mostly 4, brownish to yellowish.</text>
      <biological_entity id="o11645" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11646" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character char_type="range_value" from="brownish" name="coloration" src="d0_s14" to="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae: outer incurved, proximally ± horizontal, distally erect, compressed, [0.8–] 0.9–1 mm;</text>
      <biological_entity id="o11647" name="cypsela" name_original="cypselae" src="d0_s15" type="structure" />
      <biological_entity constraint="outer" id="o11648" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="proximally more or less" name="orientation" src="d0_s15" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>inner ± sparsely papillate;</text>
      <biological_entity id="o11649" name="cypsela" name_original="cypselae" src="d0_s16" type="structure" />
      <biological_entity constraint="inner" id="o11650" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less sparsely" name="relief" src="d0_s16" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 18–28+ bristles falling in complete or partial rings, 2.2–3 mm. 2n = 28 (former USSR, Portugal).</text>
      <biological_entity id="o11651" name="cypsela" name_original="cypselae" src="d0_s17" type="structure" />
      <biological_entity id="o11652" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11653" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s17" to="28" upper_restricted="false" />
        <character constraint="in rings" constraintid="o11654" is_modifier="false" name="life_cycle" src="d0_s17" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o11654" name="ring" name_original="rings" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="complete" value_original="complete" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="partial" value_original="partial" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11655" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
      </biological_entity>
      <relation from="o11652" id="r1078" name="consist_of" negation="false" src="d0_s17" to="o11653" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting mid Mar–early Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="mid Mar" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="mid Mar" />
        <character name="fruiting time" char_type="range_value" to="early Jul" from="mid Mar" />
        <character name="fruiting time" char_type="atypical_range" to="Aug" from="mid Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mediterranean climates, open slopes, flats, diverse substrates (including serpentine), often ruderal or disturbed sites (especially chaparral burns)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mediterranean climates" />
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="diverse substrates" />
        <character name="habitat" value="disturbed sites" modifier="ruderal" />
        <character name="habitat" value="chaparral burns" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1100(–1400) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg.; Eurasia; n Africa; also introduced in Mexico (Baja California); South America; Atlantic Islands; Pacific Islands; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also  in Mexico (Baja California)" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Daggerleaf or narrowleaf cottonrose</other_name>
  <other_name type="common_name">cotonnière de France</other_name>
  <discussion>Logfia gallica is introduced in South America, Atlantic Islands, Pacific Islands (Hawaii), Australia, and probably elsewhere.</discussion>
  <discussion>Logfia gallica is readily recognized by its relatively long and stiff awl-shaped leaves. In the flora, L. gallica is relatively common in the Californian Floristic Province from southwestern Oregon to northwestern Baja California (including the Channel Islands). It is often so well integrated with indigenous vegetation as to appear native. The first known collection in the flora area was from Newcastle, California, around 1883. It had been collected throughout central California by 1935 and had occupied most of its present North American range by 1970.</discussion>
  <discussion>In the flora area, Logfia gallica tends to grow larger than in its native range.</discussion>
  
</bio:treatment>