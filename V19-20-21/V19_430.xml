<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="treatment_page">296</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Scopoli" date="1777" rank="genus">UROSPERMUM</taxon_name>
    <place_of_publication>
      <publication_title>Intr. Hist. Nat.,</publication_title>
      <place_in_publication>122. 1777</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus UROSPERMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek uro, tail, and sperma, seed, alluding to beaks of cypselae</other_info_on_name>
    <other_info_on_name type="fna_id">134238</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals [perennials], 10–40 (–60+) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o18254" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, erect, branched distally, setose to hispid or glabrous [pilosulous].</text>
      <biological_entity id="o18255" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="setose" name="pubescence" src="d0_s2" to="hispid or glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline (at flowering) [mostly basal];</text>
      <biological_entity id="o18256" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18257" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal ± petiolate, distal sessile;</text>
      <biological_entity constraint="proximal" id="o18258" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18259" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades of the proximal mostly obovate to oblong-obovate, usually pinnately lobed or dentate, distal ovate to linear (bases often clasping), ultimate margins dentate or entire (veins often setose on abaxial faces).</text>
      <biological_entity id="o18260" name="blade" name_original="blades" src="d0_s5" type="structure" constraint="blade" constraint_original="blade; blade">
        <character char_type="range_value" from="mostly obovate" name="shape" src="d0_s5" to="oblong-obovate usually pinnately lobed or dentate" />
        <character char_type="range_value" from="mostly obovate" name="shape" src="d0_s5" to="oblong-obovate usually pinnately lobed or dentate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18261" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o18262" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o18263" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o18260" id="r1644" name="part_of" negation="false" src="d0_s5" to="o18261" />
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly or in loose, corymbiform arrays.</text>
      <biological_entity id="o18264" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o18265" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o18264" id="r1645" name="in" negation="false" src="d0_s6" to="o18265" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles little, if at all, inflated distally, rarely bracteate.</text>
      <biological_entity id="o18266" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="at all; distally" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0.</text>
      <biological_entity id="o18267" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres ± urceolate, 10–20+ mm diam.</text>
      <biological_entity id="o18268" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s9" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 7–8 (–12+) in 1 (–2) series, ovatelanceolate to lance-linear (basally connate), subequal, margins scarious, apices acuminate.</text>
      <biological_entity id="o18269" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="12" upper_restricted="false" />
        <character char_type="range_value" constraint="in series" constraintid="o18270" from="7" name="quantity" src="d0_s10" to="8" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" notes="" src="d0_s10" to="lance-linear" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o18270" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o18271" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o18272" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat to convex, pitted, hispid, epaleate.</text>
      <biological_entity id="o18273" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s11" to="convex" />
        <character is_modifier="false" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Florets 20–50+;</text>
      <biological_entity id="o18274" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, sometimes striped abaxially with red.</text>
      <biological_entity id="o18275" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes; abaxially" name="coloration" src="d0_s13" value="striped abaxially with red" value_original="striped abaxially with red" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae brown, bodies flattened-oblong, ± tuberculate, faces glabrous or scabrellous, beaks proximally dilated and tuberculate, distally acuminate and scabrous to scabrellous;</text>
      <biological_entity id="o18276" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o18277" name="body" name_original="bodies" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened-oblong" value_original="flattened-oblong" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s14" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o18278" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s14" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
      <biological_entity id="o18279" name="beak" name_original="beaks" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="relief" src="d0_s14" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="scabrous" name="relief" src="d0_s14" to="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi readily falling, of 18–22+ white [buff to rufous], subequal, plumose bristles in 1 (–2) series (basally connate, falling together).</text>
      <biological_entity id="o18281" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s15" to="22" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="true" name="size" src="d0_s15" value="subequal" value_original="subequal" />
        <character is_modifier="true" name="shape" src="d0_s15" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity id="o18282" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s15" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <relation from="o18280" id="r1646" name="consist_of" negation="false" src="d0_s15" to="o18281" />
      <relation from="o18281" id="r1647" name="in" negation="false" src="d0_s15" to="o18282" />
    </statement>
    <statement id="d0_s16">
      <text>x = 5, 7?.</text>
      <biological_entity id="o18280" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o18283" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character name="quantity" src="d0_s16" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, adjacent Africa and Asia; adventive in South America, s Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="adjacent Africa and Asia" establishment_means="introduced" />
        <character name="distribution" value="adventive in South America" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <references>
    <reference>Lack, H. W. and B. E. Leuenberger. 1979. Pollen and taxonomy of Urospermum (Asteraceae, Lactuceae). Pollen &amp; Spores 21: 415–425.</reference>
  </references>
  
</bio:treatment>