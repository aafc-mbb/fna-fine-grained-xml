<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="treatment_page">429</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Willdenow" date="1807" rank="genus">grindelia</taxon_name>
    <taxon_name authority="Steyermark" date="1934" rank="species">havardii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 474, fig. 11. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus grindelia;species havardii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066811</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (perhaps flowering first or second-year), 30–50 (–150) cm.</text>
      <biological_entity id="o4616" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, stramineous to pinkish, usually villosulous to hirtellous and/or stipitate-glandular, sometimes glabrate.</text>
      <biological_entity id="o4617" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s1" to="pinkish" />
        <character char_type="range_value" from="usually villosulous" name="pubescence" src="d0_s1" to="hirtellous and/or stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaf-blades mostly ovate to oblong, (7–) 15–30 (–55) mm, lengths 2–4 times widths, bases ± clasping, margins crenate (teeth mostly 3–5 per cm, blunt, resin-tipped), apices obtuse to acute, faces usually hirtellous and little, if at all, gland-dotted, sometimes glabrate (then gland-dotted).</text>
      <biological_entity constraint="cauline" id="o4618" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly ovate" name="shape" src="d0_s2" to="oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_distance" src="d0_s2" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="distance" src="d0_s2" to="30" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="2-4" value_original="2-4" />
      </biological_entity>
      <biological_entity id="o4619" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o4620" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o4621" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o4622" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="at all" name="coloration" src="d0_s2" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly or in open to crowded, corymbiform arrays.</text>
      <biological_entity id="o4623" name="head" name_original="heads" src="d0_s3" type="structure">
        <character constraint="in open to crowded , corymbiform arrays" is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="in open to crowded , corymbiform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres broadly urceolate to globose, 8–13 × 12–22 mm (often subtended by leaflike bracts).</text>
      <biological_entity id="o4624" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly urceolate" name="shape" src="d0_s4" to="globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries in 5–6 series, reflexed to spreading, linear to lanceolate, apices subulate to terete, ± recurved to straight (terminal setae incurved), moderately to strongly resinous.</text>
      <biological_entity id="o4625" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="reflexed" name="orientation" notes="" src="d0_s5" to="spreading" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o4626" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o4627" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s5" to="terete" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="moderately to strongly" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o4625" id="r418" name="in" negation="false" src="d0_s5" to="o4626" />
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 18–25;</text>
      <biological_entity id="o4628" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s6" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 8–11 mm.</text>
      <biological_entity id="o4629" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae stramineous to light-brown, (2–) 3–3.5 mm, apices smooth to minutely coronate, faces smooth or striate (angles ± ribbed);</text>
      <biological_entity id="o4630" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s8" to="light-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4631" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s8" value="coronate" value_original="coronate" />
      </biological_entity>
      <biological_entity id="o4632" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of 2 ± straight, usually smooth (apices usually dilated), setiform awns 4–7 mm, ± equaling disc corollas.</text>
      <biological_entity id="o4633" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
      <biological_entity id="o4634" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" modifier="more or less" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="true" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="true" name="shape" src="d0_s9" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <relation from="o4633" id="r419" name="consist_of" negation="false" src="d0_s9" to="o4634" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 12.</text>
      <biological_entity constraint="disc" id="o4635" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4636" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites, rocky slopes of limestone, dry gravelly washes, alluvium</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" />
        <character name="habitat" value="rocky slopes" constraint="of limestone , dry gravelly washes" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="dry gravelly washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  
</bio:treatment>