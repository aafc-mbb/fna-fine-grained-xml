<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">10</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">AMBROSIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 987. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 425. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus AMBROSIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ambrosia, “food of the gods,” allusion unclear</other_info_on_name>
    <other_info_on_name type="fna_id">101325</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Cavanilles" date="unknown" rank="genus">Franseria</taxon_name>
    <taxon_hierarchy>genus Franseria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="genus">Hymenoclea</taxon_name>
    <taxon_hierarchy>genus Hymenoclea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, or shrubs, 10–400+ cm (usually rhizomatous).</text>
      <biological_entity id="o24983" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="400" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o24985" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, decumbent, or prostrate, branched.</text>
      <biological_entity id="o24986" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually cauline;</text>
      <biological_entity id="o24988" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite ± throughout or opposite (proximal) and alternate or mostly alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or petiolate;</text>
      <biological_entity id="o24987" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less throughout; throughout" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (or lobes) deltate, elliptic, filiform, lanceolate, linear, obovate, ovate, or rhombic (and most intermediate shapes), usually pinnately, sometimes palmately lobed, ultimate margins entire or toothed, faces hairy or glabrate, usually glanddotted or stipitate-glandular.</text>
      <biological_entity id="o24989" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" modifier="usually pinnately; pinnately; sometimes palmately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o24990" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o24991" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid (unisexual, pistillate proximal to or intermixed with staminates, staminates usually in racemiform to spiciform arrays; rarely, single plants all or mostly staminate or pistillate).</text>
      <biological_entity id="o24992" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate heads: phyllaries 12–30 (–80+) in 1–8+ series, outer (1–) 5–8 distinct or ± connate, herbaceous, the rest (sometimes interpreted as paleae) ± connate, usually with free tips forming tubercles, spines, or wings (the whole becoming a hard perigynium or “bur”);</text>
      <biological_entity id="o24993" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24994" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="80" upper_restricted="false" />
        <character char_type="range_value" constraint="in series" constraintid="o24995" from="12" name="quantity" src="d0_s7" to="30" />
      </biological_entity>
      <biological_entity id="o24995" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="outer" id="o24996" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="8" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o24997" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o24998" name="tubercle" name_original="tubercles" src="d0_s7" type="structure" />
      <biological_entity id="o24999" name="spine" name_original="spines" src="d0_s7" type="structure" />
      <biological_entity id="o25000" name="wing" name_original="wings" src="d0_s7" type="structure" />
      <relation from="o24996" id="r1701" modifier="usually" name="with" negation="false" src="d0_s7" to="o24997" />
      <relation from="o24997" id="r1702" name="forming" negation="false" src="d0_s7" to="o24998" />
      <relation from="o24997" id="r1703" name="forming" negation="false" src="d0_s7" to="o24999" />
      <relation from="o24997" id="r1704" name="forming" negation="false" src="d0_s7" to="o25000" />
    </statement>
    <statement id="d0_s8">
      <text>florets 1 (–5+), corollas 0.</text>
      <biological_entity id="o25001" name="head" name_original="heads" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o25002" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" upper_restricted="false" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25003" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate heads: involucres cupshaped to saucer-shaped, 1.5–6+ mm diam.;</text>
      <biological_entity id="o25004" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o25005" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="cupshaped" name="shape" src="d0_s9" to="saucer-shaped" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s9" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>phyllaries 5–16+ in ± 1 series, ± connate;</text>
      <biological_entity id="o25006" name="head" name_original="heads" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o25007" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o25008" from="5" name="quantity" src="d0_s10" to="16" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="fusion" notes="" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o25008" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>receptacles ± flat or convex;</text>
      <biological_entity id="o25009" name="head" name_original="heads" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o25010" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s11" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleae spatulate to linear, membranous, sometimes villous, hirtellous, and/or glanddotted or stipitate-glandular, sometimes none;</text>
      <biological_entity id="o25011" name="head" name_original="heads" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o25012" name="palea" name_original="paleae" src="d0_s12" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s12" to="linear" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s12" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s12" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>florets 5–60+;</text>
      <biological_entity id="o25013" name="head" name_original="heads" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o25014" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s13" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas whitish or purplish, ± funnelform, lobes 5, erect or incurved;</text>
      <biological_entity id="o25015" name="head" name_original="heads" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o25016" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o25017" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="incurved" value_original="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>staminal filaments connate, anthers distinct or weakly coherent.</text>
      <biological_entity id="o25018" name="head" name_original="heads" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="staminal" id="o25019" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o25020" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="weakly" name="fusion" src="d0_s15" value="coherent" value_original="coherent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae (black) ± ovoid or fusiform, enclosed within globose to obovoid, pyramidal, pyriform, obconic, or fusiform, hard, smooth, tuberculate, spiny, or winged “burs”;</text>
      <biological_entity id="o25021" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="position" src="d0_s16" value="enclosed" value_original="enclosed" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="obovoid pyramidal pyriform obconic or fusiform" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="obovoid pyramidal pyriform obconic or fusiform" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="obovoid pyramidal pyriform obconic or fusiform" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="obovoid pyramidal pyriform obconic or fusiform" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="obovoid pyramidal pyriform obconic or fusiform" />
        <character is_modifier="false" name="texture" src="d0_s16" value="hard" value_original="hard" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s16" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 18.</text>
      <biological_entity id="o25022" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o25023" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical to subtropical and temperate New World, mostly North America, some established in Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical to subtropical and temperate New World" establishment_means="native" />
        <character name="distribution" value="mostly North America" establishment_means="native" />
        <character name="distribution" value="some established in Old World" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>244.</number>
  <other_name type="common_name">Ragweed</other_name>
  <discussion>Species 40+ (22 in the flora).</discussion>
  <references>
    <reference>Payne, W. W. 1964. A re-evaluation of the genus Ambrosia (Compositae). J. Arnold Arbor. 45: 401–430.</reference>
    <reference>Peterson, K. M. and W. W. Payne. 1973. The genus Hymenoclea (Compositae: Ambrosieae). Brittonia 25: 243–256.</reference>
    <reference>Strother, J. L. and B. G. Baldwin. 2002. Hymenocleas are ambrosias (Compositae). Madroño 49: 143–144.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals, perennials, or subshrubs (usually rhizomatous)</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades and/or lobes filiform (0.5–1.5 mm wide); burs winged, wings 5–20+, oblanceolate to cuneiform or flabellate to orbiculate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades and/or lobes mostly deltate, elliptic, lanceolate, ovate, rhombic, or triangular (broader than filiform); burs spiny, spines 8–30(–80+), ± lanceolate (sometimes navicular at bases) to subulate or acerose (tips sometimes uncinate)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Wings on burs mostly around middles, 2–3 × 1–2 mm; flowering (May–)Aug–Nov</description>
      <determination>1 Ambrosia monogyra</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Wings on burs ± scattered, 3–4(–6) × 2–4(–8) mm; flowering Mar–May(–Jun)</description>
      <determination>2 Ambrosia salsola</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades (green) hirsutulous to hirtellous abaxially</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades (often gray, silvery, or white) ± densely pilosulous, puberulent, scabrellous, strigillose, or tomentulose abaxially</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves: petioles 0–2 mm, blades elliptic to ovate, margins spiny-toothed</description>
      <determination>3 Ambrosia ilicifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves: petioles 10–35 mm, blades lanceolate to narrowly triangular, margins coarsely toothed (not spiny)</description>
      <determination>4 Ambrosia ambrosioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades (1–)2–3-pinnately lobed; pistillate heads often intermixed with staminates</description>
      <determination>5 Ambrosia dumosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades not pinnately lobed (irregularly toothed to ± laciniate in A. eriocentra); pistillate heads proximal to staminates</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades elliptic, lance-linear, or rhombic (margins irregularly toothed to ± laciniate); pistillate florets 1; burs densely villous and stipitate-glandular</description>
      <determination>6 Ambrosia eriocentra</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades deltate, lance-deltate, ovate, or rounded-deltate; pistillate florets (1–)2(–3); burs puberulent to tomentulose and/or ± stipitate-glandular</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades: bases cordate to truncate, abaxial faces densely puberulent (including veins)</description>
      <determination>7 Ambrosia cordifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades: bases cuneate to truncate, abaxial faces tomentulose (mostly between veins)</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades ovate to rounded-deltate; burs usually tomentulose (little, if at all, stipitate-glandular)</description>
      <determination>8 Ambrosia chenopodiifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades deltate to lance-deltate; burs usually stipitate-glandular (little, if at all, tomentulose)</description>
      <determination>9 Ambrosia deltoidea</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Annuals</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Perennials or subshrubs</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaves mostly opposite (usually some blades palmately 3–5-lobed)</description>
      <determination>10 Ambrosia trifida</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaves mostly opposite (in A. bidentata, opposite and alternate, or mostly alternate, usually some or all blades 1–2-pinnately lobed, except in A. bidentata with 0–4 basal lobes)</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades with (1–)2(–4) basal lobes or not lobed; peduncles of staminate heads 0–0.5 mm; burs 5–8 mm</description>
      <determination>11 Ambrosia bidentata</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades mostly 1–2-pinnately lobed; peduncles of staminate heads 0.5–2 mm; burs 2–5 mm</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Involucres of staminate heads (2–)3–5(–7) mm diam. (usually each with 1–5+ black nerves); burs ± fusiform to obpyramidal, 3–5 mm, spines 8–18+, 2–4(–5) mm</description>
      <determination>12 Ambrosia acanthicarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Involucres of staminate heads 2–3+ mm diam. (usually without black nerves); burs ± globose to pyriform, 2–3 mm, spines or tubercles 3–5+, 0.1–0.5+ mm</description>
      <determination>13 Ambrosia artemisiifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Stems ± prostrate or decumbent (usually beaches or strand)</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Stems ± erect (rarely beaches or strand)</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Staminate involucres 2–3+ mm diam.; burs ± pyriform, 1–2 mm, spines or tubercles 0–5+, 0.1–0.5+ mm</description>
      <determination>14 Ambrosia hispida</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Staminate involucres 4–6+ mm diam.; burs fusiform to ± pyriform, 4–7+ mm, spines 8–16+, 0.5–1.5+ mm</description>
      <determination>15 Ambrosia chamissonis</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades sometimes 1–2(–3)-pinnately lobed (if lobed, lobes mostly deltate to lanceolate); involucres of staminate heads usually ± saucer-shaped (cup-shaped in A. cheiranthifolia from s Texas)</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades usually 1–4-pinnately lobed (lobes or non-lobed blades mostly lanceolate to linear); involucres of staminate heads cup-shaped</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blades elliptic to lance-elliptic, 50–80(–180) × 12–20(–50+) mm, 1–2(–3)-pinnately lobed (lobes ± deltate), abaxial faces densely finely scabrellous</description>
      <determination>16 Ambrosia tomentosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blades elliptic, lanceolate, oblanceolate, oblong, or ovate, 20–60(–100+) × 8–45(–75+) mm, 1(–2)-pinnately lobed or not lobed, abaxial faces densely sericeous, strigillose, or strigose</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaves: petioles none, blades lanceolate or lance-elliptic to lance-oblong or oblanceolate, 20–50(–70+) mm, rarely ± pinnately lobed (lobes ± deltate)</description>
      <determination>17 Ambrosia cheiranthifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaves: petioles 10–45+ mm, blades elliptic to ovate, 45–60(–100+) mm, usually 1–2-pinnately lobed (lobes ± lanceolate)</description>
      <determination>18 Ambrosia grayi</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Burs: spines (1–)8–13+, subulate, tips usually uncinate</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Burs: spines or tubercles 0 or 1–6, stoutly conic to ± acerose, tips straight</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blades mostly linear (some 1-pinnate, lobes linear); involucres of staminate heads 4–6+ mm diam</description>
      <determination>19 Ambrosia linearis</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blades lanceolate to ovate, laciniately 2–4-pinnately lobed (lobes ± lanceolate); involucres of staminate heads 1.5–3+ mm diam</description>
      <determination>20 Ambrosia confertiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blades deltate to elliptic, 15–35(–75) × 12–25(–45) mm, laciniately (1–)2(–3)-pinnately lobed; burs ± fusiform, 2–2.5 mm, strigillose</description>
      <determination>21 Ambrosia pumila</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blades deltate to lanceolate, 20–60(–140) × 8–35(–50+) mm, pinnately toothed or 1(–2)-pinnately lobed; burs ± obpyramidal to globose, 2–3 mm, hirsutulous</description>
      <determination>22 Ambrosia psilostachya</determination>
    </key_statement>
  </key>
</bio:treatment>