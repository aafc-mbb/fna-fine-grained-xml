<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">467</other_info_on_meta>
    <other_info_on_meta type="mention_page">481</other_info_on_meta>
    <other_info_on_meta type="treatment_page">480</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Nuttall) Semple in J. C. Semple et al." date="2002" rank="subgenus">astropolium</taxon_name>
    <taxon_name authority="(Michaux) G. L. Nesom" date="1995" rank="species">subulatum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 293. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus astropolium;species subulatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250049273</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">subulatus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 111. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species subulatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (10–) 30–150 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o16901" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1, erect (often with purple or purplish brown areas), glabrous or glabrate, sometimes strigillose in leaf-axils.</text>
      <biological_entity id="o16902" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character constraint="in leaf-axils" constraintid="o16903" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o16903" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves thin (green to dark green), margins often strigilloso-ciliolate, faces glabrous;</text>
      <biological_entity id="o16904" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o16905" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="strigilloso-ciliolate" value_original="strigilloso-ciliolate" />
      </biological_entity>
      <biological_entity id="o16906" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering, long-petiolate (petiole bases sheathing), sparsely ciliate, blades ovate to oblanceolate, 10–90 × 6–14 mm, bases attenuate to cuneate, rounded, margins entire or serrulate or crenulate, apices rounded, obtuse, or acute;</text>
      <biological_entity constraint="basal" id="o16907" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by apices" constraintid="o16911" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o16908" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="long-petiolate" value_original="long-petiolate" />
        <character is_modifier="true" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s4" to="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16909" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="long-petiolate" value_original="long-petiolate" />
        <character is_modifier="true" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s4" to="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16910" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="long-petiolate" value_original="long-petiolate" />
        <character is_modifier="true" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s4" to="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16911" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="true" name="shape" src="d0_s4" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline withering by flowering, petiolate, subpetiolate, or sessile, blades narrowly lanceolate or subulate, 20–100 (–200) × 1.5–10 (–20) mm, bases attenuate, margins subentire, entire, or serrulate, apices acute to acuminate;</text>
      <biological_entity constraint="proximal cauline" id="o16912" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by apices" constraintid="o16916" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o16913" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="200" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="100" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16914" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="200" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="100" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16915" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="200" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="100" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16916" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades narrowly lanceolate to subulate, 5–113 × 0.5–5.5 mm, apices acuminate.</text>
      <biological_entity constraint="distal" id="o16917" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o16918" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s6" to="subulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="113" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16919" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (10–) 30–100 (–150), in open, diffuse, paniculiform arrays.</text>
      <biological_entity id="o16920" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s7" to="30" to_inclusive="false" />
        <character char_type="range_value" from="100" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="150" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s7" to="100" />
      </biological_entity>
      <biological_entity id="o16921" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="true" name="density" src="d0_s7" value="diffuse" value_original="diffuse" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o16920" id="r1567" name="in" negation="false" src="d0_s7" to="o16921" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles (0.2–) 0.5–4 cm, bracts 4–8 (–17).</text>
      <biological_entity id="o16922" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16923" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="17" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindric to turbinate, 5–7 (–8.2) mm.</text>
      <biological_entity id="o16924" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s9" to="turbinate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8.2" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–5 series, broadly or narrowly lanceolate to subulate, unequal, bases indurate, margins hyaline, often purple-tinged, entire, green zones lanceolate (usually narrow, sometimes broad and covering most of distal portion), apices acute, faces glabrous.</text>
      <biological_entity id="o16925" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" modifier="broadly; narrowly" name="shape" notes="" src="d0_s10" to="subulate" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o16926" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o16927" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o16928" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16929" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o16930" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o16931" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o16925" id="r1568" name="in" negation="false" src="d0_s10" to="o16926" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 16–30 (–54) in 1–3 series;</text>
      <biological_entity id="o16932" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="54" />
        <character char_type="range_value" constraint="in series" constraintid="o16933" from="16" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
      <biological_entity id="o16933" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas white, pink, or lavender, laminae 1.3–7 × 0.2–1.3 mm.</text>
      <biological_entity id="o16934" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
      </biological_entity>
      <biological_entity id="o16935" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 4–10 (–13);</text>
      <biological_entity id="o16936" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="13" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yello, sometimes tinged with purple, 3.4–5.2 mm, throats narrowly funnelform, lobes ± spreading to erect, narrowly triangular, 0.3–0.7 mm, glabrous.</text>
      <biological_entity id="o16937" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="tinged with purple" value_original="tinged with purple" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s14" to="5.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16938" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o16939" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="less spreading" name="orientation" src="d0_s14" to="erect" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae light-brown to purple, narrowly obovoid to fusiform, sometimes ± compressed, (1.2–) 1.5–2.7 (–3) mm, 5-nerved, faces sparsely strigillose;</text>
      <biological_entity id="o16940" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s15" to="purple" />
        <character char_type="range_value" from="narrowly obovoid" name="shape" src="d0_s15" to="fusiform" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-nerved" value_original="5-nerved" />
      </biological_entity>
      <biological_entity id="o16941" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi white, (3–) 3.5–5.5 mm.</text>
      <biological_entity id="o16942" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont.; Ala., Ariz., Ark., Calif., Conn., Del., Fla., Ga., Ill., Ind., Kans., La., Maine, Mass., Md., Mich., Miss., N.C., N.H., N.J., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Utah, Va.; Mexico, West Indies, Bermuda, Central America, South America; widely introduced worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Bermuda" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="widely  worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Annual saltmarsh or eastern annual saltmarsh aster</other_name>
  <other_name type="common_name">aster subulé</other_name>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>Five varieties of Symphyotrichum subulatum are recognized for North America based on differences in chromosome number, ray lamina color and size, array shapes, number of series of ray florets, number of disc and ray florets, and other, more cryptic characters (S. D. Sundberg 2004). These varieties were treated as species by G. L. Nesom (1994b, 2005d). Variety ligulatum is apparently an obligate outcrosser and is the least variable variety (Sundberg). Other varieties are self-compatible, which could facilitate the fixation of mutations in populations.</discussion>
  <discussion>The five varieties are nearly entirely allopatric, and intermediates between pairs of varieties are not uncommon where they approach one another. Populations that are intermediate in ray lamina size between vars. ligulatum and parviflorum are widespread in southern Texas, New Mexico, Arizona, and northern Mexico. Intermediates between vars. elongatum and parviflorum and between vars. elongatum and subulatum occur in Florida. Despite these observations, hybridization experiments and chromosome number differences suggest that the varieties are mostly reproductively isolated (S. D. Sundberg 1986, 2004).</discussion>
  <discussion>In older floras the name Aster exilis Elliott has been applied to Symphyotrichum subulatum vars. ligulatum and parviflorum. The status of this name is uncertain; the type specimen has been lost and the description of the plant is inadequate for determining the taxon to which the name should be applied (G. L. Nesom 1994b; S. D. Sundberg 2004).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray laminae lavender to blue, (3.5–)4.5–7 × 0.9–1.3 mm, drying in 3–5 coils; ray florets in 1 series; disc florets (20–)33–50 (sc United States)</description>
      <determination>4a Symphyotrichum subulatum var. ligulatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray laminae white to pink or lavender, 1.3–3.5(–4.2) × 0.2–0.6 mm, drying in (0–)1–3(–4) coils; ray florets in 1–3 series; disc florets 3–23</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray laminae pink to lavender, (2–)2.5–3.5(–4.2) × 0.3–0.6 mm, drying in 2–3(–4) coils; disc florets 11–23 (Florida, West Indies)</description>
      <determination>4c Symphyotrichum subulatum var. elongatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray laminae usually white, rarely pink or lavender, 1.3–3 × 0.2–0.5 mm, drying in 1 coil or curling; disc florets (3–)4–15</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray laminae longer than pappi; phyllaries 30–42; pappi 3.5–3.8(–4.2) mm; w United States</description>
      <determination>4b Symphyotrichum subulatum var. parviflorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray laminae shorter to slightly longer than pappi; phyllaries 18–30; pappi 3.5–5.5 mm; e and se United States</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Green zones of phyllaries narrowly to broadly lanceolate, extending phyllary length; ray laminae 0.2–0.5 mm wide; disc florets 3.8–4.6(–4.9) mm; salt marshes, e North America and Gulf Coast</description>
      <determination>4d Symphyotrichum subulatum var. subulatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Green zones of phyllaries broadly lanceolate, nearly absent from proximal portion; ray laminae 0.2–0.3 mm wide; disc florets 3.2–4.1 mm; adventive, s United States</description>
      <determination>4e Symphyotrichum subulatum var. squamatum</determination>
    </key_statement>
  </key>
</bio:treatment>