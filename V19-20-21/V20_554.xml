<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="treatment_page">254</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Semple) Semple" date="1996" rank="species">shevockii</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Heterotheca Phyllotheca,</publication_title>
      <place_in_publication>148. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species shevockii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066929</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterotheca</taxon_name>
    <taxon_name authority="(Pursh) Shinners" date="unknown" rank="species">villosa</taxon_name>
    <taxon_name authority="Semple" date="unknown" rank="variety">shevockii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>73: 453. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Heterotheca;species villosa;variety shevockii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (28–) 50–131 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o21894" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="28" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="131" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–35+, ascending to erect (sometimes brown proximally, sometimes ± brittle), sparsely strigose, moderately hispid (hairs often broken off in older stems), distally sparsely hispido-strigose and densely stipitate-glandular, abundantly long-hirsute.</text>
      <biological_entity id="o21895" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="35" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="distally sparsely" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="abundantly" name="pubescence" src="d0_s2" value="long-hirsute" value_original="long-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: proximal cauline subpetiolate to sessile, blades oblanceolate to lanceolate, 25–55 × 5–16 mm, bases convex-cuneate, margins entire, strigoso-ciliate, proximally long-ciliate, apices acute, mucronate, faces moderately hispido-strigose;</text>
      <biological_entity id="o21896" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal cauline" id="o21897" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="subpetiolate" name="architecture" src="d0_s3" to="sessile" />
      </biological_entity>
      <biological_entity id="o21898" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="55" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21899" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="convex-cuneate" value_original="convex-cuneate" />
      </biological_entity>
      <biological_entity id="o21900" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="strigoso-ciliate" value_original="strigoso-ciliate" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_shape" src="d0_s3" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o21901" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o21902" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s3" value="hispido-strigose" value_original="hispido-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal blades sessile, linear-lanceolate to lanceolate-triangular, (17–) 31–65 × 6–15 mm, reduced distally, bases abruptly convex-cuneate or rounded to sometimes subclasping, margins entire, usually revolute, faces sparsely to moderately long-hispido-strigose proximally, moderately to densely stipitate-glandular (branch leaves much reduced distally, linear to linear-oblanceolate).</text>
      <biological_entity id="o21903" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o21904" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="lanceolate-triangular" />
        <character char_type="range_value" from="17" from_unit="mm" name="atypical_length" src="d0_s4" to="31" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="31" from_unit="mm" name="length" src="d0_s4" to="65" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21905" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s4" value="convex-cuneate" value_original="convex-cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="subclasping" value_original="subclasping" />
      </biological_entity>
      <biological_entity id="o21906" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o21907" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately; proximally" name="pubescence" src="d0_s4" value="long-hispido-strigose" value_original="long-hispido-strigose" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (3–) 5–20 (–70) in paniculiform arrays, branches elongate on robust shoots.</text>
      <biological_entity id="o21908" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="70" />
        <character char_type="range_value" constraint="in arrays" constraintid="o21909" from="5" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o21909" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o21910" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character constraint="on shoots" constraintid="o21911" is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o21911" name="shoot" name_original="shoots" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="robust" value_original="robust" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 10–60+ mm, sparsely hispido-strigose, densely stipitate-glandular;</text>
      <biological_entity id="o21912" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="60" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 3–10, proximal leaflike, greatly reduced distally, linear to linear-oblanceolate, distalmost phyllary-like (not surpassing heads).</text>
      <biological_entity id="o21913" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21914" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" modifier="greatly; distally" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="linear-oblanceolate" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o21915" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="phyllary-like" value_original="phyllary-like" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindro-turbinate to campanulate, 9–13 mm.</text>
      <biological_entity id="o21916" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindro-turbinate" name="shape" src="d0_s8" to="campanulate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 5–6 series, unequal (outer lengths 1/5–1/4 inner), faces sparsely strigose especially along pronounced midnerves, densely stipitate-glandular.</text>
      <biological_entity id="o21917" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o21918" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
      <biological_entity id="o21919" name="face" name_original="faces" src="d0_s9" type="structure">
        <character constraint="along midnerves" constraintid="o21920" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o21920" name="midnerve" name_original="midnerves" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="pronounced" value_original="pronounced" />
      </biological_entity>
      <relation from="o21917" id="r2022" name="in" negation="false" src="d0_s9" to="o21918" />
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 9–15 (–18);</text>
      <biological_entity id="o21921" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="18" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s10" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>laminae 5–8 (–10) × 0.8–2 mm.</text>
      <biological_entity id="o21922" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets (31–) 40–70 (–80);</text>
      <biological_entity id="o21923" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="31" name="atypical_quantity" src="d0_s12" to="40" to_inclusive="false" />
        <character char_type="range_value" from="70" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="80" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas barely ampliate, 5–7 mm, tubes and throats sparsely strigose, lobes 0.3–0.8 (–1.1) mm, lobes glabrous or sparsely strigose (hairs 0.04–0.27 mm).</text>
      <biological_entity id="o21924" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="barely" name="size" src="d0_s13" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21925" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o21926" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o21927" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21928" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae monomorphic, obconic, compressed, 2–4.5 mm, ribs 8–12, faces moderately strigose;</text>
      <biological_entity id="o21929" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21930" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s14" to="12" />
      </biological_entity>
      <biological_entity id="o21931" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s14" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi off-white, outer of linear scales 0.25–1 mm, inner of 35–45 bristles 5–7 mm, longest weakly clavate (usually equaling or shorther than corollas).</text>
      <biological_entity id="o21932" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="off-white" value_original="off-white" />
      </biological_entity>
      <biological_entity id="o21933" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21935" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="35" is_modifier="true" name="quantity" src="d0_s15" to="45" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
      <relation from="o21932" id="r2023" name="outer of" negation="false" src="d0_s15" to="o21933" />
      <relation from="o21934" id="r2024" name="consist_of" negation="false" src="d0_s15" to="o21935" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 36.</text>
      <biological_entity constraint="inner" id="o21934" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="false" name="length" notes="" src="d0_s15" value="longest" value_original="longest" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21936" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jan–)Aug–Sep(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, gravelly soils and rock crevices along river in grass and open xeric pine and oak woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="river" constraint="in grass and open xeric pine and oak woods" />
        <character name="habitat" value="grass" />
        <character name="habitat" value="open xeric pine" />
        <character name="habitat" value="oak woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Shevock’s goldenaster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Known only from the lower Kern River Canyon, Kern County, Heterotheca shevockii is distinguished from other species by its usually tall stems and lanceolate-triangular mid cauline leaves with usually revolute margins, its large heads and floral parts, and pappi bristles usually only about 90% the length of the disc corollas at flowering. Smaller plants are similar to H. villosa var. scabra, known in California from a few locations in the Little San Bernardino Mountains (1200–1300 m).</discussion>
  
</bio:treatment>