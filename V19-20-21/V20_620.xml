<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">273</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Cronquist" date="1947" rank="species">rydbergii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>6: 191. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species rydbergii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066671</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2.5–6 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively short, thick.</text>
      <biological_entity id="o17617" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o17618" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, finely hirsutulous to villosulous, eglandular.</text>
      <biological_entity id="o17619" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="finely hirsutulous" name="pubescence" src="d0_s2" to="villosulous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal (persistent);</text>
      <biological_entity id="o17620" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17621" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear-oblanceolate to narrowly spatulate, 10–50 × 1–3 mm, cauline mostly on proximal 1/2 of stems, reduced distally, margins entire (apices obtuse to rounded), abaxial faces glabrous, adaxial glabrous or sparsely villous, eglandular.</text>
      <biological_entity id="o17622" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s4" to="narrowly spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o17623" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17624" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity id="o17625" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <biological_entity id="o17626" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17627" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17628" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o17623" id="r1634" name="on" negation="false" src="d0_s4" to="o17624" />
      <relation from="o17624" id="r1635" name="part_of" negation="false" src="d0_s4" to="o17625" />
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o17629" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–6 × 7–12 mm.</text>
      <biological_entity id="o17630" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, densely and finely hirsuto-villous (hairs without colored cross-walls), minutely and inconspicuously glandular.</text>
      <biological_entity id="o17631" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely; finely" name="pubescence" notes="" src="d0_s7" value="hirsuto-villous" value_original="hirsuto-villous" />
        <character is_modifier="false" modifier="minutely; inconspicuously" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o17632" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o17631" id="r1636" name="in" negation="false" src="d0_s7" to="o17632" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 15–35;</text>
      <biological_entity id="o17633" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas usually bluish to purplish, rarely white, 6–9 mm, laminae coiling.</text>
      <biological_entity id="o17634" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="usually bluish" name="coloration" src="d0_s9" to="purplish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17635" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.7–5 mm.</text>
      <biological_entity constraint="disc" id="o17636" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.8–2 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o17637" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o17638" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 15–20 bristles.</text>
      <biological_entity id="o17639" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o17640" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o17641" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
      <relation from="o17639" id="r1637" name="outer of" negation="false" src="d0_s12" to="o17640" />
      <relation from="o17639" id="r1638" name="inner of" negation="false" src="d0_s12" to="o17641" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open subalpine to alpine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine" modifier="open" />
        <character name="habitat" value="to alpine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39.</number>
  <other_name type="common_name">Rydberg’s fleabane</other_name>
  
</bio:treatment>