<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="treatment_page">49</other_info_on_meta>
    <other_info_on_meta type="illustration_page">43</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="section">macrocline</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">laciniata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 906. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section macrocline;species laciniata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200024394</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 50–300 cm (rhizomes often elongate, slender, plants colonial, roots fibrous).</text>
      <biological_entity id="o9016" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves green, blades broadly ovate to lanceolate, all but distalmost 1–2-pinnatifid or pinnately compound, leaflets/lobes 3–11, bases cuneate to attenuate or cordate, margins entire or dentate, apices acute to acuminate, faces glabrous or hairy (sometimes with translucent patches);</text>
      <biological_entity id="o9017" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o9018" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s1" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o9019" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="1-2-pinnatifid" value_original="1-2-pinnatifid" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s1" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o9020" name="lobe" name_original="leaflets/lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="11" />
      </biological_entity>
      <biological_entity id="o9021" name="base" name_original="bases" src="d0_s1" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s1" to="attenuate or cordate" />
      </biological_entity>
      <biological_entity id="o9022" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o9023" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s1" to="acuminate" />
      </biological_entity>
      <biological_entity id="o9024" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal (often withering before flowering) petiolate, 15–50 × 10–25 cm;</text>
      <biological_entity constraint="basal" id="o9025" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline petiolate or sessile, mostly lobed to pinnatifid, sometimes not lobed, 8–40 × 3–20 cm.</text>
      <biological_entity constraint="cauline" id="o9026" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="mostly lobed" name="shape" src="d0_s3" to="pinnatifid" />
        <character is_modifier="false" modifier="sometimes not" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (2–25) in loose, corymbiform arrays.</text>
      <biological_entity id="o9027" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o9028" from="2" name="atypical_quantity" src="d0_s4" to="25" />
      </biological_entity>
      <biological_entity id="o9028" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries to 2 cm (8–15, ovate to lanceolate, margins mostly ciliate, glabrous or hairy).</text>
      <biological_entity id="o9029" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacles hemispheric or ovoid to globose;</text>
      <biological_entity id="o9030" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>paleae 3–7 mm, apices (at least of proximal) truncate or rounded, abaxial tips densely hairy.</text>
      <biological_entity id="o9031" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9032" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9033" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 8–12;</text>
      <biological_entity id="o9034" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae elliptic to oblanceolate, 15–50 × 4–14 mm, abaxially hairy.</text>
      <biological_entity id="o9035" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s9" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Discs 9–30 × 10–23 mm.</text>
      <biological_entity id="o9036" name="disc" name_original="discs" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s10" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s10" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 150–300+;</text>
      <biological_entity id="o9037" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s11" to="300" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow to yellowish green (lobes yellow), 3.5–5 mm;</text>
      <biological_entity id="o9038" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="yellowish green" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style-branches 1–1.5 mm, apices acute to rounded.</text>
      <biological_entity id="o9039" name="branch-style" name_original="style-branches" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9040" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 3–4.5 mm;</text>
      <biological_entity id="o9041" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi coroniform or of 4 scales, to 1.5 mm.</text>
      <biological_entity id="o9042" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
        <character is_modifier="false" name="shape" src="d0_s15" value="of 4 scales" value_original="of 4 scales" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9043" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="4" value_original="4" />
      </biological_entity>
      <relation from="o9042" id="r623" name="consist_of" negation="false" src="d0_s15" to="o9043" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., N.B., N.S., Ont., P.E.I., Que.; Ala., Ariz., Ark., Colo., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., Mont., N.C., N.Dak., N.H., N.Mex., N.Y., Nebr., Ohio, Okla., Pa., S.C., S.Dak., Tenn., Tex., Va., Vt., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Cutleaf coneflower</other_name>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>Cultivars of Rudbeckia laciniata are grown as ornamentals. The cultivar ‘golden-glow’ is widely planted and occasionally escapes cultivation. Among the varieties traditionally recognized in floristic treatments, vars. ampla and heterophylla are the most distinctive. Detailed investigation may show that the other varieties, from eastern North America, represent broadly intergrading forms that should be subsumed under var. laciniata.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Receptacles ovoid; discs (17–)20–30 mm; w of Great Plains</description>
      <determination>7a Rudbeckia laciniata var. ampla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Receptacles globose or hemispheric; discs 10–20 mm; Great Plains and e United States</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal cauline leaves not lobed, adaxial faces moderately to densely hairy; Levy County, Florida</description>
      <determination>7d Rudbeckia laciniata var. heterophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal cauline leaves lobed, adaxial leaf faces sparsely hairy or glabrous; e North America (not Levy County, Florida)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Proximal leaves usually with 0, 3, or 5 lobes; se United States</description>
      <determination>7c Rudbeckia laciniata var. digitata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Proximal leaves usually 1–2-pinnatifid or with 5–11 lobes; e North America (not se United States).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Proximal cauline leaves 2-pinnatifid, mid cauline leaves 5–11-lobed; paleae 3.1–4.1 mm; cypselae 3.5–4 mm; pappi 0.7–1.5 mm</description>
      <determination>7b Rudbeckia laciniata var. bipinnata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Proximal cauline leaves pinnatifid, mid cauline leaves 5–9-lobed; paleae 4.4–6.1 mm; cypselae 4.2–6 mm; pappi 0.1–0.7 mm 7e. Rudbeckia laciniata var. laciniata</description>
      <next_statement_id>Rudbeckia laciniata var. laciniata</next_statement_id>
    </key_statement>
  </key>
</bio:treatment>