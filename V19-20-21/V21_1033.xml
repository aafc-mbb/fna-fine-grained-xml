<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">411</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="treatment_page">412</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaenactis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">chaenactis</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">glabriuscula</taxon_name>
    <taxon_name authority="A. Gray in War Department [U.S.]" date="1857" rank="variety">megacephala</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 104. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus chaenactis;section chaenactis;species glabriuscula;variety megacephala</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068142</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–40 cm;</text>
      <biological_entity id="o2907" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal indument grayish, arachnoid, early glabrescent.</text>
      <biological_entity constraint="proximal" id="o2908" name="indument" name_original="indument" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems mostly 1–5, ascending to erect;</text>
      <biological_entity id="o2909" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches mainly proximal.</text>
      <biological_entity id="o2910" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mainly" name="position" src="d0_s3" value="proximal" value_original="proximal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal (withering) and cauline, 1–8 cm;</text>
      <biological_entity id="o2911" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest blades ± plane, scarcely succulent, usually 1-pinnately lobed;</text>
      <biological_entity constraint="largest" id="o2912" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="scarcely" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
        <character is_modifier="false" modifier="usually 1-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lobes 2–7 pairs, ± remote, ± plane or terete.</text>
      <biological_entity id="o2913" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_density" src="d0_s6" value="remote" value_original="remote" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads mostly 1–3 per stem.</text>
      <biological_entity id="o2914" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o2915" from="1" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o2915" name="stem" name_original="stem" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 5–20 cm.</text>
      <biological_entity id="o2916" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s8" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres ± hemispheric to broadly cylindric.</text>
      <biological_entity id="o2917" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="less hemispheric" name="shape" src="d0_s9" to="broadly cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries: longest 7–9 × 2–3 mm;</text>
      <biological_entity id="o2918" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="length" src="d0_s10" value="longest" value_original="longest" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer glabrescent in fruit.</text>
      <biological_entity id="o2919" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o2920" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character constraint="in fruit" constraintid="o2921" is_modifier="false" name="pubescence" src="d0_s11" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o2921" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Florets: inner corollas 5–8 mm.</text>
      <biological_entity id="o2922" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o2923" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 5–8.5 mm;</text>
      <biological_entity id="o2924" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 4 (–5) scales in 1 series, longest scales mostly 5–8 mm, lengths ± 0.9 (–1) times corollas.</text>
      <biological_entity id="o2925" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o2926" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s14" to="5" />
        <character is_modifier="true" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o2927" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="longest" id="o2928" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character constraint="corolla" constraintid="o2929" is_modifier="false" modifier="more or less" name="length" src="d0_s14" value="0.9(-1) times corollas" value_original="0.9(-1) times corollas" />
      </biological_entity>
      <biological_entity id="o2929" name="corolla" name_original="corollas" src="d0_s14" type="structure" />
      <relation from="o2925" id="r218" name="consist_of" negation="false" src="d0_s14" to="o2926" />
      <relation from="o2926" id="r219" name="in" negation="false" src="d0_s14" to="o2927" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, often sandy slopes, openings in chaparral, woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy slopes" modifier="dry often" />
        <character name="habitat" value="openings" constraint="in chaparral" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15b.</number>
  <other_name type="common_name">Yellow pincushion</other_name>
  <discussion>Variety megacephala is known mainly from the southern Sierra Nevada foothills and adjacent San Joaquin Valley and Transverse Ranges; it intergrades extensively with vars. lanosa and glabriuscula. A specimen cited by P. Stockwell (1940) from Bingen, Klickitat County, Washington (Suksdorf, May 1907) was not checked; it is either misidentified or from an introduction that did not persist.</discussion>
  
</bio:treatment>