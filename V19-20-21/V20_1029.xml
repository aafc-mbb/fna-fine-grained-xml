<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="treatment_page">448</other_info_on_meta>
    <other_info_on_meta type="illustration_page">447</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Greene" date="1887" rank="genus">hazardia</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) Greene" date="1894" rank="species">squarrosa</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>2: 112. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus hazardia;species squarrosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066843</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">squarrosus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>146. 1833 (as Aplopappus)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species squarrosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 30–250 cm.</text>
      <biological_entity id="o30290" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually sparsely tomentose or pilose.</text>
      <biological_entity id="o30291" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o30292" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblong or oblong-obovate to widely obovate, 13–35 (–50) × 5–17 (–24) mm, coriaceous, bases clasping to subclasping, margins spinulose-dentate (teeth in 3–13 pairs), adaxial faces glabrous to sparsely puberulent.</text>
      <biological_entity id="o30293" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong-obovate" name="shape" src="d0_s3" to="widely obovate" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="24" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="17" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o30294" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="clasping" name="architecture" src="d0_s3" to="subclasping" />
      </biological_entity>
      <biological_entity id="o30295" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinulose-dentate" value_original="spinulose-dentate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30296" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s3" to="sparsely puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne in racemo-spiciform or glomerate-spiciform arrays.</text>
      <biological_entity id="o30297" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o30298" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemo-spiciform" value_original="racemo-spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="glomerate-spiciform" value_original="glomerate-spiciform" />
      </biological_entity>
      <relation from="o30297" id="r2806" name="borne in" negation="false" src="d0_s4" to="o30298" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate, 8–16 × 7–10 mm.</text>
      <biological_entity id="o30299" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="16" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries apically spreading to reflexed, oblong, apices with green area 1–2 mm, faces prominently stipitate-glandular (at least apically).</text>
      <biological_entity id="o30300" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="apically spreading" name="orientation" src="d0_s6" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o30301" name="apex" name_original="apices" src="d0_s6" type="structure" />
      <biological_entity id="o30302" name="area" name_original="area" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30303" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="prominently" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o30301" id="r2807" name="with" negation="false" src="d0_s6" to="o30302" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 0.</text>
      <biological_entity id="o30304" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 9–30;</text>
      <biological_entity id="o30305" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s8" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 9–11 mm.</text>
      <biological_entity id="o30306" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 5–8 mm, glabrous.</text>
      <biological_entity id="o30307" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Saw-tooth bristleweed</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves sparsely puberulent, not resinous; involucres 8–12 mm; disc florets 9–16, corollas 9–10 mm</description>
      <determination>3a Hazardia squarrosa var. grindelioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves glabrous or sparsely puberulent, resinous; involucres 11–15 mm; disc florets 18–30, corollas 10–11 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems glabrous or scabrous; phyllaries erect, ± obtuse-mucronate, glabrous, resinous</description>
      <determination>3b Hazardia squarrosa var. obtusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems distally sparsely hairy or glabrescent; phyllaries recurved, obtuse to acute, glandular</description>
      <determination>3c Hazardia squarrosa var. squarrosa</determination>
    </key_statement>
  </key>
</bio:treatment>