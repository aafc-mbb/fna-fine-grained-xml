<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">197</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="(Reichenbach) Nuttall" date="1841" rank="section">calliopsis</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="species">leavenworthii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 346. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section calliopsis;species leavenworthii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066427</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (10–) 30–70 (–150+) cm.</text>
      <biological_entity id="o2635" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: proximal blades usually 1 (–2) -pinnate, terminal lobes ± elliptic or oblanceolate to lanceolate or linear, 15–30+ × 4–8 (–12+) mm;</text>
      <biological_entity id="o2636" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o2637" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="1(-2)-pinnate" value_original="1(-2)-pinnate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2638" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="less elliptic or oblanceolate" name="shape" src="d0_s1" to="lanceolate or linear" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s1" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline blades simple or 1 (–3) -pinnate, simple blades or terminal lobes narrowly oblanceolate to linear, 8–35 (–70+) × 1–3 (–5) mm.</text>
      <biological_entity id="o2639" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o2640" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="1(-3)-pinnate" value_original="1(-3)-pinnate" />
      </biological_entity>
      <biological_entity id="o2641" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="70" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2642" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="70" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 2–5 (–8) cm.</text>
      <biological_entity id="o2643" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of deltate-ovate to oblong or linear bractlets 2–6+ mm.</text>
      <biological_entity id="o2644" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o2645" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="deltate-ovate" is_modifier="true" name="shape" src="d0_s4" to="oblong or linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <relation from="o2644" id="r201" name="consists_of" negation="false" src="d0_s4" to="o2645" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± lance-oblong to lanceolate, (4–) 5–9 mm.</text>
      <biological_entity id="o2646" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="less lance-oblong" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray laminae usually yellow throughout, rarely with proximal redbrown blotch, 8–15+ mm.</text>
      <biological_entity constraint="ray" id="o2647" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually; throughout" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2648" name="blotch" name_original="blotch" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <relation from="o2647" id="r202" modifier="rarely" name="with" negation="false" src="d0_s6" to="o2648" />
    </statement>
    <statement id="d0_s7">
      <text>Disc corollas 2.2–3.5 mm.</text>
      <biological_entity constraint="disc" id="o2649" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae 2–3 mm, wings 0.2–0.8+ mm wide;</text>
      <biological_entity id="o2650" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2651" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of 2 subulate scales (0.2–) 0.4–1 (–1.4+) mm.</text>
      <biological_entity id="o2653" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s9" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.4" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o2652" id="r203" name="consist_of" negation="false" src="d0_s9" to="o2653" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 24 (+ 0–2 Bs).</text>
      <biological_entity id="o2652" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o2654" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round, mostly May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, sandy soils, flatwoods, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" modifier="moist" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <discussion>Plants here (and generally) treated as Coreopsis leavenworthii probably should be included within the circumscription adopted here for C. tinctoria.</discussion>
  
</bio:treatment>