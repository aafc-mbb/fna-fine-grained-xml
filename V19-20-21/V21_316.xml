<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John C. Semple</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">BORRICHIA</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Pl.</publication_title>
      <place_in_publication>2: 130. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus BORRICHIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Ole Borch (Olaus Borrichius), 1626–1690, Danish botanist</other_info_on_name>
    <other_info_on_name type="fna_id">104290</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, subshrubs, or shrubs, to 150 cm (rhizomatous, forming clonal colonies, roots fibrous, sometimes adventitious).</text>
      <biological_entity id="o22407" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o22408" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect, branched ± throughout (sap sticky-resinous).</text>
      <biological_entity id="o22410" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>± petiolate or sessile;</text>
      <biological_entity id="o22411" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (usually 1-nerved, sometimes obscurely 3-nerved or 5-nerved) elliptic, linear, oblanceolate, obovate, or ovate (usually coriaceous or succulent), bases ± cuneate, margins entire or toothed (teeth often spine-tipped), faces glabrous or puberulent to villous and/or sericeous.</text>
      <biological_entity id="o22412" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o22413" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o22414" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o22415" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s5" to="villous and/or sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or (3–10) in cymiform arrays.</text>
      <biological_entity id="o22416" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o22417" from="3" name="atypical_quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o22417" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres globose or ovoid to hemispheric or broader, (5–) 8–13 (–18+) mm diam.</text>
      <biological_entity id="o22418" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s7" to="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="18" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 10–45 in 2–3 series (outer larger, elliptic to oblanceolate or ovate, apices acute, cuspidate, obtuse, rounded, or spine-tipped).</text>
      <biological_entity id="o22419" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o22420" from="10" name="quantity" src="d0_s8" to="45" />
      </biological_entity>
      <biological_entity id="o22420" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex, paleate (paleae lanceolate to ovate, ± conduplicate, partially enclosing cypselae, apices often ± pungent).</text>
      <biological_entity id="o22421" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 7–30, pistillate, fertile;</text>
      <biological_entity id="o22422" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="30" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o22423" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 20–75, bisexual, fertile;</text>
      <biological_entity id="o22424" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="75" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than funnelform throats, lobes 5, lance-triangular (anthers black with orange glands on connectives).</text>
      <biological_entity id="o22425" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o22426" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than funnelform throats" constraintid="o22427" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o22427" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o22428" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lance-triangular" value_original="lance-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae (gray to black) ± obcompressed, or obpyramidal and 3-angled or 4-angled (faces faintly finely reticulate);</text>
      <biological_entity id="o22429" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obpyramidal" value_original="obpyramidal" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obpyramidal" value_original="obpyramidal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-angled" value_original="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent or tardily falling, (stramineous) coroniform or cupular (3–4-angled).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 14.</text>
      <biological_entity id="o22430" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
        <character is_modifier="false" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cupular" value_original="cupular" />
      </biological_entity>
      <biological_entity constraint="x" id="o22431" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>290.</number>
  <other_name type="common_name">Sea oxeye daisy</other_name>
  <other_name type="common_name">seaside tansy or oxeye</other_name>
  <discussion>Species 3 (3 species, including 1 hybrid, in the flora).</discussion>
  <discussion>Leaves of Borrichia species are usually heteroblastic: leaves on primary stems are usually ± petiolate (sometimes with spine-tipped teeth on margins of petioles or near bases of blades) and larger and relatively broader than the usually sessile leaves on secondary stems.</discussion>
  <references>
    <reference>Semple, J. C. 1978. A revision of the genus Borrichia Adans. (Compositae). Ann. Missouri Bot. Gard. 65: 681–693.</reference>
    <reference>Semple, J. C. and K. S. Semple. 1977. Borrichia ×cubana (B. frutescens × arborescens): Interspecific hybridization in the Florida Keys. Syst. Bot. 2: 292–301.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves (at least mid cauline): margins entire or dentate to serrate (teeth remote, mostly toward apices), faces glabrous or sericeous; phyllaries (chartaceous in fruiting heads): apices acute, rounded, or obtuse; paleae: apices obtuse to acute</description>
      <determination>1 Borrichia arborescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves (at least mid cauline): margins usually dentate to serrate, rarely entire, faces usually villous (longer hairs) and/or sericeous (shorter hairs) to glabrate, rarely glabrous; phyllaries: apices cuspidate or spine-tipped; paleae: apices cuspidate or spine-tipped</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mid-cauline leaves (petioles or blade bases with spine-tipped teeth): margins dentate to serrate (usually only proximal 1/3–1/2, rarely most, of each margin); phyllaries (often reflexed, swollen in fruit): apices spine-tipped; paleae: apices spine-tipped</description>
      <determination>2 Borrichia frutescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mid-cauline leaves: margins dentate or serrate (at least mid portions, often undulate as well); phyllaries: apices cuspidate; paleae: apices cuspidate</description>
      <determination>3 Borrichia ×cubana</determination>
    </key_statement>
  </key>
</bio:treatment>