<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">514</other_info_on_meta>
    <other_info_on_meta type="mention_page">517</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="treatment_page">518</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="Rydberg" date="1931" rank="species">hirsuta</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>1: 98. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species hirsuta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067104</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="(Linnaeus) Michaux" date="unknown" rank="species">squarrosa</taxon_name>
    <taxon_name authority="(Rydberg) Gaiser" date="unknown" rank="variety">hirsuta</taxon_name>
    <taxon_hierarchy>genus Liatris;species squarrosa;variety hirsuta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–70 cm.</text>
      <biological_entity id="o12632" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose to slightly elongate.</text>
      <biological_entity id="o12633" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s1" to="slightly elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems hirsute to piloso-hirsute.</text>
      <biological_entity id="o12634" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s2" to="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline 3–5-nerved, linear-lanceolate, 60–180 × 2–7 mm (largest usually distal to proximalmost), gradually reduced distally, hirsute to piloso-hirsute.</text>
      <biological_entity id="o12635" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-5-nerved" value_original="3-5-nerved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s3" to="180" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s3" to="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in loose, racemiform to spiciform arrays.</text>
      <biological_entity id="o12636" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o12637" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="loose" value_original="loose" />
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s4" to="spiciform" />
      </biological_entity>
      <relation from="o12636" id="r868" name="in" negation="false" src="d0_s4" to="o12637" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0 or (peduncles spreading to ascending) 1–10 mm.</text>
      <biological_entity id="o12638" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="1-10 mm" value_original="1-10 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres cylindro-campanulate, 11–17 × 6–9 mm.</text>
      <biological_entity id="o12639" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries (spreading to reflexed) in 5–7 series, ovate-triangular (outer) to oblong-triangular, unequal, usually sparsely hirsute, margins without hyaline borders, coarsely hirsute-ciliate, apices acute-acuminate.</text>
      <biological_entity id="o12640" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate-triangular" name="shape" notes="" src="d0_s7" to="oblong-triangular" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o12641" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <biological_entity id="o12642" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_pubescence_or_shape" notes="" src="d0_s7" value="hirsute-ciliate" value_original="hirsute-ciliate" />
      </biological_entity>
      <biological_entity id="o12643" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o12644" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute-acuminate" value_original="acute-acuminate" />
      </biological_entity>
      <relation from="o12640" id="r869" name="in" negation="false" src="d0_s7" to="o12641" />
      <relation from="o12642" id="r870" name="without" negation="false" src="d0_s7" to="o12643" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 15–30;</text>
      <biological_entity id="o12645" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes glabrous inside (lobes adaxially hispid).</text>
      <biological_entity id="o12646" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 5.5–6.5 mm;</text>
      <biological_entity id="o12647" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths ± equaling corollas, bristles plumose.</text>
      <biological_entity id="o12648" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o12649" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o12650" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, rocky slopes, flats, marl ridges, pine-oak woods, streamsides, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="marl ridges" />
        <character name="habitat" value="pine-oak woods" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–500(–900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="50" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="900" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Iowa, Kans., La., Miss., Mo., Nebr., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Hairy gayfeather</other_name>
  <discussion>Liatris hirsuta occupies a geographic range separate from and nearly contiguous with L. squarrosa. They have been treated as a single species. Liatris hirsuta is sympatric (without intergrades) with L. squarrosa var. squarrosa in Texas, Oklahoma, Louisiana, and Mississippi; it is sympatric with L. compacta in Arkansas; it intergrades with L. cylindracea in Missouri. See also discussion under 1. L. compacta.</discussion>
  
</bio:treatment>