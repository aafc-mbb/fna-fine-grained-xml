<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="treatment_page">153</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="S. L. Welsh" date="1982" rank="species">ownbeyi</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>42: 200. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species ownbeyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066388</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–70 cm;</text>
      <biological_entity id="o587" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots and branched caudices with persistent dark-brown leaf-bases.</text>
      <biological_entity id="o588" name="taproot" name_original="taproots" src="d0_s1" type="structure" />
      <biological_entity id="o589" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o590" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <relation from="o588" id="r64" name="with" negation="false" src="d0_s1" to="o590" />
      <relation from="o589" id="r65" name="with" negation="false" src="d0_s1" to="o590" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, erect, simple or sparingly branched in distal 1/2, glabrous or thinly arachnoid and sparingly villous with jointed trichomes.</text>
      <biological_entity id="o591" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="in distal 1/2" constraintid="o592" is_modifier="false" modifier="sparingly" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="arachnoid" value_original="arachnoid" />
        <character constraint="with trichomes" constraintid="o593" is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o592" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o593" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blades oblong to elliptic or oblanceolate, 15–30+ × 2–7 cm, deeply 2–3-pinnately divided, lobes linear to linear-lanceolate, spinulose to spiny-dentate or shallowly lobed, main spines slender, 2–8 mm, abaxial faces glabrous to thinly tomentose and villous along major veins, soon glabrescent, adaxial glabrous;</text>
      <biological_entity id="o594" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o595" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="elliptic or oblanceolate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="deeply 2-3-pinnately" name="shape" src="d0_s3" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o596" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spiny-dentate" value_original="spiny-dentate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="main" id="o597" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o598" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s3" to="thinly tomentose" />
        <character constraint="along veins" constraintid="o599" is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="soon" name="pubescence" notes="" src="d0_s3" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o599" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o600" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal present at flowering, narrowly spiny winged-petiolate;</text>
      <biological_entity id="o601" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o602" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="narrowly" name="architecture_or_shape" src="d0_s4" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>principal cauline well distributed, proximal winged-petiolate, mid and distal sessile, gradually reduced, bases decurrent as spiny wings 1–3 cm;</text>
      <biological_entity id="o603" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="principal cauline" id="o604" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s5" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o605" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
      <biological_entity constraint="mid and distal" id="o606" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o607" name="base" name_original="bases" src="d0_s5" type="structure">
        <character constraint="as wings" constraintid="o608" is_modifier="false" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o608" name="wing" name_original="wings" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="spiny" value_original="spiny" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distalmost reduced to spiny-pectinate bracts.</text>
      <biological_entity id="o609" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distalmost" id="o610" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o611" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="spiny-pectinate" value_original="spiny-pectinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads 1–few, erect, ± crowded in corymbiform arrays.</text>
      <biological_entity id="o612" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s7" to="few" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character constraint="in arrays" constraintid="o613" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o613" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0–4 cm.</text>
      <biological_entity id="o614" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres ovoid, 1.8–2.5 cm, 1.5–2.5 cm diam., loosely arachnoid, glabrate.</text>
      <biological_entity id="o615" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s9" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 5–6 series, imbricate, green, linear-lanceolate, abaxial faces without or with poorly developed glutinous ridge;</text>
      <biological_entity id="o616" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o617" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o618" name="face" name_original="faces" src="d0_s10" type="structure" />
      <biological_entity id="o619" name="ridge" name_original="ridge" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="poorly" name="development" src="d0_s10" value="developed" value_original="developed" />
        <character is_modifier="true" name="coating" src="d0_s10" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o616" id="r66" name="in" negation="false" src="d0_s10" to="o617" />
      <relation from="o618" id="r67" name="without or with" negation="false" src="d0_s10" to="o619" />
    </statement>
    <statement id="d0_s11">
      <text>outer and mid-bases appressed, apices stiffly radiating to ascending, long, very narrow, entire, spines slender, 3–10 mm;</text>
      <biological_entity constraint="outer" id="o620" name="mid-base" name_original="mid-bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o621" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="stiffly" name="arrangement" src="d0_s11" value="radiating" value_original="radiating" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="false" modifier="very" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o622" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>apices of inner straight, flexuous.</text>
      <biological_entity id="o623" name="apex" name_original="apices" src="d0_s12" type="structure" constraint="straight; straight">
        <character is_modifier="false" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o624" name="straight" name_original="straight" src="d0_s12" type="structure" />
      <relation from="o623" id="r68" name="part_of" negation="false" src="d0_s12" to="o624" />
    </statement>
    <statement id="d0_s13">
      <text>Corollas white to pink or pink-purple, 16–20 mm, tubes 6–8 mm, throats 5–6 mm, lobes 5–7 mm;</text>
      <biological_entity id="o625" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pink or pink-purple" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s13" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o626" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o627" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o628" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style tips 3.5–4.5 mm.</text>
      <biological_entity constraint="style" id="o629" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae brown, ca. 4 mm, apical collars not differentiated;</text>
      <biological_entity id="o630" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="apical" id="o631" name="collar" name_original="collars" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s15" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 13–17 mm.</text>
      <biological_entity id="o632" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s16" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stony soils in sparsely vegetated areas of pinyon-juniper woodlands, sagebrush scrub, arid grasslands, and riparian scrub, in dry sites or sometimes on seeps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stony soils" constraint="in sparsely vegetated areas of pinyon-juniper woodlands , sagebrush scrub ," />
        <character name="habitat" value="vegetated areas" modifier="sparsely" constraint="of pinyon-juniper woodlands , sagebrush scrub ," />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="sagebrush scrub" />
        <character name="habitat" value="grasslands" modifier="arid" />
        <character name="habitat" value="riparian scrub" />
        <character name="habitat" value="dry sites" modifier="in" />
        <character name="habitat" value="seeps" modifier="sometimes on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52.</number>
  <other_name type="common_name">Ownbey’s thistle</other_name>
  <discussion>Cirsium ownbeyi is endemic to the eastern side of the Uintah Mountains in northeastern Utah, southwestern Wyoming, and northwestern Colorado. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>