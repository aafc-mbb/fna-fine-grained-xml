<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="treatment_page">401</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">dieteria</taxon_name>
    <taxon_name authority="(Pursh) Nuttall" date="1840" rank="species">canescens</taxon_name>
    <taxon_name authority="(B. L. Turner) D. R. Morgan &amp; R. L. Hartman" date="2003" rank="variety">nebraskana</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 1397. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus dieteria;species canescens;variety nebraskana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068259</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(Pursh) A. Gray" date="unknown" rank="species">canescens</taxon_name>
    <taxon_name authority="B. L. Turner" date="unknown" rank="variety">nebraskana</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>60: 78. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Machaeranthera;species canescens;variety nebraskana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or short-lived perennials.</text>
      <biological_entity id="o24931" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect, canescent, sometimes sparsely stipitate-glandular;</text>
      <biological_entity id="o24933" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches ascending.</text>
      <biological_entity id="o24934" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Mid leaf-blades lanceolate to linear-oblanceolate.</text>
      <biological_entity constraint="mid" id="o24935" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear-oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles longer to shorter than involucres or 0 (heads sessile).</text>
      <biological_entity id="o24936" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character constraint="than involucres" constraintid="o24937" is_modifier="false" name="size_or_length" src="d0_s4" value="longer to shorter" value_original="longer to shorter" />
      </biological_entity>
      <biological_entity id="o24937" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres broadly turbinate, (9–) 10–15 mm.</text>
      <biological_entity id="o24938" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 5–10 series, reflexed, apices usually densely appressed-hairy, sometimes stipitate-glandular, then obscured by hairs.</text>
      <biological_entity id="o24939" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o24940" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o24941" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s6" value="appressed-hairy" value_original="appressed-hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="by hairs" constraintid="o24942" is_modifier="false" name="prominence" src="d0_s6" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o24942" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o24939" id="r2293" name="in" negation="false" src="d0_s6" to="o24940" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets pistillate, fertile.</text>
      <biological_entity id="o24943" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae sparsely to moderately appressed-hairy.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 8.</text>
      <biological_entity id="o24944" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s8" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24945" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, pine forests, usually in sandy soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="sandy soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nebr., S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3j.</number>
  
</bio:treatment>