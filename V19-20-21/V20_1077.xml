<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">473</other_info_on_meta>
    <other_info_on_meta type="treatment_page">479</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Nuttall) Semple in J. C. Semple et al." date="2002" rank="subgenus">astropolium</taxon_name>
    <taxon_name authority="(Linnaeus) G. L. Nesom" date="1995" rank="species">tenuifolium</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 293. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus astropolium;species tenuifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067689</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">tenuifolius</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 873. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species tenuifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (20–) 40–60 (–100) cm, colonial or cespitose;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o4461" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, ascending to erect (often with purple or purplish brown areas, flexuous, wiry), little-branched, usually glabrous, sometimes hairy in lines distally.</text>
      <biological_entity id="o4462" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="little-branched" value_original="little-branched" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="in lines" constraintid="o4463" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4463" name="line" name_original="lines" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves thick (fleshy), margins entire, faces glabrous;</text>
      <biological_entity id="o4464" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o4465" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4466" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering (new winter rosettes appearing at flowering), petiolate (petioles sheathing), blades ovate or oblanceolate, 15–20 × 5–15 mm, bases cuneate, apices rounded;</text>
      <biological_entity constraint="basal" id="o4467" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, apices" constraintid="o4468, o4469, o4470" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o4468" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4469" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4470" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline withering by flowering, usually sessile, blades lanceolate, linear-lanceolate or oblanceolate, to linear, 7–80 (–150) × 1–6 (–12) mm, bases attenuate, margins entire, apices acuminate to acute;</text>
      <biological_entity constraint="proximal cauline" id="o4471" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades, bases, margins, apices" constraintid="o4472, o4473, o4474, o4475" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o4472" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o4473" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o4474" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o4475" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="150" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades narrowly lanceolate to subulate, 10–110 × 0.5–5 mm, apices acuminate.</text>
      <biological_entity constraint="distal" id="o4476" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o4477" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s6" to="subulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="110" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4478" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (1–) 3–20 (–40), in open, diffuse, paniculiform arrays, branches patent.</text>
      <biological_entity id="o4479" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s7" to="3" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="40" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
      <biological_entity id="o4480" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="in open , diffuse , paniculiform arrays" name="orientation" src="d0_s7" value="patent" value_original="patent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0.6–4 (–6) cm, bracts 2–8, awl-shaped, grading into phyllaries.</text>
      <biological_entity id="o4481" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4482" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="shape" src="d0_s8" value="awl--shaped" value_original="awl--shaped" />
      </biological_entity>
      <biological_entity id="o4483" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o4482" id="r413" name="into" negation="false" src="d0_s8" to="o4483" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres narrowly turbinate, 4.1–9.5 (–11) mm.</text>
      <biological_entity id="o4484" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="some_measurement" src="d0_s9" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–5 series, lanceolate to subulate, bases indurate, rounded, margins hyaline, often tinged with purple, entire, green zones spatulate to oblanceolate-rhombic, apices acute, adaxial faces glabrous or minutely hairy distally.</text>
      <biological_entity id="o4485" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s10" to="subulate" />
      </biological_entity>
      <biological_entity id="o4486" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o4487" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4488" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="tinged with purple" value_original="tinged with purple" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4489" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s10" to="oblanceolate-rhombic" />
      </biological_entity>
      <biological_entity id="o4490" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4491" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely; distally" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o4485" id="r414" name="in" negation="false" src="d0_s10" to="o4486" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 10–25;</text>
      <biological_entity id="o4492" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas white or pink, laminae (4.5–) 5–8.5 (–9.5) × 1.2–2 mm.</text>
      <biological_entity id="o4493" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o4494" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_length" src="d0_s12" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 25–45 (–54);</text>
      <biological_entity id="o4495" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="54" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow becoming purplish, 3.4–6 (–6.8) mm, tubes shorter than to equaling the narrowly funnelform throats (sparsely hairy at base), lobes ± erect, narrowly triangular, 0.5–0.8 mm.</text>
      <biological_entity id="o4496" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s14" value="yellow becoming purplish" value_original="yellow becoming purplish" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="6.8" to_unit="mm" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4497" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character is_modifier="false" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o4498" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o4499" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly; more or less" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae light-brown, narrowly obovoid to fusiform, sometimes ± compressed, 1.5–4 (–4.5) mm, 5–6-nerved, faces sparsely strigillose;</text>
      <biological_entity id="o4500" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="narrowly obovoid" name="shape" src="d0_s15" to="fusiform" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-6-nerved" value_original="5-6-nerved" />
      </biological_entity>
      <biological_entity id="o4501" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi tawny to white, 3–6.1 mm.</text>
      <biological_entity id="o4502" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="tawny" name="coloration" src="d0_s16" to="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="6.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Conn., Del., Fla., Ga., La., Maine, Mass., Md., Miss., N.C., N.H., N.J., N.Y., R.I., S.C., Tex., Va.; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Perennial saltmarsh aster</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Along the Gulf of Mexico Coast in central and northern peninsular Florida, vars. tenuifolium and aphyllum intergrade in nearly all characters (S. D. Sundberg 2004). G. L. Nesom (2005d) treated the varieties as species.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 1, rhizomes long; mid leaves (1.5–)3–6 mm wide; involucres 6–9.5(–11) mm; ray florets (12–)17–25; disc florets 25–45(–54), corollas (4–)4.7–6(–6.5) mm; cypselae 2.8–4(–4.5) mm; pappi 5–6.1 mm</description>
      <determination>2a Symphyotrichum tenuifolium var. tenuifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 1–5+ (clustered), rhizomes short; mid leaves (1–)1.5–2.7 mm wide; involucres 4.1–5.3 mm; ray florets 10–16; disc florets (10–)13–23, corollas 3.4–4.6 mm; cypselae 1.5–2(–2.5) mm; pappi 3–4.4 mm</description>
      <determination>2b Symphyotrichum tenuifolium var. aphyllum</determination>
    </key_statement>
  </key>
</bio:treatment>