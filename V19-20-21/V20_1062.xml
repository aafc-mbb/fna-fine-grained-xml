<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
    <other_info_on_meta type="illustration_page">459</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1995" rank="genus">canadanthus</taxon_name>
    <taxon_name authority="(Lindley) G. L. Nesom" date="1995" rank="species">modestus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 251. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus canadanthus;species modestus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066285</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">modestus</taxon_name>
    <place_of_publication>
      <publication_title>in W. J. Hooker, Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 8. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species modestus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Hooker) Porter" date="unknown" rank="species">major</taxon_name>
    <taxon_hierarchy>genus Aster;species major;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">modestus</taxon_name>
    <taxon_name authority="(Hooker) Muenscher" date="unknown" rank="variety">major</taxon_name>
    <taxon_hierarchy>genus Aster;species modestus;variety major;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Lessing ex Bongard" date="unknown" rank="species">sayianus</taxon_name>
    <taxon_hierarchy>genus Aster;species sayianus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Lindley) Á. Löve &amp; D. Löve" date="unknown" rank="species">unalaschensis</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">major</taxon_name>
    <taxon_hierarchy>genus Aster;species unalaschensis;variety major;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Weberaster</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">modestus</taxon_name>
    <taxon_hierarchy>genus Weberaster;species modestus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants openly colonial;</text>
      <biological_entity id="o21471" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="openly" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 1–3+ from base of each stem, herbaceous, ± woody with age.</text>
      <biological_entity id="o21472" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="from base" constraintid="o21473" from="1" name="quantity" src="d0_s1" to="3" upper_restricted="false" />
        <character is_modifier="false" name="growth_form_or_texture" notes="" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character constraint="with age" constraintid="o21475" is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o21473" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o21474" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <biological_entity id="o21475" name="age" name_original="age" src="d0_s1" type="structure" />
      <relation from="o21473" id="r1983" name="part_of" negation="false" src="d0_s1" to="o21474" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1, often glabrate proximally to ± villous, distally stipitate-glandular.</text>
      <biological_entity id="o21476" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="proximally to more or less" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: proximal scalelike, withering by flowering;</text>
      <biological_entity id="o21477" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o21478" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="scale-like" value_original="scalelike" />
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (16–) 50–130 × (4–) 10–30 (–40) mm, reduced distally, thin, bases auriculate-clasping, margins ± scabrous to (distal) stipitate-glandular.</text>
      <biological_entity id="o21479" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21480" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="atypical_length" src="d0_s4" to="50" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o21481" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o21482" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="less scabrous" name="pubescence" src="d0_s4" to="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (1–) 2–40+.</text>
      <biological_entity id="o21483" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="40" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles sometimes villous, densely stipitate-glandular;</text>
      <biological_entity id="o21484" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 1–2 (–3), linear-lanceolate, stipitate-glandular.</text>
      <biological_entity id="o21485" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="2" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries squarrose, innermost appressed, apices often ± purplish, acuminate.</text>
      <biological_entity id="o21486" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="squarrose" value_original="squarrose" />
      </biological_entity>
      <biological_entity constraint="innermost" id="o21487" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o21488" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often more or less" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-floret laminae 7–11 (–15) × 0.8–1.2 mm.</text>
      <biological_entity constraint="ray-floret" id="o21489" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-floret corollas 5–7 mm, glabrous, lobes 0.5–0.7 mm.</text>
      <biological_entity constraint="disc-floret" id="o21490" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21491" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae stramineous, stipitate;</text>
      <biological_entity id="o21492" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="stipitate" value_original="stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi surpassing or ± equaling disc corollas.</text>
      <biological_entity constraint="disc" id="o21494" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o21493" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="position_relational" src="d0_s12" value="surpassing" value_original="surpassing" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21495" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cold, wet soils, often ± calcareous, moist woodlands, often along streams, lake shores, alder thickets, open fields, cedar swamps, in montane and boreal forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cold" />
        <character name="habitat" value="wet soils" />
        <character name="habitat" value="calcareous" modifier="often" />
        <character name="habitat" value="moist woodlands" />
        <character name="habitat" value="streams" modifier="often along" />
        <character name="habitat" value="lake shores" />
        <character name="habitat" value="alder thickets" />
        <character name="habitat" value="open fields" />
        <character name="habitat" value="cedar swamps" />
        <character name="habitat" value="montane" modifier="in" />
        <character name="habitat" value="boreal forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Ont., Sask., Yukon, Que.; Alaska, Idaho, Mich., Minn., Mont., N.Dak., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Great northern or western bog aster</other_name>
  <other_name type="common_name">aster modeste</other_name>
  
</bio:treatment>