<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="treatment_page">357</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">stephanomeria</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">runcinata</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 428. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus stephanomeria;species runcinata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067608</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–20 (–25) cm (rhizomes stout).</text>
      <biological_entity id="o15046" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, glabrous or weakly scabrous-puberulent.</text>
      <biological_entity id="o15047" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="weakly" name="pubescence" src="d0_s1" value="scabrous-puberulent" value_original="scabrous-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually withered at flowering;</text>
      <biological_entity id="o15048" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s2" value="withered" value_original="withered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades narrowly lanceolate, markedly runcinate, 3–7 cm, margins pinnately lobed (faces glabrous or weakly scabrous-puberulent);</text>
      <biological_entity constraint="basal" id="o15049" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="markedly" name="shape" src="d0_s3" value="runcinate" value_original="runcinate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15050" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline much reduced, bractlike.</text>
      <biological_entity constraint="cauline" id="o15051" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly on branch tips.</text>
      <biological_entity id="o15052" name="head" name_original="heads" src="d0_s5" type="structure">
        <character constraint="on branch tips" constraintid="o15053" is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity constraint="branch" id="o15053" name="tip" name_original="tips" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 10–50 mm (usually minutely bracteolate).</text>
      <biological_entity id="o15054" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of appressed bractlets (unequal, lengths to 1/2 phyllaries).</text>
      <biological_entity id="o15055" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o15056" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o15055" id="r1367" name="consists_of" negation="false" src="d0_s7" to="o15056" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres 9–12 (–13) mm (phyllaries 5–6, glabrous).</text>
      <biological_entity id="o15057" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 5–6.</text>
      <biological_entity id="o15058" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae tan, 3–5 mm, faces smooth to slightly bumpy, grooved;</text>
      <biological_entity id="o15059" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="tan" value_original="tan" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15060" name="face" name_original="faces" src="d0_s10" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s10" to="slightly bumpy" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of 15–25, white bristles (persistent), wholly plumose.</text>
      <biological_entity id="o15062" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s11" to="25" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <relation from="o15061" id="r1368" name="consist_of" negation="false" src="d0_s11" to="o15062" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 16.</text>
      <biological_entity id="o15061" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="wholly" name="shape" notes="" src="d0_s11" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15063" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering June–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="June" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy places, eroded siltstones, clay flats, alkali soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sandy places" />
        <character name="habitat" value="eroded siltstones" />
        <character name="habitat" value="clay flats" />
        <character name="habitat" value="alkali soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Colo., Mont., Nebr., N.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Desert or sawtooth wirelettuce</other_name>
  <discussion>Stephanomeria runcinata grows in the upper Great Plains and adjacent intermontane valleys.</discussion>
  
</bio:treatment>