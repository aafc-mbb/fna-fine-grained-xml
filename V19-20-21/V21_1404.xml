<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="treatment_page">553</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Spach" date="1841" rank="genus">ageratina</taxon_name>
    <taxon_name authority="(Sprengel) R. M. King &amp; H. Robinson" date="1970" rank="species">adenophora</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>19: 211. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus ageratina;species adenophora</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242412095</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Sprengel" date="unknown" rank="species">adenophorum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>3: 420. 1826,;  4(qto.): 122, plate 346. 1820, not Michaux 1803</place_in_publication>
      <other_info_on_pub>based on E. glandulosum Kunth in A. von Humboldt et al., Nov. Gen. Sp. 4(fol.): 96, plate 346. 1818</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species adenophorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 50–220 cm.</text>
      <biological_entity id="o6379" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="220" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (usually purplish when young) erect, stipitate-glandular.</text>
      <biological_entity id="o6380" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o6381" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 10–25 mm;</text>
      <biological_entity id="o6382" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (abaxially purple) ovatelanceolate or ovate-deltate to lanceolate-ovate, (1.5–) 2.5–5.5 (–8) × 1.5–4 (–6) cm, bases cuneate to obtuse or nearly truncate, margins serrate, apices acute to acuminate, abaxial faces stipitate to sessile-glandular.</text>
      <biological_entity id="o6383" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate-deltate" name="shape" src="d0_s4" to="lanceolate-ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s4" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6384" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o6385" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o6386" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6387" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="stipitate" name="architecture" src="d0_s4" to="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads clustered.</text>
      <biological_entity id="o6388" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–12 mm, densely stipitate-glandular and sometimes also sparsely viscid-puberulent.</text>
      <biological_entity id="o6389" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s6" value="viscid-puberulent" value_original="viscid-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 3.5–4 mm.</text>
      <biological_entity id="o6390" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries: apices acute, abaxial faces stipitate-glandular.</text>
      <biological_entity id="o6391" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o6392" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6393" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas white, pink-tinged, lobes sparsely hispidulous.</text>
      <biological_entity id="o6394" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink-tinged" value_original="pink-tinged" />
      </biological_entity>
      <biological_entity id="o6395" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae glabrous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 51.</text>
      <biological_entity id="o6396" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6397" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="51" value_original="51" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream margins, ditches, road embankments, hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream margins" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="road embankments" />
        <character name="habitat" value="hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Intoduced; Calif.; Mexico; also introduced in Europe, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Intoduced" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="also  in Europe" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Crofton weed</other_name>
  <other_name type="common_name">sticky snakeroot</other_name>
  
</bio:treatment>