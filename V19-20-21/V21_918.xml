<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="treatment_page">369</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arnica</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="species">unalaschcensis</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>6: 238. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus arnica;species unalaschcensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066129</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 8–30 cm.</text>
      <biological_entity id="o2358" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple (sparsely to moderately hairy, hairs translucent with purple septa).</text>
      <biological_entity id="o2359" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 2–5 pairs, mostly cauline;</text>
      <biological_entity id="o2360" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolate (proximal) or sessile;</text>
      <biological_entity id="o2361" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades elliptic-oblanceolate, obovate or spatulate, (3–) 5–12 × 1.5–3 cm, margins usually entire proximal to mid-blade, serrate to dentate distally, apices acute to obtuse or abruptly pointed, faces usually glabrate, sometimes scabrous to sparsely stipitate-glandular.</text>
      <biological_entity id="o2362" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic-oblanceolate" value_original="elliptic-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2363" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character constraint="to mid-blade" constraintid="o2364" is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="distally" name="shape_or_architecture" notes="" src="d0_s4" value="serrate to dentate" value_original="serrate to dentate" />
      </biological_entity>
      <biological_entity id="o2364" name="mid-blade" name_original="mid-blade" src="d0_s4" type="structure" />
      <biological_entity id="o2365" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse or abruptly pointed" />
      </biological_entity>
      <biological_entity id="o2366" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character char_type="range_value" from="scabrous" modifier="sometimes" name="pubescence" src="d0_s4" to="sparsely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (erect).</text>
      <biological_entity id="o2367" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric or turbinate-hemispheric (bases moderately villous).</text>
      <biological_entity id="o2368" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate-hemispheric" value_original="turbinate-hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 14–24, lanceolate (apices obtuse, tips blunt, callous).</text>
      <biological_entity id="o2369" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s7" to="24" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 6–17;</text>
      <biological_entity id="o2370" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow (laminae 12–17 mm).</text>
      <biological_entity id="o2371" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets: corollas yellow;</text>
      <biological_entity id="o2372" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure" />
      <biological_entity id="o2373" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers dark purple.</text>
      <biological_entity id="o2374" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure" />
      <biological_entity id="o2375" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae brown, 4–5 mm, shortly hispidulous (hairs duplex, apically forked) and stipitate-glandular;</text>
      <biological_entity id="o2376" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi tawny, bristles subplumose.</text>
      <biological_entity id="o2377" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tawny" value_original="tawny" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 38.</text>
      <biological_entity id="o2378" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="subplumose" value_original="subplumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2379" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal tundra to alpine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal tundra" />
        <character name="habitat" value="alpine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Asia (Japan, Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Alaska arnica</other_name>
  
</bio:treatment>