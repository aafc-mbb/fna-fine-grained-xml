<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
    <other_info_on_meta type="treatment_page">323</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Jepson" date="1925" rank="species">algidus</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>1052. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species algidus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066545</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2–30 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudex or rhizome branches relatively short and thick.</text>
      <biological_entity id="o14855" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o14856" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity constraint="rhizome" id="o14857" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sparsely villoso-hirsute, minutely glandular.</text>
      <biological_entity id="o14858" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="villoso-hirsute" value_original="villoso-hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal (persistent);</text>
      <biological_entity id="o14859" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14860" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades obovate to oblanceolate, 10–70 × 2–7 (–12) mm, cauline abruptly reduced distally, margins entire (apices rounded), faces strigose to strigoso-hirsute, sometimes sparsely glandular.</text>
      <biological_entity id="o14861" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o14862" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o14863" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14864" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s4" to="strigoso-hirsute" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o14865" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–8 × 8–18 mm.</text>
      <biological_entity id="o14866" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in (2–) 3–4 series (often purplish throughout or at margins and tips), hirsute to hirsuto-villous (hair cross-walls not colored), minutely glandular.</text>
      <biological_entity id="o14867" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="hirsute" name="pubescence" notes="" src="d0_s7" to="hirsuto-villous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o14868" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o14867" id="r1356" name="in" negation="false" src="d0_s7" to="o14868" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 25–125;</text>
      <biological_entity id="o14869" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s8" to="125" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas blue to pink or purple, 5–13 mm (mostly 1–2 mm wide), laminae coiling.</text>
      <biological_entity id="o14870" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="pink or purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14871" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3–4.8 mm.</text>
      <biological_entity constraint="disc" id="o14872" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.9–2.1 mm, 2-nerved, faces strigose;</text>
      <biological_entity id="o14873" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s11" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o14874" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 10–15 (–20) bristles.</text>
      <biological_entity id="o14875" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o14876" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o14877" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s12" to="20" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s12" to="15" />
      </biological_entity>
      <relation from="o14875" id="r1357" name="outer of" negation="false" src="d0_s12" to="o14876" />
      <relation from="o14875" id="r1358" name="inner of" negation="false" src="d0_s12" to="o14877" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky sites and meadows, alpine or near timberline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="near timberline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>120.</number>
  <other_name type="common_name">Stalked fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>