<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helenium</taxon_name>
    <taxon_name authority="Small" date="1903" rank="species">campestre</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1291, 1341. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus helenium;species campestre</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066851</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–100 cm.</text>
      <biological_entity id="o14755" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, branched distally, moderately to strongly winged, densely (the proximal) to moderately hairy.</text>
      <biological_entity id="o14756" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="moderately to strongly" name="architecture" src="d0_s1" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="densely to moderately" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves moderately to densely hairy;</text>
      <biological_entity id="o14757" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades oblanceolate to obovate to spatulate, entire or undulate-serrate;</text>
      <biological_entity constraint="basal" id="o14758" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal and mid-blades oblanceolate to lanceolate, usually entire, sometimes undulate-serrate;</text>
      <biological_entity constraint="proximal" id="o14759" name="mid-blade" name_original="mid-blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="lanceolate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="undulate-serrate" value_original="undulate-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal blades lanceolate to lance-linear, entire.</text>
      <biological_entity constraint="distal" id="o14760" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="lance-linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 3–20 per plant, in paniculiform to corymbiform arrays.</text>
      <biological_entity id="o14761" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o14762" from="3" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o14762" name="plant" name_original="plant" src="d0_s6" type="structure" />
      <biological_entity id="o14763" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s6" to="corymbiform" />
      </biological_entity>
      <relation from="o14761" id="r1019" name="in" negation="false" src="d0_s6" to="o14763" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 3–11 cm, moderately to densely hairy.</text>
      <biological_entity id="o14764" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="11" to_unit="cm" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres globoid to ovoid, 9–17 × 10–20 mm.</text>
      <biological_entity id="o14765" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (barely connate proximally) moderately to densely hairy.</text>
      <biological_entity id="o14766" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 9–15, neuter;</text>
      <biological_entity id="o14767" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s10" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 20–30 × 8–15 mm.</text>
      <biological_entity id="o14768" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 150–500 (–700+);</text>
      <biological_entity id="o14769" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="500" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="700" upper_restricted="false" />
        <character char_type="range_value" from="150" name="quantity" src="d0_s12" to="500" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow proximally, reddish-brown to purple distally, 3–5 mm, lobes 5.</text>
      <biological_entity id="o14770" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="reddish-brown" modifier="distally" name="coloration" src="d0_s13" to="purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14771" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1–1.4 mm, sparsely to moderately hairy;</text>
      <biological_entity id="o14772" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of 6–7, entire, non-aristate scales 0.3–0.5 mm. 2n = 28.</text>
      <biological_entity id="o14773" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity id="o14774" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s15" to="7" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="true" name="shape" src="d0_s15" value="non-aristate" value_original="non-aristate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14775" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
      </biological_entity>
      <relation from="o14773" id="r1020" name="consist_of" negation="false" src="d0_s15" to="o14774" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ditches, fields, in washes, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ditches" />
        <character name="habitat" value="fields" constraint="in washes" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Oldfield sneezeweed</other_name>
  
</bio:treatment>