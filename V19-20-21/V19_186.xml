<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="treatment_page">170</other_info_on_meta>
    <other_info_on_meta type="illustration_page">170</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arctium</taxon_name>
    <taxon_name authority="(Hill) Bernhardi" date="1800" rank="species">minus</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Verz.,</publication_title>
      <place_in_publication>154. 1800</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus arctium;species minus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416085</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lappa</taxon_name>
    <taxon_name authority="Hill" date="unknown" rank="species">minor</taxon_name>
    <place_of_publication>
      <publication_title>Veg. Syst.</publication_title>
      <place_in_publication>4: 28. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lappa;species minor;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 50–300 cm.</text>
      <biological_entity id="o7354" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: petioles hollow (sometimes only at base), 15–50 cm, thinly to densely cobwebby;</text>
      <biological_entity constraint="basal" id="o7355" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o7356" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="hollow" value_original="hollow" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="thinly to densely" name="pubescence" src="d0_s1" value="cobwebby" value_original="cobwebby" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 30–60 × 15–35 cm, coarsely dentate to subentire (rarely deeply dissected), abaxially ± thinly gray-tomentose, adaxially green, sparsely short-hairy.</text>
      <biological_entity constraint="basal" id="o7357" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7358" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s2" to="60" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="coarsely dentate" name="shape" src="d0_s2" to="subentire" />
        <character is_modifier="false" modifier="abaxially more or less thinly" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in racemiform or paniculiform clusters, sessile to pedunculate.</text>
      <biological_entity id="o7359" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="racemiform" value_original="racemiform" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s3" to="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 0–9.5 cm.</text>
      <biological_entity id="o7360" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="9.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 15–40 mm diam.</text>
      <biological_entity id="o7361" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries linear to linear-lanceolate, glabrous to densely cobwebby, inner often purplish tinged, margins often minutely serrate with fine teeth, puberulent with glandular and or eglandular hairs.</text>
      <biological_entity id="o7362" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s6" to="densely cobwebby" />
      </biological_entity>
      <biological_entity constraint="inner" id="o7363" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish tinged" value_original="purplish tinged" />
      </biological_entity>
      <biological_entity id="o7364" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character constraint="with teeth" constraintid="o7365" is_modifier="false" modifier="often minutely" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character constraint="with glandular and" constraintid="" is_modifier="false" name="pubescence" notes="" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o7365" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="true" name="width" src="d0_s6" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o7366" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o7364" id="r677" name="with" negation="false" src="d0_s6" to="o7366" />
    </statement>
    <statement id="d0_s7">
      <text>Florets 30+;</text>
      <biological_entity id="o7367" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas purple, pink, or white, 7.5–12 mm, glabrous or limb glandular-puberulent.</text>
      <biological_entity id="o7368" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7369" name="limb" name_original="limb" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae dark-brown or with darker spots, 5–8 mm;</text>
      <biological_entity id="o7370" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character constraint="with darker spots" is_modifier="false" name="coloration" src="d0_s9" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="with darker spots" value_original="with darker spots" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappus bristles 1–3.5 mm. 2n = 32 (Germany), 36 (as A. nemorosum).</text>
      <biological_entity constraint="pappus" id="o7371" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7372" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="32" value_original="32" />
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall (Jul–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, roadsides, fields, forest clearings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="forest clearings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Ala., Ariz., Ark., Calif., Colo., Conn., Del., D.C., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Common or lesser burdock</other_name>
  <other_name type="common_name">petite bardane</other_name>
  <other_name type="common_name">cibourroche</other_name>
  <other_name type="common_name">chou bourache</other_name>
  <other_name type="common_name">bourrier</other_name>
  <discussion>Arctium minus has been reported from Delaware and Texas; I have not seen specimens.</discussion>
  <discussion>Arctium minus is a complex species with many variants that have been recognized at ranks ranging from forma to species (J. Arènes 1950). Some North American workers (e.g., R. J. Moore and C. Frankton 1974) have often distinguished plants with involucres more than 3 cm diameter that equal or overtop the corollas as A. nemorosum. Arènes treated those plants as a subspecies of A. minus. Arctium nemorosum was recognized as a species distinct from A. minus (H. Duistermaat 1996), with a different and more restricted circumscription than that used by North American workers. Although most of the characters that Duistermaat used to separate those A. nemorosum from A. minus overlap extensively, the consistently wider mid phyllaries of A. nemorosum (1.7–2.5 mm wide versus 0.6–1.6 mm in A. minus) supposedly distinguish the species. None of the North American specimens examined in preparation of this treatment had the wide phyllaries of A. nemorosum in the sense of Duistermaat, who stated that she had seen no material of this taxon from the American continent. Some American authors have taken up the name Arctium vulgare in place of A. nemorosum and applied A. vulgare (dubbed woodland burdock) to the larger-headed North American plants. Duistermaat considers A. vulgare to be a synonym of A. lappa.</discussion>
  
</bio:treatment>