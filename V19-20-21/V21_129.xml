<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">rudbeckia</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">subtomentosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 575. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section rudbeckia;species subtomentosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417176</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, to 200 cm (rhizomatous, rhizomes stout).</text>
      <biological_entity id="o6344" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems densely hirsute (hairs mostly antrorse, to 0.5 mm).</text>
      <biological_entity id="o6345" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades ovate to elliptic (not lobed), margins denticulate to serrate, apices acute to obtuse or acuminate, faces densely hirsute and glanddotted (glands fewer adaxially);</text>
      <biological_entity id="o6346" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6347" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="elliptic" />
      </biological_entity>
      <biological_entity id="o6348" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s2" to="serrate" />
      </biological_entity>
      <biological_entity id="o6349" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse or acuminate" />
      </biological_entity>
      <biological_entity id="o6350" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal 15–30 × 3–10 cm, bases attenuate;</text>
      <biological_entity id="o6351" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o6352" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6353" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline petiolate, ovate to elliptic, proximal 3–25 × 1–15 cm, usually 3–5-lobed, bases truncate to cuneate or rounded.</text>
      <biological_entity id="o6354" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o6355" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="elliptic" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6356" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <biological_entity id="o6357" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="cuneate or rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (8–25) in loose, corymbiform to paniculiform arrays.</text>
      <biological_entity id="o6358" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o6359" from="8" name="atypical_quantity" src="d0_s5" to="25" />
      </biological_entity>
      <biological_entity id="o6359" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="loose" value_original="loose" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s5" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries to 1.5 cm (faces hairy and ± glanddotted).</text>
      <biological_entity id="o6360" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles conic to hemispheric;</text>
      <biological_entity id="o6361" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s7" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>paleae 4–6 mm, apices acute, abaxial tips hirsute and glanddotted.</text>
      <biological_entity id="o6362" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6363" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6364" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 10–16;</text>
      <biological_entity id="o6365" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae (yellow to yellow-orange) linear to oblanceolate, 20–40 × 5–8 mm, abaxially sparsely hairy, abundantly glanddotted.</text>
      <biological_entity id="o6366" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s10" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Discs 10–17 × 5–15 mm.</text>
      <biological_entity id="o6367" name="disc" name_original="discs" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 200–400+;</text>
      <biological_entity id="o6368" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="200" name="quantity" src="d0_s12" to="400" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellowish green on basal 1/2, otherwise brown-purple, 3–4.2 mm;</text>
      <biological_entity id="o6369" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character constraint="on basal 1/2" constraintid="o6370" is_modifier="false" name="coloration" src="d0_s13" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="otherwise" name="coloration_or_density" notes="" src="d0_s13" value="brown-purple" value_original="brown-purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6370" name="1/2" name_original="1/2" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style-branches ca. 1 mm, apices acute.</text>
      <biological_entity id="o6371" name="branch-style" name_original="style-branches" src="d0_s14" type="structure">
        <character name="distance" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6372" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 2–3.5 mm;</text>
      <biological_entity id="o6373" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi coroniform, to ca. 0.2 mm. 2n = 38.</text>
      <biological_entity id="o6374" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6375" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic to wet prairies, stream banks, and woodland openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic to wet prairies" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="woodland openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Conn., Ill., Ind., Iowa, Kans., Ky., La., Mass., Mich., Miss., Mo., N.Y., N.C., Okla., Tenn., Tex., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Sweet coneflower</other_name>
  <discussion>Rudbeckia subtomentosa is often cultivated as an ornamental.</discussion>
  
</bio:treatment>