<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="treatment_page">598</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Greene) C. Jeffrey" date="1992" rank="species">quercetorum</taxon_name>
    <place_of_publication>
      <publication_title>Kew Bull.</publication_title>
      <place_in_publication>47: 101. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species quercetorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067266</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">quercetorum</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>2: 20. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species quercetorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greenman" date="unknown" rank="species">macropus</taxon_name>
    <taxon_hierarchy>genus Senecio;species macropus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 60–100+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted (caudices subligneous, ascending to erect).</text>
      <biological_entity id="o10474" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 or 2–4, clustered (proximally deeply purple-tinged, distally lightly tinged), glabrous or tomentose at bases and in leaf-axils.</text>
      <biological_entity id="o10475" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="4" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="at bases and in leaf-axils" constraintid="o10476" is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o10476" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline) petiolate;</text>
      <biological_entity constraint="basal" id="o10477" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades obovate or lyrate (pinnately lobed, lateral lobes 2–6+ pairs, their bases petioluliform, terminal lobes larger than laterals, midribs narrowly winged), 60–160+ × 20–40+ mm, bases wide, ultimate margins sharply dentate, crenate-dentate, or irregularly incised.</text>
      <biological_entity id="o10478" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="160" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10479" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o10480" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="incised" value_original="incised" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves gradually reduced (petiolate or sessile; shallowly lobed, midribs ± winged, distals bractlike, dentate to incised).</text>
      <biological_entity constraint="cauline" id="o10481" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 15–40+ in open, cymiform arrays.</text>
      <biological_entity id="o10482" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o10483" from="15" name="quantity" src="d0_s6" to="40" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10483" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles ebracteate, glabrous.</text>
      <biological_entity id="o10484" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi inconspicuous.</text>
      <biological_entity id="o10485" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (13–) 21, green (tips yellow), 5–7 mm, glabrous (tips sometimes hairy).</text>
      <biological_entity id="o10486" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" name="atypical_quantity" src="d0_s9" to="21" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="21" value_original="21" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets (8–) 13;</text>
      <biological_entity id="o10487" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s10" to="13" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 6–10+ mm.</text>
      <biological_entity constraint="corolla" id="o10488" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 60–70+;</text>
      <biological_entity id="o10489" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s12" to="70" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 2–3 mm, limbs 3.5–4.5 mm.</text>
      <biological_entity id="o10490" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10491" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1.5–2 mm, glabrous or ± scabrellous;</text>
      <biological_entity id="o10492" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s14" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 5.5–6.5 mm. 2n = 92.</text>
      <biological_entity id="o10493" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s15" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10494" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="92" value_original="92" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Apr–early Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jun" from="mid Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soils, open areas, scrub-oak and pinyon-pine forests, chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="open areas" />
        <character name="habitat" value="scrub-oak" />
        <character name="habitat" value="pinyon-pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <other_name type="common_name">Oak Creek ragwort</other_name>
  <discussion>Packera quercetorum is found only infrequently and in relatively small populations in central and southern Arizona and west-central New Mexico. The plants are robust and have probable affinities to P. multilobata. The plants have a bluish tinge when freshly collected and are distinctive in the field.</discussion>
  
</bio:treatment>