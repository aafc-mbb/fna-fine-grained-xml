<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">503</other_info_on_meta>
    <other_info_on_meta type="mention_page">522</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="treatment_page">533</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">artemisia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">vulgaris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 848. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus artemisia;species vulgaris</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200023371</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Pampanini" date="unknown" rank="species">opulenta</taxon_name>
    <taxon_hierarchy>genus Artemisia;species opulenta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_name authority="Ledebour" date="unknown" rank="variety">glabra</taxon_name>
    <taxon_hierarchy>genus Artemisia;species vulgaris;variety glabra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_name authority="Besser" date="unknown" rank="variety">kamtschatica</taxon_name>
    <taxon_hierarchy>genus Artemisia;species vulgaris;variety kamtschatica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (40–) 60–190 cm, sometimes faintly aromatic (rhizomes coarse).</text>
      <biological_entity id="o3363" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="190" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes faintly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems relatively numerous, erect, brownish to reddish-brown, simple proximally, branched distally (angularly ribbed), sparsely hairy or glabrous.</text>
      <biological_entity id="o3364" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="brownish" name="coloration" src="d0_s1" to="reddish-brown" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal (petiolate) and cauline (sessile), uniformly green or bicolor;</text>
      <biological_entity id="o3365" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="bicolor" value_original="bicolor" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades broadly lanceolate, ovate, or linear, (2–) 3–10 (–12) × 1.8–8 cm (proximal reduced and entire, distal pinnately dissected, lobes to 20 mm wide), faces pubescent or glabrescent (abaxial) or glabrous (adaxial).</text>
      <biological_entity id="o3366" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3367" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in compact, paniculiform or racemiform arrays (10–) 20–30 (–40) × (5–) 7–15 (–20) cm.</text>
      <biological_entity id="o3368" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="in compact" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_length" src="d0_s4" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_width" src="d0_s4" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ovoid to campanulate, 2–3 (–4) mm.</text>
      <biological_entity id="o3369" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries lanceolate, hairy or glabrescent.</text>
      <biological_entity id="o3370" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 7–10;</text>
      <biological_entity id="o3371" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual (5–) 8–20;</text>
      <biological_entity id="o3372" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellowish to reddish-brown, 1.5–3 mm, glabrous (style-branches arched-curved, truncate, ciliate).</text>
      <biological_entity id="o3373" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o3374" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s9" to="reddish-brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae ellipsoid, 0.5–1 (–1.2) mm, glabrous, sometimes resinous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 18, 36, 40, 54.</text>
      <biological_entity id="o3375" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="coating" src="d0_s10" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3376" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
        <character name="quantity" src="d0_s11" value="36" value_original="36" />
        <character name="quantity" src="d0_s11" value="40" value_original="40" />
        <character name="quantity" src="d0_s11" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or loamy soils, forested areas, coastal strands, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="loamy soils" />
        <character name="habitat" value="forested areas" />
        <character name="habitat" value="coastal strands" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Greenland; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Ala., Alaska, Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Mont., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., R.I., S.C., Tenn., Vt., Va., Wash., W.Va., Wis.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>50.</number>
  <other_name type="common_name">Common mugwort</other_name>
  <other_name type="common_name">felon-herb</other_name>
  <other_name type="common_name">green-ginger</other_name>
  <other_name type="common_name">armoise vulgaire</other_name>
  <discussion>Grown as a medicinal plant, most commonly as a vermifuge, Artemisia vulgaris is widely established in eastern North America and is often weedy in disturbed sites. Populational differences in morphologic forms are reflected in size of flowering heads, degree of dissection of leaves, and overall color of plants (from pale to dark green), suggesting multiple introductions that may date back to the first visits by Europeans. It is tempting to recognize the different forms as subspecies and varieties; the array of variation in the field is bewildering. If genetically distinct forms exist in native populations, the differences appear to have been blurred by introgression among the various introductions in North America. A case could be made for recognizing var. kamtschatica in Alaska based on its larger heads and shorter growth form; apparent introgression with populations that extend across Canada confounds that taxonomic segregation.</discussion>
  
</bio:treatment>