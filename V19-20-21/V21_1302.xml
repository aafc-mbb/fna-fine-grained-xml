<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">512</other_info_on_meta>
    <other_info_on_meta type="illustration_page">512</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Mociño ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">carminatia</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">tenuiflora</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 267. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus carminatia;species tenuiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220002372</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brickellia</taxon_name>
    <taxon_name authority="(de Candolle) D. J. Keil &amp; Pinkava" date="unknown" rank="species">tenuiflora</taxon_name>
    <taxon_hierarchy>genus Brickellia;species tenuiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually puberulent and villous (hairs crinkled, multicellular), sometimes glabrate.</text>
      <biological_entity id="o20724" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Petioles 1–5 cm, villous-ciliate.</text>
      <biological_entity id="o20725" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s1" value="villous-ciliate" value_original="villous-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades (proximal to mid-stem) broadly ovate to cordate, 2–8 × 1.5–6 cm, bases obtuse to truncate or subcordate, margins subentire to dentate, margins villous-ciliolate (sometimes sparsely villous along veins as well), apices obtuse to acute.</text>
      <biological_entity id="o20726" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s2" to="cordate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20727" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="truncate or subcordate" />
      </biological_entity>
      <biological_entity id="o20728" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s2" to="dentate" />
      </biological_entity>
      <biological_entity id="o20729" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous-ciliolate" value_original="villous-ciliolate" />
      </biological_entity>
      <biological_entity id="o20730" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1–10+ per principal node, ascending to erect;</text>
      <biological_entity constraint="principal" id="o20732" name="node" name_original="node" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sessile or on peduncles to 7 mm.</text>
      <biological_entity id="o20731" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per principal node" constraintid="o20732" from="1" name="quantity" src="d0_s3" to="10" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" notes="" src="d0_s3" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="on peduncles" value_original="on peduncles" />
      </biological_entity>
      <biological_entity id="o20733" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 9–12 mm.</text>
      <biological_entity id="o20734" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries glabrous (margins sometimes ciliolate).</text>
      <biological_entity id="o20735" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Corollas 5–6 mm, slightly exceeding pappi, 0.2–0.3 mm diam., apices slightly constricted, lobes 0.15–0.2 mm;</text>
      <biological_entity id="o20736" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s7" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20737" name="pappus" name_original="pappi" src="d0_s7" type="structure" />
      <biological_entity id="o20738" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s7" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o20739" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.15" from_unit="mm" name="some_measurement" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o20736" id="r1426" name="slightly exceeding" negation="false" src="d0_s7" to="o20737" />
    </statement>
    <statement id="d0_s8">
      <text>anthers included, ca. 0.8 mm;</text>
      <biological_entity id="o20740" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style-branches usually short-exserted.</text>
      <biological_entity id="o20741" name="style-branch" name_original="style-branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s9" value="short-exserted" value_original="short-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae gray to nearly black, 3.5–5 mm;</text>
      <biological_entity id="o20742" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s10" to="nearly black" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi white, 5–8 mm. 2n = 20.</text>
      <biological_entity id="o20743" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20744" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak-juniper woodlands, cypress woodlands, grasslands, riparian areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak-juniper woodlands" />
        <character name="habitat" value="cypress woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="riparian areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>