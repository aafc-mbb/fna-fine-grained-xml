<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="mention_page">139</other_info_on_meta>
    <other_info_on_meta type="mention_page">140</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="treatment_page">137</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Nuttall) Jepson" date="1901" rank="species">occidentale</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">occidentale</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species occidentale;variety occidentale</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068197</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, usually 30–150 cm or taller.</text>
      <biological_entity id="o8861" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="30" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="height" src="d0_s0" value="taller" value_original="taller" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf faces usually densely tomentose abaxially, less so and sometimes glabrate adaxially.</text>
      <biological_entity constraint="leaf" id="o8862" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually densely; abaxially" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="less; sometimes; adaxially" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads usually long pedunculate, sometimes in tight clusters at ends of peduncles, elevated well above proximal leaves.</text>
      <biological_entity id="o8863" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="pedunculate" value_original="pedunculate" />
        <character constraint="above proximal leaves" constraintid="o8866" is_modifier="false" name="prominence" notes="" src="d0_s2" value="elevated" value_original="elevated" />
      </biological_entity>
      <biological_entity id="o8864" name="end" name_original="ends" src="d0_s2" type="structure" />
      <biological_entity id="o8865" name="peduncle" name_original="peduncles" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o8866" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o8863" id="r819" modifier="in tight clusters" name="at" negation="false" src="d0_s2" to="o8864" />
      <relation from="o8864" id="r820" name="part_of" negation="false" src="d0_s2" to="o8865" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres usually wider than long, 4–5 cm diam., ± densely and persistently arachnoid with fine trichomes connecting tips of adjacent phyllaries.</text>
      <biological_entity id="o8867" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="usually wider than long" value_original="usually wider than long" />
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s3" to="5" to_unit="cm" />
        <character constraint="with trichomes" constraintid="o8868" is_modifier="false" modifier="more or less densely; densely; persistently" name="pubescence" src="d0_s3" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
      <biological_entity id="o8868" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o8869" name="tip" name_original="tips" src="d0_s3" type="structure" />
      <biological_entity id="o8870" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o8868" id="r821" name="connecting" negation="false" src="d0_s3" to="o8869" />
      <relation from="o8867" id="r822" name="consist_of" negation="false" src="d0_s3" to="o8870" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries usually ± imbricate, outer ascending or spreading or reflexed, mid phyllary apices ascending to spreading, straight or distally curved, usually 1–2 cm × 1–2 mm.</text>
      <biological_entity id="o8871" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity constraint="phyllary" id="o8872" name="apex" name_original="apices" src="d0_s4" type="structure" constraint_original="outer phyllary">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="phyllary" id="o8873" name="apex" name_original="apices" src="d0_s4" type="structure" constraint_original="mid phyllary">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="spreading" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="distally" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="usually" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="usually" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Corollas ± bright purple, usually 25–35 mm. 2n = 28, 29, 30.</text>
      <biological_entity id="o8874" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="bright purple" value_original="bright purple" />
        <character char_type="range_value" from="25" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s5" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8875" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="28" value_original="28" />
        <character name="quantity" src="d0_s5" value="29" value_original="29" />
        <character name="quantity" src="d0_s5" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Mar–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal scrub, chaparral, oak woodlands, stabilized dunes, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="dunes" modifier="stabilized" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40a.</number>
  <other_name type="common_name">Cobwebby thistle</other_name>
  <discussion>Variety occidentale occupies a variety of habitats in the coastal zone of southern and central California. Considerable variation occurs from population to population in head size, flower color, and pubescence. It sometimes occurs together with and appears to intergrade with var. coulteri. Where there has been no hybridization, the two may be strikingly dissimilar, but individuals of some populations cannot be assigned with confidence to either variety.</discussion>
  
</bio:treatment>