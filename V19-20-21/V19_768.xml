<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">445</other_info_on_meta>
    <other_info_on_meta type="mention_page">462</other_info_on_meta>
    <other_info_on_meta type="treatment_page">461</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">diaperia</taxon_name>
    <taxon_name authority="(Rafinesque) Morefield" date="2004" rank="species">verna</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>14: 468. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus diaperia;species verna</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">250066478</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Evax</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">verna</taxon_name>
    <place_of_publication>
      <publication_title>Atlantic J.</publication_title>
      <place_in_publication>1: 178. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Evax;species verna;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Evax</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">multicaulis</taxon_name>
    <taxon_hierarchy>genus Evax;species multicaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants greenish to grayish, 2–15 (–25) cm, ± lanuginose.</text>
      <biological_entity id="o14415" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s0" to="grayish" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="lanuginose" value_original="lanuginose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly 2–10;</text>
      <biological_entity id="o14416" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches proximal and distal (distal subopposite), rarely none.</text>
      <biological_entity id="o14417" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s2" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: largest 7–13 × 2–4 mm;</text>
      <biological_entity id="o14418" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>capitular leaves subtending glomerules, also ± hidden between and surpassed by heads.</text>
      <biological_entity id="o14419" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="capitular" id="o14420" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14421" name="glomerule" name_original="glomerules" src="d0_s4" type="structure" />
      <biological_entity id="o14422" name="head" name_original="heads" src="d0_s4" type="structure" />
      <relation from="o14420" id="r1316" name="subtending" negation="false" src="d0_s4" to="o14421" />
      <relation from="o14420" id="r1317" modifier="more or less" name="by" negation="false" src="d0_s4" to="o14422" />
    </statement>
    <statement id="d0_s5">
      <text>Heads mostly distal, in subdichasiform arrays, campanulate to ± spheric, 2–3.3 mm, heights ± equal to diams.</text>
      <biological_entity id="o14423" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character char_type="range_value" from="campanulate" name="shape" notes="" src="d0_s5" to="more or less spheric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3.3" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="diams" src="d0_s5" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o14424" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="subdichasiform" value_original="subdichasiform" />
      </biological_entity>
      <relation from="o14423" id="r1318" name="in" negation="false" src="d0_s5" to="o14424" />
    </statement>
    <statement id="d0_s6">
      <text>Receptacles pulvinate, 0.3–0.6 mm, heights ± 0.2–0.5 times diams.</text>
      <biological_entity id="o14425" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pulvinate" value_original="pulvinate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="height" src="d0_s6" value="0.2-0.5 times diams" value_original="0.2-0.5 times diams" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate paleae scarcely imbricate, longest 1.9–2.7 mm.</text>
      <biological_entity id="o14426" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="length" src="d0_s7" value="longest" value_original="longest" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s7" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate paleae mostly 3–5, apices somewhat spreading, ± plane.</text>
      <biological_entity id="o14427" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o14428" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Functionally staminate florets 3–5;</text>
      <biological_entity id="o14429" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovaries vestigial, 0–0.1 mm;</text>
      <biological_entity id="o14430" name="ovary" name_original="ovaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="vestigial" value_original="vestigial" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas hidden in heads, actinomorphic, 1.8–2.5 mm, often ± spreading-arachnoid, lobes equal.</text>
      <biological_entity id="o14431" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character constraint="in heads" constraintid="o14432" is_modifier="false" name="prominence" src="d0_s11" value="hidden" value_original="hidden" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="actinomorphic" value_original="actinomorphic" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="often more or less" name="pubescence" src="d0_s11" value="spreading-arachnoid" value_original="spreading-arachnoid" />
      </biological_entity>
      <biological_entity id="o14432" name="head" name_original="heads" src="d0_s11" type="structure" />
      <biological_entity id="o14433" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Bisexual florets 0.</text>
      <biological_entity id="o14434" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae rounded, ± terete, mostly 0.7–0.9 mm. 2n = 26.</text>
      <biological_entity id="o14435" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.7" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s13" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14436" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Ga., La., N.Mex., Okla., S.C., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Spring or many-stem rabbit-tobacco</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The two varieties of Diaperia verna intergrade within a broad band inland from the Gulf of Mexico in southeastern Texas. Though some specimens are difficult to assign with confidence, the varieties show enough correlated geographic and ecologic segregation to warrant taxonomic recognition.</discussion>
  <discussion>As neotypified by J. D. Morefield (2004), the name Evax verna now applies to the taxon that de Candolle named E. multicaulis.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate paleae collectively hidden by thick lanuginose indument; heads ± campanulate, larg- est mostly 2–2.5 mm</description>
      <determination>2a Diaperia verna var. verna</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate paleae individually visible through thin sericeous indument; heads ± spheric, largest mostly 2.5–3.3 mm</description>
      <determination>2b Diaperia verna var. drummondii</determination>
    </key_statement>
  </key>
</bio:treatment>