<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">630</other_info_on_meta>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">tetradymia</taxon_name>
    <taxon_name authority="A. Gray" date="1877" rank="species">comosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 60. 1877</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus tetradymia;species comosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067719</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 30–120 cm.</text>
      <biological_entity id="o24695" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5+, erect (wandlike), unarmed, evenly pannose.</text>
      <biological_entity id="o24696" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s1" value="pannose" value_original="pannose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: primaries lance-linear, stiff, pungent, not spinose, 20–60 (× 2–5) mm, tomentose;</text>
      <biological_entity id="o24697" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24698" name="primary" name_original="primaries" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="odor_or_shape" src="d0_s2" value="pungent" value_original="pungent" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s2" value="spinose" value_original="spinose" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="60[" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5]" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>secondaries (seldom present) spatulate, 8–15 mm, tomentose.</text>
      <biological_entity id="o24699" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24700" name="secondary" name_original="secondaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 3–6.</text>
      <biological_entity id="o24701" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 3–8 mm.</text>
      <biological_entity id="o24702" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric, 8 mm.</text>
      <biological_entity id="o24703" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 5–6, oblanceolate.</text>
      <biological_entity id="o24704" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="6" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 5–9;</text>
      <biological_entity id="o24705" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow to brownish, 8–9 mm.</text>
      <biological_entity id="o24706" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="brownish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae ca. 4 mm, copiously pilose (hairs 6–10 mm);</text>
      <biological_entity id="o24707" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" modifier="copiously" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0.2n = 60.</text>
      <biological_entity id="o24708" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24709" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly sandy sites, chaparral, coastal sage scrub, sagebrush scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy sites" modifier="mostly" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="coastal sage scrub" />
        <character name="habitat" value="sagebrush scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  
</bio:treatment>