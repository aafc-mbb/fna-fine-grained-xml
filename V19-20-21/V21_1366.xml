<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="genus">carphephorus</taxon_name>
    <taxon_name authority="(Michaux) Torrey &amp; A. Gray" date="1841" rank="species">tomentosus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 66. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus carphephorus;species tomentosus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066293</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 93. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Liatris;species tomentosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carphephorus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tomentosus</taxon_name>
    <taxon_name authority="(Elliott) Fernald" date="unknown" rank="variety">walteri</taxon_name>
    <taxon_hierarchy>genus Carphephorus;species tomentosus;variety walteri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–80 cm.</text>
      <biological_entity id="o4365" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely to densely hirsute to hirsute-villous, sometimes strigose distally, glanddotted.</text>
      <biological_entity id="o4366" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="hirsute" modifier="densely" name="pubescence" src="d0_s1" to="hirsute-villous" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline oblanceolate, mostly 3.5–15 cm;</text>
      <biological_entity id="o4367" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline gradually reduced, faces glanddotted.</text>
      <biological_entity id="o4368" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o4369" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4370" name="face" name_original="faces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in flat-topped, corymbiform arrays.</text>
      <biological_entity id="o4371" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o4372" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o4371" id="r346" name="in" negation="false" src="d0_s4" to="o4372" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles villoso-hirsute, glanddotted.</text>
      <biological_entity id="o4373" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villoso-hirsute" value_original="villoso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 7–11 mm.</text>
      <biological_entity id="o4374" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 15–40+ in 3–5+ series, ovatelanceolate to broadly ovate, villous, and glanddotted, apices acute to obtuse.</text>
      <biological_entity id="o4375" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4376" from="15" name="quantity" src="d0_s7" to="40" upper_restricted="false" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" notes="" src="d0_s7" to="broadly ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o4376" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4377" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles partially paleate (paleae often 4–5).</text>
      <biological_entity id="o4378" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="partially" name="architecture" src="d0_s8" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas glandular, lobes ca. 2 mm.</text>
      <biological_entity id="o4379" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o4380" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae eglandular;</text>
      <biological_entity id="o4381" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappus bristles in ± 1 series.</text>
      <biological_entity id="o4383" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4382" id="r347" name="in" negation="false" src="d0_s11" to="o4383" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity constraint="pappus" id="o4382" name="bristle" name_original="bristles" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o4384" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry pine savannas, flatwoods, pine-oak woodland, wire-grass savannas, fields, sometimes more moist, peaty soils, shrub bogs, seepage</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry pine savannas" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="pine-oak woodland" />
        <character name="habitat" value="wire-grass savannas" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="moist to dry pine savannas" modifier="moist" />
        <character name="habitat" value="moist" modifier="sometimes more" />
        <character name="habitat" value="peaty soils" />
        <character name="habitat" value="shrub bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Woolly chaffhead</other_name>
  
</bio:treatment>