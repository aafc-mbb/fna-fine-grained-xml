<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Curtis Clark</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="treatment_page">112</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="A. Nelson" date="1909" rank="genus">ENCELIOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>47: 432. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus ENCELIOPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Generic name Encelia and Greek -opsis, resembling</other_info_on_name>
    <other_info_on_name type="fna_id">111591</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 15–100+ cm (caudices or taproots woody).</text>
      <biological_entity id="o10691" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched from bases.</text>
      <biological_entity id="o10692" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from bases" constraintid="o10693" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o10693" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o10694" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-nerved, elliptic, ovate, rhombic, or suborbiculate, bases ± cuneate to nearly truncate, margins entire (sometimes corrugate or ruffled), faces densely puberulent or silky-velutinous.</text>
      <biological_entity id="o10695" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
      </biological_entity>
      <biological_entity id="o10696" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate to nearly" value_original="cuneate to nearly" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o10697" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10698" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="silky-velutinous" value_original="silky-velutinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly (peduncles much longer than involucres).</text>
      <biological_entity id="o10699" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± hemispheric or broader, 10–30+ mm diam.</text>
      <biological_entity id="o10700" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="true" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="30" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 30–65+ in 3–6 series.</text>
      <biological_entity id="o10701" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o10702" from="30" name="quantity" src="d0_s8" to="65" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10702" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex, paleate (paleae ± conduplicate, folded around and falling with cypselae).</text>
      <biological_entity id="o10703" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets (11–) 20–35+, neuter;</text>
      <biological_entity id="o10704" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="11" name="atypical_quantity" src="d0_s10" to="20" to_inclusive="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="35" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o10705" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets (50–) 200–500+, bisexual, fertile;</text>
      <biological_entity id="o10706" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s12" to="200" to_inclusive="false" />
        <character char_type="range_value" from="200" name="quantity" src="d0_s12" to="500" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than to equaling abruptly expanded throats, lobes 5, triangular.</text>
      <biological_entity id="o10707" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o10708" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than to equaling abruptly expanded throats" constraintid="o10709" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o10709" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" modifier="abruptly" name="size" src="d0_s13" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o10710" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae strongly compressed or flattened, ± cuneate (margins ± white, corky, usually ciliate, faces black, glabrous or ± silky-villous);</text>
      <biological_entity id="o10711" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, or persistent, of 2 awns (often with 2–10+, often connate, minute scales or teeth as well).</text>
      <biological_entity id="o10713" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <relation from="o10712" id="r735" name="consist_of" negation="false" src="d0_s15" to="o10713" />
    </statement>
    <statement id="d0_s16">
      <text>x = 18.</text>
      <biological_entity id="o10712" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o10714" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>278.</number>
  <discussion>Species 3 (3 in the flora).</discussion>
  <references>
    <reference>Sanders, D. L. and C. Clark. 1987. Comparative morphology of the capitulum of Enceliopsis. Amer. J. Bot. 74: 1072–1086.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbage dull gray; petioles not or barely winged; leaf blades ovate to suborbiculate</description>
      <determination>1 Enceliopsis nudicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbage silvery; petioles winged (wings merging with blades); leaf blades rhombic or widely elliptic</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray corolla laminae deep yellow, 12–30 mm (lengths 1–1.2 times disc diams.)</description>
      <determination>2 Enceliopsis argophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray corolla laminae light yellow, 30–50 mm (lengths 1.5–2 times disc diams.)</description>
      <determination>3 Enceliopsis covillei</determination>
    </key_statement>
  </key>
</bio:treatment>