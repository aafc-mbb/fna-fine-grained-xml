<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">544</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chromolaena</taxon_name>
    <taxon_name authority="(A. Gray) R. M. King &amp; H. Robinson" date="1970" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>20: 208. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus chromolaena;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066333</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 75. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species bigelovii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">madrense</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species madrense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 50–150 cm.</text>
      <biological_entity id="o1890" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sparsely puberulent.</text>
      <biological_entity id="o1891" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petioles 1–4 mm.</text>
      <biological_entity id="o1892" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades deltate to deltate-lanceolate or ovatelanceolate, 3–7 × 1–4 cm, margins serrate to dentate (abaxial faces reticulate-veiny with raised whitish, secondary-veins, minutely glanddotted).</text>
      <biological_entity id="o1893" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s3" to="deltate-lanceolate or ovatelanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1894" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="serrate" name="architecture_or_shape" src="d0_s3" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads usually in 3s, sometimes borne singly.</text>
      <biological_entity id="o1895" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="in 3s; sometimes" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0 or 2–8 (–12) mm.</text>
      <biological_entity id="o1896" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="2-8(-12) mm" value_original="2-8(-12) mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate to campanulate, 5–7 mm.</text>
      <biological_entity id="o1897" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s6" to="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 4–5 series, apices of the inner appressed, acute (not petaloid or expanded).</text>
      <biological_entity id="o1898" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o1899" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o1900" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1901" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <relation from="o1898" id="r143" name="in" negation="false" src="d0_s7" to="o1899" />
      <relation from="o1900" id="r144" name="part_of" negation="false" src="d0_s7" to="o1901" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas pale blue or white.</text>
      <biological_entity id="o1902" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale blue" value_original="pale blue" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry limestone hills in oak woodlands, talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry limestone hills" constraint="in oak woodlands , talus" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, San Luis Potosí).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Bigelow’s false thoroughwort</other_name>
  
</bio:treatment>