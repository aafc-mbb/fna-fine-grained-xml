<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="treatment_page">377</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">pyrrhopappus</taxon_name>
    <taxon_name authority="A. Gray" date="1876" rank="species">rothrockii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 80. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus pyrrhopappus;species rothrockii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067417</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (possibly flowering first-year), 15–40 cm.</text>
      <biological_entity id="o7828" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems seldom, if ever, scapiform, branching from bases and/or distally, glabrous or pilosulous proximally.</text>
      <biological_entity id="o7829" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="ever" name="shape" src="d0_s1" value="scapiform" value_original="scapiform" />
        <character constraint="from bases" constraintid="o7830" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
      <biological_entity id="o7830" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves (1–) 3–9+, proximal mostly spatulate or oblanceolate to linear, margins entire, dentate, or pinnately lobed, distal usually narrowly lanceolate to lance-attenuate, margins usually entire or with 1–2 lobes near bases, sometimes pinnately 3–5 (–7+) -lobed.</text>
      <biological_entity constraint="cauline" id="o7831" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="9" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7832" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="linear" />
      </biological_entity>
      <biological_entity id="o7833" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7834" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually narrowly lanceolate" name="shape" src="d0_s2" to="lance-attenuate" />
      </biological_entity>
      <biological_entity id="o7835" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="with 1-2 lobes" value_original="with 1-2 lobes" />
        <character is_modifier="false" modifier="sometimes pinnately" name="shape" notes="" src="d0_s2" value="3-5(-7+)-lobed" value_original="3-5(-7+)-lobed" />
      </biological_entity>
      <biological_entity id="o7836" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
      <biological_entity id="o7837" name="base" name_original="bases" src="d0_s2" type="structure" />
      <relation from="o7835" id="r726" name="with" negation="false" src="d0_s2" to="o7836" />
      <relation from="o7836" id="r727" name="near" negation="false" src="d0_s2" to="o7837" />
    </statement>
    <statement id="d0_s3">
      <text>Heads (1–) 3–5+ in loose, corymbiform arrays.</text>
      <biological_entity id="o7838" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o7839" from="3" name="quantity" src="d0_s3" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7839" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi: bractlets 3–5+ in 1–2 series, deltate to subulate, 2–5 mm.</text>
      <biological_entity id="o7840" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o7841" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o7842" from="3" name="quantity" src="d0_s4" to="5" upper_restricted="false" />
        <character char_type="range_value" from="deltate" name="shape" notes="" src="d0_s4" to="subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7842" name="series" name_original="series" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± cylindric, 12–15 (–20+) mm.</text>
      <biological_entity id="o7843" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="20" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 13–16+.</text>
      <biological_entity id="o7844" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s6" to="16" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets (20–) 30–60+;</text>
      <biological_entity id="o7845" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s7" to="30" to_inclusive="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s7" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 3.5–4 mm (pollen equatorial diameters unknown).</text>
      <biological_entity id="o7846" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae: bodies stramineous, 3–4 mm, beaks 6–7 mm;</text>
      <biological_entity id="o7847" name="cypsela" name_original="cypselae" src="d0_s9" type="structure" />
      <biological_entity id="o7848" name="body" name_original="bodies" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7849" name="beak" name_original="beaks" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 6–7 mm. 2n = 12.</text>
      <biological_entity id="o7850" name="cypsela" name_original="cypselae" src="d0_s10" type="structure" />
      <biological_entity id="o7851" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7852" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, stream banks, flood plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="flood plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>For the present, I concur with D. K. Northington (1974) and I treat Pyrrhopappus rothrockii as distinct from P. pauciflorus.</discussion>
  
</bio:treatment>