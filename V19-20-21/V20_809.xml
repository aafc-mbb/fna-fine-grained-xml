<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">352</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">aphanostephus</taxon_name>
    <taxon_name authority="(de Candolle) Trelease" date="1891" rank="species">skirrhobasis</taxon_name>
    <taxon_name authority="(S. F. Blake) B. L. Turner" date="1984" rank="variety">kidderi</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>56: 94. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus aphanostephus;species skirrhobasis;variety kidderi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068042</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphanostephus</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="species">kidderi</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>53: 23. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aphanostephus;species kidderi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (10–) 15–35 cm.</text>
      <biological_entity id="o26449" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decumbent, hirsuto-pilose and/or puberulent, hairs (0.2–) 0.5–1.2 mm.</text>
      <biological_entity id="o26450" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsuto-pilose" value_original="hirsuto-pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o26451" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not strongly thickened or felty gray-hairy.</text>
      <biological_entity id="o26452" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not strongly" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="felty" value_original="felty" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="gray-hairy" value_original="gray-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray-florets 1–9–40.</text>
      <biological_entity id="o26453" name="ray-floret" name_original="ray-florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="9-40" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pappi of acute or awn-tipped or setiform scales 0.4–2 mm. 2n = 6.</text>
      <biological_entity id="o26454" name="pappus" name_original="pappi" src="d0_s4" type="structure" />
      <biological_entity id="o26455" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="awn-tipped" value_original="awn-tipped" />
        <character is_modifier="true" name="shape" src="d0_s4" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26456" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="6" value_original="6" />
      </biological_entity>
      <relation from="o26454" id="r2440" name="consists_of" negation="false" src="d0_s4" to="o26455" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly soils, often in matorral and scrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="matorral" modifier="often" />
        <character name="habitat" value="scrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  <discussion>Apparent intergrades between var. kidderi and var. skirrhobasis are relatively common. There is a greater tendency in Aphanostephus skirrhobasis for leaves to be lobed or pinnatifid over the whole stem than in the other species.</discussion>
  
</bio:treatment>