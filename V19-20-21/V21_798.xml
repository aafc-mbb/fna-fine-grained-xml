<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="treatment_page">322</other_info_on_meta>
    <other_info_on_meta type="illustration_page">320</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">perityle</taxon_name>
    <taxon_name authority="(L. H. Dewey) Rydberg in N. L. Britton et al." date="1914" rank="species">ciliata</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 17. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section perityle;species ciliata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067313</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Laphamia</taxon_name>
    <taxon_name authority="L. H. Dewey" date="unknown" rank="species">ciliata</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>20: 425. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Laphamia;species ciliata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 15–30 cm (in rock crevices, stems relatively many, erect to pendulous);</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to densely short-hairy, glandular.</text>
      <biological_entity id="o14333" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 2–15 mm;</text>
      <biological_entity id="o14334" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14335" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades deltate-ovate to ovate-rhombic, 6–23 × 5–24 mm, margins usually entire or serrate to serrate-crenate, sometimes shallow-lobed.</text>
      <biological_entity id="o14336" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14337" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="deltate-ovate" name="shape" src="d0_s3" to="ovate-rhombic" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="23" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14338" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s3" value="serrate to serrate-crenate" value_original="serrate to serrate-crenate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="shallow-lobed" value_original="shallow-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in corymbiform arrays, 5–7 × 5.5–7 mm.</text>
      <biological_entity id="o14339" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" notes="" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14340" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o14339" id="r989" name="in" negation="false" src="d0_s4" to="o14340" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–25 mm.</text>
      <biological_entity id="o14341" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate.</text>
      <biological_entity id="o14342" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 13–20, linear-lanceolate to narrow-ovate, 4–5.5 × 1–2 mm.</text>
      <biological_entity id="o14343" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="20" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="narrow-ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 6–10;</text>
      <biological_entity id="o14344" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, sometimes pink tinged, laminae broadly oblong to oblongelliptic, 3–7 × 1.5–3 mm.</text>
      <biological_entity id="o14345" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="pink tinged" value_original="pink tinged" />
      </biological_entity>
      <biological_entity id="o14346" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s9" to="oblongelliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 30–40;</text>
      <biological_entity id="o14347" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, often purple-tinged, tubes 0.8–1 mm, throats tubular to tubular-funnelform, 1.2–1.4 mm, lobes 0.3–0.4 mm.</text>
      <biological_entity id="o14348" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o14349" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14350" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character char_type="range_value" from="tubular" name="shape" src="d0_s11" to="tubular-funnelform" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14351" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae linear-oblong to oblanceolate, 2–2.8 mm, margins prominently calloused, long-ciliate;</text>
      <biological_entity id="o14352" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s12" to="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14353" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="prominently" name="texture" src="d0_s12" value="calloused" value_original="calloused" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 2 (–3+) barbellulate bristles 1.5–2.5 mm plus crowns of hyaline, laciniate scales.</text>
      <biological_entity id="o14354" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o14355" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s13" to="3" upper_restricted="false" />
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellulate" value_original="barbellulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14357" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o14354" id="r990" name="consist_of" negation="false" src="d0_s13" to="o14355" />
      <relation from="o14356" id="r991" name="part_of" negation="false" src="d0_s13" to="o14357" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 34.</text>
      <biological_entity id="o14356" name="crown" name_original="crowns" src="d0_s13" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity constraint="2n" id="o14358" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In rock crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Fringed rock daisy</other_name>
  <discussion>Perityle ciliata is found only in the mountains of central Arizona in Apache, Coconino, Gila, Mohave, and Yavapai counties. It appears to be most closely related to P. coronopifolia.</discussion>
  
</bio:treatment>