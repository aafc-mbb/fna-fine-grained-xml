<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">261</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">lagophylla</taxon_name>
    <taxon_name authority="Bentham" date="1849" rank="species">dichotoma</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>317. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus lagophylla;species dichotoma</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067038</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60+ cm (strongly self-incompatible);</text>
    </statement>
    <statement id="d0_s1">
      <text>branching ± pseudo-dichotomous, distal stems eglandular or sparsely stipitate-glandular.</text>
      <biological_entity id="o8236" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8237" name="whole-organism" name_original="" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="pseudo-dichotomous" value_original="pseudo-dichotomous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8238" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades green, usually eglandular (distal eglandular or stipitate-glandular, glands mostly purple, some yellow).</text>
      <biological_entity id="o8239" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8240" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o8241" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o8242" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o8241" id="r578" name="in" negation="false" src="d0_s3" to="o8242" />
    </statement>
    <statement id="d0_s4">
      <text>Calyculi 0 or of 2–3 bractlets.</text>
      <biological_entity id="o8243" name="calyculus" name_original="calyculi" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 2" is_modifier="false" name="quantity" src="d0_s4" to="3 bractlets" />
      </biological_entity>
      <biological_entity id="o8244" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <relation from="o8243" id="r579" name="consist_of" negation="false" src="d0_s4" to="o8244" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric to obovoid.</text>
      <biological_entity id="o8245" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="less hemispheric" name="shape" src="d0_s5" to="obovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 4–6.5 mm, ± hirtellous to scabrellous on angles, hairs ± antrorsely curved, 0.1–0.6 mm.</text>
      <biological_entity id="o8246" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="hirtellous" value_original="hirtellous" />
        <character constraint="on angles" constraintid="o8247" is_modifier="false" name="relief" src="d0_s6" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
      <biological_entity id="o8247" name="angle" name_original="angles" src="d0_s6" type="structure" />
      <biological_entity id="o8248" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less antrorsely" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray laminae 7–13 mm.</text>
      <biological_entity constraint="ray" id="o8249" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae dull (striate).</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 14.</text>
      <biological_entity id="o8250" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8251" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, openings in woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="openings" constraint="in woodlands" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Lagophylla dichotoma occurs in the western Sierra Nevada foothills and adjacent eastern San Joaquin Valley and in the northern South Inner Coast Ranges (where plants are notably stipitate-glandular, unlike most Sierran and San Joaquin Valley collections).</discussion>
  
</bio:treatment>