<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">454</other_info_on_meta>
    <other_info_on_meta type="treatment_page">455</other_info_on_meta>
    <other_info_on_meta type="illustration_page">452</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. de Candolle" date="1838" rank="genus">psilostrophe</taxon_name>
    <taxon_name authority="(Nuttall) Greene" date="1891" rank="species">tagetina</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 176. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus psilostrophe;species tagetina</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067412</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Riddellia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">tagetina</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 371. 1841 (as tagetinae)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Riddellia;species tagetina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Psilostrophe</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tagetina</taxon_name>
    <taxon_name authority="(Rydberg) Heiser" date="unknown" rank="variety">grandiflora</taxon_name>
    <taxon_hierarchy>genus Psilostrophe;species tagetina;variety grandiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Psilostrophe</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tagetina</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="variety">lanata</taxon_name>
    <taxon_hierarchy>genus Psilostrophe;species tagetina;variety lanata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials (rarely flowering first-year), 10–30 (–60+) cm.</text>
      <biological_entity id="o15913" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems arachno-villous (gray to gray-green).</text>
      <biological_entity id="o15915" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="arachno-villous" value_original="arachno-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads in ± loose to crowded, corymbiform arrays.</text>
      <biological_entity id="o15916" name="head" name_original="heads" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Peduncles (3–) 12–20 (–40) mm.</text>
      <biological_entity id="o15917" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 4–8 mm.</text>
      <biological_entity id="o15918" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Rays 3–4 (–6);</text>
      <biological_entity id="o15919" name="ray" name_original="rays" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminae (3–) 7–14+ mm, spreading in fruit.</text>
      <biological_entity id="o15920" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="14" to_unit="mm" upper_restricted="false" />
        <character constraint="in fruit" constraintid="o15921" is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o15921" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 6–9 (–12).</text>
      <biological_entity id="o15922" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="12" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae usually glabrous, sometimes hirtellous and/or glanddotted;</text>
      <biological_entity id="o15923" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of (4–) 6–8 elliptic or lanceolate to lance-subulate scales 2–3+ mm.</text>
      <biological_entity id="o15925" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s9" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s9" to="8" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s9" to="lance-subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <relation from="o15924" id="r1103" name="consist_of" negation="false" src="d0_s9" to="o15925" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 32.</text>
      <biological_entity id="o15924" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o15926" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Sep(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub, grasslands, limestone soils, saline flats, sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="limestone soils" />
        <character name="habitat" value="saline flats" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(300–)600–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex., Utah; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>See comments under 6. Psilostrophe villosa.</discussion>
  
</bio:treatment>