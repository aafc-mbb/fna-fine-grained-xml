<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">181</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">galinsoginae</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavón" date="1794" rank="genus">galinsoga</taxon_name>
    <taxon_name authority="Cavanilles" date="1795" rank="species">parviflora</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">parviflora</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe galinsoginae;genus galinsoga;species parviflora;variety parviflora</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250068414</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60 cm.</text>
      <biological_entity id="o15701" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades lanceolate to broadly ovate, 20–110 × 15–70 mm, margins serrulate to serrate.</text>
      <biological_entity id="o15702" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="broadly ovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s1" to="110" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s1" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Arrays of heads loose.</text>
      <biological_entity id="o15703" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="serrulate" name="architecture_or_shape" src="d0_s1" to="serrate" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o15704" name="head" name_original="heads" src="d0_s2" type="structure" />
      <relation from="o15703" id="r1086" name="part_of" negation="false" src="d0_s2" to="o15704" />
    </statement>
    <statement id="d0_s3">
      <text>Axillary peduncles longer than subtending bractlets.</text>
      <biological_entity constraint="subtending" id="o15706" name="bractlet" name_original="bractlets" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 16.</text>
      <biological_entity constraint="axillary" id="o15705" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character constraint="than subtending bractlets" constraintid="o15706" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15707" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed soils, fields, orchards, gardens, lawns, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed soils" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="orchards" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Man., N.B., Ont., Que.; Ariz., Calif., Colo., Conn., D.C., Ill., Ind., Iowa, Kans., Ky., Md., Mass., Mich., Minn., Mo., Nebr., N.J., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., Tex., Vt., Va., W.Va., Wis.; Mexico; West Indies; Central America; South America; introduced in Europe; Asia; Africa; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>