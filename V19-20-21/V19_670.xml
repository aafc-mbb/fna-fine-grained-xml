<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="mention_page">408</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="treatment_page">411</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">antennaria</taxon_name>
    <taxon_name authority="A. E. Porsild" date="1945" rank="species">densifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Natl. Mus. Canada</publication_title>
      <place_in_publication>101: 26. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus antennaria;species densifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066071</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="A. E. Porsild" date="unknown" rank="species">ellyae</taxon_name>
    <taxon_hierarchy>genus Antennaria;species ellyae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Dioecious.</text>
      <biological_entity id="o1582" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Plants 3.5–16 cm.</text>
      <biological_entity id="o1583" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s1" to="16" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stolons 1–2 cm.</text>
      <biological_entity id="o1584" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 1-nerved, spatulate to cuneate, 3–7 × 2–5 mm, tips mucronate, faces gray-tomentose.</text>
      <biological_entity constraint="basal" id="o1585" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="cuneate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1586" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o1587" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves linear, 2–13 mm, distal flagged.</text>
      <biological_entity constraint="cauline" id="o1588" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1589" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="flagged" value_original="flagged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 2–5 in corymbiform arrays.</text>
      <biological_entity id="o1590" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o1591" from="2" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o1591" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres: staminate 3–6.5 mm;</text>
      <biological_entity id="o1592" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 4.5–7.5 mm.</text>
      <biological_entity id="o1593" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pistillate involucres 4.5–7.5 mm.</text>
      <biological_entity id="o1594" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries distally light-brown, dark-brown, or black.</text>
      <biological_entity id="o1595" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Corollas: staminate 2–3.5 mm;</text>
      <biological_entity id="o1596" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate 2.5–4.5 mm.</text>
      <biological_entity id="o1597" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 0.8–1.5 mm, glabrous;</text>
      <biological_entity id="o1598" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: staminate 2.5–3.5 mm;</text>
      <biological_entity id="o1599" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistillate 2.5–3.5 mm. 2n = 28.</text>
      <biological_entity id="o1600" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1601" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine-alpine limestone talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine-alpine limestone talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.W.T., Yukon; Alaska, Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <other_name type="common_name">Denseleaf pussytoes</other_name>
  <discussion>Antennaria densifolia is found on limestone talus below treeline in the MacKenzie, Richardson, and Ogilvie mountains of the District of MacKenzie and Yukon Territory and in Granite County, Montana (R. J. Bayer 1989c). It differs from A. aromatica in being non-glandular and in other characters. Herbarium specimens (in DAO) from British Columbia that morphologically appear to be a strictly gynoecious form of A. densifolia may be apomicts related to A. alpina that are derived from A. densifolia, a sexual progenitor of the complex.</discussion>
  
</bio:treatment>