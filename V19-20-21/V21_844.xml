<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="treatment_page">339</other_info_on_meta>
    <other_info_on_meta type="illustration_page">339</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="(Nuttall) R. Chan" date="2002" rank="section">amphiachaenia</taxon_name>
    <taxon_name authority="de Candolle ex Lindley" date="1835" rank="species">californica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">californica</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section amphiachaenia;species californica;subspecies californica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068544</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o6454" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots not fleshy, not clustered.</text>
      <biological_entity id="o6455" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually erect, sometimes decumbent (especially in coastal forms), usually branched distally, sometimes proximally, ± hairy, usually more so distally.</text>
      <biological_entity id="o6456" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes proximally; proximally; more or less" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o6457" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6458" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to oblanceolate, 8–40 (–70) × 1–6 mm, ± fleshy in coastal forms, margins entire or with 3–5+ teeth (coastal forms), faces ± hairy.</text>
      <biological_entity id="o6459" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
        <character constraint="in forms" constraintid="o6460" is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o6460" name="form" name_original="forms" src="d0_s4" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s4" value="coastal" value_original="coastal" />
      </biological_entity>
      <biological_entity id="o6461" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with 3-5+ teeth" value_original="with 3-5+ teeth" />
      </biological_entity>
      <biological_entity id="o6462" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6463" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o6461" id="r472" name="with" negation="false" src="d0_s4" to="o6462" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate or hemispheric, 5–10 mm.</text>
      <biological_entity id="o6464" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries persistent or falling with cypselae, 4–13 in 1 series, ovatelanceolate to oblong.</text>
      <biological_entity id="o6465" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character constraint="with cypselae" constraintid="o6466" is_modifier="false" name="life_cycle" src="d0_s6" value="falling" value_original="falling" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" notes="" src="d0_s6" to="oblong" />
      </biological_entity>
      <biological_entity id="o6466" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o6467" from="4" name="quantity" src="d0_s6" to="13" />
      </biological_entity>
      <biological_entity id="o6467" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 6–13;</text>
      <biological_entity id="o6468" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae oblong, 5–10 mm.</text>
      <biological_entity id="o6469" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Anther appendages deltate.</text>
      <biological_entity constraint="anther" id="o6470" name="appendage" name_original="appendages" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae black to gray, ± clavate, to 3 mm, glabrous or hairy;</text>
      <biological_entity id="o6471" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="black" name="coloration" src="d0_s10" to="gray" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi usually of 1–7 translucent (rarely opaque), brown (rarely white), linear to subulate, aristate scales (rarely variable or 0 within heads).</text>
      <biological_entity id="o6473" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="7" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s11" value="translucent" value_original="translucent" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s11" to="subulate" />
        <character is_modifier="true" name="shape" src="d0_s11" value="aristate" value_original="aristate" />
      </biological_entity>
      <relation from="o6472" id="r473" name="consist_of" negation="false" src="d0_s11" to="o6473" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 16, 32, 48.</text>
      <biological_entity id="o6472" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o6474" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
        <character name="quantity" src="d0_s12" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly open sites (virtually every habitat but desert)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" modifier="mostly" />
        <character name="habitat" value="every habitat" modifier="virtually" />
        <character name="habitat" value="desert" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">California goldfields</other_name>
  <discussion>Subspecies californica sometimes occurs with other Lasthenia taxa. R. Ornduff (1993) included L. gracilis within his circumscription of subsp. californica. Coastal forms of subsp. californica tend to have shorter, wider, toothed, fleshy leaves and larger heads; they can be distinguished from L. gracilis by their translucent, brown, linear to subulate, aristate pappus scales and more northern distribution. Sympatric epappose plants of subsp. californica and L. gracilis are not easily distinguished morphologically; molecular markers (R. Chan et al. 2001) show them to be distinct taxa.</discussion>
  
</bio:treatment>