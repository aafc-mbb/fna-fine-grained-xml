<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="treatment_page">294</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1882" rank="genus">HOLOZONIA</taxon_name>
    <place_of_publication>
      <publication_title>Bull Torrey Bot. Club</publication_title>
      <place_in_publication>9: 122. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus HOLOZONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek holos, whole or entire, and zona, belt or girdle; alluding to each phyllary fully (or mostly) investing a ray ovary (cypsela), in contrast to the half-invested cypselae of Hemizonia</other_info_on_name>
    <other_info_on_name type="fna_id">115645</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–150 cm (rhizomatous).</text>
      <biological_entity id="o4872" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (aerial) ± erect.</text>
      <biological_entity id="o4873" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o4874" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4875" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite (basally connate), distal alternate;</text>
      <biological_entity constraint="proximal" id="o4876" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity constraint="distal" id="o4877" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate to linear, margins entire, faces hirsute and (distal leaves) glandular-hirtellous (glands cupshaped).</text>
      <biological_entity id="o4878" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o4879" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4880" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-hirtellous" value_original="glandular-hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in loose, corymbiform arrays (peduncles filiform).</text>
      <biological_entity id="o4881" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o4882" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o4881" id="r371" name="in" negation="false" src="d0_s6" to="o4882" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and/or spines 0.</text>
      <biological_entity constraint="peduncular" id="o4883" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o4884" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o4885" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o4886" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± obconic or turbinate, 2–4+ mm diam.</text>
      <biological_entity id="o4887" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex, glabrous or setulose, paleate (paleae falling, in 1 series between rays and discs, connate).</text>
      <biological_entity id="o4888" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="setulose" value_original="setulose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 4–10 in 1 series (each mostly or wholly enveloping a ray ovary, ± lance-linear, herbaceous, abaxially hirsute, sometimes glandular-hirtellous, glands cupshaped).</text>
      <biological_entity id="o4889" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4890" from="4" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
      <biological_entity id="o4890" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 4–10, pistillate, fertile;</text>
      <biological_entity id="o4891" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="10" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas whitish (abaxially purplish-veined).</text>
      <biological_entity id="o4892" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 9–28, functionally staminate;</text>
      <biological_entity id="o4893" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s13" to="28" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas white (pubescent), tubes shorter than funnelform throats, lobes 5, deltate (anthers ± dark purple; styles glabrous proximal to branches).</text>
      <biological_entity id="o4894" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o4895" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o4896" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4896" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o4897" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (black) obcompressed, ± clavate (basal attachments centered, apices beakless, areolae broadly cupulate, faces glabrous);</text>
      <biological_entity id="o4898" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi (rays) 0 or coroniform (0.1–0.3 mm), or (discs) 0 or (readily falling) of 1–5 subulate scales.</text>
      <biological_entity id="o4900" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s16" to="5" />
        <character is_modifier="true" name="shape" src="d0_s16" value="subulate" value_original="subulate" />
      </biological_entity>
      <relation from="o4899" id="r372" name="consist_of" negation="false" src="d0_s16" to="o4900" />
    </statement>
    <statement id="d0_s17">
      <text>x = 14.</text>
      <biological_entity id="o4899" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 1" is_modifier="false" name="quantity" src="d0_s16" to="5 subulate scales" />
      </biological_entity>
      <biological_entity constraint="x" id="o4901" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>343.</number>
  <discussion>Species 1.</discussion>
  <discussion>Holozonia has been treated as congeneric with Hemizonia and with Lagophylla. Like Lagophylla, Holozonia has functionally staminate disc florets, cup-shaped glands, and obcompressed cypselae, each completely or mostly invested by a phyllary. Additional morphologic considerations and biosystematic studies led W. C. Thompson (1983) to reject the hypothesis of a close relationship between Holozonia and Lagophylla. Molecular phylogenetic data are in keeping with a closer relationship of Holozonia to other continental tarweeds with white corollas and x = 14 (i.e., Blepharizonia and Hemizonia), than to Lagophylla (S. Carlquist et al. 2003).</discussion>
  <references>
    <reference>Thompson, W. C. 1983. A Biosystematic Study of Lagophylla (Compositae: Heliantheae) and Related Taxa. Ph.D. dissertation. University of California, Davis.</reference>
  </references>
  
</bio:treatment>