<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David M. Spooner</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Persoon" date="1807" rank="genus">SIMSIA</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>2: 478. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus SIMSIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For John Sims, 1749–1831, British physician and botanist</other_info_on_name>
    <other_info_on_name type="fna_id">130390</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, or subshrubs [shrubs], 20–400 cm.</text>
      <biological_entity id="o25976" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="400" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o25978" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending [decumbent], sparingly to freely branched.</text>
      <biological_entity id="o25979" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparingly to freely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite (proximal) or alternate [whorled];</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petioles often ± winged, often with expanded bases, those bases sometimes fused to form nodal “discs”) [sessile];</text>
      <biological_entity id="o25980" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-nerved from bases, mostly deltate to ovate [linear], sometimes 3- [5-] lobed [pinnatifid], bases cordate to cuneate, ultimate margins entire or toothed, faces hirsute, hispid, pilose, puberulent, scabrous, or scabro-hispid [sericeous], often glanddotted or ± stipitate-glandular to glandular-puberulent.</text>
      <biological_entity id="o25981" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="from bases" constraintid="o25982" is_modifier="false" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="mostly deltate" name="shape" notes="" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="3" modifier="sometimes" name="quantity" src="d0_s5" to="45" unit="3-[5-]" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o25982" name="base" name_original="bases" src="d0_s5" type="structure" />
      <biological_entity id="o25983" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o25984" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o25985" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabro-hispid" value_original="scabro-hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabro-hispid" value_original="scabro-hispid" />
        <character char_type="range_value" from="less stipitate-glandular" modifier="often" name="pubescence" src="d0_s5" to="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate [discoid], borne singly or in 2s or 3s, or in tight to loose, corymbiform [paniculiform] arrays.</text>
      <biological_entity id="o25986" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in 2s or 3s , or in tight" />
      </biological_entity>
      <biological_entity id="o25987" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o25986" id="r1766" name="to" negation="false" src="d0_s6" to="o25987" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate [ovoid-campanulate to urceolate], 5–16 [–22] mm diam.</text>
      <biological_entity id="o25988" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="22" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, [11–] 13–43 [–66] in 2–4 series (tightly appressed to broadly reflexed, unequal to subequal).</text>
      <biological_entity id="o25989" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="11" name="atypical_quantity" src="d0_s8" to="13" to_inclusive="false" />
        <character char_type="range_value" from="43" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="66" />
        <character char_type="range_value" constraint="in series" constraintid="o25990" from="13" name="quantity" src="d0_s8" to="43" />
      </biological_entity>
      <biological_entity id="o25990" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles low-convex, paleate (paleae conduplicate, ± enclosing cypselae).</text>
      <biological_entity id="o25991" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="low-convex" value_original="low-convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets [0–] 5–21 [–45], styliferous and sterile;</text>
      <biological_entity id="o25992" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="21" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="45" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="21" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="styliferous" value_original="styliferous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas orange-yellow [lemon-yellow, pink, purple, or white].</text>
      <biological_entity id="o25993" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange-yellow" value_original="orange-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets [12–] 13–154 [–172], bisexual, fertile;</text>
      <biological_entity id="o25994" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s12" to="13" to_inclusive="false" />
        <character char_type="range_value" from="154" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="172" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s12" to="154" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas concolorous with rays (usually turning purple apically), tubes (often glandular-hairy) shorter than throats, lobes 5, ± triangular (anthers black, yellow, or yellow proximally and bronze or purple distally; style-branches relatively slender, apices sometimes attenuate).</text>
      <biological_entity id="o25995" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character constraint="with rays" constraintid="o25996" is_modifier="false" name="coloration" src="d0_s13" value="concolorous" value_original="concolorous" />
      </biological_entity>
      <biological_entity id="o25996" name="ray" name_original="rays" src="d0_s13" type="structure" />
      <biological_entity id="o25997" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than throats" constraintid="o25998" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25998" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o25999" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae flattened, thin-margined [thickened, biconvex] (shoulders minute to conspicuous, faces glabrous or hairy);</text>
      <biological_entity id="o26000" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="thin-margined" value_original="thin-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, or fragile or readily falling, of 2 ± subulate scales [plus 4–12 shorter scales].</text>
      <biological_entity id="o26002" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
      </biological_entity>
      <relation from="o26001" id="r1767" name="consist_of" negation="false" src="d0_s15" to="o26002" />
    </statement>
    <statement id="d0_s16">
      <text>x = 17.</text>
      <biological_entity id="o26001" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s15" value="fragile" value_original="fragile" />
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o26003" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, West Indies (Jamaica), Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Jamaica)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>298.</number>
  <other_name type="common_name">Bush sunflower</other_name>
  <discussion>Species 20 (2 in the flora).</discussion>
  <references>
    <reference>Spooner, D. M. 1990. Systematics of Simsia (Compositae–Heliantheae). Syst. Bot. Monogr. 30: 1–90.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials or subshrubs (roots fusiform-thickened); ray florets 8–21, corollas light orange-yellow (abaxial faces often brown- or purple-lined, or wholly brown or purple); disc florets (26–)90–154; anthers usually yellow, rarely black</description>
      <determination>1 Simsia calva</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals (rarely persisting, taprooted or fibrous rooted); ray florets 5–10, corollas orange-yellow; disc florets 13–27; anthers yellow proximally, usually purple to bronze distally</description>
      <determination>2 Simsia lagasceiformis</determination>
    </key_statement>
  </key>
</bio:treatment>