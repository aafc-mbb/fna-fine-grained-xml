<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1822" rank="genus">LAËNNECIA</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 25: 91. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus LAËNNECIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For René-Théophile-Hyacinthe Laënnec, 1781–1826, French physician, inventor of the stethoscope</other_info_on_name>
    <other_info_on_name type="fna_id">117456</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 10–120 (–350+) cm (usually taprooted, sometimes with ± woody caudices).</text>
      <biological_entity id="o29543" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="350" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="350" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, simple or branched from bases or throughout.</text>
      <biological_entity id="o29545" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from bases" constraintid="o29546" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o29546" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline (mostly cauline at flowering);</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or petiolate;</text>
      <biological_entity id="o29547" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (usually 1-nerved) elliptic, linear, oblanceolate, oblong, obovate, ovate, spatulate, sometimes 1–2-pinnately lobed, ultimate margins toothed or entire, faces glabrate, hirtellous, tomentose, or villous, usually stipitate and/or sessile-glandular as well.</text>
      <biological_entity id="o29548" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="sometimes 1-2-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o29549" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o29550" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="usually; well" name="architecture" src="d0_s5" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="well" name="architecture" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads obscurely radiate or disciform, borne in ± corymbiform, paniculiform, or racemiform arrays.</text>
      <biological_entity id="o29551" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o29552" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o29551" id="r2729" name="borne in" negation="false" src="d0_s6" to="o29552" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± turbinate, 2–7 [–10] mm diam.</text>
      <biological_entity id="o29553" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 20–40+ in 2–4 series, appressed (usually reflexed in fruit), 1-nerved (nerves not orange or brown), lanceolate to linear, ± unequal to subequal, ± herbaceous medially, margins ± membranous, glabrate, hirtellous, tomentose, or villous, usually stipitate and/or sessile-glandular as well.</text>
      <biological_entity id="o29554" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o29555" from="20" name="quantity" src="d0_s8" to="40" upper_restricted="false" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear" />
        <character char_type="range_value" from="less unequal" name="size" src="d0_s8" to="subequal" />
        <character is_modifier="false" modifier="more or less; medially" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o29555" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o29556" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="usually; well" name="architecture" src="d0_s8" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="well" name="architecture" src="d0_s8" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles ± flat, pitted or smooth, epaleate.</text>
      <biological_entity id="o29557" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peripheral (“ray”) florets 20–100 (–200), pistillate, fertile;</text>
      <biological_entity constraint="peripheral" id="o29558" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="200" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="100" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellowish (usually distally truncate or 2–5-toothed, sometimes with laminae 0.5–1+ mm).</text>
      <biological_entity id="o29559" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 2–20 [–30+], bisexual, fertile;</text>
      <biological_entity id="o29560" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="30" upper_restricted="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="20" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellowish, narrowly funnelform, tubes shorter than throats, lobes 5, erect or spreading, deltate (lengths 2–2.5+ times widths);</text>
      <biological_entity id="o29561" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o29562" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than throats" constraintid="o29563" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o29563" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o29564" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character modifier="narrowly" name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages deltate.</text>
      <biological_entity constraint="style-branch" id="o29565" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae oblong to elliptic or obovate, compressed, margins thickened, riblike, faces glabrous, ± strigillose (hairs 0.05–0.1 mm), or densely sericeous (hairs 0.3–0.5 mm), often also stipitate and/or sessile-glandular;</text>
      <biological_entity id="o29566" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="elliptic or obovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o29567" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="riblike" value_original="riblike" />
      </biological_entity>
      <biological_entity id="o29568" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s15" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, ± fragile, or readily falling, of 9–30+, white to tawny, ± equal, barbellulate, apically attenuate bristles in 1 series, or 30–40+ bristles in 2 series (outer usually shorter).</text>
      <biological_entity id="o29569" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s16" value="fragile" value_original="fragile" />
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s16" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o29570" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s16" to="30" upper_restricted="false" />
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s16" to="tawny" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s16" value="equal" value_original="equal" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellulate" value_original="barbellulate" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o29571" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s16" to="40" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o29573" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <relation from="o29569" id="r2730" name="consist_of" negation="false" src="d0_s16" to="o29570" />
      <relation from="o29570" id="r2731" name="in" negation="false" src="d0_s16" to="o29571" />
      <relation from="o29572" id="r2732" name="in" negation="false" src="d0_s16" to="o29573" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o29572" name="bristle" name_original="bristles" src="d0_s16" type="structure" />
      <biological_entity constraint="x" id="o29574" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly subtropical, tropical, and warm-temperate New World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly subtropical" establishment_means="native" />
        <character name="distribution" value="tropical" establishment_means="native" />
        <character name="distribution" value="and warm-temperate New World" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>142.</number>
  <discussion>Species ca. 18 (6 in the flora).</discussion>
  <discussion>G. L. Nesom (1990) argued for segregating some species from Conyza as members of Laënnecia. Although there are exceptions to each of the trends noted by Nesom, the rationale he presented is compelling. R. D. Noyes and L. H. Rieseberg (1999) demonstrated molecular differences that are consistent with treating laënnecias and conyzas in separate genera; at the same time, they showed Conyza to be “nested” within Erigeron.</discussion>
  <references>
    <reference>Nesom, G. L. 1990. Taxonomy of the genus Laënnecia (Asteraceae: Astereae). Phytologia 68: 205–228.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades densely to loosely tomentose (at least abaxially, sometimes glandular beneath the tomentum); pappi of 30–40+ bristles in 2 series</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades usually hirtellous to villous and usually glandular, sometimes glabrate (usually sparsely and finely tomentose and with coarse, erect, multicellular hairs and stipitate- or sessile-glandular in L. schiedeana); pappi of 9–30 bristles in 1 series</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pistillate corollas: laminae 0.5–1 mm; cypselae pale tan to brownish, 2–3 mm</description>
      <determination>1 Laënnecia eriophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pistillate corollas: laminae 0; cypselae purplish, 1.3–1.5 mm</description>
      <determination>2 Laënnecia filaginoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades mostly 1–2-pinnately lobed (lobes ovate to ± linear); pappi 2–3 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades coarsely toothed or entire; pappi 3.5–4 mm</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Bases of leaves obscurely, if at all, clasping; heads in racemiform or paniculiformarrays</description>
      <determination>3 Laënnecia sophiifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Bases of all or at least distal leaves clasping; heads in ± corymbiform arrays</description>
      <determination>4 Laënnecia turnerorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades entire or distally toothed, faces usually sparsely and finely tomentose and with coarse, erect, multicellular hairs and stipitate- or sessile-glandular;pistillate corolla laminae 0.5–1+ mm</description>
      <determination>5 Laënnecia schiedeana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades coarsely toothed from near their bases or entire, faces usually both hirtellous tovillous and ± glandular-viscid, not tomentose; pistillate corollalaminae 0</description>
      <determination>6 Laënnecia coulteri</determination>
    </key_statement>
  </key>
</bio:treatment>