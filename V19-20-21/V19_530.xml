<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. Don" date="1832" rank="genus">microseris</taxon_name>
    <taxon_name authority="(Greene) J. T. Howell" date="1948" rank="species">paludosa</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>5: 108. 1948</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus microseris;species paludosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067188</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scorzonella</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">paludosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>2: 52. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Scorzonella;species paludosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–70 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o9596" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems branched proximally, leafy proximally.</text>
      <biological_entity id="o9597" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petioles broadly winged, clasping);</text>
      <biological_entity id="o9598" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear to oblanceolate, 6–35 cm, margins entire, dentate, or pinnately lobed, apices acuminate.</text>
      <biological_entity id="o9599" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="35" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9600" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o9601" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles erect or arcuate-ascending (15–50 cm), ebracteate.</text>
      <biological_entity id="o9602" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="arcuate-ascending" value_original="arcuate-ascending" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ovoid in fruit, 10–20 mm.</text>
      <biological_entity id="o9603" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o9604" is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9604" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries: not spotted, abaxial faces usually scurfy-puberulent, usually black-villous;</text>
      <biological_entity id="o9605" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s8" value="spotted" value_original="spotted" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9606" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="scurfy-puberulent" value_original="scurfy-puberulent" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="black-villous" value_original="black-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer linear to broadly or narrowly ovate-deltate, apices erect or recurved, acuminate;</text>
      <biological_entity id="o9607" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o9608" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="broadly or narrowly ovate-deltate" />
      </biological_entity>
      <biological_entity id="o9609" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner lanceolate, apices erect, acute to acuminate.</text>
      <biological_entity id="o9610" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o9611" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o9612" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Florets 25–70;</text>
      <biological_entity id="o9613" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s11" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow-orange, surpassing phyllaries by 5+ mm.</text>
      <biological_entity id="o9614" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow-orange" value_original="yellow-orange" />
      </biological_entity>
      <biological_entity id="o9615" name="phyllary" name_original="phyllaries" src="d0_s12" type="structure" />
      <relation from="o9614" id="r887" name="surpassing" negation="false" src="d0_s12" to="o9615" />
    </statement>
    <statement id="d0_s13">
      <text>Cypselae columnar, 4–7 mm;</text>
      <biological_entity id="o9616" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 5–10, dull yellowish-brown, lanceolate, glabrous, aristate scales 2–4 mm, aristae barbellate.</text>
      <biological_entity id="o9617" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o9618" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s14" to="10" />
        <character is_modifier="true" name="reflectance" src="d0_s14" value="dull" value_original="dull" />
        <character is_modifier="true" name="coloration" src="d0_s14" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="true" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" name="shape" src="d0_s14" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o9617" id="r888" name="consist_of" negation="false" src="d0_s14" to="o9618" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o9619" name="arista" name_original="aristae" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9620" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, clay, and loam soils, grasslands, brushlands, oak woodlands. and closed-cone pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clay" modifier="sandy" />
        <character name="habitat" value="loam soils" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="brushlands" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="closed-cone pine forests" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Marsh silverpuffs</other_name>
  <discussion>Microseris paludosa in the central coastal region (D. P. Tibor 2001). It differs from M. laciniata subsp. leptosepala in its longer, brownish pappus scales and more southern coastal distribution. It is unusual among the perennial taxa of Microseris in its self-compatibility and ready self-fertilization in culture.</discussion>
  
</bio:treatment>