<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">37</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">melampodiinae</taxon_name>
    <taxon_name authority="Schrank" date="1820" rank="genus">acanthospermum</taxon_name>
    <taxon_name authority="(Swartz) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">humile</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 522. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe melampodiinae;genus acanthospermum;species humile</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250066002</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melampodium</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">humile</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>114. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Melampodium;species humile;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–20 (–30+) cm.</text>
      <biological_entity id="o6031" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect.</text>
      <biological_entity id="o6032" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades oval to lyrate, 10–30 (–45) mm, faces ± pilosulous to sparsely sericeous, glanddotted.</text>
      <biological_entity id="o6033" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s2" to="lyrate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s2" to="45" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="distance" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6034" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="less pilosulous" name="pubescence" src="d0_s2" to="sparsely sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruits strongly compressed, ± cuneate, 2–3 (–4) mm, usually 3-ribbed, terminal spines 2, divergent, 2–3 (–4) mm, often 1 ± uncinate, prickles ± uncinate, mostly along 2 ribs and around apices.</text>
      <biological_entity id="o6035" name="fruit" name_original="fruits" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="3-ribbed" value_original="3-ribbed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o6036" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character modifier="often" name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="uncinate" value_original="uncinate" />
      </biological_entity>
      <biological_entity id="o6037" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="uncinate" value_original="uncinate" />
      </biological_entity>
      <biological_entity id="o6038" name="rib" name_original="ribs" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6039" name="apex" name_original="apices" src="d0_s3" type="structure" />
      <relation from="o6037" id="r447" modifier="mostly" name="along" negation="false" src="d0_s3" to="o6038" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering all year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed, often sandy sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy sites" modifier="disturbed often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Fla., S.C.; West Indies; also introduced in Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="also  in Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>