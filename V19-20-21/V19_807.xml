<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="treatment_page">481</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="(Cassini ex Dumortier) Anderberg" date="1989" rank="tribe">plucheeae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">pluchea</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">camphorata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 452. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe plucheeae;genus pluchea;species camphorata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417010</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">camphoratus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 864. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species camphoratus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 50–200+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted.</text>
      <biological_entity id="o8771" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems minutely puberulent and sessile-glandular, usually also closely arachnose (hairs appressed).</text>
      <biological_entity id="o8773" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="usually; closely" name="pubescence" src="d0_s2" value="arachnose" value_original="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves petiolate (petioles 10–20 mm);</text>
      <biological_entity id="o8774" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades elliptic to oblongelliptic, 6–15 × 3–7 cm, margins dentate-serrate or entire, faces glandular-puberulent or puberulent and sessile-glandular.</text>
      <biological_entity id="o8775" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="oblongelliptic" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8776" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate-serrate" value_original="dentate-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8777" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in paniculiform arrays (of rounded-convex, corymbiform clusters terminating branches from distal nodes, arrays usually resulting from axillary, strongly ascending, bracteate branches, the central axis longest and first to flower and, rarely, the only component of an array).</text>
      <biological_entity id="o8778" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o8779" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o8778" id="r812" name="in" negation="false" src="d0_s5" to="o8779" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate, 4–6 × 3–4 mm.</text>
      <biological_entity id="o8780" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries usually cream, sometimes purplish, minutely sessile-glandular (the outer also sparsely puberulent), sometimes glabrate.</text>
      <biological_entity id="o8781" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas rose purplish.</text>
      <biological_entity id="o8782" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose purplish" value_original="rose purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi persistent, bristles distinct.</text>
      <biological_entity id="o8783" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o8784" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct (year-round in south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="range_value" modifier="in south" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Flatwoods, bottomland channels, other wet or moist freshwater habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="bottomland channels" />
        <character name="habitat" value="other wet" />
        <character name="habitat" value="moist freshwater habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Miss., Mo., N.J., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Plowman’s-wort</other_name>
  <discussion>Pluchea camphorata is similar to P. odorata and rarely may hybridize with it. In P. camphorata, the phyllaries of the inner 2–3 series are thin and nearly translucent, lanceolate, and more than twice as long as deltate-ovate phyllaries of the outer series. The inner may be glandular but they are otherwise glabrous, prominently different in vestiture from the outer. The phyllaries of P. odorata are more strongly graduated and the inner are glandular and also clearly puberulent as well.</discussion>
  
</bio:treatment>