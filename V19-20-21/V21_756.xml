<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="treatment_page">311</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">hymenopappinae</taxon_name>
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="A. Gray" date="1883" rank="species">mexicanus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 29. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe hymenopappinae;genus hymenopappus;species mexicanus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066977</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–90 cm.</text>
      <biological_entity id="o21376" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal simple or 1-pinnate, 6–20 cm, lobes 5–15+ × 1–9 mm;</text>
      <biological_entity id="o21377" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o21378" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21379" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 0 or 3–6.</text>
      <biological_entity id="o21380" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o21381" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1–8 (–20+) per stem.</text>
      <biological_entity id="o21382" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="20" upper_restricted="false" />
        <character char_type="range_value" constraint="per stem" constraintid="o21383" from="1" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
      <biological_entity id="o21383" name="stem" name_original="stem" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–10 cm.</text>
      <biological_entity id="o21384" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries yellowish to whitish or red-tinged, 6–9 × 2–3 mm.</text>
      <biological_entity id="o21385" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s5" to="whitish or red-tinged" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 0.</text>
      <biological_entity id="o21386" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 20–40;</text>
      <biological_entity id="o21387" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 3–4.5 mm, tubes 2–2.5 mm, throats campanulate, 1–1.5 mm, lengths 3–4 times lobes.</text>
      <biological_entity id="o21388" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21389" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21390" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character constraint="lobe" constraintid="o21391" is_modifier="false" name="length" src="d0_s8" value="3-4 times lobes" value_original="3-4 times lobes" />
      </biological_entity>
      <biological_entity id="o21391" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 4–6 mm, glabrous or sparsely hirtellous;</text>
      <biological_entity id="o21392" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 12–20 scales 0.1–0.4 mm. 2n = 34.</text>
      <biological_entity id="o21393" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity id="o21394" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s10" to="20" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21395" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
      </biological_entity>
      <relation from="o21393" id="r1474" name="consist_of" negation="false" src="d0_s10" to="o21394" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in pine, spruce, and aspen woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in pine , spruce , and aspen woodlands" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="aspen woodlands" modifier="spruce" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>