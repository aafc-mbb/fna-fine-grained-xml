<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
    <other_info_on_meta type="illustration_page">289</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1885" rank="genus">blepharizonia</taxon_name>
    <taxon_name authority="(Kellogg) Greene" date="1885" rank="species">plumosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 279. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus blepharizonia;species plumosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220001732</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calycadenia</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">plumosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>5: 49. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Calycadenia;species plumosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="(Kellogg) A. Gray" date="unknown" rank="species">plumosa</taxon_name>
    <taxon_hierarchy>genus Hemizonia;species plumosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage gray-green, sparsely, if at all, stipitate-glandular, usually with scattered tack-glands as well.</text>
      <biological_entity id="o2340" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" modifier="at all" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o2341" name="tack-gland" name_original="tack-glands" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o2340" id="r171" modifier="usually; well" name="with" negation="false" src="d0_s0" to="o2341" />
    </statement>
    <statement id="d0_s1">
      <text>Heads usually in spiciform-paniculiform arrays (branches often arched-ascending).</text>
      <biological_entity id="o2342" name="head" name_original="heads" src="d0_s1" type="structure" />
      <biological_entity id="o2343" name="array" name_original="arrays" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="spiciform-paniculiform" value_original="spiciform-paniculiform" />
      </biological_entity>
      <relation from="o2342" id="r172" name="in" negation="false" src="d0_s1" to="o2343" />
    </statement>
    <statement id="d0_s2">
      <text>Involucres usually ± canescent (sometimes also with scattered, longer, glandless hairs), usually with scattered tack-glands as well.</text>
      <biological_entity id="o2344" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity id="o2345" name="tack-gland" name_original="tack-glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o2344" id="r173" modifier="usually; well" name="with" negation="false" src="d0_s2" to="o2345" />
    </statement>
    <statement id="d0_s3">
      <text>Disc pappi 1.5–3 mm. 2n = 28.</text>
      <biological_entity constraint="disc" id="o2346" name="pappus" name_original="pappi" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2347" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes, in grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" constraint="in grasslands" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Blepharizonia plumosa is found in the eastern San Francisco Bay area and northwestern San Joaquin Valley. Northern and southern populations appear to be evolutionarily divergent and may warrant treatment as distinct taxa (B. G. Baldwin et al. 2001).</discussion>
  
</bio:treatment>