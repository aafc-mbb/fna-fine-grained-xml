<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">611</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">hasteola</taxon_name>
    <taxon_name authority="L. C. Anderson" date="1994" rank="species">robertiorum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>19: 212, figs. 1, 2. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus hasteola;species robertiorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066837</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (50–) 60–100 (–150) cm.</text>
      <biological_entity id="o6291" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal 25–54 cm, proximal cauline 25–32 cm, proximal cauline blades deltate (distal appearing sessile, petioles prominently winged).</text>
      <biological_entity id="o6292" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o6293" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="54" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal cauline" id="o6294" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="32" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal cauline" id="o6295" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres cylindric, 8–12.5 mm.</text>
      <biological_entity id="o6296" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s2" to="12.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 7–9.</text>
      <biological_entity id="o6297" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Florets 10–14;</text>
      <biological_entity id="o6298" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s4" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas pale greenish white, 9–10 mm;</text>
      <biological_entity id="o6299" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale greenish" value_original="pale greenish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers 1.5–2 mm.</text>
      <biological_entity id="o6300" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae light golden brown with darker brown veins, 6–9 mm;</text>
      <biological_entity id="o6301" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="light golden" value_original="light golden" />
        <character constraint="with veins" constraintid="o6302" is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6302" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="darker brown" value_original="darker brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi 4–6 mm. 2n = 20.</text>
      <biological_entity id="o6303" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6304" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hydric hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hydric hammocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Hasteola robertiorum is known only from Lake and Levy counties.</discussion>
  <references>
    <reference>Anderson, L. C., E. L. Bridges, and S. L. Orzell. 1995. New data on distribution and morphology for the rare Hasteola robertiorum (Asteraceae). Phytologia 78: 246–248.</reference>
  </references>
  
</bio:treatment>