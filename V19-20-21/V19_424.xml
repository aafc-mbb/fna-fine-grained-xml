<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Bogler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="treatment_page">294</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LEONTODON</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 798. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 349. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus LEONTODON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leon, lion, and odons, tooth, alluding to deeply toothed leaves</other_info_on_name>
    <other_info_on_name type="fna_id">117988</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 10–80 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted, sometimes tuberous, or with short caudices.</text>
      <biological_entity id="o20652" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o20654" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20+, simple and scapiform or sparingly branched, glabrous, tomentulose, or coarsely hirsute.</text>
      <biological_entity id="o20655" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="scapiform" value_original="scapiform" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petioles winged);</text>
      <biological_entity id="o20656" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate, margins entire or dentate or deeply lobed (faces glabrous or hispid, hairs simple or minutely 2–3-fid).</text>
      <biological_entity id="o20657" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o20658" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="dentate or deeply lobed" value_original="dentate or deeply lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly or 2–5 in loose, corymbiform arrays.</text>
      <biological_entity id="o20659" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o20660" from="2" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity id="o20660" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles slightly inflated, naked or minutely bracteate.</text>
      <biological_entity id="o20661" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="naked" value_original="naked" />
        <character is_modifier="false" modifier="minutely" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of 10–20, subulate to lanceolate bractlets in 1–2 series (unequal), glabrous, tomentulose, or hirsute.</text>
      <biological_entity id="o20662" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character char_type="range_value" from="subulate" modifier="of 10-20" name="shape" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o20663" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o20664" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <relation from="o20663" id="r1865" name="in" negation="false" src="d0_s8" to="o20664" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 4–15 mm diam.</text>
      <biological_entity id="o20665" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 16–20 in 2+ series, narrowly lanceolate, subequal, glabrous, tomentulose, or hirsute.</text>
      <biological_entity id="o20666" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o20667" from="16" name="quantity" src="d0_s10" to="20" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o20667" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles convex, pitted, sometimes slightly villous, epaleate.</text>
      <biological_entity id="o20668" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="convex" value_original="convex" />
        <character is_modifier="false" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" modifier="sometimes slightly" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Florets 20–30;</text>
      <biological_entity id="o20669" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow to orange (outer sometimes with reddish or greenish stripes).</text>
      <biological_entity id="o20670" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae light to dark-brown or reddish-brown, fusiform or cylindric, curved, distally narrowed and not beaked, or beaked, ribs 10–14, faces muricate, glabrous;</text>
      <biological_entity id="o20671" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s14" to="dark-brown or reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s14" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o20672" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="14" />
      </biological_entity>
      <biological_entity id="o20673" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of ± distinct, yellowish white, tan, or pale-brown bristles in 1–2 series (all uniformly plumose or outer reduced; pappi of outer cypselae sometimes reduced to crowns of bristlelike scales).</text>
      <biological_entity id="o20674" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="of more or less distinct" name="coloration" src="d0_s15" value="yellowish white" value_original="yellowish white" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
      <biological_entity id="o20676" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="2" />
      </biological_entity>
      <relation from="o20675" id="r1866" name="in" negation="false" src="d0_s15" to="o20676" />
    </statement>
    <statement id="d0_s16">
      <text>x = 4, 6, 7.</text>
      <biological_entity id="o20675" name="bristle" name_original="bristles" src="d0_s15" type="structure" />
      <biological_entity constraint="x" id="o20677" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="4" value_original="4" />
        <character name="quantity" src="d0_s16" value="6" value_original="6" />
        <character name="quantity" src="d0_s16" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, n North Africa, Mediterranean, w Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="n North Africa" establishment_means="introduced" />
        <character name="distribution" value="Mediterranean" establishment_means="introduced" />
        <character name="distribution" value="w Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>52.</number>
  <other_name type="common_name">Hawkbit</other_name>
  <discussion>Species ca. 50 (3 in the flora).</discussion>
  <discussion>Leontodon is recognized by the basal rosettes of pinnatifid leaves, scapiform stems, loosely imbricate phyllaries, yellow corollas, and plumose pappus bristles. Some species are somewhat doubtfully distinguished by an overlapping mixture of vestiture and pappus characters.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads (1–)2–5 in corymbiform arrays; peduncles minutely bracteate proximal to heads; pappi wholly of plumose bristles</description>
      <determination>1 Leontodon autumnalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Head borne singly on scapiform stems; peduncles usually ebracteate proximal to heads; pappi mixed (either outer series different from inner, or pappi of outer cypselae reduced to crowns of bristlelike scales)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappi mixed in all cypselae (outer series of bristlelike scales, inner of plumose bristles); phyllaries densely, coarsely hispid or hirsute</description>
      <determination>2 Leontodon hispidus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pappi of 2 types (on outer cypselae, crowns of bristlelike scales; on inner, of plumose bristles); phyllaries glabrate to coarsely hirsute</description>
      <determination>3 Leontodon saxatilis</determination>
    </key_statement>
  </key>
</bio:treatment>