<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">541</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Schultz-Bipontinus" date="1850" rank="genus">fleischmannia</taxon_name>
    <taxon_name authority="(A. Gray) R. M. King &amp; H. Robinson" date="1974" rank="species">sonorae</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>28: 82. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus fleischmannia;species sonorae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066779</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">sonorae</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 74. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species sonorae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–80 cm.</text>
      <biological_entity id="o23520" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending-erect (not sprawling-scandent).</text>
      <biological_entity id="o23521" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending-erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 0.5–3 cm;</text>
      <biological_entity id="o23522" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23523" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades deltate-ovate, 1.5–5 × 0.6–2 cm, bases broadly cuneate to subtruncate, margins irregularly crenate-serrate, apices acute to acuminate.</text>
      <biological_entity id="o23524" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23525" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate-ovate" value_original="deltate-ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23526" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate to subtruncate" value_original="cuneate to subtruncate" />
      </biological_entity>
      <biological_entity id="o23527" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s3" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o23528" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres (4–) 5–6 mm.</text>
      <biological_entity id="o23529" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries: ovate (outer) or elliptic (inner), glabrous, apices usually obtuse to rounded, rarely truncate.</text>
      <biological_entity id="o23530" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23531" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s5" to="rounded" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas usually pale-purple, rarely white.</text>
      <biological_entity id="o23532" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="pale-purple" value_original="pale-purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 1–1.2 (–1.5) mm, glabrous or glabrate.</text>
      <biological_entity id="o23533" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich soils, along streams, rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich soils" constraint="along streams , rocky slopes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Sonoran slender-thoroughwort</other_name>
  <discussion>Fleischmannia sonorae was reported for Arizona by T. H. Kearney and R. H. Peebles (1960) and F. Shreve and I. L. Wiggins (1964) as Eupatorium pycnocephalum Lessing [= Fleischmannia pycnocephala (Lessing) R. M. King &amp; H. Robinson)]; revised concepts of this species group (B. L. Turner 1996+, vol. 2) place F. pycnocephala as a related species in Mexico and Central America, not reaching northwestward to Arizona.</discussion>
  
</bio:treatment>