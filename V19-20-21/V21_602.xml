<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="treatment_page">244</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">thymophylla</taxon_name>
    <taxon_name authority="(A. Gray) Greene in N. L. Britton and A. Brown" date="1898" rank="species">aurea</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and A. Brown, Ill. Fl. N. U.S.</publication_title>
      <place_in_publication>3: 453. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus thymophylla;species aurea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067746</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lowellia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">aurea</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 91. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lowellia;species aurea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="(A. Gray) A. Nelson" date="unknown" rank="species">aurea</taxon_name>
    <taxon_hierarchy>genus Dyssodia;species aurea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, green, to 20 (–30) cm, glabrous or puberulent.</text>
      <biological_entity id="o1253" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="puberulent" value_original="puberulent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (branched from bases).</text>
      <biological_entity id="o1254" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o1255" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 15–40 mm overall, lobed, lobes 5–13, linear.</text>
      <biological_entity id="o1256" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s3" to="40" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o1257" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="13" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 10–70 mm, glabrous or puberulent.</text>
      <biological_entity id="o1258" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="70" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi 0, or of 1–2 subulate bractlets, lengths less than 1/2 phyllaries.</text>
      <biological_entity id="o1259" name="calyculus" name_original="calyculi" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1260" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="true" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="1/2" />
      </biological_entity>
      <biological_entity id="o1261" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <relation from="o1259" id="r93" name="consist_of" negation="false" src="d0_s5" to="o1260" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres obconic to campanulate, 5–6 mm.</text>
      <biological_entity id="o1262" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s6" to="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 12–15, margins of outer distinct almost to bases, abaxial faces glabrous or puberulent.</text>
      <biological_entity id="o1263" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="15" />
      </biological_entity>
      <biological_entity id="o1264" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity constraint="outer" id="o1265" name="distinct" name_original="distinct" src="d0_s7" type="structure" />
      <biological_entity id="o1266" name="base" name_original="bases" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o1267" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o1264" id="r94" name="part_of" negation="false" src="d0_s7" to="o1265" />
      <relation from="o1264" id="r95" name="to" negation="false" src="d0_s7" to="o1266" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 8–12;</text>
      <biological_entity id="o1268" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas bright-yellow, laminae 4–6 × 2–3 mm.</text>
      <biological_entity id="o1269" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
      <biological_entity id="o1270" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 30–45;</text>
      <biological_entity id="o1271" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s10" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 3 mm.</text>
      <biological_entity id="o1272" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 3 mm;</text>
      <biological_entity id="o1273" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi either of erose scales 0.3–0.6 mm, or of 3–5-aristate scales 2–3 mm.</text>
      <biological_entity id="o1274" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o1275" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture_or_relief" src="d0_s13" value="erose" value_original="erose" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1276" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="3-5-aristate" value_original="3-5-aristate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o1274" id="r96" name="consists_of" negation="false" src="d0_s13" to="o1275" />
      <relation from="o1274" id="r97" name="consists_of" negation="false" src="d0_s13" to="o1276" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., N.Mex., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The two varieties of Thymophylla aurea are sometimes found in mixed populations, which may include intermediate plants.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi of 18–20, 3–5-aristate scales 2–3 mm</description>
      <determination>7a Thymophylla aurea var. polychaeta</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi of 8–10 erose scales 0.3–0.6 mm</description>
      <determination>7b Thymophylla aurea var. aurea</determination>
    </key_statement>
  </key>
</bio:treatment>