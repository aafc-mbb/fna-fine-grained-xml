<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">514</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">530</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="Kral &amp; G. L. Nesom" date="2003" rank="species">savannensis</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 1574, fig. 1. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species savannensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067115</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (80–) 90–130 cm.</text>
      <biological_entity id="o399" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose, irregularly cylindric, narrowly ovoid, or fusiform and caudexlike.</text>
      <biological_entity id="o400" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s1" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s1" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s1" value="caudex-like" value_original="caudexlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems glabrous (often sparsely sessile-glandular).</text>
      <biological_entity id="o401" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal (often withering before flowering), 1-nerved or 3–5-nerved, linear-elliptic, linear-spatulate, or linear-oblanceolate, (170–) 200–400 × 3–10 mm, gradually reduced distally, essentially glabrous, glanddotted (proximal margins pilose-ciliate).</text>
      <biological_entity id="o402" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-5-nerved" value_original="3-5-nerved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-spatulate" value_original="linear-spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-spatulate" value_original="linear-spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="170" from_unit="mm" name="atypical_length" src="d0_s3" to="200" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="200" from_unit="mm" name="length" src="d0_s3" to="400" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o403" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in dense, spiciform arrays.</text>
      <biological_entity id="o404" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o405" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <relation from="o404" id="r20" name="in" negation="false" src="d0_s4" to="o405" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0 or (spreading-ascending) 1–2 mm.</text>
      <biological_entity id="o406" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="1-2 mm" value_original="1-2 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate-campanulate, (7–) 8–10 × (8–) 9–11 (–12) mm.</text>
      <biological_entity id="o407" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s6" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s6" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in (3–) 4 (–5) series, ovate to oblong, obovate, or spatulate, weakly unequal, essentially glabrous, margins with (pinkish purple) hyaline borders, erose to lacerate, ciliolate, apices broadly rounded.</text>
      <biological_entity id="o408" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s7" to="oblong obovate or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="oblong obovate or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="oblong obovate or spatulate" />
        <character is_modifier="false" modifier="weakly" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o409" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="5" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o410" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_relief" notes="" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o411" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o412" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o408" id="r21" name="in" negation="false" src="d0_s7" to="o409" />
      <relation from="o410" id="r22" name="with" negation="false" src="d0_s7" to="o411" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 9–17;</text>
      <biological_entity id="o413" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s8" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes pilose inside.</text>
      <biological_entity id="o414" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae ca. 4 mm;</text>
      <biological_entity id="o415" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths ± equaling corollas, bristles barbellate.</text>
      <biological_entity id="o416" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o417" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o418" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="barbellate" value_original="barbellate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Aug–)Sep–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy pinelands, moist sites dominated by slash pine, sabal palmetto, or mixture of the two</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy pinelands" />
        <character name="habitat" value="moist sites" />
        <character name="habitat" value="slash pine" />
        <character name="habitat" value="sabal palmetto" />
        <character name="habitat" value="mixture" constraint="of the two" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="common_name">Savanna gayfeather</other_name>
  
</bio:treatment>