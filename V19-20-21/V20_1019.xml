<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">440</other_info_on_meta>
    <other_info_on_meta type="treatment_page">444</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">isocoma</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1991" rank="species">humilis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>70: 92. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus isocoma;species humilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067012</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="Cronquist" date="unknown" rank="species">leverichii</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species leverichii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage villosulous or tomentose to sparsely hispidulous (hairs short, crisped, white), not resinous.</text>
      <biological_entity id="o29885" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s0" to="sparsely hispidulous" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades narrowly oblanceolate, 5–10 (–18) mm, margins usually toothed or lobed (teeth or shallow lobes in 1–2 (–3), pinnately arranged pairs), sometimes entire.</text>
      <biological_entity id="o29886" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s1" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="distance" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29887" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o29888" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o29889" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="depth" src="d0_s1" value="shallow" value_original="shallow" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o29890" name=")" name_original=")" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s1" to="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s1" to="2" />
        <character is_modifier="true" modifier="pinnately" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o29888" id="r2763" name="in" negation="false" src="d0_s1" to="o29890" />
      <relation from="o29889" id="r2764" name="in" negation="false" src="d0_s1" to="o29890" />
    </statement>
    <statement id="d0_s2">
      <text>Involucres 5–6 × 6–7 mm.</text>
      <biological_entity id="o29891" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary apices greenish, not aristate, gland-dotted, without resin pockets.</text>
      <biological_entity constraint="phyllary" id="o29892" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="aristate" value_original="aristate" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity constraint="resin" id="o29893" name="pocket" name_original="pockets" src="d0_s3" type="structure" />
      <relation from="o29892" id="r2765" name="without" negation="false" src="d0_s3" to="o29893" />
    </statement>
    <statement id="d0_s4">
      <text>Florets 19–28;</text>
      <biological_entity id="o29894" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="19" name="quantity" src="d0_s4" to="28" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas 4–5 mm.</text>
      <biological_entity id="o29895" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypsela ribs not forming apical horns.</text>
      <biological_entity constraint="cypsela" id="o29896" name="rib" name_original="ribs" src="d0_s6" type="structure" />
      <biological_entity constraint="apical" id="o29897" name="horn" name_original="horns" src="d0_s6" type="structure" />
      <relation from="o29896" id="r2766" name="forming" negation="false" src="d0_s6" to="o29897" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils from red sandstone, pinyon-juniper-shrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" constraint="from red sandstone" />
        <character name="habitat" value="red sandstone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Zion jimmyweed</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Isocoma humilis is known only from Washington County. It is recognized by its low stature (mostly 4–8 cm) and rounded habit, villosulous vestiture, small, toothed leaves, relatively large, many-flowered heads borne singly or in pairs, and small corollas and cypselae.</discussion>
  
</bio:treatment>