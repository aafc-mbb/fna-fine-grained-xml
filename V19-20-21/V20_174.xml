<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">gutierrezia</taxon_name>
    <taxon_name authority="(de Candolle) Torrey &amp; A. Gray" date="1842" rank="species">texana</taxon_name>
    <taxon_name authority="(S. Schauer) M. A. Lane" date="1980" rank="variety">glutinosa</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>8: 314. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus gutierrezia;species texana;variety glutinosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068416</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemiachyris</taxon_name>
    <taxon_name authority="S. Schauer" date="unknown" rank="species">glutinosa</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>19: 724. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemiachyris;species glutinosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gutierrezia</taxon_name>
    <taxon_name authority="(S. Schauer) Schultz-Bipontinus" date="unknown" rank="species">glutinosa</taxon_name>
    <taxon_hierarchy>genus Gutierrezia;species glutinosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Involucres mostly campanulate, (2–) 3.5–4.5 mm diam.</text>
      <biological_entity id="o23069" name="involucre" name_original="involucres" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s0" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s0" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s0" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Disc-florets 10–48.</text>
      <biological_entity id="o23070" name="disc-floret" name_original="disc-florets" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s1" to="48" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cypselae moderately to densely strigoso-sericeous, hairs usually in dense lines, at least partially obscuring faces;</text>
      <biological_entity id="o23071" name="cypsela" name_original="cypselae" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s2" value="strigoso-sericeous" value_original="strigoso-sericeous" />
      </biological_entity>
      <biological_entity id="o23072" name="hair" name_original="hairs" src="d0_s2" type="structure" />
      <biological_entity id="o23073" name="line" name_original="lines" src="d0_s2" type="structure">
        <character is_modifier="true" name="density" src="d0_s2" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o23074" name="face" name_original="faces" src="d0_s2" type="structure" />
      <relation from="o23072" id="r2133" name="in" negation="false" src="d0_s2" to="o23073" />
      <relation from="o23072" id="r2134" modifier="at-least partially; partially" name="obscuring" negation="false" src="d0_s2" to="o23074" />
    </statement>
    <statement id="d0_s3">
      <text>pappi of 1 series of distinct scales 0.2–0.5 mm, rarely more reduced.</text>
      <biological_entity id="o23076" name="series" name_original="series" src="d0_s3" type="structure" constraint="scale" constraint_original="scale; scale">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23077" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o23075" id="r2135" name="consist_of" negation="false" src="d0_s3" to="o23076" />
      <relation from="o23076" id="r2136" name="part_of" negation="false" src="d0_s3" to="o23077" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 8, 16.</text>
      <biological_entity id="o23075" name="pappus" name_original="pappi" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rarely" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23078" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="8" value_original="8" />
        <character name="quantity" src="d0_s4" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands and pine-oak-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pine-oak-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(5–)500–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="5" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila, Guanajuato, Hidalgo, Nuevo León, Querétaro, San Luis Potosí, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Guanajuato)" establishment_means="native" />
        <character name="distribution" value="Mexico (Hidalgo)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Querétaro)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  
</bio:treatment>