<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Luc Brouillet</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="genus">HERRICKIA</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 186, plate 50. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus HERRICKIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Clarence Luther Herrick, 1858–1903, geologist and botanical collector in New Mexico, president of University of New Mexico</other_info_on_name>
    <other_info_on_name type="fna_id">115203</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Cassini) Cassini" date="unknown" rank="genus">Eurybia</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) G. L. Nesom" date="unknown" rank="section">Herrickia</taxon_name>
    <taxon_hierarchy>genus Eurybia;section Herrickia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 1–70 cm (rhizomes elongate and slender to short and thick, often becoming woody, or woody caudices).</text>
      <biological_entity id="o24058" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o24059" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, usually simple, rarely branched proximally, glabrous or thinly scabridulous, sometimes stipitate-glandular (mostly distally).</text>
      <biological_entity id="o24060" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely; proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal or mostly cauline;</text>
      <biological_entity id="o24063" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or petiolate;</text>
      <biological_entity id="o24061" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-nerved, oblanceolate to spatulate, distal usually gradually reduced, margins entire or spinulose-serrate, faces glabrous or scabrellous, sometimes stipitate-glandular.</text>
      <biological_entity id="o24064" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="spatulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24065" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o24066" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spinulose-serrate" value_original="spinulose-serrate" />
      </biological_entity>
      <biological_entity id="o24067" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, in corymbiform arrays or borne singly.</text>
      <biological_entity id="o24068" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o24069" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o24068" id="r2218" name="in" negation="false" src="d0_s6" to="o24069" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindro to hemispherico-campanulate, (6–12 ×) 5–10 mm.</text>
      <biological_entity id="o24070" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindro" name="shape" src="d0_s7" to="hemispherico-campanulate" />
        <character char_type="range_value" from="[6" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="]5" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 15–40+ in 3–6 series, 1-nerved (low-keeled or rounded adaxially), spatulate, oblanceolate, oblong-obovate, oblong, ovate, lanceolate, or linear-lanceolate, unequal, bases indurate, margins narrowly scarious (sometimes foliaceous), often ciliolate;</text>
      <biological_entity id="o24071" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o24072" from="15" name="quantity" src="d0_s8" to="40" upper_restricted="false" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-obovate" value_original="oblong-obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o24072" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o24073" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o24074" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s8" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>green zones ± basally truncate, usually in distal 1/5–9/10, rarely wholly foliaceous (outer) to less than 1/6 and only along midveins (inner);</text>
      <biological_entity constraint="distal" id="o24076" name="1/5-9/10" name_original="1/5-9/10" src="d0_s9" type="structure" />
      <biological_entity id="o24077" name="midvein" name_original="midveins" src="d0_s9" type="structure" />
      <relation from="o24075" id="r2219" modifier="usually" name="in" negation="false" src="d0_s9" to="o24076" />
      <relation from="o24075" id="r2220" modifier="only" name="along" negation="false" src="d0_s9" to="o24077" />
    </statement>
    <statement id="d0_s10">
      <text>(apices acute to long-acuminate), faces glabrous, usually stipitate-glandular.</text>
      <biological_entity id="o24075" name="zone" name_original="zones" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" modifier="more or less basally" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="rarely wholly" name="architecture" notes="" src="d0_s9" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="1/6" />
      </biological_entity>
      <biological_entity id="o24078" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat to slightly convex, pitted, epaleate.</text>
      <biological_entity id="o24079" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s11" to="slightly convex" />
        <character is_modifier="false" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 8–27, pistillate, fertile;</text>
      <biological_entity id="o24080" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="27" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas white to purple (coiling at maturity).</text>
      <biological_entity id="o24081" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 12–43, bisexual, fertile;</text>
      <biological_entity id="o24082" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s14" to="43" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas yellow, becoming purple at maturity, barely ampliate, tubes shorter to longer than funnelform to campanulate throats, lobes 5, erect to spreading, triangular or lanceolate;</text>
      <biological_entity id="o24083" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character constraint="at tubes" constraintid="o24084" is_modifier="false" modifier="becoming" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o24084" name="tube" name_original="tubes" src="d0_s15" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s15" value="maturity" value_original="maturity" />
        <character is_modifier="true" modifier="barely" name="size" src="d0_s15" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o24085" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character modifier="to campanulate throats" name="quantity" src="d0_s15" value="5" value_original="5" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s15" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s15" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style-branch appendages lanceolate.</text>
      <biological_entity constraint="style-branch" id="o24086" name="appendage" name_original="appendages" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae cylindro-obconic to fusiform, ± compressed, 7–10-nerved, faces glabrous or densely strigillose, eglandular;</text>
      <biological_entity id="o24087" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character char_type="range_value" from="cylindro-obconic" name="shape" src="d0_s17" to="fusiform" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="7-10-nerved" value_original="7-10-nerved" />
      </biological_entity>
      <biological_entity id="o24088" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s17" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi persistent, of 35–70+, yellowish to cinnamon or tawny, unequal, ± stiff, barbellate, apically attenuate or (longer) sometimes ± clavate bristles in 1+ series.</text>
      <biological_entity id="o24090" name="bristle" name_original="bristles" src="d0_s18" type="structure">
        <character char_type="range_value" from="35" is_modifier="true" name="quantity" src="d0_s18" to="70" upper_restricted="false" />
        <character char_type="range_value" from="yellowish" is_modifier="true" name="coloration" src="d0_s18" to="cinnamon or tawny" />
        <character is_modifier="true" name="size" src="d0_s18" value="unequal" value_original="unequal" />
        <character is_modifier="true" modifier="more or less" name="fragility" src="d0_s18" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="architecture" src="d0_s18" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s18" value="attenuate" value_original="attenuate" />
        <character is_modifier="true" modifier="sometimes more or less" name="shape" src="d0_s18" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity id="o24091" name="series" name_original="series" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s18" upper_restricted="false" />
      </biological_entity>
      <relation from="o24089" id="r2221" name="consist_of" negation="false" src="d0_s18" to="o24090" />
      <relation from="o24090" id="r2222" name="in" negation="false" src="d0_s18" to="o24091" />
    </statement>
    <statement id="d0_s19">
      <text>x = 9.</text>
      <biological_entity id="o24089" name="pappus" name_original="pappi" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o24092" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>192.</number>
  <other_name type="common_name">Aster</other_name>
  <discussion>Species 4 (4 in the flora).</discussion>
  <discussion>G. L. Nesom (1994b) included Herrickia within Eurybia, as sect. Herrickia in subg. Eurybia, adding E. glauca and E. wasatchensis to the section. L. Brouillet et al. (2004) added H. kingii to the group and confirmed the membership proposed by Nesom using molecular phylogenetic data. Previously, Nesom (1991e) had placed H. kingii in Tonestus, a polyphyletic group. In the eurybioids, a grade basal to subtribe Machaerantherinae, the order of appearance of the genera recognized here is: Oreostemma, Herrickia, Eurybia, and Triniteurybia. This underscores the intermediate position of the eurybioids between basal x = 9, asterlike, more or less mesic ancestors, and the more xeric and derived Machaerantherinae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves basal and cauline; plants taprooted or with caudices; rays white to pale lavender</description>
      <determination>1 Herrickia kingii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves mostly cauline; plants rhizomatous; rays purple</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries appressed</description>
      <determination>2 Herrickia glauca</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries squarrose or spreading</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perennials; leaves ± clasping, entire, eglandular</description>
      <determination>3 Herrickia wasatchensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Subshrubs; leaves cordate-clasping, spinulose-serrate, glandular</description>
      <determination>4 Herrickia horrida</determination>
    </key_statement>
  </key>
</bio:treatment>