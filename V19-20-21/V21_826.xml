<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="treatment_page">331</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="(A. Gray) A. M. Powell" date="1968" rank="section">laphamia</taxon_name>
    <taxon_name authority="Greene ex A. M. Powell &amp; Yarborough" date="1994" rank="species">ambrosiifolia</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>76: 325, fig. 1. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section laphamia;species ambrosiifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067309</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 10–30 cm (stems brittle, densely leafy);</text>
    </statement>
    <statement id="d0_s1">
      <text>usually villous, often with glandular-hairs, sometimes pilose.</text>
      <biological_entity id="o19278" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o19279" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o19280" name="glandular-hair" name_original="glandular-hairs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 5–10 mm;</text>
      <biological_entity id="o19281" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19282" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 3-partite or compound-pinnatifid, 15–35 × 15–30 mm, lobes lobed, cleft, parted, or divided, ultimate margins crenate.</text>
      <biological_entity id="o19283" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19284" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="3-partite" value_original="3-partite" />
        <character is_modifier="false" name="shape" src="d0_s3" value="compound-pinnatifid" value_original="compound-pinnatifid" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19285" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s3" value="parted" value_original="parted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character is_modifier="false" name="shape" src="d0_s3" value="parted" value_original="parted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19286" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in corymbiform arrays, 7–10 × 6–11 mm.</text>
      <biological_entity id="o19287" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" notes="" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" notes="" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19288" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o19287" id="r1313" name="in" negation="false" src="d0_s4" to="o19288" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 3–10 mm.</text>
      <biological_entity id="o19289" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate.</text>
      <biological_entity id="o19290" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 14–20, linear to linear-lanceolate, 6–9 × 0.5–1.2 (–2) mm (apices usually short-attenuate, sometimes acute).</text>
      <biological_entity id="o19291" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s7" to="20" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="linear-lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets usually 0 (sometimes 1–2 reduced rays in isolated heads, laminae color unknown, 3–5 × 1.5–2 mm).</text>
      <biological_entity id="o19292" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 25–45;</text>
      <biological_entity id="o19293" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, tubes 1–1.2 mm, throats tubular to narrowly funnelform, 2–2.5 mm, lobes 0.6–0.8 mm.</text>
      <biological_entity id="o19294" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o19295" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19296" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character constraint="to funnelform" constraintid="o19297" is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19297" name="funnelform" name_original="funnelform" src="d0_s10" type="structure" />
      <biological_entity id="o19298" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae narrowly oblanceolate, 3–4 mm, margins thin-calloused, short-hairy;</text>
      <biological_entity id="o19299" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19300" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="thin-calloused" value_original="thin-calloused" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi usually 0, sometimes of 1–3 moderately stout bristles 2.8–4.5 mm, often plus hyaline, laciniate scales.</text>
      <biological_entity id="o19301" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="coloration" notes="" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o19302" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s12" to="3" />
        <character is_modifier="true" modifier="moderately" name="fragility_or_size" src="d0_s12" value="stout" value_original="stout" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
      <relation from="o19301" id="r1314" modifier="sometimes" name="consist_of" negation="false" src="d0_s12" to="o19302" />
    </statement>
    <statement id="d0_s13">
      <text>2n = ca. 17.</text>
      <biological_entity id="o19303" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19304" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices, cliff faces, and canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="cliff faces" />
        <character name="habitat" value="canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="common_name">Lace-leaved rock daisy</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Perityle ambrosiifolia occurs in the vicinity of Clifton and Morenci in Greenlee County. Most heads are discoid; 1 or 2 ray florets sometimes appear on isolated heads; color of the laminae is not known; only dried specimens without the ray color noted have been examined. The species was first recognized by E. L. Greene in 1900; no record exists that the species as proposed by him was formally published. Perityle ambrosiifolia is morphologically and geographically distinct from P. lemmonii and may have resulted from either intrasectional or intersectional hybridization between two of several possible taxa.</discussion>
  
</bio:treatment>