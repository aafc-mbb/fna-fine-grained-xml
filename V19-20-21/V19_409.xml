<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="A. Gray" date="1883" rank="species">pringlei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 69. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species pringlei</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066953</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–45+ cm.</text>
      <biological_entity id="o16168" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally usually piloso-hirsute (or lanate, hairs 5–8+ mm, usually curled and tangled), sometimes nearly glabrous, distally piloso-hirsute (hairs 1–2+ mm) and stellate-pubescent.</text>
      <biological_entity id="o16169" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally usually" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="sometimes nearly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal (2–) 3–8+, cauline (0–) 3+;</text>
      <biological_entity id="o16170" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o16171" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o16172" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades elliptic or spatulate to oblanceolate, lanceolate, or lance-linear, (35–) 50–120 (–200+) × 10–25 (–40+) mm, lengths 2–5 (–10+) times widths, bases ± cuneate, margins usually entire, rarely toothed, apices obtuse to acute, faces (proximal leaves) piloso-hirsute (to lanate, hairs 1–3+ mm; distal leaves often ± glabrate).</text>
      <biological_entity id="o16173" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16174" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate lanceolate or lance-linear" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate lanceolate or lance-linear" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate lanceolate or lance-linear" />
        <character char_type="range_value" from="35" from_unit="mm" name="atypical_length" src="d0_s3" to="50" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="200" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2-5(-10+)" value_original="2-5(-10+)" />
      </biological_entity>
      <biological_entity id="o16175" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o16176" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o16177" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o16178" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 3–12 (–20+) in corymbiform arrays.</text>
      <biological_entity id="o16179" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="20" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o16180" from="3" name="quantity" src="d0_s4" to="12" />
      </biological_entity>
      <biological_entity id="o16180" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles usually stellate-pubescent and stipitate-glandular.</text>
      <biological_entity id="o16181" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 3–5+.</text>
      <biological_entity id="o16182" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o16183" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, (7–) 8–9 mm.</text>
      <biological_entity id="o16184" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–15+, apices rounded to acute, abaxial faces stellate-pubescent and/or stipitate-glandular.</text>
      <biological_entity id="o16185" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16186" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16187" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 12–15+;</text>
      <biological_entity id="o16188" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 7–9 mm.</text>
      <biological_entity id="o16189" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae columnar, 2.2–4 mm;</text>
      <biological_entity id="o16190" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of (40–) 60–80, white or stramineous bristles in 2+ series, 4–5+ mm.</text>
      <biological_entity id="o16191" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16192" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" name="atypical_quantity" src="d0_s12" to="60" to_inclusive="false" />
        <character char_type="range_value" from="60" is_modifier="true" name="quantity" src="d0_s12" to="80" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o16193" name="series" name_original="series" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" upper_restricted="false" />
      </biological_entity>
      <relation from="o16191" id="r1474" name="consist_of" negation="false" src="d0_s12" to="o16192" />
      <relation from="o16192" id="r1475" name="in" negation="false" src="d0_s12" to="o16193" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine, oak, and pine-oak forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine-oak forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico; Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  
</bio:treatment>