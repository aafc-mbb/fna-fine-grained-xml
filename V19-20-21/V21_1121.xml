<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">447</other_info_on_meta>
    <other_info_on_meta type="treatment_page">448</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="genus">tetraneuris</taxon_name>
    <taxon_name authority="(Hooker) Greene" date="1898" rank="species">linearifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 269. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus tetraneuris;species linearifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067732</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenoxys</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">linearifolia</taxon_name>
    <place_of_publication>
      <publication_title>Icon. Pl.</publication_title>
      <place_in_publication>2: plate 146. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenoxys;species linearifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 16–50+ cm.</text>
      <biological_entity id="o17585" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudices none.</text>
      <biological_entity id="o17586" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10, erect or ± decumbent, leafy, unbranched or branched distally.</text>
      <biological_entity id="o17587" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, ± hairy, ± glanddotted;</text>
      <biological_entity id="o17588" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades spatulate to oblanceolate, entire or with 2–6 teeth or lobes;</text>
      <biological_entity constraint="basal" id="o17589" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with 2-6 teeth or lobes" />
      </biological_entity>
      <biological_entity id="o17590" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity id="o17591" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <relation from="o17589" id="r1210" name="with" negation="false" src="d0_s4" to="o17590" />
      <relation from="o17589" id="r1211" name="with" negation="false" src="d0_s4" to="o17591" />
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline blades narrowly spatulate or oblanceolate to linear-oblanceolate, entire or with 2–6 teeth or lobes;</text>
      <biological_entity constraint="proximal cauline" id="o17592" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear-oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="with 2-6 teeth or lobes" />
      </biological_entity>
      <biological_entity id="o17593" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o17594" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <relation from="o17592" id="r1212" name="with" negation="false" src="d0_s5" to="o17593" />
      <relation from="o17592" id="r1213" name="with" negation="false" src="d0_s5" to="o17594" />
    </statement>
    <statement id="d0_s6">
      <text>mid-blades oblanceolate to linear-oblanceolate to linear, 0.9–9.5 (–16) mm wide, entire or with 1–2 teeth or lobes;</text>
      <biological_entity id="o17595" name="mid-blade" name_original="mid-blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="linear-oblanceolate" />
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="mm" name="width" src="d0_s6" to="16" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s6" to="9.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="with 1-2 teeth or lobes" />
      </biological_entity>
      <biological_entity id="o17596" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <biological_entity id="o17597" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <relation from="o17595" id="r1214" name="with" negation="false" src="d0_s6" to="o17596" />
      <relation from="o17595" id="r1215" name="with" negation="false" src="d0_s6" to="o17597" />
    </statement>
    <statement id="d0_s7">
      <text>distal blades narrowly oblanceolate to linear-oblanceolate to linear, entire or with 1–2 teeth or lobes.</text>
      <biological_entity constraint="distal" id="o17598" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s7" to="linear-oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="with 1-2 teeth or lobes" />
      </biological_entity>
      <biological_entity id="o17599" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o17600" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <relation from="o17598" id="r1216" name="with" negation="false" src="d0_s7" to="o17599" />
      <relation from="o17598" id="r1217" name="with" negation="false" src="d0_s7" to="o17600" />
    </statement>
    <statement id="d0_s8">
      <text>Heads 8–50 (–80) per plant, borne singly or in corymbiform arrays.</text>
      <biological_entity id="o17601" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="80" />
        <character char_type="range_value" constraint="per plant" constraintid="o17602" from="8" name="quantity" src="d0_s8" to="50" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s8" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="in corymbiform arrays" value_original="in corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o17602" name="plant" name_original="plant" src="d0_s8" type="structure" />
      <biological_entity id="o17603" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o17601" id="r1218" name="in" negation="false" src="d0_s8" to="o17603" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 8–29 cm, ± hairy.</text>
      <biological_entity id="o17604" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s9" to="29" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres 5–10 × 7–15 mm.</text>
      <biological_entity id="o17605" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Outer phyllaries 8–21, 2.4–5.5 mm, margins 0–0.2 mm wide, not scarious or scarious, abaxial faces ± hairy.</text>
      <biological_entity constraint="outer" id="o17606" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="21" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17607" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s11" to="0.2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17608" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 9–25;</text>
      <biological_entity id="o17609" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s12" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 8.2–16.8 mm.</text>
      <biological_entity id="o17610" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="8.2" from_unit="mm" name="some_measurement" src="d0_s13" to="16.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 50–200+;</text>
      <biological_entity id="o17611" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s14" to="200" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas yellow, 1.6–3 mm.</text>
      <biological_entity id="o17612" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 1.5–2.6 mm;</text>
      <biological_entity id="o17613" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 4–8 obovate, often aristate scales 1–2.5 mm.</text>
      <biological_entity id="o17614" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity id="o17615" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s17" to="8" />
        <character is_modifier="true" name="shape" src="d0_s17" value="obovate" value_original="obovate" />
        <character is_modifier="true" modifier="often" name="shape" src="d0_s17" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o17614" id="r1219" name="consist_of" negation="false" src="d0_s17" to="o17615" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans., N.Mex., Okla., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Plants of Tetraneuris linearifolia morphologically intermediate between the two varieties are found in Bee, Duval, Live Oak, and McMullen counties, Texas.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems erect, branched distally; leaves ± hairy, basal blades entire or with 2(–6) teeth or lobes, proximal cauline blades entire or with 1–2 teeth or lobes, mid blades (0.9–)1.4–3(–4.8) mm wide; involucres (7–)8–11(–12) mm diam.; pappus scales aristate; Kansas, New Mexico, Oklahoma, Texas, on limestone-derived soils</description>
      <determination>1a Tetraneuris linearifolia var. linearifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems erect or ± decumbent, ± branched distally; leaves usually densely hairy, basal blades with 2–6 teeth or lobes, proximal cauline blades with 2(–6) teeth or lobes, mid blades (2.5–)3.9–9.5(–16) mm wide; involucres 12–15 mm diam.; pappus scales not aristate; s Texas on sand</description>
      <determination>1b Tetraneuris linearifolia var. arenicola</determination>
    </key_statement>
  </key>
</bio:treatment>