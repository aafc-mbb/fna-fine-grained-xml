<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="treatment_page">503</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="S. Watson" date="1873" rank="species">longifolia</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Naturalist</publication_title>
      <place_in_publication>7: 301. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species longifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066264</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 20–200 [–250] cm.</text>
      <biological_entity id="o18333" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched from above bases, ± glabrous.</text>
      <biological_entity id="o18334" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="above bases" constraintid="o18335" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18335" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o18336" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 0–5 mm;</text>
      <biological_entity id="o18337" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 3-nerved from bases, lance-elliptic, lanceovate, lance-linear, or linear (folded or falcate), 10–130 × 2–9 mm, bases rounded or tapering, margins entire or nearly so, apices acuminate, faces glanddotted, often puberulent (shiny).</text>
      <biological_entity id="o18338" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="from bases" constraintid="o18339" is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s4" value="lance-elliptic" value_original="lance-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="130" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18339" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o18340" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o18341" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o18342" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o18343" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in (leafy) paniculiform arrays.</text>
      <biological_entity id="o18344" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o18345" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o18344" id="r1252" name="in" negation="false" src="d0_s5" to="o18345" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 0–3 mm, glabrous or glutinous.</text>
      <biological_entity id="o18346" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s6" value="glutinous" value_original="glutinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric, 5–7 mm.</text>
      <biological_entity id="o18347" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 10–24 in 6–8 series, pale green to stramineous, 3–5-striate, unequal, (glutinous) margins scarious;</text>
      <biological_entity id="o18348" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o18349" from="10" name="quantity" src="d0_s8" to="24" />
        <character char_type="range_value" from="pale green" name="coloration" notes="" src="d0_s8" to="stramineous" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s8" value="3-5-striate" value_original="3-5-striate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o18349" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
      <biological_entity id="o18350" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer ovate (apices acute), inner lanceolate (apices obtuse).</text>
      <biological_entity constraint="outer" id="o18351" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18352" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 3–7;</text>
      <biological_entity id="o18353" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas cream, 3.5–4.5 mm.</text>
      <biological_entity id="o18354" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 1.8–2.5 mm, scabrous;</text>
      <biological_entity id="o18355" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 30–40 barbellulate bristles.</text>
      <biological_entity id="o18356" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o18357" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s13" to="40" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <relation from="o18356" id="r1253" name="consist_of" negation="false" src="d0_s13" to="o18357" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades lance-elliptic to lance-linear or linear, 10–130 × 2–9 mm, lengths 8–25 times widths, flat (not folded or falcate), bases tapering</description>
      <determination>23a Brickellia longifolia var. longifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades lance-ovate to narrowly lanceolate, 10–130 × 2–15 mm, lengths 4–8 times widths, folded along midveins and/or falcate, basesrounded</description>
      <determination>23b Brickellia longifolia var. multiflora</determination>
    </key_statement>
  </key>
</bio:treatment>