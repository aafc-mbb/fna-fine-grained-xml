<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>R. David Whetstone,Kristin R. Brodeur</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">214</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1822" rank="genus">LAUNAEA</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 25: 61, 321. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus LAUNAEA</taxon_hierarchy>
    <other_info_on_name type="etymology">For J. Cl. M. Mordant de Launay, 1750–1816, lawyer, later librarian at Musée d’Histoire Naturelle, Paris</other_info_on_name>
    <other_info_on_name type="fna_id">117736</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="de Candolle" date="unknown" rank="genus">Brachyramphus</taxon_name>
    <taxon_hierarchy>genus Brachyramphus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials [perennials, shrubs, sometimes spiny], [5–] 30–150 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually taprooted [stoloniferous].</text>
      <biological_entity id="o16893" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect [prostrate], distally branched, glabrous [± hairy].</text>
      <biological_entity id="o16895" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal or basal and proximally cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o16896" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="proximally" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades ± oblanceolate, often pinnately lobed, ultimate margins usually dentate (teeth usually ± prickly; faces glabrous [± hairy]).</text>
      <biological_entity id="o16897" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="often pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16898" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads in spiciform or racemiform to paniculiform arrays [borne singly].</text>
      <biological_entity id="o16899" name="head" name_original="heads" src="d0_s6" type="structure" />
      <biological_entity id="o16900" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
      <relation from="o16899" id="r1518" name="in" negation="false" src="d0_s6" to="o16900" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles not inflated distally, bracteolate.</text>
      <biological_entity id="o16901" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not; distally" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0 (or bractlets intergrading with phyllaries).</text>
      <biological_entity id="o16902" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindric [urceolate, campanulate, or obconic], 3–5 [–16] mm diam.</text>
      <biological_entity id="o16903" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries (persistent, reflexed in fruit) 18–25 in 3–5+ series, unequal, ovate to lanceolate (outer) or linear (inner), margins scarious, apices obtuse to acuminate (faces glabrous [± hairy]).</text>
      <biological_entity id="o16904" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o16905" from="18" name="quantity" src="d0_s10" to="25" />
        <character is_modifier="false" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o16905" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16906" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o16907" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat to convex, epaleate.</text>
      <biological_entity id="o16908" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s11" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Florets 25–30;</text>
      <biological_entity id="o16909" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow to ochroleucous [cyanic].</text>
      <biological_entity id="o16910" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="ochroleucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae blackish to grayish, cylindric to fusiform or ± prismatic, sometimes ± compressed, beaks 0 (or lengths 0.05–0.1 times bodies), 4–5-ribbed (or grooved), ribs usually muricate, faces glabrous;</text>
      <biological_entity id="o16911" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="blackish" name="coloration" src="d0_s14" to="grayish" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s14" to="fusiform or more or less prismatic" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o16912" name="beak" name_original="beaks" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="4-5-ribbed" value_original="4-5-ribbed" />
      </biological_entity>
      <biological_entity id="o16913" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s14" value="muricate" value_original="muricate" />
      </biological_entity>
      <biological_entity id="o16914" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent or tardily falling [readily falling], double [simple], of 60–100+, outer, white, often ± coiled or crisped (frizzy) hairs or bristles in 2–3 series plus 80–120+, white, coarser, barbellulate to smooth bristles in 2–3+ series, all distinct or some basally connate.</text>
      <biological_entity id="o16915" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="relief" src="d0_s15" value="coarser" value_original="coarser" />
        <character char_type="range_value" from="barbellulate" name="architecture" src="d0_s15" to="smooth" />
      </biological_entity>
      <biological_entity id="o16916" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character char_type="range_value" from="60" is_modifier="true" name="quantity" src="d0_s15" to="100" upper_restricted="false" />
        <character is_modifier="true" name="position" src="d0_s15" value="outer" value_original="outer" />
        <character is_modifier="true" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="true" modifier="often more or less" name="architecture" src="d0_s15" value="coiled" value_original="coiled" />
        <character is_modifier="true" name="shape" src="d0_s15" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o16917" name="bristle" name_original="bristles" src="d0_s15" type="structure" />
      <biological_entity id="o16918" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="3" />
        <character char_type="range_value" from="80" name="quantity" src="d0_s15" to="120" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o16920" name="series" name_original="series" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="3" upper_restricted="false" />
      </biological_entity>
      <relation from="o16915" id="r1519" name="consist_of" negation="false" src="d0_s15" to="o16916" />
      <relation from="o16915" id="r1520" name="consist_of" negation="false" src="d0_s15" to="o16917" />
      <relation from="o16917" id="r1521" name="in" negation="false" src="d0_s15" to="o16918" />
      <relation from="o16919" id="r1522" name="in" negation="false" src="d0_s15" to="o16920" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o16919" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" notes="" src="d0_s15" value="distinct" value_original="distinct" />
        <character name="fusion" src="d0_s15" value="some" value_original="some" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="x" id="o16921" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mexico; West Indies; Central America; South America; introduced also in Europe, n Africa, Atlantic Islands, sw, c Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="sw" establishment_means="introduced" />
        <character name="distribution" value="c Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <discussion>Species ca. 50 (1 in the flora).</discussion>
  <references>
    <reference>Kilian, N. 1997. Revision of Launaea Cass. (Compositae, Lactuceae, Sonchinae). Englera 17.</reference>
  </references>
  
</bio:treatment>