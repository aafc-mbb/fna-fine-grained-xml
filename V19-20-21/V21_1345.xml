<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">530</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="(Greene) K. Schumann" date="1903" rank="species">elegantula</taxon_name>
    <place_of_publication>
      <publication_title>Just’s Bot. Jahresber.</publication_title>
      <place_in_publication>29(1): 569. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species elegantula</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067098</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">elegantula</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 316. 1901 (as Laciniaria)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lacinaria;species elegantula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">graminifolia</taxon_name>
    <taxon_name authority="(Greene) Gaiser" date="unknown" rank="variety">elegantula</taxon_name>
    <taxon_hierarchy>genus Liatris;species graminifolia;variety elegantula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–105 cm.</text>
      <biological_entity id="o6398" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="105" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms mostly globose.</text>
      <biological_entity id="o6399" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s1" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems glabrous.</text>
      <biological_entity id="o6400" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline 1-nerved, linear-oblanceolate to narrowly oblanceolate, 80–210 × 2–5 (–10) mm, gradually or abruptly reduced distally, essentially glabrous (sparsely piloso-ciliate along proximal margins), glanddotted.</text>
      <biological_entity id="o6401" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s3" to="210" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly; abruptly; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in loose, racemiform arrays (internodes 2–14 mm).</text>
      <biological_entity id="o6402" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o6403" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o6402" id="r466" name="in" negation="false" src="d0_s4" to="o6403" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0 or (ascending) 2 (–7) mm.</text>
      <biological_entity id="o6404" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="2(-7) mm" value_original="2(-7) mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate, 6–8 × 5–7 mm.</text>
      <biological_entity id="o6405" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 (–5) series, mostly oblong, unequal, essentially glabrous, margins with hyaline borders (0.2–0.4 mm wide), sometimes sparsely ciliolate, apices rounded.</text>
      <biological_entity id="o6406" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" notes="" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6407" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o6408" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" notes="" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o6409" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o6410" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o6406" id="r467" name="in" negation="false" src="d0_s7" to="o6407" />
      <relation from="o6408" id="r468" name="with" negation="false" src="d0_s7" to="o6409" />
    </statement>
    <statement id="d0_s8">
      <text>Florets (7–) 8–11 (–13);</text>
      <biological_entity id="o6411" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character char_type="range_value" from="11" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="13" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="11" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes pilose inside.</text>
      <biological_entity id="o6412" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2.8–3.5 (–3.8) mm;</text>
      <biological_entity id="o6413" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths ± equaling corollas, bristles barbellate.</text>
      <biological_entity id="o6414" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o6415" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity id="o6416" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6417" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Longleaf pine-scrub oak, pine, live oak-pine, deciduous oak-pine, deciduous flatwoods, sandhills, savanna edges, edges of cypress depressions, depression meadows, live oak-pine-palmetto hammocks, sandy clay or loam, rarely clay</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine-scrub oak" modifier="longleaf" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="live oak-pine" />
        <character name="habitat" value="deciduous oak-pine" />
        <character name="habitat" value="deciduous flatwoods" />
        <character name="habitat" value="sandhills" />
        <character name="habitat" value="savanna edges" />
        <character name="habitat" value="edges" constraint="of cypress depressions" />
        <character name="habitat" value="cypress depressions" />
        <character name="habitat" value="depression meadows" />
        <character name="habitat" value="oak-pine-palmetto hammocks" modifier="live" />
        <character name="habitat" value="sandy clay" />
        <character name="habitat" value="loam" />
        <character name="habitat" value="clay" modifier="rarely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300(–500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="common_name">Greene’s elegant gayfeather</other_name>
  
</bio:treatment>