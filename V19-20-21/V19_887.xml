<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="(Miller) Lessing" date="1832" rank="subgenus">absinthium</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1886" rank="species">pattersonii</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 1(2): 453. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus absinthium;species pattersonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066158</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="(A. Gray) A. Heller" date="unknown" rank="species">monocephala</taxon_name>
    <taxon_hierarchy>genus Artemisia;species monocephala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">scopulorum</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">monocephala</taxon_name>
    <taxon_hierarchy>genus Artemisia;species scopulorum;variety monocephala;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 8–20 cm, mildly aromatic.</text>
      <biological_entity id="o10147" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="mildly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems gray-brown, glabrate or finely pubescent.</text>
      <biological_entity id="o10148" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, gray-green;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o10149" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (basal) broadly spatulate, 2–4 × 0.5 cm, pinnately lobed (lobes ca. 1.5 mm wide; cauline smaller, 1-pinnately lobed or entire), faces silky-hairy.</text>
      <biological_entity id="o10150" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character name="width" src="d0_s4" unit="cm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o10151" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="silky-hairy" value_original="silky-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly or (2–5, spreading to nodding, pedunculate) in paniculiform or racemiform arrays 1–5 × 0.5–1 cm.</text>
      <biological_entity id="o10152" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres broadly hemispheric, 5–8 × 5–8 (–10) mm.</text>
      <biological_entity id="o10153" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries gray (margins dark-brown to black), villous.</text>
      <biological_entity id="o10154" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets: pistillate 7–27;</text>
      <biological_entity id="o10155" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="27" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bisexual 32–100;</text>
      <biological_entity id="o10156" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="32" name="quantity" src="d0_s9" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas (yellow tinged with red), 2–3 mm (including exsert anthers), mostly glabrous (embedded in tangled receptacular hairs).</text>
      <biological_entity id="o10157" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10158" name="corolla" name_original="corollas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.5–2 mm, glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 14.</text>
      <biological_entity id="o10159" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10160" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3500–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="3500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="past_name">pattersoni</other_name>
  <other_name type="common_name">Patterson sagewort</other_name>
  <discussion>Artemisia pattersonii can be distinguished from the closely related A. scopulorum by its heads being borne singly and narrower phyllary margins.</discussion>
  
</bio:treatment>