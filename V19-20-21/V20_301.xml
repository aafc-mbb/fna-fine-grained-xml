<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">140</other_info_on_meta>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Rydberg) G. L. Nesom" date="1993" rank="subsection">junceae</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">guiradonis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 543. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection junceae;species guiradonis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067546</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(A. Gray) Kuntze" date="unknown" rank="species">guiradonis</taxon_name>
    <taxon_hierarchy>genus Aster;species guiradonis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–130 cm;</text>
      <biological_entity id="o13454" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices short, woody.</text>
      <biological_entity id="o13455" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, ascending-erect, glabrous, sometimes sparsely strigose in arrays.</text>
      <biological_entity id="o13456" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending-erect" value_original="ascending-erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="in arrays" is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: sometimes rosettes of smaller leaves present at flowering;</text>
      <biological_entity id="o13457" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13458" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <biological_entity constraint="smaller" id="o13459" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o13458" id="r1222" name="consist_of" negation="false" src="d0_s3" to="o13459" />
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal cauline tapering to elongate, winged petioles, bases nearly sheathing stems, blades linear-lanceolate, 50–200 (including petiole) × 4–9 mm, reduced distally, margins entire, apices acute, faces glabrous;</text>
      <biological_entity id="o13460" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="tapering" name="shape" src="d0_s4" to="elongate" />
      </biological_entity>
      <biological_entity id="o13461" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o13462" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o13463" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="nearly" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o13464" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="200" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o13465" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13466" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13467" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid and distal cauline sessile, blades linear, 25–100 × 3–7 mm, reduced distally and sometimes scalelike, margins entire.</text>
      <biological_entity id="o13468" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="mid" value_original="mid" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o13469" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="100" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o13470" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 15–190, in racemiform to narrowly paniculiform arrays, rarely slightly secund, 12–45 × 2–11 cm, branches ascending.</text>
      <biological_entity id="o13471" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s6" to="190" />
        <character is_modifier="false" modifier="rarely slightly" name="architecture" notes="" src="d0_s6" value="secund" value_original="secund" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s6" to="45" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13472" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s6" to="narrowly paniculiform" />
      </biological_entity>
      <biological_entity id="o13473" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o13471" id="r1223" name="in" negation="false" src="d0_s6" to="o13472" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 3–8 mm, glabrous or glabrate;</text>
      <biological_entity id="o13474" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 3–6, grading into phyllaries.</text>
      <biological_entity id="o13475" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o13476" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o13475" id="r1224" name="into" negation="false" src="d0_s8" to="o13476" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 2.5–4 mm.</text>
      <biological_entity id="o13477" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, linear-triangular, outer 1/3–1/2 length of inner, 1–3 × 0.4–0.9 mm, unequal, margins involute apically, midribs usually enlarged and translucent, inner with margins involute, apices sharply acute.</text>
      <biological_entity id="o13478" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="linear-triangular" value_original="linear-triangular" />
        <character is_modifier="false" name="position" src="d0_s10" value="outer" value_original="outer" />
        <character char_type="range_value" from="1/3 length of inner , 1-3×0.4-0.9 mm , unequal , margins involute apically , midribs usually enlarged and translucent , inner margins with margins involute , apices" name="length" src="d0_s10" to="1/2 length of inner , 1-3×0.4-0.9 mm , unequal , margins involute apically , midribs usually enlarged and translucent , inner margins with margins involute , apices" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13479" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <relation from="o13478" id="r1225" name="in" negation="false" src="d0_s10" to="o13479" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 8–10;</text>
      <biological_entity id="o13480" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 1.2–2.5 × ca. 0.75 mm.</text>
      <biological_entity id="o13481" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="0.75" value_original="0.75" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 10–21;</text>
      <biological_entity id="o13482" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2.8–4 mm, lobes 1 mm.</text>
      <biological_entity id="o13483" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13484" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (obconic) 1–1.5 mm (4–7 ribs lighter than body), sparsely to moderately strigose;</text>
      <biological_entity id="o13485" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 1.5–2 mm. 2n = 18.</text>
      <biological_entity id="o13486" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13487" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Near streams in asbestos-laden soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="near streams" constraint="in asbestos-laden soils" />
        <character name="habitat" value="asbestos-laden soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48.</number>
  <other_name type="common_name">Guirado’s goldenrod</other_name>
  <discussion>Solidago guiradonis is a rare species found only in the vicinity of San Carlos and San Benito peaks, San Benito and Fresno counties, California. It is similar to S. confinis but has much narrower leaves and smaller cypselae.</discussion>
  
</bio:treatment>