<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">177</other_info_on_meta>
    <other_info_on_meta type="illustration_page">174</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">galinsoginae</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="genus">bebbia</taxon_name>
    <taxon_name authority="(Bentham) Greene" date="1885" rank="species">juncea</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="variety">aspera</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 180. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe galinsoginae;genus bebbia;species juncea;variety aspera</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250068086</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bebbia</taxon_name>
    <taxon_name authority="(Greene) A. Nelson" date="unknown" rank="species">aspera</taxon_name>
    <taxon_hierarchy>genus Bebbia;species aspera;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 5–65 × 1–6 (–12) mm, faces rough hairy to glabrate (hairs antrorse).</text>
      <biological_entity id="o5415" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s0" to="65" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s0" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5416" name="face" name_original="faces" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="rough" value_original="rough" />
        <character char_type="range_value" from="hairy" name="pubescence" src="d0_s0" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Phyllaries usually lanceolate, lance-linear, or lance-elliptic, sometimes narrowly ovate, 0.9–7.5 × 0.5–2 mm, outer usually less than 1.6 mm wide, apices acute to blunt.</text>
      <biological_entity id="o5417" name="phyllary" name_original="phyllaries" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s1" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="lance-elliptic" value_original="lance-elliptic" />
        <character is_modifier="false" modifier="sometimes narrowly" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s1" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o5418" name="phyllary" name_original="phyllaries" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s1" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5419" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s1" to="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Corollas 4.5–9 mm.</text>
      <biological_entity id="o5420" name="corolla" name_original="corollas" src="d0_s2" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s2" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pappi 5–8 mm. 2n = 18.</text>
      <biological_entity id="o5421" name="pappus" name_original="pappi" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5422" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky and sandy soils, dry hillsides, slopes, desert washes, canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="dry hillsides" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="desert washes" />
        <character name="habitat" value="canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex., Tex., Utah; Mexico (Baja California, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>