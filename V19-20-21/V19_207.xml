<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">centaurea</taxon_name>
    <taxon_name authority="M. Bieberstein" date="1808" rank="species">depressa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Taur.-Caucas.</publication_title>
      <place_in_publication>2: 346. 1808</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus centaurea;species depressa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066297</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–60 cm.</text>
      <biological_entity id="o17984" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually several–many from base, spreading, ± openly branched distally, loosely gray-tomentose.</text>
      <biological_entity id="o17985" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="from base" constraintid="o17986" from="several" is_modifier="false" name="quantity" src="d0_s1" to="many" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="more or less openly; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <biological_entity id="o17986" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves ± loosely gray-tomentose;</text>
      <biological_entity id="o17987" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less loosely" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal and proximal cauline petiolate, blades oblong, 5–10 cm, margins entire or pinnatifid with terminal segment largest, apices obtuse;</text>
      <biological_entity constraint="basal and proximal cauline" id="o17988" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o17989" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17990" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character constraint="with terminal segment" constraintid="o17991" is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o17991" name="segment" name_original="segment" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid and distal cauline sessile, linear-lanceolate to oblong, blades usually not much smaller, entire, mucronate.</text>
      <biological_entity id="o17992" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o17993" name="whole-organism" name_original="" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="oblong" />
      </biological_entity>
      <biological_entity id="o17994" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually not much" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiant, borne singly, pedunculate.</text>
      <biological_entity id="o17995" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="radiant" value_original="radiant" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ovoid to campanulate, 15–20 mm.</text>
      <biological_entity id="o17996" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="campanulate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries: bodies green, ovate (outer) to oblong (inner), glabrous, margins and erect appendages silvery white to brown, scarious, fringed with slender teeth 1.5–2 mm.</text>
      <biological_entity id="o17997" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o17998" name="body" name_original="bodies" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="oblong" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17999" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="silvery white" name="coloration" src="d0_s7" to="brown" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character constraint="with slender teeth" constraintid="o18001" is_modifier="false" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o18000" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="silvery white" name="coloration" src="d0_s7" to="brown" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character constraint="with slender teeth" constraintid="o18001" is_modifier="false" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity constraint="slender" id="o18001" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 25–35;</text>
      <biological_entity id="o18002" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas of sterile florets spreading, dark blue, 25–30 mm, enlarged, those of fertile florets purple, ca. 15 mm.</text>
      <biological_entity id="o18003" name="corolla" name_original="corollas" src="d0_s9" type="structure" constraint="floret" constraint_original="floret; floret">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark blue" value_original="dark blue" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s9" to="30" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s9" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="15" value_original="15" />
      </biological_entity>
      <biological_entity id="o18004" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o18005" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o18003" id="r1619" name="part_of" negation="false" src="d0_s9" to="o18004" />
      <relation from="o18003" id="r1620" name="part_of" negation="false" src="d0_s9" to="o18005" />
    </statement>
    <statement id="d0_s10">
      <text>Cypselae brown, 4.5–6 mm, puberulent near attachment scar, otherwise glabrous;</text>
      <biological_entity id="o18006" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character constraint="near attachment scar" constraintid="o18007" is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="attachment" id="o18007" name="scar" name_original="scar" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pappi of outer series of unequal stiff bristles 2–8 mm, inner series of slender scales ca. 1.5 mm. 2n = 16 (Armenia).</text>
      <biological_entity id="o18008" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o18009" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18010" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="true" name="fragility" src="d0_s11" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18011" name="series" name_original="series" src="d0_s11" type="structure" />
      <biological_entity constraint="slender" id="o18012" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18013" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
      <relation from="o18008" id="r1621" name="consists_of" negation="false" src="d0_s11" to="o18009" />
      <relation from="o18009" id="r1622" name="part_of" negation="false" src="d0_s11" to="o18010" />
      <relation from="o18011" id="r1623" name="consist_of" negation="false" src="d0_s11" to="o18012" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed ground</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed ground" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Md., Nev.; sw, c Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="c Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Low cornflower</other_name>
  
</bio:treatment>