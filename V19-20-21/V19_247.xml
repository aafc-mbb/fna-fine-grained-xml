<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">205</other_info_on_meta>
    <other_info_on_meta type="illustration_page">205</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="Blume" date="1826" rank="genus">cyanthillium</taxon_name>
    <taxon_name authority="(Linnaeus) H. Robinson" date="1990" rank="species">cinereum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>103: 252. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus cyanthillium;species cinereum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242315954</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Conyza</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">cinerea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 862. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Conyza;species cinerea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vernonia</taxon_name>
    <taxon_name authority="(Linnaeus) Lessing" date="unknown" rank="species">cinerea</taxon_name>
    <taxon_hierarchy>genus Vernonia;species cinerea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 20–35 (–50+) × 12–25 (–30+) mm (including petioles).</text>
      <biological_entity id="o4297" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="50" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s0" to="35" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s0" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s0" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 3–10 (–20+) mm.</text>
      <biological_entity id="o4298" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bracts ± linear at proximal nodes, wanting distally.</text>
      <biological_entity id="o4299" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character constraint="at proximal nodes" constraintid="o4300" is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="distally" name="quantity" notes="" src="d0_s2" value="wanting" value_original="wanting" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4300" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 3–4 mm.</text>
      <biological_entity id="o4301" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 1.5–2 mm;</text>
      <biological_entity id="o4302" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi white, outer scales 0.1–0.3 mm, inner bristles 3–4 mm. 2n = 36.</text>
      <biological_entity id="o4303" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4304" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4305" name="bristle" name_original="bristles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4306" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; Mexico; West Indies; Central America; South America; Asia; Africa; Indian Ocean Islands; Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>