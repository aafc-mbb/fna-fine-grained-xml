<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/14 19:02:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="treatment_page">132</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Richard" date="1807" rank="genus">ACMELLA</taxon_name>
    <place_of_publication>
      <publication_title>in C. H. Persoon, Syn. Pl.</publication_title>
      <place_in_publication>2: 472. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus ACMELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">From a Singhalese name for a plant now known as Blainvillea acmella (Linnaeus) Philipson</other_info_on_name>
    <other_info_on_name type="fna_id">100279</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 10–20 (–30+) cm.</text>
      <biological_entity id="o400" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to erect, usually branched ± throughout.</text>
      <biological_entity id="o402" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually; more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate [± sessile];</text>
      <biological_entity id="o403" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (usually 3-nerved) ovate to rhombic or lanceolate [linear to filiform], bases ± cuneate, margins entire or toothed, faces sparsely pilose to strigillose, glabrescent.</text>
      <biological_entity id="o404" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="rhombic or lanceolate" />
      </biological_entity>
      <biological_entity id="o405" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o406" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o407" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparsely pilose" name="pubescence" src="d0_s5" to="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid [disciform], borne singly at tips of branches [corymbiform arrays].</text>
      <biological_entity id="o408" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o409" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o410" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <relation from="o408" id="r76" name="borne" negation="false" src="d0_s6" to="o409" />
      <relation from="o408" id="r77" name="part_of" negation="false" src="d0_s6" to="o410" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± hemispheric to ovoid, 3–6+ mm diam.</text>
      <biological_entity id="o411" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="less hemispheric" name="shape" src="d0_s7" to="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 8–15+ in 1–3 series (distinct, ovate to linear, subequal or outer longer).</text>
      <biological_entity id="o412" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o413" from="8" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o413" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic, paleate (paleae falling with fruit, ± navicular, membranous to scarious, each about equaling subtended floret).</text>
      <biological_entity id="o414" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 or 5–20+, pistillate, fertile;</text>
      <biological_entity id="o415" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="20" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow to orange [white or purplish] (laminae ovate to linear) [wanting].</text>
      <biological_entity id="o416" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 25–100 (–200+) bisexual, fertile;</text>
      <biological_entity id="o417" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="200" upper_restricted="false" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="100" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow [orange], tubes shorter than campanulate throats, lobes 4–5, deltate.</text>
      <biological_entity id="o418" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o419" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than campanulate throats" constraintid="o420" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o420" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o421" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 2–3-angled (peripheral) or strongly compressed, ellipsoid to obovoid (glabrous or ciliate on the 2–3 angles or ribs);</text>
      <biological_entity id="o422" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="strongly compressed ellipsoid" name="shape" src="d0_s14" to="obovoid" />
        <character char_type="range_value" from="strongly compressed ellipsoid" name="shape" src="d0_s14" to="obovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, or fragile, of 1–3 awnlike bristles.</text>
      <biological_entity id="o424" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="3" />
        <character is_modifier="true" name="shape" src="d0_s15" value="awnlike" value_original="awnlike" />
      </biological_entity>
      <relation from="o423" id="r78" name="consist_of" negation="false" src="d0_s15" to="o424" />
    </statement>
    <statement id="d0_s16">
      <text>x = 13.</text>
      <biological_entity id="o423" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s15" value="fragile" value_original="fragile" />
      </biological_entity>
      <biological_entity constraint="x" id="o425" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s United States, Mexico, West Indies, Central America, South America; introduced in Asia, Africa, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>292.</number>
  <discussion>Species 30 (2 in the flora).</discussion>
  <discussion>Acmella pilosa R. K. Jansen has been reported as introduced in Florida (http://www.plantatlas.usf.edu); it differs from A. repens mainly by its more densely pilose stems and leaves and more truncate to cordate (versus cuneate) leaf bases.</discussion>
  <references>
    <reference>Jansen, R. K. 1985. The systematics of Acmella (Asteraceae–Heliantheae). Syst. Bot. Monogr. 8: 1–115.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petioles 3–40+ mm; leaf blades ovate to lance-ovate, mostly 20–40(–100) × 10–35 mm, margins dentate to denticulate or entire</description>
      <determination>1 Acmella repens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petioles 2–4 mm; leaf blades lanceolate, mostly 12–40 × 3–10 mm, margins usually sinuate-dentate, rarely entire</description>
      <determination>2 Acmella pusilla</determination>
    </key_statement>
  </key>
</bio:treatment>