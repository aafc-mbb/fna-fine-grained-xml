<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">layia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="1849" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 103. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus layia;species fremontii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067062</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calliachyris</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>5: 110. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Calliachyris;species fremontii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 8–40 cm (self-incompatible);</text>
    </statement>
    <statement id="d0_s1">
      <text>not glandular, not strongly scented.</text>
      <biological_entity id="o238" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="not strongly" name="odor" src="d0_s1" value="scented" value_original="scented" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not purple-streaked.</text>
      <biological_entity id="o239" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="purple-streaked" value_original="purple-streaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades lanceolate or oblanceolate to linear, 6–70 (–90) mm, margins (basal leaves) lobed (pinnatifid).</text>
      <biological_entity id="o240" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="distance" src="d0_s3" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o241" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric to depressed-hemispheric, 4–11 × 3–11+ mm.</text>
      <biological_entity id="o242" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s4" to="depressed-hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="11" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 3–15, apices often longer (sometimes shorter) than folded bases.</text>
      <biological_entity id="o243" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
      <biological_entity id="o244" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character constraint="than folded bases" constraintid="o245" is_modifier="false" name="length_or_size" src="d0_s5" value="often longer" value_original="often longer" />
      </biological_entity>
      <biological_entity id="o245" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae subtending ± all disc-florets.</text>
      <biological_entity id="o246" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s6" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o247" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 3–15;</text>
      <biological_entity id="o248" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae proximally yellow, distally white or light yellow, 5–18 (–23) mm.</text>
      <biological_entity id="o249" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="light yellow" value_original="light yellow" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="23" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 4–100+;</text>
      <biological_entity id="o250" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3.5–4.5 mm;</text>
      <biological_entity id="o251" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers ± dark purple.</text>
      <biological_entity id="o252" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae glabrous.</text>
      <biological_entity constraint="ray" id="o253" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc pappi of 9–12 white to tawny, lance-attenuate, ± equal scales 2–5 mm, each neither plumose nor adaxially woolly.</text>
      <biological_entity constraint="disc" id="o254" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" modifier="of" name="quantity" src="d0_s13" to="12" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="tawny" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lance-attenuate" value_original="lance-attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o255" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="plumose" value_original="plumose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s13" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o256" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, meadows, open woodlands, disturbed sites, often valley bottoms, swales, edges of vernal pools, usually on heavy or shallow soils, sometimes serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="valley bottoms" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="edges" constraint="of vernal pools" />
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="heavy" />
        <character name="habitat" value="shallow soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Layia fremontii occurs in the Great Valley and adjacent foothills of the Cascade Range and Sierra Nevada.</discussion>
  
</bio:treatment>