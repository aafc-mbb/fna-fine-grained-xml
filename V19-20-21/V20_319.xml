<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(G. Don) G. L. Nesom" date="1993" rank="subsection">Venosae</taxon_name>
    <taxon_name authority="unknown" date="2003" rank="series">odorae</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">odora</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>3: 214. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection venosae;series odorae;species odora;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242417291</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Aiton) Kuntze" date="unknown" rank="species">odorus</taxon_name>
    <taxon_hierarchy>genus Aster;species odorus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">odora</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">inodora</taxon_name>
    <taxon_hierarchy>genus Solidago;species odora;variety inodora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–120 cm;</text>
      <biological_entity id="o25637" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices short, stout.</text>
      <biological_entity id="o25638" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, erect to arching, puberulent in arrays and in lines proximal to leaf-bases or uniformly.</text>
      <biological_entity id="o25639" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="arching" />
        <character constraint="in arrays and in lines" constraintid="o25640" is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o25640" name="line" name_original="lines" src="d0_s2" type="structure">
        <character constraint="to leaf-bases" constraintid="o25641" is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o25641" name="leaf-base" name_original="leaf-bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually anise-scented when crushed;</text>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal usually withering by flowering, tapering to broadly winged petioles, blades oblanceolate, margins entire, short-strigillose, faces glabrous or short scabroso-strigillose along main nerves;</text>
      <biological_entity id="o25642" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when crushed" name="odor" src="d0_s3" value="anise-scented" value_original="anise-scented" />
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o25643" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="usually" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" modifier="broadly" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short-strigillose" value_original="short-strigillose" />
      </biological_entity>
      <biological_entity id="o25644" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" modifier="broadly" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25645" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" modifier="broadly" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="main" id="o25647" name="nerve" name_original="nerves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>mid and distal cauline sessile, blades lanceolate to linear-lanceolate or narrowly ovate, 30–110 × 8–20 mm, much reduced distally, bases rounded, margins entire, midnerves prominent, sometimes scabroso-strigillose basally to much of length, apices acute, faces glabrous, finely translucent gland-dotted.</text>
      <biological_entity id="o25646" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short" value_original="short" />
        <character constraint="along main nerves" constraintid="o25647" is_modifier="false" name="pubescence" src="d0_s4" value="scabroso-strigillose" value_original="scabroso-strigillose" />
        <character is_modifier="false" name="position" src="d0_s5" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o25648" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o25649" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear-lanceolate or narrowly ovate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s5" to="110" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="much; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o25650" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o25651" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25652" name="midnerve" name_original="midnerves" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="sometimes; basally to much" name="length" src="d0_s5" value="scabroso-strigillose" value_original="scabroso-strigillose" />
      </biological_entity>
      <biological_entity id="o25653" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="much" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o25654" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="coloration" src="d0_s5" value="translucent gland-dotted" value_original="translucent gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (20–) 75–350, in paniculiform arrays, openly secund, pyramidal, proximal to mid branches ascending to spreading, recurved, secund, 3–18 cm.</text>
      <biological_entity id="o25655" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s6" to="75" to_inclusive="false" />
        <character char_type="range_value" from="75" name="quantity" src="d0_s6" to="350" />
        <character is_modifier="false" modifier="openly" name="architecture" notes="" src="d0_s6" value="secund" value_original="secund" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
      <biological_entity id="o25656" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o25657" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="secund" value_original="secund" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="18" to_unit="cm" />
      </biological_entity>
      <relation from="o25655" id="r2375" name="in" negation="false" src="d0_s6" to="o25656" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles thin, 2–8 mm, glabrate to finely puberulent, glabrous strips proximal to few linear-lanceolate bracteoles.</text>
      <biological_entity id="o25658" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s7" to="finely puberulent" />
      </biological_entity>
      <biological_entity id="o25659" name="strip" name_original="strips" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o25660" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="true" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly campanulate, 3.5–5 mm.</text>
      <biological_entity id="o25661" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–4 series, strongly unequal, yellowish, acute, glabrous;</text>
      <biological_entity id="o25662" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25663" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <relation from="o25662" id="r2376" name="in" negation="false" src="d0_s9" to="o25663" />
    </statement>
    <statement id="d0_s10">
      <text>outer narrowly ovate to lanceolate, inner lanceolate to linear-lanceolate.</text>
      <biological_entity constraint="outer" id="o25664" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o25665" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 3–4 (–6);</text>
      <biological_entity id="o25666" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 1.4–2.5 × 0.4–0.9 mm.</text>
      <biological_entity id="o25667" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 3–5;</text>
      <biological_entity id="o25668" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2.7–3.5 mm, lobes 0.5–1.3 mm.</text>
      <biological_entity id="o25669" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25670" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (obconic) 1.4–2.3 mm, strigose to glabrate;</text>
      <biological_entity id="o25671" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s15" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 2.4–3 mm.</text>
      <biological_entity id="o25672" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., Fla., Ga., Ky., La., Mass., Md., Miss., Mo., N.C., N.H., N.J., N.Y., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., Vt., W.Va.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="common_name">Anise-scented or fragrant or sweet goldenrod</other_name>
  <discussion>subspecies 2 (2 in the flora)</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems puberulent in lines or strips decurrent from distal leaf bases (at least); mid cauline leaves mostly 40–110 × 5–15(–20) mm (4–15 times as long as wide); most of range except c, s peninsular Florida</description>
      <determination>54a Solidago odora subsp. odora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems uniformly puberulent, sometimes with short glabrous-glabrate strips proximal to leaf bases; mid cauline leaves mostly (15–)30–70 × 8–20 mm (2–6 times as long as wide); Florida, mostly peninsular, seldom in e panhandle</description>
      <determination>54b Solidago odora subsp. chapmanii</determination>
    </key_statement>
  </key>
</bio:treatment>