<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="species">heterophyllus</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 74. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species heterophyllus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066885</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthus</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">elongatus</taxon_name>
    <taxon_hierarchy>genus Helianthus;species elongatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 50–120 cm (with crown buds).</text>
      <biological_entity id="o13636" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually hispid to ± hirsute.</text>
      <biological_entity id="o13637" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="usually hispid" name="pubescence" src="d0_s1" to="more or less hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o13639" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mostly opposite;</text>
      <biological_entity id="o13638" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles 0–3 cm (broadly winged);</text>
      <biological_entity id="o13640" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (3-nerved distal to bases) ovate or lanceolate to spatulate, 6–28 × 1.2–4.3 cm, bases cuneate, margins entire (often revolute), abaxial faces hispid to ± hirsute, not glanddotted (cauline leaves relatively few, narrowly lanceolate to linear, much smaller).</text>
      <biological_entity id="o13641" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="spatulate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="28" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s5" to="4.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13642" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13643" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13644" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s5" to="more or less hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–3 (–5).</text>
      <biological_entity id="o13645" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 10–15 cm.</text>
      <biological_entity id="o13646" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres broadly hemispheric, 15–25 mm diam.</text>
      <biological_entity id="o13647" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 24–30, lanceolate to lanceovate, 8–13 × 2–5 mm, (margins sometimes ciliate) apices acute to short-acuminate or acuminate, abaxial faces sparsely hispid to glabrate.</text>
      <biological_entity id="o13648" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="24" name="quantity" src="d0_s9" to="30" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="lanceovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13649" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="short-acuminate or acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13650" name="face" name_original="faces" src="d0_s9" type="structure">
        <character char_type="range_value" from="sparsely hispid" name="pubescence" src="d0_s9" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 7–9 mm, 3-toothed (apices purplish).</text>
      <biological_entity id="o13651" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 12–18;</text>
      <biological_entity id="o13652" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 14–36 mm (abaxial faces not glanddotted).</text>
      <biological_entity id="o13653" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s12" to="36" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 100+;</text>
      <biological_entity id="o13654" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 5.5–6.5 mm, lobes reddish;</text>
      <biological_entity id="o13655" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13656" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers purplish, apendages purplish (style-branches usually reddish).</text>
      <biological_entity id="o13657" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 4–5 mm, glabrate;</text>
      <biological_entity id="o13658" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 aristate scales 1.7–2.5 mm plus 1–3 deltate scales 0.5–1.5 mm. 2n = 34.</text>
      <biological_entity id="o13659" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="3" />
      </biological_entity>
      <biological_entity id="o13660" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13661" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13662" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
      </biological_entity>
      <relation from="o13659" id="r936" name="consist_of" negation="false" src="d0_s17" to="o13660" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" modifier="wet" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Wetland sunflower</other_name>
  <discussion>Helianthus heterophyllus is found on the Atlantic and Gulf coastal plains. The cauline leaves are usually abruptly reduced relative to the basal leaves; individuals sometimes have relatively large cauline leaves.</discussion>
  
</bio:treatment>