<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="treatment_page">525</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">artemisia</taxon_name>
    <taxon_name authority="Chamisso ex Besser" date="1833" rank="species">globularia</taxon_name>
    <place_of_publication>
      <publication_title>Nouv. Mém. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>3: 64. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus artemisia;species globularia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066148</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ajania</taxon_name>
    <taxon_name authority="(Besser) Poljakov" date="unknown" rank="species">globularia</taxon_name>
    <taxon_hierarchy>genus Ajania;species globularia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Fries" date="unknown" rank="species">norvegica</taxon_name>
    <taxon_name authority="(Besser) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">globularia</taxon_name>
    <taxon_hierarchy>genus Artemisia;species norvegica;subspecies globularia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (3–) 5–16 (–30) cm (cespitose), faintly aromatic (not rhizomatous, taproots stout, caudices simple or branched, proximal branches clothed with persistent leaf-bases).</text>
      <biological_entity id="o16375" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="16" to_unit="cm" />
        <character is_modifier="false" modifier="faintly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5, erect, whitish gray, densely tomentose.</text>
      <biological_entity id="o16376" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish gray" value_original="whitish gray" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal (cauline 1–4), greenish to whitish green;</text>
      <biological_entity id="o16377" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" notes="" src="d0_s2" to="whitish green" />
      </biological_entity>
      <biological_entity id="o16378" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (basal) 1–4.5 × 0.6–1.5 cm, 1–2-ternately to palmately lobed (flowering-stem blades 3-lobed), faces sparsely hairy.</text>
      <biological_entity id="o16379" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="1-2-ternately to palmately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o16380" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (2–20, peduncles 0 or to 25 mm) in subcapitate to capitate arrays 2–3 × 2–3 cm.</text>
      <biological_entity id="o16381" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="subcapitate" name="architecture_or_shape" src="d0_s4" to="capitate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate or hemispheric, 3.5–6 × 6–11 mm.</text>
      <biological_entity id="o16382" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries lanceolate (margins brown), pilose.</text>
      <biological_entity id="o16383" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 9–10;</text>
      <biological_entity id="o16384" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual 20–30;</text>
      <biological_entity id="o16385" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow or reddish black, 2–3 mm, sometimes glandular.</text>
      <biological_entity id="o16386" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o16387" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish black" value_original="reddish black" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae oblong, 1.5–2.5 mm, (apices flattened) glabrous.</text>
      <biological_entity id="o16388" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas reddish black; cypselae ca. 2.5 mm, margins with relatively narrow ribs</description>
      <determination>33a Artemisia globularia subsp. globularia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas yellow; cypselae 1.5–2 mm, margins with relatively broad ribs</description>
      <determination>33b Artemisia globularia subsp. lutea</determination>
    </key_statement>
  </key>
</bio:treatment>