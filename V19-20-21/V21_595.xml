<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">242</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">thymophylla</taxon_name>
    <taxon_name authority="(de Candolle) Small" date="1903" rank="species">pentachaeta</taxon_name>
    <taxon_name authority="(Rydberg) Strother" date="1986" rank="variety">puberula</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>11: 377. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus thymophylla;species pentachaeta;variety puberula</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068883</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thymophylla</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">puberula</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 177. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Thymophylla;species puberula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="(Rydberg) Standley" date="unknown" rank="species">puberula</taxon_name>
    <taxon_hierarchy>genus Dyssodia;species puberula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="(de Candolle) B. L. Robinson" date="unknown" rank="species">pentachaeta</taxon_name>
    <taxon_name authority="(Rydberg) Strother" date="unknown" rank="variety">puberula</taxon_name>
    <taxon_hierarchy>genus Dyssodia;species pentachaeta;variety puberula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 10–28 mm overall, lobes 3–7 (–10), subequal.</text>
      <biological_entity id="o2812" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s0" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2813" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s0" to="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s0" to="7" />
        <character is_modifier="false" name="size" src="d0_s0" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 40–100 mm.</text>
      <biological_entity id="o2814" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s1" to="100" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Calyculi 0, or of 1–3 bractlets.</text>
      <biological_entity id="o2815" name="calyculus" name_original="calyculi" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2816" name="bractlet" name_original="bractlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <relation from="o2815" id="r211" name="consist_of" negation="false" src="d0_s2" to="o2816" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres campanulate to hemispheric, 4.5–5.5 × 4–5 mm.</text>
      <biological_entity id="o2817" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s3" to="hemispheric" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s3" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 12–17, margins of outer distinct ca. 1/3 their lengths, abaxial faces densely puberulent.</text>
      <biological_entity id="o2818" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s4" to="17" />
      </biological_entity>
      <biological_entity id="o2819" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character name="lengths" src="d0_s4" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2820" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o2821" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o2819" id="r212" name="part_of" negation="false" src="d0_s4" to="o2820" />
    </statement>
    <statement id="d0_s5">
      <text>Disc-florets 50–70.</text>
      <biological_entity id="o2822" name="disc-floret" name_original="disc-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s5" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pappi of 5 erose scales alternating with 5, 1–3-aristate scales.</text>
      <biological_entity id="o2823" name="pappus" name_original="pappi" src="d0_s6" type="structure" />
      <biological_entity id="o2824" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
        <character constraint="with scales" constraintid="o2825" is_modifier="false" name="arrangement" src="d0_s6" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o2825" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="true" name="shape" src="d0_s6" value="1-3-aristate" value_original="1-3-aristate" />
      </biological_entity>
      <relation from="o2823" id="r213" name="consist_of" negation="false" src="d0_s6" to="o2824" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring and late summer, following rains.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="following rains" to="late summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous bluffs and slopes, scrublands, deserts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous bluffs" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="scrublands" />
        <character name="habitat" value="deserts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5c.</number>
  
</bio:treatment>