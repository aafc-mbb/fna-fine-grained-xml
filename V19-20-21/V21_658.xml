<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="treatment_page">268</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">layia</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">gaillardioides</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 294. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus layia;species gaillardioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220007359</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tridax</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">gaillardioides</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>148. 1833 (as galardioides)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tridax;species gaillardioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 6–60 cm (self-incompatible);</text>
    </statement>
    <statement id="d0_s1">
      <text>not glandular, often strongly scented.</text>
      <biological_entity id="o20901" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="often strongly" name="odor" src="d0_s1" value="scented" value_original="scented" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems purple-streaked.</text>
      <biological_entity id="o20902" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple-streaked" value_original="purple-streaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades oblanceolate or lanceolate to linear, 5–120 mm, margins (basal leaves) toothed to pinnatifid.</text>
      <biological_entity id="o20903" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="distance" src="d0_s3" to="120" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20904" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="toothed" name="shape" src="d0_s3" to="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres subglobose to ± campanulate or hemispheric, 4–9+ × 4–12+ mm.</text>
      <biological_entity id="o20905" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s4" to="more or less campanulate or hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="9" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 6–18, apices shorter than folded bases.</text>
      <biological_entity id="o20906" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="18" />
      </biological_entity>
      <biological_entity id="o20907" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character constraint="than folded bases" constraintid="o20908" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o20908" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series between ray and disc-florets.</text>
      <biological_entity id="o20909" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity constraint="between ray and disc-floret" constraintid="o20911-o20912" id="o20910" name="series" name_original="series" src="d0_s6" type="structure" constraint_original="between  ray and  disc-floret, ">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o20911" name="ray" name_original="ray" src="d0_s6" type="structure" />
      <biological_entity id="o20912" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure" />
      <relation from="o20909" id="r1438" name="in" negation="false" src="d0_s6" to="o20910" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 6–18;</text>
      <biological_entity id="o20913" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae yellow or bicolored (proximally yellow, distally whitish or pale-yellow), 3.5–18 mm.</text>
      <biological_entity id="o20914" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="bicolored" value_original="bicolored" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 14–100+;</text>
      <biological_entity id="o20915" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3–5 mm;</text>
      <biological_entity id="o20916" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers ± dark purple.</text>
      <biological_entity id="o20917" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae glabrous.</text>
      <biological_entity constraint="ray" id="o20918" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc pappi 0 or of 15–24 white to rufous or purplish, ± equal bristles or setiform scales 1–4 mm, each proximally plumose, not adaxially woolly.</text>
      <biological_entity constraint="disc" id="o20919" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s13" to="24" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="rufous or purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o20920" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s13" value="plumose" value_original="plumose" />
        <character is_modifier="false" modifier="not adaxially" name="pubescence" src="d0_s13" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o20921" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character is_modifier="true" name="shape" src="d0_s13" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s13" value="plumose" value_original="plumose" />
        <character is_modifier="false" modifier="not adaxially" name="pubescence" src="d0_s13" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20922" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open or semishady sites in woodlands, grasslands, meadows, chaparral, or forests, often on serpentine or sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" constraint="in woodlands , grasslands , meadows , chaparral , or forests" />
        <character name="habitat" value="semishady sites" constraint="in woodlands , grasslands , meadows , chaparral , or forests" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="serpentine" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Layia gaillardioides occurs on the North Coast and in the North Coast Ranges, San Francisco Bay area, and Inner South Coast Ranges, and in the Outer South Coast Ranges (near Cayucos). As treated here (provisionally) and previously, L. gaillardioides is not monophyletic; molecular phylogenetic data have indicated that some lineages of L. gaillardioides are more closely related to L. carnosa, L. hieracioides, and/or L. septentrionalis than to one another (B. G. Baldwin, unpubl.).</discussion>
  
</bio:treatment>