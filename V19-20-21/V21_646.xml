<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">layia</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray" date="1868" rank="species">chrysanthemoides</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 360. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus layia;species chrysanthemoides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067060</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxyura</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">chrysanthemoides</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 693. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Oxyura;species chrysanthemoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Layia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">chrysanthemoides</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="subspecies">maritima</taxon_name>
    <taxon_hierarchy>genus Layia;species chrysanthemoides;subspecies maritima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–53 cm (self-incompatible);</text>
    </statement>
    <statement id="d0_s1">
      <text>not glandular, not strongly scented.</text>
      <biological_entity id="o21003" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="53" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="not strongly" name="odor" src="d0_s1" value="scented" value_original="scented" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not purple-streaked.</text>
      <biological_entity id="o21004" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="purple-streaked" value_original="purple-streaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades lanceolate or oblanceolate to linear, 5–120 mm, margins (basal leaves) lobed to pinnatifid.</text>
      <biological_entity id="o21005" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="distance" src="d0_s3" to="120" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21006" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="lobed" name="shape" src="d0_s3" to="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric or depressed-hemispheric, 4–12 × 4–14+ mm.</text>
      <biological_entity id="o21007" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s4" value="depressed-hemispheric" value_original="depressed-hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 6–16, apices often longer (sometimes shorter) than folded bases.</text>
      <biological_entity id="o21008" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="16" />
      </biological_entity>
      <biological_entity id="o21009" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character constraint="than folded bases" constraintid="o21010" is_modifier="false" name="length_or_size" src="d0_s5" value="often longer" value_original="often longer" />
      </biological_entity>
      <biological_entity id="o21010" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae subtending ± all disc-florets.</text>
      <biological_entity id="o21011" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s6" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o21012" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 6–16;</text>
      <biological_entity id="o21013" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae usually proximally yellow, distally white or light yellow, rarely uniformly yellow throughout, 3–18 (–24) mm.</text>
      <biological_entity id="o21014" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually proximally" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="light yellow" value_original="light yellow" />
        <character is_modifier="false" modifier="rarely uniformly; throughout" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="24" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 28–100+;</text>
      <biological_entity id="o21015" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="28" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3–5 mm;</text>
      <biological_entity id="o21016" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers ± dark purple.</text>
      <biological_entity id="o21017" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae glabrous.</text>
      <biological_entity constraint="ray" id="o21018" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc pappi 0 or of 2–18 tawny, subulate to setiform, unequal scales 1–4 mm, each ± scabrous, not adaxially woolly.</text>
      <biological_entity id="o21020" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s13" to="18" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="subulate" is_modifier="true" name="shape" src="d0_s13" to="setiform" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity constraint="disc" id="o21019" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character constraint="of scales" constraintid="o21020" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 2" is_modifier="false" name="quantity" src="d0_s13" to="18 tawny , subulate to setiform , unequal scales" />
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" notes="" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="not adaxially" name="pubescence" src="d0_s13" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21021" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, open woodlands, often valley bottoms, disturbed sites, edges of vernal pools, waterways, and salt marshes, usually on heavy soils, sometimes ± alkaline or saline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="valley bottoms" modifier="often" />
        <character name="habitat" value="sites" />
        <character name="habitat" value="edges" constraint="of vernal pools , waterways , and salt marshes" />
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="waterways" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="heavy soils" />
        <character name="habitat" value="saline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Layia chrysanthemoides occurs from the western Great Valley to the coast in northern and central California. Molecular and morphologic data have indicated that L. chrysanthemoides is most closely related to L. fremontii (B. G. Baldwin, unpubl.); the two species are reportedly highly interfertile (natural hybrids have not been reported; J. Clausen 1951).</discussion>
  
</bio:treatment>