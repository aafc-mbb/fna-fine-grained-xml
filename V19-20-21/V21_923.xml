<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arnica</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">fulgens</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 527. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus arnica;species fulgens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066113</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arnica</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">monocephala</taxon_name>
    <taxon_hierarchy>genus Arnica;species monocephala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arnica</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">pedunculata</taxon_name>
    <taxon_hierarchy>genus Arnica;species pedunculata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–75 cm.</text>
      <biological_entity id="o91" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems solitary, simple.</text>
      <biological_entity id="o92" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–5 pairs, crowded toward stem-bases (some or all axils with dense tufts of brown wool);</text>
      <biological_entity id="o94" name="stem-base" name_original="stem-bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiolate (petioles at least basal leaves, narrow or broadly winged);</text>
      <biological_entity id="o93" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
        <character constraint="toward stem-bases" constraintid="o94" is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (with 3 or 5 prominent, subparallel veins) usually narrowly oblanceolate to oblong, rarely oval or broadly spatulate, 4.5–20 × 0.5–2.5 cm, margins denticulate, apices obtuse, faces moderately uniformly hairy, stipitate-glandular.</text>
      <biological_entity id="o95" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually narrowly oblanceolate" name="shape" src="d0_s4" to="oblong rarely oval or broadly spatulate" />
        <character char_type="range_value" from="usually narrowly oblanceolate" name="shape" src="d0_s4" to="oblong rarely oval or broadly spatulate" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o96" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o97" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o98" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately uniformly" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–3).</text>
      <biological_entity id="o99" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres broadly hemispheric.</text>
      <biological_entity id="o100" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 13–21, elliptic-oblong or narrowly to broadly lanceolate.</text>
      <biological_entity id="o101" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="21" />
        <character char_type="range_value" from="elliptic-oblong or" name="shape" src="d0_s7" to="narrowly broadly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 8–16;</text>
      <biological_entity id="o102" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow-orange.</text>
      <biological_entity id="o103" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow-orange" value_original="yellow-orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets: corollas yellow;</text>
      <biological_entity id="o104" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure" />
      <biological_entity id="o105" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow.</text>
      <biological_entity id="o106" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure" />
      <biological_entity id="o107" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae brown, 3.5–7 mm, densely hirsute, sometimes sparingly stipitate-glandular;</text>
      <biological_entity id="o108" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sometimes sparingly" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi usually white, sometimes tawny, bristles barbellate.</text>
      <biological_entity id="o109" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="tawny" value_original="tawny" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 38, 57.</text>
      <biological_entity id="o110" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o111" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="38" value_original="38" />
        <character name="quantity" src="d0_s14" value="57" value_original="57" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies and grasslands to montane conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="montane conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Sask.; Calif., Colo., Idaho, Mont., Nev., N.Dak., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Foothill arnica</other_name>
  
</bio:treatment>