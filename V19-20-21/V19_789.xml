<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert E. Preston</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="treatment_page">471</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">inuleae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">PULICARIA</taxon_name>
    <place_of_publication>
      <publication_title>Fruct. Sem. Pl.</publication_title>
      <place_in_publication>2: 461, plate 173, fig. 7. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe inuleae;genus PULICARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin pulex, flea, and -aria, pertaining to; alluding to use of the plants as flea repellent</other_info_on_name>
    <other_info_on_name type="fna_id">127631</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (biennials, or perennials) [shrubs, subshrubs], (5–) 20–120 cm (sometimes rhizomatous).</text>
      <biological_entity id="o18874" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and/or cauline (mostly cauline at flowering), alternate;</text>
    </statement>
    <statement id="d0_s2">
      <text>usually sessile;</text>
      <biological_entity id="o18875" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade margins entire or ± dentate to serrate.</text>
      <biological_entity constraint="blade" id="o18876" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads radiate [disciform or discoid], in corymbiform, racemiform, or paniculiform arrays.</text>
      <biological_entity id="o18877" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o18878" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o18877" id="r1700" name="in" negation="false" src="d0_s4" to="o18878" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres hemispheric to campanulate, [3–] 5–10 [–20+] mm diam.</text>
      <biological_entity id="o18879" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s5" to="20" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries persistent (reflexed in fruit), in (2–) 3–4+ series, unequal to subequal.</text>
      <biological_entity id="o18880" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o18881" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s6" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="4" upper_restricted="false" />
        <character char_type="range_value" from="unequal" name="size" notes="" src="d0_s6" to="subequal" />
      </biological_entity>
      <relation from="o18880" id="r1701" name="in" negation="false" src="d0_s6" to="o18881" />
    </statement>
    <statement id="d0_s7">
      <text>Receptacles flat, smooth or minutely alveolate, epaleate.</text>
      <biological_entity id="o18882" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s7" value="alveolate" value_original="alveolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets (10–) 20–30 [–60+], pistillate, fertile;</text>
      <biological_entity id="o18883" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s8" to="20" to_inclusive="false" />
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="60" upper_restricted="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, laminae 1.5–2+ mm.</text>
      <biological_entity id="o18884" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o18885" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets (9–) 40–100 [–150+];</text>
      <biological_entity id="o18886" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s10" to="40" to_inclusive="false" />
        <character char_type="range_value" from="100" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="150" upper_restricted="false" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s10" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, lobes 5.</text>
      <biological_entity id="o18887" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o18888" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae ellipsoid (abruptly constricted distally; often glandular distally);</text>
      <biological_entity id="o18889" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi persistent, outer of basally connate, ± erose scales (usually forming cups), inner of distinct (fragile), barbellate or flattened bristles.</text>
      <biological_entity id="o18890" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18891" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o18892" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="true" modifier="more or less" name="architecture_or_relief" src="d0_s13" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o18894" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o18891" id="r1702" name="consists_of" negation="false" src="d0_s13" to="o18892" />
      <relation from="o18893" id="r1703" name="consists_of" negation="false" src="d0_s13" to="o18894" />
    </statement>
    <statement id="d0_s14">
      <text>x = 7, 9, 10.</text>
      <biological_entity constraint="inner" id="o18893" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="x" id="o18895" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="7" value_original="7" />
        <character name="quantity" src="d0_s14" value="9" value_original="9" />
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>106.</number>
  <other_name type="common_name">False fleabane</other_name>
  <discussion>Species 100+ (1 in the flora).</discussion>
  <discussion>Pulicaria arabica (Linnaeus) Cassini (Vicoa auriculata Cassini) was collected in Alabama, California, and Florida in the late 1800s. It does not appear to have become naturalized at any of those locations (A. Cronquist 1980; J. E. Arriagada 1998).</discussion>
  <discussion>Pulicaria dysenterica (Linnaeus) Bernhardi was collected in the late 1800s as a ballast weed in New Jersey and in Pennsylvania. It was collected in the 1920s growing on the margins of a marsh in Maryland. Although it is widely cultivated for its insecticidal properties, there is no evidence that it has ever become established in the flora (J. E. Arriagada 1998).</discussion>
  <discussion>Pulicaria vulgaris Gaertner was collected as a ballast weed in New Jersey in 1879. No other collections of the species from North America are known to me.</discussion>
  
</bio:treatment>