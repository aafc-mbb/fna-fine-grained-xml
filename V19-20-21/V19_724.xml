<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="mention_page">439</other_info_on_meta>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1828" rank="genus">OMALOTHECA</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 56: 218. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus OMALOTHECA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek omalo, even or equal, and theke, container, envelope, or sheath, perhaps alluding to involucres</other_info_on_name>
    <other_info_on_name type="fna_id">122848</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2–70 cm (fibrous-rooted, rhizomatous, not stoloniferous).</text>
      <biological_entity id="o17317" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect (branched from bases or distally, woolly-tomentose to sericeous).</text>
      <biological_entity id="o17318" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal (persistent in rosettes) and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o17319" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly narrowly lanceolate to oblanceolate, bases cuneate, margins entire, faces bicolor or concolor, abaxial white to gray, thinly tomentose, adaxial white to grayish and sericeous to thinly woolly or greenish and glabrate.</text>
      <biological_entity id="o17320" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="mostly narrowly lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o17321" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o17322" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17323" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="concolor" value_original="concolor" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17324" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s5" to="gray" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17325" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s5" to="grayish" />
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s5" to="thinly woolly" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads disciform, in spiciform or subcapitate arrays.</text>
      <biological_entity id="o17326" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o17327" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="subcapitate" value_original="subcapitate" />
      </biological_entity>
      <relation from="o17326" id="r1553" name="in" negation="false" src="d0_s6" to="o17327" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate to turbinate, 5–6 mm.</text>
      <biological_entity id="o17328" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="turbinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2–3 series, stramineous to brownish (sometimes mottled; hyaline, stereomes not glandular), unequal, chartaceous toward apices.</text>
      <biological_entity id="o17329" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" notes="" src="d0_s8" to="brownish" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character constraint="toward apices" constraintid="o17331" is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o17330" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity id="o17331" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <relation from="o17329" id="r1554" name="in" negation="false" src="d0_s8" to="o17330" />
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to concave, smooth, epaleate.</text>
      <biological_entity id="o17332" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="concave" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peripheral (pistillate) florets 35–70+ (more numerous than bisexual);</text>
      <biological_entity constraint="peripheral" id="o17333" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s10" to="70" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas purplish or whitish.</text>
      <biological_entity id="o17334" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Inner (bisexual) florets 3–4;</text>
      <biological_entity constraint="inner" id="o17335" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas purplish or whitish, distally purplish or reddish.</text>
      <biological_entity id="o17336" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae obovoid to cylindric or fusiform, sometimes slightly compressed, faces strigillose (hairs not myxogenic, lengths 6–12 times diams.) and papillate (carpopodia forming minute stipes);</text>
      <biological_entity id="o17337" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s14" to="cylindric or fusiform" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o17338" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi falling readily, of 15–25 distinct (falling separately) or basally connate (falling together), barbellate bristles in 1 series.</text>
      <biological_entity id="o17340" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s15" to="25" />
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o17341" name="series" name_original="series" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17339" id="r1555" name="consist_of" negation="false" src="d0_s15" to="o17340" />
      <relation from="o17340" id="r1556" name="in" negation="false" src="d0_s15" to="o17341" />
    </statement>
    <statement id="d0_s16">
      <text>x = 14.</text>
      <biological_entity id="o17339" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o17342" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly Eurasian; three species reaching North America in native distribution (Omalotheca sylvatica perhaps not native, see below).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly Eurasian" establishment_means="native" />
        <character name="distribution" value="three species reaching North America in native distribution (Omalotheca sylvatica perhaps not native)" establishment_means="native" />
        <character name="distribution" value="three species reaching North America in native distribution (see below)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>94.</number>
  <discussion>Species 8–10 (3 in the flora).</discussion>
  <discussion>The species of Omalotheca have been placed in subg. Omalotheca (capitulescences of 1–10 heads, cypselae compressed-obovoid, and pappus bristles distinct and falling separately) and subg. Gamochaetiopsis Schultz-Bipontinus &amp; F. W. Schultz (capitulescences of 10–100 heads, cypselae cylindric, and pappus bristles basally connate and falling together). In the flora, O. norvegica and O. sylvatica belong in subg. Gamochaetiopsis; O. supina is in subg. Omalotheca.</discussion>
  <references>
    <reference>Nesom, G. L. 1990b. Taxonomic summary of Omalotheca (Asteraceae: Inuleae). Phytologia 68: 241–246.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 2–8(–12) cm; heads in subcapitate to loose, spiciform arrays; pappus bristles distinct, falling separately</description>
      <determination>1 Omalotheca supina</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 10–70 cm; heads in compact or loose, spiciform arrays; pappus bristles basally connate, falling together</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades: basal and proximal cauline 3-nerved, 6–30 mm wide, distal cauline oblanceolate, faces concolor or weakly bicolor; arrays of heads 1.5–5 cm, rarely interrupted; alpine sites</description>
      <determination>2 Omalotheca norvegica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades: basal and proximal cauline 1-nerved, 2–10 mm wide, distal cauline linear, faces bicolor; arrays of heads 4–35 cm, usually interrupted; lower elevations</description>
      <determination>3 Omalotheca sylvatica</determination>
    </key_statement>
  </key>
</bio:treatment>