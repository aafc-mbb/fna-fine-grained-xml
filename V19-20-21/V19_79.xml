<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="treatment_page">122</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Torrey ex Eaton) Torrey &amp; A. Gray" date="1843" rank="species">pitcheri</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 456. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species pitcheri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066392</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cnicus</taxon_name>
    <taxon_name authority="Torrey ex Eaton" date="unknown" rank="species">pitcheri</taxon_name>
    <place_of_publication>
      <publication_title>Man. Bot. ed.</publication_title>
      <place_in_publication>5, 180. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cnicus;species pitcheri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or short-lived monocarpic perennials, 20–100 cm;</text>
      <biological_entity id="o11420" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="true" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots long.</text>
      <biological_entity id="o11422" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 or few, erect, densely gray-tomentose;</text>
      <biological_entity id="o11423" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches 0 to several, ascending to spreading.</text>
      <biological_entity id="o11424" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades elliptic to obovate, 10–30 × 8–14 cm, deeply divided nearly to midveins, lobes ascending to spreading, linear, remote, margins revolute, entire or minutely spinulose, main spines 1–2 mm, faces gray-tomentose, more densely so abaxially;</text>
      <biological_entity id="o11425" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o11426" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s4" to="14" to_unit="cm" />
        <character constraint="to midveins" constraintid="o11427" is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o11427" name="midvein" name_original="midveins" src="d0_s4" type="structure" />
      <biological_entity id="o11428" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement_or_density" src="d0_s4" value="remote" value_original="remote" />
      </biological_entity>
      <biological_entity id="o11429" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity constraint="main" id="o11430" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11431" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal present or withered at flowering, petiolate;</text>
      <biological_entity id="o11432" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o11433" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character constraint="at flowering" is_modifier="false" name="life_cycle" src="d0_s5" value="withered" value_original="withered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline well distributed, bases decurrent as linear-lobed to spiny wings 1–3 cm;</text>
      <biological_entity id="o11434" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o11435" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity id="o11436" name="base" name_original="bases" src="d0_s6" type="structure">
        <character constraint="as wings" constraintid="o11437" is_modifier="false" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o11437" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-lobed" is_modifier="true" name="shape" src="d0_s6" to="spiny" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cauline well developed.</text>
      <biological_entity id="o11438" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal cauline" id="o11439" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s7" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–20+ in corymbiform arrays.</text>
      <biological_entity id="o11440" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o11441" from="1" name="quantity" src="d0_s8" to="20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o11441" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–5 cm.</text>
      <biological_entity id="o11442" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres ovoid to campanulate, 2–3 × 2–3 cm, loosely arachnoid on phyllary margins or glabrate.</text>
      <biological_entity id="o11443" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="3" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s10" to="3" to_unit="cm" />
        <character constraint="on phyllary margins" constraintid="o11444" is_modifier="false" modifier="loosely" name="pubescence" src="d0_s10" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="phyllary" id="o11444" name="margin" name_original="margins" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 6–8 series, imbricate, ovatelanceolate (outer) to linear-lanceolate (inner), abaxial faces with narrow glutinous ridge;</text>
      <biological_entity id="o11445" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s11" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o11446" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s11" to="8" />
      </biological_entity>
      <biological_entity id="o11448" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o11445" id="r1058" name="in" negation="false" src="d0_s11" to="o11446" />
      <relation from="o11447" id="r1059" name="with" negation="false" src="d0_s11" to="o11448" />
    </statement>
    <statement id="d0_s12">
      <text>outer and middle appressed, acute, spines ascending to spreading, slender, 1–2 (–3) mm;</text>
      <biological_entity constraint="abaxial" id="o11447" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o11449" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="middle" value_original="middle" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o11450" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s12" to="spreading" />
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner often flexuous, flattened, spineless, scabrid.</text>
      <biological_entity id="o11451" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="flexuou; flexuou">
        <character is_modifier="false" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="spineless" value_original="spineless" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrid" value_original="scabrid" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11452" name="flexuou" name_original="flexuous" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="often" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <relation from="o11451" id="r1060" name="part_of" negation="false" src="d0_s13" to="o11452" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas dull white or pinkish-tinged (rarely rich purple), 20–30 mm, tubes 8.5–15 mm, throats 4.5–10 mm, lobes 3–8 mm;</text>
      <biological_entity id="o11453" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pinkish-tinged" value_original="pinkish-tinged" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s14" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11454" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s14" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11455" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11456" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 3.5–5.5 mm.</text>
      <biological_entity constraint="style" id="o11457" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae light-brown, sometimes with darker streaks, 6–7.5 mm, apical collars lighter colored, very narrow;</text>
      <biological_entity id="o11458" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s16" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11459" name="streak" name_original="streaks" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity constraint="apical" id="o11460" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="lighter colored" value_original="lighter colored" />
        <character is_modifier="false" modifier="very" name="size_or_width" src="d0_s16" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o11458" id="r1061" modifier="sometimes" name="with" negation="false" src="d0_s16" to="o11459" />
    </statement>
    <statement id="d0_s17">
      <text>pappi 15–30 mm, usually noticeably shorter than corolla.</text>
      <biological_entity id="o11462" name="corolla" name_original="corolla" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 34.</text>
      <biological_entity id="o11461" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s17" to="30" to_unit="mm" />
        <character constraint="than corolla" constraintid="o11462" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="usually noticeably shorter" value_original="usually noticeably shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11463" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand dunes and beaches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="beaches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>180–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="180" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ill., Ind., Mich., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Dune or sand-dune thistle</other_name>
  <discussion>Cirsium pitcheri is endemic to beach and dune habitats around lakes Huron, Michigan, and Superior. It has been extirpated from portions of its former range at the southern end of Lake Michigan. It is threatened by foot traffic, off-road vehicular activity, and clearing and development of beachside habitats. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>