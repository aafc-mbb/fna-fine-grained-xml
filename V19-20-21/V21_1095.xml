<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">438</other_info_on_meta>
    <other_info_on_meta type="treatment_page">439</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1828" rank="genus">hymenoxys</taxon_name>
    <taxon_name authority="(S. F. Blake) Bierner" date="1994" rank="species">ambigens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">ambigens</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus hymenoxys;species ambigens;variety ambigens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068526</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–150 cm.</text>
      <biological_entity id="o12363" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Heads (50–) 100–400+.</text>
      <biological_entity id="o12364" name="head" name_original="heads" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s1" to="100" to_inclusive="false" />
        <character char_type="range_value" from="100" name="quantity" src="d0_s1" to="400" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries: outer 4, basally connate 1/2–2/3 their lengths, 4–5 mm, apices obtuse to acute;</text>
      <biological_entity id="o12365" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure" />
      <biological_entity constraint="outer" id="o12366" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="4" value_original="4" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s2" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/2" name="lengths" src="d0_s2" to="2/3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12367" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>inner 4, 3.2–4.5 mm, apices acuminate to mucronate.</text>
      <biological_entity id="o12368" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure" />
      <biological_entity constraint="inner" id="o12369" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="4" value_original="4" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12370" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s3" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 4;</text>
      <biological_entity id="o12371" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas 4–5.5 × 2.3–4 mm.</text>
      <biological_entity id="o12372" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 7–11.</text>
      <biological_entity id="o12373" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="11" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 2.5–3 mm, hairs straight to slightly wavy;</text>
      <biological_entity id="o12374" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12375" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi usually of 3–6 scales 1.2–2.5 mm, sometimes 0 on disc-florets.</text>
      <biological_entity id="o12377" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="6" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character constraint="on disc-florets" constraintid="o12378" modifier="sometimes" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12378" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure" />
      <relation from="o12376" id="r852" name="consist_of" negation="false" src="d0_s8" to="o12377" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 30.</text>
      <biological_entity id="o12376" name="pappus" name_original="pappi" src="d0_s8" type="structure" />
      <biological_entity constraint="2n" id="o12379" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety ambigens is known from the Mescal, Pinaleno, and Santa Teresa mountains.</discussion>
  
</bio:treatment>