<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="R. L. Hartman &amp; M. A. Lane" date="1996" rank="genus">rayjacksonia</taxon_name>
    <taxon_name authority="(Rydberg) R. L. Hartman &amp; M. A. Lane" date="1996" rank="species">annua</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>83: 368. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus rayjacksonia;species annua</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067436</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sideranthus</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">annuus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 653. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sideranthus;species annuus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Rydberg) Cory" date="unknown" rank="species">annuus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species annuus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(Rydberg) Shinners" date="unknown" rank="species">annua</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species annua;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–100 cm, herbaceous or suffrutescent.</text>
      <biological_entity id="o12001" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="suffrutescent" value_original="suffrutescent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades oblanceolate to oblong or oblong-lanceolate, midcauline (1–) 4–15 (–25) mm wide.</text>
      <biological_entity id="o12002" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="oblong or oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o12003" name="midcauline" name_original="midcauline" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="width" src="d0_s1" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads on short, sometimes bracteate peduncles, not surpassed by distal leaves.</text>
      <biological_entity id="o12004" name="head" name_original="heads" src="d0_s2" type="structure" />
      <biological_entity id="o12005" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s2" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12006" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o12004" id="r1094" name="on" negation="false" src="d0_s2" to="o12005" />
      <relation from="o12004" id="r1095" name="surpassed by" negation="true" src="d0_s2" to="o12006" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres (4.5–) 6–9 × 9–15 (–20) mm.</text>
      <biological_entity id="o12007" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_length" src="d0_s3" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries in 3–4 series, loose, strongly unequal, apices broadly spreading to squarrose, ca. 0.9–1 mm wide, herbaceous.</text>
      <biological_entity id="o12008" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" notes="" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s4" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o12009" name="series" name_original="series" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o12010" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly spreading" name="orientation" src="d0_s4" to="squarrose" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <relation from="o12008" id="r1096" name="in" negation="false" src="d0_s4" to="o12009" />
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets (13–) 20–36;</text>
      <biological_entity id="o12011" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" name="atypical_quantity" src="d0_s5" to="20" to_inclusive="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s5" to="36" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas 6–11 (–14) mm.</text>
      <biological_entity id="o12012" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets: corolla-tubes ± equaling limbs.</text>
      <biological_entity id="o12013" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure" />
      <biological_entity id="o12015" name="limb" name_original="limbs" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 12.</text>
      <biological_entity id="o12014" name="corolla-tube" name_original="corolla-tubes" src="d0_s7" type="structure" />
      <biological_entity constraint="2n" id="o12016" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jul–)Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand or sandy loam, prairies, stream sides and bottoms, alkaline flats, salt marsh edges, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand" />
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="stream sides" />
        <character name="habitat" value="bottoms" />
        <character name="habitat" value="alkaline flats" />
        <character name="habitat" value="salt marsh edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Nebr., N.Mex., Okla., Tex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Viscid camphor-daisy</other_name>
  
</bio:treatment>