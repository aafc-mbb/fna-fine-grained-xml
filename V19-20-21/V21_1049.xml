<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">419</other_info_on_meta>
    <other_info_on_meta type="illustration_page">417</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="genus">balduina</taxon_name>
    <taxon_name authority="(Pursh) B. L. Robinson" date="1911" rank="species">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>47: 215. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus balduina;species angustifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066204</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Buphthalmum</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">angustifolium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 564. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Buphthalmum;species angustifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actinospermum</taxon_name>
    <taxon_name authority="(Pursh) Torrey &amp; A. Gray" date="unknown" rank="species">angustifolium</taxon_name>
    <taxon_hierarchy>genus Actinospermum;species angustifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (taprooted).</text>
      <biological_entity id="o17488" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–20+, branched.</text>
      <biological_entity id="o17489" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="20" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves abaxially glabrous or sparsely hairy, adaxially glabrous;</text>
      <biological_entity id="o17490" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal leaves (absent at flowering) linear-spatulate, 1.5–6 × 0.05–0.19 (–0.25) cm, proximal and mid cauline leaves linear, 1.5–4.6 × 0.02–0.07 cm, distal cauline leaves similar, smaller and becoming reduced, distalmost bractlike.</text>
      <biological_entity constraint="basal" id="o17491" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-spatulate" value_original="linear-spatulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.19" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.25" to_unit="cm" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="width" src="d0_s3" to="0.19" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal and mid cauline" id="o17492" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="4.6" to_unit="cm" />
        <character char_type="range_value" from="0.02" from_unit="cm" name="width" src="d0_s3" to="0.07" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal cauline" id="o17493" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="becoming" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o17494" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="bractlike" value_original="bractlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1–11.5 cm, sparsely hairy.</text>
      <biological_entity id="o17495" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="11.5" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–20+.</text>
      <biological_entity id="o17496" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="20" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 6–15 mm diam.</text>
      <biological_entity id="o17497" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries abaxially glabrous;</text>
      <biological_entity id="o17498" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>outer proximally yellow, distally green, lanceolate, 2.4–6.7 mm, apices acuminate-aristate;</text>
      <biological_entity constraint="outer" id="o17499" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s8" to="6.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>inner lanceolate, 4.2–7 mm, apices acuminate-aristate.</text>
      <biological_entity id="o17500" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate-aristate" value_original="acuminate-aristate" />
        <character is_modifier="false" name="position" src="d0_s9" value="inner" value_original="inner" />
      </biological_entity>
      <biological_entity id="o17501" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17502" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate-aristate" value_original="acuminate-aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles: pit borders spinulose-cuspidate at angles.</text>
      <biological_entity id="o17503" name="receptacle" name_original="receptacles" src="d0_s10" type="structure" />
      <biological_entity constraint="pit" id="o17504" name="border" name_original="borders" src="d0_s10" type="structure">
        <character constraint="at angles" constraintid="o17505" is_modifier="false" name="architecture_or_shape" src="d0_s10" value="spinulose-cuspidate" value_original="spinulose-cuspidate" />
      </biological_entity>
      <biological_entity id="o17505" name="angle" name_original="angles" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 5–13;</text>
      <biological_entity id="o17506" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tubes 2–3 mm, laminae 8–18 × 2.2–6.5 mm.</text>
      <biological_entity id="o17507" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17508" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="18" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 30–90;</text>
      <biological_entity id="o17509" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="90" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, 3.5–4.5 mm;</text>
      <biological_entity id="o17510" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branches yellow.</text>
      <biological_entity id="o17511" name="style-branch" name_original="style-branches" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 1–2 mm;</text>
      <biological_entity id="o17512" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of obovate to orbiculate, entire scales 0.3–0.6 mm. 2n = 36.</text>
      <biological_entity id="o17513" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity id="o17514" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character char_type="range_value" from="obovate" is_modifier="true" name="shape" src="d0_s17" to="orbiculate" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s17" value="entire" value_original="entire" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17515" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="36" value_original="36" />
      </biological_entity>
      <relation from="o17513" id="r1202" name="consists_of" negation="false" src="d0_s17" to="o17514" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (year round south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="year round south" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sandy soils, especially in pinelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sandy soils" />
        <character name="habitat" value="pinelands" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>