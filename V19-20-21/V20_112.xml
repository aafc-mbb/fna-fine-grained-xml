<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(Cronquist) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">nana</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 87. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety nana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068284</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) Britton" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="Cronquist" date="unknown" rank="variety">nanus</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>5: 129. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;variety nanus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="(Cronquist) D. D. Keck" date="unknown" rank="subspecies">nanus</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;subspecies nanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 cm.</text>
      <biological_entity id="o25458" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems whitish, leafy, loosely tomentose.</text>
      <biological_entity id="o25459" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves dark green to grayish;</text>
      <biological_entity id="o25460" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s2" to="grayish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved, filiform to linear, 30–40 × 1–3 mm, faces compactly tomentose.</text>
      <biological_entity id="o25461" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25462" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="compactly" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 8–13 mm.</text>
      <biological_entity id="o25463" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 14–18, apices erect, acute to obtuse, outer abaxial faces sparsely tomentose, inner glabrous.</text>
      <biological_entity id="o25464" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s5" to="18" />
      </biological_entity>
      <biological_entity id="o25465" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="outer abaxial" id="o25466" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="position" src="d0_s5" value="inner" value_original="inner" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 7.5–10 mm, tubes glabrous or moderately hairy, lobes 1.6–2.5 mm, glabrous;</text>
      <biological_entity id="o25467" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25468" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25469" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages shorter than stigmatic portions.</text>
      <biological_entity constraint="style" id="o25470" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic portions" constraintid="o25471" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o25471" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae densely hairy;</text>
      <biological_entity id="o25472" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 5.2–9 mm. 2n = 18.</text>
      <biological_entity id="o25473" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25474" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky ridges and cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>211.</number>
  <other_name type="common_name">Dwarf rabbitbrush</other_name>
  
</bio:treatment>