<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="treatment_page">424</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">pyrrocoma</taxon_name>
    <taxon_name authority="(Hooker) Greene" date="1894" rank="species">uniflora</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">uniflora</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pyrrocoma;species uniflora;variety uniflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068681</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="H. M. Hall" date="unknown" rank="species">contractus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species contractus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Hooker) Torrey &amp; A. Gray" date="unknown" rank="species">uniflorus</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall" date="unknown" rank="subspecies">howellii</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species uniflorus;subspecies howellii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">uniflorus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">howellii</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species uniflorus;variety howellii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrrocoma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">howellii</taxon_name>
    <taxon_hierarchy>genus Pyrrocoma;species howellii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–40 cm.</text>
      <biological_entity id="o10300" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems reddish, tomentose, lanate, or glabrate.</text>
      <biological_entity id="o10301" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanate" value_original="lanate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal blades narrowly to broadly lanceolate, 50–100 × 5–15 mm, margins usually remotely denticulate, rarely laciniate or entire;</text>
      <biological_entity id="o10302" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o10303" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s2" to="100" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10304" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually remotely" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="laciniate" value_original="laciniate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>faces tomentose to glabrate or glabrous.</text>
      <biological_entity id="o10305" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10306" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s3" to="glabrate or glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads usually borne singly, terminal, rarely 1–2 additional proximally.</text>
      <biological_entity id="o10307" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1" modifier="rarely" name="quantity" src="d0_s4" to="2" />
        <character is_modifier="false" modifier="proximally" name="quantity" src="d0_s4" value="additional" value_original="additional" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 6–9 × 13–18 mm.</text>
      <biological_entity id="o10308" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries appressed, subequal, outer sometimes shorter.</text>
      <biological_entity id="o10309" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>2n = 12.</text>
      <biological_entity constraint="outer" id="o10310" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10311" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane pine forests, alkaline meadows, around hot springs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane pine forests" />
        <character name="habitat" value="alkaline meadows" />
        <character name="habitat" value="hot springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., N.W.T., Sask.; Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14b.</number>
  <discussion>Variety uniflora is the more widespread and variable of the two varieties.</discussion>
  
</bio:treatment>