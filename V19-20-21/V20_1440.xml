<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">541</other_info_on_meta>
    <other_info_on_meta type="treatment_page">640</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Lessing" date="1832" rank="genus">BLENNOSPERMA</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Gen. Compos.,</publication_title>
      <place_in_publication>267. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus BLENNOSPERMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek blennos, mucus, and sperma, seed, alluding to cypselae becoming mucilaginous when wetted</other_info_on_name>
    <other_info_on_name type="fna_id">104065</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 3–12 (–30) cm (taprooted).</text>
      <biological_entity id="o25179" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect (usually branched ± throughout).</text>
      <biological_entity id="o25180" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves (sometimes ± fleshy) basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>mostly sessile;</text>
      <biological_entity id="o25181" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades pinnately nerved, linear (or pinnately divided into 2–15 linear lobes), ultimate margins entire, faces glabrous or sparsely floccose-tomentose.</text>
      <biological_entity id="o25182" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o25183" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25184" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="floccose-tomentose" value_original="floccose-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o25185" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± hemispheric, 3–6+ mm diam.</text>
      <biological_entity id="o25186" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 5–13+ in ± 2 series, ± erect (inflexed in late flowering, reflexed in fruit), basally connate, elliptic to ovate, subequal, ± membranous (veiny, tips usually purple).</text>
      <biological_entity id="o25187" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o25188" from="5" name="quantity" src="d0_s8" to="13" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="ovate" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o25188" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex, smooth or foveolate, epaleate.</text>
      <biological_entity id="o25189" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s9" value="foveolate" value_original="foveolate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 5–13+, pistillate (sometimes, some pistillate florets lack corollas);</text>
      <biological_entity id="o25190" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="13" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas sessile (lacking tubes), usually yellow, rarely white (often purplish abaxially, laminae ovate to linear).</text>
      <biological_entity id="o25191" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 20–60 (–100+), functionally staminate;</text>
      <biological_entity id="o25192" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="100" upper_restricted="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="60" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter to longer than campanulate throats;</text>
      <biological_entity id="o25193" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o25194" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than campanulate throats" constraintid="o25195" is_modifier="false" name="size_or_length" src="d0_s13" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity id="o25195" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lobes 5, ± erect, deltate to lanceolate;</text>
      <biological_entity id="o25196" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s14" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles not divided.</text>
      <biological_entity id="o25197" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae ± ellipsoid, usually 5–6 (–10) -ribbed or angled, usually papillate (papillae becoming mucilaginous when wetted);</text>
      <biological_entity id="o25198" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s16" value="5-6(-10)-ribbed" value_original="5-6(-10)-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s16" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s16" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 9.</text>
      <biological_entity id="o25199" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o25200" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., South America (Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>242.</number>
  <discussion>Species 3 (2 in the flora).</discussion>
  <references>
    <reference>Ornduff, R. 1964. Biosystematics of Blennosperma (Compositae). Brittonia 16: 289–295.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal leaves not lobed or 2–3(–5)-lobed; stigmas of ray florets red</description>
      <determination>1 Blennosperma bakeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal leaves usually 5–15-lobed, rarely entire; stigmas of ray florets yellow</description>
      <determination>2 Blennosperma nanum</determination>
    </key_statement>
  </key>
</bio:treatment>