<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">stephanomeria</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">paniculata</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 428. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus stephanomeria;species paniculata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067605</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 0–100 cm.</text>
      <biological_entity id="o16020" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems single, branched (branches nearly at right angles, stiff), glabrous.</text>
      <biological_entity id="o16021" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves withered at flowering;</text>
      <biological_entity id="o16022" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" name="life_cycle" src="d0_s2" value="withered" value_original="withered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades oblanceolate, 6–10 cm, margins entire or toothed (teeth minute, faces glabrous);</text>
      <biological_entity constraint="basal" id="o16023" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16024" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline much reduced, bractlike.</text>
      <biological_entity constraint="cauline" id="o16025" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly along branches or in paniculiform arrays.</text>
      <biological_entity id="o16026" name="head" name_original="heads" src="d0_s5" type="structure">
        <character constraint="along branches or in paniculiform arrays" is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 2–10 mm.</text>
      <biological_entity id="o16027" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of appressed bractlets.</text>
      <biological_entity id="o16028" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o16029" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o16028" id="r1462" name="consists_of" negation="false" src="d0_s7" to="o16029" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres 6–9 mm.</text>
      <biological_entity id="o16030" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 5.</text>
      <biological_entity id="o16031" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae light to dark tan, 3.8–4.2 mm, faces slightly bumpy to tuberculate, (grooved);</text>
      <biological_entity id="o16032" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s10" to="dark tan" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s10" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16033" name="face" name_original="faces" src="d0_s10" type="structure">
        <character char_type="range_value" from="slightly bumpy" name="relief" src="d0_s10" to="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of 15–18 tan bristles (connate in groups of 2–4, bases persistent), plumose to tops of bases.</text>
      <biological_entity id="o16035" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s11" to="18" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity id="o16036" name="top" name_original="tops" src="d0_s11" type="structure" />
      <biological_entity id="o16037" name="base" name_original="bases" src="d0_s11" type="structure" />
      <relation from="o16034" id="r1463" name="consist_of" negation="false" src="d0_s11" to="o16035" />
      <relation from="o16036" id="r1464" name="part_of" negation="false" src="d0_s11" to="o16037" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 16.</text>
      <biological_entity id="o16034" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character constraint="to tops" constraintid="o16036" is_modifier="false" name="shape" notes="" src="d0_s11" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16038" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, sandy or volcanic soils, plains and foothills, often growing as weed along roads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="open" />
        <character name="habitat" value="volcanic soils" />
        <character name="habitat" value="plains" />
        <character name="habitat" value="foothills" />
        <character name="habitat" value="weed" modifier="often growing as" />
        <character name="habitat" value="roads" modifier="along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Stiff-branched wirelettuce</other_name>
  
</bio:treatment>