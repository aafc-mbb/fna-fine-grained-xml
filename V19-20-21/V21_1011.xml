<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">411</other_info_on_meta>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaenactis</taxon_name>
    <taxon_name authority="(Harvey &amp; A. Gray) A. Gray" date="1874" rank="section">acarphaea</taxon_name>
    <taxon_name authority="(Harvey &amp; A. Gray) A. Gray" date="1874" rank="species">artemisiifolia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 74. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus chaenactis;section acarphaea;species artemisiifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066306</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acarphaea</taxon_name>
    <taxon_name authority="Harvey &amp; A. Gray" date="unknown" rank="species">artemisiifolia</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 98. 1849 (as artemisiaefolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Acarphaea;species artemisiifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (15–) 25–90 (–200) cm.</text>
      <biological_entity id="o7311" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal (withering) and cauline, 3–15 (–20) cm;</text>
      <biological_entity id="o7312" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>largest blades ± plane, not succulent;</text>
      <biological_entity constraint="largest" id="o7313" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>primary lobes mostly 5–10 pairs, ultimate lobes ± crowded, antrorse, lanceolate to elliptic, plane.</text>
      <biological_entity constraint="primary" id="o7314" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o7315" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 1.5–6 cm.</text>
      <biological_entity id="o7316" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric, mostly 10–15 mm diam.</text>
      <biological_entity id="o7317" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" modifier="mostly" name="diameter" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries: longest 7–10 (–12) mm, ± densely villous, not or sparsely glandular;</text>
      <biological_entity id="o7318" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="length" src="d0_s6" value="longest" value_original="longest" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="not; sparsely" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apices (all) erect, ± green, acute or scarcely acuminate, not aristate, ± plane.</text>
      <biological_entity id="o7319" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o7320" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="aristate" value_original="aristate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles: paleae 0.</text>
      <biological_entity id="o7321" name="receptacle" name_original="receptacles" src="d0_s8" type="structure" />
      <biological_entity id="o7322" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas 5–7 mm.</text>
      <biological_entity id="o7323" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae compressed, 4–7 mm;</text>
      <biological_entity id="o7324" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0 or coroniform (of ± 10 scales, longest 0.1–0.5 mm).</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 16.</text>
      <biological_entity id="o7325" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7326" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry canyons, open slopes, often over granitoid rocks, locally abundant in chaparral burns or other recovering disturbances</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry canyons" />
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="granitoid rocks" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="abundant in chaparral burns" modifier="locally" />
        <character name="habitat" value="other recovering disturbances" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>80–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="80" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="past_name">artemisiaefolia</other_name>
  <other_name type="common_name">White pincushion</other_name>
  <discussion>In the flora area, Chaenactis artemisiifolia is known from the Transverse and Peninsular ranges and seaward valleys of southwestern California. It is fire-adapted; its germination is significantly enhanced by exposure to biomass smoke (J. E. Keeley and C. J. Fotheringham 1998).</discussion>
  <discussion>Chaenactis lacera Greene, the eighteenth species of the genus, is known from coastal portions (including islands) of the western Vizcaíno Desert in Baja California and Baja California Sur, Mexico. Forms of C. artemisiifolia sometimes resemble C. lacera in coastal southern California (P. Stockwell 1940), where C. lacera could eventually be introduced. Besides the key characteristics above, C. lacera differs from C. artemisiifolia by its largest leaf blades broadly ± elliptic, 2–3-pinnately lobed, ultimate lobes remote, recurved to retrorse, ± linear, involute (leaf blades appearing ± skeletal).</discussion>
  
</bio:treatment>