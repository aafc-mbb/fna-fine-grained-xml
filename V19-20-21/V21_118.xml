<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">56</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">rudbeckia</taxon_name>
    <taxon_name authority="(Sweet) C. C. Gmelin ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 556. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section rudbeckia;species grandiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417173</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Centrocarpha</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="species">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Brit. Fl. Gard., ser.</publication_title>
      <place_in_publication>2, 1: plate 87. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Centrocarpha;species grandiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, to 120 cm (roots fibrous, caudices often woody).</text>
      <biological_entity id="o14146" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally glabrous or sparsely hairy (hairs spreading), distally strigose (hairs ascending).</text>
      <biological_entity id="o14147" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades elliptic, lanceolate, or ovate (± conduplicate, not lobed), bases cuneate to rounded, margins entire or remotely serrate, apices acute, faces strigose, abaxially glanddotted;</text>
      <biological_entity id="o14148" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14149" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o14150" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
      <biological_entity id="o14151" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o14152" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o14153" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal petiolate, 10–35 × 2–11 cm;</text>
      <biological_entity id="o14154" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o14155" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s3" to="35" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline petiolate (proximal) to nearly sessile (distal), 4–30 × 1.5–9 cm.</text>
      <biological_entity id="o14156" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o14157" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="petiolate" name="architecture" src="d0_s4" to="nearly sessile" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads mostly borne singly.</text>
      <biological_entity id="o14158" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries to 15 mm (strigose and glanddotted).</text>
      <biological_entity id="o14159" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles hemispheric to ovoid;</text>
      <biological_entity id="o14160" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s7" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>paleae 5–6.5 mm, (apical margins glabrous) acuminate-cuspidate, awn-tipped, abaxial tips sparsely strigose.</text>
      <biological_entity id="o14161" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="acuminate-cuspidate" value_original="acuminate-cuspidate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14162" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 12–25;</text>
      <biological_entity id="o14163" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae elliptic to obovate (reflexed), 20–50 × 5–10 mm, abaxially hairy and glanddotted.</text>
      <biological_entity id="o14164" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s10" to="50" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Discs 10–30 × 15–25 mm.</text>
      <biological_entity id="o14165" name="disc" name_original="discs" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 200–800+;</text>
      <biological_entity id="o14166" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="200" name="quantity" src="d0_s12" to="800" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas greenish yellow basally and in lobes, otherwise maroon, 3.5–5 mm;</text>
      <biological_entity id="o14167" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s13" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="otherwise" name="coloration" notes="" src="d0_s13" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14168" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <relation from="o14167" id="r975" name="in" negation="false" src="d0_s13" to="o14168" />
    </statement>
    <statement id="d0_s14">
      <text>style-branches ca. 1.8 mm, apices obtuse.</text>
      <biological_entity id="o14169" name="branch-style" name_original="style-branches" src="d0_s14" type="structure">
        <character name="distance" src="d0_s14" unit="mm" value="1.8" value_original="1.8" />
      </biological_entity>
      <biological_entity id="o14170" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o14171" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi coroniform, to 0.5 mm.</text>
      <biological_entity id="o14172" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ga., Ill., Kans., Ky., La., Miss., Mo., Ohio, Okla., Tex.; Mostly c, e, and s United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mostly c" establishment_means="native" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="and s United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Largeflower or rough coneflower</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous or sparsely hairy proximally and hairy distally (hairs ascending, mostly shorter than 0.5 mm)</description>
      <determination>17a Rudbeckia grandiflora var. alismifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems hairy (hairs spreading proximally, ascending distally, ca. 1 mm)</description>
      <determination>17b Rudbeckia grandiflora var. grandiflora</determination>
    </key_statement>
  </key>
</bio:treatment>