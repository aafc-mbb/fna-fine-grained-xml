<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="treatment_page">297</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="species">canus</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 67. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species canus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066568</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="species">phoenicodontus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species phoenicodontus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–35 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively thick, usually retaining old leaf-bases.</text>
      <biological_entity id="o9983" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o9984" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o9985" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o9984" id="r925" modifier="usually" name="retaining" negation="false" src="d0_s1" to="o9985" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, densely white strigoso-canescent, eglandular.</text>
      <biological_entity id="o9986" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigoso-canescent" value_original="strigoso-canescent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal (persistent, ± erect; leaf-bases broadened or not, not thickened and whitish-indurate);</text>
      <biological_entity id="o9987" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9988" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear-oblanceolate to oblanceolate, 20–100 × 2–5 (–7) mm, cauline sharply reduced or 0 (restricted to proximal 1/4–1/2), margins entire, faces densely white strigoso-canescent, eglandular.</text>
      <biological_entity id="o9989" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o9990" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9991" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9992" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigoso-canescent" value_original="strigoso-canescent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–4).</text>
      <biological_entity id="o9993" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–7 × 9–16 mm.</text>
      <biological_entity id="o9994" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series, densely hirsute to strigoso-hirsute, minutely glandular.</text>
      <biological_entity id="o9995" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="densely hirsute" name="pubescence" notes="" src="d0_s7" to="strigoso-hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o9996" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o9995" id="r926" name="in" negation="false" src="d0_s7" to="o9996" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 20–50 (–70);</text>
      <biological_entity id="o9997" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="70" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white to light blue, 7–12 mm, laminae reflexing, sometimes tardily.</text>
      <biological_entity id="o9998" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="light blue" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9999" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.7–5.6 mm.</text>
      <biological_entity constraint="disc" id="o10000" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s10" to="5.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae (nearly terete,) 2.8–3.5 mm, (8–) 10–14-nerved, faces glabrous;</text>
      <biological_entity id="o10001" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="(8-)10-14-nerved" value_original="(8-)10-14-nerved" />
      </biological_entity>
      <biological_entity id="o10002" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae or bristles, inner of 24–36 bristles.</text>
      <biological_entity id="o10004" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o10005" name="bristle" name_original="bristles" src="d0_s12" type="structure" />
      <biological_entity id="o10006" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="24" is_modifier="true" name="quantity" src="d0_s12" to="36" />
      </biological_entity>
      <relation from="o10003" id="r927" name="outer of" negation="false" src="d0_s12" to="o10004" />
      <relation from="o10003" id="r928" name="outer of" negation="false" src="d0_s12" to="o10005" />
      <relation from="o10003" id="r929" name="inner of" negation="false" src="d0_s12" to="o10006" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o10003" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o10007" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry hills, grasslands, often in gravelly or shaley soil, sagebrush, pinyon-juniper, pine-oak, oak</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry hills" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="shaley soil" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="pine-oak" />
        <character name="habitat" value="oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., Nebr., N.Mex., Okla., S.Dak., Utah, Wyo.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51.</number>
  <other_name type="past_name">canum</other_name>
  <other_name type="common_name">Hoary fleabane</other_name>
  
</bio:treatment>