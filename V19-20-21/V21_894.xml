<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="treatment_page">357</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">eriophyllum</taxon_name>
    <taxon_name authority="Brandegee" date="1899" rank="species">congdonii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>27: 449. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus eriophyllum;species congdonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066708</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–30 cm.</text>
      <biological_entity id="o5441" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± spreading.</text>
      <biological_entity id="o5442" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: oblanceolate, 10–40 mm, rarely 2–3-lobed, ultimate margins usually entire, plane (apices acute), faces ± woolly.</text>
      <biological_entity id="o5443" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="2-3-lobed" value_original="2-3-lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o5444" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o5445" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly.</text>
      <biological_entity id="o5446" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 3–10 cm.</text>
      <biological_entity id="o5447" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate, 3–5 mm diam.</text>
      <biological_entity id="o5448" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8–10, distinct.</text>
      <biological_entity id="o5449" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 8–10;</text>
      <biological_entity id="o5450" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae yellow, 3–5 mm.</text>
      <biological_entity id="o5451" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 10–30;</text>
      <biological_entity id="o5452" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 2–3 mm (tubes cylindric, throats funnelform, gradually dilated, lobes not glandular; anther appendages deltate, glandular).</text>
      <biological_entity id="o5453" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2.5–3 mm;</text>
      <biological_entity id="o5454" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 3–5 ± spatulate scales 0.5–1 mm plus 3–5 lanceolate scales 1.5–2 mm. 2n = 14.</text>
      <biological_entity id="o5455" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
      <biological_entity id="o5456" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s12" to="5" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5457" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5458" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
      </biological_entity>
      <relation from="o5455" id="r410" name="consist_of" negation="false" src="d0_s12" to="o5456" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky openings, foothill woodlands or yellow pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky openings" />
        <character name="habitat" value="foothill woodlands" />
        <character name="habitat" value="yellow pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="past_name">congdoni</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>