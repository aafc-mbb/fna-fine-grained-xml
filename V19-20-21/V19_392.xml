<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="illustration_page">278</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="species">caespitosum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Belg.,</publication_title>
      <place_in_publication>62. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species caespitosum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416658</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hieracium</taxon_name>
    <taxon_name authority="Tausch" date="unknown" rank="species">pratense</taxon_name>
    <taxon_hierarchy>genus Hieracium;species pratense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pilosella</taxon_name>
    <taxon_name authority="(Dumortier) P. D. Sell &amp; C. West" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_hierarchy>genus Pilosella;species caespitosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–75 cm.</text>
      <biological_entity id="o7891" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally piloso-hirsute (hairs 1–3+ mm) and stipitate-glandular, sometimes stellate-pubescent as well, distally piloso-hirsute (hairs 1–4+ mm), stellate-pubescent, and stipitate-glandular.</text>
      <biological_entity id="o7892" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="well; distally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 3–8+, cauline 0–2 (–5+);</text>
      <biological_entity id="o7893" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o7894" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o7895" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate to lanceolate, 35–120 (–180+) × 12–20+ mm, lengths 2–6 (–10+) times widths, bases cuneate, margins entire or denticulate, apices rounded to acute, faces usually piloso-hirsute (hairs 1–3+ mm) and stellate-pubescent, sometimes glabrate.</text>
      <biological_entity id="o7896" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7897" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="180" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2-6(-10+)" value_original="2-6(-10+)" />
      </biological_entity>
      <biological_entity id="o7898" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o7899" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o7900" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o7901" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 5–25+ in ± umbelliform or congested, corymbiform arrays.</text>
      <biological_entity id="o7902" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o7903" from="5" name="quantity" src="d0_s4" to="25" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7903" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s4" value="umbelliform" value_original="umbelliform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles piloso-hirsute (hairs 1–2.5 mm), stellate-pubescent, and stipitate-glandular.</text>
      <biological_entity id="o7904" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 5–8+.</text>
      <biological_entity id="o7905" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o7906" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, 7.5–9 mm.</text>
      <biological_entity id="o7907" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–18+, apices acute to acuminate, abaxial faces piloso-hirsute (hairs 1–2.5+), stellate-pubescent, and stipitate-glandular.</text>
      <biological_entity id="o7908" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="18" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7909" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7910" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 25–50+;</text>
      <biological_entity id="o7911" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 8–12+ mm.</text>
      <biological_entity id="o7912" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae columnar, 1.5–1.8 mm;</text>
      <biological_entity id="o7913" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 25–30+, white bristles in 1 series, 4–5 (–6) mm.</text>
      <biological_entity id="o7914" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7915" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s12" to="30" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o7916" name="series" name_original="series" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <relation from="o7914" id="r736" name="consist_of" negation="false" src="d0_s12" to="o7915" />
      <relation from="o7915" id="r737" name="in" negation="false" src="d0_s12" to="o7916" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, stream sides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="stream sides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300(–1500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Conn., Del., D.C., Ga., Idaho, Ill., Ind., Ky., Maine, Md., Mass., Mich., Minn., Mont., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., Wash., W.Va., Wis., Wyo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>The type of Hieracium floribundum Wimmer &amp; Grabowski probably resulted from a cross between plants of H. caespitosum and H. lactucella (P. D. Sell and C. West 1976).</discussion>
  
</bio:treatment>