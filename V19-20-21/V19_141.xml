<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(A. Gray) B. L. Robinson" date="1911" rank="species">eatonii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">eatonii</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species eatonii;variety eatonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068173</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">eatonii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">harrisonii</taxon_name>
    <taxon_hierarchy>genus Cirsium;species eatonii;variety harrisonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, slender, 10–50 cm.</text>
      <biological_entity id="o3439" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf faces glabrous or nearly so.</text>
      <biological_entity constraint="leaf" id="o3440" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s1" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads usually short-pedunculate, in erect, short, racemiform or spiciform arrays, rarely openly branched.</text>
      <biological_entity id="o3441" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="short-pedunculate" value_original="short-pedunculate" />
        <character is_modifier="false" modifier="rarely openly" name="architecture" notes="" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o3442" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <relation from="o3441" id="r338" name="in" negation="false" src="d0_s2" to="o3442" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres 2–2.5 cm, thinly arachnoid-tomentose or glabrate, individual phyllaries evident.</text>
      <biological_entity id="o3443" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="individual" id="o3444" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries sometimes suffused with dark purple;</text>
      <biological_entity id="o3445" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="suffused with dark purple" value_original="suffused with dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>outer with few–many lateral spines;</text>
      <biological_entity constraint="outer" id="o3446" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity constraint="lateral" id="o3447" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="few" is_modifier="true" name="quantity" src="d0_s5" to="many" />
      </biological_entity>
      <relation from="o3446" id="r339" name="with" negation="false" src="d0_s5" to="o3447" />
    </statement>
    <statement id="d0_s6">
      <text>apical spines slender to stout.</text>
      <biological_entity constraint="apical" id="o3448" name="spine" name_original="spines" src="d0_s6" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s6" to="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Corollas purple, 17–26 mm, tubes 5–10 mm, throats 4.5–10.5 mm, lobes 5.5–7.5 mm.</text>
      <biological_entity id="o3449" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s7" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3450" name="tube" name_original="tubes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3451" name="throat" name_original="throats" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s7" to="10.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3452" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pappi 15–19 mm.</text>
      <biological_entity id="o3453" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jul–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jul-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, canyons, pinyon-juniper woodlands to alpine, montane coniferous forests, subalpine forests, alpine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="montane coniferous forests" modifier="alpine" />
        <character name="habitat" value="subalpine forests" />
        <character name="habitat" value="slopes" modifier="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51a.</number>
  <discussion>Variety eatonii is distributed on various of the sky islands in the Basin and Range province of Nevada and Utah. Habitats vary from shaded forest understory sites to forest openings or open exposed sites.</discussion>
  
</bio:treatment>