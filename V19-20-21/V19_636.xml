<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">389</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="treatment_page">399</other_info_on_meta>
    <other_info_on_meta type="illustration_page">397</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">antennaria</taxon_name>
    <taxon_name authority="(Hooker) Greene" date="1898" rank="species">lanata</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 288. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus antennaria;species lanata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066077</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="(Wahlenberg) Hooker" date="unknown" rank="species">carpatica</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">lanata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 329. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Antennaria;species carpatica;variety lanata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Dioecious.</text>
      <biological_entity id="o10242" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Plants 3–20 cm (caudices branching or rhizomes stout).</text>
      <biological_entity id="o10243" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stolons none.</text>
      <biological_entity id="o10244" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 3-nerved, narrowly oblanceolate, 10–60 (–100) × 3–12 mm, tips acute, faces gray-woolly or tomentose.</text>
      <biological_entity constraint="basal" id="o10245" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10246" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o10247" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gray-woolly" name="pubescence" src="d0_s3" value="or" value_original="or" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves linear, 5–40 mm, mid and distal flagged.</text>
      <biological_entity constraint="cauline" id="o10248" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="mid and distal" id="o10249" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="flagged" value_original="flagged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 3–9 in corymbiform arrays.</text>
      <biological_entity id="o10250" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o10251" from="3" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o10251" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres: staminate 4.5–6 mm;</text>
      <biological_entity id="o10252" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 5–8 mm.</text>
      <biological_entity id="o10253" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries (proximally light-brown, dark-brown, or olivaceous) distally whitish or light-brown.</text>
      <biological_entity id="o10254" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas: staminate 3–4.5 mm;</text>
      <biological_entity id="o10255" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate 2.5–4 mm.</text>
      <biological_entity id="o10256" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1–1.6 mm, glabrous;</text>
      <biological_entity id="o10257" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: staminate 4–5 mm;</text>
      <biological_entity id="o10258" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistillate 3.5–5 mm. 2n = 28 (under A. neodioica).</text>
      <biological_entity id="o10259" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10260" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Protected alpine and subalpine sites, gravelly or sandy soils near conifers at timberline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine" modifier="protected" />
        <character name="habitat" value="subalpine sites" />
        <character name="habitat" value="gravelly" constraint="near conifers at timberline" />
        <character name="habitat" value="sandy soils" constraint="near conifers at timberline" />
        <character name="habitat" value="conifers" constraint="at timberline" />
        <character name="habitat" value="timberline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Idaho, Mont., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Woolly pussytoes</other_name>
  
</bio:treatment>