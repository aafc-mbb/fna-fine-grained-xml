<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">603</other_info_on_meta>
    <other_info_on_meta type="treatment_page">604</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Rafinesque" date="1817" rank="genus">erechtites</taxon_name>
    <taxon_name authority="(Poiret) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">minimus</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>6: 437. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus erechtites;species minimus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250066506</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Poiret" date="unknown" rank="species">minimus</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl., suppl.</publication_title>
      <place_in_publication>5: 130. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species minimus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, (50–) 100–200 cm (sparsely and unevenly tomentose, glabrescent).</text>
      <biological_entity id="o2159" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="50" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots with branching lateral roots.</text>
      <biological_entity id="o2161" name="taproot" name_original="taproots" src="d0_s1" type="structure" />
      <biological_entity constraint="lateral" id="o2162" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <relation from="o2161" id="r199" name="with" negation="false" src="d0_s1" to="o2162" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1, erect.</text>
      <biological_entity id="o2163" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves evenly distributed (proximal often withering before flowering);</text>
    </statement>
    <statement id="d0_s4">
      <text>indistinctly petiolate (bases often weakly clasping);</text>
      <biological_entity id="o2164" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate to narrowly lanceolate, (3–) 7–20 × 1–4 cm, margins sharply and evenly toothed (not lobed or pinnatifid).</text>
      <biological_entity id="o2165" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="narrowly lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s5" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2166" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="evenly" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (40–) 80–200 in open, corymbiform arrays (or arrays of clusters).</text>
      <biological_entity id="o2167" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="40" name="atypical_quantity" src="d0_s6" to="80" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o2168" from="80" name="quantity" src="d0_s6" to="200" />
      </biological_entity>
      <biological_entity id="o2168" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric or turbinate, 4–7 mm.</text>
      <biological_entity id="o2169" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries usually 8.</text>
      <biological_entity id="o2170" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles 3–5 mm diam.</text>
      <biological_entity id="o2171" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1.5–2 mm, usually puberulent in lines along relatively narrow ribs, sometimes glabrous.</text>
      <biological_entity id="o2173" name="line" name_original="lines" src="d0_s10" type="structure" />
      <biological_entity id="o2174" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="relatively" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o2173" id="r200" name="along" negation="false" src="d0_s10" to="o2174" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 60.</text>
      <biological_entity id="o2172" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character constraint="in lines" constraintid="o2173" is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2175" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed, coastal sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal sites" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg., Wash.; Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="past_name">minima</other_name>
  <discussion>The type of Erechtites prenanthoides (A. Richard) de Candolle (a name sometimes misapplied to plants of E. minimus) is conspecific with the type of E. quaridentata (Labillardière) de Candolle.</discussion>
  
</bio:treatment>