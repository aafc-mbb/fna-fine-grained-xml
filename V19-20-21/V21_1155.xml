<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kunsiri Chaw Siripun,Edward E. Schilling</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">461</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="mention_page">471</other_info_on_meta>
    <other_info_on_meta type="treatment_page">462</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPATORIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 836. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 363. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus EUPATORIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">For Mithridates Eupator, King of Pontus, 132–63 B.C.</other_info_on_name>
    <other_info_on_name type="fna_id">112351</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–200 cm.</text>
      <biological_entity id="o20063" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually not branched proximal to arrays of heads (from caudices or rhizomes).</text>
      <biological_entity id="o20064" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="to arrays" constraintid="o20065" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o20065" name="array" name_original="arrays" src="d0_s1" type="structure" />
      <biological_entity id="o20066" name="head" name_original="heads" src="d0_s1" type="structure" />
      <relation from="o20065" id="r1365" name="part_of" negation="false" src="d0_s1" to="o20066" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o20068" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>usually opposite (rarely whorled, distal sometimes alternate);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o20067" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually 3-nerved from or distal to bases, or pinnately nerved, mostly deltate or ovate to lanceolate or linear (and intermediate shapes, sometimes elliptic, oblong, rhombic, or suborbiculate, sometimes pinnatifid, 1–2-pinnately, ternately, or palmately lobed), ultimate margins entire or toothed, faces glabrous or puberulent, pubescent, scabrous, or setulose, usually glanddotted.</text>
      <biological_entity id="o20069" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="from distal" constraintid="o20070" is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" modifier="pinnately" name="architecture" notes="" src="d0_s5" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o20070" name="distal" name_original="distal" src="d0_s5" type="structure" />
      <biological_entity id="o20071" name="base" name_original="bases" src="d0_s5" type="structure" />
      <biological_entity constraint="ultimate" id="o20072" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o20073" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="setulose" value_original="setulose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="setulose" value_original="setulose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="setulose" value_original="setulose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="setulose" value_original="setulose" />
      </biological_entity>
      <relation from="o20070" id="r1366" name="to" negation="false" src="d0_s5" to="o20071" />
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, in corymbiform or diffuse to dense, paniculiform arrays.</text>
      <biological_entity id="o20074" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o20075" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character char_type="range_value" from="diffuse" is_modifier="true" name="density" src="d0_s6" to="dense" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o20074" id="r1367" name="in" negation="false" src="d0_s6" to="o20075" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres obconic to ellipsoid, 1–3 (–5+) mm diam.</text>
      <biological_entity id="o20076" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s7" to="ellipsoid" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 7–15+ in 2–3 (–4+) series, (usually green) 2–3-nerved, or not notably nerved, or pinnately nerved, elliptic, lanceolate, oblong, or obovate, usually unequal, sometimes ± equal (margins scarious, hyaline, apices rounded to acute or acuminate sometimes mucronate, faces usually puberulent or villous, usually glanddotted, rarely glabrous).</text>
      <biological_entity id="o20077" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o20078" from="7" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-3-nerved" value_original="2-3-nerved" />
        <character is_modifier="false" modifier="not notably" name="architecture" src="d0_s8" value="nerved" value_original="nerved" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s8" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-3-nerved" value_original="2-3-nerved" />
        <character is_modifier="false" modifier="not notably" name="architecture" src="d0_s8" value="nerved" value_original="nerved" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s8" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-3-nerved" value_original="2-3-nerved" />
        <character is_modifier="false" modifier="not notably" name="architecture" src="d0_s8" value="nerved" value_original="nerved" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s8" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="sometimes more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o20078" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" upper_restricted="false" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat or convex, epaleate.</text>
      <biological_entity id="o20079" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets (3–) 5 (–15+);</text>
      <biological_entity id="o20080" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="15" upper_restricted="false" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas usually white, rarely pinkish, throats funnelform to campanulate, lobes 5, triangular;</text>
      <biological_entity id="o20081" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
      </biological_entity>
      <biological_entity id="o20082" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o20083" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles: bases sometimes enlarged, usually puberulent (glabrous in E. capillifolium), branches mostly filiform.</text>
      <biological_entity id="o20084" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o20085" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20086" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae (brownish to black) prismatic, 5-ribbed, usually glabrous, usually glanddotted;</text>
      <biological_entity id="o20087" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="5-ribbed" value_original="5-ribbed" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi persistent, of 20–50 (whitish) barbellulate bristles in 1 series.</text>
      <biological_entity id="o20089" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s14" to="50" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <biological_entity id="o20090" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <relation from="o20088" id="r1368" name="consist_of" negation="false" src="d0_s14" to="o20089" />
      <relation from="o20089" id="r1369" name="in" negation="false" src="d0_s14" to="o20090" />
    </statement>
    <statement id="d0_s15">
      <text>x = 10.</text>
      <biological_entity id="o20088" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o20091" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, Europe, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>392.</number>
  <discussion>Species 41+ (24 species, including 2 hybrids, in the flora).</discussion>
  <discussion>Eupatorium is treated here in a restricted circumscription, following R. M. King and H. Robinson (1987) in excluding genera that traditionally have been included in a broad Eupatorium (e.g., Ageratina, Chromolaena, Critonia, Conoclinium, Fleischmannia, Koanophyllon, Tamaulipa); Eutrochium (Eupatorium sect. Verticillatum) is also excluded here.</discussion>
  <discussion>Species identification within Eupatorium is sometimes complicated; polyploidy and apomixis have contributed to the complications. Some species include both sexual diploid and apomictic polyploid plants or populations. V. I. Sullivan (1972) made important contributions to understanding Eupatorium in North America by showing that some fairly distinct, sexual diploid species may include apomictic polyploid plants or populations that do not differ greatly from the diploids. Other apomictic polyploids appear to be intermediate morphologically between pairs of diploid or diploid/polyploid species and were proposed by Sullivan to have originated from interspecific hybridization. Distinction and level of recognition of hybrid apomictic taxa have a large arbitrary component, in part because some apomicts appear to be ephemeral and others may be relatively stable and in part because differences in the relative genomic contributions of the progenitors through dosage effects or backcrossing may affect whether an apomict is morphologically distinctive or part of a continuous series of variation.</discussion>
  <references>
    <reference>Sullivan, V. I. 1972. Investigations of the Breeding Systems, Formation of Auto- and Alloploids and the Reticulate Pattern of Hybridization in North American Eupatorium (Compositae). Ph.D. dissertation. Florida State University.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves (at least the principal) pinnatifid, or pinnately or ternately lobed, or palmately 3(–5)-lobed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves simple (not lobed, margins crenate, entire, laciniate, serrate, or serrulate)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves (at least larger proximal) palmately 3(–5)-lobed (lobes relatively broad, margins serrate; corollas usually pinkish)</description>
      <determination>4 Eupatorium cannabinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves pinnatifid or pinnately or ternately lobed (lobes relatively narrow; corollas usually white)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves (mostly ternately or 1–2-pinnately lobed): blades or lobes linear; heads in subcorymbiform to subpaniculiform arrays; florets 7–9</description>
      <determination>19 Eupatorium ×pinnatifidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves (usually pinnately or ternately lobed or pinnatifid): blades or lobes linear; heads in paniculiform arrays; florets usually 5</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems glabrous (gland-dotted, branches supporting heads recurved and secund)</description>
      <determination>11 Eupatorium leptophyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems puberulent (branches supporting heads not recurved or secund)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades or lobes 0.2–0.5(–1) mm wide (margins strongly revolute); phyllaries usually glabrate or glabrous, usually not gland-dotted</description>
      <determination>5 Eupatorium capillifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades or lobes 0.5–2.5(–4) mm wide; phyllaries usually puberulent(mostly on midveins), usually gland-dotted</description>
      <determination>6 Eupatorium compositifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves petiolate (petioles 10–30 mm)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves sessile or subsessile (petioles 0 or 1–10 mm)</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades deltate to rhombic (held vertically; stems green); florets 5</description>
      <determination>14 Eupatorium mikanioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades lanceolate (held horizontally; stems sometimes reddish to purplish);florets 9–15</description>
      <determination>23 Eupatorium serotinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Florets 7–11 (leaf bases connate-perfoliate)</description>
      <determination>16 Eupatorium perfoliatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Florets (4–)5–8 or 9–14 (leaf bases not connate-perfoliate)</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Florets usually 9–14</description>
      <determination>20 Eupatorium resinosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Florets (4–)5–8</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Phyllary apices (usually white) acuminate to attenuate</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Phyllary apices (usually with narrow white margins, sometimes pigmented) acute, obtuse, or rounded</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf bases (especially larger leaves) clasping to narrowly connate-perfoliate (leaf faces rugose); florets 5–8</description>
      <determination>7 Eupatorium ×cordigerum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf bases attenuate, cuneate, or rounded (leaf faces relatively smooth); florets (4–)5</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaves 4–10(–15) mm wide</description>
      <determination>12 Eupatorium leucolepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaves mostly 10–45 mm wide</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves little, if at all, gland-dotted; phyllaries glabrous, not gland-dotted (the larger 8–10 mm)</description>
      <determination>17 Eupatorium petaloideum</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves usually gland-dotted; phyllaries puberulent to villous (at least toward bases and on midveins), gland-dotted (the larger 5–10 mm)</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades elliptic to oblanceolate (lengths mostly 3–4 times widths), bases narrowly cuneate (sometimesoblique); phyllaries linear (larger 6–9 mm)</description>
      <determination>1 Eupatorium album</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades elliptic, lanceolate, or lance-ovate (lengths mostly 1–2 times widths), bases rounded to rounded-cuneate; phyllaries oblong to lance-oblong (larger 5–7mm)</description>
      <determination>18 Eupatorium pilosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Stems glabrous or glabrate proximally (and leaves pinnately nerved, 70–150 mm); leaves: bases truncate to somewhat rounded, facesglabrate (scattered, fine hairs)</description>
      <determination>24 Eupatorium sessilifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Stems usually pilose, puberulent, or pubescent proximally or throughout (if glabrous, leaves 3-nerved from bases, 30–50 × 5–13 mm, bases narrowly cuneate); leaves (3-nerved from or distal to bases or pinnately nerved): bases usually narrowly to broadly cuneate, rounded, subcordate, or truncate, sometimes clasping to perfoliate, faces glabrous, puberulent, scabrous, or villous.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades deltate, elliptic, lanceolate, lance-ovate, ovate, or suborbiculate (usually broadest proximal to middles), bases usually broadly cuneate, rounded, subcordate, or truncate, sometimes clasping to perfoliate</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades elliptic, lance-elliptic, lanceolate, lance-oblong, linear, oblanceolate, or oblong (usually broadest near or distal to middles), bases cuneate</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf bases (especially larger leaves) clasping to narrowly connate-perfoliate (faces conspicuously rugose); florets 5–8</description>
      <determination>7 Eupatorium ×cordigerum</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf bases broadly cuneate, rounded, subcordate, or truncate (faces smooth to somewhat rugose); florets (4–)5</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaves pinnately nerved, blades usually 50–100 mm</description>
      <determination>8 Eupatorium godfreyanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaves 3-nerved, blades 15–50(–90) mm</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf blades elliptic, lanceolate, or lance-ovate (lengths mostly 2–2.5 times widths, usually with purple borders, sometimes visible only in live plants; distal leaves sometimes alternate)</description>
      <determination>18 Eupatorium pilosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf blades usually deltate to suborbiculate, sometimes ovate (lengths mostly1–2 times widths; distal leaves sometimes alternate)</description>
      <determination>21 Eupatorium rotundifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaves strongly to obscurely 3-nerved from bases</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaves 3-nerved distal to bases, or pinnately nerved, or relatively narrow and obscurely 3-nerved from bases</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Stems usually glabrous or glabrate (at least proximally), sometimes pilose; leaves 30–50 mm, faces glabrous abaxially</description>
      <determination>10 Eupatorium lancifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Stems pubescent or pilose proximally or throughout; leaves 20–60(–120, the larger usually 50+) mm, faces puberulent, scabrous, or villous abaxially</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Leaves: margins entire proximally, serrate distally (teeth sharp, antrorse; mid-ribs and 2 major lateral veins prominent)</description>
      <determination>2 Eupatorium altissimum</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Leaves: margins usually laciniate-serrate (teeth ± divergent; 2 major lateral veins usually obscure)</description>
      <determination>9 Eupatorium hyssopifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Leaves usually in whorls (3s or 4s), sometimes opposite, blades lance-linear to linear (lengths 6–40 times widths)</description>
      <determination>9 Eupatorium hyssopifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Leaves usually opposite (sometimes whorled in E. semiserratum), blades elliptic, lance-elliptic, lance-oblong, oblanceolate, or oblong (lengths 2.5–7 times widths)</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves (spreading): blades adaxially finely puberulent or villous (stems from short caudices or rhizomes)</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves (ascending, recurved, or spreading): blades adaxially glabrous or glabrate (stems from tuberous-thickened rhizomes, not usually apparent until flowering is well under way)</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Stems densely branched distally; leaf blades (30–)50–70 mm, margins usually serrate; phyllaries elliptic (larger 2.5–3 mm); corollas 2.5–3 mm</description>
      <determination>22 Eupatorium semiserratum</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Stems usually branched at or near bases; leaf blades mostly 20–35(–45) mm, margins weakly serrate to subentire; phyllaries lanceolate (larger usually 4–5 mm); corollas 3–3.5 mm</description>
      <determination>13 Eupatorium linearifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaves (usually spreading or ascending): blades 15–50 × (5–)10–20 mm</description>
      <determination>3 Eupatorium anomalum</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaves (usually recurved): blades 20–80 × 5–10(–20) mm</description>
      <determination>15 Eupatorium mohrii</determination>
    </key_statement>
  </key>
</bio:treatment>