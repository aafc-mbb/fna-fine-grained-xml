<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="treatment_page">458</other_info_on_meta>
    <other_info_on_meta type="illustration_page">452</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">marshalliinae</taxon_name>
    <taxon_name authority="Schreber" date="1791" rank="genus">marshallia</taxon_name>
    <taxon_name authority="(Walter) Small" date="1898" rank="species">graminifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 482. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe marshalliinae;genus marshallia;species graminifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067162</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Athanasia</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">graminifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>200. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Athanasia;species graminifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marshallia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">graminifolia</taxon_name>
    <taxon_name authority="(Rafinesque) L. E. Watson" date="unknown" rank="subspecies">tenuifolia</taxon_name>
    <taxon_hierarchy>genus Marshallia;species graminifolia;subspecies tenuifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marshallia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">lacinarioides</taxon_name>
    <taxon_hierarchy>genus Marshallia;species lacinarioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marshallia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">williamsonii</taxon_name>
    <taxon_hierarchy>genus Marshallia;species williamsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–80 (–120) cm (aerial stems simple or branched near or proximal to middles).</text>
      <biological_entity id="o8953" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o8954" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal petiolate;</text>
      <biological_entity constraint="basal" id="o8955" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ± 3-nerved, oblanceolate, (2–) 4–25 (–34) cm × 2–13+ mm (cauline sessile, ascending, linear, sharply differentiated from basal).</text>
      <biological_entity id="o8956" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="34" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="13" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 1–34, 20–30 mm diam.</text>
      <biological_entity id="o8957" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="34" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–50 cm.</text>
      <biological_entity id="o8958" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 4–9.5 × 0.5–2.5 mm, (margins ± winged proximally) apices subulate.</text>
      <biological_entity id="o8959" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8960" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae linear-attenuate, apices subulate (often glanddotted).</text>
      <biological_entity id="o8961" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-attenuate" value_original="linear-attenuate" />
      </biological_entity>
      <biological_entity id="o8962" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas usually pale lavender to purple, rarely white, lobes 2.5–6 × 0.5–1 mm.</text>
      <biological_entity id="o8963" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually pale lavender" name="coloration" src="d0_s8" to="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o8964" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi: scale margins entire or denticulate.</text>
      <biological_entity id="o8965" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity constraint="scale" id="o8966" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8967" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bogs and pocosins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bogs" />
        <character name="habitat" value="pocosins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Marshallia graminifolia grows on the Atlantic and Gulf coastal plains from the Carolinas to Texas, often in bogs with pitcher plants, sundews, and orchids in open areas of pine savannas.</discussion>
  <discussion>Typical Marshallia graminifolia has undifferentiated, ascending, and firm leaves. Plants with thinner basal leaves that tend to spread and are more strongly differentiated from the cauline leaves have been called subsp. tenuifolia; they grow mostly in Florida and Georgia and along the Gulf coastal plain to Texas.</discussion>
  <references>
    <reference>Watson, L. E., W. J. Elisens, and J. R. Estes. 1994. Genetic differentiation in populations of Marshallia graminifolia s.lat. (Asteraceae). Biochem. Syst. &amp; Ecol. 22: 577–582.</reference>
  </references>
  
</bio:treatment>