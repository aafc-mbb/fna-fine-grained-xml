<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="treatment_page">523</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="Shinners" date="1959" rank="species">tenuis</taxon_name>
    <place_of_publication>
      <publication_title>SouthW. Naturalist</publication_title>
      <place_in_publication>4: 208. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species tenuis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067117</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–55 cm.</text>
      <biological_entity id="o16598" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="55" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose to subglobose.</text>
      <biological_entity id="o16599" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s1" to="subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems strigoso-puberulent.</text>
      <biological_entity id="o16600" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigoso-puberulent" value_original="strigoso-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline 1 (–3) -nerved, linear-lanceolate to linear-oblanceolate, 120–250 × 2–3 (–5) mm, abruptly reduced on distal 1/2–2/3 of stems, sparsely pilose (abaxial faces), glanddotted.</text>
      <biological_entity id="o16601" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1(-3)-nerved" value_original="1(-3)-nerved" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="linear-oblanceolate" />
        <character char_type="range_value" from="120" from_unit="mm" name="length" src="d0_s3" to="250" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character constraint="on distal 1/2-2/3" constraintid="o16602" is_modifier="false" modifier="abruptly" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16602" name="1/2-2/3" name_original="1/2-2/3" src="d0_s3" type="structure" />
      <biological_entity id="o16603" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <relation from="o16602" id="r1144" name="part_of" negation="false" src="d0_s3" to="o16603" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in loose, spiciform arrays (internodes 1–15 mm).</text>
      <biological_entity id="o16604" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o16605" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <relation from="o16604" id="r1145" name="in" negation="false" src="d0_s4" to="o16605" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0 or 1–5 mm.</text>
      <biological_entity id="o16606" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="1-5 mm" value_original="1-5 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres cylindro-campanulate, 10–13 × 5–6 (–7) mm.</text>
      <biological_entity id="o16607" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 (–5) series, outermost narrowly triangular, unequal, sparsely fine-pilose to glabrate, margins without hyaline borders, ciliate, apices (loosely divergent) acute to acuminate (innermost sometimes obtuse and short-acuminate).</text>
      <biological_entity id="o16608" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o16609" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o16610" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="sparsely fine-pilose" name="pubescence" src="d0_s7" to="glabrate" />
      </biological_entity>
      <biological_entity id="o16611" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o16612" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o16613" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
      <relation from="o16608" id="r1146" name="in" negation="false" src="d0_s7" to="o16609" />
      <relation from="o16611" id="r1147" name="without" negation="false" src="d0_s7" to="o16612" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 10–12;</text>
      <biological_entity id="o16614" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes glabrous inside.</text>
      <biological_entity id="o16615" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 4.2–4.5 mm;</text>
      <biological_entity id="o16616" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths equaling corollas, bristles barbellate or proximally plumose.</text>
      <biological_entity id="o16617" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o16618" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o16619" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="barbellate" value_original="barbellate" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s11" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Longleaf pine savannas, pine-hardwood edges, slopes, flats, uplands, near drainages, sands, sandy clays, fencerows, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="longleaf pine savannas" />
        <character name="habitat" value="pine-hardwood edges" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="uplands" />
        <character name="habitat" value="near drainages" />
        <character name="habitat" value="sands" />
        <character name="habitat" value="sandy clays" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Shinners’s gayfeather</other_name>
  <discussion>Liatris tenuis is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>