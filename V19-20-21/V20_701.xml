<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">261</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1878" rank="species">miser</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>13: 372. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species miser</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066634</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–25 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, woody [usually not collected].</text>
      <biological_entity id="o25560" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent-ascending to ascending-erect (caudexlike at bases), white-villous (hairs stiff), minutely glandular.</text>
      <biological_entity id="o25561" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent-ascending" name="orientation" src="d0_s2" to="ascending-erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="white-villous" value_original="white-villous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline;</text>
      <biological_entity id="o25562" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades narrowly obovate, 7–16 × 1–3.5 mm, margins entire, faces white-villous (hairs stiff), minutely glandular.</text>
      <biological_entity id="o25563" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="16" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25564" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25565" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="white-villous" value_original="white-villous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (discoid) 1 or 2–4 in loosely corymbiform arrays.</text>
      <biological_entity id="o25566" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" constraint="in arrays" constraintid="o25567" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o25567" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="loosely" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres (3.5–) 4–5 × 7–12 mm.</text>
      <biological_entity id="o25568" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–5 series, glabrous, densely minutely glandular.</text>
      <biological_entity id="o25569" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o25570" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <relation from="o25569" id="r2362" name="in" negation="false" src="d0_s7" to="o25570" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0.</text>
      <biological_entity id="o25571" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets corollas 3.2–4.5 mm (throats slightly indurate, not inflated).</text>
      <biological_entity constraint="disc-floret" id="o25572" name="corolla" name_original="corollas" src="d0_s9" type="structure" constraint_original="disc-florets">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2–2.5 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o25573" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o25574" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: outer of setae, inner of 18–25 (–28) bristles.</text>
      <biological_entity id="o25576" name="seta" name_original="setae" src="d0_s11" type="structure" />
      <biological_entity id="o25577" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s11" to="28" />
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s11" to="25" />
      </biological_entity>
      <relation from="o25575" id="r2363" name="outer of" negation="false" src="d0_s11" to="o25576" />
      <relation from="o25575" id="r2364" name="inner of" negation="false" src="d0_s11" to="o25577" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 18.</text>
      <biological_entity id="o25575" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o25578" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Taluses, rock crevices, montane coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="taluses" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="montane coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>103.</number>
  <other_name type="common_name">Starved fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erigeron miser differs from E. petrophilus primarily by its smaller heads; there also are overlapping differences in leaf size, disc corolla length, shape of the style appendages, and numbers of pappus bristles.</discussion>
  
</bio:treatment>