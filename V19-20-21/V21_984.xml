<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">388</other_info_on_meta>
    <other_info_on_meta type="mention_page">389</other_info_on_meta>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">palafoxia</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1842" rank="species">callosa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 369. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus palafoxia;species callosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">242416916</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stevia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">callosa</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>2: 121. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Stevia;species callosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–60 cm.</text>
      <biological_entity id="o18685" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems scabrous to glabrate, not stipitate-glandular.</text>
      <biological_entity id="o18686" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s1" to="glabrate" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear, 20–70 × 1–4 mm.</text>
      <biological_entity id="o18687" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres ± turbinate.</text>
      <biological_entity id="o18688" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="turbinate" value_original="turbinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 3–5 × 1+ mm, ± equal, ± strigillose, not stipitate-glandular.</text>
      <biological_entity id="o18689" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s4" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 0.</text>
      <biological_entity id="o18690" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 5–30;</text>
      <biological_entity id="o18691" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas actinomorphic, 5–6 mm, throats ± funnelform, shorter than lobes.</text>
      <biological_entity id="o18692" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="actinomorphic" value_original="actinomorphic" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18693" name="throat" name_original="throats" src="d0_s7" type="structure">
        <character constraint="than lobes" constraintid="o18694" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o18694" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae 3–5 mm;</text>
      <biological_entity id="o18695" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappus-scales of inner cypselae 0.3–1 mm. 2n = 20.</text>
      <biological_entity id="o18696" name="pappus-scale" name_original="pappus-scales" src="d0_s9" type="structure" constraint="cypsela" constraint_original="cypsela; cypsela">
        <character char_type="range_value" from="0.3" from_unit="mm" name="distance" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18697" name="cypsela" name_original="cypselae" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o18698" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
      </biological_entity>
      <relation from="o18696" id="r1274" name="part_of" negation="false" src="d0_s9" to="o18697" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, calcareous soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous soils" modifier="rocky" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Mo., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Some depauperate specimens of Palafoxia rosea (e.g., Oklahoma, Beckham Co., 17 Oct 1936, Eskero 1502, US) closely resemble P. callosa.</discussion>
  
</bio:treatment>