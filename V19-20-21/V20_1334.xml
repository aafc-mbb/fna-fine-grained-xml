<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="treatment_page">596</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Greene) C. Jeffrey" date="1992" rank="species">porteri</taxon_name>
    <place_of_publication>
      <publication_title>Kew Bull.</publication_title>
      <place_in_publication>47: 101. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species porteri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067265</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">porteri</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 186. 1897,</place_in_publication>
      <other_info_on_pub>based on S. renifolius Porter, Syn. Fl. Colorado, 83. 1874, not Schultz-Bipontinus 1845</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species porteri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">aureus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">alpinus</taxon_name>
    <taxon_hierarchy>genus Senecio;species aureus;variety alpinus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–10+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous (rhizomes branched).</text>
      <biological_entity id="o18731" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (often cyanic), glabrous.</text>
      <biological_entity id="o18732" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (relatively turgid, abaxial faces cyanic) petiolate;</text>
      <biological_entity constraint="basal" id="o18733" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades reniform to subreniform, 5–15+ × 5–25+ mm, bases abruptly contracted to cordate, margins usually crenate, sometimes wavy.</text>
      <biological_entity id="o18734" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s4" to="subreniform" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o18735" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o18736" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves abruptly reduced (1–4, sessile; bractlike, entire).</text>
      <biological_entity constraint="cauline" id="o18737" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly.</text>
      <biological_entity id="o18738" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles ebracteate, glabrous.</text>
      <biological_entity id="o18739" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi conspicuous (bractlets often cyanic).</text>
      <biological_entity id="o18740" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 13 or 21, deep red, 8–10+ mm, glabrous.</text>
      <biological_entity id="o18741" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="13" value_original="13" />
        <character name="quantity" src="d0_s9" unit="or" value="21" value_original="21" />
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8 or 13;</text>
      <biological_entity id="o18742" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="8" value_original="8" />
        <character name="quantity" src="d0_s10" unit="or" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 8–10+ mm.</text>
      <biological_entity constraint="corolla" id="o18743" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 40–50+;</text>
      <biological_entity id="o18744" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 2.5–3.5 mm, limbs 2.5–3.5 mm.</text>
      <biological_entity id="o18745" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18746" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o18747" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 4–5.5 mm.</text>
      <biological_entity id="o18748" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Jul–mid Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Aug" from="mid Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep talus slopes in alpine habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep talus" />
        <character name="habitat" value="alpine habitats" modifier="slopes in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2800–3900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3900" to_unit="m" from="2800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <other_name type="common_name">Porter’s groundsel</other_name>
  <discussion>Multiple collections of Packera porteri are known from Colorado; single collections are known from Oregon (1899; collector indicated few plants were seen) and Washington (1996).</discussion>
  
</bio:treatment>