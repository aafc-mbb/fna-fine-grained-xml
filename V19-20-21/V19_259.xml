<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="Schreber" date="1791" rank="genus">vernonia</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="1903" rank="species">texana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1338. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus vernonia;species texana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067811</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vernonia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">texana</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 91. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Vernonia;species angustifolia;variety texana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 4–8 (–10+) dm.</text>
      <biological_entity id="o17189" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems puberulent, glabrescent.</text>
      <biological_entity id="o17190" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o17191" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ovatelanceolate (basal) to narrowly lanceolate (distal), 5–12+ cm × (5–) 12–25+ mm, l/w = 3–5 (basal) or 8–17 (distal), abaxially glabrate (pitted, awl-shaped hairs in pits), adaxially puberulent and/or scabrellous.</text>
      <biological_entity id="o17192" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s3" to="narrowly lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s3" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" name="l/w" src="d0_s3" to="5" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s3" to="17" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in open, paniculiform-scorpioid arrays.</text>
      <biological_entity id="o17193" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o17194" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="true" name="shape" src="d0_s4" value="paniculiform-scorpioid" value_original="paniculiform-scorpioid" />
      </biological_entity>
      <relation from="o17193" id="r1544" name="in" negation="false" src="d0_s4" to="o17194" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–35 mm.</text>
      <biological_entity id="o17195" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± obconic to hemispheric, 4.5–6 × 5–7 mm.</text>
      <biological_entity id="o17196" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="less obconic" name="shape" src="d0_s6" to="hemispheric" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 35–45+ in 5–6+ series, glabrescent, margins arachno-ciliolate, the outer lance-deltate, 1–32 mm, inner oblong to linear, 4–5 (–6) mm, tips acute or rounded-apiculate.</text>
      <biological_entity id="o17197" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o17198" from="35" name="quantity" src="d0_s7" to="45" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o17198" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17199" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="arachno-ciliolate" value_original="arachno-ciliolate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17200" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lance-deltate" value_original="lance-deltate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="32" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17201" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="linear" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17202" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded-apiculate" value_original="rounded-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 12–24+.</text>
      <biological_entity id="o17203" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="24" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o17204" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi usually whitish to stramineous (rarely purplish), outer scales or bristles 20+, 0.3–1+ mm, contrasting or intergrading with 20+, 6–7+ mm inner subulate scales or bristles.</text>
      <biological_entity id="o17205" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="usually whitish" name="coloration" src="d0_s10" to="stramineous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17208" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17209" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 34.</text>
      <biological_entity constraint="outer" id="o17206" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17207" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" upper_restricted="false" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17210" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pinelands, scrub oak woodlands, sandy or sandy-clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="scrub oak woodlands" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="sandy-clay soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., La., Miss., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  
</bio:treatment>