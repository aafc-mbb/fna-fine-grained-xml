<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="treatment_page">331</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="(A. Gray) A. M. Powell" date="1968" rank="section">laphamia</taxon_name>
    <taxon_name authority="(A. Gray) J. F. Macbride" date="1918" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>56: 39. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section laphamia;species lemmonii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067326</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Laphamia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>16: 101. 1880 (as lemmoni)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Laphamia;species lemmonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Laphamia</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">dissecta</taxon_name>
    <taxon_name authority="(A. Gray) W. E. Niles" date="unknown" rank="subspecies">lemmonii</taxon_name>
    <taxon_hierarchy>genus Laphamia;species dissecta;subspecies lemmonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 6–17 (–23) cm (densely clumped, stems leafy);</text>
    </statement>
    <statement id="d0_s1">
      <text>usually densely pilose to villous, sometimes glabrate.</text>
      <biological_entity id="o21960" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="23" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="17" to_unit="cm" />
        <character char_type="range_value" from="usually densely pilose" name="pubescence" src="d0_s1" to="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o21961" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="23" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="17" to_unit="cm" />
        <character char_type="range_value" from="usually densely pilose" name="pubescence" src="d0_s1" to="villous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves (opposite or alternate): petioles 3–8 (–10) mm (usually shorter than blades);</text>
      <biological_entity id="o21962" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o21963" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ovate to ovate-deltate (margins crenate, lacerate, laciniate, or serrate), or 3–5-lobed or pinnately divided (lobes crenate, lacerate, laciniate, lobed, or serrate), 6–18 × 7–20 mm.</text>
      <biological_entity id="o21964" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21965" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovate-deltate or 3-5-lobed or pinnately divided" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovate-deltate or 3-5-lobed or pinnately divided" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or (2–3) in corymbiform arrays (often obscured by leaves), 7–10 × 5–9 mm.</text>
      <biological_entity id="o21966" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o21967" from="2" name="atypical_quantity" src="d0_s4" to="3" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" notes="" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21967" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 1–5 (–10) mm.</text>
      <biological_entity id="o21968" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate.</text>
      <biological_entity id="o21969" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 10–16, linear-lanceolate, lanceolate, to oblanceolate, 4–6.4 (–8) × 1–1.7 mm (apices acute).</text>
      <biological_entity id="o21970" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="16" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="oblanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="oblanceolate" />
        <character char_type="range_value" from="6.4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0.</text>
      <biological_entity id="o21971" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 20–40;</text>
      <biological_entity id="o21972" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, often tinged with purple, tubes 1–1.9 mm, throats broadly tubular to subfunnelform, 2–2.5 (–2.9) mm, lobes 0.4–0.6 mm.</text>
      <biological_entity id="o21973" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="tinged with purple" value_original="tinged with purple" />
      </biological_entity>
      <biological_entity id="o21974" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21975" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character char_type="range_value" from="broadly tubular" name="shape" src="d0_s10" to="subfunnelform" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21976" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae narrowly oblong to narrowly oblanceolate, 2.5–3.2 (–3.6) mm, margins thin-calloused, short-hairy;</text>
      <biological_entity id="o21977" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s11" to="narrowly oblanceolate" />
        <character char_type="range_value" from="3.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21978" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="thin-calloused" value_original="thin-calloused" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi usually of 1 (–2) delicate bristles 1–3 (–4) mm, rarely plus vestigial, hyaline scales, sometimes 0.2n = 34.</text>
      <biological_entity id="o21979" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="rarely" name="prominence" notes="" src="d0_s12" value="vestigial" value_original="vestigial" />
      </biological_entity>
      <biological_entity id="o21980" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s12" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="true" name="fragility" src="d0_s12" value="delicate" value_original="delicate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21981" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character modifier="sometimes" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21982" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="34" value_original="34" />
      </biological_entity>
      <relation from="o21979" id="r1515" name="consist_of" negation="false" src="d0_s12" to="o21980" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices of granitic boulders and cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of granitic boulders and cliffs" />
        <character name="habitat" value="granitic boulders" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="past_name">lemmoni</other_name>
  <other_name type="common_name">Lemmon’s rock daisy</other_name>
  <discussion>Perityle lemmonii, which occurs in the mountain areas of southeastern Arizona and southwestern New Mexico, has variable leaf morphology that is often consistent within geographic populations. Its former inclusion within P. dissecta is discussed under that species.</discussion>
  
</bio:treatment>