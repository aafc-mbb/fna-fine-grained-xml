<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Theodore M. Barkley†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">541</other_info_on_meta>
    <other_info_on_meta type="treatment_page">607</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="D. Don in R. Sweet" date="1834" rank="genus">PERICALLIS</taxon_name>
    <place_of_publication>
      <publication_title>in R. Sweet, Brit. Fl. Gard., ser.</publication_title>
      <place_in_publication>2, 3: plate 228. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus PERICALLIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, peri, very, and callos, beautiful, used by Homer for “very beautiful”</other_info_on_name>
    <other_info_on_name type="fna_id">124518</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials [subshrubs or shrubs], mostly 20–40 (–100) [150+] cm.</text>
      <biological_entity id="o11602" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" modifier="mostly" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="150" from_unit="cm" modifier="mostly" name="average_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect or spreading (branched distally).</text>
      <biological_entity id="o11603" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petiole bases sometimes expanded and/or clasping);</text>
      <biological_entity id="o11604" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades palmately nerved, cordate-deltate to orbiculate or polygonally lobed, margins dentate to denticulate, faces sparsely hairy.</text>
      <biological_entity id="o11605" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="cordate-deltate" name="shape" src="d0_s5" to="orbiculate or polygonally lobed" />
      </biological_entity>
      <biological_entity id="o11606" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s5" to="denticulate" />
      </biological_entity>
      <biological_entity id="o11607" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, usually in corymbiform to paniculiform arrays, rarely borne singly.</text>
      <biological_entity id="o11608" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o11609" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
      <relation from="o11608" id="r1067" modifier="usually" name="in" negation="false" src="d0_s6" to="o11609" />
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0.</text>
      <biological_entity id="o11610" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindric to urceolate, mostly 3–8+ mm diam.</text>
      <biological_entity id="o11611" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="urceolate" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="mostly" name="diameter" src="d0_s8" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries persistent, mostly 13 or 21 in (1–) 2 series, erect, distinct, ± linear, subequal, margins scarious (tips green to brown or reddish, not blackened).</text>
      <biological_entity id="o11612" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character modifier="mostly" name="quantity" src="d0_s9" unit="or in" value="13" value_original="13" />
        <character modifier="mostly" name="quantity" src="d0_s9" unit="or in" value="21" value_original="21" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s9" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o11613" name="series" name_original="series" src="d0_s9" type="structure" />
      <biological_entity id="o11614" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat, foveolate (socket margins membranous), epaleate.</text>
      <biological_entity id="o11615" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s10" value="foveolate" value_original="foveolate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets ± 13 or ± 21, pistillate, fertile;</text>
      <biological_entity id="o11616" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="13" value_original="13" />
        <character name="quantity" src="d0_s11" value="more or less" value_original="more or less" />
        <character name="quantity" src="d0_s11" value="21" value_original="21" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas whitish or bluish, pinkish, purplish, or reddish (often proximally pale and distally darker).</text>
      <biological_entity id="o11617" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bluish" value_original="bluish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bluish" value_original="bluish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bluish" value_original="bluish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bluish" value_original="bluish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 40–60+, bisexual, fertile;</text>
      <biological_entity id="o11618" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s13" to="60" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas ochroleucous, white, or purplish to reddish or pinkish, tubes longer than funnelform throats, lobes 5, erect or reflexed, deltate to lanceolate;</text>
      <biological_entity id="o11619" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="ochroleucous" value_original="ochroleucous" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s14" to="reddish or pinkish" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s14" to="reddish or pinkish" />
      </biological_entity>
      <biological_entity id="o11620" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o11621" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o11621" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o11622" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s14" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branches stigmatic in 2 lines, apices truncate [with deltate appendages].</text>
      <biological_entity id="o11623" name="style-branch" name_original="style-branches" src="d0_s15" type="structure">
        <character constraint="in lines" constraintid="o11624" is_modifier="false" name="structure_in_adjective_form" src="d0_s15" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o11624" name="line" name_original="lines" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11625" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae ± ellipsoid (sometimes ± compressed), 4–5-ribbed, glabrous or puberulent;</text>
      <biological_entity id="o11626" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="4-5-ribbed" value_original="4-5-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi readily falling, usually of 20–40+, white, barbellate bristles (discs), sometimes 2 setiform to subulate scales or 0 (rays).</text>
      <biological_entity id="o11627" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s17" value="falling" value_original="falling" />
        <character char_type="range_value" from="setiform" name="shape" src="d0_s17" to="subulate" />
      </biological_entity>
      <biological_entity id="o11628" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s17" to="40" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="barbellate" value_original="barbellate" />
        <character modifier="sometimes" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
      <relation from="o11627" id="r1068" modifier="usually" name="consist_of" negation="false" src="d0_s17" to="o11628" />
    </statement>
    <statement id="d0_s18">
      <text>x = 30.</text>
      <biological_entity id="o11629" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o11630" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Macaronesia (Canary Islands, Azores, Madeira).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Macaronesia (Canary Islands)" establishment_means="introduced" />
        <character name="distribution" value="Macaronesia (Azores)" establishment_means="introduced" />
        <character name="distribution" value="Macaronesia (Madeira)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>219.</number>
  <discussion>Species ca. 15 (1 in the flora).</discussion>
  <discussion>The taxonomic status and nomenclature of Pericallis have been treated by B. Nordenstam (1978); the origins of the florists’ cineraria have been reviewed by T. M. Barkley (1966).</discussion>
  <references>
    <reference>Barkley, T. M. 1966. A review of the origin and development of the florists’ cineraria, Senecio cruentus. Econ. Bot. 20: 386–395.</reference>
    <reference>Swenson, U. and U. Manns. 2003. Phylogeny of Pericallis (Asteraceae): A total evidence approach reappraising the double origin of woodiness. Taxon 52: 533–546.</reference>
  </references>
  
</bio:treatment>