<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mark W. Bierner</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">AMBLYOLEPIS</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 667. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus AMBLYOLEPIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ambly, blunt, and lepis, scale</other_info_on_name>
    <other_info_on_name type="fna_id">101306</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Helenium</taxon_name>
    <taxon_name authority="(de Candolle) Bentham" date="unknown" rank="section">Amblyolepis</taxon_name>
    <taxon_hierarchy>genus Helenium;section Amblyolepis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–60 cm (herbage sweet scented).</text>
      <biological_entity id="o21039" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (1–10) erect or ± decumbent, unbranched or sparingly branched distally, sparsely to densely pilose.</text>
      <biological_entity id="o21040" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s1" to="10" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparingly; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o21041" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate, oblanceolate, ovate, or spatulate, margins entire, faces usually sparsely to moderately pilose (especially on margins), sometimes glabrate or glabrous.</text>
      <biological_entity id="o21042" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o21043" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21044" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or (2–45) in paniculiform arrays.</text>
      <biological_entity id="o21045" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o21046" from="2" name="atypical_quantity" src="d0_s6" to="45" />
      </biological_entity>
      <biological_entity id="o21046" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric to globose, 12–20 mm diam.</text>
      <biological_entity id="o21047" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s7" to="globose" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 17–21 in 2 series (outer usually distinct, sometimes connate proximally, green, narrowly elliptic to lanceolate, herbaceous, moderately to densely pilose, especially bases and margins; inner phyllaries distinct, obovate, hyaline, scalelike, glabrous).</text>
      <biological_entity id="o21048" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o21049" from="17" name="quantity" src="d0_s8" to="21" />
      </biological_entity>
      <biological_entity id="o21049" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles globose to ovoid, ± pitted, usually epaleate (outer disc-florets rarely subtended by paleae, central disc-florets usually subtended by persistent enations).</text>
      <biological_entity id="o21050" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s9" to="ovoid" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8–13 (–20), pistillate, fertile;</text>
      <biological_entity id="o21051" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="20" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="13" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o21052" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 20–50, bisexual, fertile;</text>
      <biological_entity id="o21053" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="50" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than narrowly funnelform to cylindric-campanulate throats, lobes 5, ± deltate (glabrous).</text>
      <biological_entity id="o21054" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21055" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than narrowly funnelform to cylindric-campanulate throats" constraintid="o21056" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21056" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s13" value="cylindric-campanulate" value_original="cylindric-campanulate" />
      </biological_entity>
      <biological_entity id="o21057" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae narrowly obconic, prominantly 10-ribbed, densely pubescent;</text>
      <biological_entity id="o21058" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="prominantly" name="architecture_or_shape" src="d0_s14" value="10-ribbed" value_original="10-ribbed" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent, of 5–6 ovate to obovate scales.</text>
      <biological_entity id="o21060" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s15" to="6" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s15" to="obovate" />
      </biological_entity>
      <relation from="o21059" id="r1441" name="consist_of" negation="false" src="d0_s15" to="o21060" />
    </statement>
    <statement id="d0_s16">
      <text>x = 19.</text>
      <biological_entity id="o21059" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o21061" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>383.</number>
  <other_name type="common_name">Huisache-daisy</other_name>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Bierner, M. W. 1990. Present status of Amblyolepis (Asteraceae: Heliantheae). Madroño 37: 133–140.</reference>
  </references>
  
</bio:treatment>