<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John C. La Duke</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="treatment_page">138</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Desfontaines ex Jussieu" date="1789" rank="genus">TITHONIA</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Pl.,</publication_title>
      <place_in_publication>189. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus TITHONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">From Greek mythology, Tithonus, son of Laomedon and consort of Aurora, symbolic of old age; perhaps alluding to gray to white induments of some plants</other_info_on_name>
    <other_info_on_name type="fna_id">133067</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, subshrubs, or shrubs [trees], 70–500 [–700] cm.</text>
      <biological_entity id="o10987" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o10989" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="500" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="700" to_unit="cm" />
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="500" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched.</text>
      <biological_entity id="o10991" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves all or mostly cauline;</text>
      <biological_entity id="o10993" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite (proximal) or mostly alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o10992" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades often (1-), 3-nerved, or 5-nerved, mostly deltate or pentagonal [lanceolate, linear], sometimes 3-lobed or 5-lobed, bases ± truncate or auriculate [attenuate] (sometimes decurrent onto petioles), ultimate margins serrate to crenate, faces glabrate, ± hirsute, pilose, soft-pubescent, or villous, often glanddotted.</text>
      <biological_entity id="o10994" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="[1" value_original="[1" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-nerved" value_original="5-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-nerved" value_original="5-nerved" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
      <biological_entity id="o10995" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o10996" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s5" to="crenate" />
      </biological_entity>
      <biological_entity id="o10997" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="soft-pubescent" value_original="soft-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="soft-pubescent" value_original="soft-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly (peduncles usually distally dilated, fistulose).</text>
      <biological_entity id="o10998" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate to hemispheric, 10–20+ mm diam.</text>
      <biological_entity id="o10999" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 12–28+ in 2–5 series (linear to broadly rounded, unequal to subequal, apices acute to rounded).</text>
      <biological_entity id="o11000" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o11001" from="12" name="quantity" src="d0_s8" to="28" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o11001" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles hemispheric to convex, paleate (paleae persistent, embracing cypselae, striate, ± 3-toothed, middle teeth larger, stiff, acute or acuminate to aristate).</text>
      <biological_entity id="o11002" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8–30, neuter;</text>
      <biological_entity id="o11003" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="30" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow or orange.</text>
      <biological_entity id="o11004" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 40–120 [–200+], bisexual, fertile;</text>
      <biological_entity id="o11005" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="120" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="200" upper_restricted="false" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="120" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than throats (bases of throats bulbous and hairy), lobes 5, ± triangular (anthers black, brown, or tan, bases cordate-sagittate, appendages ovate; style-branches relatively slender, appendages penicillate or lanceolate to attenuate).</text>
      <biological_entity id="o11006" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o11007" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than throats" constraintid="o11008" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11008" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o11009" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae (black or brown) ± compressed or flattened, often 3-angled or 4-angled or biconvex, ± cuneiform in silhouette (sometimes with basal elaiosomes);</text>
      <biological_entity id="o11010" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="biconvex" value_original="biconvex" />
        <character constraint="in silhouette" constraintid="o11011" is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="cuneiform" value_original="cuneiform" />
      </biological_entity>
      <biological_entity id="o11011" name="silhouette" name_original="silhouette" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, or ± coroniform (of ± connate scales, 1–2 scales sometimes subulate to aristate).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 17.</text>
      <biological_entity id="o11012" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o11013" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, Central America; introduced in se United States, West Indies, South America, and Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="in se United States" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="and Old World" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>297.</number>
  <other_name type="common_name">Sunflowerweed</other_name>
  <discussion>Species 11 (3 in the flora).</discussion>
  <references>
    <reference>La Duke, J. C. 1982. Revision of Tithonia. Rhodora 84: 453–522.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials, subshrubs, or shrubs; phyllaries 16–28; ray laminae 48–69 mm; disc florets 80–120+; cypselae 4–6 mm</description>
      <determination>1 Tithonia diversifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; phyllaries 12–16; ray laminae 9–15 or 20–33 mm; disc florets 40–60 or 60–90; cypselae 5–9 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades ± deltate to pentagonal, often 3- or 5-lobed, abaxial faces soft-pubescent; rays usually orange, rarely yellow, laminae 20–33 × 6–17 mm</description>
      <determination>2 Tithonia rotundifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades mostly deltate, rarely, if ever, lobed, abaxial faces sparsely hirsute (hairs larger on veins); rays yellow, laminae 9–15 × 4–6 mm</description>
      <determination>3 Tithonia thurberi</determination>
    </key_statement>
  </key>
</bio:treatment>