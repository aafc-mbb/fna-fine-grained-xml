<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray in A. Gray et al." date="1884" rank="subsection">thyrsiflorae</taxon_name>
    <taxon_name authority="A. Gray" date="1881" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>16: 80. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection thyrsiflorae;species wrightii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067585</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Kuntze" date="unknown" rank="species">brittonii</taxon_name>
    <taxon_hierarchy>genus Aster;species brittonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">bigelovii</taxon_name>
    <taxon_hierarchy>genus Solidago;species bigelovii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bigelovii</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="variety">wrightii</taxon_name>
    <taxon_hierarchy>genus Solidago;species bigelovii;variety wrightii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="variety">adenophora</taxon_name>
    <taxon_hierarchy>genus Solidago;species wrightii;variety adenophora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–110 cm;</text>
      <biological_entity id="o25376" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices branched, thick, and woody or rhizomes short, woody.</text>
      <biological_entity id="o25377" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o25378" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–30+, simple, sparsely to densely puberulent proximally to densely so in arrays.</text>
      <biological_entity id="o25379" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="30" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="densely in arrays" constraintid="o25380" is_modifier="false" modifier="sparsely to densely; proximally to densely" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o25380" name="array" name_original="arrays" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal winged-petiolate-subpetiolate (poorly differentiated), blades to winged petioles;</text>
      <biological_entity id="o25381" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="winged-petiolate-subpetiolate" value_original="winged-petiolate-subpetiolate" />
      </biological_entity>
      <biological_entity id="o25382" name="blade" name_original="blades" src="d0_s3" type="structure" />
      <biological_entity id="o25383" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="winged" value_original="winged" />
      </biological_entity>
      <relation from="o25382" id="r2341" name="to" negation="false" src="d0_s3" to="o25383" />
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate, largest to 8 cm, usually smaller, gradually attenuate, margins entire, slightly undulate, sparsely to moderately soft-puberulent, somewhat viscid (stipitate-glands very small);</text>
      <biological_entity id="o25384" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o25385" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o25386" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s4" value="soft-puberulent" value_original="soft-puberulent" />
        <character is_modifier="false" modifier="somewhat" name="coating" src="d0_s4" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mostly withering by flowering, new rosettes sometimes present;</text>
      <biological_entity id="o25387" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by rosettes" constraintid="o25388" is_modifier="false" modifier="mostly" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o25388" name="rosette" name_original="rosettes" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="new" value_original="new" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline petiolate (petioles 0.5–1 mm), blades (linear) elliptic proximally to (narrowly) lanceolate or ovate distally, largest 50–80 × 15–25 mm, reduced distally and becoming more ovate, 15–20 mm in arrays, margins entire or serrulate.</text>
      <biological_entity id="o25389" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="cauline" id="o25390" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o25391" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character constraint="to arrays, margins" constraintid="o25392, o25393" is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o25392" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="true" modifier="distally" name="size" src="d0_s6" value="largest" value_original="largest" />
        <character char_type="range_value" from="50" from_unit="mm" is_modifier="true" name="length" src="d0_s6" to="80" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" is_modifier="true" name="width" src="d0_s6" to="25" to_unit="mm" />
        <character is_modifier="true" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="true" modifier="distally" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="15" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o25393" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="true" modifier="distally" name="size" src="d0_s6" value="largest" value_original="largest" />
        <character char_type="range_value" from="50" from_unit="mm" is_modifier="true" name="length" src="d0_s6" to="80" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" is_modifier="true" name="width" src="d0_s6" to="25" to_unit="mm" />
        <character is_modifier="true" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="true" modifier="distally" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="15" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads 1–140, not secund, in thyrsiform-paniculiform arrays, sometimes compact, nearly as wide as tall, and rounded, sometimes appearing almost rounded corymbiform, proximal branches sometimes much elongate, not secund, spreading to ascending.</text>
      <biological_entity id="o25394" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="140" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" notes="" src="d0_s7" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="nearly" name="height" src="d0_s7" value="tall" value_original="tall" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes; sometimes much" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="secund" value_original="secund" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="ascending" />
      </biological_entity>
      <biological_entity id="o25395" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="thyrsiform-paniculiform" value_original="thyrsiform-paniculiform" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o25396" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="almost" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o25394" id="r2342" name="in" negation="false" src="d0_s7" to="o25395" />
      <relation from="o25394" id="r2343" modifier="sometimes" name="appearing" negation="false" src="d0_s7" to="o25396" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0.5–5 (–2.5) mm, densely short hispid-canescent, sparsely to densely stipitate-glandular, ± viscid;</text>
      <biological_entity id="o25397" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="average_some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispid-canescent" value_original="hispid-canescent" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="more or less" name="coating" src="d0_s8" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts small, grading into phyllaries.</text>
      <biological_entity id="o25398" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o25399" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure" />
      <relation from="o25398" id="r2344" name="into" negation="false" src="d0_s9" to="o25399" />
    </statement>
    <statement id="d0_s10">
      <text>Involucres broadly campanulate, (3.5–) 4–5 (–5.5) mm.</text>
      <biological_entity id="o25400" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 2–3 series, unequal, oblong to linear-lanceolate, acute to attenuate, sparsely to moderately strigose, especially distally, sparsely to densely stipitate-glandular, ± viscid.</text>
      <biological_entity id="o25401" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="linear-lanceolate acute" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="linear-lanceolate acute" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="especially distally; distally; sparsely to densely" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="more or less" name="coating" src="d0_s11" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o25402" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="3" />
      </biological_entity>
      <relation from="o25401" id="r2345" name="in" negation="false" src="d0_s11" to="o25402" />
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 6–10;</text>
      <biological_entity id="o25403" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s12" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>laminae ca. 3–5 × 1–2 mm.</text>
      <biological_entity id="o25404" name="lamina" name_original="laminae" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 10–20;</text>
      <biological_entity id="o25405" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas 3–4 mm, lobes ca. 1 mm.</text>
      <biological_entity id="o25406" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25407" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 1.5–2.5 mm, ± moderately short-strigose;</text>
      <biological_entity id="o25408" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less moderately" name="pubescence" src="d0_s16" value="short-strigose" value_original="short-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 3–4 mm. 2n = 18.</text>
      <biological_entity id="o25409" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25410" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open oak-pine woods and rocky open slopes, disturbed ground</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak-pine woods" modifier="open" />
        <character name="habitat" value="rocky open slopes" />
        <character name="habitat" value="disturbed ground" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Okla., Tex.; Mexico (Chihuahua, Durango, Sonora, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Wright’s goldenrod</other_name>
  <discussion>In Texas, Solidago wrightii grows primarily in the trans-Pecos region. It is one of the few usually obviously stipitate-glandular, viscid goldenrods; the stipitate glands are minute when present on other species. Two varieties have been recognized on degree of glandularity; these grade into each other to such a degree that their recognition does not appear warranted. The few plants seen from northeastern New Mexico and westernmost Oklahoma are possible variants of S. petiolaris, but overall they fit better in S. wrightii. A detailed morphometric study of this and the next two species is needed to resolve ambiguous species limits on the western Great Plains.</discussion>
  
</bio:treatment>