<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="treatment_page">283</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Cronquist" date="1947" rank="species">abajoensis</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>6: 168. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species abajoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066541</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="species">awapensis</taxon_name>
    <taxon_hierarchy>genus Erigeron;species awapensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–15 (–24) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices branches relatively thick and short, retaining old leaf-bases.</text>
      <biological_entity id="o23590" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="24" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudices" id="o23591" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o23592" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o23591" id="r2181" name="retaining" negation="false" src="d0_s1" to="o23592" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to decumbent (greenish proximally), usually loosely strigose (hairs ascending, attenuate, basal-cells erect), uncommonly hirsute or hirtellous (hairs spreading-deflexed), eglandular.</text>
      <biological_entity id="o23593" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="decumbent" />
        <character is_modifier="false" modifier="usually loosely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="uncommonly" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o23594" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades usually oblanceolate, 15–70 × 2–6 (–8) mm (bases attenuate), margins entire (apices rounded to obtuse);</text>
      <biological_entity constraint="basal" id="o23595" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23596" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades linear to linear-oblong or narrowly lanceolate-oblong, mostly 10–40 × 1.5–4 mm, gradually reduced distally;</text>
      <biological_entity constraint="cauline" id="o23597" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-oblong or narrowly lanceolate-oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="mostly; gradually; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>faces strigose, eglandular.</text>
      <biological_entity id="o23598" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads 1–4.</text>
      <biological_entity id="o23599" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres 3.7–5.2 × (5–) 10–11 mm.</text>
      <biological_entity id="o23600" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="length" src="d0_s8" to="5.2" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s8" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 2–3 (–4) series, strigose to strigulose or finely hirsuto-villous, minutely glandular.</text>
      <biological_entity id="o23601" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" notes="" src="d0_s9" to="strigulose or finely hirsuto-villous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o23602" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s9" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <relation from="o23601" id="r2182" name="in" negation="false" src="d0_s9" to="o23602" />
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 35–60;</text>
      <biological_entity id="o23603" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s10" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas blue to pink or white, 5–6 mm, laminae weakly coiling.</text>
      <biological_entity id="o23604" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s11" to="pink or white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23605" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s11" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc corollas 2.5–4 mm.</text>
      <biological_entity constraint="disc" id="o23606" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 1.5–2 mm, 2-nerved, faces strigose;</text>
      <biological_entity id="o23607" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o23608" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi: outer of setae, inner of 12–20 bristles.</text>
      <biological_entity id="o23609" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o23610" name="seta" name_original="setae" src="d0_s14" type="structure" />
      <biological_entity id="o23611" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
      <relation from="o23609" id="r2183" name="outer of" negation="false" src="d0_s14" to="o23610" />
      <relation from="o23609" id="r2184" name="inner of" negation="false" src="d0_s14" to="o23611" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky or gravelly open slopes, sagebrush, pinyon-juniper, ponderosa pine, spruce-fir</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open slopes" modifier="rocky or gravelly" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="ponderosa pine" />
        <character name="habitat" value="spruce-fir" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(2000–)2800–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3500" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Abajo fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erigeron abajoensis is a segregate of E. caespitosus, separated from the latter only by its strigose (versus hirsute) stems and leaves and greater tendency for 1-nerved leaves. Further, some populations of E. abajoensis have plants variable in orientation of stem vestiture [both antrorsely appressed and deflexed; e.g., Cronquist 9430, Garfield Co., Utah (BRIT, NY); Heil and Clifford 17919, Apache Co., Ariz. (BRIT, SJNM)] and similar variation in cauline vestiture also occurs in E. caespitosus.</discussion>
  
</bio:treatment>