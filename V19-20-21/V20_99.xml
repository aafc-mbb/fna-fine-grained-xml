<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="treatment_page">61</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="species">nana</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 319. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066526</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Nuttall) D. C. Eaton" date="unknown" rank="species">nanus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species nanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–50 cm.</text>
      <biological_entity id="o28606" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading or recurved, green when young, soon becoming tan to brown, then nearly black when older, highly branched, twigs glabrous, glandular, usually resinous.</text>
      <biological_entity id="o28607" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading or recurved" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="tan" modifier="soon becoming; becoming" name="coloration" src="d0_s1" to="brown" />
        <character is_modifier="false" modifier="when older" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" modifier="highly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o28608" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="usually" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending to spreading;</text>
      <biological_entity id="o28609" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades usually elliptic to oblanceolate, occasionally spatulate, 10–15 × 0.5–1.5 mm, mostly adaxially sulcate, margins entire, midnerves usually obscure to weakly evident, apices acute, apiculate, faces glandular, sometimes irregularly gland-dotted (in shallow pits), resinous;</text>
      <biological_entity id="o28610" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually elliptic" name="shape" src="d0_s3" to="oblanceolate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="mostly adaxially" name="architecture" src="d0_s3" value="sulcate" value_original="sulcate" />
      </biological_entity>
      <biological_entity id="o28611" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o28612" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually obscure" name="prominence" src="d0_s3" to="weakly evident" />
      </biological_entity>
      <biological_entity id="o28613" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles present, persistent.</text>
      <biological_entity id="o28614" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sometimes irregularly" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in congested, cymiform arrays (0.5–2.5 cm wide).</text>
      <biological_entity id="o28615" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o28616" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o28615" id="r2650" name="in" negation="false" src="d0_s5" to="o28616" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 0.2–5 mm (mostly ebracteate, glabrous).</text>
      <biological_entity id="o28617" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres obconic, 5.5–7.5 × 2.5–4 mm.</text>
      <biological_entity id="o28618" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s7" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 22–30 in 4–5 series, mostly tan, lanceolate to elliptic, 2–6.5 × 0.5–1.2 mm, strongly unequal, outer sometimes herbaceous or herbaceous-tipped (body apices obtuse or truncate to retuse, appendages erect), midnerves not evident or slightly raised, slightly expanded subapically, (mostly margins narrowly membranous, entire) apices acute to acuminate or attenuate, mid often aristate to cuspidate, abaxial faces glabrous, resinous.</text>
      <biological_entity id="o28619" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o28620" from="22" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="false" modifier="mostly" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o28620" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o28621" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="herbaceous-tipped" value_original="herbaceous-tipped" />
      </biological_entity>
      <biological_entity id="o28622" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s8" value="evident" value_original="evident" />
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s8" value="raised" value_original="raised" />
        <character is_modifier="false" modifier="slightly; subapically" name="size" src="d0_s8" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o28623" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate or attenuate" />
      </biological_entity>
      <biological_entity constraint="mid" id="o28624" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="aristate" name="shape" src="d0_s8" to="cuspidate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28625" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 1–7;</text>
      <biological_entity id="o28626" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae elliptic, 3–4 × 0.8–1.3 mm.</text>
      <biological_entity id="o28627" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 4–8;</text>
      <biological_entity id="o28628" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 4.5–6.5 mm.</text>
      <biological_entity id="o28629" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae tan, narrowly oblanceoloid, 4–5.5 mm, glabrous or densely sericeous;</text>
      <biological_entity id="o28630" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="oblanceoloid" value_original="oblanceoloid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi tan, 4–5.5 mm. 2n = 18.</text>
      <biological_entity id="o28631" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="tan" value_original="tan" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28632" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arid rocky plains, desert mountain cliffs, crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arid rocky plains" />
        <character name="habitat" value="desert mountain cliffs" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Dwarf goldenbush</other_name>
  <discussion>Some populations of Ericameria nana exhibit extreme variation in leaf shape and in phyllary apex length and shape. Whether such variants represent distinct taxa remains to be tested.</discussion>
  
</bio:treatment>