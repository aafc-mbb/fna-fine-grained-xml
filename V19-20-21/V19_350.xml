<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">261</other_info_on_meta>
    <other_info_on_meta type="illustration_page">259</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lactuca</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">canadensis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 796. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus lactuca;species canadensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416714</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lactuca</taxon_name>
    <taxon_name authority="Elliott" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="Kuntze" date="unknown" rank="variety">latifolia</taxon_name>
    <taxon_hierarchy>genus Lactuca;species canadensis;variety latifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lactuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="(Michaux) Farwell" date="unknown" rank="variety">longifolia</taxon_name>
    <taxon_hierarchy>genus Lactuca;species canadensis;variety longifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lactuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="Wiegand" date="unknown" rank="variety">obovata</taxon_name>
    <taxon_hierarchy>genus Lactuca;species canadensis;variety obovata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lactuca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sagittifolia</taxon_name>
    <taxon_hierarchy>genus Lactuca;species sagittifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials, (15–) 40–200 (–450+) cm.</text>
      <biological_entity id="o6514" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="450" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves on proximal 1/2–3/4 of each stem;</text>
      <biological_entity id="o6515" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o6516" name="1/2-3/4" name_original="1/2-3/4" src="d0_s1" type="structure" />
      <biological_entity id="o6517" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o6515" id="r617" name="on" negation="false" src="d0_s1" to="o6516" />
      <relation from="o6516" id="r618" name="part_of" negation="false" src="d0_s1" to="o6517" />
    </statement>
    <statement id="d0_s2">
      <text>blades of undivided cauline leaves oblong, obovate, or lanceolate to spatulate or lance-linear, margins entire or denticulate, midribs sometimes sparsely pilose.</text>
      <biological_entity id="o6518" name="blade" name_original="blades" src="d0_s2" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="spatulate or lance-linear" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="spatulate or lance-linear" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o6519" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity id="o6520" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o6521" name="midrib" name_original="midribs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o6518" id="r619" name="part_of" negation="false" src="d0_s2" to="o6519" />
    </statement>
    <statement id="d0_s3">
      <text>Heads in ± corymbiform to paniculiform arrays.</text>
      <biological_entity id="o6522" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o6523" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="less corymbiform" is_modifier="true" name="architecture" src="d0_s3" to="paniculiform" />
      </biological_entity>
      <relation from="o6522" id="r620" name="in" negation="false" src="d0_s3" to="o6523" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres 10–12+ mm.</text>
      <biological_entity id="o6524" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries usually reflexed in fruit.</text>
      <biological_entity id="o6525" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character constraint="in fruit" constraintid="o6526" is_modifier="false" modifier="usually" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o6526" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Florets 15–20+;</text>
      <biological_entity id="o6527" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s6" to="20" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas bluish or yellowish, usually deliquescent.</text>
      <biological_entity id="o6528" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="bluish" value_original="bluish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="usually" name="architecture_or_texture" src="d0_s7" value="deliquescent" value_original="deliquescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae: bodies brown (often mottled), ± flattened, elliptic, 5–6 mm, beaks ± filiform, 1–3 mm, faces 1 (–3) -nerved;</text>
      <biological_entity id="o6529" name="cypsela" name_original="cypselae" src="d0_s8" type="structure" />
      <biological_entity id="o6530" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6531" name="beak" name_original="beaks" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6532" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1(-3)-nerved" value_original="1(-3)-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi white, 5–6 mm. 2n = 34.</text>
      <biological_entity id="o6533" name="cypsela" name_original="cypselae" src="d0_s9" type="structure" />
      <biological_entity id="o6534" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6535" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, swamps, salt marshes, thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., N.B., N.S., Ont., P.E.I., Que., Yukon; Ala., Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Mexico; Central America; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>