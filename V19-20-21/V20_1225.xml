<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="treatment_page">554</other_info_on_meta>
    <other_info_on_meta type="illustration_page">555</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="Richardson in J. Franklin et al." date="1823" rank="species">lugens</taxon_name>
    <place_of_publication>
      <publication_title>in J. Franklin et al., Narr. Journey Polar Sea,</publication_title>
      <place_in_publication>748. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species lugens</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067492</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">glaucescens</taxon_name>
    <taxon_hierarchy>genus Senecio;species glaucescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">imbricatus</taxon_name>
    <taxon_hierarchy>genus Senecio;species imbricatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">integerrimus</taxon_name>
    <taxon_name authority="(Richardson) B. Boivin" date="unknown" rank="variety">lugens</taxon_name>
    <taxon_hierarchy>genus Senecio;species integerrimus;variety lugens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (10–) 20–35 (–50) cm (rhizomes suberect to creeping).</text>
      <biological_entity id="o4133" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage loosely, often unevenly, floccose-tomentose, glabrescent.</text>
      <biological_entity id="o4134" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often unevenly; unevenly" name="pubescence" src="d0_s1" value="floccose-tomentose" value_original="floccose-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems single or clustered.</text>
      <biological_entity id="o4135" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves reduced distally;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o4136" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades narrowly obovate to oblanceolate, (4–) 8–18 (–25) cm, bases tapered, margins subentire to dentate (denticles callous; mid and distal leaves bractlike, clasping).</text>
      <biological_entity id="o4137" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly obovate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4138" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o4139" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s5" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads (2–) 7–12 (–20+) in corymbiform arrays.</text>
      <biological_entity id="o4140" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s6" to="7" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="20" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o4141" from="7" name="quantity" src="d0_s6" to="12" />
      </biological_entity>
      <biological_entity id="o4141" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 2–5 linear bractlets (1–2 mm).</text>
      <biological_entity id="o4142" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o4143" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <relation from="o4142" id="r377" name="consist_of" negation="false" src="d0_s7" to="o4143" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries (± 8) ± 13 (± 21), 4–7 mm, tips black.</text>
      <biological_entity id="o4144" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s8" value="8" value_original="8" />
        <character modifier="more or less" name="quantity" src="d0_s8" value="13" value_original="13" />
        <character modifier="more or less" name="quantity" src="d0_s8" value="21" value_original="21" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4145" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets (± 5) ± 8 (± 13);</text>
      <biological_entity id="o4146" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s9" value="5" value_original="5" />
        <character modifier="more or less" name="quantity" src="d0_s9" value="8" value_original="8" />
        <character modifier="more or less" name="quantity" src="d0_s9" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae 8–10 (–15) mm.</text>
      <biological_entity constraint="corolla" id="o4147" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 40, 80.</text>
      <biological_entity id="o4148" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4149" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows, gravelly streambeds, open woods in alpine or boreal sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="gravelly streambeds" />
        <character name="habitat" value="open woods" constraint="in alpine or boreal sites" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="boreal sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska, Mont., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion>Senecio lugens varies greatly in robustness across its range. It is scattered widely in the Rocky Mountain uplift and adjacent regions from northern Wyoming to Alaska; it is disjunct in the Olympic Peninsula, Washington. Superficially similar to S. integerrimus, S. lugens has well-developed, coarse, spreading rootstocks with branching roots; S. integerrimus arises from foreshortened, buttonlike caudices with abundant unbranched, fleshy-fibrous roots.</discussion>
  
</bio:treatment>