<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="treatment_page">238</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Nuttall) Shinners" date="1951" rank="species">sessiliflora</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">sessiliflora</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species sessiliflora;subspecies sessiliflora</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068486</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–30, decumbent to ascending-erect, sparsely to moderately hispid, densely stipitate-glandular distally.</text>
      <biological_entity id="o6644" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="30" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s0" to="ascending-erect" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s0" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves sessile, blades lanceolate, 9–16 (–21) × 4–7 mm, reduced distally, bases rounded, margins strongly undulate, proximal large cilia grading into distal, small ones, faces moderately to densely hispido-strigose, sparsely to moderately glandular;</text>
      <biological_entity constraint="cauline" id="o6645" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6646" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="21" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s1" to="16" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6647" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o6648" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6649" name="cilium" name_original="cilia" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o6650" name="one" name_original="ones" src="d0_s1" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o6651" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s1" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
      <relation from="o6649" id="r602" name="into" negation="false" src="d0_s1" to="o6650" />
    </statement>
    <statement id="d0_s2">
      <text>distal green, not stiff, faces usually sparsely to moderately, rarely densely hispido-strigose, densely stipitate-glandular.</text>
      <biological_entity constraint="distal" id="o6652" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o6653" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately; rarely densely" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncle bracts 3–6, proximal lanceolate, leafy, sometimes distally reduced and phyllary-like, often 1–3+ larger, leaflike bracts subtending, equaling or surpassing heads.</text>
      <biological_entity constraint="peduncle" id="o6654" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="sometimes distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="phyllary-like" value_original="phyllary-like" />
        <character char_type="range_value" from="1" modifier="often" name="quantity" src="d0_s3" to="3" upper_restricted="false" />
        <character is_modifier="false" name="size" src="d0_s3" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o6655" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s3" value="subtending" value_original="subtending" />
        <character is_modifier="false" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s3" value="surpassing heads" value_original="surpassing heads" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres (7–) 8.4–10.4 (–11) mm.</text>
      <biological_entity id="o6656" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="8.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="8.4" from_unit="mm" name="some_measurement" src="d0_s4" to="10.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 14–24;</text>
      <biological_entity id="o6657" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s5" to="24" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminae 5–9 (–11) mm.</text>
      <biological_entity id="o6658" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets: corolla lobes sparsely pilose (hairs 0.15–0.4 mm).</text>
      <biological_entity id="o6659" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure" />
      <biological_entity constraint="corolla" id="o6660" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypsela faces sparsely strigose.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 18.</text>
      <biological_entity constraint="cypsela" id="o6661" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6662" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Jun–Sep+.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes, strand, mud flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="strand" />
        <character name="habitat" value="mud flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–60 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="60" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  <other_name type="common_name">Beach goldenaster</other_name>
  <discussion>The peak blooming period for subsp. sessiliflora is mid to late summer, but rare individuals can produce some heads throughout much of the year (San Diego County to northern Baja California). The type comes from Santa Barbara, where the subspecies now appears to be extinct. The plants vary greatly in habit. Some are sometimes short and herbaceous, the shoots dying back at the end of the growth season. In the northern Baja California coastal dunes are meter-high shrubs, the shoots of which have not been killed back to rootstock level. The shift from densely hairy proximal cauline leaves to densely glandular distal ones occurs abruptly in some shoots and gradually in others. The subspecies has become very rare in California.</discussion>
  
</bio:treatment>