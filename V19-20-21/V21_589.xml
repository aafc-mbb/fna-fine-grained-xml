<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="treatment_page">241</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">thymophylla</taxon_name>
    <taxon_name authority="(de Candolle) Strother" date="1986" rank="species">micropoides</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>11: 377. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus thymophylla;species micropoides</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067748</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalopsis</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">micropoides</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 258. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalopsis;species micropoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="(de Candolle) Loesener" date="unknown" rank="species">micropoides</taxon_name>
    <taxon_hierarchy>genus Dyssodia;species micropoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, ashy white, to 15+ cm, arachnose to floccose.</text>
      <biological_entity id="o24924" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="ashy white" value_original="ashy white" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="arachnose" name="pubescence" src="d0_s0" to="floccose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, often prostrate.</text>
      <biological_entity id="o24925" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly alternate;</text>
      <biological_entity id="o24926" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades not lobed, spatulate, 10–25 × 3–6 mm, margins entire or toothed.</text>
      <biological_entity id="o24927" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24928" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 0–5+ mm, tomentose.</text>
      <biological_entity id="o24929" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of 3–5 linear bractlets, lengths 1/2+ phyllaries.</text>
      <biological_entity id="o24930" name="calyculus" name_original="calyculi" src="d0_s5" type="structure" />
      <biological_entity id="o24931" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o24932" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <relation from="o24930" id="r1693" name="consist_of" negation="false" src="d0_s5" to="o24931" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres obconic, 6–7 mm.</text>
      <biological_entity id="o24933" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 12–14, margins of outer distinct less than 1/5 their lengths, abaxial faces tomentose.</text>
      <biological_entity id="o24934" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="14" />
      </biological_entity>
      <biological_entity id="o24935" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s7" to="1/5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o24936" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o24937" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <relation from="o24935" id="r1694" name="part_of" negation="false" src="d0_s7" to="o24936" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 10–15;</text>
      <biological_entity id="o24938" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas bright-yellow, becoming greenish, laminae 5 × 3 mm.</text>
      <biological_entity id="o24939" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o24940" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character name="length" src="d0_s9" unit="mm" value="5" value_original="5" />
        <character name="width" src="d0_s9" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets ca. 60;</text>
      <biological_entity id="o24941" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="60" value_original="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 3 mm.</text>
      <biological_entity id="o24942" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2.3–3 mm;</text>
      <biological_entity id="o24943" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 5 erose scales to 1 mm alternating with 5 lanceolate, 1-aristate scales to 3 mm. 2n = 16.</text>
      <biological_entity id="o24944" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character constraint="with scales" constraintid="o24946" is_modifier="false" name="arrangement" src="d0_s13" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o24945" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s13" value="erose" value_original="erose" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24946" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="true" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s13" value="1-aristate" value_original="1-aristate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24947" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
      <relation from="o24944" id="r1695" name="consist_of" negation="false" src="d0_s13" to="o24945" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round, following rains.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous outcrops and derived soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous outcrops" />
        <character name="habitat" value="derived soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–200+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200+" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>