<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">182</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="treatment_page">414</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Coville" date="1893" rank="genus">OROCHAENACTIS</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>4: 134, plate 10. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus OROCHAENACTIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek horos, mountain, and generic name Chaenactis</other_info_on_name>
    <other_info_on_name type="fna_id">123215</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–25 cm.</text>
      <biological_entity id="o19726" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, simple or ± branched (very slender).</text>
      <biological_entity id="o19727" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite (proximal) or alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>obscurely petiolate or sessile;</text>
      <biological_entity id="o19728" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear to narrowly oblong-oblanceolate, margins entire, faces tomentose to glabrate, glanddotted.</text>
      <biological_entity id="o19729" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly oblong-oblanceolate" />
      </biological_entity>
      <biological_entity id="o19730" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19731" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, borne singly or (2–5) in clusters (at stem tips).</text>
      <biological_entity id="o19732" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in clusters" from="2" name="atypical_quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric or narrowly turbinate, 3–4 (–6) mm diam.</text>
      <biological_entity id="o19733" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 4–7 in ± 1 series (distinct, oblong, unequal, glandular-puberulent).</text>
      <biological_entity id="o19734" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o19735" from="4" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <biological_entity id="o19735" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles ± flat, ± knobby, epaleate.</text>
      <biological_entity id="o19736" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="knobby" value_original="knobby" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0.</text>
      <biological_entity id="o19737" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 4–9;</text>
      <biological_entity id="o19738" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, lobes 5, short-triangular (anther bases short-sagittate, appendages short-triangular; style-branches linear, tapering).</text>
      <biological_entity id="o19739" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o19740" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s12" value="short-triangular" value_original="short-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae terete, linear-clavate, narrowly ribbed, minutely glandular-puberulent, glabrescent;</text>
      <biological_entity id="o19741" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s13" value="linear-clavate" value_original="linear-clavate" />
        <character is_modifier="false" modifier="narrowly" name="architecture_or_shape" src="d0_s13" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s13" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi falling (in rings), of 11–17 (basally connate) oblanceolate, fringed, obtuse scales.</text>
      <biological_entity id="o19743" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="11" is_modifier="true" name="quantity" src="d0_s14" to="17" />
        <character is_modifier="true" name="shape" src="d0_s14" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="true" name="shape" src="d0_s14" value="fringed" value_original="fringed" />
        <character is_modifier="true" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o19742" id="r1343" name="consist_of" negation="false" src="d0_s14" to="o19743" />
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o19742" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s14" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o19744" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>379.</number>
  <other_name type="common_name">California mountain-pincushion</other_name>
  <discussion>Species 1.</discussion>
  
</bio:treatment>