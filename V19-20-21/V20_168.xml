<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">88</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(de Candolle) Nuttall" date="1840" rank="genus">amphiachyris</taxon_name>
    <taxon_name authority="(Shinners) Solbrig" date="1960" rank="species">amoena</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>62: 52. 1960</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus amphiachyris;species amoena</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066060</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthocephalum</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="species">amoenum</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>19: 77. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Xanthocephalum;species amoenum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gutierrezia</taxon_name>
    <taxon_name authority="(Shinners) Diggs" date="unknown" rank="species">amoena</taxon_name>
    <place_of_publication>
      <publication_title>Lipscomb &amp; O’Kennon</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Gutierrezia;species amoena;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–40 (–60) cm.</text>
      <biological_entity id="o4434" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Primary-stems usually (2–) 5 mm diam.</text>
      <biological_entity id="o4435" name="primary-stem" name_original="primary-stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s1" to="5" to_inclusive="false" to_unit="mm" />
        <character name="diameter" src="d0_s1" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear, rarely linear-lanceolate, 5–25 (–35) × 0.2–1 (–2) mm.</text>
      <biological_entity id="o4436" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in open paniculiform arrays.</text>
      <biological_entity id="o4437" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o4438" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o4437" id="r411" name="in" negation="false" src="d0_s3" to="o4438" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 3–4 × 1–2 mm.</text>
      <biological_entity id="o4439" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles with hooked, swollen-based hairs.</text>
      <biological_entity id="o4440" name="receptacle" name_original="receptacles" src="d0_s5" type="structure" />
      <biological_entity id="o4441" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="hooked" value_original="hooked" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="swollen-based" value_original="swollen-based" />
      </biological_entity>
      <relation from="o4440" id="r412" name="with" negation="false" src="d0_s5" to="o4441" />
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 2–3 mm, 4–6-ribbed, long-setulose.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 8.</text>
      <biological_entity id="o4442" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="4-6-ribbed" value_original="4-6-ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="long-setulose" value_original="long-setulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4443" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="late Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous soils and gravels on or near limestone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soils" modifier="calcareous" />
        <character name="habitat" value="gravels" />
        <character name="habitat" value="limestone outcrops" modifier="on or near" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–650 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="650" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Texas broomweed</other_name>
  
</bio:treatment>