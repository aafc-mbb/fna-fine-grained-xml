<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
    <other_info_on_meta type="illustration_page">236</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="(E. James) Torrey &amp; A. Gray" date="1843" rank="species">runcinata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 487. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species runcinata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066454</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hieracium</taxon_name>
    <taxon_name authority="E. James" date="unknown" rank="species">runcinatum</taxon_name>
    <place_of_publication>
      <publication_title>Account Exped. Pittsburgh</publication_title>
      <place_in_publication>1: 453. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hieracium;species runcinatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–65 cm (taproots relatively long, caudices swollen).</text>
      <biological_entity id="o4912" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="65" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3, erect or ascending, scapiform, branched near middles, glabrous or hispid, sometimes stipitate-glandular distally.</text>
      <biological_entity id="o4913" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="scapiform" value_original="scapiform" />
        <character constraint="near middles" constraintid="o4914" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o4914" name="middle" name_original="middles" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal (rosettes);</text>
      <biological_entity id="o4916" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o4915" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades elliptic, lanceolate, linear, oblanceolate, obovate, or spatulate, 3–30 × 0.5–8 cm (bases attenuate) margins usually entire or weakly dentate, sometimes serrate, dentate, or pinnately lobed, apices rounded, faces glabrous or hispid to hispidulous (sometimes glaucous).</text>
      <biological_entity id="o4917" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4918" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o4919" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4920" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s4" to="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (1–) 3–15 (–30), borne singly or in ± corymbiform arrays.</text>
      <biological_entity id="o4921" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="30" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="15" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in more or less corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o4922" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o4921" id="r482" name="in" negation="false" src="d0_s5" to="o4922" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 5–12, narrowly triangular, glabrous or tomentulose bractlets 1–3 mm.</text>
      <biological_entity id="o4923" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="of 5-12; narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity id="o4924" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres turbinate-campanulate, 7–21 × 8–12 mm.</text>
      <biological_entity id="o4925" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="21" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 10–16, lanceolate or oblong, 8–10 mm, (bases keeled and thickened, margins scarious) apices usually acute, sometimes attenuate or obtuse (often ciliate-tufted), abaxial faces glabrous or tomentulose, sometimes stipitate-glandular, adaxial glabrous.</text>
      <biological_entity id="o4926" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="16" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4927" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4928" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4929" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 20–50;</text>
      <biological_entity id="o4930" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas golden yellow, 9–18 mm.</text>
      <biological_entity id="o4931" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae dark to golden reddish or yellowish-brown, fusiform, 3.5–8 mm, tapered distally or beaked, ribs 10–13 (strong);</text>
      <biological_entity id="o4932" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark" name="coloration" src="d0_s11" to="golden reddish or yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s11" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o4933" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi white, 4–9 mm.</text>
      <biological_entity id="o4934" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Sask.; Ariz., Calif., Colo., Idaho, Minn., Mont., N.Dak., N.Mex., Nebr., Nev., Oreg., S.Dak., Tex., Utah, Wash., Wyo.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion>Subspecies 7 (7 in the flora).</discussion>
  <discussion>Crepis runcinata is recognized by its basal rosettes of weakly dentate or almost entire leaves, scapiform stems, branching near middles, and reduced cauline leaves. The stems and leaves are usually glabrous. Multiple subspecies were described by E. B. Babcock (1947); the variation is continuous. Babcock suggested that this is the only American species that shows a relationship to Asian species.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries eglandular</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries usually stipitate-glandular or finely glandular-hispid</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves obovate, oblanceolate, or spatulate, 1.5–4 cm wide</description>
      <determination>20d Crepis runcinata subsp. glauca</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves narrowly oblanceolate or linear, 0.5–2 cm wide</description>
      <determination>20c Crepis runcinata subsp. barberi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Teeth of leaves prominently white-tipped; phyllaries broadly lanceolate or oblong (California, Nevada, Oregon)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Teeth of leaves not prominently white-tipped (or only minutely so); phyllaries lanceolate</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucres 19–21 mm; phyllaries broadly lanceolate, apices long-acuminate; cypselae ± distinctly beaked</description>
      <determination>20b Crepis runcinata subsp. andersonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucres 10–13 mm; phyllaries oblong, apices obtuse or acute; cypselae not beaked</description>
      <determination>20g Crepis runcinata subsp. imbricata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves coarsely dentate or pinnately lobed (glaucous)</description>
      <determination>20e Crepis runcinata subsp. hallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves remotely toothed or serrate, or pinnately lobed, or entire</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves 0.5–3.5 cm wide</description>
      <determination>20a Crepis runcinata subsp. runcinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves 2.5–8 cm wide</description>
      <determination>20f Crepis runcinata subsp. hispidulosa</determination>
    </key_statement>
  </key>
</bio:treatment>