<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gary I. Baird</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="mention_page">335</other_info_on_meta>
    <other_info_on_meta type="treatment_page">323</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Rafinesque" date="1817" rank="genus">AGOSERIS</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Ludov.,</publication_title>
      <place_in_publication>58. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus AGOSERIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek agos, leader, and seris, chicory; allusion unclear</other_info_on_name>
    <other_info_on_name type="fna_id">100829</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Dioecious.</text>
      <biological_entity id="o5596" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Plants 3–14 cm (bases woody).</text>
      <biological_entity id="o5597" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="14" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stolons none.</text>
      <biological_entity id="o5598" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="0" value_original="none" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves absent at flowering.</text>
      <biological_entity constraint="basal" id="o5599" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves linear-lanceolate to cuneate-oblanceolate, 11–35 × 2–6 imm, acute, not flagged (apices acute), faces gray-pubescent.</text>
      <biological_entity constraint="cauline" id="o5600" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="cuneate-oblanceolate" />
        <character char_type="range_value" from="11" name="quantity" src="d0_s4" to="35" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="flagged" value_original="flagged" />
      </biological_entity>
      <biological_entity id="o5601" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="gray-pubescent" value_original="gray-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 3–25 in corymbiform to paniculiform arrays.</text>
      <biological_entity id="o5602" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o5603" from="3" name="quantity" src="d0_s5" to="25" />
      </biological_entity>
      <biological_entity id="o5603" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s5" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres: staminate 6–8 mm;</text>
      <biological_entity id="o5604" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 6–8 mm.</text>
      <biological_entity id="o5605" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries distally red to pink, light-brown, or white.</text>
      <biological_entity id="o5606" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="distally red" name="coloration" src="d0_s8" to="pink light-brown or white" />
        <character char_type="range_value" from="distally red" name="coloration" src="d0_s8" to="pink light-brown or white" />
        <character char_type="range_value" from="distally red" name="coloration" src="d0_s8" to="pink light-brown or white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas: staminate 3–4.5 mm;</text>
      <biological_entity id="o5607" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate 5–6 mm.</text>
      <biological_entity id="o5608" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2–2.5 mm, pubescent and papillate;</text>
      <biological_entity id="o5609" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: staminate 6–7 mm (capillary);</text>
      <biological_entity id="o5610" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistillate 6–7 mm. 2n = 28.</text>
      <biological_entity id="o5611" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5612" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>66.</number>
  <other_name type="common_name">Mountain- or false dandelion</other_name>
  <discussion>Species 11 (10, including 1 hybrid, in the flora).</discussion>
  <discussion>Agoseris consists of widespread species that individually exhibit great morphologic plasticity. Difficulty in correctly identifying individual specimens is compounded by traits that may vary from region to region, the perpetuation of misleading or inaccurate traits in the literature, and the presence of intermediates. Correct identification of Agoseris specimens can be assisted by knowing that species may exhibit variable traits (e.g., pubescence, corolla color, cypsela morphology), some species have leaf lobing variable on single plants (e.g., outermost entire versus inner lobed), and intermediate specimens may occur with any sympatric taxa. Hybridization among members of the genus is common, especially among polyploid taxa, and some hybrid populations appear to be persistent. Autogamy has been demonstrated in some species (K. L. Chambers 1963) and is suspected in others. It appears to be correlated with a reduction in corolla and anther size. Autogamous populations or taxa often exhibit seemingly unique features that appear localized. Attempts at naming these variant populations or regional phases have resulted in a large number of synonyms.</discussion>
  <discussion>Agoseris has a New World, amphitropical distribution. All of the species are restricted to North America except A. coronopifolia (D’Urville) K. L. Chambers, which is found in temperate regions of southern South America. The South American disjunction appears to be the result of long-distance dispersal from North America (K. L. Chambers 1963).</discussion>
  <discussion>Agoseris appears to be most closely related to Nothocalaïs.</discussion>
  <discussion>Cryptopleura Nuttall, referable here, is a rejected name.</discussion>
  <discussion>In keys and descriptions, lengths of cypselae include beaks.</discussion>
  <references>
    <reference>Baird, G. I. 1996. The Systematics of Agoseris (Asteraceae: Lactuceae). Ph.D. dissertation. University of Texas.</reference>
    <reference>Jones, Q. 1954. Monograph of Agoseris, Tribe Cichorieae. Ph.D. dissertation. Harvard University.</reference>
    <reference>Chambers, K. L. 1963. Amphitropical species pairs in Microseris and Agoseris (Compositae: Cichorieae). Quart. Rev. Biol. 38: 124–140.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals</description>
      <determination>9 Agoseris heterophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas orange, pink, red, or purplish (often drying purplish)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas yellow, outermost often each with abaxial purplish stripe (often drying whitish, purple stripe still evident)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peduncles (and phyllaries) eglandular; widespread in North America</description>
      <determination>2 Agoseris aurantiaca</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peduncles (and phyllaries) ± stipitate-glandular; California, Oregon, and Washington</description>
      <determination>10 Agoseris ×elata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cypsela beaks 1–4(–10) mm (lengths to 1/2 bodies); inner phyllaries not elongating in fruit</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cypsela beaks 5+ mm, lengths usually equaling or greater than bodies, if less than 5 mm, beaks usually 1/2+ bodies; inner phyllaries elongating in fruit</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf margins usually lobed, rarely entire, lobes (3–)5–8 pairs, retrorse to spreading; peduncles (and phyllaries) usually hairy to lanate, sometimes glabrous, eglandular; cypsela beaks (3–)4–10 mm, lengths (1/2–)2 times bodies</description>
      <determination>3 Agoseris parviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf margins entire or toothed to lobed, teeth or lobes usually 2–3 pairs, antrorse to spreading, or diverging; peduncles (and phyllaries) glabrous or puberulent to lanate, sometimes stipitate-glandular or eglandular; cypsela beaks 1–4 mm, lengths to 1/2 bodies</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Peduncles glabrous, or basally glabrate, apically puberulent to lanate, sometimes stipitate-glandular; leaves usually erect, sometimes decumbent, margins usually entire, sometimes dentate, rarely lobed or lacerate; receptacles sometimes paleate; phyllaries in 2–3 series; widespread, various soils and elevations, n Great Plains westward</description>
      <determination>1 Agoseris glauca</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Peduncles basally lanate, apically hairy to villous and stipitate-glandular; leaves usually decumbent or prostrate, margins usually dentate or lobed, rarely entire; receptacles rarely paleate; phyllaries in 2–4(–6) series; mostly at high elevations, volcanic or pyroclastic soils, Sierra Nevada and s Cascade Mountains, sporadically eastward to Blue Mountains and Great Basin</description>
      <determination>4 Agoseris monticola</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Peduncles and phyllaries ± stipitate-glandular</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Peduncles and phyllaries glabrous or hairy, eglandular</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves 3–10(–15) cm (plants usually ± caulescent, stems often buried by drifting sand, appearing pseudorhizomatous, sometimes acaulescent); mostly coastal dunes and beach heads, Pacific Coast</description>
      <determination>7 Agoseris apargioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves (7–)10–30 cm (plants acaulescent); mostly grassy hills, meadows, or lowland prairies (not coastal sand dunes)</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf margins usually pinnately lobed, lobes 5–7(–9) pairs; corolla tubes 3–6 mm; cypselae 9–14 mm; pappus bristles in 3–4 series, 6–10 mm; Coast Ranges of California, especially around San Francisco Bay</description>
      <determination>8 Agoseris hirsuta</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf margins entire or laciniately pinnatifid, lobes 2–4 pairs; corolla tubes 8–10 mm; cypselae 14–20 mm; pappus bristles in 2–3 series, 10–14 mm; Washington to California (not Coast Ranges)</description>
      <determination>10 Agoseris ×elata</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Cypsela beaks (9–)10–25 mm, lengths usually 3–4 times bodies; phyllaries in 3–6 series</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Cypsela beaks (1–)3–10 mm, lengths usually 1/2–2 times bodies; phyllaries in 2–3 series</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf lobes antrorse to spreading; corolla tubes 4–7(–10) mm, ligules 3–7 mm, anthers 1–3 mm; pappus bristles in 2–3 series, 7–15 mm.</description>
      <determination>5 Agoseris grandiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf lobes retrorse to spreading; corolla tubes (8–)10–20 mm, ligules 6–12(–16) mm, anthers 2–5 mm; pappus bristles in 4–6 series, (11–) 15–20 mm</description>
      <determination>6 Agoseris retrorsa</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Corolla tubes 2–5.5 mm, ligules 3–16 mm, anthers 1.5–4.5 mm; cypsela bodies 3–5 mm; leaves 3–10(–15) cm; Pacific shore coastal dunes.</description>
      <determination>7 Agoseris apargioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Corolla tubes (4–)6–15 mm, ligules (4–)6–20 mm, anthers 2–5 mm; cypsela bodies 5–9 mm; leaves (5–)10–38 cm; widespread North America east of Pacific coast ranges</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Corolla ligules 4–12 mm; inner phyllaries elongating in fruit; leaf margins entire or laciniately lobed, lobes 2–4 pairs, spreading to antrorse; montane forests to alpine tundra, often disturbed habitats</description>
      <determination>2 Agoseris aurantiaca</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Corolla ligules 10–20 mm; inner phyllaries not elongating in fruit; leaf margins usually lobed, rarely entire, lobes (3–)5–8 pairs, mostly retrorse; sagebrush steppes, grasslands, pinyon-juniper woodlands, open forests at lower elevations</description>
      <determination>3 Agoseris parviflora</determination>
    </key_statement>
  </key>
</bio:treatment>