<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="treatment_page">278</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">centromadia</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1894" rank="species">parryi</taxon_name>
    <taxon_name authority="(D. D. Keck) B. G. Baldwin" date="1999" rank="subspecies">australis</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 466. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus centromadia;species parryi;subspecies australis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068129</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">parryi</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="subspecies">australis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>3: 15. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species parryi;subspecies australis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="(D. D. Keck) D. D. Keck" date="unknown" rank="species">australis</taxon_name>
    <taxon_hierarchy>genus Hemizonia;species australis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves (and peduncular bracts) villous or hirsute to hirtellous, coarsely stipitate-glandular (glands yellow).</text>
      <biological_entity id="o18602" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s0" to="hirtellous" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres 2.5–6 (–7) mm.</text>
      <biological_entity id="o18603" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Paleae sometimes with 2 purple lines (along inner edges of scarious margins).</text>
      <biological_entity id="o18604" name="palea" name_original="paleae" src="d0_s2" type="structure" />
      <biological_entity id="o18605" name="line" name_original="lines" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s2" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o18604" id="r1267" name="with" negation="false" src="d0_s2" to="o18605" />
    </statement>
    <statement id="d0_s3">
      <text>Ray laminae 2–4 mm.</text>
      <biological_entity constraint="ray" id="o18606" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Anthers reddish to dark purple.</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 22.</text>
      <biological_entity id="o18607" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s4" to="dark purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18608" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Inner edges of salt marshes, coastal grasslands, vernal pool edges and beds, openings in coastal scrub, often ± saline or alkaline sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="inner edges" constraint="of salt marshes , coastal grasslands , vernal pool edges and beds , openings in coastal scrub , often" />
        <character name="habitat" value="salt marshes" />
        <character name="habitat" value="coastal grasslands" />
        <character name="habitat" value="vernal pool edges" />
        <character name="habitat" value="beds" />
        <character name="habitat" value="openings" constraint="in coastal scrub" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="saline" modifier="often" />
        <character name="habitat" value="alkaline sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies australis occurs along the southern coast of California. Natural, ± sterile hybrids with Deinandra fasciculata have been documented from Santa Barbara County (B. D. Tanowitz 1977).</discussion>
  
</bio:treatment>