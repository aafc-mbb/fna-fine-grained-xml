<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">silphium</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">perfoliatum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1232. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus silphium;species perfoliatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417268</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants caulescent, 75–300 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted.</text>
      <biological_entity id="o2014" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character char_type="range_value" from="75" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems square, glabrous, hispid, or scabrous.</text>
      <biological_entity id="o2015" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="square" value_original="square" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal caducous;</text>
      <biological_entity id="o2016" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o2017" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline usually opposite, rarely whorled (in 3s), petiolate or sessile;</text>
      <biological_entity id="o2018" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o2019" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s4" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades deltate, lanceolate, or ovate, 2–41 × 0.5–24 cm, bases attenuate or truncate (distal connate-perfoliate), margins entire, dentate, or bidentate, apices acuminate to acute, faces scabrous to hispid.</text>
      <biological_entity id="o2020" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o2021" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="41" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="24" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2022" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o2023" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="bidentate" value_original="bidentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o2024" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o2025" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 25–37 in 2–3 series, outer appressed, apices acute to acuminate, abaxial faces scabrous or hispid.</text>
      <biological_entity id="o2026" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o2027" from="25" name="quantity" src="d0_s6" to="37" />
      </biological_entity>
      <biological_entity id="o2027" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o2028" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o2029" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2030" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 17–35;</text>
      <biological_entity id="o2031" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="17" name="quantity" src="d0_s7" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow.</text>
      <biological_entity id="o2032" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 85–150 (–200);</text>
      <biological_entity id="o2033" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="150" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="200" />
        <character char_type="range_value" from="85" name="quantity" src="d0_s9" to="150" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow.</text>
      <biological_entity id="o2034" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 8–12 × 5–9 mm;</text>
      <biological_entity id="o2035" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi 0.5–1.5 mm.</text>
      <biological_entity id="o2036" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Ill., Ind., Iowa, Kans., Ky., La., Mass., Mich., Minn., Miss., Mo., N.C., N.Dak., N.J., N.Y., Nebr., Ohio, Okla., Pa., S.Dak., Tenn., Tex., Va., Vt., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Cup plant</other_name>
  <discussion>Varieties 2 (2 in flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous or sparsely scabrous; peduncles glabrous</description>
      <determination>5a Silphium perfoliatum var. connatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous, hispid, or scabrous; peduncles usually scabrous to hispid</description>
      <determination>5b Silphium perfoliatum var. perfoliatum</determination>
    </key_statement>
  </key>
</bio:treatment>