<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="treatment_page">598</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Rydberg) W. A. Weber &amp; Á. Löve" date="1981" rank="species">sanguisorboides</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>49: 48. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species sanguisorboides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067267</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">sanguisorboides</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>27: 170, plate 5, fig. 14. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species sanguisorboides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or biennials, 30–50+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>± fibrous-rooted (bases creeping, ascending to erect).</text>
      <biological_entity id="o424" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 or 2–3, clustered, glabrous or leaf-axils tomentose.</text>
      <biological_entity id="o426" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o427" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline) petiolate;</text>
      <biological_entity constraint="basal" id="o428" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades broadly oblanceolate (pinnately lobed, lateral lobes 2–3+ pairs, their bases petioluliform, terminal lobes larger than laterals, ovate to reniform, midribs not winged), 60–120+ × 20–60 mm, bases contracted, ultimate margins crenate to crenate-dentate.</text>
      <biological_entity id="o429" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="120" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o430" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o431" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="crenate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves gradually reduced (petiolate or sessile; lyrate to sublyrate, midribs winged, terminal lobes weakly distinct, shallowly dentate).</text>
      <biological_entity constraint="cauline" id="o432" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 3–8+ in subumbelliform or compound, cymiform arrays (of 2–4+ cymiform clusters of 2–5+ heads each).</text>
      <biological_entity id="o433" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o434" from="3" name="quantity" src="d0_s6" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o434" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="subumbelliform" value_original="subumbelliform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles inconspicuously bracteate, glabrous or tomentose proximally.</text>
      <biological_entity id="o435" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi inconspicuous.</text>
      <biological_entity id="o436" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 13, bright green (tips light green to yellow), 4–7 mm, glabrous.</text>
      <biological_entity id="o437" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright green" value_original="bright green" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8;</text>
      <biological_entity id="o438" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 6–12 mm.</text>
      <biological_entity constraint="corolla" id="o439" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 35–50+;</text>
      <biological_entity id="o440" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s12" to="50" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 2.5–3.5 mm, limbs 2–3 mm.</text>
      <biological_entity id="o441" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o442" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1.5–2 mm, glabrous;</text>
      <biological_entity id="o443" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 4.5–5.5 mm. 2n = 46.</text>
      <biological_entity id="o444" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o445" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jul–mid Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Sep" from="late Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp, open meadows, spruce-aspen forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="damp" />
        <character name="habitat" value="open meadows" />
        <character name="habitat" value="spruce-aspen forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2700–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="2700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>45.</number>
  <other_name type="common_name">Burnet ragwort</other_name>
  <discussion>Packera sanguisorboides is known from the San Juan and Sangre de Cristo mountains of northern New Mexico, the Magdalena Mountains of western New Mexico, and the Sacramento Mountains of southern Lincoln and Otero counties. It may have affinities with P. coahuilensis Greenman.</discussion>
  
</bio:treatment>