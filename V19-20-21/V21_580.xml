<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
    <other_info_on_meta type="illustration_page">238</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg in N. L. Britton et al." date="1915" rank="genus">dysodiopsis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Rydberg in N. L. Britton et al." date="1915" rank="species">tagetoides</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 171. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus dysodiopsis;species tagetoides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220004523</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">tagetoides</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 361. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dyssodia;species tagetoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 40–90 × 2–6 mm.</text>
      <biological_entity id="o12924" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s0" to="90" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 30–50 mm.</text>
      <biological_entity id="o12925" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s1" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ray laminae linear-elliptic, 10–15 × 3–6 mm.</text>
      <biological_entity constraint="ray" id="o12926" name="lamina" name_original="laminae" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="linear-elliptic" value_original="linear-elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Disc corollas ca. 4 mm.</text>
      <biological_entity constraint="disc" id="o12927" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 3–3.5 mm;</text>
      <biological_entity id="o12928" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi: scales 1–2.5 mm. 2n = 26.</text>
      <biological_entity id="o12929" name="pappus" name_original="pappi" src="d0_s5" type="structure" />
      <biological_entity id="o12930" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12931" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone outcrops and derived soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone outcrops" />
        <character name="habitat" value="derived soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>