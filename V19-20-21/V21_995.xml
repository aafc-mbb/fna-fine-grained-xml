<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Dieter H. Wilken</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="treatment_page">396</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="1858" rank="genus">HULSEA</taxon_name>
    <place_of_publication>
      <publication_title>in War Department[U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>6(3): 77, plate 13. 1858 ·</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus HULSEA</taxon_hierarchy>
    <other_info_on_name type="etymology">Alpinegold [For Gilbert White Hulse, 1807–1883, physician and plant collector</other_info_on_name>
    <other_info_on_name type="fna_id">115855</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 5–150 cm.</text>
      <biological_entity id="o21571" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branched (aerial shoots often from subterranean caudices).</text>
      <biological_entity id="o21573" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal or basal and cauline, proximally whorled, distally alternate;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate (at least basal) or sessile;</text>
      <biological_entity id="o21574" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="proximally" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades mostly lance-linear, oblanceolate, ovate, or spatulate, margins entire, lobed, or toothed, faces thinly lanate to densely woolly (hairs crisped, tangled, or matted, usually 0.8+ mm) and/or glanddotted, glandular-puberulent, glandular-villous, or stipitate-glandular.</text>
      <biological_entity id="o21575" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o21576" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o21577" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="thinly lanate" name="pubescence" src="d0_s4" to="densely woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular-villous" value_original="glandular-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiate, borne singly or in corymbiform arrays.</text>
      <biological_entity id="o21578" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in corymbiform arrays" value_original="in corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o21579" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o21578" id="r1486" name="in" negation="false" src="d0_s5" to="o21579" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres obconic to hemispheric, 8–26 mm diam.</text>
      <biological_entity id="o21580" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s6" to="hemispheric" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s6" to="26" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries persistent, 14–35 (–60+) in 2–4 series (reflexed in fruit, mostly lanceovate to lanceolate or linear, herbaceous).</text>
      <biological_entity id="o21581" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="35" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="60" upper_restricted="false" />
        <character char_type="range_value" constraint="in series" constraintid="o21582" from="14" name="quantity" src="d0_s7" to="35" />
      </biological_entity>
      <biological_entity id="o21582" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles flat, knobby or pitted, epaleate.</text>
      <biological_entity id="o21583" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s8" value="knobby" value_original="knobby" />
        <character is_modifier="false" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 9–60+, pistillate, fertile;</text>
      <biological_entity id="o21584" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s9" to="60" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, orange, or red.</text>
      <biological_entity id="o21585" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 20–200+, bisexual, fertile;</text>
      <biological_entity id="o21586" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s11" to="200" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow to orange (glabrous or sparsely hairy), tubes shorter than or about equaling cylindric throats, lobes 5, deltate to lanceovate.</text>
      <biological_entity id="o21587" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="orange" />
      </biological_entity>
      <biological_entity id="o21588" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character constraint="than or about equaling cylindric throats" constraintid="o21589" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21589" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o21590" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s12" to="lanceovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae clavate to linear, compressed (lenticular in cross-section), silky-hairy;</text>
      <biological_entity id="o21591" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="clavate" name="shape" src="d0_s13" to="linear" />
        <character is_modifier="false" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="silky-hairy" value_original="silky-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 4 (distinct) quadrate to spatulate, equal, subequal, or unequal, erose to laciniate scales (the alternate alike).</text>
      <biological_entity id="o21593" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>x = 19.</text>
      <biological_entity id="o21592" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character modifier="of" name="quantity" src="d0_s14" value="4" value_original="4" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s14" to="spatulate" />
        <character is_modifier="false" name="variability" src="d0_s14" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="size" src="d0_s14" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="size" src="d0_s14" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s14" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity constraint="x" id="o21594" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>377.</number>
  <discussion>Species 7 (7 in the flora).</discussion>
  <references>
    <reference>Wilken, D. H. 1975. A systematic study of Hulsea (Asteraceae). Brittonia 27: 228–244.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and proximal cauline leaves gray to grayish green, lanate to woolly (hairs mostly eglandular)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and proximal cauline leaves ± green, mostly glandular-puberulent or glandular villous (sometimes sparsely lanate as well, sometimes woolly in H. nana)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves basal and cauline, blades broadly oblanceolate to spatulate, 6–10 cm; heads 2–5; cypselae 4–6 mm</description>
      <determination>3 Hulsea californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves mostly basal, blades spatulate, 1–9 cm; heads 1–2; cypselae 5–10 mm</description>
      <determination>7 Hulsea vestita</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves mostly basal (cauline none or relatively few); heads 1–2 (per basal leaf rosette)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves mostly cauline (basal usually present as well); heads 3–5</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants (10–)20–40 cm; leaf margins sinuate, lobed, or toothed (lobes or teeth mostly triangular); phyllary apices acute to attenuate; ray florets 28–59</description>
      <determination>1 Hulsea algida</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants 5–15(–20) cm; leaf margins lobed (lobes mostly oblong); phyllary apices acuminate to acute; ray florets 12–30</description>
      <determination>6 Hulsea nana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ray laminae red to reddish purple (narrowly oblong to linear, ciliate)</description>
      <determination>4 Hulsea heterochroma</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ray laminae yellow (narrowly elliptic to narrowly lance-oblong)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Ray florets 10–23, corolla tubes hairy</description>
      <determination>2 Hulsea brevifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Ray florets 20–35, corolla tubes glabrous</description>
      <determination>5 Hulsea mexicana</determination>
    </key_statement>
  </key>
</bio:treatment>