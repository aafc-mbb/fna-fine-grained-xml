<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="mention_page">485</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="treatment_page">552</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Webb ex Schultz-Bipontinus in P. B. Webb and S. Berthelot" date="1844" rank="genus">ARGYRANTHEMUM</taxon_name>
    <place_of_publication>
      <publication_title>in P. B. Webb and S. Berthelot, Hist. Nat. Îles Canaries</publication_title>
      <place_in_publication>3(2,75): 245, 258. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus ARGYRANTHEMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek argyros, silver, and anthemon, flower; allusion unclear</other_info_on_name>
    <other_info_on_name type="fna_id">102552</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 10–80+ [–150] cm.</text>
      <biological_entity id="o4768" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80+" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="80+" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, procumbent to erect, usually branched, glabrous [hairy].</text>
      <biological_entity id="o4770" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o4772" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o4771" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades ± obovate [oblong to lanceolate or linear] (bases sometimes ± clasping), [0] (1–) 2–3-pinnately lobed (lobes cuneate to linear), ultimate margins dentate [entire], faces glabrous [hairy].</text>
      <biological_entity id="o4773" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="[0](1-)2-3-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o4774" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o4775" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate [discoid], borne singly or in open, corymbiform arrays.</text>
      <biological_entity id="o4776" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in open , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o4777" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o4776" id="r474" name="in" negation="false" src="d0_s6" to="o4777" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric or broader, [6–] 10–18 [–22+] mm diam.</text>
      <biological_entity id="o4778" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="22" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 28–45+ in 3–4 series, distinct, oblanceolate or ovate to lance-deltate or lanceolate (not keeled abaxially), unequal, margins and apices (stramineous to brown) scarious (tips of inner often ± dilated).</text>
      <biological_entity id="o4779" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o4780" from="28" name="quantity" src="d0_s8" to="45" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lance-deltate or lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o4780" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o4781" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o4782" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex to conic, epaleate.</text>
      <biological_entity id="o4783" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s9" to="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 12–35+, pistillate, fertile;</text>
      <biological_entity id="o4784" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s10" to="35" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas usually white, sometimes yellow or pink, laminae ± ovate to linear.</text>
      <biological_entity id="o4785" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o4786" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="less ovate" name="shape" src="d0_s11" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets [50–] 80–150+, bisexual, fertile;</text>
      <biological_entity id="o4787" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s12" to="80" to_inclusive="false" />
        <character char_type="range_value" from="80" name="quantity" src="d0_s12" to="150" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow [red, purple], tubes ± cylindric (not basally dilated, ± gland-dotted), throats campanulate, lobes 5, deltate (without resin sacs).</text>
      <biological_entity id="o4788" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o4789" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o4790" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o4791" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae dimorphic: outer (ray) 3-angled, each angle usually ± winged (wings not spine-tipped);</text>
      <biological_entity id="o4792" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4793" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
      </biological_entity>
      <biological_entity id="o4794" name="angle" name_original="angle" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>inner (disc) compressed-prismatic (± quadrate, sometimes 2 angles winged, wings not spine-tipped);</text>
      <biological_entity id="o4795" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4796" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed-prismatic" value_original="compressed-prismatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>all ± ribbed or nerved, faces usually glabrous, sometimes gland-dotted between ribs (pericarps without myxogenic cells or resin sacs; embryo-sac development bisporic);</text>
      <biological_entity id="o4797" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s16" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s16" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="nerved" value_original="nerved" />
      </biological_entity>
      <biological_entity id="o4798" name="face" name_original="faces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character constraint="between ribs" constraintid="o4799" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s16" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity id="o4799" name="rib" name_original="ribs" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>pappi 0 (cypselar wall tissue sometimes produced as teeth, crowns, or oblique tubes similar in texture to cypselar wings).</text>
      <biological_entity id="o4800" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s17" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 9.</text>
      <biological_entity id="o4801" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o4802" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Atlantic Ocean Islands (Macaronesia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Atlantic Ocean Islands (Macaronesia)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>131.</number>
  <other_name type="common_name">Marguerite</other_name>
  <discussion>Species 24 (1 in the flora).</discussion>
  <references>
    <reference>Humphries, C. J. 1976. A revision of the Macaronesian genus Argyranthemum Webb ex Schultz Bip. (Compositae–Anthemideae). Bull. Brit. Mus. (Nat. Hist.), Bot. 5: 147–240.</reference>
  </references>
  
</bio:treatment>