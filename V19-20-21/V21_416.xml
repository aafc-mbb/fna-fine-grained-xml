<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="genus">heliomeris</taxon_name>
    <taxon_name authority="(B. L. Robinson &amp; Greenman) Cockerell" date="1918" rank="species">longifolia</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>18: 183. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus heliomeris;species longifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066906</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnolomia</taxon_name>
    <taxon_name authority="B. L. Robinson &amp; Greenman" date="unknown" rank="species">longifolia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Boston Soc. Nat. Hist.</publication_title>
      <place_in_publication>29: 92. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gymnolomia;species longifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viguiera</taxon_name>
    <taxon_name authority="(B. L. Robinson &amp; Greenman) S. F. Blake" date="unknown" rank="species">longifolia</taxon_name>
    <taxon_hierarchy>genus Viguiera;species longifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 7–15 cm (taprooted).</text>
      <biological_entity id="o8731" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems strigose.</text>
      <biological_entity id="o8732" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually opposite proximally, sometimes alternate (distal);</text>
      <biological_entity id="o8733" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually; proximally" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades lance-linear to linear, 10–160 × 1.5–8 (–12) mm, margins ciliate to 1/4 their lengths, hairs mostly less than 0.5 mm, faces strigose to strigillose (hair-bases narrowly tuberculate), abaxial often glanddotted.</text>
      <biological_entity id="o8734" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape_or_course" src="d0_s3" value="lance-linear to linear" value_original="lance-linear to linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="160" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8735" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s3" to="1/4" />
      </biological_entity>
      <biological_entity id="o8736" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8737" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s3" to="strigillose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8738" name="face" name_original="faces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads 6–25+.</text>
      <biological_entity id="o8739" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="25" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0.2–0.5 (–2) cm, lengths 1/20–1/2 times leafy portions of stems.</text>
      <biological_entity id="o8740" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="0.5" to_unit="cm" />
        <character constraint="of stems" constraintid="o8742" is_modifier="false" name="length" src="d0_s5" value="1/20-1/2 times leafy portions" value_original="1/20-1/2 times leafy portions" />
      </biological_entity>
      <biological_entity id="o8741" name="portion" name_original="portions" src="d0_s5" type="structure" />
      <biological_entity id="o8742" name="stem" name_original="stems" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 6–14 mm diam.</text>
      <biological_entity id="o8743" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 3–6 mm.</text>
      <biological_entity id="o8744" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Paleae oblong, 4–4.5 mm, acute to cuspidate.</text>
      <biological_entity id="o8745" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 12–14;</text>
      <biological_entity id="o8746" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae ± elliptic, 5–17 mm (glabrous or puberulent).</text>
      <biological_entity id="o8747" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 50+;</text>
      <biological_entity id="o8748" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s11" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 2.5–4 mm.</text>
      <biological_entity id="o8749" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae black or mottled, 1.8–2 mm.</text>
      <biological_entity id="o8750" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="mottled" value_original="mottled" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Longleaf false goldeneye</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems branching mostly in distal 1/2, bases usually 7+ mm diam.; leaves (40–)80–160 × 4–8(–12) mm; involucres 9–14 mm diam</description>
      <determination>3a Heliomeris longifolia var. longifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems branching ± throughout, bases usually less than 5 mm diam.; leaves 10–70(–85) × 1.5–5 mm; involucres 6–9 mm diam. 3b. Heliomeris longifolia var. annua</description>
      <next_statement_id>1</next_statement_id>
    </key_statement>
  </key>
</bio:treatment>