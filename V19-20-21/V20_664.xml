<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="species">garrettii</taxon_name>
    <place_of_publication>
      <publication_title>in J. M. Coulter and A. Nelson, New Man. Bot. Rocky Mt.,</publication_title>
      <place_in_publication>526. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species garrettii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066600</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">controversus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species controversus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (3–) 5–23 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches lignescent, apparently elongate.</text>
      <biological_entity id="o19164" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="23" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o19165" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="lignescent" value_original="lignescent" />
        <character is_modifier="false" modifier="apparently" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sparsely strigillose to glabrate, eglandular.</text>
      <biological_entity id="o19166" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="sparsely strigillose" name="pubescence" src="d0_s2" to="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal (persistent);</text>
      <biological_entity id="o19167" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19168" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate to spatulate (not folding), 20–70 (–120) × 3–13 mm, cauline abruptly reduced distally (bases not clasping), margins entire, sparsely ciliate (apices rounded to acute), faces glabrate or glabrous, eglandular.</text>
      <biological_entity id="o19169" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o19170" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o19171" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19172" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o19173" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–8 × 8–17 mm.</text>
      <biological_entity id="o19174" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series (purplish at least at tips), glabrous or sparsely strigillose, densely minutely glandular.</text>
      <biological_entity id="o19175" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o19176" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o19175" id="r1777" name="in" negation="false" src="d0_s7" to="o19176" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 20–35;</text>
      <biological_entity id="o19177" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, drying white or sometimes lilac-tinged, 7–13 mm, laminae coiling.</text>
      <biological_entity id="o19178" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="condition" src="d0_s9" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="lilac-tinged" value_original="lilac-tinged" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19179" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.9–5.3 mm.</text>
      <biological_entity constraint="disc" id="o19180" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s10" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2.5–3 mm, 2 (–3) -nerved, faces sparsely strigose;</text>
      <biological_entity id="o19181" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2(-3)-nerved" value_original="2(-3)-nerved" />
      </biological_entity>
      <biological_entity id="o19182" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae or scales, inner of 13–23 (white-shiny) bristles.</text>
      <biological_entity id="o19184" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o19185" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o19186" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="13" is_modifier="true" name="quantity" src="d0_s12" to="23" />
      </biological_entity>
      <relation from="o19183" id="r1778" name="outer of" negation="false" src="d0_s12" to="o19184" />
      <relation from="o19183" id="r1779" name="outer of" negation="false" src="d0_s12" to="o19185" />
      <relation from="o19183" id="r1780" name="inner of" negation="false" src="d0_s12" to="o19186" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o19183" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o19187" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist cliff faces and crevices, mostly limestone, soil among boulders</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist cliff" />
        <character name="habitat" value="crevices" modifier="faces and" />
        <character name="habitat" value="boulders" modifier="limestone soil among" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2700–3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="2700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>79.</number>
  <other_name type="common_name">Garrett’s fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>