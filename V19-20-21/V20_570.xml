<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">280</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1880" rank="species">eatonii</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="1992" rank="variety">sonnei</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>73: 190. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species eatonii;variety sonnei</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068334</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">sonnei</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 218. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species sonnei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">nevadensis</taxon_name>
    <taxon_name authority="(Greene) Smiley" date="unknown" rank="variety">sonnei</taxon_name>
    <taxon_hierarchy>genus Erigeron;species nevadensis;variety sonnei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 4–12 (–21) cm.</text>
      <biological_entity id="o13260" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="21" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Heads 1 (–2), held just beyond to well beyond basal leaves.</text>
      <biological_entity id="o13261" name="head" name_original="heads" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="2" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13262" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <relation from="o13261" id="r1202" modifier="beyond; well" name="beyond" negation="false" src="d0_s1" to="o13262" />
    </statement>
    <statement id="d0_s2">
      <text>Involucres 4.5–8 × 8–12 (–16) mm.</text>
      <biological_entity id="o13263" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="16" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries hirsutulous to villous (hairs 0.5–1.5 mm), eglandular.</text>
      <biological_entity id="o13264" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="hirsutulous" name="pubescence" src="d0_s3" to="villous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray laminae 4.5–6.6 (–8.5) mm.</text>
      <biological_entity constraint="ray" id="o13265" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character char_type="range_value" from="6.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s4" to="6.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas 3.5–5 mm.</text>
      <biological_entity constraint="disc" id="o13266" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 2.8–3.5 mm;</text>
      <biological_entity id="o13267" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappi of 18–30 bristles 3.5–5 mm. 2n = 18.</text>
      <biological_entity id="o13268" name="pappus" name_original="pappi" src="d0_s7" type="structure" />
      <biological_entity id="o13269" name="bristle" name_original="bristles" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s7" to="30" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13270" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
      <relation from="o13268" id="r1203" name="consist_of" negation="false" src="d0_s7" to="o13269" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, seeps, rocky flats, often with sagebrush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="rocky flats" />
        <character name="habitat" value="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10b.</number>
  
</bio:treatment>