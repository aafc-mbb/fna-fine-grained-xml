<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">461</other_info_on_meta>
    <other_info_on_meta type="treatment_page">511</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Mociño ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">CARMINATIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 267. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus CARMINATIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Bassiani Carminati, eighteenth-century Italian author of book on hygiene, therapeutics, and materia medica</other_info_on_name>
    <other_info_on_name type="fna_id">105675</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–100+ cm.</text>
      <biological_entity id="o20378" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (round to 4-angled or ribbed), unbranched or sparingly branched (from proximal nodes, puberulent and/or villous to pilose, often in lines, sometimes glabrate).</text>
      <biological_entity id="o20379" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o20381" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>usually opposite (proximal often withering before flowering, distal sometimes alternate);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o20380" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (proximal) 3–5-nerved, triangular to broadly ovate (bases obtuse to cordate), margins subentire to dentate, faces glabrous or sparsely villous (at least along veins and on margins), not glanddotted.</text>
      <biological_entity id="o20382" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-5-nerved" value_original="3-5-nerved" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="broadly ovate" />
      </biological_entity>
      <biological_entity id="o20383" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s5" to="dentate" />
      </biological_entity>
      <biological_entity id="o20384" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, in spiciform or narrow, ± paniculiform arrays (subtended by lanceolate to linear, scalelike bracts).</text>
      <biological_entity id="o20385" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric, [2–] 3–4+ mm diam.</text>
      <biological_entity id="o20386" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent (spreading in age), (12–) 17–22+ in 3–4+ series, 3-nerved, lance-deltate or lanceolate to linear, unequal (apices acute to acuminate).</text>
      <biological_entity id="o20387" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s8" to="17" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o20388" from="17" name="quantity" src="d0_s8" to="22" upper_restricted="false" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o20388" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, epaleate.</text>
      <biological_entity id="o20389" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 8–12;</text>
      <biological_entity id="o20390" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas greenish white to cream-colored, (bases slightly enlarged, tubes and throats not markedly differentiated externally) throats cylindric to filiform, lobes 5, triangular-ovate (very short);</text>
      <biological_entity id="o20391" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s11" to="cream-colored" />
      </biological_entity>
      <biological_entity id="o20392" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s11" to="filiform" />
      </biological_entity>
      <biological_entity id="o20393" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s11" value="triangular-ovate" value_original="triangular-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles: bases not enlarged, glabrous, branches ± filiform (appendages linear or weakly clavate).</text>
      <biological_entity id="o20394" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o20395" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20396" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae ± prismatic (usually 5-angled) or subcylindric, usually 5-ribbed, minutely puberulent;</text>
      <biological_entity id="o20397" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subcylindric" value_original="subcylindric" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s13" value="5-ribbed" value_original="5-ribbed" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi usually readily falling, of 8–13 plumose bristles or setiform scales in 1 series (basally coherent or weakly connate, falling together or in groups).</text>
      <biological_entity id="o20399" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s14" to="13" />
        <character is_modifier="true" name="shape" src="d0_s14" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity id="o20400" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="setiform" value_original="setiform" />
      </biological_entity>
      <biological_entity id="o20401" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <relation from="o20398" id="r1402" name="consist_of" negation="false" src="d0_s14" to="o20399" />
      <relation from="o20398" id="r1403" name="consist_of" negation="false" src="d0_s14" to="o20400" />
      <relation from="o20400" id="r1404" name="in" negation="false" src="d0_s14" to="o20401" />
    </statement>
    <statement id="d0_s15">
      <text>x = 10.</text>
      <biological_entity id="o20398" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually readily" name="life_cycle" src="d0_s14" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o20402" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>409.</number>
  <discussion>Species 3 (1 in the flora).</discussion>
  <references>
    <reference>Turner, B. L. 1988b. Taxonomy of Carminatia (Asteraceae, Eupatorieae). Pl. Syst. Evol. 160: 169–179.</reference>
  </references>
  
</bio:treatment>