<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Willdenow" date="1807" rank="genus">grindelia</taxon_name>
    <taxon_name authority="Steyermark" date="1934" rank="species">howellii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 549, fig. 30. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus grindelia;species howellii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066813</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (25–) 60–90+ cm.</text>
      <biological_entity id="o9810" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, stramineous to redbrown, stipitate-glandular (at least distally).</text>
      <biological_entity id="o9811" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s1" to="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaf-blades spatulate or oblong to oblanceolate or lanceolate, 25–60 (–90) mm, lengths 3–5 (–7) times widths, bases cuneate to ± clasping, margins entire or serrate to denticulate (teeth apiculate), apices obtuse to acute, faces usually finely stipitate-glandular, sometimes glabrous (or scabridulous near margins) and gland-dotted.</text>
      <biological_entity constraint="cauline" id="o9812" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="oblanceolate or lanceolate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s2" to="90" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="distance" src="d0_s2" to="60" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="3-5(-7)" value_original="3-5(-7)" />
      </biological_entity>
      <biological_entity id="o9813" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o9814" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s2" value="serrate to denticulate" value_original="serrate to denticulate" />
      </biological_entity>
      <biological_entity id="o9815" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o9816" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually finely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in open to crowded, corymbiform to paniculiform arrays.</text>
      <biological_entity id="o9817" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o9818" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s3" to="paniculiform" />
      </biological_entity>
      <relation from="o9817" id="r904" name="open to" negation="false" src="d0_s3" to="o9818" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres broadly urceolate to globose, 8–15 × 12–20 (–30) mm.</text>
      <biological_entity id="o9819" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly urceolate" name="shape" src="d0_s4" to="globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries in 6–9 series, spreading to appressed, linear or linear-attenuate to lance-linear, apices mostly looped to hooked (inner sometimes recurved to straight), ± terete, moderately to strongly resinous.</text>
      <biological_entity id="o9820" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s5" to="appressed" />
        <character char_type="range_value" from="linear-attenuate" name="shape" src="d0_s5" to="lance-linear" />
      </biological_entity>
      <biological_entity id="o9821" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o9822" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="mostly looped" name="shape" src="d0_s5" to="hooked" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="moderately to strongly" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o9820" id="r905" name="in" negation="false" src="d0_s5" to="o9821" />
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 20–30+;</text>
      <biological_entity id="o9823" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 8–12 mm.</text>
      <biological_entity id="o9824" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae whitish to stramineous, 4–5.5 mm, apices ± coronate, faces striate or furrowed;</text>
      <biological_entity id="o9825" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s8" to="stramineous" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9826" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="coronate" value_original="coronate" />
      </biological_entity>
      <biological_entity id="o9827" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s8" value="striate" value_original="striate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of 2 straight to curled, usually smooth, sometimes barbellulate, subulate scales 2.5–4+ mm, shorter than disc corollas.</text>
      <biological_entity id="o9828" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character constraint="than disc corollas" constraintid="o9830" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9829" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="true" name="shape" src="d0_s9" value="curled" value_original="curled" />
        <character is_modifier="true" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s9" value="barbellulate" value_original="barbellulate" />
        <character is_modifier="true" name="shape" src="d0_s9" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="disc" id="o9830" name="corolla" name_original="corollas" src="d0_s9" type="structure" />
      <relation from="o9828" id="r906" name="consist_of" negation="false" src="d0_s9" to="o9829" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, grasslands, forest openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="forest openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>But for the usually stipitate-glandular indument of the stems and leaves, plants called Grindelia howellii are very much like plants that have been called G. nana forma brownii Steyermark, G. nana forma longisquama Steyermark, and G. paysonorum H. St. John [= G. nana var. paysonorum (H. St. John) Steyermark], all typified by specimens included here in G. hirsutula. In some plants of G. howellii, the glands on the leaves range from stipitate to ± embedded (e.g., Pierce 1146, ID). Taxonomic rank for plants that have been called G. howellii should be reconsidered.</discussion>
  
</bio:treatment>