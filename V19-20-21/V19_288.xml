<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="Greene" date="1895" rank="species">modocensis</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 48. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species modocensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066446</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–35 cm (taproots slender, caudices branched).</text>
      <biological_entity id="o2915" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–4, erect, slender to stout, simple or sparsely branched, glabrate to tomentose and bristly-setose.</text>
      <biological_entity id="o2916" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="slender" name="size" src="d0_s1" to="stout" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s1" to="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="bristly-setose" value_original="bristly-setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o2917" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades lanceolate, 7–25 × 2–4 cm, margins deeply pinnately lobed (lobes lanceolate, dentate, teeth mucronate), apices acuminate, faces tomentulose (at least when young).</text>
      <biological_entity constraint="basal" id="o2918" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2919" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o2920" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o2921" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–9, borne singly or 2–9 in cymiform arrays.</text>
      <biological_entity id="o2922" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="9" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o2923" from="2" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o2923" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 8–10, lanceolate, tomentose and often setose bractlets 2–4 mm.</text>
      <biological_entity id="o2924" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o2925" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="true" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="true" modifier="often" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o2924" id="r287" name="consist_of" negation="false" src="d0_s6" to="o2925" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric, 11–21 × 5–10 mm.</text>
      <biological_entity id="o2926" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s7" to="21" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 8–18, (medially green) lanceolate, 10–16 mm, (bases keeled, margins yellowish, often scarious), apices acute, abaxial faces often densely, blackish or whitish tomentose or setose, sometimes glabrous, adaxial with fine (shiny) hairs.</text>
      <biological_entity id="o2927" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="18" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2928" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2929" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="setose" value_original="setose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2930" name="face" name_original="faces" src="d0_s8" type="structure" />
      <biological_entity id="o2931" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="fine" value_original="fine" />
      </biological_entity>
      <relation from="o2930" id="r288" name="with" negation="false" src="d0_s8" to="o2931" />
    </statement>
    <statement id="d0_s9">
      <text>Florets 10–60;</text>
      <biological_entity id="o2932" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 13–22 mm.</text>
      <biological_entity id="o2933" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s10" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae blackish or greenish, reddish, reddish-brown, or yellowish, subcylindric to fusiform, 7–12 mm, apices tapered or beaked (beaks 1–3 mm), ribs 10 (strong to weak);</text>
      <biological_entity id="o2934" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="subcylindric" name="shape" src="d0_s11" to="fusiform" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2935" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o2936" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi dusky white, 5–13 mm. 2n = 22, 33, 44, 55, 66, 88.</text>
      <biological_entity id="o2937" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dusky white" value_original="dusky white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2938" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="22" value_original="22" />
        <character name="quantity" src="d0_s12" value="33" value_original="33" />
        <character name="quantity" src="d0_s12" value="44" value_original="44" />
        <character name="quantity" src="d0_s12" value="55" value_original="55" />
        <character name="quantity" src="d0_s12" value="66" value_original="66" />
        <character name="quantity" src="d0_s12" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Modoc or Siskiyou hawksbeard</other_name>
  <discussion>Subspecies 4 (4 in the flora).</discussion>
  <discussion>Crepis modocensis is recognized by its tomentose or coarsely bristly stems and petioles, rosettes of deeply pinnately lobed leaves, rather large heads with relatively many phyllaries, and blackish cypselae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Setae of stems and petioles yellowish, ± straight, (those of phyllaries blackish or 0); cypselae tapered, not distinctly beaked</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Setae of stems and petioles (and phyllaries) whitish, conspicuously curled; cypselae beaked (beaks 1–3 mm)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems branching near middle; involucres 11–16 mm; pappi 5–10 mm</description>
      <determination>11a Crepis modocensis subsp. modocensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems low, branching proximally; involucres 13–21 mm; pappi 9–13 mm</description>
      <determination>11d Crepis modocensis subsp. subacaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants 15–30 cm; involucres 12–17 mm; pappi 7–10 mm</description>
      <determination>11c Crepis modocensis subsp. rostrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants 6–20 cm; involucres 11–13 mm; pappi 5– 7 mm</description>
      <determination>11b Crepis modocensis subsp. glareosa</determination>
    </key_statement>
  </key>
</bio:treatment>