<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="treatment_page">325</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1882" rank="species">muirii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 210. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species muirii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066636</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">grandiflorus</taxon_name>
    <taxon_name authority="(A. Gray) Hultén" date="unknown" rank="subspecies">muirii</taxon_name>
    <taxon_hierarchy>genus Erigeron;species grandiflorus;subspecies muirii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–10 (–12) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, rhizomes or caudices simple, relatively short and thick.</text>
      <biological_entity id="o19667" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o19668" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o19669" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, densely cobwebby-lanate (hairs white), eglandular.</text>
      <biological_entity id="o19670" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="cobwebby-lanate" value_original="cobwebby-lanate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o19671" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate to subspatulate, (10–) 20–60 × 2–10 mm, margins entire, faces lanate, eglandular;</text>
      <biological_entity id="o19672" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="subspatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s4" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19673" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19674" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="lanate" value_original="lanate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades oblong to linear-oblong, gradually reduced, bractlike.</text>
      <biological_entity constraint="cauline" id="o19675" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="linear-oblong" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s5" value="bractlike" value_original="bractlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1.</text>
      <biological_entity id="o19676" name="head" name_original="heads" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 8–10 × 12–20 mm.</text>
      <biological_entity id="o19677" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2 (–3) series (purple beneath pubescence), densely and closely lanate, sparsely glandular (glandularity obscured).</text>
      <biological_entity id="o19678" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely; closely" name="pubescence" notes="" src="d0_s8" value="lanate" value_original="lanate" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o19679" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o19678" id="r1825" name="in" negation="false" src="d0_s8" to="o19679" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 60–100;</text>
      <biological_entity id="o19680" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s9" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white, often drying pinkish, 8–13 mm (mostly 1–2 mm wide), laminae coiling.</text>
      <biological_entity id="o19681" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="condition" src="d0_s10" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19682" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 3.5–5 mm.</text>
      <biological_entity constraint="disc" id="o19683" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 1.9–2.2 mm, 2-nerved, faces strigose-hirsute;</text>
      <biological_entity id="o19684" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s12" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o19685" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="strigose-hirsute" value_original="strigose-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae, inner of 13–20 bristles.</text>
      <biological_entity id="o19687" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o19688" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="13" is_modifier="true" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
      <relation from="o19686" id="r1826" name="outer of" negation="false" src="d0_s13" to="o19687" />
      <relation from="o19686" id="r1827" name="inner of" negation="false" src="d0_s13" to="o19688" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o19686" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o19689" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and ridges, tundra, gravel barrens, sandstone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="gravel barrens" />
        <character name="habitat" value="sandstone outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>125.</number>
  <other_name type="common_name">Muir’s fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erigeron muirii is known from Herschel Island in northern Yukon. Its densely lanate vestiture gives the whole plant a gray-green aspect.</discussion>
  
</bio:treatment>