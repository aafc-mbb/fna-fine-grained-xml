<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
    <other_info_on_meta type="treatment_page">332</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Porter in T. C. Porter and J. M. Coulter" date="1874" rank="species">coulteri</taxon_name>
    <place_of_publication>
      <publication_title>in T. C. Porter and J. M. Coulter, Syn. Fl. Colorado,</publication_title>
      <place_in_publication>61. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species coulteri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066579</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–70 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, sometimes with branched caudices and scale-leaved stolons.</text>
      <biological_entity id="o15294" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o15295" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o15296" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="scale-leaved" value_original="scale-leaved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sparsely hispido-villous (hair cross-walls sometimes black), often glabrate, eglandular.</text>
      <biological_entity id="o15297" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hispido-villous" value_original="hispido-villous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent or not) and cauline;</text>
      <biological_entity id="o15298" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal blades broadly oblanceolate to elliptic or oblong-lanceolate, 40–120 (–150) × 7–25 mm, margins entire or with 1–5 pairs of shallow teeth, faces sparsely strigose to strigoso-villous, eglandular;</text>
      <biological_entity constraint="proximal" id="o15299" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly oblanceolate" name="shape" src="d0_s4" to="elliptic or oblong-lanceolate" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15300" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with 1-5 pairs" value_original="with 1-5 pairs" />
      </biological_entity>
      <biological_entity id="o15301" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o15302" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="depth" src="d0_s4" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o15303" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s4" to="strigoso-villous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o15300" id="r1395" name="with" negation="false" src="d0_s4" to="o15301" />
      <relation from="o15301" id="r1396" name="part_of" negation="false" src="d0_s4" to="o15302" />
    </statement>
    <statement id="d0_s5">
      <text>cauline blades becoming elliptic-ovate to lanceolate, gradually reduced distally (bases usually clasping).</text>
      <biological_entity constraint="cauline" id="o15304" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1 (–4).</text>
      <biological_entity id="o15305" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="4" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 7–10 × 10–16 mm.</text>
      <biological_entity id="o15306" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2 (–3) series, hirsuto-villous (hair cross-walls black), minutely glandular.</text>
      <biological_entity id="o15307" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="hirsuto-villous" value_original="hirsuto-villous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o15308" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o15307" id="r1397" name="in" negation="false" src="d0_s8" to="o15308" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 45–140;</text>
      <biological_entity id="o15309" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="45" name="quantity" src="d0_s9" to="140" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 9–25 mm, laminae coiling, white.</text>
      <biological_entity id="o15310" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15311" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 3–4.4 mm.</text>
      <biological_entity constraint="disc" id="o15312" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae (1.3–) 1.5–1.8 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o15313" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o15314" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae, inner of 20–25 bristles.</text>
      <biological_entity id="o15316" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o15317" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
      <relation from="o15315" id="r1398" name="outer of" negation="false" src="d0_s13" to="o15316" />
      <relation from="o15315" id="r1399" name="inner of" negation="false" src="d0_s13" to="o15317" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o15315" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o15318" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist coniferous woods, moist to wet meadows, open areas along creeks, aspen, spruce-fir</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist coniferous woods" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="open areas" constraint="along creeks , aspen ," />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="aspen" />
        <character name="habitat" value="moist to wet meadows" />
        <character name="habitat" value="open areas along creeks" />
        <character name="habitat" value="spruce-fir" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>141.</number>
  <other_name type="common_name">Coulter’s fleabane</other_name>
  
</bio:treatment>