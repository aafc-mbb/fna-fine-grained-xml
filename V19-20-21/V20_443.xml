<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="treatment_page">201</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="genus">townsendia</taxon_name>
    <taxon_name authority="D. C. Eaton in S. Watson" date="1871" rank="species">scapigera</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>145, plate 17, figs. 1–7. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus townsendia;species scapigera</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067778</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>(Biennials) perennials, 3–5 (–12+) cm (usually ± pulvinate).</text>
      <biological_entity id="o14015" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect;</text>
      <biological_entity id="o14016" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 0.1–1 mm, ± strigose.</text>
      <biological_entity id="o14017" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, blades ± spatulate to oblanceolate, 15–30 (–70) × 2–5 (–9) mm, not fleshy, faces ± strigose.</text>
      <biological_entity id="o14018" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o14019" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="less spatulate" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="70" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o14020" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads on scapiform peduncles 30–60 (–120) mm.</text>
      <biological_entity id="o14021" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o14022" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="scapiform" value_original="scapiform" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s4" to="60" to_unit="mm" />
      </biological_entity>
      <relation from="o14021" id="r1288" name="on" negation="false" src="d0_s4" to="o14022" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate, 12–20 (–32) mm diam.</text>
      <biological_entity id="o14023" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s5" to="32" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 16–32+ in 3–4+ series, the longer ± lanceolate, (7–) 9–13 mm (l/w = 3–5), apices acute, abaxial faces piloso-strigose to strigose.</text>
      <biological_entity id="o14024" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o14025" from="16" name="quantity" src="d0_s6" to="32" upper_restricted="false" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14025" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14026" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14027" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="piloso-strigose" name="pubescence" src="d0_s6" to="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 18–35;</text>
      <biological_entity id="o14028" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s7" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white adaxially, laminae 7–16 mm, glandular-puberulent abaxially.</text>
      <biological_entity id="o14029" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o14030" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 40–100+;</text>
      <biological_entity id="o14031" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3.5–5.5 mm.</text>
      <biological_entity id="o14032" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 4–5.5 mm, faces hairy, hair tips forked or entire;</text>
      <biological_entity id="o14033" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14034" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="hair" id="o14035" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="forked" value_original="forked" />
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent;</text>
    </statement>
    <statement id="d0_s13">
      <text>on ray cypselae 20–30+ subulate to setiform scales 3–6+ mm;</text>
      <biological_entity id="o14036" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>on disc cypselae 20–30 subulate to setiform scales 5–7+ mm.</text>
      <biological_entity id="o14037" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s13" to="setiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="30" />
      </biological_entity>
      <biological_entity id="o14038" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s14" to="setiform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in sagebrush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in sagebrush" />
        <character name="habitat" value="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion>Plants that key here and have peduncles 5–15+ mm (sporting smaller heads, fewer ray florets, and smaller cypselae than are characteristic of Townsendia scapigera) may belong to T. jonesii (which see). Plants included here in T. scapigera from Sweetwater Mountains, California, with relatively large heads and high numbers of florets were identified on their labels (e.g., DeDecker 3928, RSA) as T. parryi, a species not known to occur in California.</discussion>
  
</bio:treatment>