<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">219</other_info_on_meta>
    <other_info_on_meta type="illustration_page">214</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Cavanilles" date="1795" rank="genus">heterosperma</taxon_name>
    <taxon_name authority="Cavanilles" date="1795" rank="species">pinnatum</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>3: 34 1795/1796 (as pinnata)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus heterosperma;species pinnatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242432365</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 1–4+ cm, lobes 0.5–1 (–3) mm wide.</text>
      <biological_entity id="o3128" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3129" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ray laminae 1–2+ mm.</text>
      <biological_entity constraint="ray" id="o3130" name="lamina" name_original="laminae" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Disc corollas ca. 2.5 mm.</text>
      <biological_entity constraint="disc" id="o3131" name="corolla" name_original="corollas" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cypselae 5–18 mm;</text>
      <biological_entity id="o3132" name="cypsela" name_original="cypselae" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pappi 0.5–1 (–3) mm.</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 48, 50.</text>
      <biological_entity id="o3133" name="pappus" name_original="pappi" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3134" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="48" value_original="48" />
        <character name="quantity" src="d0_s5" value="50" value_original="50" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Igneous and calcareous soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="igneous" />
        <character name="habitat" value="calcareous soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Heterosperma pinnatum occurs throughout much of the southwestern United States. No specimens have been seen to substantiate claims of occurrences in Maryland and Massassachusetts, where it may have been ephemeral. Calyculi of H. pinnatum have been interpreted as leaves, the heads being interpreted as terminal and sessile.</discussion>
  
</bio:treatment>