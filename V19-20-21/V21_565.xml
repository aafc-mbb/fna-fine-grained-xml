<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">CHRYSACTINIA</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 93. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus CHRYSACTINIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chrysos, gold, and actinos, ray</other_info_on_name>
    <other_info_on_name type="fna_id">106947</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 10–40 [–80] cm (evergreen).</text>
      <biological_entity id="o25843" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, strictly branched.</text>
      <biological_entity id="o25845" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="strictly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate [opposite];</text>
      <biological_entity id="o25846" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades simple [pinnate], linear to clavate or acerose (± fleshy), margins entire, faces usually glabrous, sometimes puberulent (oil-glands marginal or submarginal).</text>
      <biological_entity id="o25847" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="clavate or acerose" />
      </biological_entity>
      <biological_entity id="o25848" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25849" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o25850" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi 0.</text>
      <biological_entity id="o25851" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres turbinate to hemispheric, 3.5–5 [–8] mm diam.</text>
      <biological_entity id="o25852" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s7" to="hemispheric" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 8–14 in ± 2 series (distinct to bases, linear to lance-linear [ovate], carinate, each usually bearing 1–5 oil-glands).</text>
      <biological_entity id="o25853" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o25854" from="8" name="quantity" src="d0_s8" to="14" />
      </biological_entity>
      <biological_entity id="o25854" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex to hemispheric, ± pitted, rarely paleate (paleae readily falling).</text>
      <biological_entity id="o25855" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s9" to="hemispheric" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8 (–13), pistillate, fertile;</text>
      <biological_entity id="o25856" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="13" />
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas bright-yellow.</text>
      <biological_entity id="o25857" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 15–70, bisexual, fertile;</text>
      <biological_entity id="o25858" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s12" to="70" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow to orange, tubes much shorter than cylindro-funnelform throats, lobes 5, lance-deltate.</text>
      <biological_entity id="o25859" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="orange" />
      </biological_entity>
      <biological_entity id="o25860" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than cylindro-funnelform throats" constraintid="o25861" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o25861" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o25862" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lance-deltate" value_original="lance-deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae cylindric to fusiform, strigillose or glabrescent;</text>
      <biological_entity id="o25863" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s14" to="fusiform" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent, of [20–] 30–40 bristles in ± 1 series.</text>
      <biological_entity id="o25865" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="atypical_quantity" src="d0_s15" to="30" to_inclusive="false" />
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s15" to="40" />
      </biological_entity>
      <biological_entity id="o25866" name="series" name_original="series" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <relation from="o25864" id="r1757" name="consist_of" negation="false" src="d0_s15" to="o25865" />
      <relation from="o25865" id="r1758" name="in" negation="false" src="d0_s15" to="o25866" />
    </statement>
    <statement id="d0_s16">
      <text>x = 15.</text>
      <biological_entity id="o25864" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o25867" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>319.</number>
  <discussion>Species 5 (1 in the flora).</discussion>
  <references>
    <reference>Strother, J. L. 1977. Taxonomy of Chrysactinia, Harnackia, and Lescaillea (Compositae: Tageteae). Madroño 24: 129–139.</reference>
  </references>
  
</bio:treatment>