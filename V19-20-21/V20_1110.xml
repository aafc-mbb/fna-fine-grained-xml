<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">467</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="treatment_page">494</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rafinesque) G. L. Nesom" date="1995" rank="subgenus">virgulus</taxon_name>
    <taxon_name authority="(Linnaeus) G. L. Nesom" date="1995" rank="species">ericoides</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 280. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus virgulus;species ericoides</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067642</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">ericoides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 875. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species ericoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lasallea</taxon_name>
    <taxon_name authority="(Linnaeus) Semple &amp; Brouillet" date="unknown" rank="species">ericoides</taxon_name>
    <taxon_hierarchy>genus Lasallea;species ericoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Virgulus</taxon_name>
    <taxon_name authority="(Linnaeus) Reveal &amp; Keener" date="unknown" rank="species">ericoides</taxon_name>
    <taxon_hierarchy>genus Virgulus;species ericoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–80 (–100) cm, colonial or cespitose, eglandular;</text>
    </statement>
    <statement id="d0_s1">
      <text>branched rhizomatous, or with ± cormoid, branched, woody caudices.</text>
      <biological_entity id="o21277" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o21278" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s1" value="cormoid" value_original="cormoid" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–3+, ascending to erect (grayish brown to brown), sparsely to densely hispido-strigose, sometimes glabrescent proximally.</text>
      <biological_entity id="o21279" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sometimes; proximally" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually all except rameal withered by flowering, (light grayish green) firm apices ± white-spine-tipped (often with clusters of smaller leaves in axils);</text>
      <biological_entity id="o21280" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21281" name="rameal" name_original="rameal" src="d0_s3" type="structure">
        <character constraint="by apices" constraintid="o21282" is_modifier="false" name="life_cycle" src="d0_s3" value="withered" value_original="withered" />
      </biological_entity>
      <biological_entity id="o21282" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="texture" src="d0_s3" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s3" value="white-spine-tipped" value_original="white-spine-tipped" />
      </biological_entity>
      <relation from="o21280" id="r1963" name="except" negation="false" src="d0_s3" to="o21281" />
    </statement>
    <statement id="d0_s4">
      <text>basal sessile, blades (3-nerved) oblanceolate to oblong or spatulate, 10–50 × 10–25 mm, bases attenuate, margins usually entire, rarely remotely serrate, scabrous, apices rounded to obtuse, faces usually sparsely hairy, often glabrous;</text>
      <biological_entity constraint="basal" id="o21283" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o21284" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="oblong or spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21285" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o21286" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely remotely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21287" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o21288" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline sessile, blades (1-nerved or 3-nerved) linear to lanceolate or oblong, 10–40 (–60) × 1.5–4 (–7) mm, reduced distally, bases cuneate, coarsely ciliate, margins entire, coarsely ciliate, apices acute or obtuse, faces moderately to densely strigose or hirsute;</text>
      <biological_entity constraint="proximal cauline" id="o21289" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o21290" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate or oblong" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21291" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21292" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21293" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o21294" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades oblong-ovate, 10–40 × 1.5–3.5 mm, abruptly reduced distally, bases cuneate, margins entire, apices acute, faces moderately to densely strigose.</text>
      <biological_entity constraint="distal" id="o21295" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o21296" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="40" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21297" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o21298" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21299" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o21300" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (1–200+) in paniculiform arrays, branches fastigiate or arrays often pyramidal, racemiform, secund, crowded.</text>
      <biological_entity id="o21301" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o21302" from="1" name="atypical_quantity" src="d0_s7" to="200" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o21302" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o21303" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="fastigiate" value_original="fastigiate" />
        <character name="arrangement" src="d0_s7" value="arrays" value_original="arrays" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="pyramidal" value_original="pyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="secund" value_original="secund" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0.5–1 (–2) cm or subsessile, densely hairy, bracts dense, linear to narrowly lanceolate, usually reflexed, sometime appressed to ascending, 1.5–5 (–6) mm, densely hairy, grading into phyllaries.</text>
      <biological_entity id="o21304" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="1" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21305" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="appressed" modifier="sometime" name="orientation" src="d0_s8" to="ascending" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21306" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o21305" id="r1964" name="into" negation="false" src="d0_s8" to="o21306" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres cylindric to campanulate, 2.5–4.5 (–5) mm.</text>
      <biological_entity id="o21307" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s9" to="campanulate" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, oblanceolate to ± spatulate, unequal, firm, bases (whitish to tan) ± indurate in proximal 1/2–2/3, margins hyaline, scabrous proximally, green zones diamond-shaped, in distal 1/2, apices spine-tipped, (outer) spreading to reflexed or squarrose, faces (outer) sparsely to densely hispid, scabroso-hirsute adaxially, (inner) glabrous.</text>
      <biological_entity id="o21308" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" notes="" src="d0_s10" to="more or less spatulate" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="texture" src="d0_s10" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o21309" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o21310" name="base" name_original="bases" src="d0_s10" type="structure">
        <character constraint="in proximal 1/2-2/3" constraintid="o21311" is_modifier="false" modifier="more or less" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21311" name="1/2-2/3" name_original="1/2-2/3" src="d0_s10" type="structure" />
      <biological_entity id="o21312" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="proximally" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21313" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s10" value="diamond--shaped" value_original="diamond--shaped" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21314" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
      <biological_entity id="o21315" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="spine-tipped" value_original="spine-tipped" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s10" to="reflexed or squarrose" />
      </biological_entity>
      <biological_entity id="o21316" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s10" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="scabroso-hirsute" value_original="scabroso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o21308" id="r1965" name="in" negation="false" src="d0_s10" to="o21309" />
      <relation from="o21313" id="r1966" name="in" negation="false" src="d0_s10" to="o21314" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (8–) 10–18 (–20);</text>
      <biological_entity id="o21317" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s11" to="10" to_inclusive="false" />
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="20" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas usually white, rarely pink or bluish, laminae 6–12 (–20) × 0.7–1.2 mm.</text>
      <biological_entity id="o21318" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bluish" value_original="bluish" />
      </biological_entity>
      <biological_entity id="o21319" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 6–12 (–20);</text>
      <biological_entity id="o21320" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="20" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s13" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow becoming brown, 2.5–4 mm, throats narrowly funnelform, lobes triangular, 0.5–0.6 mm, glabrous.</text>
      <biological_entity id="o21321" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s14" value="yellow becoming brown" value_original="yellow becoming brown" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21322" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o21323" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae deep purple turning brown, obovoid to oblong-obovoid, ± falcate, not compressed, 1.2–2 × 0.4–0.6 mm, 7–9-nerved (faint), faces sericeous or densely strigillose;</text>
      <biological_entity id="o21324" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="depth" src="d0_s15" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple turning brown" value_original="purple turning brown" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s15" to="oblong-obovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s15" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="7-9-nerved" value_original="7-9-nerved" />
      </biological_entity>
      <biological_entity id="o21325" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi whitish, 3–4 mm.</text>
      <biological_entity id="o21326" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Que., Sask.; Ariz., Ark., Colo., Conn., D.C., Del., Idaho, Ill., Ind., Iowa, Kans., Maine, Mass., Md., Mich., Minn., Miss., Mo., Mont., N.Dak., N.J., N.Mex., N.Y., Nebr., Ohio, Okla., Oreg., Pa., R.I., S.Dak., Tex., Utah, Va., Vt., W.Va., Wash., Wis., Wyo.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">White heath aster</other_name>
  <other_name type="common_name">aster éricoïde</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Symphyotrichum ericoides resembles S. pilosum var. pilosum, which has larger heads, longer rays, and phyllaries that are not spine-tipped, though the revolute margins can make them appear so. Two subspecies and four weakly separated varieties of S. ericoides were recognized by A. G. Jones (1978). Tetraploids of var. ericoides on the eastern prairies can be difficult to distinguish from S. falcatum. A number of aster cultivars are sold under the name “Aster ericoides.” These are all derived from European garden plants and are either cultivars of S. dumosum, S. lateriflorum, S. pilosum, or S. racemosum, or hybrids involving one of those species and another taxon. The misapplication of the epithet ericoides dates back to the nineteenth century and has persisted in the horticultural literature.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants colonial, rhizomatous, not cormoid; involucres cylindro-campanulate when fresh</description>
      <determination>22a Symphyotrichum ericoides var. ericoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants cespitose, with cormoid caudices, not strongly rhizomatous; involucres broadly campanulate when fresh</description>
      <determination>22b Symphyotrichum ericoides var. pansum</determination>
    </key_statement>
  </key>
</bio:treatment>