<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">197</other_info_on_meta>
    <other_info_on_meta type="illustration_page">196</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">arctotideae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">gazania</taxon_name>
    <taxon_name authority="(Thunberg) Druce" date="1917" rank="species">linearis</taxon_name>
    <place_of_publication>
      <publication_title>Rep. Bot. Soc. Exch. Club Brit. Isles</publication_title>
      <place_in_publication>4: 624. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe arctotideae;genus gazania;species linearis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066797</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gorteria</taxon_name>
    <taxon_name authority="Thunberg" date="unknown" rank="species">linearis</taxon_name>
    <place_of_publication>
      <publication_title>Prodr. Pl. Cap.,</publication_title>
      <place_in_publication>162. 1800</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gorteria;species linearis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gazania</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">longiscapa</taxon_name>
    <taxon_hierarchy>genus Gazania;species longiscapa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems sometimes proximally woody.</text>
      <biological_entity id="o988" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes proximally" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o989" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o990" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades either linear to lanceolate and not lobed, 10–20 (–38) cm × 6–10 mm, or oblanceolate to oblong and pinnately lobed, 10–20 cm × 25–50 mm, or both;</text>
      <biological_entity id="o991" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="38" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="oblong" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s2" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bases usually attenuate, margins usually entire, sometimes ± prickly, revolute, midveins prominent, abaxial faces white-villous, adaxial faces glabrate to arachnose.</text>
      <biological_entity id="o992" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o993" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes more or less" name="architecture" src="d0_s3" value="prickly" value_original="prickly" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o994" name="midvein" name_original="midveins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o995" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="white-villous" value_original="white-villous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o996" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s3" to="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 3.5–8 cm diam.</text>
    </statement>
    <statement id="d0_s5">
      <text>(across rays).</text>
      <biological_entity id="o997" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="diameter" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles scapiform, (6–) 10–30 (–35) cm.</text>
      <biological_entity id="o998" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="scapiform" value_original="scapiform" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries: outer lanceolate, margins prickly-ciliate;</text>
      <biological_entity id="o999" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity constraint="outer" id="o1000" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o1001" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="prickly-ciliate" value_original="prickly-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>the inner with margins undulate, ciliate, with ± submarginal dark stripe, cuspidate.</text>
      <biological_entity id="o1002" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o1003" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s8" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s8" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o1004" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o1005" name="dark-stripe" name_original="dark stripe" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s8" value="submarginal" value_original="submarginal" />
      </biological_entity>
      <relation from="o1003" id="r88" name="with" negation="false" src="d0_s8" to="o1004" />
      <relation from="o1003" id="r89" name="with" negation="false" src="d0_s8" to="o1005" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 13–18;</text>
      <biological_entity id="o1006" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s9" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae yellow or orange, usually each with dark abaxial stripe and adaxial basal blotch or spot, (20–) 35–42 × 10 mm.</text>
      <biological_entity constraint="corolla" id="o1007" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" notes="" src="d0_s10" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" notes="" src="d0_s10" to="42" to_unit="mm" />
        <character name="width" notes="" src="d0_s10" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1008" name="stripe" name_original="stripe" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity constraint="adaxial basal" id="o1009" name="blotch" name_original="blotch" src="d0_s10" type="structure" />
      <biological_entity constraint="adaxial basal" id="o1010" name="spot" name_original="spot" src="d0_s10" type="structure" />
      <relation from="o1007" id="r90" modifier="usually" name="with" negation="false" src="d0_s10" to="o1008" />
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1–2 mm;</text>
      <biological_entity id="o1011" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 7–8 scales 3–4 mm.</text>
      <biological_entity id="o1012" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o1013" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s12" to="8" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o1012" id="r91" name="consist_of" negation="false" src="d0_s12" to="o1013" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, waste places, especially in urban coastal areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="urban coastal areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400(–900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., N.Mex.; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>