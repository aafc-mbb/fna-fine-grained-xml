<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="treatment_page">478</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">eutrochium</taxon_name>
    <taxon_name authority="(Barratt) E. E. Lamont" date="2004" rank="species">fistulosum</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 901. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus eutrochium;species fistulosum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066767</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Barratt" date="unknown" rank="species">fistulosum</taxon_name>
    <place_of_publication>
      <publication_title>Eupatoria Verticillata, no.</publication_title>
      <place_in_publication>1. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species fistulosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatoriadelphus</taxon_name>
    <taxon_name authority="(Barratt) R. M. King &amp; H. Robinson" date="unknown" rank="species">fistulosus</taxon_name>
    <taxon_hierarchy>genus Eupatoriadelphus;species fistulosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">purpureum</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="variety">angustifolium</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species purpureum;variety angustifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–350+ cm.</text>
      <biological_entity id="o3927" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="350" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually purple throughout, sometimes greenish or purple-spotted, hollow proximally, usually glabrous proximally (rarely pubescent toward bases when young), ± glandular-puberulent distally and among heads (glaucous throughout, at least when fresh).</text>
      <biological_entity id="o3928" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually; throughout" name="coloration_or_density" src="d0_s1" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="hollow" value_original="hollow" />
        <character is_modifier="false" modifier="usually; proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less; distally" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o3929" name="head" name_original="heads" src="d0_s1" type="structure" />
      <relation from="o3928" id="r308" name="among" negation="false" src="d0_s1" to="o3929" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly in 4s–6s (–7s);</text>
      <biological_entity id="o3930" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petioles (5–) 10–30 (–50) mm, glabrous;</text>
      <biological_entity id="o3931" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades pinnately veined, narrowly to broadly lanceolate, mostly (8–) 12–25 (–28) × (1.5–) 2–6 (–9) cm, bases gradually tapered, margins finely serrate (teeth rounded, blunt), abaxial faces sparingly and minutely ± scabrellous to glabrate, adaxial faces glabrous or sparingly puberulent.</text>
      <biological_entity id="o3932" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_length" src="d0_s4" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="28" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3933" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly; gradually" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o3934" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3935" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely more or less" name="relief" src="d0_s4" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3936" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in convex to rounded (dome-shaped), compound corymbiform arrays (ultimately broadly cylindric).</text>
      <biological_entity id="o3937" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o3938" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="convex" is_modifier="true" name="shape" src="d0_s5" to="rounded" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o3937" id="r309" name="in" negation="false" src="d0_s5" to="o3938" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres often purplish, 6.5–9 × 2.5–5 mm.</text>
      <biological_entity id="o3939" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries glabrous or outer with hairs on midveins.</text>
      <biological_entity constraint="outer" id="o3941" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3942" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <biological_entity id="o3943" name="midvein" name_original="midveins" src="d0_s7" type="structure" />
      <relation from="o3941" id="r310" name="with" negation="false" src="d0_s7" to="o3942" />
      <relation from="o3942" id="r311" name="on" negation="false" src="d0_s7" to="o3943" />
    </statement>
    <statement id="d0_s8">
      <text>Florets (4–) 5–7;</text>
      <biological_entity id="o3944" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas usually pale pinkish or purplish, 4.5–6 mm.</text>
      <biological_entity id="o3945" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="pale pinkish" value_original="pale pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 3–4.5 mm. 2n = 20.</text>
      <biological_entity id="o3946" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3947" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet lowlands, alluvial woods, along streams, moist meadows, bogs, marshes with permanently saturated or seasonally flooded organic soils, open sun or partial shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet lowlands" />
        <character name="habitat" value="alluvial woods" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="marshes" constraint="with permanently saturated or seasonally flooded organic soils" />
        <character name="habitat" value="saturated" modifier="permanently" />
        <character name="habitat" value="flooded organic soils" modifier="seasonally" />
        <character name="habitat" value="open sun" />
        <character name="habitat" value="partial shade" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1400+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Ky., La., Maine, Md., Mass., Mich., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Trumpetweed</other_name>
  <other_name type="common_name">hollow joepyeweed</other_name>
  <discussion>In the field, Eutrochium fistulosum is the most distinct species of the genus; herbarium specimens do not always document the distinguishing characteristics: heights commonly surpassing 2 m, proximal stem diameters usually 2 cm or greater, stems strongly glaucous (mostly hollow, sometimes distally hollow), leaves commonly 6 or 7 per node, arrays of heads commonly 30 × 22 cm.</discussion>
  
</bio:treatment>