<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">382</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Roth" date="1797" rank="genus">schkuhria</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1841" rank="species">multiflora</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Hooker)</publication_title>
      <place_in_publication>3: 322. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus schkuhria;species multiflora</taxon_hierarchy>
    <other_info_on_name type="fna_id">242428639</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bahia</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="species">neomexicana</taxon_name>
    <taxon_hierarchy>genus Bahia;species neomexicana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schkuhria</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">neomexicana</taxon_name>
    <taxon_hierarchy>genus Schkuhria;species neomexicana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly 3–12 (–25+) cm.</text>
      <biological_entity id="o12410" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± decumbent-ascending to erect.</text>
      <biological_entity id="o12411" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="less decumbent-ascending" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly 1–3 cm;</text>
      <biological_entity id="o12412" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear or lobed (lobes 3–7+, linear to filiform), faces puberulent and glanddotted.</text>
      <biological_entity id="o12413" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o12414" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles mostly 5–25 mm.</text>
      <biological_entity id="o12415" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± obconic, 5–6 mm.</text>
      <biological_entity id="o12416" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 7–10+, green to purple, weakly carinate, oblanceolate to obovate, ± hirsutulous and glanddotted.</text>
      <biological_entity id="o12417" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="10" upper_restricted="false" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s6" to="purple" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s6" value="carinate" value_original="carinate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="obovate" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 0.</text>
      <biological_entity id="o12418" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 15–30+;</text>
      <biological_entity id="o12419" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellowish, 1–2 mm.</text>
      <biological_entity id="o12420" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae blackish to buff, 3 mm, hispidulous to villous, especially on angles and/or at bases;</text>
      <biological_entity id="o12421" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="blackish" name="coloration" src="d0_s10" to="buff" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="hispidulous" name="pubescence" src="d0_s10" to="villous" />
      </biological_entity>
      <biological_entity id="o12422" name="angle" name_original="angles" src="d0_s10" type="structure" />
      <biological_entity id="o12423" name="base" name_original="bases" src="d0_s10" type="structure" />
      <relation from="o12421" id="r854" modifier="especially" name="on" negation="false" src="d0_s10" to="o12422" />
      <relation from="o12422" id="r855" name="at" negation="false" src="d0_s10" to="o12423" />
    </statement>
    <statement id="d0_s11">
      <text>pappi of 8 white to tawny or purplish, obovate-rounded or oblanceolate to ± quadrate, apically truncate or acute scales 1–2 mm. 2n = 22.</text>
      <biological_entity id="o12424" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character modifier="of" name="quantity" src="d0_s11" value="8" value_original="8" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="tawny or purplish" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s11" to="more or less quadrate apically truncate or acute" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s11" to="more or less quadrate apically truncate or acute" />
      </biological_entity>
      <biological_entity id="o12425" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12426" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, sandy slopes, and washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="sandy slopes" />
        <character name="habitat" value="washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., N.Mex., Tex.; Mexico (Chihuahua); South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>