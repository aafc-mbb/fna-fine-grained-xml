<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">chrysothamnus</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1895" rank="species">vaseyi</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 96. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysothamnus;species vaseyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066354</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">vaseyi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 58. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bigelowia;species vaseyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–30 cm;</text>
      <biological_entity id="o15639" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices woody, very branched, bark tan to dark gray, fibrous with age.</text>
      <biological_entity id="o15640" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o15641" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s1" to="dark gray" />
        <character constraint="with age" constraintid="o15642" is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o15642" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending, green becoming tan, glabrous or puberulent, resin-dotted to resinous.</text>
      <biological_entity id="o15643" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="green becoming tan" value_original="green becoming tan" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="resin-dotted" value_original="resin-dotted" />
        <character is_modifier="false" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ascending to spreading;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o15644" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades with faint midnerves, linear to oblanceolate, 10–40 × 1–2.5 mm, flat or sulcate, usually not twisted, margins eciliate or sparsely puberulent, apices acute to apiculate, faces usually glabrous, sometimes puberulent, usually gland-dotted, resinous.</text>
      <biological_entity id="o15645" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o15646" name="midnerve" name_original="midnerves" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity id="o15647" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o15648" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="apiculate" />
      </biological_entity>
      <biological_entity id="o15649" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o15645" id="r1428" name="with" negation="false" src="d0_s5" to="o15646" />
    </statement>
    <statement id="d0_s6">
      <text>Heads in dense cymiform arrays (to 5 cm wide), sometimes overtopped by distal leaves.</text>
      <biological_entity id="o15650" name="head" name_original="heads" src="d0_s6" type="structure" />
      <biological_entity id="o15651" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15652" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o15650" id="r1429" name="in" negation="false" src="d0_s6" to="o15651" />
      <relation from="o15650" id="r1430" modifier="sometimes" name="overtopped by" negation="false" src="d0_s6" to="o15652" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to obconic, (5–) 6–8 × 2–4 mm.</text>
      <biological_entity id="o15653" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="obconic" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–18 in 3–4 series, in weak vertical ranks, mostly tan, greenish apically, midnerves evident and ± expanded distally, ovate to elliptic, unequal, 1.5–7 × 1–1.8 mm, chartaceous, weakly keeled, margins scarious, ciliate to lacerate, apices acute to obtuse-rounded, faces glabrous or gland-dotted.</text>
      <biological_entity id="o15654" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o15655" from="12" name="quantity" src="d0_s8" to="18" />
        <character is_modifier="false" modifier="mostly" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="apically" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o15655" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o15656" name="rank" name_original="ranks" src="d0_s8" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s8" value="weak" value_original="weak" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o15657" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="evident" value_original="evident" />
        <character is_modifier="false" modifier="more or less; distally" name="size" src="d0_s8" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="elliptic" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o15658" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="ciliate" name="shape" src="d0_s8" to="lacerate" />
      </biological_entity>
      <biological_entity id="o15659" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse-rounded" />
      </biological_entity>
      <biological_entity id="o15660" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <relation from="o15654" id="r1431" name="in" negation="false" src="d0_s8" to="o15656" />
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 5–7;</text>
      <biological_entity id="o15661" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 4.5–6.5 mm, lobes 1.2–1.7 mm;</text>
      <biological_entity id="o15662" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15663" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style-branches 2.5–3.8 mm, appendages 0.8–1.2 mm.</text>
      <biological_entity id="o15664" name="branch-style" name_original="style-branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s11" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15665" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae reddish-brown, cylindric to turbinate, 4–5 mm, 5–10-nerved, faces glabrous;</text>
      <biological_entity id="o15666" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s12" to="turbinate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-10-nerved" value_original="5-10-nerved" />
      </biological_entity>
      <biological_entity id="o15667" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi tan (fine), 3.5–5 mm. 2n = 18.</text>
      <biological_entity id="o15668" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15669" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods (oak or ponderosa pine) and dry meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="ponderosa pine" />
        <character name="habitat" value="dry meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Vasey’s rabbitbrush</other_name>
  
</bio:treatment>