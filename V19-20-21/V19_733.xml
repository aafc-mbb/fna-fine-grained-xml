<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">443</other_info_on_meta>
    <other_info_on_meta type="illustration_page">442</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="genus">facelis</taxon_name>
    <taxon_name authority="(Lamarck) Schultz-Bipontinus" date="1866" rank="species">retusa</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>34: 532. 1866</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus facelis;species retusa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250005992</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">retusum</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>2: 758. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species retusum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually with decumbent to nearly procumbent branches arising from bases, 3–30 cm, loosely tomentose.</text>
      <biological_entity id="o4848" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o4849" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="decumbent" is_modifier="true" name="growth_form" src="d0_s0" to="nearly procumbent" />
        <character constraint="from bases" constraintid="o4850" is_modifier="false" name="orientation" src="d0_s0" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o4850" name="base" name_original="bases" src="d0_s0" type="structure" />
      <relation from="o4848" id="r477" name="with" negation="false" src="d0_s0" to="o4849" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves crowded;</text>
      <biological_entity id="o4851" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 7–20 (–30) × 1.5–4 mm, apices truncate-apiculate to retuse.</text>
      <biological_entity id="o4852" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4853" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate-apiculate" name="shape" src="d0_s2" to="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads: clusters subtended by bractlike leaves.</text>
      <biological_entity id="o4854" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o4855" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <relation from="o4854" id="r478" name="subtended by" negation="false" src="d0_s3" to="o4855" />
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 1.6 mm;</text>
      <biological_entity id="o4856" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="1.6" value_original="1.6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi 10–11 mm. 2n = 14.</text>
      <biological_entity id="o4857" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4858" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lawns, roadsides, pastures, other disturbed sites, usually on sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lawns" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="other disturbed sites" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Kans., La., Miss., N.C., Okla., S.C., Tex., Va.; South America; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Annual trampweed</other_name>
  
</bio:treatment>