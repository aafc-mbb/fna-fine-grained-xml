<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">630</other_info_on_meta>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">tetradymia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1843" rank="species">nuttallii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 447. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus tetradymia;species nuttallii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067722</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–120 cm.</text>
      <biological_entity id="o24217" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5+, erect, spiny, pannose but for glabrescent streaks.</text>
      <biological_entity id="o24218" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="spiny" value_original="spiny" />
        <character constraint="but-for streaks" constraintid="o24219" is_modifier="false" name="pubescence" src="d0_s1" value="pannose" value_original="pannose" />
      </biological_entity>
      <biological_entity id="o24219" name="streak" name_original="streaks" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: primaries forming straight or recurved spines, 5–25 mm;</text>
      <biological_entity id="o24220" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24221" name="primary" name_original="primaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24222" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="true" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o24221" id="r2230" name="forming" negation="false" src="d0_s2" to="o24222" />
    </statement>
    <statement id="d0_s3">
      <text>secondaries spatulate, 10–20 mm, tomentose to nearly glabrous.</text>
      <biological_entity id="o24223" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24224" name="secondary" name_original="secondaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s3" to="nearly glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 4–6.</text>
      <biological_entity id="o24225" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 2–12 mm.</text>
      <biological_entity id="o24226" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate to cylindric, 6–9 mm.</text>
      <biological_entity id="o24227" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s6" to="cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 4, oblong.</text>
      <biological_entity id="o24228" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 4;</text>
      <biological_entity id="o24229" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas bright-yellow, 8–10 mm.</text>
      <biological_entity id="o24230" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 4–6 mm, densely hirsute;</text>
      <biological_entity id="o24231" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of 75–100 bristles 9–10 mm. 2n = 60.</text>
      <biological_entity id="o24232" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o24233" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="75" is_modifier="true" name="quantity" src="d0_s11" to="100" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24234" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="60" value_original="60" />
      </biological_entity>
      <relation from="o24232" id="r2231" name="consist_of" negation="false" src="d0_s11" to="o24233" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky or sandy places, sagebrush scrub or shadscale scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy places" />
        <character name="habitat" value="sagebrush scrub" />
        <character name="habitat" value="shadscale scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  
</bio:treatment>