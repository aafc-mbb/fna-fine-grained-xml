<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">404</other_info_on_meta>
    <other_info_on_meta type="treatment_page">405</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaenactis</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1842" rank="section">macrocarphus</taxon_name>
    <taxon_name authority="(Kellogg) A. Gray in W. H. Brewer et al." date="1876" rank="species">nevadensis</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. Calif.</publication_title>
      <place_in_publication>1: 391. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus chaenactis;section macrocarphus;species nevadensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066313</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenopappus</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">nevadensis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>5: 46. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenopappus;species nevadensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2–10 (–12) cm (cespitose or ± matted);</text>
      <biological_entity id="o11628" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal indument ± thinning with age, whitish, lanuginose.</text>
      <biological_entity constraint="proximal" id="o11629" name="indument" name_original="indument" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanuginose" value_original="lanuginose" />
      </biological_entity>
      <biological_entity id="o11630" name="age" name_original="age" src="d0_s1" type="structure" />
      <relation from="o11629" id="r801" name="thinning with" negation="false" src="d0_s1" to="o11630" />
    </statement>
    <statement id="d0_s2">
      <text>Stems mostly 10–20+, decumbent to ± erect.</text>
      <biological_entity id="o11631" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="more or less erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ± basal, 2.5–5 cm;</text>
      <biological_entity id="o11632" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11633" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest blades ovate to deltate, ± plane, (1–) 2-pinnately lobed;</text>
      <biological_entity constraint="largest" id="o11634" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="deltate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="(1-)2-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>primary lobes mostly 2–4 pairs, ± congested, ultimate lobes ± plane.</text>
      <biological_entity constraint="primary" id="o11635" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o11636" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1 (–2) per stem.</text>
      <biological_entity id="o11637" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character constraint="per stem" constraintid="o11638" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11638" name="stem" name_original="stem" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles mostly ascending to erect, (0.5–) 3–11 cm.</text>
      <biological_entity id="o11639" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="mostly ascending" name="orientation" src="d0_s7" to="erect" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres obconic to ± cylindric.</text>
      <biological_entity id="o11640" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s8" to="more or less cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries: longest 9–12 (–14) mm;</text>
      <biological_entity id="o11641" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="longest" value_original="longest" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer stipitate-glandular, apices erect, ± rigid.</text>
      <biological_entity id="o11642" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o11643" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o11644" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s10" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Corollas 5.5–8 mm.</text>
      <biological_entity id="o11645" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 5.5–7.5 mm;</text>
      <biological_entity id="o11646" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s12" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: longest scales 3–5 mm. 2n = 12.</text>
      <biological_entity id="o11647" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="longest" id="o11648" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11649" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–mid Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Loose sandy or gravelly, mainly volcanic soils or scree (rarely on serpentine), openings in or above subalpine conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="volcanic soils" modifier="loose sandy or gravelly mainly" />
        <character name="habitat" value="scree" />
        <character name="habitat" value="openings" />
        <character name="habitat" value="subalpine conifer forests" />
        <character name="habitat" value="sandy" modifier="loose" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Sierra pincushion</other_name>
  <other_name type="common_name">Nevada dustymaidens</other_name>
  <discussion>Chaenactis nevadensis is known mainly from the northern Sierra Nevada and southern Cascade Range (Shasta to Placer counties, California; Washoe County, Nevada). It was recently discovered disjunct on ultramafic rocks of Bully Choop Mountain west of Redding, California, where it approaches small forms of C. suffrutescens in habit (see discussion there). It is sometimes cultivated in rock-gardens and may be found beyond its native range. Chaenactis nevadensis and C. suffrutescens appear to be sister or ancestor-derivative species. I have seen no evidence to support reports that C. nevadensis intergrades with C. alpigena (P. Stockwell 1940, as C. nevadensis var. mainsiana), with C. douglasii var. alpina (M. Graf 1999), or with any other taxon.</discussion>
  
</bio:treatment>