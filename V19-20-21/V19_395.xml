<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">285</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="Torrey ex Hooker" date="1833" rank="species">longipilum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 298. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species longipilum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416660</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–75 (–200) cm.</text>
      <biological_entity id="o9570" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally piloso-hirsute (hairs 6–15+ mm), distally piloso-hirsute (hairs 3–10+ mm), sometimes stellate-pubescent as well.</text>
      <biological_entity id="o9571" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="sometimes; well" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 3–8+, cauline (3–) 6–12+;</text>
      <biological_entity id="o9572" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o9573" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o9574" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s2" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="12" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate, 45–80 (–250+) × 12–30 (–40+) mm, lengths 4–7+ times widths, bases cuneate, margins entire, apices rounded to acute, faces piloso-hirsute (hairs 3–8+ mm).</text>
      <biological_entity id="o9575" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9576" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="250" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="4-7+" value_original="4-7+" />
      </biological_entity>
      <biological_entity id="o9577" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o9578" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9579" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o9580" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 10–20+ in paniculiform to nearly racemiform arrays.</text>
      <biological_entity id="o9581" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o9582" from="10" name="quantity" src="d0_s4" to="20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9582" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s4" to="nearly racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles stellate-pubescent and stipitate-glandular, sometimes piloso-hirsute as well.</text>
      <biological_entity id="o9583" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes; well" name="pubescence" src="d0_s5" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 9–13+.</text>
      <biological_entity id="o9584" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o9585" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s6" to="13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, 6–8 (–10) mm.</text>
      <biological_entity id="o9586" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–21+, apices acuminate, abaxial faces stellate-pubescent and stipitate-glandular.</text>
      <biological_entity id="o9587" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="21" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9588" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9589" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 30–40 (–60);</text>
      <biological_entity id="o9590" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="60" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s9" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, ca. 7 mm.</text>
      <biological_entity id="o9591" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae urceolate, 3–4+ mm;</text>
      <biological_entity id="o9592" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 35–40+, stramineous to sordid bristles in 2+ series, 5.5–6.5 mm.</text>
      <biological_entity id="o9593" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9594" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="35" is_modifier="true" name="quantity" src="d0_s12" to="40" upper_restricted="false" />
        <character char_type="range_value" from="stramineous" is_modifier="true" name="coloration" src="d0_s12" to="sordid" />
      </biological_entity>
      <biological_entity id="o9595" name="series" name_original="series" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" upper_restricted="false" />
      </biological_entity>
      <relation from="o9593" id="r885" name="consist_of" negation="false" src="d0_s12" to="o9594" />
      <relation from="o9594" id="r886" name="in" negation="false" src="d0_s12" to="o9595" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, prairies, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ark., Ill., Ind., Iowa, Kans., Ky., La., Mich., Minn., Mo., Nebr., Ohio, Okla., Tenn., Tex., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Hieracium longipilum may be no longer present in Quebec.</discussion>
  
</bio:treatment>