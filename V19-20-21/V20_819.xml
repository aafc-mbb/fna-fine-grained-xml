<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">357</other_info_on_meta>
    <other_info_on_meta type="treatment_page">355</other_info_on_meta>
    <other_info_on_meta type="illustration_page">354</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="L’Héritier" date="1789" rank="genus">boltonia</taxon_name>
    <taxon_name authority="(Linnaeus) L’Héritier" date="1789" rank="species">asteroides</taxon_name>
    <place_of_publication>
      <publication_title>Sert. Angl.,</publication_title>
      <place_in_publication>27. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus boltonia;species asteroides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416182</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Matricaria</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">asteroides</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.,</publication_title>
      <place_in_publication>116. 1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Matricaria;species asteroides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–200 mm;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous.</text>
      <biological_entity id="o25958" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s0" to="200" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect.</text>
      <biological_entity id="o25959" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades linear or lanceolate to oblanceolate;</text>
      <biological_entity id="o25960" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 20–220 × 2–30 mm, bases not decurrent.</text>
      <biological_entity constraint="cauline" id="o25961" name="leaf-blade" name_original="leaf_blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="220" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25962" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually in corymbiform, sometimes paniculiform arrays, branches spreading to ascending, most bracts leaflike, 15–120 × 2–18 mm.</text>
      <biological_entity id="o25963" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o25964" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o25965" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="ascending" />
      </biological_entity>
      <biological_entity id="o25966" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="120" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
      <relation from="o25963" id="r2404" name="in" negation="false" src="d0_s5" to="o25964" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–220 mm;</text>
      <biological_entity id="o25967" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="220" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 0–15, linear to subulate or ovate to oblanceolate, 2–12 (–62) mm.</text>
      <biological_entity id="o25968" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="subulate or ovate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="62" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres 3–5 × 4–14 mm.</text>
      <biological_entity id="o25969" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–5 series, subulate, linear, lanceolate, or spatulate, subequal, 0–6 merging down peduncles;</text>
      <biological_entity id="o25970" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
      <biological_entity id="o25971" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <biological_entity id="o25972" name="peduncle" name_original="peduncles" src="d0_s9" type="structure" />
      <relation from="o25970" id="r2405" name="in" negation="false" src="d0_s9" to="o25971" />
      <relation from="o25970" id="r2406" name="merging" negation="false" src="d0_s9" to="o25972" />
    </statement>
    <statement id="d0_s10">
      <text>outer 1.5–3.8 × 0.4–1.6 mm;</text>
      <biological_entity constraint="outer" id="o25973" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s10" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>inner 2.1–4 × 0.3–1.5.</text>
      <biological_entity constraint="inner" id="o25974" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.1" name="quantity" src="d0_s11" to="4" />
        <character char_type="range_value" from="0.3" name="quantity" src="d0_s11" to="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 20–60;</text>
      <biological_entity id="o25975" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas white to lilac, laminae 5–13 mm, tubes 0.5–1.7 mm.</text>
      <biological_entity id="o25976" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="lilac" />
      </biological_entity>
      <biological_entity id="o25977" name="lamina" name_original="laminae" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25978" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 65–170;</text>
      <biological_entity id="o25979" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="65" name="quantity" src="d0_s14" to="170" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas 1.5–2.6 mm.</text>
      <biological_entity id="o25980" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae obovoid, 1–3 × 0.8–2 mm, wings 0.1–0.5 mm wide;</text>
      <biological_entity id="o25981" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s16" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25982" name="wing" name_original="wings" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi awns 0.4–2 mm (lengths ± 2/3 cypselae).</text>
      <biological_entity constraint="pappi" id="o25983" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Sask.; Ala., Ark., Conn., Del., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., N.C., N.Dak., N.J., N.Y., Nebr., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Va., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">White doll’s-daisy</other_name>
  <other_name type="common_name">asterlike boltonia</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Variety asteroides is restricted to the coastal plain province. Varieties latisquama and recognita occur in the central lowland province and in the highland provinces between it and the coastal plain; var. recognita is also disjunct in Idaho and Oregon.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries linear to linear-attenuate or subulate, apices acute; pappus awn lengths to 2/3 cypselae</description>
      <determination>2a Boltonia asteroides var. asteroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries spatulate, oblanceolate, or linear-oblanceolate, apices cuspidate; pappus awn lengths 2/3 + cypselae</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries spatulate to obovate-spatulate, membranaceous margins broad, (2–)2.5–6 mm wide</description>
      <determination>2b Boltonia asteroides var. latisquama</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries, oblanceolate to linear-oblanceolate, membranaceous margins ± narrow, 1–2.5(–3) mm wide</description>
      <determination>2c Boltonia asteroides var. recognita</determination>
    </key_statement>
  </key>
</bio:treatment>