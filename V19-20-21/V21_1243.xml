<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="treatment_page">494</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="A. Gray" date="1852" rank="species">baccharidea</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 87. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species baccharidea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066247</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 100–150 cm.</text>
      <biological_entity id="o5259" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (ascending) branched, pubescent, sometimes glandular-pubescent.</text>
      <biological_entity id="o5260" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o5261" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 4–10 mm;</text>
      <biological_entity id="o5262" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 3-nerved from bases, rhombic-ovate, 10–40 × 10–20 mm, bases cuneate, margins laciniate-dentate, apices acute to obtuse, faces glabrate or glanddotted.</text>
      <biological_entity id="o5263" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="from bases" constraintid="o5264" is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5264" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o5265" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o5266" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="laciniate-dentate" value_original="laciniate-dentate" />
      </biological_entity>
      <biological_entity id="o5267" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o5268" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character name="pubescence" src="d0_s4" value="glanddotted" value_original="glanddotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in dense, paniculiform arrays.</text>
      <biological_entity id="o5269" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o5270" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o5269" id="r400" name="in" negation="false" src="d0_s5" to="o5270" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–40 mm, pubescent.</text>
      <biological_entity id="o5271" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="40" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres narrowly campanulate, 7–11 mm.</text>
      <biological_entity id="o5272" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 35–40 in 5–7 series, 3–5-striate, unequal, (glabrous, ± chartaceous) margins narrowly scarious (apices acute to acuminate);</text>
      <biological_entity id="o5273" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o5274" from="35" name="quantity" src="d0_s8" to="40" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" notes="" src="d0_s8" value="3-5-striate" value_original="3-5-striate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o5274" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <biological_entity id="o5275" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer ovate, inner lanceolate.</text>
      <biological_entity constraint="outer" id="o5276" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5277" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 16–17;</text>
      <biological_entity id="o5278" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s10" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas cream, 6–7 mm.</text>
      <biological_entity id="o5279" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2–3 mm, pubescent;</text>
      <biological_entity id="o5280" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 18–24 white, barbellate bristles.</text>
      <biological_entity id="o5282" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s13" to="24" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <relation from="o5281" id="r401" name="consist_of" negation="false" src="d0_s13" to="o5282" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o5281" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o5283" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to granitic soils, limestone slopes, granitic cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to granitic soils" />
        <character name="habitat" value="limestone slopes" />
        <character name="habitat" value="granitic cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>