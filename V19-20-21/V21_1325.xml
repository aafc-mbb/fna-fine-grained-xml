<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">514</other_info_on_meta>
    <other_info_on_meta type="mention_page">524</other_info_on_meta>
    <other_info_on_meta type="treatment_page">523</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="(Greene) Kittell in I. Tidestrom and T. Kittell" date="1941" rank="species">lancifolia</taxon_name>
    <place_of_publication>
      <publication_title>in I. Tidestrom and T. Kittell, Fl. Ariz. New Mex.,</publication_title>
      <place_in_publication>370. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species lancifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067106</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">lancifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 118. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lacinaria;species lancifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="(Britton) Rydberg" date="unknown" rank="species">kansana</taxon_name>
    <taxon_hierarchy>genus Liatris;species kansana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (20–) 40–80 cm.</text>
      <biological_entity id="o17662" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose.</text>
      <biological_entity id="o17663" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems glabrous.</text>
      <biological_entity id="o17664" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and lower cauline 3–5-nerved, narrowly oblong-lanceolate to narrowly spatulate-oblanceolate, 60–180 (–330) × 6–12 (–15) mm (usually becoming more densely arranged distally), abruptly reduced near midstem (continuing densely to immediately proximal to heads), essentially glabrous, glanddotted (bases of basal often fibrous-persistent).</text>
      <biological_entity id="o17665" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="lower" value_original="lower" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-5-nerved" value_original="3-5-nerved" />
        <character char_type="range_value" from="narrowly oblong-lanceolate" name="shape" src="d0_s3" to="narrowly spatulate-oblanceolate" />
        <character char_type="range_value" from="180" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="330" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s3" to="180" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
        <character constraint="near midstem" constraintid="o17666" is_modifier="false" modifier="abruptly" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="essentially" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17666" name="midstem" name_original="midstem" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in dense, spiciform arrays.</text>
      <biological_entity id="o17667" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o17668" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <relation from="o17667" id="r1220" name="in" negation="false" src="d0_s4" to="o17668" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0.</text>
      <biological_entity id="o17669" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate-cylindric to turbinate-campanulate, 7–9 × 4–7 mm.</text>
      <biological_entity id="o17670" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="turbinate-cylindric" name="shape" src="d0_s6" to="turbinate-campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series, ovate to oblong, unequal, glabrous, margins with hyaline borders, sometimes ciliolate, apices rounded to obtuse.</text>
      <biological_entity id="o17671" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s7" to="oblong" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17672" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o17673" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o17674" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o17675" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
      <relation from="o17671" id="r1221" name="in" negation="false" src="d0_s7" to="o17672" />
      <relation from="o17673" id="r1222" name="with" negation="false" src="d0_s7" to="o17674" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 5–8 (–12);</text>
      <biological_entity id="o17676" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="12" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes glabrous inside.</text>
      <biological_entity id="o17677" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 4–4.5 mm;</text>
      <biological_entity id="o17678" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths ± equaling corollas, bristles barbellate.</text>
      <biological_entity id="o17679" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o17680" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity id="o17681" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17682" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies (often wet or moist), banks of spring-fed streams, sandy and sandy-clay soils, saline sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="banks" constraint="of spring-fed streams" />
        <character name="habitat" value="spring-fed streams" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="sandy-clay soils" />
        <character name="habitat" value="sites" modifier="saline" />
        <character name="habitat" value="saline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Nebr., N.Mex., S.Dak., Tex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <other_name type="common_name">Great Plains gayfeather</other_name>
  <discussion>The abrupt size reduction of cauline leaves in Liatris lancifolia is similar to that of L. spicata var. resinosa; the long, dense spikes and wider basal leaves are more like those of L. spicata var. spicata. Little differentiation exists between L. lancifolia and L. spicata, but L. lancifolia is maintained here at specific rank, coordinate with L. spicata, primarily because of its wide geographic disjunction and generally different habitat. Recognition that the two elements within L. spicata have a nearly analogous relationship of range and habitat might provide rationale for treating all three of these closely similar taxa at equivalent rank. Liatris lancifolia is expected in Oklahoma.</discussion>
  
</bio:treatment>