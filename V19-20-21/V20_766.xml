<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">268</other_info_on_meta>
    <other_info_on_meta type="treatment_page">335</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Howell" date="1900" rank="species">aliceae</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N.W. Amer.,</publication_title>
      <place_in_publication>317. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species aliceae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066546</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–8 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudices relatively thick.</text>
      <biological_entity id="o14878" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o14879" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, loosely strigose to glabrate, sometimes hirsuto-villous proximally, eglandular.</text>
      <biological_entity id="o14880" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="loosely strigose" name="pubescence" src="d0_s2" to="glabrate" />
        <character is_modifier="false" modifier="sometimes; proximally" name="pubescence" src="d0_s2" value="hirsuto-villous" value_original="hirsuto-villous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (usually persistent) and cauline;</text>
      <biological_entity id="o14881" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal cauline blades often prominently 3-nerved, oblanceolate-spatulate to elliptic-spatulate, 80–200 × 15–35 mm, margins usually coarsely dentate (teeth 1–5 pairs), sometimes entire, faces weakly strigose to hirsute, eglandular;</text>
      <biological_entity constraint="basal and proximal cauline" id="o14882" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often prominently" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="oblanceolate-spatulate" name="shape" src="d0_s4" to="elliptic-spatulate" />
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s4" to="200" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14883" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually coarsely" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14884" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="weakly strigose" name="pubescence" src="d0_s4" to="hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades becoming lanceolate to linear, sometimes ovate, gradually reduced distally (bases weakly subclasping or not clasping).</text>
      <biological_entity constraint="cauline" id="o14885" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–4 (–7) (from branches distal to midstem).</text>
      <biological_entity id="o14886" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="7" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 6–10 × (10–) 12–16 (–20) mm.</text>
      <biological_entity id="o14887" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s7" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3 (–4) series, glabrous or moderately villous to white-hirsuto-villous (sometimes only at peduncle-involucre region), densely minutely glandular.</text>
      <biological_entity id="o14888" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="moderately villous" name="pubescence" notes="" src="d0_s8" to="white-hirsuto-villous" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o14889" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <relation from="o14888" id="r1359" name="in" negation="false" src="d0_s8" to="o14889" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 45–80;</text>
      <biological_entity id="o14890" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="45" name="quantity" src="d0_s9" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white to pinkish purple, 10–15 mm, laminae coiling at tips.</text>
      <biological_entity id="o14891" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pinkish purple" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14892" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character constraint="at tips" constraintid="o14893" is_modifier="false" name="shape" src="d0_s10" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o14893" name="tip" name_original="tips" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 2.5–4 mm.</text>
      <biological_entity constraint="disc" id="o14894" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2–2.8 mm, 2 (–4) -nerved, faces moderately to densely strigose;</text>
      <biological_entity id="o14895" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2(-4)-nerved" value_original="2(-4)-nerved" />
      </biological_entity>
      <biological_entity id="o14896" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae, inner of 18–32 bristles.</text>
      <biological_entity id="o14897" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o14898" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o14899" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s13" to="32" />
      </biological_entity>
      <relation from="o14897" id="r1360" name="outer of" negation="false" src="d0_s13" to="o14898" />
      <relation from="o14897" id="r1361" name="inner of" negation="false" src="d0_s13" to="o14899" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky ridges and slopes, talus, wet meadows, open roadsides, fir, hemlock-fir, chaparral, sometimes on serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="open roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>147.</number>
  <other_name type="common_name">Alice Eastwood’s fleabane</other_name>
  
</bio:treatment>