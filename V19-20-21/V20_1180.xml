<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">467</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">538</other_info_on_meta>
    <other_info_on_meta type="treatment_page">531</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rydberg) G. L. Nesom" date="1995" rank="section">Occidentales</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 271. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section Occidentales</taxon_hierarchy>
    <other_info_on_name type="fna_id">316938</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Occidentales</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Colorado,</publication_title>
      <place_in_publication>352. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;unranked Occidentales;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, eglandular;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o1081" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually simple, sometimes ± branched.</text>
      <biological_entity id="o1082" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes more or less" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads radiate.</text>
      <biological_entity id="o1083" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="radiate" value_original="radiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets fewer than disc-florets, in 1 series (conspicuous).</text>
      <biological_entity id="o1084" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character constraint="than disc-florets" constraintid="o1085" is_modifier="false" name="quantity" src="d0_s4" value="fewer" value_original="fewer" />
      </biological_entity>
      <biological_entity id="o1085" name="disc-floret" name_original="disc-florets" src="d0_s4" type="structure" />
      <biological_entity id="o1086" name="series" name_original="series" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <relation from="o1084" id="r97" name="in" negation="false" src="d0_s4" to="o1086" />
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas ± ampliate, lobes erect.</text>
      <biological_entity constraint="disc" id="o1087" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s5" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o1088" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pappi shorter than to equaling disc-florets at flowering.</text>
      <biological_entity id="o1090" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character is_modifier="true" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>x = 8.</text>
      <biological_entity id="o1089" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character constraint="than to equaling disc-florets" constraintid="o1090" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="x" id="o1091" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>214d.3.</number>
  <discussion>Species 12 (12 in the flora).</discussion>
  <references>
    <reference>Allen, G. A. 1984. Morphological and cytological variation within the western North American Aster occidentalis complex (Asteraceae). Syst. Bot. 9: 175–191.</reference>
  </references>
  
</bio:treatment>