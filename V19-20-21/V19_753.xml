<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James D. Morefield</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">388</other_info_on_meta>
    <other_info_on_meta type="mention_page">450</other_info_on_meta>
    <other_info_on_meta type="mention_page">455</other_info_on_meta>
    <other_info_on_meta type="treatment_page">454</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MICROPUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 927. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 398. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus MICROPUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek micros, small, and pous, foot, perhaps alluding to tiny receptacles</other_info_on_name>
    <other_info_on_name type="fna_id">120625</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(de Candolle) Smoljaninova" date="unknown" rank="genus">Bombycilaena</taxon_name>
    <taxon_hierarchy>genus Bombycilaena;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–50 cm.</text>
      <biological_entity id="o16603" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, ± erect, or 2–5 [–10], ascending to erect [prostrate].</text>
      <biological_entity id="o16604" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="10" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate [opposite];</text>
      <biological_entity id="o16605" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades narrowly oblanceolate to elliptic [spatulate].</text>
      <biological_entity id="o16606" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s4" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually in glomerules of 2–5 in racemiform to paniculiform or distally ± dichasiform [axillary] arrays, sometimes borne singly.</text>
      <biological_entity id="o16607" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="racemiform" name="architecture" src="d0_s5" to="paniculiform" />
        <character is_modifier="false" modifier="distally more or less" name="arrangement" src="d0_s5" value="dichasiform" value_original="dichasiform" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o16608" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" modifier="of" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <relation from="o16607" id="r1494" name="in" negation="false" src="d0_s5" to="o16608" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres inconspicuous.</text>
      <biological_entity id="o16609" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 4–6, ± equal (unlike paleae, scarious, hyaline).</text>
      <biological_entity id="o16610" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="6" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles depressed-spheric or obovoid (heights 0.5–1.8 times diams.), glabrous.</text>
      <biological_entity id="o16611" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="depressed-spheric" value_original="depressed-spheric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate paleae falling, erect to incurved;</text>
      <biological_entity id="o16612" name="palea" name_original="paleae" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bodies with 5+ nerves (nerves ± parallel, obscure), obovoid, saccate most of lengths (trigonously [evenly] compressed, galeate, abaxially rounded [corniculate-crested], each enclosing a floret);</text>
      <biological_entity id="o16613" name="body" name_original="bodies" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="obovoid" value_original="obovoid" />
        <character constraint="of lengths" is_modifier="false" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o16614" name="nerve" name_original="nerves" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <relation from="o16613" id="r1495" name="with" negation="false" src="d0_s10" to="o16614" />
    </statement>
    <statement id="d0_s11">
      <text>wings ± erect (and lateral) or inflexed (and subapical).</text>
      <biological_entity id="o16615" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="inflexed" value_original="inflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate paleae 0 or 1–3, falling, erect in fruit (not enlarged), shorter than pistillate paleae;</text>
      <biological_entity id="o16616" name="palea" name_original="paleae" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="3" />
        <character is_modifier="false" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
        <character constraint="in fruit" constraintid="o16617" is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character constraint="than pistillate paleae" constraintid="o16618" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o16617" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o16618" name="palea" name_original="paleae" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>bodies linear-lanceolate to oblanceolate.</text>
      <biological_entity id="o16619" name="body" name_original="bodies" src="d0_s13" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s13" to="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate florets 4–12.</text>
      <biological_entity id="o16620" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s14" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Functionally staminate florets 2–5;</text>
      <biological_entity id="o16621" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>corolla lobes 4–5, ± equal.</text>
      <biological_entity constraint="corolla" id="o16622" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s16" to="5" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s16" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Bisexual florets 0.</text>
      <biological_entity id="o16623" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s17" value="bisexual" value_original="bisexual" />
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Cypselae brown, monomorphic: ± trigonously [evenly] compressed, ± obovoid, curved, gibbous abaxially, faces glabrous, smooth, shiny, corolla scars ± lateral;</text>
      <biological_entity id="o16624" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="more or less trigonously" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="course" src="d0_s18" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s18" value="gibbous" value_original="gibbous" />
      </biological_entity>
      <biological_entity id="o16625" name="face" name_original="faces" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o16626" name="scar" name_original="scars" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s18" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pappi: pistillate 0, staminate 0 or of 1–5 bristles (hidden in heads).</text>
      <biological_entity id="o16628" name="bristle" name_original="bristles" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s19" to="5" />
      </biological_entity>
      <relation from="o16627" id="r1496" name="consist_of" negation="false" src="d0_s19" to="o16628" />
    </statement>
    <statement id="d0_s20">
      <text>x = 14.</text>
      <biological_entity id="o16627" name="pappus" name_original="pappi" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
        <character name="presence" src="d0_s19" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="staminate" value_original="staminate" />
        <character name="presence" src="d0_s19" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 1" is_modifier="false" name="quantity" src="d0_s19" to="5 bristles" />
      </biological_entity>
      <biological_entity constraint="x" id="o16629" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico, s Europe, sw Asia, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>100.</number>
  <other_name type="common_name">Cottonseed</other_name>
  <other_name type="common_name">micrope</other_name>
  <other_name type="common_name">falzblume</other_name>
  <discussion>Species 5 (2 in the flora).</discussion>
  <discussion>See discussion of Filagininae following the tribal description (p. 385).</discussion>
  <discussion>Micropus species are found mostly in dry, open habitats of Mediterranean climates. In the flora, they are known only from west-draining portions of the Californian Floristic Province and the Willamette Valley in Oregon.</discussion>
  <discussion>The two North American species constitute Micropus sect. Rhyncholepis Nuttall. Recent European workers (e.g., J. Holub 1998) have included sect. Rhyncholepis in Bombycilaena, leaving M. supinus Linnaeus in a monotypic genus. Based on phylogenetic data (J. D. Morefield 1992), that approach would include in Bombycilaena species ancestral to, and derived from ancestors of, Micropus. I maintain Micropus in its traditional sense here. Micropus and Psilocarphus appear to be monophyletic sister genera derived from near or within Stylocline. A malformed specimen from Monterey County, California, appears to be a sterile hybrid between M. californicus and a species of Psilocarphus.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate paleae 8–12 in 2 series, bodies mainly chartaceous, cartilaginous medially, wings prominent, subapical, inflexed, plane to concave; cypselae: corolla scars in distal 1/4; receptacle heights 1.2–1.8 times diams.; staminate paleae mostly 1–3; staminate corolla lobes 4 (–5); staminate pappi of 1–5 bristles</description>
      <determination>1 Micropus amphibolus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate paleae 4–7(–8) in 1 series, bodies cartilaginous to bony throughout, wings obscure, lateral, ± erect, involute; cypselae: corolla scars ± median; receptacle heights 0.5–0.8 times diams.; staminate paleae 0; staminate corolla lobes usually 5; staminate pappi 0 or of 1 bristle</description>
      <determination>2 Micropus californicus</determination>
    </key_statement>
  </key>
</bio:treatment>