<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">484</other_info_on_meta>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cavanilles" date="1797" rank="genus">stevia</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1820" rank="species">viscida</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>4(fol.): 110, plate 351. 1818</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <place_in_publication>4(qto.): 140. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus stevia;species viscida</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067615</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–100 cm.</text>
      <biological_entity id="o3779" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly alternate (usually crowded with axillary clusters of smaller leaves);</text>
      <biological_entity id="o3780" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles 0;</text>
      <biological_entity id="o3781" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (3–nerved) linear-oblong, linear-oblanceolate, or linear, mostly 2–5 cm, margins shallowly toothed distally or entire.</text>
      <biological_entity id="o3782" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3783" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shallowly; distally" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in open, subcorymbiform arrays.</text>
      <biological_entity id="o3784" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o3785" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="subcorymbiform" value_original="subcorymbiform" />
      </biological_entity>
      <relation from="o3784" id="r297" name="in" negation="false" src="d0_s4" to="o3785" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles mostly 5–30 mm, viscid-puberulent and stipitate-glandular.</text>
      <biological_entity id="o3786" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="30" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="viscid-puberulent" value_original="viscid-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 6–8 mm, stipitate and sessile-glandular.</text>
      <biological_entity id="o3787" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries stipitate and sessile-glandular, apices acute to acuminate.</text>
      <biological_entity id="o3788" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o3789" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas white or pink, purplish, or rose, lobes glandular and sparsely hispid-villous.</text>
      <biological_entity id="o3790" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="rose" value_original="rose" />
      </biological_entity>
      <biological_entity id="o3791" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hispid-villous" value_original="hispid-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi ± equaling corollas.</text>
      <biological_entity id="o3793" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 22, 33, 44.</text>
      <biological_entity id="o3792" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o3794" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="22" value_original="22" />
        <character name="quantity" src="d0_s10" value="33" value_original="33" />
        <character name="quantity" src="d0_s10" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, pastures, other disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="other disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Tex.; Mexico; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Viscid candyleaf</other_name>
  
</bio:treatment>