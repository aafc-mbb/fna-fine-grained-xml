<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James D. Morefield</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">388</other_info_on_meta>
    <other_info_on_meta type="mention_page">450</other_info_on_meta>
    <other_info_on_meta type="mention_page">454</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="treatment_page">456</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">PSILOCARPHUS</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 340. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus PSILOCARPHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek psilos, slender, and karphos, chaff, alluding to papery paleae of heads</other_info_on_name>
    <other_info_on_name type="fna_id">127330</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 1–15 (–20) cm.</text>
      <biological_entity id="o10572" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, erect, or 2–10, ascending to ± prostrate.</text>
      <biological_entity id="o10573" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="10" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="more or less prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly opposite;</text>
      <biological_entity id="o10574" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to ovate or obovate.</text>
      <biological_entity id="o10575" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="ovate or obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly or in glomerules of 2–4 in ± dichasiform (sometimes ± paniculiform) arrays.</text>
      <biological_entity id="o10576" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o10578" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o10577" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o10578" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="arrangement" src="d0_s5" value="dichasiform" value_original="dichasiform" />
      </biological_entity>
      <relation from="o10576" id="r979" name="borne" negation="false" src="d0_s5" to="o10577" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 0.</text>
      <biological_entity id="o10579" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 0.</text>
      <biological_entity id="o10580" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles ± obovoid (sometimes lobed, heights 1–2 times diams.), glabrous.</text>
      <biological_entity id="o10581" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate paleae falling, radiating in all directions;</text>
      <biological_entity id="o10582" name="palea" name_original="paleae" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character constraint="in directions" constraintid="o10583" is_modifier="false" name="arrangement" src="d0_s9" value="radiating" value_original="radiating" />
      </biological_entity>
      <biological_entity id="o10583" name="direction" name_original="directions" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>bodies with 5+ nerves (nerves reticulate, prominent), obovoid to ± cylindric, saccate most of lengths (terete, galeate or cucullate, each loosely enclosing a floret);</text>
      <biological_entity id="o10584" name="body" name_original="bodies" src="d0_s10" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" notes="" src="d0_s10" to="more or less cylindric" />
        <character constraint="of lengths" is_modifier="false" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o10585" name="nerve" name_original="nerves" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <relation from="o10584" id="r980" name="with" negation="false" src="d0_s10" to="o10585" />
    </statement>
    <statement id="d0_s11">
      <text>wings inflexed (± lateral).</text>
      <biological_entity id="o10586" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="inflexed" value_original="inflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate paleae 0.</text>
      <biological_entity id="o10587" name="palea" name_original="paleae" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate florets (8–) 20–100+.</text>
      <biological_entity id="o10588" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s13" to="20" to_inclusive="false" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Functionally staminate florets 2–10;</text>
      <biological_entity id="o10589" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corolla lobes 4–5, ± equal.</text>
      <biological_entity constraint="corolla" id="o10590" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s15" to="5" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s15" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Bisexual florets 0.</text>
      <biological_entity id="o10591" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae brown, monomorphic: terete to ± compressed, narrowly obovoid (then slightly incurved, abaxially gibbous) to ± cylindric, faces glabrous, smooth, shiny;</text>
      <biological_entity id="o10592" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s17" to="more or less compressed narrowly obovoid" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s17" to="more or less compressed narrowly obovoid" />
      </biological_entity>
      <biological_entity id="o10593" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>corolla scars usually ± subapical;</text>
      <biological_entity id="o10594" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o10595" name="scar" name_original="scars" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="position" src="d0_s18" value="subapical" value_original="subapical" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pappi 0.</text>
      <biological_entity id="o10596" name="cypsela" name_original="cypselae" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>x = 14.</text>
      <biological_entity id="o10597" name="pappus" name_original="pappi" src="d0_s19" type="structure">
        <character name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o10598" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Amphitropical, w North America, nw Mexico, s South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Amphitropical" establishment_means="native" />
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>101.</number>
  <other_name type="common_name">Woolly marbles</other_name>
  <other_name type="common_name">woollyheads</other_name>
  <discussion>Species 5 (5 in the flora).</discussion>
  <discussion>See discussion of Filagininae following the tribal description (p. 385).</discussion>
  <discussion>In the flora area, Psilocarphus inhabits sites with Mediterranean, semiarid, and cool-temperate climates. In the south, it remains within the Californian Floristic Province, not entering the Mojave and Sonoran deserts; to the north, it extends from the Pacific Northwest eastward across the Great Basin, Columbia Plateau, and northern Rocky Mountains to the western edge of the northern Great Plains. Ongoing degradation of vernal pool habitats in California may soon justify conservation concern for P. chilensis and P. brevissimus var. multiflorus.</discussion>
  <discussion>The amphitropical species of Psilocarphus and Micropsis (P. brevissimus var. brevissimus, P. chilensis, M. dasycarpa) occupy littoral habitats; migratory shorebirds probably facilitate occasional long-distance dispersal of their light cypsela-palea complexes (A. Cronquist 1950). Populations of these self-pollinating species can establish from one cypsela.</discussion>
  <discussion>Psilocarphus is monophyletic and probably sister to Micropus, with ancestors in or near Stylocline (J. D. Morefield 1992). Psilocarphus is easily recognized by leaves opposite and paleae cucullate or galeate, reticulately nerved; the clusters of heads resemble compact bunches of woolly grapes or marbles. Differences between species are slight but consistent in most specimens.</discussion>
  <references>
    <reference>Cronquist, A. 1950. A review of the genus Psilocarphus. Res. Stud. State Coll. Wash. 18: 71–89.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Largest heads 6–14 mm; pistillate paleae collectively hidden by indument and/or longest 2.8–4 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Largest heads mostly 3–6 mm; pistillate paleae usually individually visible through indument, longest mostly 1.5–2.7 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads ± ovoid, largest 9–14 mm; receptacles deeply lobed; pistillate paleae ± cylindric, lengths mostly 3.5–6 times longest diams. (wings ± median)</description>
      <determination>1 Psilocarphus brevissimus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads spheric, largest 6–9 mm; receptacles unlobed or shallowly lobed; pistillate paleae obovoid, lengths 1.5–3 times longest diams. (wings supramedian to subapical)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capitular leaves mostly lanceolate to ovate, widest in proximal 2/3, longest mostly 8–15 mm, lengths mostly 1.5–4 times widths; plants usually densely lanuginose; cypselae narrowly obovoid</description>
      <determination>1 Psilocarphus brevissimus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capitular leaves mostly oblanceolate to nearly linear, widest in distal 1/3, longest mostly 17–35 mm, lengths mostly 4.5–9 times widths; plants ± sericeous; cypselae ± cylindric</description>
      <determination>2 Psilocarphus elatior</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capitular leaves linear to narrowly oblanceolate, lengths mostly 6–12 times widths, (3–) 3.5–5 times head heights; cypselae ± cylindric</description>
      <determination>3 Psilocarphus oregonus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capitular leaves mostly spatulate to obovate or ovate, lengths mostly 1.2–5 times widths, 1–2.5(–3) times head heights; cypselae ± obovoid</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capitular leaves mostly not appressed to heads, spatulate to obovate, lengths mostly 2–5 widths; proximal internode lengths mostly 1–2(–3) times leaf lengths; staminate corolla lobes mostly 5</description>
      <determination>4 Psilocarphus tenellus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capitular leaves appressed to heads, ovate to broadly elliptic, lengths mostly 1.2–1.8(–2) times widths; proximal internode lengths (2–)3–6 times leaf lengths; staminate corolla lobes mostly 4</description>
      <determination>5 Psilocarphus chilensis</determination>
    </key_statement>
  </key>
</bio:treatment>