<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(S. F. Blake) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">psilocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 87. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety psilocarpa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068288</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) Britton" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="S. F. Blake" date="unknown" rank="variety">psilocarpus</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>27: 376. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;variety psilocarpus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="(S. F. Blake) L. C. Anderson" date="unknown" rank="subspecies">psilocarpus</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;subspecies psilocarpus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–50 cm.</text>
      <biological_entity id="o23275" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems yellowish green, leafy, compactly tomentose.</text>
      <biological_entity id="o23276" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="compactly" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves yellowish green;</text>
      <biological_entity id="o23277" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1-nerved, narrowly oblanceolate, 30–70 × 1–2 mm, faces glabrate.</text>
      <biological_entity id="o23278" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s3" to="70" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23279" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 8.5–12.2 mm.</text>
      <biological_entity id="o23280" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s4" to="12.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 15–18, apices erect, acute or acuminate to cuspidate, abaxial faces glabrous.</text>
      <biological_entity id="o23281" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s5" to="18" />
      </biological_entity>
      <biological_entity id="o23282" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="cuspidate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23283" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 8.6–9.6 mm, tubes sparsely hairy, lobes 1.3–1.5 mm, glabrous;</text>
      <biological_entity id="o23284" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="8.6" from_unit="mm" name="some_measurement" src="d0_s6" to="9.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23285" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23286" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages slightly longer than stigmatic portions.</text>
      <biological_entity constraint="style" id="o23287" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic portions" constraintid="o23288" is_modifier="false" name="length_or_size" src="d0_s7" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o23288" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae glabrous;</text>
      <biological_entity id="o23289" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 6.8–9.1 mm. 2n = 18.</text>
      <biological_entity id="o23290" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="6.8" from_unit="mm" name="some_measurement" src="d0_s9" to="9.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23291" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering later summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush and Elymus salinus communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="communities" modifier="salinus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21p.</number>
  <other_name type="common_name">Smooth-fruit rabbitbrush</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety psilocarpa is apparently restricted to Carbon, Duchesne, Emery, and Wasatch counties in east-central Utah.</discussion>
  <discussion>L. C. Anderson (1986b) excluded the name Ericameria nauseosa var. glareosa (M. E. Jones) G. L. Nesom &amp; G. I. Baird [as Chrysothamnus nauseosus subsp. glareosus (M. E. Jones) H. M. Hall &amp; Clements], because he had seen no herbarium or field material. Jones’s original description is sketchy; he mentioned its similarity to subsp. leiosperma. S. L. Welsh et al. (1987) noted the uncertain disposition of this taxon. A. Cronquist (1994) recognized C. nauseosus var. glareosus (M. E. Jones) H. M. Hall and placed C. nauseosus var. psilocarpus in synonymy, noting that it was equal to E. nauseosa var. glareosa (M. E. Jones) G. L. Nesom &amp; G. I. Baird. Without more substantial documentation, use of var. psilocarpa is preferred over var. glareosa, which we take to be of uncertain application.</discussion>
  
</bio:treatment>