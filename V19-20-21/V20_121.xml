<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(L. C. Anderson) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">washoensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 88. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species nauseosa;variety washoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068293</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(Pallas ex Pursh) Britton" date="unknown" rank="species">nauseosus</taxon_name>
    <taxon_name authority="L. C. Anderson" date="unknown" rank="subspecies">washoensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>38: 315, fig. 3. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species nauseosus;subspecies washoensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ericameria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nauseosa</taxon_name>
    <taxon_name authority="(L. C. Anderson) G. L. Nesom &amp; G. I. Baird" date="unknown" rank="variety">washoensis</taxon_name>
    <taxon_hierarchy>genus Ericameria;species nauseosa;variety washoensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–45 (–100) cm.</text>
      <biological_entity id="o2086" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems whitish, sparsely leafy, loosely tomentose.</text>
      <biological_entity id="o2087" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves grayish green;</text>
      <biological_entity id="o2088" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish green" value_original="grayish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 1–3-nerved, spatulate to narrowly oblanceolate, 20–30 × 2–3 mm, faces loosely hairy.</text>
      <biological_entity id="o2089" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-3-nerved" value_original="1-3-nerved" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2090" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 10–12 mm.</text>
      <biological_entity id="o2091" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 16–20 (–30), (brownish costal nerves often evident) apices acute to obtuse, outer abaxial faces sparsely villous to tomentose, inner tomentulose.</text>
      <biological_entity id="o2092" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="30" />
        <character char_type="range_value" from="16" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o2093" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="outer abaxial" id="o2094" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparsely villous" name="pubescence" src="d0_s5" to="tomentose" />
        <character is_modifier="false" name="position" src="d0_s5" value="inner" value_original="inner" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas 7.5–9 (–10) mm, tubes puberulent, lobes 1.3–1.6 mm, villous;</text>
      <biological_entity id="o2095" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2096" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o2097" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s6" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style appendages longer than stigmatic portions.</text>
      <biological_entity constraint="style" id="o2098" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character constraint="than stigmatic portions" constraintid="o2099" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o2099" name="portion" name_original="portions" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Cypselae distally pilose;</text>
      <biological_entity id="o2100" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi 6.5–10 mm. 2n = 18.</text>
      <biological_entity id="o2101" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2102" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, open sites in juniper and pinyon grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" modifier="rocky" constraint="in juniper and pinyon grasslands" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="pinyon grasslands" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21u.</number>
  <other_name type="common_name">Washoe rabbitbrush</other_name>
  <discussion>Variety washoensis is known from northeastern California and northwestern Nevada.</discussion>
  <discussion>of conservation concern</discussion>
  
</bio:treatment>