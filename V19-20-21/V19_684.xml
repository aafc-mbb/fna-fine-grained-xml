<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">417</other_info_on_meta>
    <other_info_on_meta type="treatment_page">419</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Kirpicznikov" date="1950" rank="genus">pseudognaphalium</taxon_name>
    <taxon_name authority="(Davidson) Anderberg" date="1991" rank="species">beneolens</taxon_name>
    <place_of_publication>
      <publication_title>Opera Bot.</publication_title>
      <place_in_publication>104: 147. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus pseudognaphalium;species beneolens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067384</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Davidson" date="unknown" rank="species">beneolens</taxon_name>
    <place_of_publication>
      <publication_title>Bull. S. Calif. Acad. Sci.</publication_title>
      <place_in_publication>17: 17, unnumb. fig. p. 16. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species beneolens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">canescens</taxon_name>
    <taxon_name authority="(Davidson) Stebbins &amp; D. J. Keil" date="unknown" rank="subspecies">beneolens</taxon_name>
    <taxon_hierarchy>genus Gnaphalium;species canescens;subspecies beneolens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pseudognaphalium</taxon_name>
    <taxon_name authority="(de Candolle) Anderberg" date="unknown" rank="species">canescens</taxon_name>
    <taxon_name authority="(Davidson) Kartesz" date="unknown" rank="subspecies">beneolens</taxon_name>
    <taxon_hierarchy>genus Pseudognaphalium;species canescens;subspecies beneolens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or short-lived perennials, 30–80 (–110) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o13122" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems persistently tomentose, not glandular.</text>
      <biological_entity id="o13124" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="persistently" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades mostly linear, 3–6 cm × 1.5–3.5 mm (sometimes smaller distally), bases not clasping, decurrent 5–15 mm, margins flat, faces concolor, loosely tomentose, not glandular.</text>
      <biological_entity id="o13125" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13126" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13127" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o13128" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="concolor" value_original="concolor" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads usually in loose, paniculiform arrays.</text>
      <biological_entity id="o13129" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o13130" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o13129" id="r1194" name="in" negation="false" src="d0_s4" to="o13130" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate-campanulate, 5–6 mm.</text>
      <biological_entity id="o13131" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in (4–) 5–6 (–7) series, white (opaque, dull to shiny), ovate to ovate-oblong (inner usually with filiform keel and slight apiculum), glabrous.</text>
      <biological_entity id="o13132" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="white" value_original="white" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="ovate-oblong" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13133" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s6" to="5" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="7" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <relation from="o13132" id="r1195" name="in" negation="false" src="d0_s6" to="o13133" />
    </statement>
    <statement id="d0_s7">
      <text>Pistillate florets (39–) 44–69.</text>
      <biological_entity id="o13134" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="39" name="atypical_quantity" src="d0_s7" to="44" to_inclusive="false" />
        <character char_type="range_value" from="44" name="quantity" src="d0_s7" to="69" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Bisexual florets 5–8 (–11).</text>
      <biological_entity id="o13135" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="11" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae ridged, smooth or weakly papillate-roughened.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 14.</text>
      <biological_entity id="o13136" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="weakly" name="relief" src="d0_s9" value="papillate-roughened" value_original="papillate-roughened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13137" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open slopes and ridges, streambeds, road banks and other disturbed sites, sandy flats, dunes, coastal sage scrub, chaparral, yellow pine, foothill pine, blue oak woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="streambeds" />
        <character name="habitat" value="road banks" />
        <character name="habitat" value="other disturbed sites" />
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="coastal sage scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="yellow pine" />
        <character name="habitat" value="foothill pine" />
        <character name="habitat" value="blue oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1–)50–800(–2000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="50" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="1" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Fragrant rabbit-tobacco</other_name>
  <discussion>Pseudognaphalium beneolens differs from P. thermale in its leaves linear throughout, heads usually in elongate, paniculiform arrays, larger heads (greater numbers of phyllaries in greater numbers of series) with phyllaries more opaque and duller, and greater numbers of bisexual florets. The cauline leaves of P. beneolens tend to become curving-coiling. In areas of sympatry, habitats of P. beneolens are characteristically at lower elevations than those of P. thermale.</discussion>
  
</bio:treatment>