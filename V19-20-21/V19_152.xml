<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="treatment_page">156</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">scariosum</taxon_name>
    <taxon_name authority="(Rydberg) D. J. Keil" date="2004" rank="variety">coloradense</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 215. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species scariosum;variety coloradense</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068210</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">coloradensis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>32: 132. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carduus;species coloradensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Rydberg) Cockerell ex Daniels" date="unknown" rank="species">coloradense</taxon_name>
    <taxon_hierarchy>genus Cirsium;species coloradense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Rydberg) K. Schumann" date="unknown" rank="species">erosum</taxon_name>
    <taxon_hierarchy>genus Cirsium;species erosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Congdon) Petrak" date="unknown" rank="species">tioganum</taxon_name>
    <taxon_name authority="(Rydberg) Dorn" date="unknown" rank="variety">coloradense</taxon_name>
    <taxon_hierarchy>genus Cirsium;species tioganum;variety coloradense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually erect, caulescent (rarely acaulescent), 20–150 cm.</text>
      <biological_entity id="o20621" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually simple, proximally unbranched, very leafy, ± villous with septate trichomes and/or thinly arachnoid tomentose, often glabrate.</text>
      <biological_entity id="o20622" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character constraint="with trichomes" constraintid="o20623" is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o20623" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="septate" value_original="septate" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s1" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades oblanceolate or narrowly elliptic, pinnately lobed, longer spines ± stout, usually 1 cm or shorter, abaxial faces glabrous to thinly gray-tomentose, adaxial glabrous;</text>
      <biological_entity id="o20624" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20625" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="longer" id="o20626" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character modifier="usually" name="some_measurement" src="d0_s2" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20627" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="thinly gray-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20628" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal narrow, firm, green throughout or unpigmented proximally.</text>
      <biological_entity id="o20629" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o20630" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="throughout; throughout; proximally" name="coloration" src="d0_s3" value="green throughout or unpigmented" value_original="green throughout or unpigmented" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 3–10+, ± sessile or pedunculate, usually in spiciform or racemiform arrays, subtended by ± reduced bractlike distal leaves that often do not overtop the heads.</text>
      <biological_entity id="o20631" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="10" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o20632" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20633" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <biological_entity id="o20634" name="head" name_original="heads" src="d0_s4" type="structure" />
      <relation from="o20631" id="r1861" modifier="usually" name="in" negation="false" src="d0_s4" to="o20632" />
      <relation from="o20631" id="r1862" name="subtended by" negation="false" src="d0_s4" to="o20633" />
      <relation from="o20633" id="r1863" name="overtop the" negation="false" src="d0_s4" to="o20634" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0–18 cm.</text>
      <biological_entity id="o20635" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 2–3 cm.</text>
      <biological_entity id="o20636" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries: outer and mid lanceolate to narrowly ovate, spines slender to stout, 1–5 mm;</text>
      <biological_entity id="o20637" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity constraint="outer and mid" id="o20638" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="narrowly ovate" />
      </biological_entity>
      <biological_entity id="o20639" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s7" to="stout" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apices of inner acuminate and entire or serrate, or abruptly expanded into scarious, erose-toothed appendages.</text>
      <biological_entity id="o20640" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o20641" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="serrate" value_original="serrate" />
        <character constraint="into appendages" constraintid="o20643" is_modifier="false" modifier="abruptly" name="size" src="d0_s8" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20642" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o20643" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="true" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="shape" src="d0_s8" value="erose-toothed" value_original="erose-toothed" />
      </biological_entity>
      <relation from="o20641" id="r1864" name="part_of" negation="false" src="d0_s8" to="o20642" />
    </statement>
    <statement id="d0_s9">
      <text>Corollas white (rarely purple), 22–29 mm, tubes 11–16 mm, throats 4–5 mm, lobes 6–9.5 mm;</text>
      <biological_entity id="o20644" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s9" to="29" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20645" name="tube" name_original="tubes" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20646" name="throat" name_original="throats" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20647" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style tips 4–6 mm.</text>
      <biological_entity constraint="style" id="o20648" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 4–6 mm;</text>
      <biological_entity id="o20649" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi 18–25 mm. 2n = 34, 36?</text>
      <biological_entity constraint="2n" id="o20651" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="34" value_original="34" />
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>(as C. foliosum).</text>
      <biological_entity id="o20650" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s12" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, forests, meadows, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54b.</number>
  <other_name type="common_name">Colorado thistle</other_name>
  <discussion>Variety coloradense is common in the mountains of southern and central Colorado with outlying populations in northern New Mexico, northeastern Arizona, central Utah, and southeastern Wyoming. In Colorado it is largely allopatric with the usually acaulescent var. americanum. Some plants from the White Mountains and San Francisco Peaks of Arizona and from Gunnison County, Colorado, approach var. thorneae in having deeply divided, extremely spiny distal leaves that overtop the heads. Putative hybrids between var. coloradense and Cirsium grahamii have been documented in Apache County, Arizona, and between var. coloradense and C. undulatum in Las Animas County, Colorado.</discussion>
  
</bio:treatment>