<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="treatment_page">502</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="A. Gray" date="1882" rank="species">lemmonii</taxon_name>
    <taxon_name authority="(B. L. Robinson) B. L. Turner" date="1990" rank="variety">conduplicata</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>68: 163. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species lemmonii;variety conduplicata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068113</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brickellia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">betonicifolia</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">conduplicata</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>43: 37. 1907 (as betonicaefolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Brickellia;species betonicifolia;variety conduplicata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brickellia</taxon_name>
    <taxon_name authority="(B. L. Robinson) B. L. Robinson" date="unknown" rank="species">conduplicata</taxon_name>
    <taxon_hierarchy>genus Brickellia;species conduplicata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brickellia</taxon_name>
    <taxon_name authority="Flyr" date="unknown" rank="species">viejensis</taxon_name>
    <taxon_hierarchy>genus Brickellia;species viejensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades broadly ovate to subdeltate, 20–60 × 7–30 mm, lengths 1–2 times widths.</text>
      <biological_entity id="o9383" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s0" to="subdeltate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s0" to="60" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s0" to="30" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s0" value="1-2" value_original="1-2" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 2–22 mm.</text>
      <biological_entity id="o9384" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries: apices (inner) mostly obtuse to nearly rounded.</text>
      <biological_entity id="o9385" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure" />
      <biological_entity id="o9386" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly obtuse" name="shape" src="d0_s2" to="nearly rounded" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Igneous soils, montane and canyon slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="igneous soils" />
        <character name="habitat" value="montane" />
        <character name="habitat" value="canyon slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22b.</number>
  
</bio:treatment>