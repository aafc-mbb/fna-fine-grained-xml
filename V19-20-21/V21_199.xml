<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">berlandiera</taxon_name>
    <taxon_name authority="Small" date="1903" rank="species">×humilis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1246, 1340. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus berlandiera;species ×humilis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068088</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 cm.</text>
      <biological_entity id="o19620" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (erect) usually branched.</text>
      <biological_entity id="o19621" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves crowded near stem-bases;</text>
      <biological_entity id="o19623" name="stem-base" name_original="stem-bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o19622" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="near stem-bases" constraintid="o19623" is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades usually lyrate, sometimes pinnatifid, slightly chartaceous, ultimate margins entire, faces sparsely hirsute (at least adaxial).</text>
      <biological_entity id="o19624" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19625" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19626" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o19627" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o19628" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o19627" id="r1336" name="in" negation="false" src="d0_s5" to="o19628" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles hairy (hairs relatively fine and usually mixed with shorter, stouter hairs).</text>
      <biological_entity id="o19629" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 16–22 mm diam.</text>
      <biological_entity id="o19630" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="diameter" src="d0_s7" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray corollas yellow to orange-yellow, abaxial veins green, laminae 12–17 × 5.5–7.8 mm.</text>
      <biological_entity constraint="ray" id="o19631" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="orange-yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19632" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o19633" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s8" to="7.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas red to maroon.</text>
      <biological_entity constraint="disc" id="o19634" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s9" to="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (obovate) 5–6 × 3–4.5 mm. 2n = 30.</text>
      <biological_entity id="o19635" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19636" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy pine flats, openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy pine flats" />
        <character name="habitat" value="openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Berlandiera ×humilis is a hybrid of B. pumila and B. subacaulis. Specimens from Alabama, Georgia, and South Carolina have some leaf-blade bases with 1+ lobes, evidence of introgression with B. pumila.</discussion>
  
</bio:treatment>