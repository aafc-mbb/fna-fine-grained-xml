<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">556</other_info_on_meta>
    <other_info_on_meta type="illustration_page">556</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="(Kitamura) Kitamura" date="1978" rank="genus">nipponanthemum</taxon_name>
    <taxon_name authority="(Franchet ex Maximowicz) Kitamura" date="1978" rank="species">nipponicum</taxon_name>
    <place_of_publication>
      <publication_title>Acta Phytotax. Geobot.</publication_title>
      <place_in_publication>29: 169. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus nipponanthemum;species nipponicum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067204</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucanthemum</taxon_name>
    <taxon_name authority="Franchet ex Maximowicz" date="unknown" rank="species">nipponicum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Acad. Imp. Sci. Saint-Pétersbourg</publication_title>
      <place_in_publication>17: 420. 1872</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leucanthemum;species nipponicum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysanthemum</taxon_name>
    <taxon_name authority="(Franchet ex Maximowicz) Sprenger" date="unknown" rank="species">nipponicum</taxon_name>
    <taxon_hierarchy>genus Chrysanthemum;species nipponicum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 20–75 (–90) × 12–15 (–20) mm, ultimate margins distally dentate or entire.</text>
      <biological_entity id="o6032" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="90" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s0" to="75" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s0" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s0" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o6033" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s0" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ray laminae 20–30 mm.</text>
      <biological_entity constraint="ray" id="o6034" name="lamina" name_original="laminae" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cypselae 3–4 mm. 2n = 18.</text>
      <biological_entity id="o6035" name="cypsela" name_original="cypselae" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6036" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.J., N.Y.; Asia (Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Nippon daisy</other_name>
  
</bio:treatment>