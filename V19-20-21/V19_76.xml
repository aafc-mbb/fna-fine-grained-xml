<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="treatment_page">121</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(L. F. Henderson) J. T. Howell" date="1959" rank="species">ciliolatum</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>9: 9. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species ciliolatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066362</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Nuttall) Sprengel" date="unknown" rank="species">undulatum</taxon_name>
    <taxon_name authority="L. F. Henderson" date="unknown" rank="variety">ciliolatum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>27: 348. 1900 (as Circium)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cirsium;species undulatum;variety ciliolatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Petrak" date="unknown" rank="species">howellii</taxon_name>
    <taxon_hierarchy>genus Cirsium;species howellii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 60–200 cm, arachnoid, tomentose;</text>
      <biological_entity id="o78" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>runner roots producing adventitious-buds.</text>
      <biological_entity constraint="runner" id="o79" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o80" name="adventitiou-bud" name_original="adventitious-buds" src="d0_s1" type="structure" />
      <relation from="o79" id="r7" name="producing" negation="false" src="d0_s1" to="o80" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, erect, thinly arachnoid to densely white-tomentose;</text>
      <biological_entity id="o81" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="thinly arachnoid" name="pubescence" src="d0_s2" to="densely white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches 0–few, ascending.</text>
      <biological_entity id="o82" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="few" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades oblongelliptic, 10–30 × 3–12 cm, margins finely spiny-toothed and otherwise undivided to coarsely dentate, shallowly lobed, or deeply laciniate-pinnatifid, lobes broadly triangular to linear-lanceolate, main spines 1–6 mm, abaxial white-tomentose, adaxial faces ± green, thinly to densely arachnoid-tomentose;</text>
      <biological_entity id="o83" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o84" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o85" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s4" value="spiny-toothed" value_original="spiny-toothed" />
        <character is_modifier="false" modifier="otherwise" name="architecture_or_shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undivided to coarsely" value_original="undivided to coarsely" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="laciniate-pinnatifid" value_original="laciniate-pinnatifid" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="laciniate-pinnatifid" value_original="laciniate-pinnatifid" />
      </biological_entity>
      <biological_entity id="o86" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly triangular" name="shape" src="d0_s4" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity constraint="main" id="o87" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o88" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o89" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="thinly to densely" name="pubescence" src="d0_s4" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal often present at flowering, winged petiolate;</text>
      <biological_entity id="o90" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o91" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline well distributed, proximally winged-petiolate, distally sessile, gradually reduced, bases auriculate-clasping or short-decurrent as spiny wings;</text>
      <biological_entity id="o92" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o93" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o94" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="auriculate-clasping" value_original="auriculate-clasping" />
        <character constraint="as wings" constraintid="o95" is_modifier="false" name="architecture" src="d0_s6" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
      <biological_entity id="o95" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cauline few, reduced to linear bracts.</text>
      <biological_entity id="o96" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal cauline" id="o97" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o98" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads borne singly and terminal on main-stem and branches, or few in corymbiform arrays.</text>
      <biological_entity id="o99" name="head" name_original="heads" src="d0_s8" type="structure">
        <character constraint="in arrays" constraintid="o102" is_modifier="false" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o100" name="main-stem" name_original="main-stem" src="d0_s8" type="structure" constraint="terminal">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o101" name="branch" name_original="branches" src="d0_s8" type="structure" constraint="terminal">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o102" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o99" id="r8" name="borne" negation="false" src="d0_s8" to="o100" />
      <relation from="o99" id="r9" name="borne" negation="false" src="d0_s8" to="o101" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–15 cm.</text>
      <biological_entity id="o103" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres ovoid to hemispheric, 1.5–2.3 × 1.5–3 cm, thinly arachnoid, ± glabrate.</text>
      <biological_entity id="o104" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="hemispheric" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s10" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s10" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 5–7 series, strongly imbricate, greenish with subapical darker central zone, lanceolate (outer) to linear (inner), abaxial faces with prominent glutinous ridge;</text>
      <biological_entity id="o105" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character constraint="with subapical central zone" constraintid="o107" is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s11" to="linear" />
      </biological_entity>
      <biological_entity id="o106" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
      <biological_entity constraint="subapical central" id="o107" name="zone" name_original="zone" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o108" name="face" name_original="faces" src="d0_s11" type="structure" />
      <biological_entity id="o109" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o105" id="r10" name="in" negation="false" src="d0_s11" to="o106" />
      <relation from="o108" id="r11" name="with" negation="false" src="d0_s11" to="o109" />
    </statement>
    <statement id="d0_s12">
      <text>outer and middle bodies appressed, entire, apices entire or finely serrulate, spines ascending, slender, 1–3 mm;</text>
      <biological_entity constraint="outer and middle" id="o110" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o111" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s12" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o112" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner spreading to erect, narrow, ± flattened, finely serrulate, ± scabrous.</text>
      <biological_entity id="o113" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="body" constraint_original="body; body">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s13" to="erect" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s13" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o114" name="body" name_original="bodies" src="d0_s13" type="structure" />
      <relation from="o113" id="r12" name="part_of" negation="false" src="d0_s13" to="o114" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas dull white to lavender, 15–25 mm, tubes 7–11 mm, throats 5–7 mm, lobes 5–7 mm;</text>
      <biological_entity id="o115" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dull" value_original="dull" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="lavender" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o116" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o117" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o118" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 5–7 mm.</text>
      <biological_entity constraint="style" id="o119" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae brown, 3.5–7 mm, apical collars tan, 0.1–0.2 mm;</text>
      <biological_entity id="o120" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o121" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="tan" value_original="tan" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi white or tawny, 15–20 mm.</text>
      <biological_entity id="o122" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s17" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy areas, open woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" />
        <character name="habitat" value="open woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Ashland thistle</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Cirsium ciliolatum is restricted to a few sites in and around the Klamath Range of Jackson County, Oregon, and Siskiyou County, California. It is listed by the state of California as endangered. It is closely related to C. undulatum and perhaps should be treated as a variety of that species. Pending a comprehensive study of the variation within C. undulatum, I have maintained C. ciliolatum as a distinct species.</discussion>
  
</bio:treatment>