<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="treatment_page">355</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">stephanomeria</taxon_name>
    <taxon_name authority="Gottlieb" date="1978" rank="species">malheurensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>25: 44, fig. 1. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus stephanomeria;species malheurensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067604</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–60 cm.</text>
      <biological_entity id="o6847" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems single, branches ascending, glabrous.</text>
      <biological_entity id="o6848" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o6849" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves withered at flowering;</text>
      <biological_entity id="o6850" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" name="life_cycle" src="d0_s2" value="withered" value_original="withered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades oblanceolate to spatulate, 5–7 cm, margins entire to pinnately lobed (faces glabrous);</text>
      <biological_entity constraint="basal" id="o6851" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="spatulate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6852" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s3" to="pinnately lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline much reduced, bractlike.</text>
      <biological_entity constraint="cauline" id="o6853" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly along branches.</text>
      <biological_entity id="o6854" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o6855" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o6854" id="r647" name="borne" negation="false" src="d0_s5" to="o6855" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–10 mm (glabrous).Calyculi of appressed bractlets.</text>
      <biological_entity id="o6856" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6857" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o6858" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o6857" id="r648" name="consists_of" negation="false" src="d0_s6" to="o6858" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres 8–9.5 mm.</text>
      <biological_entity id="o6859" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 5–6 (ligules usually pink, rarely white or orange-yellow).</text>
      <biological_entity id="o6860" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae tan to light-brown, 3.3–3.8 mm, faces moderately tuberculate, grooved;</text>
      <biological_entity id="o6861" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s9" to="light-brown" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s9" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6862" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="moderately" name="relief" src="d0_s9" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 9–12 (–15), light tan bristles (connate in groups of 2–4, bristles and/or bases persistent), plumose on distal 50–60%.</text>
      <biological_entity id="o6864" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="15" />
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s10" to="12" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="light tan" value_original="light tan" />
      </biological_entity>
      <biological_entity id="o6865" name="distal" name_original="distal" src="d0_s10" type="structure" />
      <relation from="o6863" id="r649" name="consist_of" negation="false" src="d0_s10" to="o6864" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 16.</text>
      <biological_entity id="o6863" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character constraint="on distal" constraintid="o6865" is_modifier="false" name="shape" notes="" src="d0_s10" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6866" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soils derived from volcanic tuff, high desert. of conservation concern</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soils" />
        <character name="habitat" value="volcanic tuff" />
        <character name="habitat" value="high desert" />
        <character name="habitat" value="conservation concern" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Malheur wirelettuce</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Stephanomeria malheurensis has been examined in a series of studies (L. D. Gottlieb 1973b, 1977, 1978b, 1979, 1991; Gottlieb and J. P. Bennett 1983; S. Brauner and Gottlieb 1987, 1989; B. A. Bohm and Gottlieb 1989) because it is one of the very few examples of the recent, natural origin of a diploid, annual plant species. At the type locality, it grows with a population of S. exigua subsp. coronaria that is thought to be its progenitor.</discussion>
  <discussion>Stephanomeria malheurensis is known from a single locality in Harney County, Oregon, growing in soil derived from volcanic tuff in the high desert of eastern Oregon. It is a federally listed rare and endangered species, and is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>