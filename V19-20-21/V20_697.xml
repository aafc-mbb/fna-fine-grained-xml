<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">261</other_info_on_meta>
    <other_info_on_meta type="mention_page">316</other_info_on_meta>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Greene" date="1888" rank="species">petrophilus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 218. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species petrophilus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066652</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–20 (–30) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices woody, branches rhizomelike.</text>
      <biological_entity id="o15002" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o15003" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o15004" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomelike" value_original="rhizomelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually decumbent-ascending, sometimes nearly erect, sparsely to densely white-villous (hairs loose, thin, often crinkly) or sparsely hispido-pilose (hairs usually stiff, spreading, sometimes antrorsely ascending or appressed; var. viscidulus), usually densely minutely glandular at least distally, sometimes eglandular or obscurely glandular (var. viscidulus).</text>
      <biological_entity id="o15005" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="decumbent-ascending" value_original="decumbent-ascending" />
        <character is_modifier="false" modifier="sometimes nearly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely to densely; densely" name="pubescence" src="d0_s2" value="white-villous" value_original="white-villous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hispido-pilose" value_original="hispido-pilose" />
        <character is_modifier="false" modifier="usually densely minutely; at-least distally; distally" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline;</text>
      <biological_entity id="o15006" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades narrowly oblong to oblanceolate, 10–25 × 1–5 mm, usually even-sized, margins entire, ciliate, faces sparsely to densely villous to hirsuto-villous, usually densely minutely glandular, sometimes eglandular or obscurely glandular (var. viscidulus).</text>
      <biological_entity id="o15007" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s4" value="equal-sized" value_original="even-sized" />
      </biological_entity>
      <biological_entity id="o15008" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15009" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="villous" modifier="densely" name="pubescence" src="d0_s4" to="hirsuto-villous" />
        <character is_modifier="false" modifier="usually densely minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (discoid) (1–) 2–5 (–10) in corymbiform arrays.</text>
      <biological_entity id="o15010" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="2" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="10" />
        <character char_type="range_value" constraint="in arrays" constraintid="o15011" from="2" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o15011" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5.5–7 (–8) × 8–12 mm.</text>
      <biological_entity id="o15012" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–5 series (sometimes purplish apically), usually glabrous, sometimes with spreading hairs, densely glandular.</text>
      <biological_entity id="o15013" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" notes="" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o15014" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o15015" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o15013" id="r1369" name="in" negation="false" src="d0_s7" to="o15014" />
      <relation from="o15013" id="r1370" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o15015" />
    </statement>
    <statement id="d0_s8">
      <text>Ray (pistillate) florets 0.</text>
      <biological_entity constraint="ray" id="o15016" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 4–6 mm (throats slightly indurate, not inflated).</text>
      <biological_entity constraint="disc" id="o15017" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (2–) 2.5–3 mm, 2-nerved, glabrous;</text>
      <biological_entity id="o15018" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-nerved" value_original="2-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: outer of setae, inner of 22–30 (–35) bristles.</text>
      <biological_entity id="o15019" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o15020" name="seta" name_original="setae" src="d0_s11" type="structure" />
      <biological_entity id="o15021" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s11" to="35" />
        <character char_type="range_value" from="22" is_modifier="true" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
      <relation from="o15019" id="r1371" name="outer of" negation="false" src="d0_s11" to="o15020" />
      <relation from="o15019" id="r1372" name="inner of" negation="false" src="d0_s11" to="o15021" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>102.</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and leaves eglandular or obscurely glandular; nonglandular hairs mostly stiff, straight or curved</description>
      <determination>102c Erigeron petrophilus var. viscidulus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and leaves densely glandular; nonglandular hairs usually loose, often crinkly</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries with distinctly expanded, purplish area at apices</description>
      <determination>102a Erigeron petrophilus var. petrophilus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries with apices not purplish and differently colored from proximal portion</description>
      <determination>102b Erigeron petrophilus var. sierrensis</determination>
    </key_statement>
  </key>
</bio:treatment>