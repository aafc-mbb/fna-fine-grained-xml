<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="treatment_page">342</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Greene" date="1891" rank="species">multiceps</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 167. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species multiceps</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066637</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, short-lived, 12–20 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices simple or branched.</text>
      <biological_entity id="o23356" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o23357" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (single or multiple from bases) decumbent-ascending (branched from midstems), strigose (hairs fine, ascending-appressed), eglandular.</text>
      <biological_entity id="o23358" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent-ascending" value_original="decumbent-ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal (persistent);</text>
      <biological_entity id="o23359" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23360" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades spatulate to oblanceolate, 10–30 × 2–6 mm, cauline reduced distally, linear, margins entire, strigose, eglandular.</text>
      <biological_entity id="o23361" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o23362" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o23363" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–3 on proximal branches).</text>
      <biological_entity id="o23364" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3.5–4 × 6–7 mm.</text>
      <biological_entity id="o23365" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series, sparsely hispid, minutely glandular.</text>
      <biological_entity id="o23366" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s7" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o23367" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o23366" id="r2154" name="in" negation="false" src="d0_s7" to="o23367" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 75–125;</text>
      <biological_entity id="o23368" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="75" name="quantity" src="d0_s8" to="125" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white to purplish 5–8 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o23369" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="purplish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23370" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 1.8–2.2 mm (throats white-indurate and somewhat inflated).</text>
      <biological_entity constraint="disc" id="o23371" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 0.7–0.9 mm, 2-nerved, sparsely strigose;</text>
      <biological_entity id="o23372" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 5–8 bristles.</text>
      <biological_entity id="o23373" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o23374" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o23375" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
      <relation from="o23373" id="r2155" name="outer of" negation="false" src="d0_s12" to="o23374" />
      <relation from="o23373" id="r2156" name="inner of" negation="false" src="d0_s12" to="o23375" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, open meadows and meadow edges near mixed conifer and aspen stands, sometimes in disturbed sites such as logging roads, sandy creek banks, annual drainages, and washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="open meadows" />
        <character name="habitat" value="meadow edges" constraint="near mixed conifer and aspen" />
        <character name="habitat" value="mixed conifer" />
        <character name="habitat" value="aspen" />
        <character name="habitat" value="disturbed sites" modifier="stands sometimes in" />
        <character name="habitat" value="logging roads" />
        <character name="habitat" value="sandy creek banks" />
        <character name="habitat" value="annual drainages" />
        <character name="habitat" value="washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>163.</number>
  <other_name type="common_name">Kern River fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Although Erigeron multiceps has been considered to be indistinct or doubtfully distinct from E. divergens (see G. L. Nesom 1993f), California botanists with field experience note that the two taxa can be distinguished when occuring in close proximity. Plants of the type collection and some others have thick taproots and thickened caudex branches, unlike closely related species. Plants from Clark County, Nevada (Red Rock Canyon Recreation Area, Leary 4031, UNLV) have taproots with simple caudices, decumbent-ascending, strigose stems, and persistent basal leaves; they are technically, at least, identified as E. multiceps. The disjunct Mexican plants are closely similar to those in California.</discussion>
  
</bio:treatment>