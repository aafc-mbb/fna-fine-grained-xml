<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="treatment_page">189</other_info_on_meta>
    <other_info_on_meta type="illustration_page">187</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">chrysothamnus</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1895" rank="species">greenei</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>3: 94. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysothamnus;species greenei</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066349</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">greenei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 75. 1876 (as Bigelovia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bigelowia;species greenei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–50 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>with woody, moderately branched caudices, bark light gray, flaky to fibrous with age.</text>
      <biological_entity id="o2995" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o2996" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="true" modifier="moderately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o2997" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="light gray" value_original="light gray" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="flaky" value_original="flaky" />
        <character constraint="with age" constraintid="o2998" is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o2998" name="age" name_original="age" src="d0_s1" type="structure" />
      <relation from="o2995" id="r276" name="with" negation="false" src="d0_s1" to="o2996" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending, green becoming tan, glabrous, resinous.</text>
      <biological_entity id="o2999" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="green becoming tan" value_original="green becoming tan" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ascending to spreading;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o3000" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades with faint midnerves, linear-filiform, 10–40 × 0.5–2 mm, flat or sulcate, sometimes twisted, margins glabrous or hirtellous, apices acute to apiculate, faces glabrous.</text>
      <biological_entity id="o3001" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="linear-filiform" value_original="linear-filiform" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o3002" name="midnerve" name_original="midnerves" src="d0_s5" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s5" value="faint" value_original="faint" />
      </biological_entity>
      <biological_entity id="o3003" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o3004" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="apiculate" />
      </biological_entity>
      <biological_entity id="o3005" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o3001" id="r277" name="with" negation="false" src="d0_s5" to="o3002" />
    </statement>
    <statement id="d0_s6">
      <text>Heads in densely cymiform arrays (4 cm wide), distal leaves reaching into but not overtopping arrays.</text>
      <biological_entity id="o3006" name="head" name_original="heads" src="d0_s6" type="structure" />
      <biological_entity id="o3007" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="densely" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3008" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o3009" name="array" name_original="arrays" src="d0_s6" type="structure" />
      <relation from="o3006" id="r278" name="in" negation="false" src="d0_s6" to="o3007" />
      <relation from="o3008" id="r279" name="reaching into but" negation="false" src="d0_s6" to="o3009" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to turbinate, 5–8 × 1.5–2.5 mm.</text>
      <biological_entity id="o3010" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="turbinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 15–20 in 3–4 series, often in vertical ranks or spirals, mostly tan, ± greenish apically, midveins rarely visible, ovate or oblong to elliptic, 1.5–5 × 0.8–1.4 mm, unequal, mostly chartaceous, sometimes ± keeled, apices acuminate to cuspidate (cusp often recurved or falcate), faces glabrous or with a few long, crooked hairs proximally and near margins, resinous.</text>
      <biological_entity id="o3011" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3012" from="15" name="quantity" src="d0_s8" to="20" />
        <character is_modifier="false" modifier="mostly" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="more or less; apically" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o3012" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o3013" name="rank" name_original="ranks" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o3014" name="spiral" name_original="spirals" src="d0_s8" type="structure" />
      <biological_entity id="o3015" name="midvein" name_original="midveins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="rarely" name="prominence" src="d0_s8" value="visible" value_original="visible" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o3016" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s8" to="cuspidate" />
      </biological_entity>
      <biological_entity id="o3017" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with a few long , crooked hairs" />
        <character is_modifier="false" name="coating" notes="" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o3018" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="true" name="course_or_orientation" src="d0_s8" value="crooked" value_original="crooked" />
      </biological_entity>
      <biological_entity id="o3019" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <relation from="o3011" id="r280" modifier="often" name="in" negation="false" src="d0_s8" to="o3013" />
      <relation from="o3011" id="r281" modifier="often" name="in" negation="false" src="d0_s8" to="o3014" />
      <relation from="o3017" id="r282" name="with" negation="false" src="d0_s8" to="o3018" />
      <relation from="o3017" id="r283" name="near" negation="false" src="d0_s8" to="o3019" />
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 4–5;</text>
      <biological_entity id="o3020" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3.7–5.5 mm, lobes 0.8–1.5 mm;</text>
      <biological_entity id="o3021" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3022" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style-branches 2–2.4 mm (exserted beyond spreading corolla lobes), appendages 0.5–0.8 mm (shorter than stigmatic portion).</text>
      <biological_entity id="o3023" name="branch-style" name_original="style-branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s11" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3024" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae reddish-brown, turbinate, 3–4 mm, faces densely hairy;</text>
      <biological_entity id="o3025" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3026" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi tan, 3.7–5 mm. 2n = 18.</text>
      <biological_entity id="o3027" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3028" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy washes, dry open places in desert</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy washes" />
        <character name="habitat" value="dry open places" constraint="in desert" />
        <character name="habitat" value="desert" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Greene’s rabbitbrush</other_name>
  
</bio:treatment>