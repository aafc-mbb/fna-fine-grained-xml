<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
    <other_info_on_meta type="illustration_page">388</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">dieteria</taxon_name>
    <taxon_name authority="(A. Gray) D. R. Morgan &amp; R. L. Hartman" date="2003" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 1394. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus dieteria;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066480</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 97. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species bigelovii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">bigelovii</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species bigelovii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or short-lived perennials.</text>
      <biological_entity id="o17325" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems, branches, and peduncles sometimes puberulent or canescent, stipitate-glandular.</text>
      <biological_entity id="o17327" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o17328" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o17329" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades lanceolate to oblanceolate (linear to linear-oblanceolate in var. mucronata), mid 20–80 × (2–) 5–15 mm, margins entire to irregularly dentate or serrate, faces glabrous or puberulent, often sparsely stipitate-glandular;</text>
      <biological_entity id="o17330" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="oblanceolate" />
        <character is_modifier="false" name="position" src="d0_s2" value="mid" value_original="mid" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="80" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17331" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s2" to="irregularly dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o17332" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="often sparsely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal bases usually cordate to auriculate, clasping.</text>
      <biological_entity constraint="distal" id="o17333" name="base" name_original="bases" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually cordate" name="shape" src="d0_s3" to="auriculate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres broadly turbinate to hemispheric.</text>
      <biological_entity id="o17334" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly turbinate" name="shape" src="d0_s4" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries in 5–10 series, spreading to reflexed, apices long-acuminate, 2–6 mm, herbaceous (acute to acuminate, 1–3 mm in var. commixta), faces stipitate-glandular.</text>
      <biological_entity id="o17335" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s5" to="reflexed" />
      </biological_entity>
      <biological_entity id="o17336" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o17337" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="long-acuminate" value_original="long-acuminate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o17338" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o17335" id="r1609" name="in" negation="false" src="d0_s5" to="o17336" />
    </statement>
    <statement id="d0_s6">
      <text>Receptacles 4–9 mm diam.</text>
      <biological_entity id="o17339" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets pistillate, fertile;</text>
      <biological_entity id="o17340" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae blue to purple, 10–25 × 1–2 mm.</text>
      <biological_entity id="o17341" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="purple" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 5–7 (–8) mm.</text>
      <biological_entity constraint="disc" id="o17342" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae sparsely appressed-hairy.</text>
      <biological_entity id="o17343" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres hemispheric, widths 2–3 times heights; phyllaries 90–100, 0.5–1 mm wide (at midpoint), apices long-acuminate, 2–5 mm</description>
      <determination>2c Dieteria bigelovii var. mucronata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres broadly turbinate to hemispheric, 1–2(–3) times heights; phyllaries 25–100, 1–2 mm wide (at midpoint), apices acute to long-acuminate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries 50–100, apices long-acuminate, 3–6 mm, ray florets 30–60</description>
      <determination>2a Dieteria bigelovii var. bigelovii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries 25–50, apices acute to acuminate, 2–4 mm; ray florets 12–30</description>
      <determination>2b Dieteria bigelovii var. commixta</determination>
    </key_statement>
  </key>
</bio:treatment>