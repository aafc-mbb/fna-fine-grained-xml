<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Dale E. Johnson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">363</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="treatment_page">351</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg in N. L. Britton et al." date="1915" rank="genus">PSEUDOBAHIA</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 83. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus PSEUDOBAHIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pseudes, false, and generic name Bahia</other_info_on_name>
    <other_info_on_name type="fna_id">126967</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="de Candolle" date="unknown" rank="genus">Monolopia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="section">Pseudobahia</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>1: 383. 1876 (as Pseudo-Bahia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Monolopia;section Pseudobahia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 30 (–70) cm.</text>
      <biological_entity id="o14694" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually branched.</text>
      <biological_entity id="o14695" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>usually petiolate;</text>
      <biological_entity id="o14696" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually 1–2-pinnately lobed, sometimes 3-lobed or entire, faces sparsely to moderately woolly.</text>
      <biological_entity id="o14697" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually 1-2-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14698" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s5" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o14699" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, 5–9 mm diam.</text>
      <biological_entity id="o14700" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, (3–) 8 in 1 series (± erect in fruit, connate at bases or to 1/2 their lengths, elliptic, lanceolate, or oblanceolate, equal, margins somewhat hyaline, apices acute to acuminate, abaxial faces usually woolly).</text>
      <biological_entity id="o14701" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character constraint="in series" constraintid="o14702" name="quantity" src="d0_s8" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o14702" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic to hemispheric, pitted or smooth, glabrous, epaleate.</text>
      <biological_entity id="o14703" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="hemispheric" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets (3–) 8, pistillate, fertile;</text>
      <biological_entity id="o14704" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow (with rings of hairs at bases of laminae).</text>
      <biological_entity id="o14705" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 8–25+, bisexual, fertile;</text>
      <biological_entity id="o14706" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="25" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow (with rings of hairs at bases of limbs), tubes shorter than funnelform or cylindric throats, lobes 5, ± deltate (anther appendages deltate, widest at bases, glandular; style-branch appendages deltate).</text>
      <biological_entity id="o14707" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14708" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than funnelform or cylindric throats" constraintid="o14709" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14709" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o14710" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae ± obcompressed, 3–4-angled and obpyramidal to clavate, hairy or glabrate;</text>
      <biological_entity id="o14711" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-4-angled" value_original="3-4-angled" />
        <character char_type="range_value" from="obpyramidal" name="shape" src="d0_s14" to="clavate" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, or coroniform (minute scales).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 4.</text>
      <biological_entity id="o14712" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o14713" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>358.</number>
  <other_name type="common_name">Sunburst</other_name>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>B. G. Baldwin and B. L. Wessa (2000) found that Pseudobahia bahiifolia and P. peirsonii nest within a clade of Eriophyllum and Syntrichopappus species; circumscriptions of these genera are likely to change.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves entire or 3-lobed</description>
      <determination>1 Pseudobahia bahiifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves mostly 1–2-pinnately lobed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves mostly 1-pinnately lobed; phyllaries connate to 1/2 their lengths</description>
      <determination>2 Pseudobahia heermannii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves mostly 2-pinnately lobed (except in smaller plants); phyllaries connate at bases</description>
      <determination>3 Pseudobahia peirsonii</determination>
    </key_statement>
  </key>
</bio:treatment>