<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Bogler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Lessing" date="1832" rank="genus">PINAROPAPPUS</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Gen. Compos.,</publication_title>
      <place_in_publication>143. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus PINAROPAPPUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pinaro, dirty, squalid, and pappos, pappus, alluding to color of pappi</other_info_on_name>
    <other_info_on_name type="fna_id">125501</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–40 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted (taproots deep, woody) or rhizomatous.</text>
      <biological_entity id="o4061" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20+, erect or ascending, simple or branched proximally, ± scapiform, glabrous.</text>
      <biological_entity id="o4062" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="scapiform" value_original="scapiform" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o4063" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal blades linear to lanceolate, margins entire, toothed, or pinnately lobed (faces glabrous);</text>
      <biological_entity constraint="basal" id="o4064" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o4065" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline foliaceous or reduced to minute bracts distally.</text>
      <biological_entity constraint="cauline" id="o4066" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character name="architecture" src="d0_s6" value="reduced to minute" value_original="reduced to minute" />
      </biological_entity>
      <biological_entity id="o4067" name="bract" name_original="bracts" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Heads borne singly.</text>
      <biological_entity id="o4068" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles not inflated distally, sometimes bracteate.</text>
      <biological_entity id="o4069" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not; distally" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyculi 0.</text>
      <biological_entity id="o4070" name="calyculus" name_original="calyculi" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres cylindric to campanulate, 3–20 mm diam.</text>
      <biological_entity id="o4071" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries 18–22 in 3–5 series, ovate to lanceolate, unequal, margins scarious, apices acute.</text>
      <biological_entity id="o4072" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4073" from="18" name="quantity" src="d0_s11" to="22" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s11" to="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o4073" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity id="o4074" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o4075" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Receptacles slightly convex, slightly pitted, glabrous, paleate (paleae scarious, acuminate).</text>
      <biological_entity id="o4076" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s12" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Florets (10–) 20–40 (–60);</text>
      <biological_entity id="o4077" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s13" to="20" to_inclusive="false" />
        <character char_type="range_value" from="40" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="60" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas pink, purple, lavender, or nearly white.</text>
      <biological_entity id="o4078" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae golden or yellowish-brown, cylindric or fusiform, tapered to slender beaks, ribs 5–6, rounded, obscure, scabrous or hispidulous;</text>
      <biological_entity id="o4079" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="golden" value_original="golden" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s15" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity constraint="slender" id="o4080" name="beak" name_original="beaks" src="d0_s15" type="structure" />
      <biological_entity id="o4081" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s15" to="6" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 15–60, distinct, tawny or yellowish-brown, unequal, barbellulate bristles in 1 series.</text>
      <biological_entity id="o4082" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="of 15-60" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="size" src="d0_s16" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o4083" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <biological_entity id="o4084" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4083" id="r415" name="in" negation="false" src="d0_s16" to="o4084" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>81.</number>
  <other_name type="common_name">Rocklettuce</other_name>
  <discussion>Species 7–10 (2 in the flora).</discussion>
  <discussion>Plants of Pinaropappus are recognized by the glabrous leaves in dense rosettes, scapiform stems, graduated phyllaries, and pale lavender and whitish corollas. They are commonly found in dry, rocky, limestone habitats; some species are cliff-dwellers.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 3–7 cm (forming dense clumps and mats); involucres cylindric, 8–10 × 3–5 mm; phyllaries purplish, margins white, apices purplish to dull brown (necrotic)</description>
      <determination>1 Pinaropappus parvus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 10–30 cm (forming individual rosettes or clumps); involucres campanulate, 10–15 × 12–20 mm; phyllaries pale green, margins pink, apices dark brown</description>
      <determination>2 Pinaropappus roseus</determination>
    </key_statement>
  </key>
</bio:treatment>