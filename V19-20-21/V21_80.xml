<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="treatment_page">40</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">milleriinae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1829" rank="genus">GUIZOTIA</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 59: 237, 247, 248. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe milleriinae;genus GUIZOTIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Pierre Guizot, 1787–1874, French historian, politician</other_info_on_name>
    <other_info_on_name type="fna_id">114186</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals [perennials, subshrubs, or shrubs], [3–] 20–200 cm.</text>
      <biological_entity id="o5736" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect [creeping], branched.</text>
      <biological_entity id="o5737" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o5739" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite (distal sometimes alternate);</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o5738" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades rhombic to lanceolate or oblanceolate [spatulate], margins entire or serrate, faces glabrous or puberulent to pilose, glanddotted (at least abaxial).</text>
      <biological_entity id="o5740" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s5" to="lanceolate or oblanceolate" />
      </biological_entity>
      <biological_entity id="o5741" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o5742" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s5" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, in corymbiform arrays [borne singly].</text>
      <biological_entity id="o5743" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o5744" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o5743" id="r425" name="in" negation="false" src="d0_s6" to="o5744" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate [hemispheric], [8–] 10–15 [–20] mm diam.</text>
      <biological_entity id="o5745" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 10–13 in 2 series (distinct, outer 5–6+ oblong, herbaceous, inner narrower, more scarious).</text>
      <biological_entity id="o5746" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o5747" from="10" name="quantity" src="d0_s8" to="13" />
      </biological_entity>
      <biological_entity id="o5747" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic to hemispheric, paleate (paleae oblong to lanceolate, plane [cucullate], membranous to scarious).</text>
      <biological_entity id="o5748" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s9" to="hemispheric" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 6–18, pistillate, fertile;</text>
      <biological_entity id="o5749" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="18" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow (hairy at bases of tubes).</text>
      <biological_entity id="o5750" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 100+, bisexual, fertile;</text>
      <biological_entity id="o5751" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s12" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes cylindric (hairy), shorter than campanulate throats, lobes 5, deltate.</text>
      <biological_entity id="o5752" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o5753" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character constraint="than campanulate throats" constraintid="o5754" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5754" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o5755" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae weakly compressed, 3–4-angled, glabrous (shining);</text>
      <biological_entity id="o5756" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-4-angled" value_original="3-4-angled" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 15.</text>
      <biological_entity id="o5757" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o5758" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>259.</number>
  <discussion>Species 6 (1 in the flora).</discussion>
  <discussion>Taxonomic affinities of Guizotia generally have been acknowledged as obscure; the genus has been placed in Coreopsidinae, Melampodiinae, Milleriinae, and Verbesininae.</discussion>
  <references>
    <reference>Baagøe, J. 1974. The genus Guizotia (Compositae). A taxonomic revision. Bot. Tidsskr. 69: 1–39.</reference>
  </references>
  
</bio:treatment>