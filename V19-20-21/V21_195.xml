<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">berlandiera</taxon_name>
    <taxon_name authority="Bentham" date="1839" rank="species">lyrata</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>17. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus berlandiera;species lyrata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066218</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Berlandiera</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">incisa</taxon_name>
    <taxon_hierarchy>genus Berlandiera;species incisa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60 (–120) cm.</text>
      <biological_entity id="o18880" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (erect to decumbent) usually branched.</text>
      <biological_entity id="o18881" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves evenly distributed along stems;</text>
      <biological_entity id="o18883" name="stem" name_original="stems" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o18882" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="along stems" constraintid="o18883" is_modifier="false" modifier="evenly" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate or obovate to spatulate, often lyrate, sometimes ± pinnatifid (terminal lobes usually shorter than pinnatifid portions, crenate to irregularly incised), membranous to slightly chartaceous, ultimate margins crenate or entire, faces ± velvety.</text>
      <biological_entity id="o18884" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="spatulate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s4" to="slightly chartaceous" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o18885" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18886" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="velvety" value_original="velvety" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in corymbiform arrays.</text>
      <biological_entity id="o18887" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o18888" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o18887" id="r1286" name="in" negation="false" src="d0_s5" to="o18888" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles hairy (some hairs reddish, bulbous-based, wartlike, surpassing white, appressed hairs).</text>
      <biological_entity id="o18889" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 13–17 mm diam.</text>
      <biological_entity id="o18890" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="diameter" src="d0_s7" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray corollas deep yellow to orange-yellow, abaxial veins (sometimes whole surfaces) red to maroon, laminae 10–14 × 5.5–8 mm.</text>
      <biological_entity constraint="ray" id="o18891" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="deep" value_original="deep" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="orange-yellow" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18892" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s8" to="maroon" />
      </biological_entity>
      <biological_entity id="o18893" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas red to maroon (rarely yellow).</text>
      <biological_entity constraint="disc" id="o18894" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s9" to="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 4.5–6 × 2.7–3.7 mm. 2n = 30.</text>
      <biological_entity id="o18895" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s10" to="3.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18896" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering nearly year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy loams, rocky, limestone soils, roadsides, grasslands with mesquite, oak, and juniper</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy loams" modifier="dry" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="limestone soils" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="grasslands" constraint="with mesquite , oak , and juniper" />
        <character name="habitat" value="mesquite" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., N.Mex., Okla., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Chocolate flower</other_name>
  <discussion>Berlandiera lyrata is cultivated in Arizona. Exceptional specimens that are scapiform (sometimes monocephalic) with mostly undivided leaves and with wartlike hairs on peduncles occur at higher elevations (south-central New Mexico, trans-Pecos Texas, and Nuevo León). They have yellow disc corollas, as do most collections from Chihuahua, Durango, Nuevo León, and Tamaulipas.</discussion>
  
</bio:treatment>