<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">layia</taxon_name>
    <taxon_name authority="D. D. Keck" date="1958" rank="species">discoidea</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>4: 106. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus layia;species discoidea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067061</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–20 cm (self-incompatible);</text>
    </statement>
    <statement id="d0_s1">
      <text>glandular, not strongly scented.</text>
      <biological_entity id="o2930" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="not strongly" name="odor" src="d0_s1" value="scented" value_original="scented" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not purple-streaked.</text>
      <biological_entity id="o2931" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="purple-streaked" value_original="purple-streaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades oblanceolate or lanceolate to linear, 2–35 mm, margins (basal leaves) lobed.</text>
      <biological_entity id="o2932" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s3" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2933" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres cylindric or narrowly obconic to campanulate, 4–7 × 2–6+ mm.</text>
      <biological_entity id="o2934" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly obconic" name="shape" src="d0_s4" to="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 0 (“involucres” formed of “paleae”).</text>
      <biological_entity id="o2935" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series (interpreted as constituting the involucre).</text>
      <biological_entity id="o2936" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o2937" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o2936" id="r220" name="in" negation="false" src="d0_s6" to="o2937" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 0.</text>
      <biological_entity id="o2938" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 5–35+;</text>
      <biological_entity id="o2939" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="35" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 2.5–4 mm;</text>
      <biological_entity id="o2940" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow to brownish.</text>
      <biological_entity id="o2941" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray cypselae 0.</text>
      <biological_entity constraint="ray" id="o2942" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc pappi of 8–15 whitish to tawny, lanceolate to subulate, ± equal (often apically or marginally notched) scales 0.5–1.5 mm, each ± plumose or villous, not adaxially woolly.</text>
      <biological_entity id="o2944" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s12" to="15" />
        <character char_type="range_value" from="whitish" is_modifier="true" name="coloration" src="d0_s12" to="tawny" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s12" to="subulate" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o2943" id="r221" name="consist_of" negation="false" src="d0_s12" to="o2944" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity constraint="disc" id="o2943" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s12" value="plumose" value_original="plumose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="not adaxially" name="pubescence" src="d0_s12" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2945" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, ± barren slopes and terraces, in chaparral, woodlands, forest, and meadows, on serpentine soils, talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="± barren slopes" />
        <character name="habitat" value="terraces" constraint="in chaparral , woodlands , forest , and meadows" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="forest" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="serpentine soils" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Layia discoidea occurs in the South Inner Coast Ranges (Fresno and San Benito counties). Artificial hybrids with L. glandulosa are highly fertile (J. Clausen 1951).</discussion>
  
</bio:treatment>