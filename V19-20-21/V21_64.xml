<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">34</other_info_on_meta>
    <other_info_on_meta type="treatment_page">35</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">melampodiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">melampodium</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">cinereum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 518. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe melampodiinae;genus melampodium;species cinereum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067171</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melampodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cinereum</taxon_name>
    <taxon_name authority="Stuessy" date="unknown" rank="variety">hirtellum</taxon_name>
    <taxon_hierarchy>genus Melampodium;species cinereum;variety hirtellum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melampodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cinereum</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray" date="unknown" rank="variety">ramosissimum</taxon_name>
    <taxon_hierarchy>genus Melampodium;species cinereum;variety ramosissimum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 10–20+ cm.</text>
      <biological_entity id="o15807" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o15808" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades linear-oblong to linear, 10–55 × 1–14 mm, lengths 3–10+ times widths, pinnately lobed, lobes 1–10, ultimate margins entire.</text>
      <biological_entity id="o15809" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s1" to="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s1" to="55" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="14" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s1" value="3-10+" value_original="3-10+" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o15810" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15811" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 1–7 cm.</text>
      <biological_entity id="o15812" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Outer phyllaries 5, connate 1/6–1/3+ their lengths, ovate, 3–7+ mm.</text>
      <biological_entity constraint="outer" id="o15813" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/6" name="lengths" src="d0_s3" to="1/3" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray-florets 7–13;</text>
      <biological_entity id="o15814" name="ray-floret" name_original="ray-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas cream-white (sometimes purplish abaxially), laminae oblongelliptic, 2–8+ × 1–3+ mm.</text>
      <biological_entity id="o15815" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="cream-white" value_original="cream-white" />
      </biological_entity>
      <biological_entity id="o15816" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 25–50.</text>
      <biological_entity id="o15817" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s6" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Fruits 1.3–2.2 mm. 2n = 20, 40.</text>
      <biological_entity id="o15818" name="fruit" name_original="fruits" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15819" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="20" value_original="20" />
        <character name="quantity" src="d0_s7" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites, sand, gravel, loam, or clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="open" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="gravel" />
        <character name="habitat" value="loam" />
        <character name="habitat" value="clay soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Along the interface of the distributions of Melampodium cinereum and M. leucanthum, some plants are intermediate for traits used to characterize the two species; perhaps the two should be treated as one.</discussion>
  <discussion>Plants of Melampodium cinereum with leaf blades linear-oblong and basally strigose with hairs 0.3–0.6 mm have been treated as var. cinereum, plants with leaf blades linear-oblong and basally strigose with hairs 0.6–1.5 mm as var. hirtellum, and plants with leaf blades linear and basally strigose with hairs 0.3–0.6 mm as var. ramosissimum (T. F. Stuessy 1972).</discussion>
  
</bio:treatment>