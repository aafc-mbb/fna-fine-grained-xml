<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="treatment_page">58</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">rudbeckia</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="species">mollis</taxon_name>
    <place_of_publication>
      <publication_title>Sketch Bot. S. Carolina</publication_title>
      <place_in_publication>2: 453. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section rudbeckia;species mollis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067450</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, to 100 cm (taprooted).</text>
      <biological_entity id="o18784" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems softly pilose to woolly (hairs spreading, 1–4 mm, longer ones toward bases).</text>
      <biological_entity id="o18787" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="softly pilose" name="pubescence" src="d0_s1" to="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades oblong to oblong-lanceolate (not lobed), margins entire or serrate, apices acute to obtuse, faces softly pilose to woolly, glanddotted;</text>
      <biological_entity id="o18788" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18789" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o18790" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o18791" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
      <biological_entity id="o18792" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="softly pilose" name="pubescence" src="d0_s2" to="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal petiolate, 5–12 × 0.5–2 cm, bases attenuate;</text>
      <biological_entity id="o18793" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o18794" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18795" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline sessile, (elliptic to pandurate) 1.5–10 × 1–4 cm (mid largest, bases cuneate to auriculate).</text>
      <biological_entity id="o18796" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o18797" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly or (2–12) in loose, corymbiform arrays.</text>
      <biological_entity id="o18798" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o18799" from="2" name="atypical_quantity" src="d0_s5" to="12" />
      </biological_entity>
      <biological_entity id="o18799" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s5" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries to 1.5 cm (faces hairy and glanddotted).</text>
      <biological_entity id="o18800" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles conic to hemispheric;</text>
      <biological_entity id="o18801" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s7" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>paleae 5–6.5 mm, apices acute, abaxial tips hirsute and glanddotted.</text>
      <biological_entity id="o18802" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18803" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18804" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 10–16;</text>
      <biological_entity id="o18805" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae linear to oblanceolate, 20–40 × 4–7 mm, abaxially hairy and glanddotted.</text>
      <biological_entity id="o18806" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s10" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Discs 10–20 × 12–18 mm.</text>
      <biological_entity id="o18807" name="disc" name_original="discs" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s11" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 200–400+;</text>
      <biological_entity id="o18808" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="200" name="quantity" src="d0_s12" to="400" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla bases yellowish green, otherwise brown-purple, 3–4.2 mm;</text>
      <biological_entity constraint="corolla" id="o18809" name="base" name_original="bases" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="otherwise" name="coloration_or_density" src="d0_s13" value="brown-purple" value_original="brown-purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branches ca. 1.5 mm, apices acute to obtuse.</text>
      <biological_entity id="o18810" name="branch-style" name_original="style-branches" src="d0_s14" type="structure">
        <character name="distance" src="d0_s14" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o18811" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s14" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 2.5–3.5 mm;</text>
      <biological_entity id="o18812" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0 or coroniform, to 0.1 mm. 2n = 38.</text>
      <biological_entity id="o18813" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18814" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" modifier="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–90 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="90" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Softhair coneflower</other_name>
  
</bio:treatment>