<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="treatment_page">457</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">marshalliinae</taxon_name>
    <taxon_name authority="Schreber" date="1791" rank="genus">marshallia</taxon_name>
    <taxon_name authority="(Walter) Beadle &amp; F. E. Boynton" date="1901" rank="species">obovata</taxon_name>
    <place_of_publication>
      <publication_title>Biltmore Bot. Stud.</publication_title>
      <place_in_publication>1: 5. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe marshalliinae;genus marshallia;species obovata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067165</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Athanasia</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">obovata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>201. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Athanasia;species obovata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marshallia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">obovata</taxon_name>
    <taxon_name authority="(M. A. Curtis) Beadle &amp; F. E. Boynton" date="unknown" rank="variety">platyphylla</taxon_name>
    <taxon_hierarchy>genus Marshallia;species obovata;variety platyphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marshallia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">obovata</taxon_name>
    <taxon_name authority="Channell" date="unknown" rank="variety">scaposa</taxon_name>
    <taxon_hierarchy>genus Marshallia;species obovata;variety scaposa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60 cm.</text>
      <biological_entity id="o14870" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly basal (distal slightly reduced);</text>
      <biological_entity id="o14871" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o14872" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal petiolate;</text>
      <biological_entity constraint="basal" id="o14873" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 3-nerved, elliptic, oblanceolate, or spatulate, 5–10 cm × 5–15 mm.</text>
      <biological_entity id="o14874" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 1–2, 20–30 mm diam.</text>
      <biological_entity id="o14875" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 20–40 cm.</text>
      <biological_entity id="o14876" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s5" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 5–10 × 2–3 mm, apices obtuse.</text>
      <biological_entity id="o14877" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14878" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae linear-spatulate, apices obtuse.</text>
      <biological_entity id="o14879" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-spatulate" value_original="linear-spatulate" />
      </biological_entity>
      <biological_entity id="o14880" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas white, lobes 2.5–6 × 0.5–1 mm.</text>
      <biological_entity id="o14881" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o14882" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi: scale margins entire or denticulate.</text>
      <biological_entity id="o14883" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity constraint="scale" id="o14884" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14885" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Understories, pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="understories" />
        <character name="habitat" value="pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Marshallia obovata grows on the piedmont and on the Atlantic coastal plain. Scapiform plants with the leafy parts of stems less than 1/4 lengths of peduncles are sometimes known as var. scaposa. They occur along the inner Atlantic coastal plain.</discussion>
  
</bio:treatment>