<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">496</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">chamaemelum</taxon_name>
    <taxon_name authority="(Brotero) Vasconcellos" date="1966" rank="species">fuscatum</taxon_name>
    <place_of_publication>
      <publication_title>Anais Inst. Vinho Porto</publication_title>
      <place_in_publication>20: 276. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus chamaemelum;species fuscatum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250066328</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anthemis</taxon_name>
    <taxon_name authority="Brotero" date="unknown" rank="species">fuscata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Lusit.</publication_title>
      <place_in_publication>1: 394. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anthemis;species fuscata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–20 (–35+) cm.</text>
      <biological_entity id="o20421" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending (simple or branched, not forming mats), glabrous or puberulent.</text>
      <biological_entity id="o20422" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal petiolate, ± ovate to elliptic, 1–4 cm, usually 2-pinnately lobed;</text>
      <biological_entity id="o20423" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o20424" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="less ovate" name="shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="usually 2-pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal sessile, ± elliptic, 1–2 cm, 0–1-pinnately lobed.</text>
      <biological_entity id="o20425" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o20426" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="0-1-pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 3–4 × 6–10 mm.</text>
      <biological_entity id="o20427" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries: margins and apices brownish, abaxial faces glabrous or glabrate.</text>
      <biological_entity id="o20428" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity id="o20429" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o20430" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20431" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae 2–3 mm, margins brownish.</text>
      <biological_entity id="o20432" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20433" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 12–15+;</text>
      <biological_entity id="o20434" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae 8–15 mm.</text>
      <biological_entity id="o20435" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 2.5–3 mm.</text>
      <biological_entity constraint="disc" id="o20436" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1–1.3 mm. 2n = 18.</text>
      <biological_entity id="o20437" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20438" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Chamomile</other_name>
  <discussion>Chamaemelum fuscatum is found on the Outer North Coast ranges, especially in vineyards. It is native to the Mediterranean.</discussion>
  
</bio:treatment>