<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="treatment_page">321</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="(A. Gray) A. M. Powell" date="1968" rank="section">laphamia</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="species">microglossa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">microglossa</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section laphamia;species microglossa;variety microglossa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068640</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Perityle</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">microglossa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">effusa</taxon_name>
    <taxon_hierarchy>genus Perityle;species microglossa;variety effusa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–60 cm (stems erect or decumbent);</text>
    </statement>
    <statement id="d0_s1">
      <text>usually copiously glandular-pubescent, rarely puberulent with scattered glandular-hairs.</text>
      <biological_entity id="o23940" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually copiously" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character constraint="with glandular-hairs" constraintid="o23941" is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o23941" name="glandular-hair" name_original="glandular-hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 5–30 (–40) mm;</text>
      <biological_entity id="o23942" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23943" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades often turning purplish (variable in size and shape), cordate, ovate, or broadly ovate-cordate to subreniform, or subdeltate, usually 20–60 × 20–50 mm, margins singly or double crenate, even or irregular to strongly 3-lobed or cleft, pedately divided to subhastate (faces puberulent, glandular-puberulent, or glabrous).</text>
      <biological_entity id="o23944" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23945" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character name="arrangement" src="d0_s3" value="double" value_original="double" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="even" name="architecture_or_course" src="d0_s3" value="irregular" value_original="irregular" />
        <character char_type="range_value" from="cleft pedately divided" name="shape" src="d0_s3" to="subhastate" />
        <character char_type="range_value" from="cleft pedately divided" name="shape" src="d0_s3" to="subhastate" />
      </biological_entity>
      <biological_entity id="o23946" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
        <character is_modifier="true" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character char_type="range_value" from="broadly ovate-cordate" is_modifier="true" name="shape" src="d0_s3" to="subreniform or subdeltate" />
        <character char_type="range_value" from="20" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="50" to_unit="mm" />
      </biological_entity>
      <relation from="o23945" id="r1638" name="turning" negation="false" src="d0_s3" to="o23946" />
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in corymbiform arrays, 3.5–4.5 (–6) × 4–6.5 (–7) mm.</text>
      <biological_entity id="o23947" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in corymbiform arrays" value_original="in corymbiform arrays" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" notes="" src="d0_s4" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="" src="d0_s4" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23948" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o23947" id="r1639" name="in" negation="false" src="d0_s4" to="o23948" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 6–70 mm.</text>
      <biological_entity id="o23949" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate.</text>
      <biological_entity id="o23950" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 14–30, narrowly to broadly lanceolate, 2.5–3 × 0.6–1 mm.</text>
      <biological_entity id="o23951" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s7" to="30" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 5–13;</text>
      <biological_entity id="o23952" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, laminae oblong, 1.5–3.5 mm.</text>
      <biological_entity id="o23953" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o23954" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 40–100;</text>
      <biological_entity id="o23955" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s10" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, tubes 0.5–0.7 mm, throats tubular-funnelform, 0.5–0.8 mm, lobes 0.1–0.2 mm.</text>
      <biological_entity id="o23956" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o23957" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23958" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23959" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae usually linear-oblong to linear-elliptic, rarely narrowly obovate, 1.5–2 mm, margins usually prominently calloused, ciliate;</text>
      <biological_entity id="o23960" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="usually linear-oblong" name="shape" src="d0_s12" to="linear-elliptic" />
        <character is_modifier="false" modifier="rarely narrowly" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23961" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually prominently" name="texture" src="d0_s12" value="calloused" value_original="calloused" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 2 unequal, antrorsely barbellate bristles 0.5–1.2 mm plus crowns of hyaline, laciniate scales.</text>
      <biological_entity id="o23962" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o23963" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="true" modifier="antrorsely" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23965" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o23962" id="r1640" name="consist_of" negation="false" src="d0_s13" to="o23963" />
      <relation from="o23964" id="r1641" name="part_of" negation="false" src="d0_s13" to="o23965" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 68, 102.</text>
      <biological_entity id="o23964" name="crown" name_original="crowns" src="d0_s13" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity constraint="2n" id="o23966" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="68" value_original="68" />
        <character name="quantity" src="d0_s14" value="102" value_original="102" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year around.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8a.</number>
  <discussion>The only known occurrence of Perityle microglossa from Arizona is the type of P. microglossa var. effusa (Santa Catalina Mountains, Pima County, 1882). Variety microglossa occurs as an introduced weed in extreme southern Texas and northern Mexico. Its shorter ligules and more glandular nature distinguish it from var. saxosa, which occurs only in Mexico. Cypsela and pappus characters, and distribution, distinguish P. microglossa from P. emoryi.</discussion>
  
</bio:treatment>