<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Linda E. Watson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="mention_page">544</other_info_on_meta>
    <other_info_on_meta type="treatment_page">543</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">COTULA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 891. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 380. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus COTULA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kotule, small cup</other_info_on_name>
    <other_info_on_name type="fna_id">108176</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 2–25 [–50+] cm (sometimes aromatic).</text>
      <biological_entity id="o6155" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect or prostrate to decumbent or ascending (sometimes rooting at nodes), usually branched, glabrous or ± strigillose to villous (hairs mostly basifixed).</text>
      <biological_entity id="o6157" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="more or less strigillose" name="pubescence" src="d0_s1" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually mostly cauline [basal];</text>
      <biological_entity id="o6159" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate [opposite];</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o6158" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades obovate or spatulate to lanceolate or linear, sometimes 1–3-pinnately [palmati-pinnately] lobed, ultimate margins entire or irregularly toothed, faces glabrous or ± strigillose to villous [lanate] (hairs mostly basifixed).</text>
      <biological_entity id="o6160" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="lanceolate or linear" />
        <character is_modifier="false" modifier="sometimes 1-3-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o6161" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o6162" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="more or less strigillose" name="pubescence" src="d0_s5" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads disciform [discoid or radiate], borne singly (peduncles sometimes dilated).</text>
      <biological_entity id="o6163" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres broadly hemispheric to saucer-shaped, 3–12+ [–15+] mm diam.</text>
      <biological_entity id="o6164" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly hemispheric" name="shape" src="d0_s7" to="saucer-shaped" />
        <character char_type="range_value" from="12+" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 13–30+ in 2–3+ series, margins and apices (colorless, light to dark-brown, or purplish) scarious.</text>
      <biological_entity id="o6165" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o6166" from="13" name="quantity" src="d0_s8" to="30" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6166" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6167" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o6168" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex [conic], epaleate (sometimes ± covered with persistent stalks of florets).</text>
      <biological_entity id="o6169" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 [5–8+, pistillate, fertile; corollas white] (peripheral pistillate florets 8–80+ in 1–3+ series; corollas usually none).</text>
      <biological_entity id="o6170" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 12–200+ [–600+], bisexual, fertile [functionally staminate];</text>
      <biological_entity id="o6171" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="200+" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="600" upper_restricted="false" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="200" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas ochroleucous or yellow, tubes ± cylindric (bases sometimes adaxially saccate), throats abruptly ampliate, lobes (3–) 4, ± deltate (sometimes one larger than others, usually each with central resin canal).</text>
      <biological_entity id="o6172" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o6173" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o6174" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s12" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o6175" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="4" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae obovoid to oblong, obcompressed or flattened, ribs 2, lateral, sometimes becoming wings, faces ± papillate (pericarps relatively thin, sometimes with myxogenic cells and/or 2 lateral resin sacs);</text>
      <biological_entity id="o6176" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s13" to="oblong obcompressed or flattened" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s13" to="oblong obcompressed or flattened" />
      </biological_entity>
      <biological_entity id="o6177" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s13" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o6178" name="wing" name_original="wings" src="d0_s13" type="structure" />
      <biological_entity id="o6179" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 0.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 10.</text>
      <biological_entity id="o6180" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o6181" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; s Old World; introduced also (perhaps some native) in Mexico, South America, s Oceanic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Old World" establishment_means="introduced" />
        <character name="distribution" value="also (perhaps some native) in Mexico" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="s Oceanic Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>126.</number>
  <discussion>Species 55 (2 in the flora).</discussion>
  <discussion>Some species of Cotula are widely naturalized. F. Hrusa et al. (2002) reported Cotula mexicana (de Candolle) Cabrera as established on golf courses in California; it is similar to C. australis and differs in leaf blades mostly 1-pinnate, receptacles pilose, and disc florets functionally staminate.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals (± villous); leaf blades 2–3-pinnately lobed; involucres 3–4(–6) mm diam.; pistillate florets 8–40(–80+) in ± 1–3+ series</description>
      <determination>1 Cotula australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials (glabrous); leaf blades entire or irregularly toothed or lobed; involucres 6–9 (–12+) mm diam.; pistillate florets 12–40+ in 1 series</description>
      <determination>2 Cotula coronopifolia</determination>
    </key_statement>
  </key>
</bio:treatment>