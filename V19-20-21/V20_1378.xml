<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Theodore M. Barkley†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">541</other_info_on_meta>
    <other_info_on_meta type="treatment_page">614</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="H. Robinson &amp; Brettell" date="1974" rank="genus">BARKLEYANTHUS</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>27: 407. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus BARKLEYANTHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">For Theodore M. Barkley, 1934–2004, North American botanist</other_info_on_name>
    <other_info_on_name type="fna_id">103513</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 100–200 [–400+] cm (usually glabrous, sometimes sparsely arachnose or glabrate).</text>
      <biological_entity id="o6693" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="400" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect to lax.</text>
      <biological_entity id="o6694" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate (clustered distally);</text>
    </statement>
    <statement id="d0_s4">
      <text>weakly petiolate;</text>
      <biological_entity id="o6695" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades weakly 3-nerved, lance-elliptic or lanceolate to lance-linear, margins obscurely dentate to subentire or entire, faces usually glabrous.</text>
      <biological_entity id="o6696" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="lance-linear" />
      </biological_entity>
      <biological_entity id="o6697" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="obscurely dentate" name="shape" src="d0_s5" to="subentire or entire" />
      </biological_entity>
      <biological_entity id="o6698" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, in cymiform or paniculiform arrays (crowded in terminal and axillary clusters).</text>
      <biological_entity id="o6699" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o6700" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o6699" id="r606" name="in" negation="false" src="d0_s6" to="o6700" />
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0 or of 1–2+ bractlets.</text>
      <biological_entity id="o6701" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 1" is_modifier="false" name="quantity" src="d0_s7" to="2+ bractlets" />
      </biological_entity>
      <biological_entity id="o6702" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" upper_restricted="false" />
      </biological_entity>
      <relation from="o6701" id="r607" name="consist_of" negation="false" src="d0_s7" to="o6702" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric or campanulate to weakly turbinate, 5–8 mm diam.</text>
      <biological_entity id="o6703" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s8" to="weakly turbinate" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries persistent, 5–8+ in (1–) 2 series, erect, distinct, elliptic or oblong to obovate, equal, margins ± scarious.</text>
      <biological_entity id="o6704" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o6705" from="5" name="quantity" src="d0_s9" to="8" upper_restricted="false" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="obovate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o6705" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s9" to="2" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6706" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to convex, deeply foveolate (margins of sockets raggedly toothed), epaleate.</text>
      <biological_entity id="o6707" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="convex" />
        <character is_modifier="false" modifier="deeply" name="relief" src="d0_s10" value="foveolate" value_original="foveolate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (3–) 5 (–8), pistillate, fertile;</text>
      <biological_entity id="o6708" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="8" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow (laminae linear-elliptic).</text>
      <biological_entity id="o6709" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 14–25+, bisexual, fertile;</text>
      <biological_entity id="o6710" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s13" to="25" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, tubes longer than to equaling narrowly funnelform throats, lobes 5, recurved, lance-linear;</text>
      <biological_entity id="o6711" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o6712" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than to equaling narrowly funnelform throats" constraintid="o6713, o6714" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6713" name="funnelform" name_original="funnelform" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o6714" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o6715" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branches: stigmatic areas continuous, apices ± dilated-truncate.</text>
      <biological_entity id="o6716" name="style-branch" name_original="style-branches" src="d0_s15" type="structure" />
      <biological_entity constraint="stigmatic" id="o6717" name="area" name_original="areas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o6718" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s15" value="dilated-truncate" value_original="dilated-truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae ± prismatic to obpyramidal, 5-nerved, strigillose to hirtellous;</text>
      <biological_entity id="o6719" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="less prismatic" name="shape" src="d0_s16" to="obpyramidal" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5-nerved" value_original="5-nerved" />
        <character char_type="range_value" from="strigillose" name="pubescence" src="d0_s16" to="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi persistent, of 60–80 (–120), white, barbellulate bristles.</text>
      <biological_entity id="o6721" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s17" to="120" />
        <character char_type="range_value" from="60" is_modifier="true" name="quantity" src="d0_s17" to="80" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <relation from="o6720" id="r608" name="consist_of" negation="false" src="d0_s17" to="o6721" />
    </statement>
    <statement id="d0_s18">
      <text>x = 30.</text>
      <biological_entity id="o6720" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o6722" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>226.</number>
  <other_name type="common_name">Jarilla</other_name>
  <discussion>Species 1.</discussion>
  
</bio:treatment>