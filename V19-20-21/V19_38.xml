<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="treatment_page">88</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">onopordum</taxon_name>
    <taxon_name authority="Willdenow" date="1803" rank="species">tauricum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>3: 1687. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus onopordum;species tauricum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250067221</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–200 cm, herbage glandular-puberulent, sticky throughout.</text>
      <biological_entity id="o403" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o404" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="throughout" name="texture" src="d0_s0" value="sticky" value_original="sticky" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: wings 0.5–2 cm wide.</text>
      <biological_entity id="o405" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o406" name="wing" name_original="wings" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 10–30 cm, margins shallowly to deeply 1–2-pinnatifid, with 6–8 pairs of acutely triangular lobes, thinly arachnoid tomentose when young.</text>
      <biological_entity id="o407" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o408" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="shallowly to deeply" name="shape" src="d0_s2" value="1-2-pinnatifid" value_original="1-2-pinnatifid" />
        <character is_modifier="false" modifier="thinly" name="pubescence" notes="" src="d0_s2" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o409" name="pair" name_original="pairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s2" to="8" />
      </biological_entity>
      <biological_entity id="o410" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="acutely" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o408" id="r39" name="with" negation="false" src="d0_s2" to="o409" />
      <relation from="o409" id="r40" name="part_of" negation="false" src="d0_s2" to="o410" />
    </statement>
    <statement id="d0_s3">
      <text>Heads mostly borne singly at branch tips.</text>
      <biological_entity id="o411" name="head" name_original="heads" src="d0_s3" type="structure">
        <character constraint="at branch tips" constraintid="o412" is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity constraint="branch" id="o412" name="tip" name_original="tips" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± spheric, 20–50 mm diam (excluding spines), base truncate to concave.</text>
      <biological_entity id="o413" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o414" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries lanceolate, bases 3–4 mm wide, glabrous or glandular-puberulent, sometimes ± cobwebby-tomentose, spines to 4 mm.</text>
      <biological_entity id="o415" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o416" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s5" value="cobwebby-tomentose" value_original="cobwebby-tomentose" />
      </biological_entity>
      <biological_entity id="o417" name="spine" name_original="spines" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Corollas purplish-pink, 25–30 mm.</text>
      <biological_entity id="o418" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish-pink" value_original="purplish-pink" />
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 5–6 mm;</text>
      <biological_entity id="o419" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi of many whitish to tan, scabrous or minutely barbed bristles 8–10 mm. 2n = 34 (Russia).</text>
      <biological_entity id="o420" name="pappus" name_original="pappi" src="d0_s8" type="structure" />
      <biological_entity id="o421" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="many" value_original="many" />
        <character char_type="range_value" from="whitish" is_modifier="true" name="coloration" src="d0_s8" to="tan" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="true" modifier="minutely" name="architecture_or_shape" src="d0_s8" value="barbed" value_original="barbed" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o422" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="34" value_original="34" />
      </biological_entity>
      <relation from="o420" id="r41" name="consists_of" negation="false" src="d0_s8" to="o421" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, arid woodlands, riparian areas, roadsides, agricultural lands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="arid woodlands" />
        <character name="habitat" value="riparian areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="agricultural lands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Colo.; s Europe; sw Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Taurian thistle</other_name>
  <other_name type="common_name">bull cottonthistle</other_name>
  <discussion>Taurian thistle is a noxious weed in California and Colorado. In southeastern Colorado it sometimes grows with Onopordum acanthium. Putative hybrids have been observed in this area.</discussion>
  
</bio:treatment>