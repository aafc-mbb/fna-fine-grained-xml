<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Theodore M. Barkley†,David F. Murray</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">541</other_info_on_meta>
    <other_info_on_meta type="mention_page">542</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
    <other_info_on_meta type="mention_page">545</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="treatment_page">615</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="(Reichenbach) Reichenbach" date="1841" rank="genus">TEPHROSERIS</taxon_name>
    <place_of_publication>
      <publication_title>Deut. Bot. Herb.-Buch,</publication_title>
      <place_in_publication>87. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus TEPHROSERIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek tephros, ashlike or ash-colored, and seris, endive or chicory, presumably alluding to color of the densely woolly leaves</other_info_on_name>
    <other_info_on_name type="fna_id">132452</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Cineraria</taxon_name>
    <taxon_name authority="Reichenbach" date="unknown" rank="unranked">Tephroseris</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Mössler and H. G. L. Reichenbach, Handb. Gewächsk. ed.</publication_title>
      <place_in_publication>2, 2: 1498. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cineraria;unranked Tephroseris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, (5–) 10–100+ cm (rhizomatous or caudices ± erect; plants usually arachnose, floccose, lanate, tomentose, or villous, sometimes unevenly glabrate).</text>
      <biological_entity id="o1854" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 or more (loosely clustered), erect.</text>
      <biological_entity id="o1857" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" unit="or more" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (basal and proximal cauline; distal leaves usually sessile, smaller, bractlike);</text>
      <biological_entity id="o1858" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades pinnately nerved, lanceolate, linear-oblanceolate, oblanceolate, ovate, or subrhombic (bases tapering or contracted to petioles), margins entire or dentate, denticulate, subentire, subpinnatifid, or wavy, faces usually arachnose, floccose, lanate, tomentose, or villous, sometimes unevenly glabrate.</text>
      <biological_entity id="o1859" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subrhombic" value_original="subrhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subrhombic" value_original="subrhombic" />
      </biological_entity>
      <biological_entity id="o1860" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subpinnatifid" value_original="subpinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o1861" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="arachnose" value_original="arachnose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="lanate" value_original="lanate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes unevenly" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, borne singly or (2–40+) in corymbiform arrays.</text>
      <biological_entity id="o1862" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o1863" from="2" name="atypical_quantity" src="d0_s6" to="40" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1863" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0.</text>
      <biological_entity id="o1864" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric or campanulate to turbinate, 8–12+ mm diam.</text>
      <biological_entity id="o1865" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s8" to="turbinate" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s8" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries persistent, usually 8, 13, or 21 in (1–) 2 series, erect, distinct (margins interlocking), lance-linear to lanceolate or oblong, equal, margins ± scarious (abaxial faces usually arachnose, floccose, lanate, tomentose, or villous, sometimes unevenly glabrate).</text>
      <biological_entity id="o1866" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character modifier="usually" name="quantity" src="d0_s9" value="8" value_original="8" />
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
        <character constraint="in series" constraintid="o1867" name="quantity" src="d0_s9" value="21" value_original="21" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="lance-linear" name="shape" src="d0_s9" to="lanceolate or oblong" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o1867" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s9" to="2" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1868" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat or ± dome-shaped (not conic), smooth, epaleate.</text>
      <biological_entity id="o1869" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="dome--shaped" value_original="dome--shaped" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 0 or mostly 8, 13, or 21, pistillate, fertile;</text>
      <biological_entity id="o1870" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s11" value="mostly" value_original="mostly" />
        <character name="quantity" src="d0_s11" value="8" value_original="8" />
        <character name="quantity" src="d0_s11" value="13" value_original="13" />
        <character name="quantity" src="d0_s11" value="21" value_original="21" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas usually yellow, orange, or orange-yellow, sometimes ochroleucous or white [brick-colored, purplish] (laminae usually 5–20 mm, sometimes 1–3 mm).</text>
      <biological_entity id="o1871" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 30–80+, bisexual, fertile;</text>
      <biological_entity id="o1872" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="80" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas usually yellow, orange, or orange-yellow, sometimes ochroleucous or white [brick-colored, purplish], tubes longer than or equaling campanulate throats, lobes 5, erect or recurved, lance-linear (anther collars cylindric);</text>
      <biological_entity id="o1873" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1874" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than or equaling campanulate throats" constraintid="o1875" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1875" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="shape" src="d0_s14" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o1876" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branches: stigmatic areas continuous, apices rounded-truncate.</text>
      <biological_entity id="o1877" name="style-branch" name_original="style-branches" src="d0_s15" type="structure" />
      <biological_entity constraint="stigmatic" id="o1878" name="area" name_original="areas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o1879" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded-truncate" value_original="rounded-truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae ± cylindric, 10-ribbed or nerved, glabrous or puberulent;</text>
      <biological_entity id="o1880" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="10-ribbed" value_original="10-ribbed" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi persistent, of 30–60+, white, whitish, or brownish, barbellulate bristles (equaling or slightly exceeding involucres, sometimes exceeding involucres to 10 mm in T. palustris).</text>
      <biological_entity id="o1882" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s17" to="60" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="brownish" value_original="brownish" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <relation from="o1881" id="r177" name="consist_of" negation="false" src="d0_s17" to="o1882" />
    </statement>
    <statement id="d0_s18">
      <text>x = 24.</text>
      <biological_entity id="o1881" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o1883" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>n North America, centered in n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="n North America" establishment_means="native" />
        <character name="distribution" value="centered in n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>227.</number>
  <discussion>Species 40–50 (6 in the flora).</discussion>
  <discussion>Species of Tephroseris are variable and poorly defined; their nomenclature is complex. The present treatment is provisional.</discussion>
  <discussion>Tephroseris has been treated within Senecio in most North American floristic studies (T. M. Barkley 1999). Moreover, the tradition has been to treat much of the variation as varieties or subspecies within a broadly circumscribed Senecio (Tephroseris) atropurpureus (e.g., Barkley 1978; E. Hultén 1968; H. J. Scoggan 1978–1979, part 4; S. L. Welsh 1974); current thought is to recognize more species and thereby bring North American species concepts more into accord with those of Russian botanists (e.g., S. S. Kharkevich 1992, vol. 6; I. M. Krasnoborov 1997, vol. 13; E. Wiebe 2000). The circumscription of Tephroseris was discussed by B. Nordenstam (1978).</discussion>
  <discussion>The Eurasian Tephroseris atropurpureus (Ledebour) B. Fedtschenko, in the strict sense, and T. subfrigida (Komarov) Holub may occur in far western Alaska. The former resembles T. frigida; it has smaller heads, narrower, purplish phyllaries, and a different “aspect.” Tephroseris subfrigida is a relatively tall, thin plant with phyllaries purplish on the distal one-third; the bases of the heads have a light yellowish, non-woolly tomentum.</discussion>
  <references>
    <reference>Cufodontis, G. 1933. Kritische Revision von Senecio Section Tephroseris. Feddes Repert. Spec. Nov. Regni Veg. 70: 1–266.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals or biennials (rarely perennials; caudices fibrous-rooted); leaves basal and cauline (basal and proximal sometimes withering before flowering, mid-stem leaves prominent at flowering); heads (4–)6–20(–40+)</description>
      <determination>1 Tephroseris palustris</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials (rhizomes spreading, sometimes suberect); leaves basal and cauline (basal and proximal cauline usually evident at flowering; distal cauline smaller, bractlike); heads 1–4(–6+)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal leaves: margins usually irregularly toothed; phyllaries brown-woolly</description>
      <determination>2 Tephroseris kjellmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal leaves: margins subentire or denticulate (if shallowly toothed, phyllaries yellow-hairy at bases); phyllaries ± villous or floccose- or lanate-tomentose (hairs yellow, white, or purplish), sometimes glabrate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries floccose-tomentose (hairs yellow)</description>
      <determination>3 Tephroseris yukonensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Phyllaries ± villous or floccose- or lanate-tomentose (hairs white or purplish), sometimes glabrate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucres tapering to peduncles; phyllaries (and distal herbage) ± villous (at least some hairs purplish or with purplish cross-walls)</description>
      <determination>4 Tephroseris frigida</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucres ± abruptly contracted to peduncles; phyllaries floccose- or lanate-tomentose (hairs white) to unevenly glabrate</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Phyllaries purple (at least distal 1/3); corollas orange or orange-yellow; laminae of ray corollas usually 15–25 mm</description>
      <determination>5 Tephroseris lindstroemii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Phyllaries greenish (tips sometimes purplish); corollas yellow; laminae of ray corollas (8–)10–15 mm</description>
      <determination>6 Tephroseris tundricola</determination>
    </key_statement>
  </key>
</bio:treatment>