<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="treatment_page">321</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="Greene" date="1886" rank="species">squalida</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>2: 152. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species squalida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067158</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacothrix</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">foliosa</taxon_name>
    <taxon_name authority="(Greene) E. W. Williams" date="unknown" rank="variety">squalida</taxon_name>
    <taxon_hierarchy>genus Malacothrix;species foliosa;variety squalida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacothrix</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">insularis</taxon_name>
    <taxon_name authority="(Greene) Ferris" date="unknown" rank="variety">squalida</taxon_name>
    <taxon_hierarchy>genus Malacothrix;species insularis;variety squalida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–30 cm.</text>
      <biological_entity id="o12704" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3+, ascending to erect (stout), branched from bases and distally, ± leafy, glabrous.</text>
      <biological_entity id="o12705" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character constraint="from bases" constraintid="o12706" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally; more or less" name="architecture" notes="" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12706" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal obovate to oblanceolate, pinnately lobed (lobes 2–6 pairs), not fleshy, ultimate margins entire or dentate, faces glabrous;</text>
      <biological_entity constraint="cauline" id="o12707" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o12708" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="oblanceolate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o12709" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o12710" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal not notably reduced (narrowly ovate with 5–10 narrow teeth or lobes).</text>
      <biological_entity constraint="cauline" id="o12711" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o12712" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not notably" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi 0.</text>
      <biological_entity id="o12713" name="calyculus" name_original="calyculi" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres narrowly to broadly campanulate, 9–12 × 4–10 mm.</text>
      <biological_entity id="o12714" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 31–49 in 5–6+ series (midstripes green or reddish), broadly ovate (outermost) to lance-oblong or lance-linear, unequal, hyaline margins 0.6–1 mm wide, faces glabrous.</text>
      <biological_entity id="o12715" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o12716" from="31" name="quantity" src="d0_s6" to="49" />
        <character char_type="range_value" from="broadly ovate" name="shape" notes="" src="d0_s6" to="lance-oblong or lance-linear" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o12716" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12717" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12718" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles usually not bristly.</text>
      <biological_entity id="o12719" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually not" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 39–133;</text>
      <biological_entity id="o12720" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="39" name="quantity" src="d0_s8" to="133" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas light yellow, 12–19 mm;</text>
      <biological_entity id="o12721" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light yellow" value_original="light yellow" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 6–11 mm.</text>
      <biological_entity constraint="outer" id="o12722" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± prismatic or columnar, 1.3–2.1 mm, ribs extending to (and just beyond) apices, 5 more prominent than others;</text>
      <biological_entity id="o12723" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12724" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character constraint="than others" constraintid="o12726" is_modifier="false" name="prominence" src="d0_s11" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o12725" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <biological_entity id="o12726" name="other" name_original="others" src="d0_s11" type="structure" />
      <relation from="o12724" id="r1162" name="extending to" negation="false" src="d0_s11" to="o12725" />
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent, of 15–20, irregular, ± deltate teeth (often hidden by apices of cypselae) plus 0 (–1) bristles.</text>
      <biological_entity id="o12727" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o12728" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s12" to="20" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s12" value="irregular" value_original="irregular" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o12729" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s12" to="20" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s12" value="irregular" value_original="irregular" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
      <relation from="o12727" id="r1163" name="consist_of" negation="false" src="d0_s12" to="o12728" />
      <relation from="o12727" id="r1164" name="consist_of" negation="false" src="d0_s12" to="o12729" />
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 4-porate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 28.</text>
      <biological_entity id="o12730" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="4-porate" value_original="4-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12731" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas between shrubs, on ridges, knife-edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" constraint="between shrubs" />
        <character name="habitat" value="shrubs" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="knife-edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Island or Santa Cruz desertdandelion</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Malacothrix squalida is known only from Middle Anacapa and Santa Cruz islands.</discussion>
  
</bio:treatment>