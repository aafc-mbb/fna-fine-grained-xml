<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Heiser" date="1958" rank="species">paradoxus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>60: 272, fig. 1. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species paradoxus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066893</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 130–200 cm.</text>
      <biological_entity id="o12819" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="130" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, glabrous or ± hispid.</text>
      <biological_entity id="o12820" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o12822" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>opposite (proximal) or mostly alternate;</text>
      <biological_entity id="o12821" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles 1.5–6 cm;</text>
      <biological_entity id="o12823" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate to lanceovate, 7–17.5 × 1.7–8.5 cm, bases cuneate, margins entire or (larger leaves) toothed, abaxial faces ± scabrous, not glanddotted.</text>
      <biological_entity id="o12824" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="lanceovate" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s5" to="17.5" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="width" src="d0_s5" to="8.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12825" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o12826" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12827" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–5.</text>
      <biological_entity id="o12828" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 12–18 cm.</text>
      <biological_entity id="o12829" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s7" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric, 15–20 mm diam.</text>
      <biological_entity id="o12830" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 15–25, lanceolate to lanceovate, 6–19 × 0.7–4 mm (equaling or slightly surpassing discs), (margins ciliate) apices (spreading to recurved) acuminate, abaxial faces usually glabrate or sparsely hispid.</text>
      <biological_entity id="o12831" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="25" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="lanceovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="19" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12832" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12833" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 8–9 mm, apices 3-toothed (apices glabrous).</text>
      <biological_entity id="o12834" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12835" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 12–20;</text>
      <biological_entity id="o12836" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 20–30 mm.</text>
      <biological_entity id="o12837" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s12" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 50+;</text>
      <biological_entity id="o12838" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 5–5.5 mm, lobes reddish;</text>
      <biological_entity id="o12839" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12840" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dark, appendages yellowish or dark (style-branches reddish).</text>
      <biological_entity id="o12841" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o12842" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 3–4 mm, glabrous;</text>
      <biological_entity id="o12843" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 lanceolate scales 2.5–2.9 mm. 2n = 34.</text>
      <biological_entity id="o12844" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity id="o12845" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12846" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
      </biological_entity>
      <relation from="o12844" id="r878" name="consist_of" negation="false" src="d0_s17" to="o12845" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Saturated saline soils, desert wetlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saturated saline soils" />
        <character name="habitat" value="desert wetlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Pecos or paradox sunflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Helianthus paradoxus is listed by the U.S. Fish and Wildlife Service as an endangered species and is in the Center for Plant Conservation’s National Collection of Endangered Plants. It is of hybrid origin; the parents are H. annuus and H. petiolaris (L. H. Rieseberg et al. 1990). It occupies a different habitat type than either parent (H. annuus usually on clay-based mesic soils and H. petiolaris usually on dry, sandy soils).</discussion>
  
</bio:treatment>