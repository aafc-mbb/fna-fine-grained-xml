<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="Haller f." date="1797" rank="species">setosa</taxon_name>
    <place_of_publication>
      <publication_title>Arch. Bot. (Leipzig):</publication_title>
      <place_in_publication>1(2): 1. 1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species setosa</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242416377</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 8–80 cm (taproots shallow).</text>
      <biological_entity id="o914" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, erect (often reddish), stout (fistulose), simple or branched proximally, coarsely setose or hispid (at least distally, setae yellowish).</text>
      <biological_entity id="o915" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s1" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o916" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate, often runcinate or lyrate, 5–30 × 1–8 cm, margins dentate to pinnately lobed (terminal lobes often relatively large), apices acute to obtuse, faces finely hispid (coarsely setose along midribs; cauline leaves lanceolate, bases sagittate with acuminate lobes, margins dentate to deeply laciniate proximally).</text>
      <biological_entity id="o917" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="runcinate" value_original="runcinate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o918" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s4" to="pinnately lobed" />
      </biological_entity>
      <biological_entity id="o919" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o920" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 10–20, in paniculiform or cymiform arrays.</text>
      <biological_entity id="o921" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o922" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o921" id="r82" name="in" negation="false" src="d0_s5" to="o922" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 10–14, linear, coarsely setose bractlets 2–4 mm.</text>
      <biological_entity id="o923" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o924" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s6" to="14" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="true" modifier="coarsely" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o923" id="r83" name="consist_of" negation="false" src="d0_s6" to="o924" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindro-campanulate, 6–10 × 4–10 mm.</text>
      <biological_entity id="o925" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–16, lanceolate, 6–7 mm, (bases strongly keeled and thickened, margins green to yellowish), apices acuminate, abaxial faces coarsely setose or hispid, adaxial with fine hairs.</text>
      <biological_entity id="o926" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="16" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o927" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o928" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s8" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o929" name="face" name_original="faces" src="d0_s8" type="structure" />
      <biological_entity id="o930" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="fine" value_original="fine" />
      </biological_entity>
      <relation from="o929" id="r84" name="with" negation="false" src="d0_s8" to="o930" />
    </statement>
    <statement id="d0_s9">
      <text>Florets 10–20;</text>
      <biological_entity id="o931" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, sometimes reddish abaxially, 8–10 mm.</text>
      <biological_entity id="o932" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes; abaxially" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae reddish-brown, fusiform, 3–5 mm, beaked (beaks 1–2 mm), ribs 10 (rounded, spiculate near bases of beaks);</text>
      <biological_entity id="o933" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o934" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi white (fine, soft), 4 mm. 2n = 8.</text>
      <biological_entity id="o935" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="2n" id="o936" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in mixed conifer forest, disturbed areas, lawns</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in mixed conifer forest" />
        <character name="habitat" value="mixed conifer forest" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Conn., Mo., Mont., N.Y., Ohio, Oreg., Pa., Tenn., Tex., Vt., Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Bristly hawksbeard</other_name>
  <discussion>Crepis setosa is recognized by its annual habit, shallow roots, coarsely setose stems, leaves, and involucres, the relatively large runcinate leaves, sagittate-laciniate cauline leaves, finely beaked cypselae, and white, fine pappus bristles.</discussion>
  
</bio:treatment>