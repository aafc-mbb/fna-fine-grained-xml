<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
    <other_info_on_meta type="illustration_page">239</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Nuttall) Shinners" date="1951" rank="species">sessiliflora</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>19: 71. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species sessiliflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066928</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">sessiliflora</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 317. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysopsis;species sessiliflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Pursh) Nuttall ex de Candolle" date="unknown" rank="species">villosa</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="variety">sessiliflora</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species villosa;variety sessiliflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (10–) 20–70 (–110) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o13514" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–40+, decumbent to ascending or erect (sometimes ± brittle, sometimes reddish-brown), moderately to densely hispido-strigose (long-spreading hairs often broken off), sometimes moderately hairy and densely stipitate-glandular distally (axillary leaf fascicles sometimes present).</text>
      <biological_entity id="o13515" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="40" upper_restricted="false" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending or erect" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sometimes moderately" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: margins flat to strongly undulate;</text>
      <biological_entity id="o13516" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13517" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s3" to="strongly undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal usually withering by flowering;</text>
      <biological_entity id="o13518" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character constraint="by flowering" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline subsessile, blades oblanceolate, (8–) 15–40 (–60) × (2.4–) 4–8 (–11) mm, bases cuneate, margins entire, strigoso-ciliate (proximal hairs longer, spreading), apices acute, faces moderately to densely hispido-strigose;</text>
      <biological_entity id="o13519" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal cauline" id="o13520" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o13521" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s5" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="60" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="atypical_width" src="d0_s5" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13522" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13523" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="strigoso-ciliate" value_original="strigoso-ciliate" />
      </biological_entity>
      <biological_entity id="o13524" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13525" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s5" value="hispido-strigose" value_original="hispido-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades usually narrowly to broadly lanceolate, sometimes oblanceolate, 6.5–40 × 1.5–8 mm, usually reduced distally, bases rounded, faces sparsely to densely hispido-strigose or short-strigose (5–245 hairs/mm2), sparsely to densely glandular (0–50 glands/mm2).</text>
      <biological_entity id="o13526" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o13527" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o13528" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s6" to="40" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="usually; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o13529" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o13530" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="short-strigose" modifier="densely" name="pubescence" src="d0_s6" to="sparsely densely glandular" />
        <character char_type="range_value" from="short-strigose" name="pubescence" src="d0_s6" to="sparsely densely glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (1–) 17–36 (–126) in corymbiform or paniculiform arrays, branches ascending.</text>
      <biological_entity id="o13531" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s7" to="17" to_inclusive="false" />
        <character char_type="range_value" from="36" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="126" />
        <character char_type="range_value" constraint="in arrays" constraintid="o13532" from="17" name="quantity" src="d0_s7" to="36" />
      </biological_entity>
      <biological_entity id="o13532" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o13533" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 2–100 mm, densely hispid, strigose, or glandular;</text>
      <biological_entity id="o13534" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="100" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 2–5+, proximal lanceolate, leaflike, distal reduced, phyllary-like, (2–) 2.5–8.5 (–13) × (0.3–) 0.5–1.8 (–4) mm, sometimes a few, large, leafy bracts proximal to heads.</text>
      <biological_entity id="o13535" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="5" upper_restricted="false" />
        <character constraint="to heads" constraintid="o13536" is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character constraint="to heads" constraintid="o13537" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="phyllary-like" value_original="phyllary-like" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s9" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s9" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character is_modifier="false" name="size" src="d0_s9" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o13536" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o13537" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o13538" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="leafy" value_original="leafy" />
        <character constraint="to heads" constraintid="o13539" is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o13539" name="head" name_original="heads" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Involucres cylindric, turbinate, or campanulate (campanulate upon drying), (6–) 7.5–11 (–15) mm.</text>
      <biological_entity id="o13540" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 4–6 series, mid narrowly triangular, unequal (outer lengths 1/5–1/4 inner), margins hyaline, fimbriate-ciliate apically, faces very sparsely to moderately strigose, very sparsely to moderately stipitate-glandular.</text>
      <biological_entity id="o13541" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure" />
      <biological_entity id="o13542" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
      <biological_entity constraint="mid" id="o13543" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o13544" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="apically" name="architecture_or_pubescence_or_shape" src="d0_s11" value="fimbriate-ciliate" value_original="fimbriate-ciliate" />
      </biological_entity>
      <biological_entity id="o13545" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="very sparsely; sparsely to moderately" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o13541" id="r1230" name="in" negation="false" src="d0_s11" to="o13542" />
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets (4–) 7–15 (–24);</text>
      <biological_entity id="o13546" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="7" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="24" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s12" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>laminae (3.5–) 4.5–10.5 (–18.5) × (0.7–) 1–1.7 (–2.4) mm.</text>
      <biological_entity id="o13547" name="lamina" name_original="laminae" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s13" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="18.5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s13" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_width" src="d0_s13" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets (9–) 20–50 (–81);</text>
      <biological_entity id="o13548" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s14" to="20" to_inclusive="false" />
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="81" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas ± ampliate, (4–) 5.5–7 (–9.5) mm, glabrous to glabrate, lobes 0.4–1 mm, sparsely pilose (hairs 0.1–1 mm, osteolate-celled ones often fragile).</text>
      <biological_entity id="o13549" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s15" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s15" to="glabrate" />
      </biological_entity>
      <biological_entity id="o13550" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae monomorphic, obconic, compressed, (1.3–) 2–3 (–4.5) mm, ribs 6–10, faces sparsely to moderately strigose;</text>
      <biological_entity id="o13551" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13552" name="rib" name_original="ribs" src="d0_s16" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s16" to="10" />
      </biological_entity>
      <biological_entity id="o13553" name="face" name_original="faces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s16" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi off-white, outer of linear scales 0.25–0.5 mm, inner of 25–45 bristles 5–8 (–10) mm, longest attenuate to weakly clavate.</text>
      <biological_entity id="o13554" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="off-white" value_original="off-white" />
      </biological_entity>
      <biological_entity id="o13555" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13557" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s17" to="45" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o13554" id="r1231" name="outer of" negation="false" src="d0_s17" to="o13555" />
      <relation from="o13556" id="r1232" name="consist_of" negation="false" src="d0_s17" to="o13557" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 18, 36.</text>
      <biological_entity constraint="inner" id="o13556" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s17" value="longest" value_original="longest" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s17" to="weakly clavate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13558" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
        <character name="quantity" src="d0_s18" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Sessileflower goldenaster</other_name>
  <discussion>Subspecies 4 (4 in the flora).</discussion>
  <discussion>Except for Heterotheca monarchensis, H. sessiliflora is the only species in the section with long (more than 0.25 mm), fragile, osteolate-celled hairs on the corolla lobes, and it is the only species native to the central and southern coastal ranges and valleys of California. The species is divided into four subspecies and five varieties differentiated on the basis of indument features, degree of waviness of the leaf margins, and to a lesser extent, stem height and leaf shape. The treatment here is based on J. C. Semple (1996), which includes a key to and descriptions, illustrations, and distribution maps of the varieties.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal leaf margins distinctly undulate; disc corolla lobe hairs 0.1–1 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal leaf margins weakly undulate to flat; disc corolla lobe hairs 0.2–2.5 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal leaves green, not stiff, usually densely glandular in arrays; heads often subtended by 1–3 large, leaflike bracts equaling or surpassing them, peduncle bracts sometimes phyllary-like distally; coast, dunes of south coastal ranges, 0–100 m, s California, nw Baja California (Mexico)</description>
      <determination>4a Heterotheca sessiliflora subsp. sessiliflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal leaves whitish, stiff; heads not subtended by leaflike bracts surpassing them, bracts reduced distally; (150–)300–1800(–2200) m, Ventura e to e San Bernadino Mountains, Mt. Palomar</description>
      <determination>4b Heterotheca sessiliflora subsp. fastigiata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Proximal stems hispid; distal leaves elliptic to lanceolate, reduced distally (in tall plants), margins sometimes somewhat undulate, faces strigoso-hispid; inland hills and valleys, c California</description>
      <determination>4c Heterotheca sessiliflora subsp. echioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Proximal stems sparsely hispido-strigose; distal leaves oblanceolate, margins flat, faces long-strigose, little reduced distally; dunes, headlands, Mendocino to Santa Cruz (rarely to s of Monterey), San Francisco Bay area at 10–150 m</description>
      <determination>4d Heterotheca sessiliflora subsp. bolanderi</determination>
    </key_statement>
  </key>
</bio:treatment>