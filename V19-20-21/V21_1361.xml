<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">536</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="genus">carphephorus</taxon_name>
    <taxon_name authority="(J. F. Gmelin) H. J.-C. Hebert" date="1968" rank="species">odoratissimus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>70: 483. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus carphephorus;species odoratissimus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066291</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysocoma</taxon_name>
    <taxon_name authority="J. F. Gmelin" date="unknown" rank="species">odoratissima</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 1204. 1792</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysocoma;species odoratissima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="(J. F. Gmelin) Michaux" date="unknown" rank="species">odoratissima</taxon_name>
    <taxon_hierarchy>genus Liatris;species odoratissima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trilisa</taxon_name>
    <taxon_name authority="(J. F. Gmelin) Cassini" date="unknown" rank="species">odoratissima</taxon_name>
    <taxon_hierarchy>genus Trilisa;species odoratissima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–140 (–180) cm.</text>
      <biological_entity id="o910" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="140" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="140" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous.</text>
      <biological_entity id="o911" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal oblanceolate to obovate, mostly 9–50 cm;</text>
      <biological_entity id="o912" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o913" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="obovate" />
        <character char_type="range_value" from="9" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal cauline well developed, gradually reduced distally, clasping, faces not glanddotted.</text>
      <biological_entity id="o914" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o915" name="face" name_original="faces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in flat-topped, corymbiform arrays.</text>
      <biological_entity id="o916" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o917" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o916" id="r57" name="in" negation="false" src="d0_s4" to="o917" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles glabrous.</text>
      <biological_entity id="o918" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3.5–5 mm.</text>
      <biological_entity id="o919" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 5–12 in 1–2 (–3) series, oblanceolate, glanddotted, apices obtuse.</text>
      <biological_entity id="o920" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o921" from="5" name="quantity" src="d0_s7" to="12" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o921" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o922" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles epaleate or partially paleate (paleae 1–2).</text>
      <biological_entity id="o923" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
        <character is_modifier="false" modifier="partially" name="architecture" src="d0_s8" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas glandular, lobes 0.8–1 mm.</text>
      <biological_entity id="o924" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o925" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae glandular;</text>
      <biological_entity id="o926" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappus bristles in ± 1 series.</text>
      <biological_entity constraint="pappus" id="o927" name="bristle" name_original="bristles" src="d0_s11" type="structure" />
      <biological_entity id="o928" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <relation from="o927" id="r58" name="in" negation="false" src="d0_s11" to="o928" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Vanillaleaf</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants with strong odor of coumarin or vanilla; basal leaves usually more than 15 × 5 cm; midstem leaves broadly elliptic, margins often shallowly dentate, apices flared away from stems; primary head-bearing branches diverging from main axes at 10–20°; florets mostly 7–10</description>
      <determination>2a Carphephorus odoratissimus var. odoratissimus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants with slight or no odor of coumarin or vanilla; basal leaves less than 15 × 4 cm; midstem leaves narrowly elliptic, margins entire, apices appressed to stems; primary head-bearing branches diverging from main axes at 30–45°; florets mostly 10–14</description>
      <determination>2b Carphephorus odoratissimus var. subtropicanus</determination>
    </key_statement>
  </key>
</bio:treatment>