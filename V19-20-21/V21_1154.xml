<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">456</other_info_on_meta>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">Eupatorieae</taxon_name>
    <place_of_publication>
      <publication_title>J. Phys. Chim. Hist. Nat. Arts</publication_title>
      <place_in_publication>88: 202. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe Eupatorieae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20539</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, perennials, subshrubs, shrubs, or vines [trees].</text>
      <biological_entity id="o4177" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o4180" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually cauline, sometimes basal or basal and cauline;</text>
      <biological_entity id="o4184" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4185" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>usually opposite, sometimes whorled or alternate;</text>
    </statement>
    <statement id="d0_s3">
      <text>usually petiolate, sometimes sessile;</text>
      <biological_entity id="o4183" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire, toothed, lobed, or dissected.</text>
      <biological_entity constraint="blade" id="o4186" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads homogamous (usually discoid [radiant]), usually in corymbiform, paniculiform, racemiform, or spiciform arrays, sometimes borne singly or in glomerules.</text>
      <biological_entity id="o4187" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="homogamous" value_original="homogamous" />
      </biological_entity>
      <biological_entity id="o4188" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <biological_entity id="o4189" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o4187" id="r326" modifier="usually" name="in" negation="false" src="d0_s5" to="o4188" />
      <relation from="o4187" id="r327" modifier="sometimes" name="borne" negation="false" src="d0_s5" to="o4189" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi 0.</text>
      <biological_entity id="o4190" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries usually persistent (readily falling), usually in 2–8+ series, distinct, and unequal, sometimes in 1–2 series, distinct, and subequal to equal, usually herbaceous to chartaceous, margins and/or apices sometimes scarious (abaxial faces often striate-nerved).</text>
      <biological_entity id="o4191" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="usually herbaceous" name="texture" src="d0_s7" to="chartaceous" />
      </biological_entity>
      <biological_entity id="o4192" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4193" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <biological_entity id="o4194" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o4195" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o4191" id="r328" modifier="usually" name="in" negation="false" src="d0_s7" to="o4192" />
      <relation from="o4191" id="r329" modifier="sometimes" name="in" negation="false" src="d0_s7" to="o4193" />
    </statement>
    <statement id="d0_s8">
      <text>Receptacles usually flat to convex, sometimes spheric or conic, usually epaleate, rarely paleate (paleae readily falling).</text>
      <biological_entity id="o4196" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually flat" name="shape" src="d0_s8" to="convex" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s8" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0.</text>
      <biological_entity id="o4197" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets bisexual, fertile;</text>
      <biological_entity id="o4198" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white, ochroleucous, or pink to purplish, not yellow, not 2-lipped (sometimes ± zygomorphic), lobes (4–) 5, usually ± deltate to lanceovate, sometimes lanceolate to lance-linear;</text>
      <biological_entity id="o4199" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="purplish" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="purplish" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="2-lipped" value_original="2-lipped" />
      </biological_entity>
      <biological_entity id="o4200" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="less deltate" name="shape" src="d0_s11" to="lanceovate" />
        <character char_type="range_value" from="lanceolate" modifier="sometimes" name="shape" src="d0_s11" to="lance-linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anther bases obtuse, rounded, or truncate, not tailed, apical appendages usually ovate to lanceolate, sometimes 0;</text>
      <biological_entity constraint="anther" id="o4201" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="tailed" value_original="tailed" />
      </biological_entity>
      <biological_entity constraint="apical" id="o4202" name="appendage" name_original="appendages" src="d0_s12" type="structure">
        <character char_type="range_value" from="usually ovate" name="shape" src="d0_s12" to="lanceolate" />
        <character modifier="sometimes" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles abaxially papillate to hirsutulous (usually distally, sometimes at bases), branches ± linear, adaxially stigmatic in 2 lines from bases to appendages, appendages usually terete to clavate (lengths often 2–5+ times lengths of stigmatic lines), usually papillate.</text>
      <biological_entity id="o4203" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
      <biological_entity id="o4204" name="branch" name_original="branches" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character constraint="in lines" constraintid="o4205" is_modifier="false" modifier="adaxially" name="structure_in_adjective_form" src="d0_s13" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o4205" name="line" name_original="lines" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4206" name="base" name_original="bases" src="d0_s13" type="structure" />
      <biological_entity id="o4207" name="appendage" name_original="appendages" src="d0_s13" type="structure" />
      <biological_entity id="o4208" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character char_type="range_value" from="usually terete" name="shape" src="d0_s13" to="clavate" />
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
      <relation from="o4205" id="r330" name="from" negation="false" src="d0_s13" to="o4206" />
      <relation from="o4206" id="r331" name="to" negation="false" src="d0_s13" to="o4207" />
    </statement>
    <statement id="d0_s14">
      <text>Cypselae usually ± monomorphic within heads, usually columnar to fusiform, sometimes prismatic or compressed to flattened, rarely, if ever, beaked, bodies often 10-ribbed or (4–) 5-angled, smooth or papillate to rugose between ribs or angles (glabrous or hairy);</text>
      <biological_entity id="o4209" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character constraint="within heads" constraintid="o4210" is_modifier="false" modifier="usually more or less" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="usually columnar" name="shape" notes="" src="d0_s14" to="fusiform" />
        <character char_type="range_value" from="compressed" modifier="sometimes" name="shape" src="d0_s14" to="flattened" />
        <character is_modifier="false" modifier="rarely; ever" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o4210" name="head" name_original="heads" src="d0_s14" type="structure" />
      <biological_entity id="o4211" name="body" name_original="bodies" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="10-ribbed" value_original="10-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="(4-)5-angled" value_original="(4-)5-angled" />
        <character char_type="range_value" constraint="between ribs, angles" constraintid="o4212, o4213" from="papillate" name="relief" src="d0_s14" to="rugose" />
      </biological_entity>
      <biological_entity id="o4212" name="rib" name_original="ribs" src="d0_s14" type="structure" />
      <biological_entity id="o4213" name="angle" name_original="angles" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pappi (rarely 0) usually persistent, usually of fine to coarse, barbellulate to plumose bristles, sometimes of scales (scales often aristate) or awns, sometimes of bristles and scales.</text>
      <biological_entity id="o4214" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o4215" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character is_modifier="true" name="width" src="d0_s15" value="fine" value_original="fine" />
        <character is_modifier="true" name="relief" src="d0_s15" value="coarse" value_original="coarse" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="barbellulate" value_original="barbellulate" />
        <character is_modifier="true" name="shape" src="d0_s15" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity id="o4216" name="scale" name_original="scales" src="d0_s15" type="structure" />
      <biological_entity id="o4217" name="awn" name_original="awns" src="d0_s15" type="structure" />
      <biological_entity id="o4218" name="bristle" name_original="bristles" src="d0_s15" type="structure" />
      <biological_entity id="o4219" name="scale" name_original="scales" src="d0_s15" type="structure" />
      <relation from="o4214" id="r332" modifier="usually" name="consists_of" negation="false" src="d0_s15" to="o4215" />
      <relation from="o4214" id="r333" modifier="sometimes" name="consists_of" negation="false" src="d0_s15" to="o4216" />
      <relation from="o4214" id="r334" modifier="sometimes" name="consists_of" negation="false" src="d0_s15" to="o4217" />
      <relation from="o4214" id="r335" modifier="sometimes" name="consists_of" negation="false" src="d0_s15" to="o4218" />
      <relation from="o4214" id="r336" modifier="sometimes" name="consists_of" negation="false" src="d0_s15" to="o4219" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly subtropics, tropics, and warm-temperate New World, also in Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly subtropics" establishment_means="native" />
        <character name="distribution" value="tropics" establishment_means="native" />
        <character name="distribution" value="and warm-temperate New World" establishment_means="native" />
        <character name="distribution" value="also in Old World" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>187n.</number>
  <discussion>Genera 170, species 2400 (27 genera, 159 species in the flora).</discussion>
  <discussion>In a survey of Compositae, G. Bentham (1873) noted 35 genera and 750 or so species for Eupatorieae; he treated more than 50% of those species as belonging within one genus, Eupatorium. The current view of circumscriptions of most genera within Eupatorieae has stemmed largely from the work of H. Robinson, which was summarized by R. M. King and Robinson (1987), who reported 45 species for Eupatorium (i.e., ca. 2 % of the total species in the tribe).</discussion>
  <discussion>Authors of molecular studies have repeatedly found Eupatorieae to be a coherent clade “nested” within Heliantheae (broad sense) (e.g., R. K. Jansen et al. 1990). Some of those authors have suggested inclusion of Eupatorieae within Heliantheae in the broad sense as a subtribe; others have suggested break-up of Heliantheae into a dozen or so tribes (e.g., J. L. Panero and V. A. Funk 2002).</discussion>
  <references>
    <reference>Gaiser, L. O. Studies in the Kuhniinae (Eupatorieae). II. J. Arnold Arbor. 35: 87–133.</reference>
    <reference>King, R. M. and H. Robinson. 1987. The genera of the Eupatorieae (Asteraceae). Monogr. Syst. Bot. Missouri Bot. Gard. 22.</reference>
    <reference>Schmidt, G. J. and E. E. Schilling. 2000. Phylogeny and biogeography of Eupatorium (Asteraceae: Eupatorieae) based on nuclear ITS sequence data. Amer. J. Bot. 87: 716–726.</reference>
    <reference>Schilling, E. E., J. L. Panero, and P. B. Cox. 1999. Chloroplast DNA restriction site data support a narrowed interpretation of Eupatorium (Asteraceae). Pl. Syst. Evol. 219: 209–223.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres narrowly cylindric, (1–)2–3 mm diam.; phyllaries 4 or 5(–6) in ± 1–2 series; florets 4 or 5(–6)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres campanulate, cylindric, ellipsoid, hemispheric, or obconic, (2–)3–7(–25) mm diam.; phyllaries (5–)8–45(–65+) in (1–)2–8+ series; florets (3–)10–125(–200+).</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Subshrubs or shrubs; phyllaries 5(–6); florets 5(–6)</description>
      <determination>397 Stevia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Vines; phyllaries 4; florets 4</description>
      <determination>417 Mikania</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cypselae 8–10-ribbed</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cypselae (3–)4–5(–8)-ribbed</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pappi of 0–5+, muticous, erose, lacerate, or lanceolate to subulate scales (1–4 mm) plus [5–]9–12+, aristate scales (10–15 mm)</description>
      <determination>398 Carphochaete</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pappi of 10–100+ bristles</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves basal or basal and cauline (cauline mostly sessile)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves mostly cauline (at flowering; mostly petiolate, sometimes sessile)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads usually in spiciform or racemiform, rarely corymbiform or thyrsiform, arrays; receptacles epaleate; pappi of 12–40 coarsely barbellate to plumose bristles</description>
      <determination>410 Liatris</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads in corymbiform to paniculiform arrays; receptacles sometimes (at least partially) paleate; pappi of 35–40 barbellulate to barbellate (subequal) bristles</description>
      <determination>411 Carphephorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves all or mostly alternate (at flowering)</description>
      <determination>412 Garberia</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves all or mostly opposite</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades deltate, lance-elliptic, lance-linear, lanceolate, lance-ovate, lance-rhombic, linear, oblong, obovate, ovate, rhombic-ovate, spatulate, or suborbiculate, margins crenate, dentate, entire, laciniate-dentate, lobed, or serrate; style bases enlarged, hairy</description>
      <determination>403 Brickellia</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades linear (distal sometimes scalelike), margins entire; style bases not enlarged, glabrous</description>
      <determination>406 Asanthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Pappi usually 0 or of 2–6(–12), muticous or aristate to subulate scales plus 0–6(–12), setiform scales or bristles, rarely coroniform (Ageratum) or of 1–5 ± glandular setae (Hartwrightia)</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Pappi of (5–)10–80+ barbellulate, barbellate, or plumose bristles or setiform scales</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Pappi usually 0, rarely 1–5 ± glandular setae (Hartwrightia)</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Pappi usually of 2–6(–12) muticous or aristate to subulate scales plus 0–6(–12),setiform scales or bristles, rarely coroniform (Ageratum)</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaves basal and cauline, mostly alternate; cypselae obpyramidal (gland-dotted)</description>
      <determination>413 Hartwrightia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaves cauline, all or mostly opposite; cypselae prismatic (not gland-dotted)</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Heads in dense to open, cymiform or corymbiform arrays; phyllaries 30–40; style branches ± linear to clavate (distally dilated)</description>
      <determination>396 Ageratum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Heads in tight, corymbiform to subcapitate arrays or borne singly; phyllaries 10–30; style branches ± filiform or linear-filiform (little, if at all, distally dilated)</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves sessile; phyllaries not notably nerved; receptacles epaleate; stylebases not enlarged</description>
      <determination>400 Shinnersia</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves petiolate or sessile; phyllaries 2- or 3-nerved; receptacles paleate(paleae similar to inner phyllaries); style bases enlarged</description>
      <determination>402 Isocarpha</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Phyllaries unequal; receptacles flat to convex (not warty)</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Phyllaries ± equal; receptacles convex to conic or hemispheric (sometimes warty)</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaves mostly sessile (or nearly so), blades linear; cypselae ± fusiform</description>
      <determination>407 Malperia</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaves petiolate, blades ovate, deltate, or rhombic to lanceolate; cypselae prismatic</description>
      <determination>408 Pleurocoronis</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaves whorled (4 or 6 per node), blades linear; heads borne singly</description>
      <determination>401 Sclerolepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaves mostly opposite (distal sometimes alternate), blades elliptic, lanceolate, or oblong; heads usually in cymiform to corymbiform arrays, sometimes borne singly</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves petiolate; involucres 3–6 mm diam.; phyllaries usually 2-nerved; pappi usually of 5–6 aristate scales, rarely coroniform</description>
      <determination>396 Ageratum</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves sessile; involucres 3–4(–5) mm diam.; phyllaries obscurely 3–4-nerved; pappi of 2–6 setiform scales</description>
      <determination>399 Trichocoronis</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Involucres cylindric (3–4+ mm diam); pappus bristles plumose (basally coherent or connate, falling together or in groups)</description>
      <determination>409 Carminatia</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Involucres usually obconic to hemispheric, sometimes campanulate, cylindric, or ellipsoid (2–7 mm diam.); pappus bristles smooth to barbellulate or barbellate (not plumose)</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Phyllaries ± equal</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Phyllaries unequal (outer shorter)</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Receptacles conic</description>
      <determination>394 Conoclinium</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Receptacles flat or convex</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Phyllaries 2- or 3-nerved, or not notably nerved, or pinnately nerved; style bases usually puberulent (glabrous in Eupatorium capillifolium); cypselae usually gland-dotted</description>
      <determination>392 Eupatorium</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Phyllaries 3-nerved, or 0- or 2-nerved; style bases glabrous; cypselae sometimes gland-dotted</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Involucres 2–3 mm diam.; phyllaries 7–16 in 1–2 series; florets 3–13</description>
      <determination>415 Koanophyllon</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Involucres 3–6 mm diam.; phyllaries ca. 30 in 2–3 series; florets 10–60</description>
      <determination>418 Ageratina</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Style bases usually puberulent (glabrous in Eupatorium capillifolium); cypselae usually glabrous and gland-dotted, sometimes scabrellous on ribs</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Style bases usually glabrous (hirsute in Flyriella); cypselae glabrous or hirsute, hirtellous, hispidulous, hispidulo-strigose, puberulent, or scabrellous (sometimes gland-dotted)</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves mostly opposite (sometimes whorled, distal sometimes alternate)</description>
      <determination>392 Eupatorium</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves mostly whorled (3–7 per node), rarely opposite</description>
      <determination>393 Eutrochium</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Annuals or perennials; involucres 2–5+ mm diam.; florets 10–30</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Perennials, subshrubs, or shrubs; involucres (2–)4–7 mm diam.; florets (3–)25–50</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Perennials, 20–60 cm (viscid); corollas white to ochroleucous, throats ± cylindric (± contracted distally, lengths 4–6 times diams.)</description>
      <determination>405 Flyriella</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Annuals or perennials, 30–120+ cm (not viscid, stems usually puberulent, hairs curled); corollas bluish, pinkish, purplish, or white, throats funnelform (not contracted distally, lengths 2.5–4 times diams.)</description>
      <determination>414 Fleischmannia</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Phyllaries usually readily falling, 18–65+ in 4–6+ series, 3–5-nerved; cypselae (3–)5-ribbed, scabrellous, usually gland-dotted</description>
      <determination>416 Chromolaena</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Phyllaries usually persistent, 7–35 in (1–)2–4 series, 2- or 4-nerved, 3-nerved, or obscurely nerved; cypselae 5(–7)-ribbed, hispidulous, hispidulo-strigose, puberulent, or sparsely scabrellous (sometimes gland-dotted)</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Phyllaries 2- or 4-nerved; corollas white to yellowish white; pappi readily falling or fragile</description>
      <determination>404 Brickelliastrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Phyllaries 3-nerved or obscurely nerved; corollas usually blue, lavender, or pinkish, sometimes white; pappi persistent</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Involucres 5–7 mm diam.; phyllaries 30–35; florets 30–50</description>
      <determination>395 Tamaulipa</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Involucres 2–3 mm diam.; phyllaries 7–16; florets 3–13</description>
      <determination>415 Koanophyllon</determination>
    </key_statement>
  </key>
</bio:treatment>