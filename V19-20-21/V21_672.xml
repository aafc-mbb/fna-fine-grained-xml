<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="treatment_page">275</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">calycadenia</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 100. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus calycadenia;species fremontii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066275</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calycadenia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">ciliosa</taxon_name>
    <taxon_hierarchy>genus Calycadenia;species ciliosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calycadenia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">elegans</taxon_name>
    <taxon_hierarchy>genus Calycadenia;species elegans;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calycadenia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">pauciflora</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">elegans</taxon_name>
    <taxon_hierarchy>genus Calycadenia;species pauciflora;variety elegans;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–100 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>self-incompatible.</text>
      <biological_entity id="o2946" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="self-incompatible" value_original="self-incompatible" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or branched (main axes usually obvious, branches relatively few, ± rigid), hispidulous to strigose (especially proximally, usually strigose with scattered, stouter hairs distally).</text>
      <biological_entity id="o2947" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="hispidulous" name="pubescence" src="d0_s2" to="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly alternate, 2–8 cm (proximal), hispid to hispidulous and ± long-hairy (especially on margins and adaxial faces).</text>
      <biological_entity id="o2948" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s3" to="hispidulous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="long-hairy" value_original="long-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in open, spiciform arrays (1–2+ per node).</text>
      <biological_entity id="o2949" name="head" name_original="heads" src="d0_s4" type="structure">
        <character constraint="in open , spiciform arrays" is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in open , spiciform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncular bracts narrowly elliptic to oblanceolate, 2–10 mm (hispidulous, ± bristly and/or strongly pectinate-fimbriate) apices rounded or truncate, tack-glands 1–5+.</text>
      <biological_entity constraint="peduncular" id="o2950" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2951" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o2952" name="tack-gland" name_original="tack-glands" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 3–7 mm, abaxial faces ± hispidulous, ± bristly, shaggy long-hairy on distal margins, tack-glands (0–) 1–5+.</text>
      <biological_entity id="o2953" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2954" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="shaggy" value_original="shaggy" />
        <character constraint="on distal margins" constraintid="o2955" is_modifier="false" name="pubescence" src="d0_s6" value="long-hairy" value_original="long-hairy" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2955" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o2956" name="tack-gland" name_original="tack-glands" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae 3–7 mm (receptacular cups ± campanulate, lengths often ± equaling diams.).</text>
      <biological_entity id="o2957" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets (1–) 2–6;</text>
      <biological_entity id="o2958" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s8" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white or cream to pinkish, or yellow (sometimes basally reddish), tubes 1–2 mm (± papillate, throats sometimes with dark red “eyes”), laminae 4.5–7 mm (central lobes ± equaling or narrower than laterals, mostly obovate, widest beyond middles, sinuses nearly equaling laminae).</text>
      <biological_entity id="o2959" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s9" to="pinkish or yellow" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s9" to="pinkish or yellow" />
      </biological_entity>
      <biological_entity id="o2960" name="tube" name_original="tubes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2961" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets (4–) 6–21;</text>
      <biological_entity id="o2962" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white to pinkish, or yellow, 4–5 mm.</text>
      <biological_entity id="o2963" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pinkish or yellow" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pinkish or yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae 2.5–4 mm, smooth, glabrous or glabrate.</text>
      <biological_entity constraint="ray" id="o2964" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc cypselae 2–4 mm (often ± angular), appressed-hairy;</text>
      <biological_entity constraint="disc" id="o2965" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 8–14 long-aristate and short-blunt, fimbriate scales 1–3 mm (alternating).</text>
      <biological_entity id="o2967" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s14" to="14" />
        <character is_modifier="true" name="shape" src="d0_s14" value="long-aristate" value_original="long-aristate" />
        <character is_modifier="true" name="shape" src="d0_s14" value="short-blunt" value_original="short-blunt" />
        <character is_modifier="true" name="shape" src="d0_s14" value="fimbriate" value_original="fimbriate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o2966" id="r222" name="consist_of" negation="false" src="d0_s14" to="o2967" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 12.</text>
      <biological_entity id="o2966" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o2968" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, dry meadows, hillsides, gravelly outwashes, rocky barrens in chaparral associations</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry meadows" modifier="open" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="gravelly outwashes" />
        <character name="habitat" value="rocky barrens" constraint="in chaparral associations" />
        <character name="habitat" value="chaparral associations" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Calycadenia fremontii is variable and not easily divisible. It ranges from Siskiyou County through Mendocino, Lake, and Butte counties in California with one outlying population near Grants Pass in southern Oregon. Genetic change and chromosome repatterning have produced an extremely complex taxon that (as here circumscribed) includes populations with individuals to 20 cm and extensively branched with white corollas as well as populations with individuals to 100 cm and unbranched or with relatively few branches with yellow corollas. Some relatively small forms with white corollas are more closely related to tall forms with yellow corollas than they are to each other. At present, there is no known combination of characters that allows reasonably consistent delimitation of these or other elements within this assemblage of populations. The present circumscription combines three previously described species, including one traditionally considered an element of C. pauciflora. Additionally, C. fremontii intergrades with C. pauciflora (see discussion under 10. C. pauciflora).</discussion>
  
</bio:treatment>