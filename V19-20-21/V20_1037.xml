<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Staci Markos,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="treatment_page">450</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="D. D. Keck" date="1956" rank="genus">BENITOA</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>8: 26. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus BENITOA</taxon_hierarchy>
    <other_info_on_name type="etymology">For San Benito County, California, alluding to distribution</other_info_on_name>
    <other_info_on_name type="fna_id">103788</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, mostly 10–100 cm (taprooted).</text>
      <biological_entity id="o14682" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched distally, stipitate-glandular (at least distally).</text>
      <biological_entity id="o14683" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline (at flowering);</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or petiolate (bases of blades ± decurrent onto petioles);</text>
      <biological_entity id="o14684" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1 (–3+) -nerved, oblanceolate to linear (sessile, smaller, bractlike distally), margins entire or nearly so, faces stipitate-glandular.</text>
      <biological_entity id="o14685" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1(-3+)-nerved" value_original="1(-3+)-nerved" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o14686" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o14687" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in open, corymbiform to paniculiform arrays.</text>
      <biological_entity id="o14688" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in arrays" constraintid="o14689" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in open , corymbiform to paniculiform arrays" />
      </biological_entity>
      <biological_entity id="o14689" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± campanulate to turbinate or fusiform, (8–10 ×) 3–5 mm.</text>
      <biological_entity id="o14690" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s7" to="turbinate or fusiform" />
        <character char_type="range_value" from="[8" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="]3" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 22–35+ in (4–) 5–6+ series, 1-nerved (flat), lanceolate to linear, unequal, herbaceous to ± cartilaginous, margins scarious, apices (slightly spreading to nearly squarrose) some or all bearing a tack-shaped gland.</text>
      <biological_entity id="o14691" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o14692" from="22" name="quantity" src="d0_s8" to="35" upper_restricted="false" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s8" to="more or less cartilaginous" />
      </biological_entity>
      <biological_entity id="o14692" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14693" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o14694" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o14695" name="gland" name_original="gland" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="tack--shaped" value_original="tack--shaped" />
      </biological_entity>
      <relation from="o14694" id="r1343" name="bearing a" negation="false" src="d0_s8" to="o14695" />
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, pitted, epaleate.</text>
      <biological_entity id="o14696" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 5–8 (–13+), pistillate, fertile;</text>
      <biological_entity id="o14697" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="13" upper_restricted="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow (occasionally suffused with red, becoming revolute).</text>
      <biological_entity id="o14698" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 9–20+, functionally staminate;</text>
      <biological_entity id="o14699" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s12" to="20" upper_restricted="false" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes about equaling funnelform to campanulate throats, lobes 5, erect, lance-deltate;</text>
      <biological_entity id="o14700" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14701" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="to throats" constraintid="o14702" is_modifier="false" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o14702" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o14703" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lance-deltate" value_original="lance-deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages lanceolate.</text>
      <biological_entity constraint="style-branch" id="o14704" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (mottled purple-brown) ± plumply clavate, ± triquetrous, 3-nerved, faces sericeous;</text>
      <biological_entity id="o14705" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less plumply" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="triquetrous" value_original="triquetrous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-nerved" value_original="3-nerved" />
      </biological_entity>
      <biological_entity id="o14706" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi readily falling (fragile), of 2–8 whitish, subulate, barbellate scales (flattened bristles) in 1 series.</text>
      <biological_entity id="o14708" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s16" to="8" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="shape" src="d0_s16" value="subulate" value_original="subulate" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o14709" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o14707" id="r1344" name="consist_of" negation="false" src="d0_s16" to="o14708" />
      <relation from="o14708" id="r1345" name="in" negation="false" src="d0_s16" to="o14709" />
    </statement>
    <statement id="d0_s17">
      <text>x = 5.</text>
      <biological_entity id="o14707" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s16" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o14710" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>207.</number>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Markos, S. and B. G. Baldwin. 2001. Higher-level relationships and major lineages of Lessingia (Compositae, Astereae) based on nuclear rDNA internal and external transcribed spacer (ITS and ETS) sequences. Syst. Bot. 26: 168–183.</reference>
  </references>
  
</bio:treatment>