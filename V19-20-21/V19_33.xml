<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">echinops</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">ritro</taxon_name>
    <taxon_name authority="(M. Bieberstein) Nyman" date="1879" rank="subspecies">ruthenicus</taxon_name>
    <place_of_publication>
      <publication_title>Consp. Fl. Eur.</publication_title>
      <place_in_publication>2: 399. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus echinops;species ritro;subspecies ruthenicus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250068267</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinops</taxon_name>
    <taxon_name authority="M. Bieberstein" date="unknown" rank="species">ruthenicus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Taur.-Caucas.</publication_title>
      <place_in_publication>3: 597. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinops;species ruthenicus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 100–150 cm.</text>
      <biological_entity id="o1787" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or distally ± branched, ± white-tomentose, proximally sometimes ± glabrate.</text>
      <biological_entity id="o1788" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="distally more or less" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="proximally sometimes more or less" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline winged-petiolate, mid cauline ± clasping;</text>
      <biological_entity id="o1789" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal and proximal cauline" id="o1790" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
      <biological_entity constraint="mid cauline" id="o1791" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades elliptic to obovate, margins 2-pinnately divided nearly to midvein, lobes linear to oblong, spiny-serrate, spine-tipped, abaxial faces white-tomentose, adaxial faces green, sparsely viscid-glandular;</text>
      <biological_entity id="o1792" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1793" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="obovate" />
      </biological_entity>
      <biological_entity id="o1794" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="to midvein" constraintid="o1795" is_modifier="false" modifier="2-pinnately" name="shape" src="d0_s3" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o1795" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity id="o1796" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spiny-serrate" value_original="spiny-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1797" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1798" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s3" value="viscid-glandular" value_original="viscid-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal smaller, less divided.</text>
      <biological_entity id="o1799" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o1800" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="less" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Secondary heads 3.5–4.5 cm diam.</text>
      <biological_entity constraint="secondary" id="o1801" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="diameter" src="d0_s5" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 15–20 mm.</text>
      <biological_entity id="o1802" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inner phyllaries lavender, apices attenuate, serrulate, scabrous, not glandular.</text>
      <biological_entity constraint="inner" id="o1803" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration_or_odor" src="d0_s7" value="lavender" value_original="lavender" />
      </biological_entity>
      <biological_entity id="o1804" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas blue to purple (white).</text>
      <biological_entity id="o1805" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s8" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae ca. 6 mm;</text>
      <biological_entity id="o1806" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of ± connate, minutely barbed scales ca. 1 mm. 2n = 32 (Greece).</text>
      <biological_entity id="o1807" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity id="o1808" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="true" modifier="minutely" name="architecture_or_shape" src="d0_s10" value="barbed" value_original="barbed" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1809" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="32" value_original="32" />
      </biological_entity>
      <relation from="o1807" id="r173" name="consists_of" negation="false" src="d0_s10" to="o1808" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.Y., Wash.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  <other_name type="common_name">Southern globe-thistle</other_name>
  <discussion>Subspecies ruthenicus sometimes escapes from cultivation.</discussion>
  
</bio:treatment>