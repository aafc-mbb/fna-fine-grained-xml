<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="mention_page">204</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="treatment_page">203</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ASTRANTHIUM</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 312. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ASTRANTHIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek astron, star, and anthos, flower, alluding to head as seen from above</other_info_on_name>
    <other_info_on_name type="fna_id">102980</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, 5–50 cm (with taproots or fibrous-roots).</text>
      <biological_entity id="o18007" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decumbent, usually branched throughout, sparsely to densely strigose to hirsuto-pilose.</text>
      <biological_entity id="o18010" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" modifier="usually; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="strigose" modifier="sparsely to densely; densely" name="pubescence" src="d0_s1" to="hirsuto-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>mostly petiolate;</text>
      <biological_entity id="o18011" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-nerved, spatulate-obovate (proximal) or narrower, 1–8 (–12) cm (some clasping to subclasping), margins usually entire, sometimes toothed, faces glabrous or strigose.</text>
      <biological_entity id="o18012" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate-obovate" value_original="spatulate-obovate" />
        <character is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18013" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o18014" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly (usually on long, leafless or bracteate peduncles).</text>
      <biological_entity id="o18015" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate or hemispheric, (3.5–8 ×) 4–8 mm.</text>
      <biological_entity id="o18016" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="[3.5" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="]4" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 15–30 in 2 (–3) series, appressed, midnerves inconspicuous, broadly oblanceolate to linear-lanceolate, subequal, thin-herbaceous, margins often hyaline, faces glabrous or strigose.</text>
      <biological_entity id="o18017" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o18018" from="15" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o18018" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o18019" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="broadly oblanceolate" name="shape" src="d0_s8" to="linear-lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="thin-herbaceous" value_original="thin-herbaceous" />
      </biological_entity>
      <biological_entity id="o18020" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o18021" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles conic, pitted, epaleate.</text>
      <biological_entity id="o18022" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 10–65 (–85), pistillate, fertile;</text>
      <biological_entity id="o18023" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="65" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="85" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="65" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas abaxially drying white or with blue to purplish blue midstripe, adaxially white.</text>
      <biological_entity id="o18024" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="abaxially" name="condition" src="d0_s11" value="drying" value_original="drying" />
        <character constraint="with midstripe" constraintid="o18025" is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="with blue to purplish blue midstripe" />
        <character is_modifier="false" modifier="adaxially" name="coloration" notes="" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o18025" name="midstripe" name_original="midstripe" src="d0_s11" type="structure">
        <character char_type="range_value" from="blue" is_modifier="true" name="coloration" src="d0_s11" to="purplish blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 25–200 [–250], bisexual, fertile;</text>
      <biological_entity id="o18026" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="200" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="250" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="200" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes much shorter than tubular-funnelform throats, lobes 5, spreading, deltate;</text>
      <biological_entity id="o18027" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o18028" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than tubular-funnelform throats" constraintid="o18029" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o18029" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="tubular-funnelform" value_original="tubular-funnelform" />
      </biological_entity>
      <biological_entity id="o18030" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages lanceolate-acute.</text>
      <biological_entity constraint="style-branch" id="o18031" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate-acute" value_original="lanceolate-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (brownish) obovoid to oblanceoloid-obovoid, compressed, 2-nerved (nerves marginal, usually thin, sometimes thick), faces smooth, striate, or papillate-pebbly, and glabrous or glochidiate-hairy;</text>
      <biological_entity id="o18032" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s15" to="oblanceoloid-obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o18033" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s15" value="striate" value_original="striate" />
        <character is_modifier="false" name="relief" src="d0_s15" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glochidiate-hairy" value_original="glochidiate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0 or coroniform (of setae or scales to 0.1 mm).</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 3, 4, 5.</text>
      <biological_entity id="o18034" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o18035" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character name="quantity" src="d0_s17" value="4" value_original="4" />
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>177.</number>
  <other_name type="common_name">Western-daisy</other_name>
  <discussion>Species 12 (3 in the flora).</discussion>
  <discussion>Astranthium is characterized by its herbaceous habit, heads borne singly on long peduncles, subequal phyllaries in 2–3 series, conic receptacles, white or blue rays, tubular disc corollas, and flattened, papillate cypselae with coroniform or no pappi. Apically glochidiate cypsela hairs have each apical cell abruptly reflexed in opposite directions. The species are separated by small but consistent morphologic differences, often accompanied by differences in chromosome number. All but three of the species are known only from Mexico (D. C. D. De Jong 1965; J. Rzedowski 1983).</discussion>
  <discussion>Vegetatively and florally, Astranthium is similar to the monotypic Dichaetophora, which has the same chromosome number (2n = 6) as northern species of Astranthium. Epappose species of Erigeron may be superficially similar to Astranthium (D. C. D. De Jong and G. L. Nesom 1982); they are distantly related.</discussion>
  <references>
    <reference>De Jong, D. C. D. 1965. A systematic study of the genus Astranthium (Compositae, Astereae). Publ. Mus. Michigan State Univ., Biol. Ser. 2: 429–528.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants fibrous-rooted; cypsela faces glabrous or glochidiate-hairy only distally</description>
      <determination>1 Astranthium integrifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants taprooted; cypsela faces uniformly glochidiate-hairy</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray florets (7–)13–24; disc corollas 2–3 mm; cypselae 1–1.6 × 0.6–0.8 mm, faces minutely striate, otherwise nearly smooth, rarely papillate-pebbly</description>
      <determination>2 Astranthium ciliatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray florets 18–35; disc corollas 3.2–3.8 mm; cypselae 1.4–2.1 × 0.7–1.2 mm, faces minutely, glandular-papillate-pebbly</description>
      <determination>3 Astranthium robustum</determination>
    </key_statement>
  </key>
</bio:treatment>