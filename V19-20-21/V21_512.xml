<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">211</other_info_on_meta>
    <other_info_on_meta type="treatment_page">210</other_info_on_meta>
    <other_info_on_meta type="illustration_page">204</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">bidens</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">bipinnata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 832. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus bidens;species bipinnata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200023531</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bidens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bipinnata</taxon_name>
    <taxon_name authority="Sherff" date="unknown" rank="variety">biternatoides</taxon_name>
    <taxon_hierarchy>genus Bidens;species bipinnata;variety biternatoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (15–) 30–100 (–150+) cm.</text>
      <biological_entity id="o15006" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles 20–50 mm;</text>
      <biological_entity id="o15007" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15008" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s1" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades rounded-deltate to ovate or lanceolate overall, (20–) 30–70+ × (20–) 30–60+ mm, (1–) 2 (–3) -pinnatisect, ultimate lobes obovate or lanceolate, 15–45+ × 10–25+ mm, bases truncate to cuneate, ultimate margins entire, sometimes ciliolate, apices rounded to acute or attenuate, faces usually glabrous, sometimes hirtellous.</text>
      <biological_entity id="o15009" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15010" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded-deltate" name="shape" src="d0_s2" to="ovate or lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" src="d0_s2" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="70" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_width" src="d0_s2" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s2" to="60" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="(1-)2(-3)-pinnatisect" value_original="(1-)2(-3)-pinnatisect" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15011" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="45" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o15012" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15013" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o15014" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="acute or attenuate" />
      </biological_entity>
      <biological_entity id="o15015" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly, sometimes in ± corymbiform arrays.</text>
      <biological_entity id="o15016" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o15017" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o15016" id="r1032" modifier="sometimes" name="in" negation="false" src="d0_s3" to="o15017" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles (10–) 20–50 (–100) mm.</text>
      <biological_entity id="o15018" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of (7–) 8 (–10) linear bractlets 3–5 mm, ± appressed, margins ciliate, abaxial faces usually glabrous.</text>
      <biological_entity id="o15019" name="calyculus" name_original="calyculi" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" notes="" src="d0_s5" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o15020" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="atypical_quantity" src="d0_s5" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="10" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="8" value_original="8" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15021" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15022" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o15019" id="r1033" name="consist_of" negation="false" src="d0_s5" to="o15020" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± campanulate, 5–7 × 3–4 (–5) mm.</text>
      <biological_entity id="o15023" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 8–12, lanceolate to linear, 4–6 mm.</text>
      <biological_entity id="o15024" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="12" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0 or 3–5+;</text>
      <biological_entity id="o15025" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae yellowish or whitish, 1–2 (–3) mm.</text>
      <biological_entity id="o15026" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 10–20 (–30+);</text>
      <biological_entity id="o15027" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="30" upper_restricted="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellowish to whitish, 2–3 mm.</text>
      <biological_entity id="o15028" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s11" to="whitish" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae redbrown, outer weakly obcompressed, 7–15 mm, inner ± 4-angled, linear to linear-fusiform, 12–18 mm, margins not ciliate, apices ± attenuate, faces 2-grooved, often tuberculate-hispidulous;</text>
      <biological_entity id="o15029" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity constraint="outer" id="o15030" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s12" value="obcompressed" value_original="obcompressed" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o15031" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="linear-fusiform" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15032" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15033" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o15034" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-grooved" value_original="2-grooved" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s12" value="tuberculate-hispidulous" value_original="tuberculate-hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of (2–) 3–4, erect to divergent, retrorsely barbed awns 2–4 mm. 2n = 24, 72.</text>
      <biological_entity id="o15035" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o15036" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s13" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s13" to="4" />
        <character is_modifier="true" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="true" name="arrangement" src="d0_s13" value="divergent" value_original="divergent" />
        <character is_modifier="true" modifier="retrorsely" name="architecture_or_shape" src="d0_s13" value="barbed" value_original="barbed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15037" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
        <character name="quantity" src="d0_s13" value="72" value_original="72" />
      </biological_entity>
      <relation from="o15035" id="r1034" name="consist_of" negation="false" src="d0_s13" to="o15036" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Aug–Sep(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, forests, disturbed, wettish sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="disturbed" />
        <character name="habitat" value="wettish sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont.; Ala., Ariz., Ark., Conn., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Miss., Mo., Nebr., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va.; South America; Europe; Asia; Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Bidens bipinnata is probably native in eastern Asia and introduced in South America, Europe, Asia, and Pacific Islands.</discussion>
  
</bio:treatment>