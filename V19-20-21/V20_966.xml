<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">419</other_info_on_meta>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">pyrrocoma</taxon_name>
    <taxon_name authority="(Hooker) Greene" date="1894" rank="species">lanceolata</taxon_name>
    <taxon_name authority="(Greene) Mayes ex G. K. Brown &amp; D. J. Keil" date="1992" rank="variety">subviscosa</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>73: 58. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pyrrocoma;species lanceolata;variety subviscosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068674</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrrocoma</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">subviscosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>47: 549. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pyrrocoma;species subviscosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Greene) S. F. Blake" date="unknown" rank="species">subviscosus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species subviscosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–40 cm.</text>
      <biological_entity id="o26555" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, ascending or decumbent, tomentulose, densely stipitate-glandular, especially peduncles.</text>
      <biological_entity id="o26556" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o26557" name="peduncle" name_original="peduncles" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal blades 80–200 × 8–30 mm, faces tomentulose to glabrate, stipitate-glandular.</text>
      <biological_entity id="o26558" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o26559" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s2" to="200" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26560" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="tomentulose" name="pubescence" src="d0_s2" to="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads (1–) 3–7 in paniculiform arrays.</text>
      <biological_entity id="o26561" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o26562" from="3" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity id="o26562" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 2–5 cm.</text>
      <biological_entity id="o26563" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 7–9 × 10–15 mm. 2n = 12.</text>
      <biological_entity id="o26564" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26565" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes, grassy alkaline meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="grassy alkaline meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8b.</number>
  <discussion>Variety subviscosa geographically overlaps with var. lanceolata; it occurs at somewhat higher elevations. It is distinguished mostly by its stipitate glands.</discussion>
  
</bio:treatment>