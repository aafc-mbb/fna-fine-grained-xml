<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="treatment_page">166</other_info_on_meta>
    <other_info_on_meta type="illustration_page">157</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="de Candolle" date="1810" rank="genus">saussurea</taxon_name>
    <taxon_name authority="D. C. Eaton" date="1881" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>6: 283. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus saussurea;species americana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067462</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–120+ cm;</text>
      <biological_entity id="o18485" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rootstocks short, stout;</text>
      <biological_entity id="o18486" name="rootstock" name_original="rootstocks" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>herbage loosely tomentose when young, glabrescent, sometimes ± glandular.</text>
      <biological_entity id="o18487" name="herbage" name_original="herbage" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="sometimes more or less" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems 1–many, leafy, simple or with ascending branches.</text>
      <biological_entity id="o18488" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="with ascending branches" value_original="with ascending branches" />
      </biological_entity>
      <biological_entity id="o18489" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o18488" id="r1672" name="with" negation="false" src="d0_s3" to="o18489" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline, usually more than 20, well distributed, proximal and mid with winged petioles to 6 cm, wings sometimes decurrent 1–2 cm on stems, blades lanceolate to triangular-ovate, 5–15 cm, bases cordate to truncate or tapering, margins sharply dentate, apices acute;</text>
      <biological_entity id="o18490" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="20" modifier="usually" name="quantity" src="d0_s4" upper_restricted="false" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s4" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character constraint="with petioles" constraintid="o18491" is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o18491" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18492" name="wing" name_original="wings" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
        <character char_type="range_value" constraint="on stems" constraintid="o18493" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18493" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <biological_entity id="o18494" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="triangular-ovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18495" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="truncate or tapering" />
      </biological_entity>
      <biological_entity id="o18496" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o18497" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid and distal usually sessile, smaller, narrower, bases tapering.</text>
      <biological_entity constraint="mid and distal" id="o18498" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="size" src="d0_s5" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o18499" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 5–30+ in tight to open corymbiform arrays;</text>
      <biological_entity id="o18501" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s6" value="tight" value_original="tight" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>(peduncles 0–5 cm).</text>
      <biological_entity id="o18500" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o18501" from="5" name="quantity" src="d0_s6" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres 10–15 mm.</text>
      <biological_entity id="o18502" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in ca. 5 series, strongly unequal, outer ± ovate, inner lanceolate, abaxial faces pale green, distally dark purplish to nearly black, loosely tomentose.</text>
      <biological_entity id="o18503" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o18504" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18505" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18506" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18507" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="distally dark purplish" name="coloration" src="d0_s9" to="nearly black" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <relation from="o18503" id="r1673" name="in" negation="false" src="d0_s9" to="o18504" />
    </statement>
    <statement id="d0_s10">
      <text>Receptacles naked.</text>
      <biological_entity id="o18508" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Florets 8–21;</text>
      <biological_entity id="o18509" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas usually pale lavender-blue to dark purple (rarely white), 11–13 mm;</text>
      <biological_entity id="o18510" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="usually pale lavender-blue" name="coloration" src="d0_s12" to="dark purple" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tubes 5–6.5 mm, throats 1.5–2 mm, lobes 3.5–4 mm.</text>
      <biological_entity id="o18511" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18512" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18513" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 4–6 mm;</text>
      <biological_entity id="o18514" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappus bristles brownish, outer 3–7 mm, inner 9–10 mm.</text>
      <biological_entity constraint="pappus" id="o18515" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="position" src="d0_s15" value="outer" value_original="outer" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="inner" value_original="inner" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s15" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist canyons, meadows, streamsides in montane forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist canyons" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="streamsides" constraint="in montane forests" />
        <character name="habitat" value="montane forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska, Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">American saw-wort</other_name>
  <discussion>Saussurea americana is closely related to an Asian species, S. foliosa Ledebour.</discussion>
  
</bio:treatment>