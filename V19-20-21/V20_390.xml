<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="treatment_page">179</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Urbatsch" date="2005" rank="genus">lorandersonia</taxon_name>
    <taxon_name authority="(Cronquist) Urbatsch" date="2005" rank="species">microcephala</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 1622. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus lorandersonia;species microcephala</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067125</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="Cronquist" date="unknown" rank="species">microcephalus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>11: 186. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species microcephalus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tonestus</taxon_name>
    <taxon_name authority="(Cronquist) G. L. Nesom &amp; D. R. Morgan" date="unknown" rank="species">microcephalus</taxon_name>
    <taxon_hierarchy>genus Tonestus;species microcephalus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 5–20 cm (mat-forming);</text>
      <biological_entity id="o18973" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices woody.</text>
      <biological_entity id="o18974" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 3–20+ (erect), green, simple (± ridged), sparsely stipitate-glandular, resinous.</text>
      <biological_entity id="o18975" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="coating" src="d0_s2" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, crowded near base of current-year growth, mostly ascending;</text>
      <biological_entity id="o18976" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character constraint="near base" constraintid="o18977" is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="mostly" name="orientation" notes="" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o18977" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="current-year" id="o18978" name="growth" name_original="growth" src="d0_s3" type="structure" />
      <relation from="o18977" id="r1753" name="part_of" negation="false" src="d0_s3" to="o18978" />
    </statement>
    <statement id="d0_s4">
      <text>blades with midnerves prominent usually plus 2 evident, collateral veins, elliptic to narrowly oblanceolate, 20–40 × 2–3.5 mm, flat to ± concave adaxially, margins entire, scarious, stipitate-glandular, apices acute, faces usually glabrous or stipitate-glandular, adaxially pustulate, gland-dotted and resinous along major nerves.</text>
      <biological_entity id="o18979" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o18980" name="midnerve" name_original="midnerves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="collateral" id="o18981" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="narrowly oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="flat" modifier="adaxially" name="shape" src="d0_s4" to="more or less concave" />
      </biological_entity>
      <biological_entity id="o18982" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o18983" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o18984" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s4" value="pustulate" value_original="pustulate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
        <character constraint="along nerves" constraintid="o18985" is_modifier="false" name="coating" src="d0_s4" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o18985" name="nerve" name_original="nerves" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
      </biological_entity>
      <relation from="o18979" id="r1754" name="with" negation="false" src="d0_s4" to="o18980" />
    </statement>
    <statement id="d0_s5">
      <text>Heads in corymbiform arrays 1–4 cm wide.</text>
      <biological_entity id="o18986" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o18987" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <relation from="o18986" id="r1755" name="in" negation="false" src="d0_s5" to="o18987" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres obconic to subhemispheric, 6–8 mm.</text>
      <biological_entity id="o18988" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s6" to="subhemispheric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 24–30 in 3–4 series, green to tan, midnerves evident, ovate or lanceolate to elliptic, 3.5–7 × 0.7–1.3 mm, subequal, herbaceous or herbaceous-tipped (outer), usually chartaceous (inner), usually flat to convex, margins narrowly scarious, sometimes sparsely ciliate, apices attenuate to cuspidate (outer), acuminate to acute (inner), faces glabrous, resinous.</text>
      <biological_entity id="o18989" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o18990" from="24" name="quantity" src="d0_s7" to="30" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s7" to="tan" />
      </biological_entity>
      <biological_entity id="o18990" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o18991" name="midnerve" name_original="midnerves" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="evident" value_original="evident" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="elliptic" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="herbaceous-tipped" value_original="herbaceous-tipped" />
        <character is_modifier="false" modifier="usually" name="pubescence_or_texture" src="d0_s7" value="chartaceous" value_original="chartaceous" />
        <character char_type="range_value" from="usually flat" name="shape" src="d0_s7" to="convex" />
      </biological_entity>
      <biological_entity id="o18992" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18993" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s7" to="cuspidate acuminate" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s7" to="cuspidate acuminate" />
      </biological_entity>
      <biological_entity id="o18994" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s7" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles without scales.</text>
      <biological_entity id="o18995" name="receptacle" name_original="receptacles" src="d0_s8" type="structure" />
      <biological_entity id="o18996" name="scale" name_original="scales" src="d0_s8" type="structure" />
      <relation from="o18995" id="r1756" name="without" negation="false" src="d0_s8" to="o18996" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 5–8;</text>
      <biological_entity id="o18997" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae elliptic to narrowly obovate, 3.5–5 × ca. 1 mm.</text>
      <biological_entity id="o18998" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="narrowly obovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 10–15;</text>
      <biological_entity id="o18999" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 4.3–4.7 mm, glabrous, lobes 0.5–1 mm;</text>
      <biological_entity id="o19000" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19001" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style-branches exserted, ca. 2.5 mm, appendages lanceolate, ca. 1.7 mm.</text>
      <biological_entity id="o19002" name="branch-style" name_original="style-branches" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character name="distance" src="d0_s13" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity id="o19003" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1.7" value_original="1.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae tan, oblong, 1.5–2.3 mm, 5–7 ribbed, faces moderately hairy;</text>
      <biological_entity id="o19004" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s14" to="7" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o19005" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi tan (fine, barbellate), 3–5 mm. 2n = 18.</text>
      <biological_entity id="o19006" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="tan" value_original="tan" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19007" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thin soils or cracks on granite outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thin soils" constraint="on granite outcrops" />
        <character name="habitat" value="cracks" constraint="on granite outcrops" />
        <character name="habitat" value="granite outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2400–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="2400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Smallhead heathgoldenrod</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>