<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>W. Dennis Clark</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="treatment_page">445</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Greene" date="1887" rank="genus">HAZARDIA</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 28. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus HAZARDIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Barclay Hazard, 1852–1938, amateur botanist from Santa Barbara, California</other_info_on_name>
    <other_info_on_name type="fna_id">114772</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Cassini" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall" date="unknown" rank="section">Hazardia</taxon_name>
    <taxon_hierarchy>genus Haplopappus;section Hazardia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, subshrubs, or shrubs, 20–250 cm (root crowns woody).</text>
      <biological_entity id="o3547" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o3548" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually much branched, glabrous or lanate-tomentose to scabrid or short-villous.</text>
      <biological_entity id="o3550" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="lanate-tomentose" name="pubescence" src="d0_s1" to="scabrid or short-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline (deciduous);</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>subpetiolate, subsessile, or sessile;</text>
      <biological_entity id="o3551" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-nerved, oblong, oblanceolate, elliptic, obovate-cuneate (often coriaceous, bases usually clasping or subclasping), margins entire or toothed, faces glabrous or densely tomentose, sometimes gland-dotted, resinous.</text>
      <biological_entity id="o3552" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate-cuneate" value_original="obovate-cuneate" />
      </biological_entity>
      <biological_entity id="o3553" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o3554" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, disciform, or discoid, in spiciform, racemiform, narrowly paniculiform, thyrsiform, or cymiform arrays, rarely borne singly (sessile or short-pedunculate).</text>
      <biological_entity id="o3555" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o3556" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" modifier="narrowly" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="thyrsiform" value_original="thyrsiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o3555" id="r321" name="in" negation="false" src="d0_s6" to="o3556" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to turbinate or campanulate, (5–20 ×) 3–12 mm.</text>
      <biological_entity id="o3557" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="turbinate or campanulate" />
        <character char_type="range_value" from="[5" from_unit="mm" name="length" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="]3" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 20–60 in 5–9 series, recurved or erect, 1-nerved (keeled), oblong, linear-oblong, or linear-lanceolate, unequal, herbaceous distally, margins not scarious, faces stipitate-glandular (distally).</text>
      <biological_entity id="o3558" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3559" from="20" name="quantity" src="d0_s8" to="60" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="distally" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o3559" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
      <biological_entity id="o3560" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o3561" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, shallowly pitted, epaleate.</text>
      <biological_entity id="o3562" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="shallowly" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 or 3–25, pistillate, fertile or sterile;</text>
      <biological_entity id="o3563" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="25" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow or drying red-purple.</text>
      <biological_entity id="o3564" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red-purple" value_original="red-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 4–30, bisexual, fertile;</text>
      <biological_entity id="o3565" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="30" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, sometimes becoming red-purple, gradually ampliate from middle, tubes longer than narrowly tubular throats, lobes 5, erect, spreading, triangular;</text>
      <biological_entity id="o3566" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes becoming" name="coloration_or_density" src="d0_s13" value="red-purple" value_original="red-purple" />
        <character constraint="from middle" constraintid="o3567" is_modifier="false" modifier="gradually" name="size" src="d0_s13" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o3567" name="middle" name_original="middle" src="d0_s13" type="structure" />
      <biological_entity id="o3568" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than narrowly tubular throats" constraintid="o3569" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3569" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s13" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o3570" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages usually triangular (linear in H. whitneyi).</text>
      <biological_entity constraint="style-branch" id="o3571" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae fusiform to deltoid, subterete to compressed, 4–5-nerved, faces glabrous, canescent, or densely silky;</text>
      <biological_entity id="o3572" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="deltoid subterete" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="deltoid subterete" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="4-5-nerved" value_original="4-5-nerved" />
      </biological_entity>
      <biological_entity id="o3573" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="canescent" value_original="canescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="canescent" value_original="canescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="silky" value_original="silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 20–30 reddish-brown, fine, smooth bristles in 1–2 series.</text>
      <biological_entity id="o3575" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s16" to="30" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="true" name="width" src="d0_s16" value="fine" value_original="fine" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o3576" name="series" name_original="series" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s16" to="2" />
      </biological_entity>
      <relation from="o3574" id="r322" name="consist_of" negation="false" src="d0_s16" to="o3575" />
      <relation from="o3575" id="r323" name="in" negation="false" src="d0_s16" to="o3576" />
    </statement>
    <statement id="d0_s17">
      <text>x = 5 (4, 6).</text>
      <biological_entity id="o3574" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3577" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character name="quantity" src="d0_s17" value="[4" value_original="[4" />
        <character name="quantity" src="d0_s17" value="6]" value_original="6]" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>206.</number>
  <other_name type="common_name">Bristleweed</other_name>
  <discussion>Species 13 (7 in the flora).</discussion>
  <discussion>Hazardia is restricted to the Pacific coast area of North America from Oregon through California and southern Nevada to Baja California Sur. W. D. Clark (1979) recognized three sections comprising 13 species, six of which are endemic to Mexico. All species have a chromosome number of 2n = 10 except two: H. brickellioides (2n = 12) and H. whitneyi (2n = 8). Species of Hazardia are variable in vestiture, arrays, head shape, size, and biology (ray florets fertile or sterile).</discussion>
  <discussion>W. D. Clark (1979) treated Hazardia as a segregate of Haplopappus de Candolle; he observed that South American species of Haplopappus probably are the closest relatives of Hazardia. Haplopappus in South America (x = 5; ca. 70 species in 4 sections) has a range of variation similar to that of Hazardia. Haplopappus sect. Haplopappus and sect. Gymnocoma Nuttall are closely related to each other, forming the core of the genus in its strict sense; sections Polyphylla H. M. Hall and Xylolepis H. M. Hall, however, may be congeneric with Hazardia, based on morphologic and chemical evidence (Clark; G. K. Brown and Clark 1981, 1982). R. C. Jackson (1979) and J. Grau (1976), more conservatively, have viewed Hazardia as congeneric with the South American Haplopappus. Jackson would further include the species of Hall’s Haplopappus sect. Blepharodon [= Xanthisma sect. Blepharodon (de Candolle) D. R. Morgan &amp; R. L. Hartman (D. R. Morgan and R. L. Hartman 2003)] within this widely defined Haplopappus.</discussion>
  <references>
    <reference>Clark, W. D. 1979. The taxonomy of Hazardia (Compositae: Astereae). Madroño 26: 105–127.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials or subshrubs</description>
      <determination>1 Hazardia whitneyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads discoid</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads radiate or disciform</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves 15–25 × 5–12 mm, faces glabrous; phyllaries stramineous, glabrous except minutely gland-dotted at tips; florets 4–8(–10)</description>
      <determination>2 Hazardia stenolepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves 13–35(–50) × 5–17(–24) mm, faces adaxially glabrous or sparsely puberulent; phyllaries with prominent green apical area, prominently stipitate-glandular; florets 9–30</description>
      <determination>3 Hazardia squarrosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves entire; ray corollas longer than involucres, conspicuous (heads radiate)</description>
      <determination>4 Hazardia orcuttii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves subentire to prominently toothed; ray corollas shorter than involucres, inconspicuous (heads disciform)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants 20–80 cm; leaf faces pilose to scabrous</description>
      <determination>5 Hazardia brickellioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants 60–250 cm; leaf faces densely tomentose, at least abaxially</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves thin, adaxial faces glabrate or glabrescent; phyllaries loosely woolly-tufted apically; disc corollas 5–8 mm</description>
      <determination>6 Hazardia cana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves subcoriaceous, adaxial faces densely short-tomentose; phyllaries densely woolly; disc corollas 8–10 mm</description>
      <determination>7 Hazardia detonsa</determination>
    </key_statement>
  </key>
</bio:treatment>