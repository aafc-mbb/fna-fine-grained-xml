<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">bidens</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Britton" date="1893" rank="species">discoidea</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>20: 281. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus bidens;species discoidea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416176</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">discoidea</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 339. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Coreopsis;species discoidea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (10–) 20–60 (–180) cm.</text>
      <biological_entity id="o19422" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles 10–40 (–60) mm;</text>
      <biological_entity id="o19423" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19424" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades deltate to lanceovate overall, 30–80 (–100+) × 10–30 (–80+) mm, usually 3-foliolate, leaflets petiolulate, lanceovate to lanceolate, (10–) 20–50 (–100) × 5–20 (–40) mm, bases cuneate, ultimate margins usually serrate, sometimes ciliate, apices acuminate to attenuate, faces glabrous or hirtellous.</text>
      <biological_entity id="o19425" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19426" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s2" to="lanceovate" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="100" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="80" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="80" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
      <biological_entity id="o19427" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolulate" value_original="petiolulate" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s2" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19428" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19429" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19430" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s2" to="attenuate" />
      </biological_entity>
      <biological_entity id="o19431" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly, sometimes in 2s or 3s.</text>
      <biological_entity id="o19432" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 10–20 (–50+) mm.</text>
      <biological_entity id="o19433" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="50" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of (3–) 4 (–5+) ± appressed, spatulate to linear, seldom foliaceous bractlets or bracts (3–) 12–25 mm, margins seldom ciliate, abaxial faces usually glabrous.</text>
      <biological_entity id="o19434" name="calyculus" name_original="calyculi" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" modifier="of (3-)4(-5+) more or less appressed" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o19435" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="seldom" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19436" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="seldom" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19437" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="seldom" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19438" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± hemispheric, 4–5 (–7) × 4–6 (–9) mm.</text>
      <biological_entity id="o19439" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 5–7, oblong to lanceolate, 4–6+ mm.</text>
      <biological_entity id="o19440" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0.</text>
      <biological_entity id="o19441" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets (10–) 15–20+;</text>
      <biological_entity id="o19442" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s9" to="15" to_inclusive="false" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="20" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas ± orange, 1.5–2 mm.</text>
      <biological_entity id="o19443" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae blackish to redbrown or stramineous, ± flattened, linear to narrowly cuneate, outer 3–5 mm, inner 4–6+ mm, margins not distinctly ciliate, apices ± truncate to concave, faces ± tuberculate, antrorsely strigillose;</text>
      <biological_entity id="o19444" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="blackish" name="coloration" src="d0_s11" to="redbrown or stramineous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="narrowly cuneate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o19445" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o19446" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19447" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not distinctly" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19448" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="less truncate" name="shape" src="d0_s11" to="concave" />
      </biological_entity>
      <biological_entity id="o19449" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s11" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 2 ± erect, antrorsely barbed or smooth awns (0.2–) 1–2.4 mm. 2n = 24.</text>
      <biological_entity id="o19450" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="of 2 more or less erect; antrorsely" name="architecture" src="d0_s12" value="barbed" value_original="barbed" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o19451" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19452" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ponds, swamps, other relatively wet sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="other wet sites" modifier="relatively" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Bident discoïde</other_name>
  <discussion>Bidens discoidea is not nearly as variable morphologically as the closely related B. vulgata or B. frondosa. It differs from B. frondosa in having smaller heads, cypselae with shorter, antrorsely barbed awns, fewer phyllaries and flowers per head, sparse indument, and leaves with fewer, more acuminate leaflets. Both B. discoidea and B. frondosa differ from B. vulgata in texture of flowers, in heads, and in shapes and surfaces of cypselae (M. L. Roberts 1982, 1983; M. G. Hickler 1999).</discussion>
  
</bio:treatment>