<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="treatment_page">513</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Britton) G. L. Nesom" date="1995" rank="species">priceae</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 290. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species priceae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067677</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">priceae</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. N. States,</publication_title>
      <place_in_publication>960. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species priceae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">kentuckiensis</taxon_name>
    <taxon_hierarchy>genus Aster;species kentuckiensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">pilosus</taxon_name>
    <taxon_name authority="(Britton) Cronquist" date="unknown" rank="variety">priceae</taxon_name>
    <taxon_hierarchy>genus Aster;species pilosus;variety priceae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–100 cm, cespitose;</text>
    </statement>
    <statement id="d0_s1">
      <text>with short, branched caudices.</text>
      <biological_entity id="o24644" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o24645" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o24644" id="r2264" name="with" negation="false" src="d0_s1" to="o24645" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–3+, decumbent to ascending (curved or straight, sometimes stout, green to reddish-brown), glabrous.</text>
      <biological_entity id="o24646" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" upper_restricted="false" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (dark green to bluish green) thin, apices acute to acuminate, mucronate, faces glabrate or glabrous;</text>
      <biological_entity id="o24647" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o24648" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o24649" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering (vernal rosettes developed at flowering), petiolate or sessile (petioles winged, sheathing, ciliate), blades oblanceolate to obovate, 10–70 × 3–5 mm, bases cuneate to attenuate, margins entire or rarely sparsely serrate distally, apices obtuse to rounded, cuspidate;</text>
      <biological_entity constraint="basal" id="o24650" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by apices" constraintid="o24654" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o24651" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24652" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24653" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24654" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="rarely sparsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline usually withering by flowering, petiolate or sessile (petioles narrowly winged, bases ± clasping), blades oblanceolate to linear-oblanceolate, 70–105 × 2–4 cm, bases attenuate to cuneate, or ± auriculate, ± clasping;</text>
      <biological_entity constraint="proximal cauline" id="o24655" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades, bases" constraintid="o24656, o24657" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
        <character char_type="range_value" from="attenuate" name="shape" notes="" src="d0_s5" to="cuneate or more or less auriculate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o24656" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear-oblanceolate" />
        <character char_type="range_value" from="70" from_unit="cm" name="length" src="d0_s5" to="105" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="cuneate or more or less auriculate" />
      </biological_entity>
      <biological_entity id="o24657" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear-oblanceolate" />
        <character char_type="range_value" from="70" from_unit="cm" name="length" src="d0_s5" to="105" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="cuneate or more or less auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades linear-lanceolate to narrowly subulate, 5–65 × 1–4 mm, progressively reduced distally, bases subauriculate, ± clasping, margins entire, with marginal cilia proximally, faces glabrous;</text>
      <biological_entity constraint="distal" id="o24658" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o24659" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s6" to="narrowly subulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="65" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o24660" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subauriculate" value_original="subauriculate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o24661" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o24662" name="cilium" name_original="cilia" src="d0_s6" type="structure" />
      <relation from="o24661" id="r2265" name="with" negation="false" src="d0_s6" to="o24662" />
    </statement>
    <statement id="d0_s7">
      <text>reduced array leaves often in fascicles along branches, markedly 3-nerved.</text>
      <biological_entity id="o24663" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o24664" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="markedly" name="architecture" notes="" src="d0_s7" value="3-nerved" value_original="3-nerved" />
      </biological_entity>
      <biological_entity id="o24665" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <relation from="o24664" id="r2266" modifier="in fascicles" name="along" negation="false" src="d0_s7" to="o24665" />
    </statement>
    <statement id="d0_s8">
      <text>Heads in leafy, paniculiform to racemiform arrays, branches nearly divaricate to ascending, straight, sometimes secund, secondarily ramified, leafy with array leaves ± stiff, asscending to appressed.</text>
      <biological_entity id="o24666" name="head" name_original="heads" src="d0_s8" type="structure" />
      <biological_entity id="o24667" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s8" to="racemiform" />
      </biological_entity>
      <biological_entity id="o24668" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="secondarily" name="architecture" src="d0_s8" value="ramified" value_original="ramified" />
        <character constraint="with array, leaves" constraintid="o24669, o24670" is_modifier="false" name="architecture" src="d0_s8" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="asscending" name="orientation" notes="" src="d0_s8" to="appressed" />
      </biological_entity>
      <biological_entity id="o24669" name="array" name_original="array" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o24670" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o24666" id="r2267" name="in" negation="false" src="d0_s8" to="o24667" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0.4–2 cm, mostly borne on secondary branches, glabrous, bracts 3–6, linear to subulate, stiff, distalmost sometimes surpassing involucres, glabrous.</text>
      <biological_entity id="o24671" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o24672" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity id="o24673" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="6" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="subulate" />
        <character is_modifier="false" name="fragility" src="d0_s9" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="position" src="d0_s9" value="distalmost" value_original="distalmost" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24674" name="involucre" name_original="involucres" src="d0_s9" type="structure" />
      <relation from="o24671" id="r2268" modifier="mostly" name="borne on" negation="false" src="d0_s9" to="o24672" />
      <relation from="o24673" id="r2269" modifier="sometimes" name="surpassing" negation="false" src="d0_s9" to="o24674" />
    </statement>
    <statement id="d0_s10">
      <text>Involucres cylindric, (4.5–) 5.5–7.1 (–8.5) mm.</text>
      <biological_entity id="o24675" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 4–6 series, oblong-lanceolate to oblanceolate to sometimes linear (innermost), unequal to subequal, bases indurate 1/4–2/3, margins narrowly scarious, erose, hyaline, sometimes sparsely ciliolate, green zones diamond-shaped to lanceolate, apices acute to long-acuminate, involute, spreading, mucronate to apiculate, faces glabrous.</text>
      <biological_entity id="o24676" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" notes="" src="d0_s11" to="oblanceolate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character char_type="range_value" from="unequal" name="size" src="d0_s11" to="subequal" />
      </biological_entity>
      <biological_entity id="o24677" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
      <biological_entity id="o24678" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="indurate" value_original="indurate" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s11" to="2/3" />
      </biological_entity>
      <biological_entity id="o24679" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s11" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o24680" name="zone" name_original="zones" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character char_type="range_value" from="diamond-shaped" name="shape" src="d0_s11" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o24681" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="long-acuminate" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="involute" value_original="involute" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="mucronate" name="shape" src="d0_s11" to="apiculate" />
      </biological_entity>
      <biological_entity id="o24682" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o24676" id="r2270" name="in" negation="false" src="d0_s11" to="o24677" />
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets (13–) 20–28 (–34);</text>
      <biological_entity id="o24683" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="13" name="atypical_quantity" src="d0_s12" to="20" to_inclusive="false" />
        <character char_type="range_value" from="28" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="34" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="28" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas usually blue-violet, rarely white, laminae (7–) 9–15 (–19) × 0.6–2.1 mm.</text>
      <biological_entity id="o24684" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="blue-violet" value_original="blue-violet" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o24685" name="lamina" name_original="laminae" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s13" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="19" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s13" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s13" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets (28–) 33–51 (–68);</text>
      <biological_entity id="o24686" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="28" name="atypical_quantity" src="d0_s14" to="33" to_inclusive="false" />
        <character char_type="range_value" from="51" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="68" />
        <character char_type="range_value" from="33" name="quantity" src="d0_s14" to="51" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas yellow turning brown, 3.4–4.6 (–5.5) mm, tubes shorter than funnelform throats, lobes lanceolate, 0.5–1 mm.</text>
      <biological_entity id="o24687" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow turning brown" value_original="yellow turning brown" />
        <character char_type="range_value" from="4.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s15" to="4.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24688" name="tube" name_original="tubes" src="d0_s15" type="structure">
        <character constraint="than funnelform throats" constraintid="o24689" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o24689" name="throat" name_original="throats" src="d0_s15" type="structure" />
      <biological_entity id="o24690" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae tan to brown, obovoid, ± compressed, 1.5–2.1 mm, 4–5-nerved (thin), faces sparsely strigillose;</text>
      <biological_entity id="o24691" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s16" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="4-5-nerved" value_original="4-5-nerved" />
      </biological_entity>
      <biological_entity id="o24692" name="face" name_original="faces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi white, 3–5 mm. 2n = 64.</text>
      <biological_entity id="o24693" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24694" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Broken limestone pavements of cedar glades, limestone disturbed roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="broken limestone pavements" constraint="of cedar glades" />
        <character name="habitat" value="cedar glades" />
        <character name="habitat" value="roadsides" modifier="limestone disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <other_name type="common_name">Price’s or lavender oldfield aster</other_name>
  <discussion>Symphyotrichum priceae blooms earlier than S. pilosum var. pilosum.</discussion>
  
</bio:treatment>