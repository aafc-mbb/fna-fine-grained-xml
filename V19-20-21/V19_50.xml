<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="treatment_page">94</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carduus</taxon_name>
    <taxon_name authority="Curtis" date="1789" rank="species">tenuiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Londin.</publication_title>
      <place_in_publication>2(6,61): plate 55. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus carduus;species tenuiflorus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250005004</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pycnocephalus</taxon_name>
    <taxon_name authority="(Curtis) Fiori" date="unknown" rank="variety">tenuiflorus</taxon_name>
    <taxon_hierarchy>genus Carduus;species pycnocephalus;variety tenuiflorus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–200 cm.</text>
      <biological_entity id="o12763" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or openly branched, loosely tomentose with fine single-celled hairs and villous with curled, septate hairs;</text>
      <biological_entity id="o12764" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="with hairs" constraintid="o12765" is_modifier="false" modifier="loosely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character constraint="with hairs" constraintid="o12766" is_modifier="false" name="pubescence" notes="" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o12765" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="single-celled" value_original="single-celled" />
      </biological_entity>
      <biological_entity id="o12766" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="curled" value_original="curled" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>teeth of wings to 25 mm, wing spines to 15 mm.</text>
      <biological_entity id="o12767" name="tooth" name_original="teeth" src="d0_s2" type="structure" constraint="wing" constraint_original="wing; wing">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12768" name="wing" name_original="wings" src="d0_s2" type="structure" />
      <biological_entity constraint="wing" id="o12769" name="spine" name_original="spines" src="d0_s2" type="structure" />
      <relation from="o12767" id="r1166" name="part_of" negation="false" src="d0_s2" to="o12768" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal tapered to winged petioles, blades 10–25 cm, margins pinnately 6–10-lobed, abaxial faces tomentose, adaxial faces loosely tomentose and villous or ± glabrate;</text>
      <biological_entity id="o12770" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o12771" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o12772" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o12773" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12774" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s3" value="6-10-lobed" value_original="6-10-lobed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12775" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12776" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline sessile, shorter, less divided.</text>
      <biological_entity id="o12777" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o12778" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="less" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads clustered in ± tight arrays of 5–20+ at ends of stems, usually sessile, 15–22 mm × 7–12 mm.</text>
      <biological_entity id="o12779" name="head" name_original="heads" src="d0_s5" type="structure">
        <character constraint="in arrays" constraintid="o12780" is_modifier="false" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s5" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12780" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="arrangement_or_density" src="d0_s5" value="tight" value_original="tight" />
        <character char_type="range_value" constraint="at ends" constraintid="o12781" from="5" modifier="of" name="quantity" src="d0_s5" to="20" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12781" name="end" name_original="ends" src="d0_s5" type="structure" />
      <biological_entity id="o12782" name="stem" name_original="stems" src="d0_s5" type="structure" />
      <relation from="o12781" id="r1167" name="part_of" negation="false" src="d0_s5" to="o12782" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres cylindric to ellipsoid (appearing campanulate when pressed), 15–20 × 7–12 mm.</text>
      <biological_entity id="o12783" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s6" to="ellipsoid" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries linear-lanceolate, bases appressed, 2–2.5 mm wide, ± glabrate, and ascending, appendages 0.5–1.5 mm wide, narrowly scarious-margined, distally glabrous or minutely ciliolate, spine tips 1–2 mm, inner phyllaries with erect, straight, unarmed tips.</text>
      <biological_entity id="o12784" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o12785" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o12786" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s7" value="scarious-margined" value_original="scarious-margined" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s7" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity constraint="spine" id="o12787" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o12788" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o12789" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="true" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <relation from="o12788" id="r1168" name="with" negation="false" src="d0_s7" to="o12789" />
    </statement>
    <statement id="d0_s8">
      <text>Corollas pinkish, 10–14 mm;</text>
      <biological_entity id="o12790" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lobes 1.5–2.5 times longer than throat.</text>
      <biological_entity id="o12791" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character constraint="throat" constraintid="o12792" is_modifier="false" name="length_or_size" src="d0_s9" value="1.5-2.5 times longer than throat" />
      </biological_entity>
      <biological_entity id="o12792" name="throat" name_original="throat" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Cypselae brown, 4–5 mm, finely 10–13-nerved;</text>
      <biological_entity id="o12793" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="architecture" src="d0_s10" value="10-13-nerved" value_original="10-13-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappus bristles 10–15 mm. 2n = 54.</text>
      <biological_entity constraint="pappus" id="o12794" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12795" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer (Apr–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Aggressive weed of waste ground, pastures, roadsides, fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="aggressive weed" constraint="of waste ground , pastures , roadsides ," />
        <character name="habitat" value="waste ground" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg., Pa.; s Europe (Mediterranean region).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="s Europe (Mediterranean region)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Slender-flowered thistle</other_name>
  <discussion>Carduus tenuiflorus has been reported from New Jersey, Texas, and Washington; I have not seen specimens from those states.</discussion>
  <discussion>Carduus pycnocephalus and C. tenuiflorus are similar annuals with small, usually tightly clustered heads. The number of heads per capitulescence is usually ultimately greater in C. tenuiflorus, but early season plants of this species often have only a few heads. At the end of the growing season the fruiting heads of C. tenuiflorus are aggregated in dense, subspheric clusters. Stem wings tend to be more pronounced in C. tenuiflorus. Fresh corollas of C. pycnocephalus are rose-purple whereas those of C. tenuiflorus have a more pinkish tinge, but this difference is subtle and not reliable on herbarium material. The phyllaries of C. tenuiflorus are membranous-margined, more or less glabrate, and lack the short, stiff, upwardly appressed trichomes of C. pycnocephalus. All published chromosome counts for Carduus tenuiflorus from both Old and New World material are the same.</discussion>
  <discussion>The two species sometimes grow in mixed populations and at times appear to intergrade. Hybridization has been reported in Europe (S. W. T. Batra et al. 1981) and is suspected to occur in California. Hybrids between C. pycnocephalus and C. tenuiflorus have been designated Carduus ×theriotii Rouy.</discussion>
  
</bio:treatment>