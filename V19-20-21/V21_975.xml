<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">388</other_info_on_meta>
    <other_info_on_meta type="illustration_page">386</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">hymenothrix</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">wrightii</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 97. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus hymenothrix;species wrightii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066982</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 3–6 dm.</text>
      <biological_entity id="o22126" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Phyllaries (8–) 12–16, mostly oblong to ovate or obovate.</text>
      <biological_entity id="o22128" name="phyllary" name_original="phyllaries" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s1" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s1" to="16" />
        <character char_type="range_value" from="mostly oblong" name="shape" src="d0_s1" to="ovate or obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rays 0.</text>
      <biological_entity id="o22129" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Disc-florets 15–30;</text>
      <biological_entity id="o22130" name="disc-floret" name_original="disc-florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s3" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corollas white or pinkish to purplish, 5–6 mm, lobe lengths 2–3+ times throats;</text>
      <biological_entity id="o22131" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s4" to="purplish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22132" name="lobe" name_original="lobe" src="d0_s4" type="structure">
        <character constraint="throat" constraintid="o22133" is_modifier="false" name="length" src="d0_s4" value="2-3+ times throats" value_original="2-3+ times throats" />
      </biological_entity>
      <biological_entity id="o22133" name="throat" name_original="throats" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>anthers pinkish to purplish.</text>
      <biological_entity id="o22134" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s5" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 4–5 mm;</text>
      <biological_entity id="o22135" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pappus-scales 4–6 mm. 2n = 24.</text>
      <biological_entity id="o22136" name="pappus-scale" name_original="pappus-scales" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="distance" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22137" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky places, often with pinyons and junipers</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky places" />
        <character name="habitat" value="pinyons" />
        <character name="habitat" value="junipers" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Tex.; Mexico (Baja California, Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>