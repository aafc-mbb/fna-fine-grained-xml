<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1897" rank="species">pygmaeus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.,</publication_title>
      <place_in_publication>390. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species pygmaeus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066663</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">nevadensis</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">pygmaeus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 649. 1873 (as nevadense var. pygmaeum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species nevadensis;variety pygmaeus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 1–6 (–14) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively thick, retaining old leaf-bases.</text>
      <biological_entity id="o19261" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="14" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o19262" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o19263" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o19262" id="r1790" name="retaining" negation="false" src="d0_s1" to="o19263" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, hirtellous to sparsely hirsute (hairs straight-spreading, not deflexed), densely minutely glandular.</text>
      <biological_entity id="o19264" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s2" to="sparsely hirsute" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline (petioles prominently ciliate, hairs spreading, thick-based);</text>
      <biological_entity id="o19265" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to narrowly oblanceolate or subspatulate (usually folding), 20–35 (–45) × 2–4 mm;</text>
      <biological_entity id="o19266" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly oblanceolate or subspatulate" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline often abruptly reduced distally, margins entire, faces hirsuto-strigose, densely minutely glandular.</text>
      <biological_entity constraint="cauline" id="o19267" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often abruptly; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o19268" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19269" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsuto-strigose" value_original="hirsuto-strigose" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1.</text>
      <biological_entity id="o19270" name="head" name_original="heads" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 4–7 × 6–15 mm.</text>
      <biological_entity id="o19271" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2–3 series (commonly purplish to purplish black, sometimes only at tips, midvein region greenish), hirsute, densely minutely glandular.</text>
      <biological_entity id="o19272" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o19273" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o19272" id="r1791" name="in" negation="false" src="d0_s8" to="o19273" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 20–37;</text>
      <biological_entity id="o19274" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="37" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas usually blue or purple, rarely white, 4–10 mm, laminae tardily, often only slightly, reflexing.</text>
      <biological_entity id="o19275" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19276" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often only; only slightly; slightly" name="orientation" src="d0_s10" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 3.7–5.3 mm (throats tubular).</text>
      <biological_entity constraint="disc" id="o19277" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s11" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2.3–2.8 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o19278" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o19279" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of inconspicuous, fine setae, inner of 15–25 bristles.</text>
      <biological_entity id="o19280" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o19281" name="seta" name_original="setae" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="true" name="width" src="d0_s13" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o19283" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
      <relation from="o19280" id="r1792" name="outer of" negation="false" src="d0_s13" to="o19281" />
      <relation from="o19282" id="r1793" name="consist_of" negation="false" src="d0_s13" to="o19283" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity constraint="inner" id="o19282" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o19284" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky ridges or slopes, often talus, above timberline, sometimes alpine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="talus" modifier="often" constraint="above timberline" />
        <character name="habitat" value="timberline" />
        <character name="habitat" value="alpine" modifier="sometimes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2900–4100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4100" to_unit="m" from="2900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <other_name type="common_name">Pygmy fleabane</other_name>
  <discussion>Erigeron pygmaeus is found in the Sierra Nevada.</discussion>
  
</bio:treatment>