<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">355</other_info_on_meta>
    <other_info_on_meta type="illustration_page">354</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="L’Héritier" date="1789" rank="genus">boltonia</taxon_name>
    <taxon_name authority="L. C. Anderson" date="1987" rank="species">apalachicolensis</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>12: 133, fig. 1. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus boltonia;species apalachicolensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066241</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 50–180 cm;</text>
      <biological_entity id="o11446" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="180" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons and rhizomes absent, fallen stems frequently rooting at nodes.</text>
      <biological_entity id="o11447" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11448" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11449" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="fallen" value_original="fallen" />
        <character constraint="at nodes" constraintid="o11450" is_modifier="false" modifier="frequently" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o11450" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect.</text>
      <biological_entity id="o11451" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades linear to oblanceolate, obovate, or oblique-elliptic;</text>
      <biological_entity id="o11452" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate obovate or oblique-elliptic" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate obovate or oblique-elliptic" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate obovate or oblique-elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline 45–120 × 8–12 mm, bases not decurrent.</text>
      <biological_entity constraint="cauline" id="o11453" name="leaf-blade" name_original="leaf_blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="45" from_unit="mm" name="length" src="d0_s4" to="120" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11454" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in corymbiform arrays, branches spreading to ascending, most bracts leaflike, 10–40 × 3–8 mm.</text>
      <biological_entity id="o11455" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o11456" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <biological_entity id="o11457" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="ascending" />
      </biological_entity>
      <biological_entity id="o11458" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o11455" id="r1047" name="in" negation="false" src="d0_s5" to="o11456" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 6–35 mm;</text>
      <biological_entity id="o11459" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts (0–) 1–3, linear to linear-oblanceolate, 1–2.5 mm.</text>
      <biological_entity id="o11460" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="linear-oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres 2–2.5 (–4) × 4–6 mm.</text>
      <biological_entity id="o11461" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 2–4 series, linear to subulate, subequal, 0–4 merging down peduncle;</text>
      <biological_entity id="o11462" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s9" to="subulate" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o11463" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o11464" name="peduncle" name_original="peduncle" src="d0_s9" type="structure" />
      <relation from="o11462" id="r1048" name="in" negation="false" src="d0_s9" to="o11463" />
      <relation from="o11462" id="r1049" name="merging" negation="false" src="d0_s9" to="o11464" />
    </statement>
    <statement id="d0_s10">
      <text>outer 1.3–2 × 0.3–0.6 mm;</text>
      <biological_entity constraint="outer" id="o11465" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>inner 1.5–3 × 0.3–0.6.</text>
      <biological_entity constraint="inner" id="o11466" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" name="quantity" src="d0_s11" to="3" />
        <character char_type="range_value" from="0.3" name="quantity" src="d0_s11" to="0.6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 20–35;</text>
      <biological_entity id="o11467" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas white to lilac, laminae 5–9 mm, tubes 0.6–0.9 mm.</text>
      <biological_entity id="o11468" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="lilac" />
      </biological_entity>
      <biological_entity id="o11469" name="lamina" name_original="laminae" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11470" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 80–134;</text>
      <biological_entity id="o11471" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s14" to="134" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas 2.1–2.9 mm.</text>
      <biological_entity id="o11472" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s15" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae obovoid, 1.3–2.1 × 0.8–1.3 mm, wings 0.2–0.3 mm wide;</text>
      <biological_entity id="o11473" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s16" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11474" name="wing" name_original="wings" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s16" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi awns 0.3–0.8 mm. 2n = 18.</text>
      <biological_entity constraint="pappi" id="o11475" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11476" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Sep–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="late Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, gray sand, in semishade or deep shade, hardwood floodplain forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="gray sand" />
        <character name="habitat" value="semishade" />
        <character name="habitat" value="deep shade" />
        <character name="habitat" value="hardwood floodplain forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Apalachicola doll’s-daisy</other_name>
  
</bio:treatment>