<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">347</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="K. L. Chambers" date="1991" rank="genus">stebbinsoseris</taxon_name>
    <taxon_name authority="(K. L. Chambers) K. L. Chambers" date="1991" rank="species">decipiens</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>78: 1025. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus stebbinsoseris;species decipiens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067594</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Microseris</taxon_name>
    <taxon_name authority="K. L. Chambers" date="unknown" rank="species">decipiens</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Dudley Herb.</publication_title>
      <place_in_publication>4: 290, fig. 17. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Microseris;species decipiens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Peduncles 15–60 cm.</text>
      <biological_entity id="o11597" name="peduncle" name_original="peduncles" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres 6–19 mm.</text>
      <biological_entity id="o11598" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s1" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Florets 10–80 (–100);</text>
      <biological_entity id="o11599" name="floret" name_original="florets" src="d0_s2" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="100" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corollas yellow.</text>
      <biological_entity id="o11600" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae brown to purplish, narrowly truncate-fusiform, 5–8 mm, each filled by embryo or no more than distal 0.5 mm vacant, apices not enlarged at bases of pappi;</text>
      <biological_entity id="o11601" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s4" to="purplish" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="truncate-fusiform" value_original="truncate-fusiform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="no" value_original="no" />
        <character is_modifier="false" modifier="more than" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character modifier="more than" name="some_measurement" src="d0_s4" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o11602" name="embryo" name_original="embryo" src="d0_s4" type="structure" />
      <biological_entity id="o11603" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character constraint="at bases" constraintid="o11604" is_modifier="false" modifier="not" name="size" src="d0_s4" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o11604" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o11605" name="pappus" name_original="pappi" src="d0_s4" type="structure" />
      <relation from="o11601" id="r1074" name="filled by" negation="false" src="d0_s4" to="o11602" />
      <relation from="o11604" id="r1075" name="part_of" negation="false" src="d0_s4" to="o11605" />
    </statement>
    <statement id="d0_s5">
      <text>pappi 7–10 mm, scale bodies 3–5 mm, faces glabrous, aristae 4–5 mm. 2n = 36.</text>
      <biological_entity id="o11606" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="scale" id="o11607" name="body" name_original="bodies" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11608" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11609" name="arista" name_original="aristae" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11610" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, shale, or serpentine soils, grasslands, coastal scrub, chaparral, closed-cone pine woods, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine soils" modifier="sandy shale or" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="closed-cone pine woods" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="shale" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Santa Cruz silverpuffs</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Morphologic and molecular evidence (K. L. Chambers 1955; C. Irmler et al. 1982; R. S. Wallace and R. K. Jansen 1990) proves that Stebbinsoseris decipiens is an allopolyploid derivative of the hybrid Microseris bigelovii × Uropappus lindleyi. It occurs in a limited area of central coastal California where the parental taxa are sympatric. Diploid hybrids between the parents, produced experimentally (Chambers), had irregular meiosis and were completely seed-sterile. The species is included in Inventory of Rare and Endangered Plants of California, ed. 6 (D. P. Tibor 2001).</discussion>
  <references>
    <reference>Irmler, C. et al. 1982. Enzymes and quantitative morphological characters compared between the allotetraploid Microseris decipiens and its diploid parental species. Beitr. Biol. Pflanzen 57: 269–289.</reference>
  </references>
  
</bio:treatment>