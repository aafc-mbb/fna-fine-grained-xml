<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">297</other_info_on_meta>
    <other_info_on_meta type="illustration_page">295</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Scopoli" date="1777" rank="genus">urospermum</taxon_name>
    <taxon_name authority="(Linnaeus) F. W. Schmidt" date="1795" rank="species">picroides</taxon_name>
    <place_of_publication>
      <publication_title>Samml. Phys.-Oekon. Aufsätze</publication_title>
      <place_in_publication>1: 275. 1795</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus urospermum;species picroides</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220013996</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tragopogon</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">picroides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 790. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tragopogon;species picroides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: proximal blades 8–15 × 3–5+ cm, distal 4–8+ × 1–3+ cm.</text>
      <biological_entity id="o17179" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="proximal" id="o17180" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s0" to="5" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17181" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s0" to="8" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="3" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 5–15+ cm.</text>
      <biological_entity id="o17182" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries: free portions lance-linear, abaxial faces setose or ± glabrate.</text>
      <biological_entity id="o17183" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure" />
      <biological_entity id="o17184" name="portion" name_original="portions" src="d0_s2" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s2" value="free" value_original="free" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17185" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="setose" value_original="setose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cypselae (including beaks) 10–15 mm;</text>
      <biological_entity id="o17186" name="cypsela" name_original="cypselae" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pappi 9–12 mm. 2n = 8, 10.</text>
      <biological_entity id="o17187" name="pappus" name_original="pappi" src="d0_s4" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17188" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="8" value_original="8" />
        <character name="quantity" src="d0_s4" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe; Asia; also introduced in South America, s Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="also  in South America" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>