<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="treatment_page">236</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Nuttall) Shinners" date="1951" rank="species">oregona</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>19: 71. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species oregona</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066925</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ammodia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">oregona</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 321. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ammodia;species oregona;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">oregona</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species oregona;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–105 cm;</text>
      <biological_entity id="o7673" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="105" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices branched.</text>
      <biological_entity id="o7674" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect (sometimes ± brittle, sometimes brown), glabrate to densely hispid or densely stipitate-andular (axillary leaf fascicles sometimes present).</text>
      <biological_entity id="o7675" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s2" to="densely hispid" />
        <character name="pubescence" src="d0_s2" value="densely" value_original="densely" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline withering by flowering;</text>
      <biological_entity id="o7676" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o7677" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid and distal blades ovate to linear-lanceolate, 12–50 × 4–40 mm, rounded to very weakly clasping, margins flat, strigoso-ciliate, apices acute, faces glabrate to densely hairy and/or stipitate-glandular;</text>
      <biological_entity id="o7678" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="mid and distal" id="o7679" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="40" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="very weakly" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o7680" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="strigoso-ciliate" value_original="strigoso-ciliate" />
      </biological_entity>
      <biological_entity id="o7681" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7682" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="densely hairy and/or stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branch leaves reduced.</text>
      <biological_entity id="o7683" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="branch" id="o7684" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads (discoid) 1–15 per branch, in corymbiform, openly paniculiform, or racemiform arrays.</text>
      <biological_entity id="o7685" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per branch" constraintid="o7686" from="1" name="quantity" src="d0_s7" to="15" />
      </biological_entity>
      <biological_entity id="o7686" name="branch" name_original="branch" src="d0_s7" type="structure" />
      <biological_entity id="o7687" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" modifier="openly" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o7685" id="r698" name="in" negation="false" src="d0_s7" to="o7687" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 5–50 mm, sparsely to densely hispid, stipitate-glandular;</text>
      <biological_entity id="o7688" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="50" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts linear to narrowly lanceolate, 3–9 × 0.5–2 mm.</text>
      <biological_entity id="o7689" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="narrowly lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres turbino-cylindric, 7.5–14 mm.</text>
      <biological_entity id="o7690" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="turbino-cylindric" value_original="turbino-cylindric" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 5–7 series, outer ovate to deltate (2–5 × 0.6–1.7 mm), inner lanceolate, unequal, abaxial faces sparsely to densely hispid or stipitate-glandular (in green zone).</text>
      <biological_entity id="o7691" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure" />
      <biological_entity id="o7692" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
      <biological_entity constraint="outer" id="o7693" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="deltate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o7694" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7695" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o7691" id="r699" name="in" negation="false" src="d0_s11" to="o7692" />
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 0.</text>
      <biological_entity id="o7696" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (14–) 20–45 (–60);</text>
      <biological_entity id="o7697" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="14" name="atypical_quantity" src="d0_s13" to="20" to_inclusive="false" />
        <character char_type="range_value" from="45" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="60" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas ± ampliate, (7.5–) 8.5–11 (–14) mm, throats and lobes sparsely short-pilose, lobes 0.4–0.9 mm (margins thickened).</text>
      <biological_entity id="o7698" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s14" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="8.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="14" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7699" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
      <biological_entity id="o7700" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
      <biological_entity id="o7701" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae monomorphic, obconic, compressed, 2–5 × 0.35–1.6 mm, ribs 5–10, faces sparsely strigose;</text>
      <biological_entity id="o7702" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s15" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.35" from_unit="mm" name="width" src="d0_s15" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7703" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s15" to="10" />
      </biological_entity>
      <biological_entity id="o7704" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi tan, outer of linear scales 0.1–0.5 mm (not obvious), inner of 30–40 bristles 4.5–6.4, longest weakly clavate.</text>
      <biological_entity id="o7705" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity id="o7706" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7708" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s16" to="40" />
        <character char_type="range_value" from="4.5" name="quantity" src="d0_s16" to="6.4" />
      </biological_entity>
      <relation from="o7705" id="r700" name="outer of" negation="false" src="d0_s16" to="o7706" />
      <relation from="o7707" id="r701" name="consist_of" negation="false" src="d0_s16" to="o7708" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 18.</text>
      <biological_entity constraint="inner" id="o7707" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="length" notes="" src="d0_s16" value="longest" value_original="longest" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s16" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7709" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Oregon goldenaster</other_name>
  <discussion>varieties 4 (4 in the flora</discussion>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal stems densely, coarsely stipitate-glandular; mid cauline leaves averaging 13.9 × 3.9 mm (9.2–19.4 × 2.2–7.8 mm); outer phyllaries narrowly to broadly ovate; inner Coast Ranges, California</description>
      <determination>3d Heterotheca oregona var. scaberrima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal stems sparsely to moderately, coarsely stipitate-glandular, or moderately to densely, finely stipitate-glandular; mid cauline leaves averaging 22–32 × 4.7–9.1 mm (1.5–42.5 × 2.1–36.3 mm); outer phyllaries lanceolate; Cascades to Coastal ranges, Washington to c California</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems usually branched proximally; leaves gray-green, long-villous (200–1000+ m in moun-tains, n California)</description>
      <determination>3c Heterotheca oregona var. compacta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems usually openly branched distally; leaves green, glabrate to moderately hispid</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems moderately to densely, finely stipitate-glandular; branch leaves glabrate to sparsely hispid (fewer than 5 hairs/mm²), moderately stipitate-glandular; Olympic Peninsula and Cascades to Coastal ranges (n, c California)</description>
      <determination>3a Heterotheca oregona var. oregona</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems sparsely to moderately coarsely stipitate-glandular; branch leaves moderately short-hispid (more than 5 hairs/mm²), sparsely to moderately, coarsely stipitate-glandular; coast to lower mountains, s Oregon, n California</description>
      <determination>3b Heterotheca oregona var. rudis</determination>
    </key_statement>
  </key>
</bio:treatment>