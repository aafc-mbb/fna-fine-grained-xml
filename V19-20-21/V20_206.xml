<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="treatment_page">105</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="genus">CHRYSOMA</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 67. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus CHRYSOMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chrysos, gold, and - ome, having the condition of; alluding to predominantly yellow-gold heads and corymbs</other_info_on_name>
    <other_info_on_name type="fna_id">106996</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 50–100 cm (evergreen), glabrous, resinous.</text>
      <biological_entity id="o26846" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched.</text>
      <biological_entity id="o26847" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o26848" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-nerved, oblanceolate to narrowly elliptic (bases sometimes attenuate), margins entire, faces gland-dotted, resinous (covered by reticulum of subisodiametric areoles, each areole surrounded by sunken border).</text>
      <biological_entity id="o26849" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="narrowly elliptic" />
      </biological_entity>
      <biological_entity id="o26850" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o26851" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads usually radiate, sometimes discoid, in dense (terminal), cymiform arrays.</text>
      <biological_entity id="o26852" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o26853" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o26852" id="r2487" name="in" negation="false" src="d0_s6" to="o26853" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric, (5–6 ×) 2–2.5 mm.</text>
      <biological_entity id="o26854" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="[5" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="]2" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 8–12 in 3–4 (–5) series (in vertical ranks), erect, loose, stamineous, 1-nerved (midnerves orange-resinous from bases to apices; flat), lanceolate, unequal, margins scarious.</text>
      <biological_entity id="o26855" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o26856" from="8" name="quantity" src="d0_s8" to="12" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s8" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o26856" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o26857" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, shallowly pitted, epaleate.</text>
      <biological_entity id="o26858" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="shallowly" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets (0–) 1–2 (–3), pistillate, fertile;</text>
      <biological_entity id="o26859" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s10" to="1" to_inclusive="false" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o26860" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets (2–) 3–4 (–5), bisexual, fertile;</text>
      <biological_entity id="o26861" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s12" to="3" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="5" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="4" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubular-funnelform, tubes longer than throats, lobes 5, spreading-recurving, lanceolate;</text>
      <biological_entity id="o26862" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="tubular-funnelform" value_original="tubular-funnelform" />
      </biological_entity>
      <biological_entity id="o26863" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than throats" constraintid="o26864" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o26864" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o26865" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading-recurving" value_original="spreading-recurving" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages linear-triangular (closely papillate).</text>
      <biological_entity constraint="style-branch" id="o26866" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="linear-triangular" value_original="linear-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (brownish) turbinate-oblong, nearly terete, 8–10-ribbed, densely strigoso-sericeous;</text>
      <biological_entity id="o26867" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="turbinate-oblong" value_original="turbinate-oblong" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s15" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="8-10-ribbed" value_original="8-10-ribbed" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s15" value="strigoso-sericeous" value_original="strigoso-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 40–60 tan, unequal, barbellate, apically attenuate bristles in 2–3 series.</text>
      <biological_entity id="o26869" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" name="quantity" src="d0_s16" to="60" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="tan" value_original="tan" />
        <character is_modifier="true" name="size" src="d0_s16" value="unequal" value_original="unequal" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s16" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o26870" name="series" name_original="series" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s16" to="3" />
      </biological_entity>
      <relation from="o26868" id="r2488" name="consist_of" negation="false" src="d0_s16" to="o26869" />
      <relation from="o26869" id="r2489" name="in" negation="false" src="d0_s16" to="o26870" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o26868" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o26871" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>161.</number>
  <discussion>Species 1.</discussion>
  <discussion>Chrysoma pauciflosculosa was originally described within Solidago; it is distinct in anatomy (F. E. Lloyd 1901; K. Phillips 1963; L. C. Anderson and J. B. Creech 1975) and other features. Chrysoma is recognized by its shrubby habit, areolate-resinous leaves, corymbiform arrays of glomerate, cylindric, few-flowered heads, long disc corolla lobes, and papillate style-branch appendages.</discussion>
  <references>
    <reference>Lloyd, F. E. 1901. Some points in the anatomy of Chrysoma pauciflosculosa. Bull. Torrey Bot. Club 28: 445–450.</reference>
    <reference>Phillips, K. 1963. A Taxonomic and Morphological Study of Chrysoma pauciflosculosa (Michx.) Greene. M.S. thesis. University of South Carolina.</reference>
  </references>
  
</bio:treatment>