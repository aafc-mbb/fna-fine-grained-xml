<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">210</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">bidens</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 91. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus bidens;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066225</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (10–) 20–80+ cm.</text>
      <biological_entity id="o7071" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles 5–25 mm;</text>
      <biological_entity id="o7072" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o7073" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades rounded-deltate overall, 25–90+ × 15–35+ mm, (1–) 2 (–3) -pinnatisect, ultimate lobes lance-rhombic or ovate to lanceolate, 15–30 (–50+) × 5–15 (–30+) mm, bases truncate to cuneate, ultimate margins entire or ± serrate to incised, usually ciliolate, apices obtuse to acuminate, faces glabrous.</text>
      <biological_entity id="o7074" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7075" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded-deltate" value_original="rounded-deltate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s2" to="90" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s2" to="35" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="(1-)2(-3)-pinnatisect" value_original="(1-)2(-3)-pinnatisect" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o7076" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="50" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7077" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o7078" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s2" value="more or less serrate to incised" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o7079" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
      <biological_entity id="o7080" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly, sometimes in open, ± corymbiform arrays.</text>
      <biological_entity id="o7081" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles (10–) 30–50 (–150+) mm.</text>
      <biological_entity id="o7082" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="150" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of 8–13 usually spreading, narrowly lanceolate, oblanceolate, or subulate to linear bractlets 2–5 mm, margins entire, usually ciliate, abaxial faces usually glabrous.</text>
      <biological_entity id="o7083" name="calyculus" name_original="calyculi" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="of 8-13 usually spreading; narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o7084" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7085" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7086" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres ± campanulate, 2.5–5+ × 1.5–3 (–4) mm.</text>
      <biological_entity id="o7087" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 8–13+, lanceolate, (3–) 4–6+ mm.</text>
      <biological_entity id="o7088" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="13" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0 or 1 (–5+);</text>
      <biological_entity id="o7089" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" upper_restricted="false" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae whitish, 1–3 (–7) mm.</text>
      <biological_entity id="o7090" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 13–25+;</text>
      <biological_entity id="o7091" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s10" to="25" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellowish, 1–2 mm.</text>
      <biological_entity id="o7092" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae: outer redbrown, obcompressed, cuneate, 6–7 mm, margins not ciliate, apices truncate, faces 2-grooved, antrorsely tuberculate-hirtellous;</text>
      <biological_entity id="o7093" name="cypsela" name_original="cypselae" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o7094" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7095" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7096" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o7097" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-grooved" value_original="2-grooved" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s12" value="tuberculate-hirtellous" value_original="tuberculate-hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner dark-brown to blackish, ± equally 4-angled, linear-fusiform, 10–14 mm, margins not ciliate, apices ± attenuate, faces glabrous;</text>
      <biological_entity id="o7098" name="cypsela" name_original="cypselae" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o7099" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s13" to="blackish" />
        <character is_modifier="false" modifier="more or less equally" name="shape" src="d0_s13" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s13" value="linear-fusiform" value_original="linear-fusiform" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7100" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s13" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7101" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o7102" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 2 (–3), erect, retrorsely barbed awns (1–) 2–4 mm.</text>
      <biological_entity id="o7103" name="cypsela" name_original="cypselae" src="d0_s14" type="structure" />
      <biological_entity id="o7104" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o7105" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s14" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="true" modifier="retrorsely" name="architecture_or_shape" src="d0_s14" value="barbed" value_original="barbed" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o7104" id="r510" name="consist_of" negation="false" src="d0_s14" to="o7105" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams, other wettish sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="other wettish sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Okla., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>