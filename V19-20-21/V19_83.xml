<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="treatment_page">124</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">brevifolium</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 421. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species brevifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066360</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Piper) Piper" date="unknown" rank="species">palousense</taxon_name>
    <taxon_hierarchy>genus Cirsium;species palousense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 25–120 cm;</text>
      <biological_entity id="o20940" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots with horizontal root sprouts.</text>
      <biological_entity id="o20941" name="taproot" name_original="taproots" src="d0_s1" type="structure" />
      <biological_entity constraint="root" id="o20942" name="sprout" name_original="sprouts" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <relation from="o20941" id="r1887" name="with" negation="false" src="d0_s1" to="o20942" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, erect, thinly gray-tomentose with fine, non-septate trichomes;</text>
      <biological_entity id="o20943" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="with trichomes" constraintid="o20944" is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <biological_entity id="o20944" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="fine" value_original="fine" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="non-septate" value_original="non-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches 0–many, ascending.</text>
      <biological_entity id="o20945" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves oblanceolate or elliptic, 15–45 × 2–10 cm, unlobed and merely spinulose to dentate or deeply pinnatifid, lobes well separated, linear to triangular-ovate, merely spinulose to few toothed or lobed near base, margins often revolute, main spines 2–3 (–6) mm, abaxial faces densely gray-tomentose, adaxial green, thinly tomentose or ± glabrate;</text>
      <biological_entity id="o20946" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s4" to="45" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="merely" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o20947" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="triangular-ovate merely spinulose" />
        <character char_type="range_value" constraint="near base" constraintid="o20948" from="linear" name="shape" src="d0_s4" to="triangular-ovate merely spinulose" />
      </biological_entity>
      <biological_entity id="o20948" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o20949" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="main" id="o20950" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20951" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20952" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal often present at flowering, narrowly winged-petiolate;</text>
      <biological_entity constraint="basal" id="o20953" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline well distributed, gradually reduced distally, bases of proximal cauline winged-petiolate or sessile, bases of distal cauline expanded and ± clasping, margins sometimes spinier than those of proximal;</text>
      <biological_entity constraint="principal cauline" id="o20954" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o20955" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="cauline proximal" id="o20956" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o20957" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="expanded" value_original="expanded" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="cauline distal" id="o20958" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o20955" id="r1888" name="part_of" negation="false" src="d0_s6" to="o20956" />
      <relation from="o20957" id="r1889" name="part_of" negation="false" src="d0_s6" to="o20958" />
    </statement>
    <statement id="d0_s7">
      <text>distalmost cauline becoming bractlike, often unlobed or less deeply divided than proximal.</text>
      <biological_entity id="o20959" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="distalmost" value_original="distalmost" />
        <character constraint="than proximal leaves" constraintid="o20960" is_modifier="false" name="shape" src="d0_s7" value="less deeply divided" value_original="less deeply divided" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20960" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s7" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads borne singly and terminal on main-stems and branches or few from distal axils in corymbiform or paniculiform arrays.</text>
      <biological_entity id="o20961" name="head" name_original="heads" src="d0_s8" type="structure">
        <character constraint="from distal axils" constraintid="o20964" is_modifier="false" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o20962" name="main-stem" name_original="main-stems" src="d0_s8" type="structure" constraint="terminal">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o20963" name="branch" name_original="branches" src="d0_s8" type="structure" constraint="terminal">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20964" name="axil" name_original="axils" src="d0_s8" type="structure" />
      <biological_entity id="o20965" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o20961" id="r1890" name="borne" negation="false" src="d0_s8" to="o20962" />
      <relation from="o20961" id="r1891" name="borne" negation="false" src="d0_s8" to="o20963" />
      <relation from="o20964" id="r1892" name="in" negation="false" src="d0_s8" to="o20965" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 1–8 cm.</text>
      <biological_entity id="o20966" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres hemispheric to campanulate, 2.5–3.5 × 2–4 cm, glabrous or loosely floccose.</text>
      <biological_entity id="o20967" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s10" to="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s10" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s10" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s10" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 6–10 series, strongly imbricate, greenish to brown, ovate to lanceolate (outer) to linear (inner), abaxial faces with prominent glutinous ridge;</text>
      <biological_entity id="o20968" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s11" to="brown" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o20969" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <biological_entity id="o20971" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o20968" id="r1893" name="in" negation="false" src="d0_s11" to="o20969" />
      <relation from="o20970" id="r1894" name="with" negation="false" src="d0_s11" to="o20971" />
    </statement>
    <statement id="d0_s12">
      <text>outer and middle appressed, bodies entire, spines abruptly spreading, fine, 2–3 (–5) mm;</text>
      <biological_entity constraint="abaxial" id="o20970" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o20972" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="middle" value_original="middle" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o20973" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20974" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="abruptly" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="width" src="d0_s12" value="fine" value_original="fine" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner commonly flexuous or reflexed, flat, scarious.</text>
      <biological_entity id="o20975" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="flexuou; flexuou">
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character is_modifier="false" name="texture" src="d0_s13" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20976" name="flexuou" name_original="flexuous" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="commonly" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <relation from="o20975" id="r1895" name="part_of" negation="false" src="d0_s13" to="o20976" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas creamy white, rarely lavender-tinged, 22–28 mm, tubes 8–13 mm, throats 7–11 mm, lobes 4–6 mm;</text>
      <biological_entity id="o20977" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="lavender-tinged" value_original="lavender-tinged" />
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s14" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20978" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20979" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20980" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style tips 5–6 mm.</text>
      <biological_entity constraint="style" id="o20981" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae brown, 5–6 mm, apical collars yellowish, 0.5–1 mm;</text>
      <biological_entity id="o20982" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o20983" name="collar" name_original="collars" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi 18–22 mm. 2n = 22, 26.</text>
      <biological_entity id="o20984" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s17" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20985" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="22" value_original="22" />
        <character name="quantity" src="d0_s17" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Palouse prairie</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="palouse prairie" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="past_name">Circium</other_name>
  <other_name type="common_name">Palouse thistle</other_name>
  <discussion>Cirsium brevifolium occurs in the Palouse prairie region of eastern Washington, eastern Oregon, and western Idaho.</discussion>
  
</bio:treatment>