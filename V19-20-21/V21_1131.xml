<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="treatment_page">451</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="genus">tetraneuris</taxon_name>
    <taxon_name authority="Greene" date="1898" rank="species">herbacea</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 268. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus tetraneuris;species herbacea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067730</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actinea</taxon_name>
    <taxon_name authority="(Greene) B. L. Robinson" date="unknown" rank="species">herbacea</taxon_name>
    <taxon_hierarchy>genus Actinea;species herbacea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenoxys</taxon_name>
    <taxon_name authority="(Pursh) K. F. Parker" date="unknown" rank="species">acaulis</taxon_name>
    <taxon_name authority="(A. Gray) K. F. Parker" date="unknown" rank="variety">glabra</taxon_name>
    <taxon_hierarchy>genus Hymenoxys;species acaulis;variety glabra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenoxys</taxon_name>
    <taxon_name authority="(Greene) Cronquist" date="unknown" rank="species">herbacea</taxon_name>
    <taxon_hierarchy>genus Hymenoxys;species herbacea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 6–35+ cm.</text>
      <biological_entity id="o10456" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudices ± branched, branches notably thickened distally.</text>
      <biological_entity id="o10457" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o10458" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="notably; distally" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10, erect, unbranched.</text>
      <biological_entity id="o10459" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves all basal, new leaves tightly clustered;</text>
      <biological_entity id="o10460" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o10461" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10462" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="new" value_original="new" />
        <character is_modifier="false" modifier="tightly" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades spatulate or oblanceolate to linear-oblanceolate, entire, glabrous or sparsely to rarely moderately hairy, moderately to densely glanddotted.</text>
      <biological_entity id="o10463" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="linear-oblanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s4" to="sparsely rarely moderately hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–10 per plant, borne singly.</text>
      <biological_entity id="o10464" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o10465" from="1" name="quantity" src="d0_s5" to="10" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o10465" name="plant" name_original="plant" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 5–35 cm, ± hairy proximally, densely hairy distally.</text>
      <biological_entity id="o10466" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="35" to_unit="cm" />
        <character is_modifier="false" modifier="more or less; proximally" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 10–15 × 12–20 mm.</text>
      <biological_entity id="o10467" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Outer phyllaries 8–12, 5–7.5 mm, margins 0–0.2 mm wide, sometimes slightly scarious, abaxial faces ± hairy.</text>
      <biological_entity constraint="outer" id="o10468" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="12" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10469" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s8" to="0.2" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes slightly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10470" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 14–27;</text>
      <biological_entity id="o10471" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s9" to="27" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 11.8–20 mm.</text>
      <biological_entity id="o10472" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="11.8" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 50–100+;</text>
      <biological_entity id="o10473" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s11" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, 3.4–4 mm.</text>
      <biological_entity id="o10474" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 2.4–3.4 mm;</text>
      <biological_entity id="o10475" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s13" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 4–7 obovate, usually non-aristate scales 1.9–2.2 mm. 2n = 28.</text>
      <biological_entity id="o10476" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o10477" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s14" to="7" />
        <character is_modifier="true" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
        <character is_modifier="true" modifier="usually" name="shape" src="d0_s14" value="non-aristate" value_original="non-aristate" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s14" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10478" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="28" value_original="28" />
      </biological_entity>
      <relation from="o10476" id="r715" name="consist_of" negation="false" src="d0_s14" to="o10477" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jun(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alvars (limestone flats), openings in woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alvars" />
        <character name="habitat" value="limestone flats" />
        <character name="habitat" value="openings" constraint="in woods" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ill., Ohio.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Tetraneuris herbacea is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>