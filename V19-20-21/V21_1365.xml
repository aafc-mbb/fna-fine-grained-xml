<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="treatment_page">537</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="genus">carphephorus</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="species">pseudoliatris</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Sci. Soc. Philom. Paris</publication_title>
      <place_in_publication>1816: 198. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus carphephorus;species pseudoliatris</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220002384</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">squamosa</taxon_name>
    <taxon_hierarchy>genus Liatris;species squamosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–100 cm.</text>
      <biological_entity id="o15647" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems finely villous to villoso-hirsute, eglandular.</text>
      <biological_entity id="o15648" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="finely villous" name="pubescence" src="d0_s1" to="villoso-hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline narrowly linear, mostly 10–40 cm;</text>
      <biological_entity id="o15649" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline well developed, gradually reduced, faces glanddotted.</text>
      <biological_entity id="o15650" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o15651" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o15652" name="face" name_original="faces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads in flat-topped, corymbiform arrays.</text>
      <biological_entity id="o15653" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o15654" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o15653" id="r1083" name="in" negation="false" src="d0_s4" to="o15654" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles finely villous (or hairs somewhat appressed).</text>
      <biological_entity id="o15655" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 6–9 mm.</text>
      <biological_entity id="o15656" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 15–40+ in 3–5+ series, triangular-lanceolate, apices acute, abaxial faces villous, eglandular.</text>
      <biological_entity id="o15657" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o15658" from="15" name="quantity" src="d0_s7" to="40" upper_restricted="false" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="triangular-lanceolate" value_original="triangular-lanceolate" />
      </biological_entity>
      <biological_entity id="o15658" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o15659" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15660" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles paleate (nearly throughout).</text>
      <biological_entity id="o15661" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas eglandular, lobes 1–2 mm.</text>
      <biological_entity id="o15662" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o15663" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae eglandular;</text>
      <biological_entity id="o15664" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappus bristles in ± 2 series.</text>
      <biological_entity id="o15666" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o15665" id="r1084" name="in" negation="false" src="d0_s11" to="o15666" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity constraint="pappus" id="o15665" name="bristle" name_original="bristles" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o15667" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jul–)Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry sites, pine barrens, savannas, cutover pine woods, wiregrass savannas, pine-palmetto flatwoods, grass-sedge bogs, swamp edges, ditches, depressions</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sites" />
        <character name="habitat" value="pine barrens" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="moist to dry sites" />
        <character name="habitat" value="cutover" />
        <character name="habitat" value="pine woods" />
        <character name="habitat" value="wiregrass savannas" />
        <character name="habitat" value="pine-palmetto flatwoods" />
        <character name="habitat" value="grass-sedge bogs" />
        <character name="habitat" value="swamp edges" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="depressions" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–90 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="90" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="past_name">pseudo-liatris</other_name>
  <other_name type="common_name">Bristleleaf chaffhead</other_name>
  
</bio:treatment>