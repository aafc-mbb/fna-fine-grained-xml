<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">362</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. C. Eaton in S. Watson" date="1871" rank="genus">glyptopleura</taxon_name>
    <taxon_name authority="D. C. Eaton in S. Watson" date="1871" rank="species">marginata</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>207, plate 20, figs. 11–18. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus glyptopleura;species marginata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220005690</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–6 cm.</text>
      <biological_entity id="o7667" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 0.5–5 cm, margins conspicuously white-crustose.</text>
      <biological_entity id="o7668" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7669" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="texture" src="d0_s1" value="white-crustose" value_original="white-crustose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads 1–2 cm diam.</text>
      <biological_entity id="o7670" name="head" name_original="heads" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Calyculi: margins of bractlets crustose-dentate ± throughout.</text>
      <biological_entity id="o7671" name="calyculus" name_original="calyculi" src="d0_s3" type="structure" />
      <biological_entity id="o7672" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less throughout; throughout" name="architecture_or_shape" src="d0_s3" value="crustose-dentate" value_original="crustose-dentate" />
      </biological_entity>
      <biological_entity id="o7673" name="bractlet" name_original="bractlets" src="d0_s3" type="structure" />
      <relation from="o7672" id="r713" name="part_of" negation="false" src="d0_s3" to="o7673" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres 10–14 mm.</text>
      <biological_entity id="o7674" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Florets 9–18;</text>
      <biological_entity id="o7675" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s5" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas white to cream, aging pink or purple, ligules 4–10 mm, equaling or scarcely exserted beyond involucres.</text>
      <biological_entity id="o7676" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="cream" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>2n = 18.</text>
      <biological_entity id="o7677" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s6" value="scarcely exserted beyond involucres" value_original="scarcely exserted beyond involucres" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7678" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or rocky deserts, alkali flats, arid grasslands, often with Atriplex, sometimes with Larrea</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky deserts" />
        <character name="habitat" value="alkali flats" />
        <character name="habitat" value="grasslands" modifier="arid" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">White-margined wax-plant</other_name>
  <other_name type="common_name">carveseed</other_name>
  <discussion>Glyptopleura marginata is found in the Great Basin and Mojave deserts.</discussion>
  
</bio:treatment>