<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Eric E. Lamont</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">539</other_info_on_meta>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="A. Gray" date="1880" rank="genus">GARBERIA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>1879: 379. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus GARBERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Abram P. Garber, 1838–1881, of Columbia, Pennsylvania, noted for his contributions to the flora of Florida</other_info_on_name>
    <other_info_on_name type="fna_id">113251</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 100–250 cm (± evergreen).</text>
      <biological_entity id="o14724" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (terete, striate when dry), branched (usually glanddotted, farinaceous to puberulent when young).</text>
      <biological_entity id="o14725" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>all or mostly alternate (at flowering);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or subsessile;</text>
      <biological_entity id="o14726" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades obscurely nerved, spatulate to spatulate-obovate or orbiculate-obovate, margins entire, faces glanddotted (viscid, farinaceous when young).</text>
      <biological_entity id="o14727" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="spatulate-obovate or orbiculate-obovate" />
      </biological_entity>
      <biological_entity id="o14728" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14729" name="face" name_original="faces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, in corymbiform or paniculiform arrays.</text>
      <biological_entity id="o14730" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o14731" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o14730" id="r1014" name="in" negation="false" src="d0_s6" to="o14731" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres narrowly cylindric, 3.5–5 (–6) mm diam.</text>
      <biological_entity id="o14732" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, (12–) 15–20 in 3–5 series, ± striate, lanceolate to linear-oblong, unequal (apices acute or acuminate, abaxial faces farinaceous, usually glanddotted).</text>
      <biological_entity id="o14733" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s8" to="15" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o14734" from="15" name="quantity" src="d0_s8" to="20" />
        <character is_modifier="false" modifier="more or less" name="coloration_or_pubescence_or_relief" notes="" src="d0_s8" value="striate" value_original="striate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear-oblong" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o14734" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles weakly convex, epaleate.</text>
      <biological_entity id="o14735" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets usually 5 (aromatic);</text>
      <biological_entity id="o14736" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas pink to purplish, throats ± campanulate, lobes 5, triangular to lanceovate;</text>
      <biological_entity id="o14737" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="purplish" />
      </biological_entity>
      <biological_entity id="o14738" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o14739" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s11" to="lanceovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles: bases not enlarged, glabrous, branches filiform to linear-clavate (distally papillose).</text>
      <biological_entity id="o14740" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o14741" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14742" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s12" to="linear-clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae prismatic, ca. 10-ribbed, densely scabrellous;</text>
      <biological_entity id="o14743" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="10-ribbed" value_original="10-ribbed" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s13" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi persistent, of ca. 60–70, barbellate bristles in 2–3 series (outer shorter than inner).</text>
      <biological_entity id="o14744" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o14745" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="60" is_modifier="true" name="quantity" src="d0_s14" to="70" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o14746" name="series" name_original="series" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s14" to="3" />
      </biological_entity>
      <relation from="o14744" id="r1015" name="consist_of" negation="false" src="d0_s14" to="o14745" />
      <relation from="o14745" id="r1016" name="in" negation="false" src="d0_s14" to="o14746" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>412.</number>
  <discussion>Species 1.</discussion>
  <discussion>The close relationship between Garberia and Liatris has been long recognized. T. Nuttall (1822) included G. heterophylla in Liatris sect. Leptoclinium (as L. fruticosa Nuttall). Garberia is distinct by its shrubby habit and karyotype (L. O. Gaiser 1954).</discussion>
  <references>
    <reference>Curtiss, A. H. 1881. Chapmannia and Garberia. Bot. Gaz. 6: 257–259.</reference>
  </references>
  
</bio:treatment>