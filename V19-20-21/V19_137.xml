<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">hookerianum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 418. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species hookerianum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066375</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Rydberg) Petrak" date="unknown" rank="species">kelseyi</taxon_name>
    <taxon_hierarchy>genus Cirsium;species kelseyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or monocarpic (sometimes polycarpic?) perennials, 20–150 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o6737" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1 and erect, less commonly several and ascending, simple to sparingly short-branched in distal 1/2, variably villous with jointed trichomes, and/or finely arachnoid, or ± glabrate;</text>
      <biological_entity id="o6739" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6740" name="1/2" name_original="1/2" src="d0_s2" type="structure">
        <character char_type="range_value" from="simple" name="architecture" src="d0_s2" to="sparingly short-branched" />
        <character constraint="with trichomes" constraintid="o6741" is_modifier="false" modifier="variably" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o6741" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="jointed" value_original="jointed" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches on distal stems 0–many, short, ascending.</text>
      <biological_entity id="o6742" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6743" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="false" name="quantity" src="d0_s3" to="many" />
      </biological_entity>
      <relation from="o6742" id="r638" name="on" negation="false" src="d0_s3" to="o6743" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades linear-oblong to elliptic, 5–25 × 1–8 cm, subentire to coarsely dentate or deeply pinnatifid, lobes lance-oblong to broadly triangular, spinulose to spiny-dentate or shallowly lobed, main spines 2–10 mm, abaxial faces usually ± densely gray or white-tomentose with felted arachnoid trichomes, ± villous to tomentose along major veins with septate trichomes, sometimes glabrous or glabrate, adaxial ± green, glabrous to thinly arachnoid, often ± villous or tomentose with septate trichomes;</text>
      <biological_entity id="o6744" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6745" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="coarsely dentate or deeply pinnatifid" />
      </biological_entity>
      <biological_entity id="o6746" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="lance-oblong" name="shape" src="d0_s4" to="broadly triangular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spiny-dentate" value_original="spiny-dentate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="main" id="o6747" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6748" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually more or less densely" name="coloration" src="d0_s4" value="gray" value_original="gray" />
        <character constraint="with trichomes" constraintid="o6749" is_modifier="false" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
        <character char_type="range_value" constraint="along veins" constraintid="o6750" from="less villous" name="pubescence" notes="" src="d0_s4" to="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o6749" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="felted" value_original="felted" />
        <character is_modifier="true" name="pubescence" src="d0_s4" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
      <biological_entity id="o6750" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o6751" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6752" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="thinly arachnoid" />
        <character is_modifier="false" modifier="often more or less" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character constraint="with trichomes" constraintid="o6753" is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o6753" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
      <relation from="o6750" id="r639" name="with" negation="false" src="d0_s4" to="o6751" />
    </statement>
    <statement id="d0_s5">
      <text>basal often present at flowering, spiny winged-petiolate or sessile;</text>
      <biological_entity id="o6754" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o6755" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="often" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline well distributed, proximally winged-petiolate, distally sessile, gradually reduced, bases sometimes short-decurrent;</text>
      <biological_entity id="o6756" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o6757" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o6758" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal ± reduced, often narrower than proximal, sometimes with non-pigmented bases, sometimes pectinately spiny.</text>
      <biological_entity id="o6759" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o6760" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character constraint="than proximal leaves" constraintid="o6761" is_modifier="false" name="width" src="d0_s7" value="often narrower" value_original="often narrower" />
        <character is_modifier="false" modifier="sometimes pectinately" name="architecture_or_shape" notes="" src="d0_s7" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6761" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o6762" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="non-pigmented" value_original="non-pigmented" />
      </biological_entity>
      <relation from="o6760" id="r640" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o6762" />
    </statement>
    <statement id="d0_s8">
      <text>Heads 1–many, borne singly or crowded in spiciform, racemiform, subcapitate, or sometimes more openly branched corymbiform arrays.</text>
      <biological_entity id="o6763" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s8" to="many" />
        <character is_modifier="false" modifier="singly" name="arrangement" src="d0_s8" value="or" value_original="or" />
        <character constraint="in spiciform" is_modifier="false" name="arrangement" src="d0_s8" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="racemiform" value_original="racemiform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="subcapitate" value_original="subcapitate" />
        <character is_modifier="false" modifier="sometimes; openly" name="architecture" src="d0_s8" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles 0–8+ cm.</text>
      <biological_entity id="o6764" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="8" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres (green or often purplish), broadly ovoid, 2–3.3 × 1.5–4 cm, loosely to densely villous with septate trichomes to tomentose and/or arachnoid.</text>
      <biological_entity id="o6765" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="3.3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="4" to_unit="cm" />
        <character constraint="with trichomes" constraintid="o6766" is_modifier="false" modifier="loosely to densely" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o6766" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="septate" value_original="septate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 4–8 series, imbricate to subequal, bases short-appressed, entire, abaxial faces with or without narrow glutinous ridge, apices stiffly spreading to ascending, linear, long, plane, spines straight, slender, 3–5 mm;</text>
      <biological_entity id="o6767" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o6768" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="8" />
      </biological_entity>
      <biological_entity id="o6769" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="short-appressed" value_original="short-appressed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6770" name="face" name_original="faces" src="d0_s11" type="structure" />
      <biological_entity id="o6771" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <biological_entity id="o6772" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="stiffly spreading" name="orientation" src="d0_s11" to="ascending" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s11" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o6773" name="spine" name_original="spines" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o6767" id="r641" name="in" negation="false" src="d0_s11" to="o6768" />
      <relation from="o6770" id="r642" name="with or without" negation="false" src="d0_s11" to="o6771" />
    </statement>
    <statement id="d0_s12">
      <text>apices of inner flexuous, sometimes expanded and erose.</text>
      <biological_entity id="o6774" name="apex" name_original="apices" src="d0_s12" type="structure" constraint="phyllary" constraint_original="phyllary; phyllary">
        <character is_modifier="false" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s12" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s12" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6775" name="phyllary" name_original="phyllaries" src="d0_s12" type="structure" />
      <relation from="o6774" id="r643" name="part_of" negation="false" src="d0_s12" to="o6775" />
    </statement>
    <statement id="d0_s13">
      <text>Corollas white, ochroleucous, or occasionally pink, 20–28 mm, tubes 10–13 mm, throats 6.5–9 mm, lobes 5–7 mm;</text>
      <biological_entity id="o6776" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6777" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6778" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6779" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style tips 3–5.5 mm.</text>
      <biological_entity constraint="style" id="o6780" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae dark-brown, 5–6.5 mm, apical collars not differentiated;</text>
      <biological_entity id="o6781" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o6782" name="collar" name_original="collars" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s15" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 18–22 mm. 2n = 34.</text>
      <biological_entity id="o6783" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s16" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6784" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist soil, grasslands, aspen parkland, forest edges and openings, subalpine, alpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist soil" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="aspen parkland" />
        <character name="habitat" value="forest edges" />
        <character name="habitat" value="openings" />
        <character name="habitat" value="subalpine" />
        <character name="habitat" value="alpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Idaho, Mont., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48.</number>
  <other_name type="past_name">Circium</other_name>
  <other_name type="common_name">Hooker’s or white thistle</other_name>
  <discussion>Cirsium hookerianum occurs from the Canadian Coast Ranges of British Columbia east to the northern Cascade Range and the northern Rocky Mountains. The relationship between C. hookerianum, C. kelseyi, which I have tentatively included in C. hookerianum, and C. longistylum needs further investigation. A case could be made for including all three in an expanded concept of C. hookerianum, but more investigation of the variation patterns is needed before this is done. Certainly C. kelseyi is better treated within or as a close ally of C. hookerianum than in C. scariosum (var. scariosum), where R. J. Moore and C. Frankton (1974) synonymized it. Cirsium hookerianum is known to hybridize with C. undulatum.</discussion>
  
</bio:treatment>