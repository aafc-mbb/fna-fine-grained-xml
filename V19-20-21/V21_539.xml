<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">PECTIS</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1221. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus PECTIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pecten, comb, alluding to ciliate leaf margins</other_info_on_name>
    <other_info_on_name type="fna_id">124224</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 1–120 cm (herbage often lemon or spicy scented).</text>
      <biological_entity id="o17218" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to erect, simple to much branched.</text>
      <biological_entity id="o17220" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple to much" value_original="simple to much" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>usually sessile;</text>
      <biological_entity id="o17221" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly linear to elliptic, oblanceolate, oblong, or obovate, margins usually setose-ciliate (mostly near bases), faces glabrous or hairy (abaxial and/or margins dotted with oil-glands).</text>
      <biological_entity id="o17222" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear to elliptic" value_original="linear to elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o17223" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s5" value="setose-ciliate" value_original="setose-ciliate" />
      </biological_entity>
      <biological_entity id="o17224" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in congested to open, cymiform arrays (peduncles usually bracteate).</text>
      <biological_entity id="o17225" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in arrays" constraintid="o17226" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in congested to open , cymiform arrays" />
      </biological_entity>
      <biological_entity id="o17226" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="congested" is_modifier="true" name="architecture" src="d0_s6" to="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0.</text>
      <biological_entity id="o17227" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres campanulate, cylindric, ellipsoid, or fusiform, 2–8+ mm diam.</text>
      <biological_entity id="o17228" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 3–15 [–21] in 1 series (usually distinct, falling individually, each with a ray cypsela, sometimes cohering at bases, falling together as units enclosing all cypselae of a head; individually convex, indurate-keeled, narrowly to broadly hyaline-margined, apices often ciliolate, bearing oil-glands on margins and/or faces).</text>
      <biological_entity id="o17229" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="21" />
        <character char_type="range_value" constraint="in series" constraintid="o17230" from="3" name="quantity" src="d0_s9" to="15" />
      </biological_entity>
      <biological_entity id="o17230" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to hemispheric, smooth or pitted, epaleate.</text>
      <biological_entity id="o17231" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="hemispheric" />
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 3–15 [–21], pistillate, fertile (inserted on phyllary bases);</text>
      <biological_entity id="o17232" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="21" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, often reddened abaxially (often drying white to purplish; laminae elliptic, entire or 2-lobed or 3-lobed, glabrous or proximally glandular-puberulent).</text>
      <biological_entity id="o17233" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often; abaxially" name="coloration" src="d0_s12" value="reddened" value_original="reddened" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets [1–] 3–55 [–100], usually bisexual;</text>
      <biological_entity id="o17234" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s13" to="3" to_inclusive="false" />
        <character char_type="range_value" from="55" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="100" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="55" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow (sometimes drying white to purplish), tubes shorter than narrowly funnelform throats, lobes (4–) 5, deltate to lanceovate (lobes ± equal and corollas actinomorphic, not 2-lipped, or lobes unequal with 3 or 4 forming an adaxial lip opposite 1-lobed lip and corollas zygomorphic, 2-lipped, all lobes glabrous or proximally glandular-puberulent; anther bases rounded or subcordate, apical appendages rounded or emarginate; styles included to long-exserted, branches ± papillose knobs).</text>
      <biological_entity id="o17235" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o17236" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than narrowly funnelform throats" constraintid="o17237" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17237" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o17238" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s14" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s14" to="lanceovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (blackish or dark-brown) cylindric to narrowly clavate, ribbed or angled, puberulent to pilose;</text>
      <biological_entity id="o17239" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s15" to="narrowly clavate ribbed or angled" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s15" to="narrowly clavate ribbed or angled" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s15" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, usually of awns, bristles, or scales, sometimes coroniform.</text>
      <biological_entity id="o17241" name="awn" name_original="awns" src="d0_s16" type="structure" />
      <biological_entity id="o17242" name="bristle" name_original="bristles" src="d0_s16" type="structure" />
      <biological_entity id="o17243" name="scale" name_original="scales" src="d0_s16" type="structure" />
      <relation from="o17240" id="r1187" modifier="usually" name="consists_of" negation="false" src="d0_s16" to="o17241" />
      <relation from="o17240" id="r1188" modifier="usually" name="consists_of" negation="false" src="d0_s16" to="o17242" />
      <relation from="o17240" id="r1189" modifier="usually" name="consists_of" negation="false" src="d0_s16" to="o17243" />
    </statement>
    <statement id="d0_s17">
      <text>x = 12.</text>
      <biological_entity id="o17240" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="sometimes" name="shape" notes="" src="d0_s16" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o17244" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Pacific Islands (Galapagos Islands, Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Galapagos Islands)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>316.</number>
  <discussion>Species about 90 (13, including 1 hybrid, in the flora).</discussion>
  <discussion>Pectis is unusual among Compositae in having the C4 photosynthetic pathway and the accompanying Kranz anatomy—leaves with vascular bundle sheath cells that contain numerous chloroplasts (B. N. Smith and B. L. Turner 1975). All of the closely related genera have the C3 pathway. The selective advantage imparted by C4 photosynthesis (greatly reduced photorespiration in bundle sheath cells) has enabled Pectis species to occupy a variety of hot, dry habitats including deserts, tropical and subtropical grasslands, arid scrublands, and tropical beaches. Some species of Pectis grow in hot desert areas of the western United States following summer precipitation.</discussion>
  <discussion>Leaves and phyllaries in all Pectis species are dotted with embedded pellucid glands (here called oil-glands). In some species, the liquid within the schizogenous cavities includes a mixture of strongly scented monoterpenes; in other species the gland contents have little or no aroma. Herbage containing strongly scented essential oils are described as “lemon-scented” when citral is the predominant compound and “spicy-scented” when other oils are predominant. For most taxa the odor (or lack thereof) of the crushed herbage is a readily apparent field characteristic. Unfortunately, labels of most herbarium specimens lack information on odor. Intact glands on herbarium specimens may retain liquid contents for years; odors may change as the specimens age. On living specimens, the glands are translucent; on herbarium specimens, they tend to be golden brown or blackish. The glands probably function as a deterrent to herbivores, including insects.</discussion>
  <discussion>Ray florets of Pectis are inserted directly onto the bases of the phyllaries, and the phyllaries and ray cypselae tend to fall together when the heads shatter at maturity. The numbers of phyllaries and ray florets per Pectis capitulum vary along the Fibonacci series and rarely deviate from the sequence (i.e., in Pectis, the modal numbers of phyllaries and rays are 3, 5, 8, 13, and 21).</discussion>
  <discussion>Pectis coulteri Harvey &amp; A. Gray has been attributed in floras to California and Arizona on the basis of the ambiguously labeled type collection (“California,” without locality or date, T. Coulter 331, holotype TCD) and speculations as to its origin. This species is known to occur only from central Sonora to northern Sinaloa, Mexico, and has never been documented in the United States. The type was probably collected in Sonora (D. J. Keil 1975).</discussion>
  <references>
    <reference>Keil, D. J. 1975. Revision of Pectis sect. Heteropectis (Compositae: Tageteae). Madroño 23: 181–191.</reference>
    <reference>Keil, D. J. 1977. A revision of Pectis section Pectothrix (Compositae: Tageteae). Rhodora 79: 32–78.</reference>
    <reference>Keil, D. J. 1977b. Chromosome studies in North and Central American species of Pectis L. (Compositae: Tageteae). Rhodora 79: 79–94.</reference>
    <reference>Keil, D. J. 1978. Revision of Pectis section Pectidium (Compositae: Tageteae). Rhodora 80: 135–146.</reference>
    <reference>Keil, D. J. 1986. Synopsis of the Florida species of Pectis (Asteraceae). Sida 11: 385–395.</reference>
    <reference>Keil, D. J. 1996. Pectis. In: B. L. Turner, ed. 1996+. The Comps of Mexico: A Systematic Account of the Family Asteraceae. 2+ vols. Huntsville, Tex. Vol. 6, pp. 22–43. [Phytologia Mem. 10, 11.]</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray florets (7–)8–13(–15)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray florets 3–5</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads in congested or open, cymiform arrays; peduncles 1–40 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads borne singly or in open, cymiform arrays; peduncles 20–160 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pappi of disc cypselae usually of 16–24 subplumose bristles, rarely coroniform; cypselae strigillose to short-pilose (hair tips curled, bulbous)</description>
      <determination>9 Pectis papposa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pappi of disc cypselae coroniform and/or of 1–7 scabrid awns or bristles; cypselae strigillose (hair tips straight, forked)</description>
      <determination>10 Pectis angustifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Perennials (rhizomatous); heads borne singly; peduncles 30–160 mm; ray florets (8–)13(–15)</description>
      <determination>7 Pectis longipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Annuals (taprooted); heads borne singly or in open, (leafy) cymiform arrays; ray florets 8(–13)</description>
      <determination>8 Pectis rusbyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Disc corollas not 2-lipped (actinomorphic, lobes equal, 4 or all 5 each bearing an oil-gland); leaf margins with 0–1 pairs of setae; pappi coroniform or of awns</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Disc corollas ± 2-lipped (zygomorphic, lobes not bearing oil-glands); leaf margins with 1–12 pairs of setae; pappi usually of bristles, scales, or awns, sometimes coroniform plus 0–3 awns or bristles (P. filipes)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perennials; ray corollas 6–11 mm</description>
      <determination>12 Pectis imberbis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Annuals; ray corollas 2–3 mm</description>
      <determination>13 Pectis linifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Phyllaries coherent (at least at bases, falling together as units enclosing all cypselae of a head)</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Phyllaries distinct (spreading and falling individually, each with a ray cypsela)</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Ray florets 3(–4)</description>
      <determination>2 Pectis cylindrica</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Ray florets 5</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Peduncles 1–2 mm; pappi of lanceolate scales (ovaries usually forming cypselae)</description>
      <determination>1 Pectis prostrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Peduncles 5–25 mm; pappi of slender, aristate scales (pericarps darkening, not swelling, ovules abortive)</description>
      <determination>6 Pectis ×floridana</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves oblong-oblanceolate to obovate; herbage not scented</description>
      <determination>3 Pectis humifusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves linear, linear-oblanceolate, narrowly elliptic, or narrowly linear; herbage lemon-scented or spicy-scented</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Peduncles 0–1 mm</description>
      <determination>5 Pectis linearifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Peduncles 3–65 mm</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pappi of 0–5, antrorsely scabrid bristles or slender scales 1–2 mm plus 0–5, entire or irregularly lacerate scales 0.2–0.7 mm; Florida</description>
      <determination>4 Pectis glaucescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pappi of 0–3 awns 3–4 mm, usually with a crown of shorter scales; sw United States</description>
      <determination>11 Pectis filipes</determination>
    </key_statement>
  </key>
</bio:treatment>