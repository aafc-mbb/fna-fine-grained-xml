<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray in A. Gray et al." date="1884" rank="section">Silphidium</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(2): 294. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section Silphidium</taxon_hierarchy>
    <other_info_on_name type="fna_id">316953</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="unranked">Silphidium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 341. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Coreopsis;unranked Silphidium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o23445" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline, opposite;</text>
      <biological_entity id="o23446" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades usually simple, sometimes with 1–2 auricles at base (margins dentate).</text>
      <biological_entity id="o23447" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o23448" name="auricle" name_original="auricles" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
      <biological_entity id="o23449" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o23447" id="r1608" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o23448" />
      <relation from="o23448" id="r1609" name="at" negation="false" src="d0_s2" to="o23449" />
    </statement>
    <statement id="d0_s3">
      <text>Calyculi of 3–8+ bractlets.</text>
      <biological_entity id="o23450" name="calyculus" name_original="calyculi" src="d0_s3" type="structure" />
      <biological_entity id="o23451" name="bractlet" name_original="bractlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="8" upper_restricted="false" />
      </biological_entity>
      <relation from="o23450" id="r1610" name="consist_of" negation="false" src="d0_s3" to="o23451" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 5.</text>
      <biological_entity id="o23452" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 4–5, neuter;</text>
      <biological_entity id="o23453" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminae ± elliptic to oblong, broadest at or near middle, apices entire or 2–3-toothed.</text>
      <biological_entity id="o23454" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="less elliptic" name="shape" src="d0_s6" to="oblong" />
        <character constraint="at middle" constraintid="o23455" is_modifier="false" name="width" src="d0_s6" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o23455" name="middle" name_original="middle" src="d0_s6" type="structure" />
      <biological_entity id="o23456" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="2-3-toothed" value_original="2-3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 10–15+;</text>
      <biological_entity id="o23457" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas ochroleucous or pale-yellow, lobes 5;</text>
      <biological_entity id="o23458" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o23459" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style-branch apices ± conic-attenuate.</text>
      <biological_entity constraint="style-branch" id="o23460" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="conic-attenuate" value_original="conic-attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae ± linear.</text>
      <biological_entity id="o23461" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± linear, margins thin, not winged, not ciliolate, faces smooth, glabrous;</text>
      <biological_entity id="o23462" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o23463" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s11" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o23464" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi 0 or (1–) 2 cusps (0.1–0.2+ mm).</text>
      <biological_entity id="o23466" name="cusp" name_original="cusps" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>x = 13.</text>
      <biological_entity id="o23465" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s12" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="x" id="o23467" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>308d.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>