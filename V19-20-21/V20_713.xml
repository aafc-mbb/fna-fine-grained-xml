<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">319</other_info_on_meta>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Shinners" date="1947" rank="species">geiseri</taxon_name>
    <place_of_publication>
      <publication_title>Wrightia</publication_title>
      <place_in_publication>1: 183. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species geiseri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066601</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–40 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>slenderly taprooted, caudices simple.</text>
      <biological_entity id="o12740" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="slenderly" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o12741" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect (herbaceous), sparsely spreading to deflexed-villous proximally, sparsely strigose (hairs straight) distally, eglandular.</text>
      <biological_entity id="o12742" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s2" value="deflexed-villous" value_original="deflexed-villous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (usually not persistent) and cauline;</text>
      <biological_entity id="o12743" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades oblanceolate to subspatulate, 15–60 × 4–11 mm, cauline gradually reduced distally (sometimes barely subclasping), margins entire or with 2–4 pairs of teeth or lobes, faces hirsute, eglandular.</text>
      <biological_entity constraint="basal" id="o12744" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="subspatulate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o12745" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o12746" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with 2-4 pairs" value_original="with 2-4 pairs" />
      </biological_entity>
      <biological_entity id="o12747" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o12748" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity id="o12749" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o12750" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o12746" id="r1158" name="with" negation="false" src="d0_s4" to="o12747" />
      <relation from="o12747" id="r1159" name="part_of" negation="false" src="d0_s4" to="o12748" />
      <relation from="o12747" id="r1160" name="part_of" negation="false" src="d0_s4" to="o12749" />
    </statement>
    <statement id="d0_s5">
      <text>Heads 1–20 in diffuse arrays (branches from midstem distally, peduncles often dilated near heads).</text>
      <biological_entity id="o12751" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o12752" from="1" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o12752" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 2.5–3.5 × 5–7 mm.</text>
      <biological_entity id="o12753" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, sparsely hirsute, sometimes minutely glandular.</text>
      <biological_entity id="o12754" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o12755" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o12754" id="r1161" name="in" negation="false" src="d0_s7" to="o12755" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 44–70;</text>
      <biological_entity id="o12756" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="44" name="quantity" src="d0_s8" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, drying white to pink, 3–6 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o12757" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="drying" value_original="drying" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pink" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12758" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 1.2–1.6 mm (throats slightly indurate and inflated).</text>
      <biological_entity constraint="disc" id="o12759" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 0.7–0.9 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o12760" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o12761" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of scales, inner of 9–11 bristles.</text>
      <biological_entity id="o12762" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o12763" name="scale" name_original="scales" src="d0_s12" type="structure" />
      <biological_entity id="o12764" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s12" to="11" />
      </biological_entity>
      <relation from="o12762" id="r1162" name="outer of" negation="false" src="d0_s12" to="o12763" />
      <relation from="o12762" id="r1163" name="inner of" negation="false" src="d0_s12" to="o12764" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="late Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy prairies, sandy loam, clay, fields, roadsides, fencerows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy prairies" />
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fencerows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>110.</number>
  <other_name type="common_name">Geiser’s fleabane</other_name>
  <discussion>Erigeron geiseri is characterized by its annual duration and slender taproots, relatively small size, lobed basal leaves, peduncles often dilated near the heads, obovate phyllaries with broad, scarious margins, and relatively small disc florets and cypselae, the latter with pappi of conspicuous scales and relatively few bristles.</discussion>
  
</bio:treatment>