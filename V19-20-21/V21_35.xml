<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">ambrosiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">parthenium</taxon_name>
    <taxon_name authority="A. Gray" date="1882" rank="species">confertum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 216. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ambrosiinae;genus parthenium;species confertum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067287</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parthenium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">confertum</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="variety">divaricatum</taxon_name>
    <taxon_hierarchy>genus Parthenium;species confertum;variety divaricatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parthenium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">confertum</taxon_name>
    <taxon_name authority="(A. Gray) Rollins" date="unknown" rank="variety">lyratum</taxon_name>
    <taxon_hierarchy>genus Parthenium;species confertum;variety lyratum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parthenium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">confertum</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="variety">microcephalum</taxon_name>
    <taxon_hierarchy>genus Parthenium;species confertum;variety microcephalum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials (sometimes flowering first-year or persisting), 10–30 (–60+) cm.</text>
      <biological_entity id="o16321" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ovate or rounded-deltate to elliptic, 30–80 (–120+) × 10–20 (–40+) mm, usually ± pinnately or sub-bipinnately lobed, ultimate margins entire, faces strigillose, usually also with erect hairs 1–2 mm and glanddotted.</text>
      <biological_entity id="o16322" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded-deltate" name="shape" src="d0_s1" to="elliptic" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="120" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s1" to="80" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s1" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="usually more or less; more or less pinnately; sub-bipinnately; sub-bipinnately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16323" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o16324" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o16325" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o16324" id="r1125" modifier="usually" name="with" negation="false" src="d0_s1" to="o16325" />
    </statement>
    <statement id="d0_s2">
      <text>Heads disciform or obscurely radiate, borne in open, ± paniculiform arrays.</text>
      <biological_entity id="o16326" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="disciform" value_original="disciform" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s2" value="radiate" value_original="radiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 2–8 (–12+) mm.</text>
      <biological_entity id="o16327" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries: outer 5 lanceovate to elliptic, 2.5–3+ mm, inner 5 ± orbiculate, 3–3.5 mm.</text>
      <biological_entity id="o16328" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o16329" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16330" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate florets 5;</text>
      <biological_entity id="o16331" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla laminae 0 or ± coroniform, 0.1–0.5 mm.</text>
      <biological_entity constraint="corolla" id="o16332" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="coroniform" value_original="coroniform" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 20–30+.</text>
      <biological_entity id="o16333" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae ± obovoid, 2–3 mm;</text>
      <biological_entity id="o16334" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappuslike enations 2, ± erect, deltate to ovate, 0.5–1 mm (sometimes a third, subulate spur near apex adaxially).</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 36, 68, 72.</text>
      <biological_entity id="o16335" name="enation" name_original="enations" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="pappuslike" value_original="pappuslike" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16336" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
        <character name="quantity" src="d0_s10" value="68" value_original="68" />
        <character name="quantity" src="d0_s10" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy plains, openings in mesquite grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy plains" />
        <character name="habitat" value="openings" constraint="in mesquite grasslands" />
        <character name="habitat" value="mesquite grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>