<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">409</other_info_on_meta>
    <other_info_on_meta type="mention_page">410</other_info_on_meta>
    <other_info_on_meta type="treatment_page">408</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaenactis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Chaenactis</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus chaenactis;section Chaenactis</taxon_hierarchy>
    <other_info_on_name type="fna_id">316959</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
      <biological_entity id="o16978" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal indument predominantly arachnoid, lanuginose, stipitate-glandular, or glabrescent, not farinose.</text>
      <biological_entity constraint="proximal" id="o16979" name="indument" name_original="indument" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="predominantly" name="pubescence" src="d0_s1" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="lanuginose" value_original="lanuginose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="farinose" value_original="farinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems mostly 1–5 (–12), usually erect to ascending;</text>
      <biological_entity id="o16980" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="12" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="usually erect" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches proximal and/or distal.</text>
      <biological_entity id="o16981" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16982" name="stem" name_original="stems" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: largest blades usually ± oblanceolate to elliptic or linear (± ovate in some C. macrantha), (0–) 1–2-pinnately lobed, not glanddotted (except C. macrantha).</text>
      <biological_entity id="o16983" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest" id="o16984" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="(0-)1-2-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads discoid or ± radiant.</text>
      <biological_entity id="o16985" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s5" value="radiant" value_original="radiant" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles erect to ascending (by flowering).</text>
      <biological_entity id="o16986" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries: outer usually ± blunt, sometimes acute.</text>
      <biological_entity id="o16987" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity constraint="outer" id="o16988" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="shape" src="d0_s7" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets: corollas white to pinkish, cream, or bright to dark yellow, actinomorphic and ± equal, or peripheral enlarged (and often zygomorphic).</text>
      <biological_entity id="o16989" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity id="o16990" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="cream" value_original="cream" />
        <character is_modifier="false" name="reflectance" src="d0_s8" value="bright" value_original="bright" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark yellow" value_original="dark yellow" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="actinomorphic" value_original="actinomorphic" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="peripheral" id="o16991" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae ± terete (except C. nevii);</text>
      <biological_entity id="o16992" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 0, coroniform (of ± 10 scales), or of (1–) 4–8 (–14) scales in 1 series, in 2, abruptly unequal series, or in 2–3, gradually unequal series.</text>
      <biological_entity id="o16994" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="atypical_quantity" src="d0_s10" to="4" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="14" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="8" />
      </biological_entity>
      <biological_entity id="o16995" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16996" name="series" name_original="series" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" modifier="abruptly" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o16997" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="true" modifier="gradually" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o16993" id="r1169" name="consist_of" negation="false" src="d0_s10" to="o16994" />
      <relation from="o16994" id="r1170" name="in" negation="false" src="d0_s10" to="o16995" />
      <relation from="o16993" id="r1171" name="in" negation="false" src="d0_s10" to="o16996" />
      <relation from="o16993" id="r1172" name="in" negation="false" src="d0_s10" to="o16997" />
    </statement>
    <statement id="d0_s11">
      <text>x = 6.</text>
      <biological_entity id="o16993" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s10" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o16998" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>378c.</number>
  <discussion>Species 7 (7 in the flora).</discussion>
  <discussion>Taxa of sect. Chaenactis grow mainly in valley and foothill habitats; all except Chaenactis macrantha, C. cusickii, and C. nevii appear to form occasional natural hybrids where sympatric. Chaenactis macrantha and C. cusickii may each warrant a separate monotypic section and here probably render sect. Chaenactis polyphyletic. Both species share similarities with C. xantiana.</discussion>
  <discussion>The species with zygomorphic and enlarged peripheral corollas (Chaenactis glabriuscula, C. stevioides, and C. fremontii) are closely related, and identification of some specimens requires careful evaluation, especially when fresh corolla color is not evident. D. W. Kyhos (1965) presented strong evidence that the species with corollas white and 2n = 10 (C. stevioides and C. fremontii) evolved separately from within C. glabriuscula (corollas yellow, 2n = 12) via chromosomal rearrangements. That study did not sample very well the large amount of variation in C. glabriuscula, nor did it rule out involvement of other taxa such as C. xantiana. Chaenactis xantiana (corollas white, 2n = 14) is known to form hybrids with C. glabriuscula that also exhibit 2n = 10 in some individuals (P. Stockwell 1940).</discussion>
  <discussion>A series of natural hybrids and backcrosses (keyed below), involving various combinations of the four species named in the preceding paragraph, appears to occur in the inner South Coast Ranges and adjacent southern and western San Joaquin Valley of west-central California. Where Chaenactis xantiana or C. glabriuscula var. heterocarpha is involved, a partial second series of pappus scales is usually evident. Identification of specimens from this area can be difficult.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas bright to dark yellow; mainly west of deserts</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas white to pinkish, cream, or pale yellow; mainly in and near deserts</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peripheral corollas actinomorphic, scarcely enlarged (relative to inner); cypselae com-pressed; pappi 0 or coroniform (of ± 10 scales, longest 0.1–0.5 mm); Oregon</description>
      <determination>14 Chaenactis nevii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peripheral corollas ± zygomorphic, enlarged (relative to inner); cypselae ± terete; pappi of (1–)4, or (5–)8, scales, longest scales (1–)2–8 mm; California</description>
      <determination>15 Chaenactis glabriuscula</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peripheral corollas zygomorphic, enlarged; pappi of (1–)4(–5) scales, usually in 1 series (rarely with partial outer series in Chaenactis stevioides); peduncles usually stipitate-glandular distally (at least early, sometimes glabrescent by fruit)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peripheral corollas actinomorphic, not or scarcely enlarged; pappi of (6–)8–14 scales in 2 abruptly unequal series, or in 2–3 gradually unequal series; peduncles ± arachnoid to lanuginose distally, not stipitate-glandular</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pappi: longest scales 1.5–6 mm, lengths mostly 0.3–0.9 times corollas (apices hidden among corollas at flowering); proximal indument ± arachnoid-sericeous (tardily glabrescent except around nodes); leaf blades mostly 1–2-pinnately lobed, primary lobes mostly 4–8 pairs; outer phyllaries usually stipitate-glandular and/or± arachnoid in fruit, apices blunt</description>
      <determination>16 Chaenactis stevioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pappi: longest scales 6–8.5 mm, lengths 1–1.3 times corollas (apices visible among corollas at flowering); proximal indument glabrescent (early ± arachnoid, glabrous by flowering); leaf blades 0–1-pinnately lobed, lobes 1–2(–5) pairs; outer phyllaries usually glabrescent in fruit, apices acute</description>
      <determination>17 Chaenactis fremontii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas (nocturnal) 9–12(–15) mm (lengths 1.8–2.2 times cypselae; anthers ± included); leaf blades not succulent</description>
      <determination>11 Chaenactis macrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas (diurnal) 3–10 mm (± equaling cypselae; anthers exserted); leaf blades± succulent</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Longest phyllaries 10–18 mm (surpassed by florets), outer distally tomentulose-puberulent in fruit (proximally glabrous), not stipitate-glandular</description>
      <determination>13 Chaenactis xantiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Longest phyllaries 6–9(–10) mm (or surpassing florets), outer (uniformly) ± arachnoid and/or stipitate-glandular or glabrescent</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Pappi of (8–)10–14 scales in 2–3 gradually unequal series; leaf blades ± oblanceolate, not lobed (margins entire or distally ± crenate); peduncles: bracts 1–2, leaflike, surpassing heads; outer phyllaries sparsely arachnoid to glabrescent, not stipitate-glandular (inner apically brownish villosulous)</description>
      <determination>12 Chaenactis cusickii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Pappi of 6–8 scales in 2, abruptly unequal series; leaves mostly ± elliptic, mostly 1–2-pinnately lobed; peduncles: bracts 0 or 1–2 ± subulate, surpassed by heads; outer phyllaries usually stipitate-glandular, other indument various (apices of inner not villosulous) [hybrids among Chaenactis fremontii, C. glabriuscula, C. stevioides, and C. xantiana; see discussion of C. sect. Chaenactis]</description>
      <next_statement_id>1</next_statement_id>
    </key_statement>
  </key>
</bio:treatment>