<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">533</other_info_on_meta>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="(Linnaeus) Willdenow" date="1803" rank="species">scariosa</taxon_name>
    <taxon_name authority="(Lunell) E. G. Voss" date="1996" rank="variety">nieuwlandii</taxon_name>
    <place_of_publication>
      <publication_title>Michigan Bot.</publication_title>
      <place_in_publication>34: 139. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species scariosa;variety nieuwlandii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068570</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="(Linnaeus) Hill" date="unknown" rank="species">scariosa</taxon_name>
    <taxon_name authority="Lunell" date="unknown" rank="variety">nieuwlandii</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>2: 176. 1912 (as Laciniaria)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lacinaria;species scariosa;variety nieuwlandii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="(Lunell) Gaiser" date="unknown" rank="species">×nieuwlandii</taxon_name>
    <taxon_hierarchy>genus Liatris;species ×nieuwlandii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="(Lunell) Shinners" date="unknown" rank="species">novae-angliae</taxon_name>
    <taxon_name authority="(Lunell) Shinners" date="unknown" rank="variety">nieuwlandii</taxon_name>
    <taxon_hierarchy>genus Liatris;species novae-angliae;variety nieuwlandii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–100 cm.</text>
      <biological_entity id="o3658" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems with 20–85 leaves or leafy bracts proximal to heads.</text>
      <biological_entity id="o3659" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o3660" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s1" to="85" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character constraint="to heads" constraintid="o3662" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o3661" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s1" to="85" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character constraint="to heads" constraintid="o3662" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o3662" name="head" name_original="heads" src="d0_s1" type="structure" />
      <relation from="o3659" id="r283" name="with" negation="false" src="d0_s1" to="o3660" />
      <relation from="o3659" id="r284" name="with" negation="false" src="d0_s1" to="o3661" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal cauline usually narrowly lanceolate-spatulate, sometimes broader, mostly 100–500 × 25–50 (–55) mm, glabrous or hirtello-puberulent (glanddotted).</text>
      <biological_entity id="o3663" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s2" value="lanceolate-spatulate" value_original="lanceolate-spatulate" />
        <character is_modifier="false" modifier="sometimes" name="width" src="d0_s2" value="broader" value_original="broader" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s2" to="500" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s2" to="50" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirtello-puberulent" value_original="hirtello-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually 9–20.</text>
      <biological_entity id="o3664" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s3" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Florets 30–80.</text>
      <biological_entity id="o3665" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s4" to="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, glades, open woods, bluff ledges, railroads, rocky limestone soils, red clays, jack pine, pine-oak, oak-juniper, oak-hickory, aspen</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="bluff ledges" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="rocky limestone soils" />
        <character name="habitat" value="red clays" />
        <character name="habitat" value="jack pine" />
        <character name="habitat" value="pine-oak" />
        <character name="habitat" value="oak-juniper" />
        <character name="habitat" value="oak-hickory" />
        <character name="habitat" value="aspen" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ill., Ind., Mich., Mo., N.Y., Ohio, Pa., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35b.</number>
  <discussion>Plants of var. nieuwlandii are usually relatively tall and have relatively numerous, even-sized, densely arranged, lanceolate cauline leaves.</discussion>
  
</bio:treatment>