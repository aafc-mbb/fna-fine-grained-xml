<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">132</other_info_on_meta>
    <other_info_on_meta type="mention_page">145</other_info_on_meta>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(G. Don) G. L. Nesom" date="1993" rank="subsection">Venosae</taxon_name>
    <taxon_name authority="unknown" date="1830" rank="series">venosae</taxon_name>
    <taxon_name authority="Muhlenberg ex Willdenow" date="1803" rank="species">ulmifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>3: 2060. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection venosae;series venosae;species ulmifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417300</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Muhlenberg ex Willdenow) Kuntze" date="unknown" rank="species">ulmifolius</taxon_name>
    <taxon_hierarchy>genus Aster;species ulmifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–120 cm;</text>
      <biological_entity id="o16157" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices branching, woody.</text>
      <biological_entity id="o16158" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, erect, glabrous, sparsely hairy in arrays.</text>
      <biological_entity id="o16159" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="in arrays" constraintid="o16160" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16160" name="array" name_original="arrays" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal when present similar to proximal cauline;</text>
      <biological_entity id="o16161" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o16162" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>proximal cauline often withering by flowering, tapering (sometimes rather abruptly) to short, winged petioles, blades ovatelanceolate, 60–100 (–150) × 30–40 (–50) mm, thin, margins coarsely serrate, apices acute, abaxial faces hirsute on main nerves, adaxial sparsely hirsute to somewhat scabrous;</text>
      <biological_entity id="o16163" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal cauline" id="o16164" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by petioles, blades, margins, apices, faces, leaves" constraintid="o16165, o16166, o16167, o16168, o16169, o16170" is_modifier="false" modifier="often" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute to somewhat" value_original="hirsute to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o16165" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o16166" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o16167" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o16168" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o16169" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o16170" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16171" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
      <biological_entity constraint="main" id="o16172" name="nerve" name_original="nerves" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o16173" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
      <relation from="o16165" id="r1479" name="on" negation="false" src="d0_s4" to="o16172" />
      <relation from="o16166" id="r1480" name="on" negation="false" src="d0_s4" to="o16172" />
      <relation from="o16167" id="r1481" name="on" negation="false" src="d0_s4" to="o16172" />
      <relation from="o16168" id="r1482" name="on" negation="false" src="d0_s4" to="o16172" />
      <relation from="o16169" id="r1483" name="on" negation="false" src="d0_s4" to="o16172" />
      <relation from="o16170" id="r1484" name="on" negation="false" src="d0_s4" to="o16172" />
    </statement>
    <statement id="d0_s5">
      <text>mid to distal cauline subsessile to sessile, blades lanceolate, 20–50 × 5–20 mm, gradually reduced distally, margins entire.</text>
      <biological_entity id="o16174" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o16175" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s5" to="sessile" />
      </biological_entity>
      <biological_entity id="o16176" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o16177" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 20–150, secund, in open paniculiform arrays, proximal branches elongate and widely divergent, sometimes pyramidal-secund with proximal branches short and recurved-secund.</text>
      <biological_entity id="o16178" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="150" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o16179" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16180" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
        <character constraint="with proximal branches" constraintid="o16181" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="pyramidal-secund" value_original="pyramidal-secund" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="recurved-secund" value_original="recurved-secund" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16181" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
      </biological_entity>
      <relation from="o16178" id="r1485" name="in" negation="false" src="d0_s6" to="o16179" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 1.7–2 mm, sparsely to moderately short-hispido-strigose, bracteoles 2–7, ovate, grading into phyllaries.</text>
      <biological_entity id="o16182" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s7" value="short-hispido-strigose" value_original="short-hispido-strigose" />
      </biological_entity>
      <biological_entity id="o16183" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o16184" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure" />
      <relation from="o16183" id="r1486" name="into" negation="false" src="d0_s7" to="o16184" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres 3–4 mm.</text>
      <biological_entity id="o16185" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (16–18) in 2–3 series, unequal;</text>
      <biological_entity id="o16186" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o16187" from="16" name="atypical_quantity" src="d0_s9" to="18" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o16187" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ovate, acute, inner linear-lanceolate, obtuse to acute.</text>
      <biological_entity constraint="outer" id="o16188" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16189" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 3–6;</text>
      <biological_entity id="o16190" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 1–2 × 0.5–1 mm.</text>
      <biological_entity id="o16191" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 4–7;</text>
      <biological_entity id="o16192" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2.7–3 mm, lobes 0.5–1.1 mm.</text>
      <biological_entity id="o16193" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16194" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 1–1.6 mm, finely hairy;</text>
      <biological_entity id="o16195" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi ± 2.5 mm.</text>
      <biological_entity id="o16196" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S., Ont.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., Nebr., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49.</number>
  <other_name type="common_name">Elm-leaf goldenrod</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Solidago helleri is possibly a hybrid of S. ulmifolia with S. delicatula.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous or nearly so proximal to arrays; throughout range</description>
      <determination>49a Solidago ulmifolia var. ulmifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems distinctly spreading-hirsute; Arkansas, Mississippi, Missouri</description>
      <determination>49b Solidago ulmifolia var. palmeri</determination>
    </key_statement>
  </key>
</bio:treatment>