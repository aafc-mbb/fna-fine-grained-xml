<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypochaeris</taxon_name>
    <taxon_name authority="(Schultz-Bipontinus) Cabrera" date="1937" rank="species">microcephala</taxon_name>
    <taxon_name authority="(Kuntze) Cabrera" date="1937" rank="variety">albiflora</taxon_name>
    <place_of_publication>
      <publication_title>Notas Mus. La Plata, Bot.</publication_title>
      <place_in_publication>2: 201. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hypochaeris;species microcephala;variety albiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250006611</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypochaeris</taxon_name>
    <taxon_name authority="(Lessing) Grisebach" date="unknown" rank="species">brasiliensis</taxon_name>
    <taxon_name authority="Kuntze" date="unknown" rank="variety">albiflora</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Gen. Pl.</publication_title>
      <place_in_publication>3([3]): 159. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hypochaeris;species brasiliensis;variety albiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–60 cm;</text>
      <biological_entity id="o21208" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots vertical, thick, caudices woody.</text>
      <biological_entity id="o21209" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o21210" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, simple or branched distally, glabrous or sparsely hairy proximally.</text>
      <biological_entity id="o21211" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and proximally cauline;</text>
      <biological_entity id="o21212" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="proximally" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades narrowly oblanceolate, 40–102 (–250) × 10–50 mm, margins entire to sharply dentate or deeply pinnatifid (lobes long and narrow, ciliate), faces glabrous or ± glabrate;</text>
      <biological_entity constraint="basal" id="o21213" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="102" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="250" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="102" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21214" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>(cauline blades not clasping, distally reduced, margins shallowly dentate to coarsely pinnatifid).</text>
      <biological_entity id="o21215" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads borne singly or 2–13 in paniculiform to corymbiform arrays.</text>
      <biological_entity id="o21216" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o21217" from="2" name="quantity" src="d0_s6" to="13" />
      </biological_entity>
      <biological_entity id="o21217" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s6" to="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric or narrowly campanulate, 8–15 (–18) × 5–10 (–12) mm.</text>
      <biological_entity id="o21218" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 20–24, narrowly lanceolate, 1–14 mm, unequal, margins scarious (apices darkened), faces glabrous or sparsely tomentulose.</text>
      <biological_entity id="o21219" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="24" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o21220" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o21221" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 50–100+;</text>
      <biological_entity id="o21222" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white, 5–7 mm, not surpassing phyllaries at flowering.</text>
      <biological_entity id="o21223" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21224" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure" />
      <relation from="o21223" id="r1922" name="surpassing" negation="true" src="d0_s10" to="o21224" />
    </statement>
    <statement id="d0_s11">
      <text>Cypselae monomorphic, all beaked (beaks 4–5 mm);</text>
      <biological_entity id="o21225" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bodies brown, fusiform, 7–8 mm, 10-nerved, muricate-roughened;</text>
      <biological_entity id="o21226" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="10-nerved" value_original="10-nerved" />
        <character is_modifier="false" name="relief_or_texture" src="d0_s12" value="muricate-roughened" value_original="muricate-roughened" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of white, plumose bristles in 1 series, 7–8 mm. 2n = 8, 10.</text>
      <biological_entity id="o21227" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21228" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="shape" src="d0_s13" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity id="o21229" name="series" name_original="series" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21230" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="8" value_original="8" />
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
      <relation from="o21227" id="r1923" name="consists_of" negation="false" src="d0_s13" to="o21228" />
      <relation from="o21228" id="r1924" name="in" negation="false" src="d0_s13" to="o21229" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grazed woodlands, stream bottoms, ditches, sandy or silty soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" modifier="grazed" />
        <character name="habitat" value="stream bottoms" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="silty soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="1" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; La., Okla., Tex.; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  <other_name type="past_name">Hypochoeris</other_name>
  
</bio:treatment>