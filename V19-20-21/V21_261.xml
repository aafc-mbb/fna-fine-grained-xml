<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="(A. Gray) B. L. Robinson &amp; Greenman" date="1899" rank="species">nana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>34: 543. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species nana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067797</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ximenesia</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">encelioides</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">nana</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 92. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ximenesia;species encelioides;variety nana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly 7–15+ cm (perennating bases ± erect, internodes not winged).</text>
      <biological_entity id="o5284" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly opposite (distal sometimes alternate);</text>
      <biological_entity id="o5285" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ± deltate to rhombic, 3–8+ × 2–5+ cm, bases ± cuneate, margins raggedly toothed, apices obtuse to acute, faces strigoso-sericeous.</text>
      <biological_entity id="o5286" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="less deltate" name="shape" src="d0_s2" to="rhombic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="5" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o5287" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o5288" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="raggedly" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o5289" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o5290" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigoso-sericeous" value_original="strigoso-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly or 2–3 together.</text>
      <biological_entity id="o5291" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character char_type="range_value" from="2" modifier="together" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres shallowly bowl-shaped, 15–25+ mm diam.</text>
      <biological_entity id="o5292" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="bowl--shaped" value_original="bowl--shaped" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s4" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 28–35+ in 2–3 series, ± erect, linear to lanceolate, 8–12+ mm.</text>
      <biological_entity id="o5293" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o5294" from="28" name="quantity" src="d0_s5" to="35" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o5294" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 12–16+;</text>
      <biological_entity id="o5295" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s6" to="16" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 10–12+ mm.</text>
      <biological_entity id="o5296" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 80–150+;</text>
      <biological_entity id="o5297" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s8" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow to orange.</text>
      <biological_entity id="o5298" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae blackish, ± elliptic, 6.5–8 mm, faces ± strigillose;</text>
      <biological_entity id="o5299" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="blackish" value_original="blackish" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5300" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s10" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0.5–1+ mm.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 68.</text>
      <biological_entity id="o5301" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5302" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Silty flats, roadsides, desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="silty flats" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  
</bio:treatment>