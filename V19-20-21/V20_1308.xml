<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="treatment_page">587</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Wiegand) W. A. Weber &amp; Á. Löve" date="1981" rank="species">flettii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>49: 46. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species flettii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067246</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Wiegand" date="unknown" rank="species">flettii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>26: 137, plate 355, fig. 2. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species flettii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–40+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous (rhizomes horizontal to erect).</text>
      <biological_entity id="o23781" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 or 2–5 (often scapiform), loosely clustered, glabrous or leaf-axils sparsely hairy.</text>
      <biological_entity id="o23782" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23783" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline) petiolate;</text>
      <biological_entity constraint="basal" id="o23784" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades obovate to ovate or sublyrate, 30–60+ × 10–40+ mm, bases tapering, margins deeply dissected or pinnatifid, ultimate margins crenate to crenate-dentate.</text>
      <biological_entity id="o23785" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="ovate or sublyrate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o23786" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o23787" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o23788" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="crenate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves abruptly reduced or 0 (sessile; ovate to obovate, dissected or pinnatifid).</text>
      <biological_entity constraint="cauline" id="o23789" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 4–10+ in subumbelliform cymiform arrays.</text>
      <biological_entity id="o23790" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o23791" from="4" name="quantity" src="d0_s6" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o23791" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="subumbelliform" value_original="subumbelliform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles inconspicuously bracteate or ebracteate, glabrous.</text>
      <biological_entity id="o23792" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0 or inconspicuous.</text>
      <biological_entity id="o23793" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (8–) 13, light green (tips yellowish), 4–7 mm, glabrous.</text>
      <biological_entity id="o23794" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s9" to="13" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="light green" value_original="light green" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 5 or 8;</text>
      <biological_entity id="o23795" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="5" value_original="5" />
        <character name="quantity" src="d0_s10" unit="or" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 5–10 mm.</text>
      <biological_entity constraint="corolla" id="o23796" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 20–30+;</text>
      <biological_entity id="o23797" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 2.5–3.5 mm, limbs 2.5–3.5 mm.</text>
      <biological_entity id="o23798" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23799" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1–1.5 mm, glabrous;</text>
      <biological_entity id="o23800" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 4–4.5 mm. 2n = 40.</text>
      <biological_entity id="o23801" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23802" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early Jul–mid Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Aug" from="early Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed slopes, rocky or gravelly soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" modifier="exposed" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="gravelly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19</number>
  <other_name type="common_name">Flett’s ragwort</other_name>
  <discussion>Packera flettii is known from the Olympic Mountains and near Mt. Rainer in Washington and coastal mountains of Clatsop County, Oregon. It has a chromosome number unique in the genus and is not known to hybridize with other species of Packera.</discussion>
  
</bio:treatment>