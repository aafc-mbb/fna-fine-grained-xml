<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">499</other_info_on_meta>
    <other_info_on_meta type="treatment_page">501</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">sphaeromeria</taxon_name>
    <taxon_name authority="(D. C. Eaton) Rydberg in N. L. Britton et al." date="1916" rank="species">diversifolia</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 242. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus sphaeromeria;species diversifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067590</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tanacetum</taxon_name>
    <taxon_name authority="D. C. Eaton" date="unknown" rank="species">diversifolium</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>180, plate 19, figs. 1–7. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tanacetum;species diversifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 15–40 cm (aromatic).</text>
      <biological_entity id="o7107" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, glabrous.</text>
      <biological_entity id="o7108" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o7109" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7110" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (green, mostly linear, 15–80 mm) entire or irregularly toothed or lobed, faces glabrous.</text>
      <biological_entity id="o7111" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o7112" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 5–20 (–30) in corymbiform or subumbelliform arrays (1.3–5.6 cm across, as wide as or wider than long).</text>
      <biological_entity id="o7113" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="30" />
        <character char_type="range_value" constraint="in arrays" constraintid="o7114" from="5" name="quantity" src="d0_s4" to="20" />
      </biological_entity>
      <biological_entity id="o7114" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="subumbelliform" value_original="subumbelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 2–4 mm.</text>
      <biological_entity id="o7115" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8–12 (–14), glabrate or sparsely hairy.</text>
      <biological_entity id="o7116" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="14" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="12" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 0.5–1 mm (apices with irregular, thickened rims, faces glabrous or gland-dotted).</text>
      <biological_entity id="o7117" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices of cliffs, steep rocky slopes, usually on limestone, sometimes on quartzite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of cliffs , steep rocky slopes ," />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="steep rocky slopes" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="quartzite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Separateleaf chickensage or false sagebrush</other_name>
  
</bio:treatment>