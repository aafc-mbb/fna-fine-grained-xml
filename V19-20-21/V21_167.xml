<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">76</other_info_on_meta>
    <other_info_on_meta type="illustration_page">76</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="A. Gray &amp; Engelmann" date="1847" rank="genus">lindheimera</taxon_name>
    <taxon_name authority="A. Gray &amp; Engelmann" date="1847" rank="species">texana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>1: 47. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus lindheimera;species texana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220007658</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 4–12 (–18+) × 1–3 (–5+) cm.</text>
      <biological_entity id="o25458" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="18" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s0" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s0" to="5" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 1–3 (–6) cm.</text>
      <biological_entity id="o25459" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries: outer ± lance-linear to linear, 8–12 mm, scabrellous and/or hispid;</text>
      <biological_entity id="o25460" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure" />
      <biological_entity constraint="outer" id="o25461" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="less lance-linear" name="arrangement_or_course_or_shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>inner lanceovate to elliptic, 9–15 mm, scabrellous to glabrate.</text>
      <biological_entity id="o25462" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure" />
      <biological_entity constraint="inner" id="o25463" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Ray laminae 10–14 × 4–9 mm.</text>
      <biological_entity constraint="ray" id="o25464" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Disc corollas 3–4 mm.</text>
      <biological_entity constraint="disc" id="o25465" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 4.5–6 mm, ± scabrellous to glabrate (pappuslike processes 0.5–1 mm).</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 16.</text>
      <biological_entity id="o25466" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s6" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25467" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstones, clays, alkaline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstones" />
        <character name="habitat" value="clays" />
        <character name="habitat" value="alkaline soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Star daisy</other_name>
  <discussion>Leaves, peduncles, and phyllaries of Lindheimera texana sometimes bear stipitate glands reminiscent of glands in some members of Madiinae.</discussion>
  
</bio:treatment>