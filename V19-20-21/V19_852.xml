<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">505</other_info_on_meta>
    <other_info_on_meta type="mention_page">508</other_info_on_meta>
    <other_info_on_meta type="treatment_page">506</other_info_on_meta>
    <other_info_on_meta type="illustration_page">507</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="Besser" date="1829" rank="subgenus">drancunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">campestris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 846. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus drancunculus;species campestris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200023184</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials, (10–) 30–80 (–150) cm, faintly aromatic;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices branched.</text>
      <biological_entity id="o13731" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="faintly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="faintly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o13733" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1–5, turning reddish-brown, (often ribbed) tomentose or glabrous.</text>
      <biological_entity id="o13734" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent or deciduous, mostly basal;</text>
      <biological_entity id="o13735" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o13736" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades 4–12 cm, cauline gradually reduced, 2–4 × 0.5–1.5 cm, 2–3-pinnately lobed, lobes linear to narrowly oblong, apices acute, faces densely to sparsely white-pubescent.</text>
      <biological_entity constraint="basal" id="o13737" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o13738" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="2-3-pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o13739" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly oblong" />
      </biological_entity>
      <biological_entity id="o13740" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13741" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely to sparsely" name="pubescence" src="d0_s4" value="white-pubescent" value_original="white-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (pedunculate) in (mostly leafless) paniculiform arrays.</text>
      <biological_entity id="o13742" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o13743" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o13742" id="r1259" name="in" negation="false" src="d0_s5" to="o13743" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres broadly turbinate, 2.5–3 (–5) × 2–3.5 (–7) mm.</text>
      <biological_entity id="o13744" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries (margins scarious) glabrous or villous-tomentose.</text>
      <biological_entity id="o13745" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous-tomentose" value_original="villous-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets: pistillate 5–20;</text>
      <biological_entity id="o13746" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>functionally staminate 12–30;</text>
      <biological_entity id="o13747" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas pale-yellow, sparsely hairy or glabrous.</text>
      <biological_entity id="o13748" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <biological_entity id="o13749" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae oblong-lanceoloid, somewhat compressed, 0.8–1 mm, faintly nerved, glabrous.</text>
      <biological_entity id="o13750" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong-lanceoloid" value_original="oblong-lanceoloid" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s11" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nunavut, Ont., Que., Sask.; Alaska, Ariz., Ark., Calif., Colo., Conn., Fla., Idaho, Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Minn., Miss., Mo., Mont., N.C., N.Dak., N.H., N.J., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tex., Utah, Vt., Wash., Wis., Wyo.; especially mountains and high latitudes; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="especially mountains and high latitudes" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Field sagewort</other_name>
  <other_name type="common_name">sand wormwood</other_name>
  <discussion>Subspecies ca. 7 (3 in the flora).</discussion>
  <discussion>Artemisia campestris varies; each morphologic form grades into another. The present circumscription is conservative in that only three subspecies are recognized; the subspecies usually can be separated geographically as well as morphologically. Populations in western North America consist primarily of subsp. pacifica; east of the continental divide, plants are assigned to subsp. canadensis in northern latitudes and to subsp. caudata in southern latitudes.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials; stems 2–5; basal rosettes persistent</description>
      <determination>3c Artemisia campestris subsp. pacifica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Biennials; stems 1(–3); basal rosettes not persistent (withering before flowering)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres globose, 3–4 × 3.5–5 mm; n of 50°, primarily Canada</description>
      <determination>3a Artemisia campestris subsp. canadensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucres turbinate, 2–3 × 2–3 mm; s of 50°, e from Rocky Mountains to coastal North America</description>
      <determination>3b Artemisia campestris subsp. caudata</determination>
    </key_statement>
  </key>
</bio:treatment>