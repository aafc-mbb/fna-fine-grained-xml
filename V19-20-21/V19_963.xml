<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">552</other_info_on_meta>
    <other_info_on_meta type="illustration_page">549</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Webb ex Schultz-Bipontinus in P. B. Webb and S. Berthelot" date="1844" rank="genus">argyranthemum</taxon_name>
    <taxon_name authority="(Willdenow) Webb ex Schultz-Bipontinus in P. B. Webb and S. Berthelot" date="1844" rank="species">foeniculaceum</taxon_name>
    <place_of_publication>
      <publication_title>in P. B. Webb and S. Berthelot, Hist. Nat. Îles Canaries</publication_title>
      <place_in_publication>3(2,75): 262. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus argyranthemum;species foeniculaceum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066099</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrethrum</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">foeniculaceum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.,</publication_title>
      <place_in_publication>903. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pyrethrum;species foeniculaceum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Argyranthemum</taxon_name>
    <taxon_name authority="Webb" date="unknown" rank="species">anethifolium</taxon_name>
    <taxon_hierarchy>genus Argyranthemum;species anethifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysanthemum</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">anethifolium</taxon_name>
    <taxon_hierarchy>genus Chrysanthemum;species anethifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysanthemum</taxon_name>
    <taxon_name authority="(Willdenow) Steudel" date="unknown" rank="species">foeniculaceum</taxon_name>
    <taxon_hierarchy>genus Chrysanthemum;species foeniculaceum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves scattered or crowded at bases of peduncles, ± succulent;</text>
      <biological_entity id="o13791" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character constraint="at bases" constraintid="o13792" is_modifier="false" name="arrangement" src="d0_s0" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="more or less" name="texture" notes="" src="d0_s0" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o13792" name="base" name_original="bases" src="d0_s0" type="structure" />
      <biological_entity id="o13793" name="peduncle" name_original="peduncles" src="d0_s0" type="structure" />
      <relation from="o13792" id="r1266" name="part_of" negation="false" src="d0_s0" to="o13793" />
    </statement>
    <statement id="d0_s1">
      <text>blades ± obovate, 20–45 (–100) × 10–30 (–65) mm, (1–) 2–3-pinnately lobed, ultimate margins dentate or entire, faces glabrous, ± glaucous.</text>
      <biological_entity id="o13794" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s1" to="45" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="65" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s1" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="(1-)2-3-pinnately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o13795" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13796" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 5–10 cm.</text>
      <biological_entity id="o13797" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cypselae 2–4 (–6) mm;</text>
      <biological_entity id="o13798" name="cypsela" name_original="cypselae" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pappi 0 (cypselar wall tissue sometimes produced as teeth or crowns at apices of some cypselae).</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 18.</text>
      <biological_entity id="o13799" name="pappus" name_original="pappi" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13800" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Macaronesia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Macaronesia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Cultivars of Argyranthemum foeniculaceum and/or A. frutescens (Linnaeus) Schultz-Bipontinus (similar, not glaucous) are widely used horticulturally in landscaping and for cut flowers and are often sold as florists’ daisies, marguerites, or Paris daisies.</discussion>
  
</bio:treatment>