<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">hymenopappinae</taxon_name>
    <taxon_name authority="L’Héritier" date="1788" rank="genus">hymenopappus</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="species">filifolius</taxon_name>
    <taxon_name authority="(Rydberg) B. L. Turner" date="1956" rank="variety">nanus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>58: 240. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe hymenopappinae;genus hymenopappus;species filifolius;variety nanus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068516</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenopappus</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">nanus</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 53. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenopappus;species nanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–50 cm.</text>
      <biological_entity id="o2748" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal axils ± densely tomentose, terminal lobes 3–15 mm;</text>
      <biological_entity id="o2749" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o2750" name="axil" name_original="axils" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2751" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline 0–2 (–3).</text>
      <biological_entity id="o2752" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o2753" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="3" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1–6.</text>
      <biological_entity id="o2754" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 3–15 cm.</text>
      <biological_entity id="o2755" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 6–9 mm.</text>
      <biological_entity id="o2756" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Florets 30–60;</text>
      <biological_entity id="o2757" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s6" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas pale yellowish, 3–4 mm, throats 1.5–2 mm, lengths 2–4 times lobes;</text>
      <biological_entity id="o2758" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale yellowish" value_original="pale yellowish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2759" name="throat" name_original="throats" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character constraint="lobe" constraintid="o2760" is_modifier="false" name="length" src="d0_s7" value="2-4 times lobes" value_original="2-4 times lobes" />
      </biological_entity>
      <biological_entity id="o2760" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>anthers 2.5–3 mm.</text>
      <biological_entity id="o2761" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 4.5–5.5 mm, hairs 0.1–1 (–2) mm;</text>
      <biological_entity id="o2762" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2763" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi (1–) 1.5–3 mm. 2n = 34.</text>
      <biological_entity id="o2764" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2765" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestones, shales</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestones" />
        <character name="habitat" value="shales" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4k.</number>
  
</bio:treatment>