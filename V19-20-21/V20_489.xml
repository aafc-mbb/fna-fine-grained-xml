<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="treatment_page">221</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Nuttall) Elliott" date="1823" rank="genus">chrysopsis</taxon_name>
    <taxon_name authority="(Michaux) Elliott" date="1823" rank="species">gossypina</taxon_name>
    <taxon_name authority="(Nuttall) Semple" date="1980" rank="subspecies">hyssopifolia</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>58: 148. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysopsis;species gossypina;subspecies hyssopifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068152</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">hyssopifolia</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 67. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysopsis;species hyssopifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">gigantea</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species gigantea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Dress" date="unknown" rank="species">mixta</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species mixta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Nuttall) Elliott" date="unknown" rank="species">trichophylla</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="unknown" rank="variety">hyssopifolia</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species trichophylla;variety hyssopifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diplogon</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze" date="unknown" rank="species">hyssopifolia</taxon_name>
    <taxon_hierarchy>genus Diplogon;species hyssopifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterotheca</taxon_name>
    <taxon_name authority="(Nuttall) R. W. Long" date="unknown" rank="species">hyssopifolia</taxon_name>
    <taxon_hierarchy>genus Heterotheca;species hyssopifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Short-lived perennials (usually), 30–80 cm.</text>
      <biological_entity id="o21496" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5 (older plants), ascending to erect, rarely procumbent, usually simple.</text>
      <biological_entity id="o21497" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="rarely" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: mid to distal cauline sometimes numerous (to 75 on tall stems), sometimes crowded;</text>
      <biological_entity id="o21498" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o21499" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear to lanceolate, margins coarsely long-pilose, faces sparsely pilose to glabrate.</text>
      <biological_entity id="o21500" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21501" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o21502" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s3" value="long-pilose" value_original="long-pilose" />
      </biological_entity>
      <biological_entity id="o21503" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparsely pilose" name="pubescence" src="d0_s3" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads usually in subumbelliform, sometimes in compound, subumbelliform arrays.</text>
      <biological_entity id="o21504" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o21505" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="subumbelliform" value_original="subumbelliform" />
      </biological_entity>
      <relation from="o21504" id="r1984" modifier="in subumbelliform; sometimes" name="in" negation="false" src="d0_s4" to="o21505" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries: apices spreading, faces glabrate.</text>
      <biological_entity id="o21506" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity id="o21507" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>2n = 18.</text>
      <biological_entity id="o21508" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21509" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Sep–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="late Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy and clay soils, open pine flatwoods, oak-pine scrub, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="clay soils" />
        <character name="habitat" value="open pine flatwoods" />
        <character name="habitat" value="oak-pine scrub" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11b.</number>
  <discussion>A few plants similar to subsp. hyssopifolia have been collected in northern peninsular Florida.</discussion>
  
</bio:treatment>