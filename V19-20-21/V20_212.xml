<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="Semple" date="2004" rank="subsection">Multiradiatae</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 760. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection Multiradiatae</taxon_hierarchy>
    <other_info_on_name type="fna_id">316979</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal sometimes present in rosettes at flowering;</text>
      <biological_entity id="o18579" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o18580" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="in rosettes" constraintid="o18581" is_modifier="false" modifier="sometimes" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18581" name="rosette" name_original="rosettes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>proximalmost cauline petiolate, sometimes present at flowering, sometimes largest, not 3-nerved.</text>
      <biological_entity id="o18582" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximalmost cauline" id="o18583" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
        <character constraint="at flowering" is_modifier="false" modifier="sometimes" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s1" value="largest" value_original="largest" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="3-nerved" value_original="3-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads in round-topped corymbiform arrays, not stipitate-glandular.</text>
      <biological_entity id="o18584" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o18585" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="round-topped" value_original="round-topped" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o18584" id="r1723" name="in" negation="false" src="d0_s2" to="o18585" />
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries not striate.</text>
      <biological_entity id="o18586" name="phyllarie" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="coloration_or_pubescence_or_relief" src="d0_s3" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pappus bristles in 2 series (inner moderately to strongly clavate).</text>
      <biological_entity constraint="pappus" id="o18587" name="bristle" name_original="bristles" src="d0_s4" type="structure" />
      <biological_entity id="o18588" name="series" name_original="series" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <relation from="o18587" id="r1724" name="in" negation="false" src="d0_s4" to="o18588" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>163a.1.</number>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>Solidago series Multiradiatae Juzepczuk is an invalid name.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves sharply serrate, acuminate; North Carolina, Tennessee (1600–2000 m)</description>
      <determination>3 Solidago spithamaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal leaves not sharply serrate, acute; across Canada, Alaska, and s to California and New Mexico in Western Cordillera (arctic and alpine habitats)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries subequal, acute to acuminate, arctic and alpine in west, across Canada</description>
      <determination>1 Solidago multiradiata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries unequal, obtuse to rounded; mountains, Quebec and Maine to New York</description>
      <determination>2 Solidago leiocarpa</determination>
    </key_statement>
  </key>
</bio:treatment>