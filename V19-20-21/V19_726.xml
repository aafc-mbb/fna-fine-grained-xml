<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">439</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1828" rank="genus">omalotheca</taxon_name>
    <taxon_name authority="(Gunnerus) Schultz-Bipontinus &amp; F. W. Schultz in F. W. Schultz" date="1861" rank="species">norvegica</taxon_name>
    <place_of_publication>
      <publication_title>in F. W. Schultz, Arch. Fl.,</publication_title>
      <place_in_publication>311. 1861</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus omalotheca;species norvegica</taxon_hierarchy>
    <other_info_on_name type="fna_id">242334346</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Gunnerus" date="unknown" rank="species">norvegicum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Norveg.</publication_title>
      <place_in_publication>2: 105. 1772</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species norvegicum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–40 cm.</text>
      <biological_entity id="o19063" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline, basal petiolate, blades 3-nerved, lanceolate to oblanceolate, 5–12 cm × 6–30 mm, distal cauline slightly smaller, oblanceolate, faces concolor or weakly bicolor, grayish, thinly woolly.</text>
      <biological_entity id="o19064" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o19065" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s1" to="30" to_unit="mm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s1" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="shape" src="d0_s1" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o19066" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="concolor" value_original="concolor" />
        <character is_modifier="false" modifier="weakly" name="coloration" src="d0_s1" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads (10–60+) in compact, spiciform (leafy-bracteate, sometimes interrupted) arrays (1.5–14 cm, occupying 1/8–1/4 of plant heights, primary-axes usually not visible).</text>
      <biological_entity id="o19067" name="head" name_original="heads" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o19068" from="10" name="atypical_quantity" src="d0_s2" to="60" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19068" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s2" value="compact" value_original="compact" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="spiciform" value_original="spiciform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres cylindro-campanulate, 5.5–6 mm.</text>
      <biological_entity id="o19069" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries brown to reddish-brown with narrow pale center and base.</text>
      <biological_entity id="o19070" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="with base" constraintid="o19072" from="brown" name="coloration" src="d0_s4" to="reddish-brown" />
      </biological_entity>
      <biological_entity id="o19071" name="center" name_original="center" src="d0_s4" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o19072" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Cypselae cylindric, minutely strigose;</text>
      <biological_entity id="o19073" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappus bristles basally connate, falling together.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 56.</text>
      <biological_entity constraint="pappus" id="o19074" name="bristle" name_original="bristles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="together" name="life_cycle" src="d0_s6" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19075" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet or peaty slopes, alpine and subalpine meadows, cliff ledges, rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="peaty slopes" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="subalpine meadows" />
        <character name="habitat" value="cliff ledges" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr., Que.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Norwegian Arctic-cudweed</other_name>
  <other_name type="common_name">gnaphale de Norvège</other_name>
  
</bio:treatment>