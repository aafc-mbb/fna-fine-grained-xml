<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">115</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Rydberg) Semple" date="2003" rank="subsection">humiles</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1820" rank="species">simplex</taxon_name>
    <taxon_name authority="(G. S. Ringius) G. S. Ringius" date="1991" rank="variety">ontarioensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>70: 398. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection humiles;species simplex;variety ontarioensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068793</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">glutinosa</taxon_name>
    <taxon_name authority="G. S. Ringius" date="unknown" rank="variety">ontarioensis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>62: 432. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Solidago;species glutinosa;variety ontarioensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (7–) 13–41 (–61) cm.</text>
      <biological_entity id="o18599" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="13" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="41" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="61" to_unit="cm" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s0" to="41" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal spatulate, acute to obtuse, margins entire-crenate to crenate;</text>
      <biological_entity id="o18600" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o18601" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
      <biological_entity id="o18602" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="entire-crenate" name="shape" src="d0_s1" to="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline (2–) 4–13 (–24), proximal lanceolate to narrowly spatulate, (26–) 44–102 (–185) × (4–) 5.7–12 (–21.5) mm, margins serrate;</text>
      <biological_entity id="o18603" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o18604" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="4" to_inclusive="false" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="24" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="13" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18605" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="narrowly spatulate" />
        <character char_type="range_value" from="26" from_unit="mm" name="atypical_length" src="d0_s2" to="44" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="102" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="185" to_unit="mm" />
        <character char_type="range_value" from="44" from_unit="mm" name="length" src="d0_s2" to="102" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s2" to="5.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="21.5" to_unit="mm" />
        <character char_type="range_value" from="5.7" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18606" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mid to distal (7.8–) 10.1–60 (–132) × 1–8 (–15.5) mm, margins entire or finely serrate distally.</text>
      <biological_entity id="o18607" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="mid to distal" id="o18608" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="7.8" from_unit="mm" name="atypical_length" src="d0_s3" to="10.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="132" to_unit="mm" />
        <character char_type="range_value" from="10.1" from_unit="mm" name="length" src="d0_s3" to="60" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="15.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18609" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="finely; distally" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in open wandlike to congested paniculiform arrays.</text>
      <biological_entity id="o18610" name="head" name_original="heads" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles: bracteoles 1–3 (–4).</text>
      <biological_entity id="o18611" name="peduncle" name_original="peduncles" src="d0_s5" type="structure" />
      <biological_entity id="o18612" name="bracteole" name_original="bracteoles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries: shortest (1.4–) 1.8–2.7 (–3.4) mm.</text>
      <biological_entity id="o18613" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length" src="d0_s6" value="shortest" value_original="shortest" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="1.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae sparsely strigose.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 36.</text>
      <biological_entity id="o18614" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18615" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Basaltic rocks, calcareous shorelines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="basaltic rocks" />
        <character name="habitat" value="calcareous shorelines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Mich.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b</number>
  <other_name type="common_name">Ontario goldenrod</other_name>
  <discussion>Variety ontarioensis is found on basaltic rocks on the Keweenaw Peninsula and the eastern shoreline of Lake Superior, and on calcareous shorelines of central and northern Lake Huron, the Georgian Bay islands, and the tip of Bruce Peninsula, Ontario.</discussion>
  
</bio:treatment>