<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Fougeroux" date="1788" rank="genus">gaillardia</taxon_name>
    <taxon_name authority="Gay" date="1839" rank="species">amblyodon</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>2, 12: 62. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus gaillardia;species amblyodon</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066783</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–45+ cm.</text>
      <biological_entity id="o13687" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves cauline;</text>
      <biological_entity id="o13688" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiolar bases 0–3+ cm;</text>
      <biological_entity constraint="petiolar" id="o13689" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate, oblong, or spatulate, 2–6 cm × 6–24 mm, (bases of distal ± clasping) margins usually entire or obscurely toothed, rarely lobed, faces scabrellous and/or ± villous (hairs scattered, jointed).</text>
      <biological_entity id="o13690" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13691" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o13692" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 5–10 (–15) cm.</text>
      <biological_entity id="o13693" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 22–32, lanceolate to lance-attenuate, 7–15 mm, ciliate with jointed hairs.</text>
      <biological_entity id="o13694" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="22" name="quantity" src="d0_s5" to="32" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="lance-attenuate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
        <character constraint="with hairs" constraintid="o13695" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13695" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacular setae 2.5–3 mm.</text>
      <biological_entity constraint="receptacular" id="o13696" name="seta" name_original="setae" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 8–12;</text>
      <biological_entity id="o13697" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas mostly bright red to dark purple, rarely yellow, 15–25 mm.</text>
      <biological_entity id="o13698" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="mostly bright red" name="coloration" src="d0_s8" to="dark purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 30–60+;</text>
      <biological_entity id="o13699" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s9" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow with purple tips, tubes 1.5 mm, throats narrowly cylindric, 4.5 mm, lobes ovate-deltate, 0.5–1 mm, jointed hairs more than 0.3 mm.</text>
      <biological_entity id="o13700" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character constraint="with tips" constraintid="o13701" is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o13701" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o13702" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o13703" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="4.5" value_original="4.5" />
      </biological_entity>
      <biological_entity id="o13704" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate-deltate" value_original="ovate-deltate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13705" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae dimorphic: the peripheral usually ± obcompressed-clavate, 2–4+ mm, glabrous or nearly so, with pappi of (6–) 8+, muticous scales 0.1–0.3+ mm;</text>
      <biological_entity id="o13706" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position" src="d0_s11" value="peripheral" value_original="peripheral" />
        <character is_modifier="false" modifier="usually more or less" name="shape" src="d0_s11" value="obcompressed-clavate" value_original="obcompressed-clavate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s11" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o13707" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o13708" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="atypical_quantity" src="d0_s11" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s11" upper_restricted="false" />
        <character is_modifier="true" name="shape" src="d0_s11" value="muticous" value_original="muticous" />
      </biological_entity>
      <relation from="o13706" id="r938" name="with" negation="false" src="d0_s11" to="o13707" />
      <relation from="o13707" id="r939" name="part_of" negation="false" src="d0_s11" to="o13708" />
    </statement>
    <statement id="d0_s12">
      <text>the inner obpyramidal, 2–3 mm, hairs 1–3 mm, inserted at bases and on angles, with pappi of 8–12 lanceolate, aristate scales 5–6 mm (scarious bases 2.5–3 × 0.5–1 mm).</text>
      <biological_entity id="o13709" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13710" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13712" name="angle" name_original="angles" src="d0_s12" type="structure" />
      <biological_entity id="o13713" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13714" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s12" to="12" />
        <character is_modifier="true" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s12" value="aristate" value_original="aristate" />
      </biological_entity>
      <relation from="o13711" id="r940" name="inserted at bases and on" negation="false" src="d0_s12" to="o13712" />
      <relation from="o13711" id="r941" name="with" negation="false" src="d0_s12" to="o13713" />
      <relation from="o13713" id="r942" name="part_of" negation="false" src="d0_s12" to="o13714" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 34.</text>
      <biological_entity id="o13711" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13715" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open places on sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open places" constraint="on sandy soils" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Gaillardia amblyodon may prove to be not distinct from G. pulchella at species rank.</discussion>
  
</bio:treatment>