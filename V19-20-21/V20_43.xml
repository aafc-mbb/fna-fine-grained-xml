<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Geraldine A. Allen</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">16</other_info_on_meta>
    <other_info_on_meta type="treatment_page">39</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">EUCEPHALUS</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 298. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus EUCEPHALUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek eu -, good or original, and kephalotos, with a head; alluding “to the elegant qualities of the calyx”—T. Nuttall 1840</other_info_on_name>
    <other_info_on_name type="fna_id">112258</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Nuttall) Munz &amp; D. D. Keck ex A. G. Jones" date="unknown" rank="section">Eucephalus</taxon_name>
    <taxon_hierarchy>genus Aster;section Eucephalus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Nuttall) Bentham" date="unknown" rank="subsection">Eucephalus</taxon_name>
    <taxon_hierarchy>genus Aster;subsection Eucephalus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–160 cm (usually cespitose, induments usually of stipitate-glandular and smooth-surfaced, curved or twisted woolly hairs, plants with caudices or short rhizomes, roots fibrous).</text>
      <biological_entity id="o30801" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="160" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or erect, simple, glabrate, puberulent, pilose, cottony, or woolly, eglandular or glandular.</text>
      <biological_entity id="o30802" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile (proximal withering by flowering; proximalmost reduced, scalelike);</text>
      <biological_entity id="o30803" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (1-nerved) ovate, elliptic, oblong, lanceolate, or linear (± uniform in size), margins entire, faces glabrate, scabrous, cottony, or woolly, eglandular or stipitate-glandular.</text>
      <biological_entity id="o30804" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o30805" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30806" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, usually in open, racemiform, paniculiform, or corymbiform arrays, sometimes borne singly.</text>
      <biological_entity id="o30807" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" notes="" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o30808" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o30807" id="r2843" modifier="usually" name="in" negation="false" src="d0_s6" to="o30808" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres turbinate-cylindric, turbinate, turbinate-obconic, or campanulate, 10–25 mm diam.</text>
      <biological_entity id="o30809" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate-cylindric" value_original="turbinate-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate-obconic" value_original="turbinate-obconic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate-obconic" value_original="turbinate-obconic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 20–50 in 3–6 series, ± unequal (± appressed, often reddish or purplish at margins and tips), 1-nerved (keeled), ovate, lance-oblong, lanceolate, linear-oblong, or linear, chartaceous at bases, margins sometimes hyaline, especially proximally;</text>
      <biological_entity id="o30810" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o30811" from="20" name="quantity" src="d0_s8" to="50" />
        <character is_modifier="false" modifier="more or less" name="size" notes="" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lance-oblong" value_original="lance-oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear" value_original="linear" />
        <character constraint="at bases" constraintid="o30812" is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o30811" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o30812" name="base" name_original="bases" src="d0_s8" type="structure" />
      <biological_entity id="o30813" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>apices acute to obtuse, green, usually puberulent, tomentose, and/or stipitate-glandular, sometimes glabrous.</text>
      <biological_entity id="o30814" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="obtuse" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles ± flat, pitted, epaleate.</text>
      <biological_entity id="o30815" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 0–21 (usually 5, 8, or 13), pistillate, fertile;</text>
      <biological_entity id="o30816" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s11" to="21" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas violet-purple, purple, pink, or white.</text>
      <biological_entity id="o30817" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="violet-purple" value_original="violet-purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 10–35, bisexual, fertile;</text>
      <biological_entity id="o30818" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="35" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, ± ampliate, tubes shorter than funnelform throats, lobes 5, erect or reflexed, triangular;</text>
      <biological_entity id="o30819" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s14" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o30820" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than funnelform throats" constraintid="o30821" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o30821" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o30822" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branch appendages lanceolate.</text>
      <biological_entity constraint="style-branch" id="o30823" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae ± obconic, flattened, laterally 1–2-ribbed, sometimes with 1–2 additional nerves on each face, glabrous, pilose, or strigose, eglandular;</text>
      <biological_entity id="o30824" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="laterally" name="architecture_or_shape" src="d0_s16" value="1-2-ribbed" value_original="1-2-ribbed" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o30825" name="nerve" name_original="nerves" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s16" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s16" value="additional" value_original="additional" />
      </biological_entity>
      <biological_entity id="o30826" name="face" name_original="face" src="d0_s16" type="structure" />
      <relation from="o30824" id="r2844" modifier="sometimes" name="with" negation="false" src="d0_s16" to="o30825" />
      <relation from="o30825" id="r2845" name="on" negation="false" src="d0_s16" to="o30826" />
    </statement>
    <statement id="d0_s17">
      <text>pappi persistent, of 30–50 whitish to tawny, barbellate or smooth, apically clavate or more conspicuously barbellate bristles in 2 (–3) series (outer usually 1 mm or less, sometimes 0, inner 5–10 mm).</text>
      <biological_entity id="o30828" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s17" to="50" />
        <character char_type="range_value" from="whitish" is_modifier="true" name="coloration" src="d0_s17" to="tawny" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="barbellate" value_original="barbellate" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s17" value="clavate" value_original="clavate" />
        <character is_modifier="true" modifier="conspicuously" name="architecture" src="d0_s17" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o30829" name="series" name_original="series" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s17" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
      <relation from="o30827" id="r2846" name="consist_of" negation="false" src="d0_s17" to="o30828" />
      <relation from="o30828" id="r2847" name="in" negation="false" src="d0_s17" to="o30829" />
    </statement>
    <statement id="d0_s18">
      <text>x = 9.</text>
      <biological_entity id="o30827" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o30830" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>143.</number>
  <other_name type="common_name">Aster</other_name>
  <discussion>Species 10 (10 in the flora).</discussion>
  <discussion>Eucephalus, a relatively well-marked western North American group, has been treated as a section of Aster or as a distinct genus. Recent molecular evidence places Eucephalus, together with the eastern North American Doellingeria, at the base of the North American clade of Astereae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray florets usually 1–4, often 0</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray florets commonly 5, 8, or 13+</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray florets 0; leaves 5–9 cm, ± glabrous abaxially, glandular adaxially; plants 60–120 cm; open woods, Lane County, Oregon</description>
      <determination>10 Eucephalus vialis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ray florets usually 1–4; leaves 2–6 cm, hairy; plants 10–100 cm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves glabrous or nearly so abaxially, moderately to densely hairy adaxially</description>
      <determination>9 Eucephalus tomentellus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves glabrous, eglandular or sparsely glandular on both faces</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries subequal</description>
      <determination>1 Eucephalus breweri</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries strongly unequal</description>
      <determination>4 Eucephalus glabratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems, leaves, and phyllaries glabrous, glaucous; plants 40–160 cm; leaves linear tonarrowly lance-elliptic, 4–10 cm; rays purple</description>
      <determination>5 Eucephalus glaucescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems, leaves, and phyllaries pubescent or glabrate, glandular or not, not glaucous; plants 10–120(–150) cm; leaves elliptic, oblong, lance-ovate, lance-elliptic, lanceolate, linear-oblong or -lanceolate, 1.5–10 cm; rays white, pink, violet, or purple.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves 5–10 cm, elliptic to lanceolate, glabrous and eglandular, or abaxially ± glandular and/or villous; plants 50–150 cm; rays white to pink</description>
      <determination>3 Eucephalus engelmannii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves 1.5–7 cm, elliptic, elliptic-oblong, oblong, lance-ovate, lance-elliptic, linear-oblong or -lanceolate, glandular or not, scabrous or cottony; plants 10–80 cm; rays white or violet to purple</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Rays white; stems pilose or sparsely to moderately glandular-pubescent</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Rays violet to purple; stems scabrous (to scabrellous) or cottony and/or glandular-pubescent (especially peduncles)</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries lance-ovate; Cascade Mountains, Oregon</description>
      <determination>6 Eucephalus gormanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllaries lance-linear; Olympic Mountains, Washington</description>
      <determination>8 Eucephalus paucicapitatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves moderately scabrellous (and sometimes glandular) on both faces</description>
      <determination>2 Eucephalus elegans</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves sparsely scabrous abaxially, strongly cottony adaxially</description>
      <determination>7 Eucephalus ledophyllus</determination>
    </key_statement>
  </key>
</bio:treatment>