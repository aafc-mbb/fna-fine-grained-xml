<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="treatment_page">154</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) G. L. Nesom" date="1993" rank="subsection">triplinerviae</taxon_name>
    <taxon_name authority="C. E. S. Taylor &amp; R. J. Taylor" date="1983" rank="species">altiplanities</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>10: 178, figs 2, 3. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection triplinerviae;species altiplanities</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067534</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–100 cm;</text>
      <biological_entity id="o12239" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices woody, rhizomes elongate, branching, woody, forming new centers of growth.</text>
      <biological_entity id="o12240" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o12241" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o12242" name="center" name_original="centers" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o12243" name="growth" name_original="growth" src="d0_s1" type="structure" />
      <relation from="o12241" id="r1113" name="forming" negation="false" src="d0_s1" to="o12242" />
      <relation from="o12241" id="r1114" name="part_of" negation="false" src="d0_s1" to="o12243" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20, erect, finely scabroso-puberulent, sparsely so with age proximally, densely so distally.</text>
      <biological_entity id="o12244" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="scabroso-puberulent" value_original="scabroso-puberulent" />
      </biological_entity>
      <biological_entity id="o12245" name="age" name_original="age" src="d0_s2" type="structure" />
      <relation from="o12244" id="r1115" modifier="sparsely; densely; distally" name="with" negation="false" src="d0_s2" to="o12245" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline withering by flowering, subsessile, gradually tapering to short-winged petiolelike bases;</text>
      <biological_entity id="o12246" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
        <character char_type="range_value" from="gradually tapering" name="shape" src="d0_s3" to="short-winged petiolelike" />
      </biological_entity>
      <biological_entity id="o12247" name="base" name_original="bases" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>mid and distal cauline sessile, blades linear-elliptic to linear-lanceolate, 40–90 × 4–5 mm, 3-nerved from base, midnerves prominent, margins entire, finely scabrous, apices attenuate-acute, faces finely, sparsely to moderately strigose, more so on main nerves, finer nerves translucent, faces sometimes shiny.</text>
      <biological_entity id="o12248" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o12249" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-elliptic" name="shape" src="d0_s4" to="linear-lanceolate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character constraint="from base" constraintid="o12250" is_modifier="false" name="architecture" src="d0_s4" value="3-nerved" value_original="3-nerved" />
      </biological_entity>
      <biological_entity id="o12250" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o12251" name="midnerve" name_original="midnerves" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o12252" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12253" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate-acute" value_original="attenuate-acute" />
      </biological_entity>
      <biological_entity id="o12254" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="main" id="o12255" name="nerve" name_original="nerves" src="d0_s4" type="structure" />
      <biological_entity constraint="finer" id="o12256" name="nerve" name_original="nerves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s4" value="translucent" value_original="translucent" />
      </biological_entity>
      <biological_entity id="o12257" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
      </biological_entity>
      <relation from="o12254" id="r1116" name="on" negation="false" src="d0_s4" to="o12255" />
    </statement>
    <statement id="d0_s5">
      <text>Heads 25–350, in short-to-elongate, secund paniculiform arrays, branches ascending or ascending and distally recurved, sometimes second, sometimes elongate.</text>
      <biological_entity id="o12258" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s5" to="350" />
      </biological_entity>
      <biological_entity id="o12259" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="short-to-elongate" value_original="short-to-elongate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o12260" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="sometimes; sometimes" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o12258" id="r1117" name="in" negation="false" src="d0_s5" to="o12259" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 2–6 mm, finely strigose;</text>
      <biological_entity id="o12261" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles 1–10, often crowded, linear-lanceolate, 2–3 mm, grading into phyllaries.</text>
      <biological_entity id="o12262" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="10" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12263" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure" />
      <relation from="o12262" id="r1118" name="into" negation="false" src="d0_s7" to="o12263" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly campanulate, 3.5–4 mm.</text>
      <biological_entity id="o12264" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–4 series, linear-lanceolate, strongly unequal, margins hyaline, distally subulate-ciliate, apices acute, glabrous.</text>
      <biological_entity id="o12265" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s9" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o12266" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o12267" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s9" value="subulate-ciliate" value_original="subulate-ciliate" />
      </biological_entity>
      <biological_entity id="o12268" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12265" id="r1119" name="in" negation="false" src="d0_s9" to="o12266" />
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 4–5;</text>
      <biological_entity id="o12269" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>laminae 1.5–2.5 × 0.5 mm.</text>
      <biological_entity id="o12270" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character name="width" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 6–8, 3–3.5 mm, lobes 0.5 mm.</text>
      <biological_entity id="o12271" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s12" to="8" />
        <character char_type="range_value" from="3" from_unit="mm" name="distance" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12272" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae (narrowly obconic) 1.5–2.5 mm, sparsely strigillose;</text>
      <biological_entity id="o12273" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 3 mm. 2n = 18.</text>
      <biological_entity id="o12274" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12275" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed gypsum and shale soils, rocky slopes, escarpments, and ridges in high plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed gypsum" />
        <character name="habitat" value="shale soils" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="escarpments" />
        <character name="habitat" value="ridges" constraint="in high plains" />
        <character name="habitat" value="high plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>59.</number>
  <other_name type="common_name">High-plains goldenrod</other_name>
  
</bio:treatment>