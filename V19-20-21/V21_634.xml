<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">257</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="(A. Gray) Bentham &amp; Hooker f." date="1873" rank="genus">raillardella</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray in W. H. Brewer et al." date="1876" rank="species">scaposa</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>1: 417. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus raillardella;species scaposa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067434</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Railliardia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">scaposa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 551. 1865 (as Raillardia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Railliardia;species scaposa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 6–53 cm.</text>
      <biological_entity id="o2348" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="53" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades lanceolate or oblanceolate to linear, margins entire, faces stipitate-glandular, sometimes sparsely scabrellous as well.</text>
      <biological_entity id="o2349" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="linear" />
      </biological_entity>
      <biological_entity id="o2350" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2351" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes sparsely; well" name="relief" src="d0_s1" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ray-florets 0 or 1–7;</text>
      <biological_entity id="o2352" name="ray-floret" name_original="ray-florets" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corollas yellow to yellow-orange, laminae 5–25+ mm.</text>
      <biological_entity id="o2353" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s3" to="yellow-orange" />
      </biological_entity>
      <biological_entity id="o2354" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc-florets 7–44;</text>
      <biological_entity id="o2355" name="disc-floret" name_original="disc-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="44" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corollas yellow to yellow-orange, 7.5–12 mm. 2n = 68, 70.</text>
      <biological_entity id="o2356" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s5" to="yellow-orange" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2357" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="68" value_original="68" />
        <character name="quantity" src="d0_s5" value="70" value_original="70" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet to dry, often sandy sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet to dry" />
        <character name="habitat" value="wet to sandy sites" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Variation among populations of Raillardella scaposa spans the morphologic and ecologic divide between R. argentea and R. pringlei; R. scaposa is evidently an allopolyploid that descended from ancestors closely related to each of the other two species. Raillardella scaposa is widely distributed in the Sierra Nevada and southern Cascade Range, often near populations of R. argentea. Putative hybrids between R. scaposa and R. argentea have been noted (e.g., R. Snow 293, UC, from Tuolumne County, California).</discussion>
  
</bio:treatment>