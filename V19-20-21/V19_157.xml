<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">scariosum</taxon_name>
    <taxon_name authority="(Petrak) D. J. Keil" date="2004" rank="variety">citrinum</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 215. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species scariosum;variety citrinum</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068209</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(A. Gray) Jepson" date="unknown" rank="species">quercetorum</taxon_name>
    <taxon_name authority="Petrak" date="unknown" rank="variety">citrinum</taxon_name>
    <place_of_publication>
      <publication_title>Beih. Bot. Centralbl.</publication_title>
      <place_in_publication>35(2): 363. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cirsium;species quercetorum;variety citrinum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">validus</taxon_name>
    <taxon_hierarchy>genus Carduus;species validus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_name authority="Petrak" date="unknown" rank="subspecies">latisquamum</taxon_name>
    <taxon_hierarchy>genus Cirsium;species drummondii;subspecies latisquamum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="Petrak" date="unknown" rank="species">loncholepis</taxon_name>
    <taxon_hierarchy>genus Cirsium;species loncholepis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants acaulescent to bushy and mounding or erect, 5–100 cm.</text>
      <biological_entity id="o3013" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushy" value_original="bushy" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mounding" value_original="mounding" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, branched distally or openly so throughout, leafy, glabrous or thinly arachnoid-tomentose.</text>
      <biological_entity id="o3014" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="distally; distally; openly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s1" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades oblanceolate to elliptic, deeply pinnately lobed, longer spines slender to stout, usually 1 cm or shorter, abaxial faces usually green, glabrous or glabrate or thinly to densely tomentose, adaxial faces glabrous to sparingly villous with septate trichomes or thinly arachnoid-tomentose.</text>
      <biological_entity id="o3015" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3016" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="elliptic" />
        <character is_modifier="false" modifier="deeply pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="longer" id="o3017" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s2" to="stout" />
        <character modifier="usually" name="some_measurement" src="d0_s2" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3018" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="glabrate or" name="pubescence" src="d0_s2" to="thinly densely tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3019" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="with trichomes" constraintid="o3020" from="glabrous" name="pubescence" src="d0_s2" to="sparingly villous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
      </biological_entity>
      <biological_entity id="o3020" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 2–many, subsessile or short-pedunculate, in ± congested corymbiform to subcapitate arrays at stem tips (in age clustered axillary heads often developing), subtended and ± overtopped by distal leaves or these ± reduced.</text>
      <biological_entity id="o3021" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="false" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-pedunculate" value_original="short-pedunculate" />
      </biological_entity>
      <biological_entity id="o3022" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform to subcapitate" value_original="corymbiform to subcapitate" />
      </biological_entity>
      <biological_entity constraint="stem" id="o3023" name="tip" name_original="tips" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o3024" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less; more or less" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o3021" id="r297" modifier="in more or less congested" name="corymbiform to subcapitate" negation="false" src="d0_s3" to="o3022" />
      <relation from="o3022" id="r298" name="at" negation="false" src="d0_s3" to="o3023" />
      <relation from="o3021" id="r299" name="overtopped by" negation="false" src="d0_s3" to="o3024" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres 2.5–4 cm.</text>
      <biological_entity id="o3025" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries: outer and mid lanceolate to ovate, spines 1–6 mm, slender and weak or broad and flat;</text>
      <biological_entity id="o3026" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity constraint="outer and mid" id="o3027" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovate" />
      </biological_entity>
      <biological_entity id="o3028" name="spine" name_original="spines" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="weak" value_original="weak" />
        <character is_modifier="false" name="width" src="d0_s5" value="broad" value_original="broad" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apices of inner stiffly erect or thin and often contorted, acuminate and entire or rarely toothed.</text>
      <biological_entity id="o3029" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure" />
      <biological_entity id="o3030" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="often" name="arrangement_or_shape" src="d0_s6" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3031" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure" />
      <relation from="o3030" id="r300" name="part_of" negation="false" src="d0_s6" to="o3031" />
    </statement>
    <statement id="d0_s7">
      <text>Corollas white or lightly purple-tinged (purple), (21–) 26–35 mm, tubes (11–) 14–22 mm, throats 6–9 mm, lobes (4–) 5.5–8 mm;</text>
      <biological_entity id="o3032" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="lightly" name="coloration" src="d0_s7" value="purple-tinged" value_original="purple-tinged" />
        <character char_type="range_value" from="21" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="26" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="26" from_unit="mm" name="some_measurement" src="d0_s7" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3033" name="tube" name_original="tubes" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="14" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s7" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3034" name="throat" name_original="throats" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3035" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style tips 4–6 mm.</text>
      <biological_entity constraint="style" id="o3036" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 4.5–5 mm;</text>
      <biological_entity id="o3037" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 23–31 mm. 2n = 34 (as Cirsium loncholepis).</text>
      <biological_entity id="o3038" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s10" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3039" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly spring–summer (Apr–Jul, occasionally as late as Nov in coastal sites).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="range_value" modifier="occasionally;  in coastal sites" to="late " from="late " constraint=" as Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet ground, meadows, pastures, springs, marshes, both coastal and interior</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" modifier="wet ground" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="coastal" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California?).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California?)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54g.</number>
  <other_name type="common_name">La Graciosa thistle</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety citrinum is the southern California race of Cirsium scariosum. As is the case in other varieties of this complex, taxonomic recognition has been given to local populations, named on the basis of limited samples. After examining specimens gathered from various populations in southern California, I realized that although exemplars of local populations may have distinctive features, there are so many intermediates that recognizing any of the local races becomes arbitrary. Individuals treated as Cirsium loncholepis (from coastal sites bordering the mouth of the Santa Maria River and adjacent regions in San Luis Obispo and Santa Barbara counties) are often indistinguishable from plants of upland populations of the San Emigdio Mountains (Kern and Ventura counties) in the vicinity of the headwaters of the Cuyama River, a tributary of the Santa Maria River. Other individuals from the San Emigdio Mountains cannot be distinguished from plants of other upland and lowland southern California sites ranging south to San Diego County.</discussion>
  <discussion>Cirsium loncholepis was recognized by the California Department of Fish and Game in 1990 as a threatened species, and in 2000 by the U.S. Fish and Wildlife Service as endangered. Its conservation status is being reevaluated in light of my conclusions about its taxonomy. I believe that the combined taxon, C. scariosum var. citrinum, should still be of conservation concern. Many of the collections from southern California are from highly developed areas, and the number and size of extant populations need to be evaluated.</discussion>
  <discussion>Interspecific hybridization involving var. citrinum is known or suspected in several instances. A putative hybrid between it and Cirsium occidentale var. occidentale from dunes in San Luis Obispo County combines the leaf pubescence and arachnoid involucre of C. occidentale with features of C. scariosum. An apparent hybrid between var. citrinum and the strikingly different C. rhothophilum was collected from coastal dunes in northern Santa Barbara County. A population of unusual, purple-flowered individuals from Price Canyon northeast of Los Alamos in Santa Barbara County may be the product of past hybridization with another species. These plants are tall (1–1.6 m according to label data) and their stems and distal leaf faces are villous with septate trichomes.</discussion>
  
</bio:treatment>