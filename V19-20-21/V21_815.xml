<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="treatment_page">328</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="(A. Gray) A. M. Powell" date="1968" rank="section">laphamia</taxon_name>
    <taxon_name authority="(M. E. Jones) Rydberg in N. L. Britton et al." date="1914" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>34: 19. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section laphamia;species gracilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067322</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Laphamia</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 703. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Laphamia;species gracilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 10–20 (–25) cm (stems erect, spreading, or pendent);</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrate or densely puberulent.</text>
      <biological_entity id="o25506" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o25507" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 2–5 (–10) mm;</text>
      <biological_entity id="o25508" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25509" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades palmately 3-lobed, or ± cruciform, sometimes entire, or with 1–3 lobes or teeth per side, 3–10 (–15) × 2–15 mm, ultimate margins sometimes irregularly lobed or toothed.</text>
      <biological_entity id="o25510" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o25511" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cruciform" value_original="cruciform" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cruciform" value_original="cruciform" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" notes="" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25512" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o25513" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity id="o25514" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="ultimate" id="o25515" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes irregularly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <relation from="o25511" id="r1732" name="with" negation="false" src="d0_s3" to="o25512" />
      <relation from="o25511" id="r1733" name="with" negation="false" src="d0_s3" to="o25513" />
      <relation from="o25513" id="r1734" name="per" negation="false" src="d0_s3" to="o25514" />
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or (2–6+) in corymbiform arrays, 5–7 × 4–5.5 mm.</text>
      <biological_entity id="o25516" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o25517" from="2" name="atypical_quantity" src="d0_s4" to="6" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="" src="d0_s4" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25517" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–15 mm.</text>
      <biological_entity id="o25518" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate.</text>
      <biological_entity id="o25519" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries (8–) 12–17, linear-lanceolate to lanceolate, 3.5–5.5 × 0.7–1.2 mm.</text>
      <biological_entity id="o25520" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s7" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="17" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 0.</text>
      <biological_entity id="o25521" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 30–42;</text>
      <biological_entity id="o25522" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s9" to="42" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, tubes 0.8–1.1 mm, throats ± tubular, (1.3–) 1.6–2 mm, lobes 0.4–0.6 mm.</text>
      <biological_entity id="o25523" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o25524" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25525" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25526" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae oblongelliptic, 1.8–2.5 (–2.8) mm, margins thin-calloused, short-hairy;</text>
      <biological_entity id="o25527" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25528" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="thin-calloused" value_original="thin-calloused" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi 0 or of 1 (–3) stout bristles to 2 mm, usually plus callous crowns or vestigial, hyaline scales.</text>
      <biological_entity id="o25529" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character char_type="range_value" from="of 1(" is_modifier="false" name="quantity" src="d0_s12" to="3) stout bristles" />
      </biological_entity>
      <biological_entity id="o25530" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s12" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="true" name="fragility_or_size" src="d0_s12" value="stout" value_original="stout" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25531" name="crown" name_original="crowns" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="callous" value_original="callous" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s12" value="vestigial" value_original="vestigial" />
      </biological_entity>
      <relation from="o25529" id="r1735" name="consist_of" negation="false" src="d0_s12" to="o25530" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 68.</text>
      <biological_entity id="o25532" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25533" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices of limestone rock faces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of limestone rock" />
        <character name="habitat" value="limestone rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Slender or three-lobed rock daisy</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Perityle gracilis is known from southeastern Nevada in Meadow Valley Wash and in the Sheep Range. The only Arizona collection is the type (M. E. Jones in 1894, Kaibab Plateau). The 3-fid leaves with cuneate bases set P. gracilis apart from the similar P. congesta and P. tenella.</discussion>
  
</bio:treatment>