<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="treatment_page">204</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">vernonieae</taxon_name>
    <taxon_name authority="Blume" date="1826" rank="genus">CYANTHILLIUM</taxon_name>
    <place_of_publication>
      <publication_title>Bijdr. Fl. Ned. Ind.,</publication_title>
      <place_in_publication>889. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe vernonieae;genus CYANTHILLIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Origin uncertain; probably Greek cyanos, blue, and anthyllion, little flower, alluding to corollas</other_info_on_name>
    <other_info_on_name type="fna_id">108770</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (perhaps persisting), 2–6 (–12+) dm.</text>
      <biological_entity id="o17633" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="12" to_unit="dm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly cauline (at flowering);</text>
      <biological_entity id="o17634" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o17635" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles ± winged;</text>
      <biological_entity id="o17636" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s2" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades ovate to trullate, deltate, oblanceolate, or spatulate, bases ± cuneate, margins serrate, apices rounded to acute, abaxial faces ± hirtellous to densely piloso-strigillose, resin-gland-dotted, adaxial faces ± scabrellous or glabrate.</text>
      <biological_entity id="o17637" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="trullate deltate oblanceolate or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="trullate deltate oblanceolate or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="trullate deltate oblanceolate or spatulate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="trullate deltate oblanceolate or spatulate" />
      </biological_entity>
      <biological_entity id="o17638" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o17639" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o17640" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17641" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="less hirtellous" name="pubescence" src="d0_s3" to="densely piloso-strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="resin-gland-dotted" value_original="resin-gland-dotted" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17642" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads discoid, ± pedunculate, not subtended by foliaceous bracts, (12–) 40–100+ in ± corymbiform arrays (6–) 10–15+ cm diam.</text>
      <biological_entity id="o17643" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="pedunculate" value_original="pedunculate" />
        <character char_type="range_value" from="6" from_unit="cm" name="diameter" src="d0_s4" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="diameter" src="d0_s4" to="15" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17644" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="12" modifier="not" name="atypical_quantity" src="d0_s4" to="40" to_inclusive="false" />
        <character char_type="range_value" constraint="in more or less corymbiform arrays" from="40" modifier="not" name="quantity" src="d0_s4" to="100" upper_restricted="false" />
      </biological_entity>
      <relation from="o17643" id="r1588" name="subtended by" negation="false" src="d0_s4" to="o17644" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate to turbinate or hemispheric, 4–5 mm diam.</text>
      <biological_entity id="o17645" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s5" to="turbinate or hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 24–32+ in 3–4+ series, the outer subulate to lanceolate, inner ± lanceolate, all ± chartaceous, margins entire, tips apiculate to spinose, abaxial faces ± strigillose, ± resin-gland-dotted.</text>
      <biological_entity id="o17646" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o17647" from="24" name="quantity" src="d0_s6" to="32" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17647" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="4" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17648" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17649" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="pubescence_or_texture" src="d0_s6" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o17650" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17651" name="tip" name_original="tips" src="d0_s6" type="structure">
        <character char_type="range_value" from="apiculate" name="architecture_or_shape" src="d0_s6" to="spinose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17652" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="resin-gland-dotted" value_original="resin-gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 13–20 (–24+);</text>
      <biological_entity id="o17653" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="24" upper_restricted="false" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas lavender to pink or purplish, tubes longer than funnelform throats, lobes 5, lance-linear, ± equal.</text>
      <biological_entity id="o17654" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s8" to="pink or purplish" />
      </biological_entity>
      <biological_entity id="o17655" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character constraint="than funnelform throats" constraintid="o17656" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o17656" name="throat" name_original="throats" src="d0_s8" type="structure" />
      <biological_entity id="o17657" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae ± columnar, not ribbed, ± strigillose;</text>
      <biological_entity id="o17658" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="columnar" value_original="columnar" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s9" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of ± 20 ± persistent outer scales, plus ± 20 caducous inner bristles.</text>
      <biological_entity constraint="outer" id="o17660" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character modifier="more or less" name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17661" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character is_modifier="true" name="duration" src="d0_s10" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>x = 9 (18?).</text>
      <biological_entity id="o17659" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character modifier="more or less" name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
      <biological_entity constraint="x" id="o17662" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="9" value_original="9" />
        <character name="quantity" src="d0_s11" value="[18" value_original="[18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; probably paleotropical in origin, now widely established in tropical and warm-temperate regions as naturalized ruderals.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="probably paleotropical in origin" establishment_means="native" />
        <character name="distribution" value="now widely established in tropical and warm-temperate regions as naturalized ruderals" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <discussion>Species 1–2 (1 in the flora).</discussion>
  
</bio:treatment>